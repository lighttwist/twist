@echo off

rem  Put together the backup file name

set month=%date:~7,2%
if %month%==01 set month=Jan
if %month%==02 set month=Feb
if %month%==03 set month=Mar
if %month%==04 set month=Apr
if %month%==05 set month=May
if %month%==06 set month=Jun
if %month%==07 set month=Jul
if %month%==08 set month=Aug
if %month%==09 set month=Sep
if %month%==10 set month=Oct
if %month%==11 set month=Nov
if %month%==12 set month=Dec

set  day=%date:~4,2%
set year=%date:~-4,4%

set archive=%~dp0..\%month%%day%-%year% twist2019_lib.7z


rem  Back the Lighttwist library source up

cd "%~dp0"

call "_clean_up.bat"

cd ..

"C:\Program Files\7-Zip\7z.exe" a -mx9 -mmt -mhe -ppamplemousse -x!repo\external -x!repo\.git "%archive%" repo

move /Y "%archive%" "F:\Dropbox\Backup\twist_lib\"

echo "%archive%" backed-up



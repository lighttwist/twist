
#include "app_includes.hpp"

#include "unstructured_test_utils.hpp"

#include "twist/os_utils.hpp"
#include "twist/test/test_globals.hpp"

#include <catch2/catch_session.hpp>

#include <iostream>

// #define RUN_UNSTRUCT_TESTS

using namespace twist;
using namespace twist::test;

auto initialise() -> bool
{
	try {
		auto root_dir = get_running_module_path().parent_path() / L"../../../../__test_data";
		TwistTestDataManager::init(std::move(root_dir));
		return true;
	}
	catch (const std::exception& ex) {
		std::cout << "Exception caught: " << ex.what();
		return false;
	}
}

auto main([[maybe_unused]] int argc, [[maybe_unused]] char* argv[]) -> int 
{
	if (!initialise()) {
		return -1;
	}

#ifndef RUN_UNSTRUCT_TESTS
	return Catch::Session().run(argc, argv);
#else
	try {
		run_unstructured_tests();
		return 0;
	}
	catch (const std::exception& ex) {
		std::cout << "Exception caught: " << ex.what();
		return -1;
	}
#endif
}


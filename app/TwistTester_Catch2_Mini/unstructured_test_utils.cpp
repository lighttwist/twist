/// @file "unstructured_test_utils.cpp"
/// Implementation file for "unstructured_test_utils.hpp"

//  Copyright (c) 2007-2022, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "TwistTester_Catch2_Mini/app_includes.hpp"

#include "TwistTester_Catch2_Mini/unstructured_test_utils.hpp"

#include "twist/feedback_providers.hpp"
#include "twist/test/gis/test_gis.hpp"
#include "twist/test/math/test_space_grid.hpp"
#include "twist/test/db/test_csv.hpp"
#include "twist/test/db/test_table.hpp"

#pragma warning (disable: 4100)

using namespace twist;

void run_unstructured_db_tests(MessageFeedbackProv& feedback_prov);
void run_unstructured_gis_tests(MessageFeedbackProv& feedback_prov);
void run_unstructured_math_tests(MessageFeedbackProv& feedback_prov);

void run_unstructured_tests()
{
	auto feedback_prov = make_cout_feedback_prov();
	run_unstructured_db_tests(feedback_prov);
	//run_unstructured_gis_tests(feedback_prov);
	//run_unstructured_math_tests(feedback_prov);
}

void run_unstructured_db_tests(MessageFeedbackProv& feedback_prov)
{
	using namespace twist::test::db;
}

void run_unstructured_gis_tests(MessageFeedbackProv& feedback_prov)
{
	using namespace twist::test::gis;
}

void run_unstructured_math_tests(MessageFeedbackProv& feedback_prov)
{
	using namespace twist::test::math;
}

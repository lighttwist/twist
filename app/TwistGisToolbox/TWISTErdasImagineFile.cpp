/// @file TWISTErdasImagineFile.cpp
/// Implementation file for "TWISTErdasImagineFile.h"

//  Copyright (c) 2007-2022, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 
#include "TwistGisToolbox/pch.h"

#include "TwistGisToolbox/TWISTErdasImagineFile.h"

#include "TwistGisToolbox/TWISTSquareGeoSpaceGrid.h"

#include "twist/gis/ErdasImagineFile.hpp"
#include "twist/math/ClosedInterval.hpp"

TWIST_COM_CLASS_IMPLEMENTATIONS(TWISTErdasImagineFile, IID_ITWISTErdasImagineFile)

using namespace twist::com;
using namespace twist::gis;

TWISTErdasImagineFile::TWISTErdasImagineFile()
{
}

TWISTErdasImagineFile::~TWISTErdasImagineFile()
{
}

STDMETHODIMP TWISTErdasImagineFile::width(INT* thewidth)
{
	COM_METHOD_IMPL_BEGIN
	if (is_closed_) {
		TWIST_THROW(L"The file has been closed.");
	}
	*thewidth = static_cast<INT>(file_->width());
	COM_METHOD_IMPL_END
}
	
STDMETHODIMP TWISTErdasImagineFile::height(INT* theheight)
{
	COM_METHOD_IMPL_BEGIN
	if (is_closed_) {
		TWIST_THROW(L"The file has been closed.");
	}
	*theheight = static_cast<INT>(file_->height());
	COM_METHOD_IMPL_END
}

STDMETHODIMP TWISTErdasImagineFile::space_grid(ITWISTSquareGeoSpaceGrid** grid)
{
	COM_METHOD_IMPL_BEGIN
	if (is_closed_) {
		TWIST_THROW(L"The file has been closed.");
	}
	auto map_info = file_->read_map_info();
	if (!map_info) {
		TWIST_THRO2(L"IMG file \"{}\" does not contain geographic info.", file_->path().filename().c_str());
	}
	auto grid_ptr = TWISTSquareGeoSpaceGrid::create(map_info->space_grid());
	grid_ptr.CopyTo(grid);
	COM_METHOD_IMPL_END
}
	
STDMETHODIMP TWISTErdasImagineFile::data_type(TWISTDataValueType* datatype)
{
	COM_METHOD_IMPL_BEGIN
	if (is_closed_) {
		TWIST_THROW(L"The file has been closed.");
	}
	*datatype = static_cast<TWISTDataValueType>(file_->data_type());
	COM_METHOD_IMPL_END
}

STDMETHODIMP TWISTErdasImagineFile::read_uint8(VARIANT_BOOL* has_nodata_value, INT* nodata_value, VARIANT* data_grid)
{
	COM_METHOD_IMPL_BEGIN
	read_impl<uint8_t>(VT_UI1, data_grid, has_nodata_value, nodata_value);
	COM_METHOD_IMPL_END
}

STDMETHODIMP TWISTErdasImagineFile::read_int16(VARIANT_BOOL* has_nodata_value, INT* nodata_value, VARIANT* data_grid)
{
	COM_METHOD_IMPL_BEGIN
	read_impl<int16_t>(VT_I2, data_grid, has_nodata_value, nodata_value);
	COM_METHOD_IMPL_END
}

STDMETHODIMP TWISTErdasImagineFile::read_uint16(VARIANT_BOOL* has_nodata_value, INT* nodata_value, VARIANT* data_grid)
{
	COM_METHOD_IMPL_BEGIN
	read_impl<uint16_t>(VT_UI2, data_grid, has_nodata_value, nodata_value);	
	COM_METHOD_IMPL_END
}

STDMETHODIMP TWISTErdasImagineFile::read_int32(VARIANT_BOOL* has_nodata_value, INT* nodata_value, VARIANT* data_grid)
{
	COM_METHOD_IMPL_BEGIN
	read_impl<int32_t>(VT_I4, data_grid, has_nodata_value, nodata_value);
	COM_METHOD_IMPL_END
}

STDMETHODIMP TWISTErdasImagineFile::write_uint8(VARIANT data_grid)
{
	COM_METHOD_IMPL_BEGIN
	if (is_closed_) {
		TWIST_THROW(L"The file has been closed.");
	}

	const auto data_grd = copy_from_2d_numeric_safearray<std::uint8_t>(data_grid);
	file_->write_uint8(data_grd, std::nullopt);
	COM_METHOD_IMPL_END
}

STDMETHODIMP TWISTErdasImagineFile::write_uint8_with_nodata_val(VARIANT data_grid, 
		 unsigned char nodata_value)
{
	COM_METHOD_IMPL_BEGIN
	if (is_closed_) TWIST_THROW(L"The file has been closed.");

	const auto data_grd = copy_from_2d_numeric_safearray<std::uint8_t>(data_grid);
	file_->write_uint8(data_grd, nodata_value);
	COM_METHOD_IMPL_END
}

STDMETHODIMP TWISTErdasImagineFile::write_uint16(VARIANT data_grid)
{
	COM_METHOD_IMPL_BEGIN
	if (is_closed_) {
		TWIST_THROW(L"The file has been closed.");
	}
	const auto data_grd = copy_from_2d_numeric_safearray<std::uint16_t>(data_grid);
	file_->write_uint16(data_grd, std::nullopt);
	COM_METHOD_IMPL_END
}

STDMETHODIMP TWISTErdasImagineFile::write_uint16_with_nodata_val(VARIANT data_grid, unsigned short nodata_value)
{
	COM_METHOD_IMPL_BEGIN
	if (is_closed_) {
		TWIST_THROW(L"The file has been closed.");
	}
	const auto data_grd = copy_from_2d_numeric_safearray<std::uint16_t>(data_grid);
	file_->write_uint16(data_grd, nodata_value);
	COM_METHOD_IMPL_END
}

STDMETHODIMP TWISTErdasImagineFile::write_int32(VARIANT data_grid)
{
	COM_METHOD_IMPL_BEGIN
	if (is_closed_) {
		TWIST_THROW(L"The file has been closed.");
	}
	const auto data_grd = copy_from_2d_numeric_safearray<std::int32_t>(data_grid);
	file_->write_int32(data_grd, std::nullopt);
	COM_METHOD_IMPL_END
}

STDMETHODIMP TWISTErdasImagineFile::write_int32_with_nodata_val(VARIANT data_grid, 
		 INT nodata_value)
{
	COM_METHOD_IMPL_BEGIN
	if (is_closed_) {
		TWIST_THROW(L"The file has been closed.");
	}
	const auto data_grd = copy_from_2d_numeric_safearray<std::int32_t>(data_grid);
	file_->write_int32(data_grd, nodata_value);
	COM_METHOD_IMPL_END
}

STDMETHODIMP TWISTErdasImagineFile::close()
{
	COM_METHOD_IMPL_BEGIN
	if (is_closed_) {
		TWIST_THROW(L"The file has already been closed.");
	}
	file_.reset();
	is_closed_ = true;
	COM_METHOD_IMPL_END
}

template<class DataVal>
void TWISTErdasImagineFile::read_impl(VARTYPE vartype, VARIANT* data_grid, VARIANT_BOOL* has_nodata_value, 
		INT* nodata_value)
{
	if (is_closed_) {
		TWIST_THROW(L"The file has been closed.");
	}

	auto safearray = make_2d_safearray(vartype, 0, file_->height(), 0, file_->width());
	*data_grid = safearray_to_variant(*safearray, vartype);
	
	// Read the data from file
	std::optional<DataVal> nodata_val;
	const auto data = read<DataVal>(*file_, nodata_val);

	// Copy the data to the safearray
	copy_to_2d_safearray<DataVal>(*data, *data_grid);

	*has_nodata_value = to_varbool(nodata_val.has_value());
	if (nodata_val) {
		*nodata_value = *nodata_val;	
	}
}

auto TWISTErdasImagineFile::create_file(const fs::path& path, int width, int height, 
		GeoGridDataType data_type) -> CComPtr<ITWISTErdasImagineFile> 
{
	auto [icom, com] = create_com_atl<ITWISTErdasImagineFile, TWISTErdasImagineFile>();
	com->file_ = std::make_unique<ErdasImagineFile>(ErdasImagineFile::create, path, width, height, data_type);
	return icom;
}

auto TWISTErdasImagineFile::open_file(const fs::path& path) -> CComPtr<ITWISTErdasImagineFile>
{
	auto [icom, com] = create_com_atl<ITWISTErdasImagineFile, TWISTErdasImagineFile>();
	com->file_ = std::make_unique<ErdasImagineFile>(ErdasImagineFile::open, path);
	return icom;
}

#ifdef _DEBUG
void TWISTErdasImagineFile::check_invariant() const noexcept
{
	assert((file_ == nullptr) == is_closed_);
}
#endif 


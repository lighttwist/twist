/// @file TWISTGeotiffProjection.h
/// TWISTGeotiffProjection COM class definition

//  Copyright (c) 2007-2022, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#pragma once

#include "TwistGisToolbox/resource.h"       
#include "TwistGisToolbox/TwistGisToolbox_i.h"

namespace twist::gis {
class GeotiffProjection;
}

using namespace ATL;

/*! Class storing information about the projection used by a geospatial coordinate system / projection used by 
    and stored inside a GeoTIFF.
 */
class ATL_NO_VTABLE TWISTGeotiffProjection :
	public CComObjectRootEx<CComMultiThreadModel>,
	public CComCoClass<TWISTGeotiffProjection, &CLSID_TWISTGeotiffProjection>,
	public ISupportErrorInfo,
	public IDispatchImpl<ITWISTGeotiffProjection, &IID_ITWISTGeotiffProjection, &LIBID_TwistGisToolboxLib, 1, 0> {
public:
	
	/*! Find out whether the projection matches one of the "known geographic coordinate reference systems" defined in 
	    this library.   
		\param[in] refsys_id  The "known reference system" ID
		\param[out,retval] match  Whether the projection matches the "known reference system" ID
     */   
	STDMETHOD(matches_known_refsys)(TWISTKnownGeoRefsysId refsys_id, VARIANT_BOOL* match);

	// --- Other ---

	[[nodiscard]] static auto create(twist::gis::GeotiffProjection proj) -> CComPtr<ITWISTGeotiffProjection>;

	[[nodiscard]] auto projection_info() const -> const twist::gis::GeotiffProjection&;

	TWIST_COM_CLASS_DECLARATIONS(TWISTGeotiffProjection, ITWISTGeotiffProjection, IDR_TWIST_GEOTIFF_PROJECTION)

private:
	std::unique_ptr<twist::gis::GeotiffProjection> proj_;

	TWIST_CHECK_INVARIANT_DECL
};

OBJECT_ENTRY_NON_CREATEABLE_EX_AUTO(__uuidof(TWISTGeotiffProjection), TWISTGeotiffProjection)

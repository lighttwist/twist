/// @file TWISTRasterFileIOManager.h
/// TWISTRasterFileIOManager COM class

//  Copyright (c) 2007-2022, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#pragma once

#include "TwistGisToolbox/resource.h"       
#include "TwistGisToolbox/TwistGisToolbox_i.h"

using namespace ATL;

//! Manager for creating and opening geographic raster files.
class ATL_NO_VTABLE TWISTRasterFileIOManager :
	public CComObjectRootEx<CComMultiThreadModel>,
	public CComCoClass<TWISTRasterFileIOManager, &CLSID_TWISTRasterFileIOManager>,
	public ISupportErrorInfo,
	public IDispatchImpl<ITWISTRasterFileIOManager, &IID_ITWISTRasterFileIOManager, &LIBID_TwistGisToolboxLib, 1, 0> {
public:
	// --- ITWISTRasterFileIOManager ---

	/*! Create a new binary raster file following the "GeoTIFF" file format.
        \param[in] path  Path to the new TIFF file
        \param[in] space_grid  The spatial grid (with square cells) underlying the raster
        \param[in] data_type  The data type of a pixel/cell value
	    \param[in] nodata_value  Textual representation of the "no-data" placeholder value; pass in an empty string or 
		                         NULL if the grid does not use a "no-data" value
		\param[out,retval] file  Open connection to the file
	 */
	STDMETHOD(create_geotiff_file)(BSTR path, ITWISTSquareGeoSpaceGrid* space_grid, TWISTDataValueType data_type, 
								   BSTR nodata_value, ITWISTGeotiff** file);

	/*! Open an existing TIFF file.  
        \param[in] path  Path to the existing TIFF file
		\param[in] read_only  Whether the file will be open only for reading
		\param[out,retval] file  Open connection to the file
	 */
	STDMETHOD(open_geotiff_file)(BSTR path, VARIANT_BOOL read_only, ITWISTGeotiff** file);

	/*! Create a new binary raster file following the "ERDAT Imagine" (IMG) file format.
        \param[in] path  Path to the new IMG file
        \param[in] width  The width of the new IMG file in pixels/cells; in other words the number of columns in the 
		                  underying grid
        \param[in] height  The height of the new IMG file in pixels/cells; in other words the number of rows in the 
						   underying grid
        \param[in] data_type  The data type of a pixel/cell value
		\param[out,retval] file  Open connection to the file
     */
	STDMETHOD(create_erdas_imagine_file)(BSTR path, int width, int height, TWISTDataValueType data_type, 
	                                     ITWISTErdasImagineFile** file);

	/*! Open a connection to an existing binary raster file following the "ERDAT Imagine" (IMG) file format.
        \param[in] path  Path to the IMG file
		\param[out,retval] file  Open connection to the file
	 */
	STDMETHOD(open_erdas_imagine_file)(BSTR path, ITWISTErdasImagineFile** file);

	TWIST_COM_CLASS_DECLARATIONS(TWISTRasterFileIOManager, ITWISTRasterFileIOManager, IDR_TWISTRASTERFILEIOMANAGER)

private:
	TWIST_CHECK_INVARIANT_DECL
};

OBJECT_ENTRY_AUTO(__uuidof(TWISTRasterFileIOManager), TWISTRasterFileIOManager)

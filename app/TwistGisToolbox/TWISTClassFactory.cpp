/// @file TWISTClassFactory.cpp
/// Implementation file for "TWISTClassFactory.h"

//  Copyright (c) 2007-2022, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "TwistGisToolbox/pch.h"

#include "TwistGisToolbox/TWISTClassFactory.h"

#include "TwistGisToolbox/TWISTGeoCoordProjector.h"
#include "TwistGisToolbox/TWISTGeoLineString.h"
#include "TwistGisToolbox/TWISTGeoLinearRing.h"
#include "TwistGisToolbox/TWISTGeoPolygon.h"
#include "TwistGisToolbox/TWISTGeotiffProjection.h"
#include "TwistGisToolbox/TWISTGeoShapeCollection.h"
#include "TwistGisToolbox/TWISTSquareGeoSpaceGrid.h"

#include "twist/gis/geotiff_utils.hpp"
#include "twist/gis/twist_gdal_utils.hpp"

TWIST_COM_CLASS_IMPLEMENTATIONS(TWISTClassFactory, IID_ITWISTClassFactory)

using namespace twist;
using namespace twist::com;
using namespace twist::gis;

TWISTClassFactory::TWISTClassFactory()
{
}

TWISTClassFactory::~TWISTClassFactory()
{
}

STDMETHODIMP TWISTClassFactory::make_geo_space_grid(DOUBLE left, DOUBLE bottom, DOUBLE cell_size, INT nof_rows, 
                                                    INT nof_cols, ITWISTSquareGeoSpaceGrid** grid)
{
	COM_METHOD_IMPL_BEGIN
	auto icom = TWISTSquareGeoSpaceGrid::create(SquareGeoSpaceGrid{left, bottom, cell_size, nof_rows, nof_cols});
	icom.CopyTo(grid);
	COM_METHOD_IMPL_END
}

STDMETHODIMP TWISTClassFactory::make_geo_line_string(VARIANT point_coords, ITWISTGeoLineString** line_string)
{
	COM_METHOD_IMPL_BEGIN
	const auto coord_matrix = copy_from_2d_safearray<DOUBLE, double>(point_coords);
	auto icom = TWISTGeoLineString::create(to_points(coord_matrix));
	icom.CopyTo(line_string);
	COM_METHOD_IMPL_END
}

STDMETHODIMP TWISTClassFactory::make_geo_linear_ring(VARIANT point_coords, ITWISTGeoLinearRing** linear_ring)
{
	COM_METHOD_IMPL_BEGIN
	const auto coord_matrix = copy_from_2d_safearray<DOUBLE, double>(point_coords);
	auto icom = TWISTGeoLinearRing::create(to_points(coord_matrix));
	icom.CopyTo(linear_ring);
	COM_METHOD_IMPL_END
}

STDMETHODIMP TWISTClassFactory::make_geo_polygon(ITWISTGeoLinearRing* outer_ring, 
												 ITWISTGeoShapeCollection* inner_rings, ITWISTGeoPolygon** poly)
{
	COM_METHOD_IMPL_BEGIN
	auto out_ring = dyn_nonull_cast<TWISTGeoLinearRing*>(outer_ring)->linear_ring();
	auto in_rings = std::vector<GeoLinearRing>{};
	if (inner_rings) {
		const auto& in_shapes = dyn_nonull_cast<TWISTGeoShapeCollection*>(inner_rings)->collection();
		if (!in_shapes.empty()) {
			in_rings.reserve(in_shapes.size());
			for (const auto& shape : in_shapes) {
				auto in_ring = query_com_atl<ITWISTGeoLinearRing>(shape);
				in_rings.push_back(dyn_nonull_cast<TWISTGeoLinearRing*>(in_ring.p)->linear_ring());
			}
		}
	}
	auto icom = TWISTGeoPolygon::create(GeoPolygon{std::move(out_ring), move(in_rings)});
	icom.CopyTo(poly);
	COM_METHOD_IMPL_END
}

STDMETHODIMP TWISTClassFactory::make_geo_coord_projector_from_prj_file(BSTR path, ITWISTGeoCoordProjector** projector)
{
	COM_METHOD_IMPL_BEGIN
	auto [icom, com] = create_com_atl<ITWISTGeoCoordProjector, TWISTGeoCoordProjector>();
	com->init_from_prj_file(bstr_to_path(path));
	icom.CopyTo(projector);
	COM_METHOD_IMPL_END
}

STDMETHODIMP TWISTClassFactory::make_geotiff_projection_from_known_geo_refsys(TWISTKnownGeoRefsysId refsys_id, 
																	          ITWISTGeotiffProjection** projection)
{
	COM_METHOD_IMPL_BEGIN
	if (refsys_id == TWISTKnownGeoRefsysId::no_refsys) {
		TWIST_THRO2(L"'no_refsys' is not a valid value for the coordinate reference system parameter.");
	}
	auto projection_ptr = TWISTGeotiffProjection::create(
	                              geotiff_projection_from_known_geo_refsys(static_cast<KnownGeoRefsysId>(refsys_id)));
	projection_ptr.CopyTo(projection);
	COM_METHOD_IMPL_END
}

#ifdef _DEBUG
void TWISTClassFactory::check_invariant() const noexcept
{
}
#endif

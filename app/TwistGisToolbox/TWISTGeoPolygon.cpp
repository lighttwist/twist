/// @file TWISTGeoPolygon.cpp
/// Implementation file for "TWISTGeoPolygon.h"

//  Copyright (c) 2007-2022, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "TwistGisToolbox/pch.h"

#include "TwistGisToolbox/TWISTGeoPolygon.h"

#include "TwistGisToolbox/TWISTGeoLinearRing.h"
#include "TwistGisToolbox/TWISTGeoShapeCollection.h"

#include "twist/std_vector_utils.hpp"
#include "twist/gis/twist_gdal_utils.hpp"

TWIST_COM_CLASS_IMPLEMENTATIONS(TWISTGeoPolygon, IID_ITWISTGeoPolygon)

using namespace twist;
using namespace twist::com;
using namespace twist::gis;
using namespace twist::math;

TWISTGeoPolygon::TWISTGeoPolygon()
{
}

TWISTGeoPolygon::~TWISTGeoPolygon()
{
}

STDMETHODIMP TWISTGeoPolygon::outer_ring(ITWISTGeoLinearRing** out_ring)
{
	COM_METHOD_IMPL_BEGIN
	auto icom = TWISTGeoLinearRing::create(poly_->outer_ring().points());
	icom.CopyTo(out_ring);
	COM_METHOD_IMPL_END
}

STDMETHODIMP TWISTGeoPolygon::inner_rings(ITWISTGeoShapeCollection** inner_rings)
{
	COM_METHOD_IMPL_BEGIN
    const auto rings = poly_->inner_rings();
	auto shapes = reserve_vector<CComPtr<ITWISTGeoShape>>(ssize(rings));
	for (const auto& ring : rings) {
		auto out_ring = TWISTGeoLinearRing::create(ring.points());
		shapes.push_back(query_com_atl<ITWISTGeoShape>(out_ring));
	}
	auto shapes_ptr = TWISTGeoShapeCollection::create(move(shapes));
	shapes_ptr.CopyTo(inner_rings);
	COM_METHOD_IMPL_END
}

STDMETHODIMP TWISTGeoPolygon::is_holey(VARIANT_BOOL* is)
{
	COM_METHOD_IMPL_BEGIN
    *is = to_varbool(poly_->is_holey());
	COM_METHOD_IMPL_END
}

auto TWISTGeoPolygon::create(GeoPolygon poly) -> CComPtr<ITWISTGeoPolygon>
{
	auto [icom, com] = create_com_atl<ITWISTGeoPolygon, TWISTGeoPolygon>();
	com->poly_.emplace(std::move(poly));
	return icom;
}

auto TWISTGeoPolygon::polygon() const -> GeoPolygon
{
	TWIST_CHECK_INVARIANT
	return *poly_;
}

#ifdef _DEBUG
auto TWISTGeoPolygon::check_invariant() const noexcept -> void
{
	assert(poly_);
}
#endif

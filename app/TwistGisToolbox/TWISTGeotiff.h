/// @file TWISTGeotiff.h
/// TWISTGeotiff COM class definition

//  Copyright (c) 2007-2022, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#pragma once

#include "TwistGisToolbox/resource.h"       
#include "TwistGisToolbox/TwistGisToolbox_i.h"

namespace twist::img {
class Tiff;
}

using namespace ATL;

/*! A connection to a GeoTIFF file, ie a TIFF (Tagged Image File Format) file which contains metadata associating GIS
    specific information (eg geographic coordinates corresponding to the pixel grid) with the TIFF.
	\note  The connection to the file is guaranteed to be open until close() is called.
 */
class ATL_NO_VTABLE TWISTGeotiff :
	public CComObjectRootEx<CComMultiThreadModel>,
	public CComCoClass<TWISTGeotiff, &CLSID_TWISTGeotiff>,
	public ISupportErrorInfo,
	public IDispatchImpl<ITWISTGeotiff, &IID_ITWISTGeotiff, &LIBID_TwistGisToolboxLib, 1, 0> {
public:
	// --- ITWISTGeotiff ---

	/*! The TIFF image width (pixels).
	    \param[out,retval] val  The width
	 */
	STDMETHOD(width)(LONG* val);

	/*! The TIFF image height (pixels).
	    \param[out,retval] val  The height
	 */
	STDMETHOD(height)(LONG* val);

	/*! Read information about the underlying geographic data grid. 
        This function will only work if the data grid cells are square.
		If the TIFF does not contain geographic information, a COM error occurs.
	    \param[out] space_grid  The spatial grid underlying the data grid (in geographic coordinates)
	    \param[out] data_value_type  The type of a grid cell value
	    \param[out] nodata_value  Textual representation of the "no-data" placeholder value; pass in an empty string if 
	                              the grid does not use a "no-data" value
	*/
	STDMETHOD(get_data_grid_info)(ITWISTSquareGeoSpaceGrid** space_grid, TWISTDataValueType* data_value_type, 
	                              BSTR* nodata_value);

	/*! Set, in an image following the "scanline" format, the values of the pixels in the pixel grid, in a specific 
	    plane. If the image follows the "tiled" format, an exception is thrown.
	    \note  Writing to planes other than 0 in not implemented yet.
	    \param[in] pixel_values  Address of the buffer where the unsigned 16-bit integer pixel values are stored 
		                         contiguously, line after line. The size of the buffer must be width() * height(). 
	    \param[in] plane  The plane index; if a non-zero value is passed in, a COM error occurs
	 */
	STDMETHOD(write_uint16_scanlined)(UINT_PTR pixel_values, INT plane);

	/*! Read the geographic projection information (if any) stored in the GeoTIFF file.
		\param[out,retval]  The projection info, or nullptr if the TIFF contains no projection info
	 */
	STDMETHOD(read_projection)(ITWISTGeotiffProjection** proj);

	/*! Write geographic projection informatio to a GeoTIFF file. 
		If the GeoTIFF already contains projection information, this new information may be ignored.
		\param[in] proj  The projection info
	 */
	STDMETHOD(write_projection)(ITWISTGeotiffProjection* proj);

	/*! Close the connection to the file. 
	    \note  Calling any other COM method afterwards will result in a COM error.
	 */
	STDMETHOD(close)();

	// --- Other ---

	[[nodiscard]] static auto create(std::unique_ptr<twist::img::Tiff> file) -> CComPtr<ITWISTGeotiff>;

	TWIST_COM_CLASS_DECLARATIONS(TWISTGeotiff, ITWISTGeotiff, IDR_TWISTGEOTIFF)

private:
	std::unique_ptr<twist::img::Tiff> file_;

	TWIST_CHECK_INVARIANT_DECL
};

OBJECT_ENTRY_NON_CREATEABLE_EX_AUTO(__uuidof(TWISTGeotiff), TWISTGeotiff)

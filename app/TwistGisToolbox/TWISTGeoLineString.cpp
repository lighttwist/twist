// TWISTGeoLineString.cpp : Implementation of TWISTGeoLineString

#include "TwistGisToolbox/pch.h"

#include "TwistGisToolbox/TWISTGeoLineString.h"

#include "twist/std_vector_utils.hpp"

TWIST_COM_CLASS_IMPLEMENTATIONS(TWISTGeoLineString, IID_ITWISTGeoLineString)

using namespace twist;
using namespace twist::com;
using namespace twist::gis;
using namespace twist::math;

TWISTGeoLineString::TWISTGeoLineString()
{
}

TWISTGeoLineString::~TWISTGeoLineString()
{
}

STDMETHODIMP TWISTGeoLineString::nof_points(LONG* count)
{
	COM_METHOD_IMPL_BEGIN
	*count = static_cast<LONG>(line_string_->nof_points());
	COM_METHOD_IMPL_END
}

STDMETHODIMP TWISTGeoLineString::point(LONG idx, DOUBLE* x, DOUBLE* y)
{
	COM_METHOD_IMPL_BEGIN
	const auto point = line_string_->point(idx);
	*x = point.x();
	*y = point.y();
	COM_METHOD_IMPL_END
}

auto TWISTGeoLineString::create(std::vector<GeoPoint> points) -> CComPtr<ITWISTGeoLineString>
{
	auto [icom, com] = create_com_atl<ITWISTGeoLineString, TWISTGeoLineString>();
	com->line_string_.emplace(move(points));
	return icom;
}

auto TWISTGeoLineString::line_string() const -> twist::gis::GeoLineString
{
	TWIST_CHECK_INVARIANT
	return *line_string_;
}

#ifdef _DEBUG
auto TWISTGeoLineString::check_invariant() const noexcept -> void
{
	assert(line_string_);
}
#endif

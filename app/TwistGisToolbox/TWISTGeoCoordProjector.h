
#pragma once

#include "TwistGisToolbox/resource.h"       
#include "TwistGisToolbox/TwistGisToolbox_i.h"

namespace twist::gis {
class GeoCoordProjector;
}

using namespace ATL;

/*! Class which performs coordinate transformations between a given, "projected" coordinate system (based on X and Y 
    coordinates, and measured in metres) and the "geographic" coordinate system (based on longitude and latitude, and 
	measured in degrees) - currently "WGS84". 
 */
class ATL_NO_VTABLE TWISTGeoCoordProjector :
	public CComObjectRootEx<CComMultiThreadModel>,
	public CComCoClass<TWISTGeoCoordProjector, &CLSID_TWISTGeoCoordProjector>,
	public ISupportErrorInfo,
	public IDispatchImpl<ITWISTGeoCoordProjector, &IID_ITWISTGeoCoordProjector, &LIBID_TwistGisToolboxLib, 1, 0> {
public:
	// --- ITWISTGeoCoordProjector methods ---

	/*! Project a geographic point from decimal degree coordinates to metre coordinates.
		\param[in] point_degrees  The point (in decimal degree coordinates)
		\param[out,retval] point_metres  The projected point (in metre coordinates)
	 */
	STDMETHOD(degree_to_metre)(TWISTGeoPointDecDegree point_degrees, TWISTGeoPointMetre* point_metres);

	/*! Project a geographic point from metre coordinates to decimal degree coordinates.
		\param[in] point_metres  The point (in metre coordinates)
		\param[in] point_degrees  The projected point (in decimal degree coordinates)
	 */
	STDMETHOD(metre_to_degree)(TWISTGeoPointMetre point_metres, TWISTGeoPointDecDegree* point_degrees);

	// --- Other ---

	/*! Initialise the projector, basing the "projected" coordinate system on an Esri ".prj" file. 
		\param[in] path  The ".prj" file path
	 */
	auto init_from_prj_file(const fs::path& path) -> void;

	TWIST_COM_CLASS_DECLARATIONS(TWISTGeoCoordProjector, ITWISTGeoCoordProjector, IDR_TWISTGEOCOORDPROJECTOR)

private:
	std::unique_ptr<twist::gis::GeoCoordProjector> projector_;

	TWIST_CHECK_INVARIANT_DECL
};

OBJECT_ENTRY_NON_CREATEABLE_EX_AUTO(__uuidof(TWISTGeoCoordProjector), TWISTGeoCoordProjector)

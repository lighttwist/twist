/// @file TWISTGdalToolbox.h
/// TWISTGdalToolbox COM class

//  Copyright (c) 2007-2022, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#pragma once

#include "TwistGisToolbox/resource.h"       
#include "TwistGisToolbox/TwistGisToolbox_i.h"

using namespace ATL;

//! Utilities for working with the GDAL library (Geospatial Data Abstraction Library).
class ATL_NO_VTABLE TWISTGdalToolbox :
	public CComObjectRootEx<CComMultiThreadModel>,
	public CComCoClass<TWISTGdalToolbox, &CLSID_TWISTGdalToolbox>,
	public ISupportErrorInfo,
	public IDispatchImpl<ITWISTGdalToolbox, &IID_ITWISTGdalToolbox, &LIBID_TwistGisToolboxLib, 1, 0> {
public:
	// --- ITWISTGdalToolbox interface ---

	/*! Polygonise a raster, ie create a vector layer in which the raster cells which are connected (in the 
	    4-direction/rook's case connectivity sense) and have the same value are added to the same polygon. 
	    The function creates a shapefile storing the vector layer with an attributes table containing a field which 
		stores the source raster value corresponding to each polygon.
	    Note that currently the source pixel values are read into a signed 32bit integer buffer, so floating-point or 
		complex bands will be implicitly truncated before processing.	 
	    \param[in] in_path  The path of the input raster file
	    \param[in] out_path  The path of the output shapefile directory; if the directory exists and it is 
						     not empty, an exception is thrown
	    \param[in] out_field_name  The name of the shapefile attributes table field 
	    \param[in] out_field_type  The data type of the shapefile attributes table field 
	 */
	STDMETHOD(polygonise_raster)(BSTR in_path, BSTR out_path, BSTR out_field_name, 
	                             TWISTFeatureFieldType out_field_type);

	/*! Image reprojection and warping function.
		This is the equivalent of the gdalwarp utility.
		\note  Only a small subset of the functionality of the gdalwarp utility is implemented at the moment.
		\param[in] src_path  The path of the input raster file
		\param[in] out_path  The path of the output raster file; if the file already exists, an exception is thrown 
		\param[in] out_format  The output raster file format; make sure to also give the output path an extension 
		                       matching the desired format, otherwise an exception will be thrown
		\param[in] geo_space_grid  The spatial grid underlying the output raster; the input raster data will be clipped 
		                           and resampled based on this; if null, no re-rasterising takes place 
		\param[in] out_data_type  The data type of the output raster values; "data_type_none" means the data type stays
		                          unchanged
		\param[in] out_nodata_val  The "no-data" value placeholder in the destination image; if the source image 
		                           specifies a "no-data" value, then any pixel values matching the old value will be 
								   changed to the new value; pass in "None" to have no "no-data" specification in the 
								   destination image; blank or null means the "no-data" specification stays unchanged  
		\param[in] resample_algo  The algorithm to be used for resampling; must be a valid value
	 */
	STDMETHOD(warp_raster)(BSTR src_path, 
						   BSTR out_path,
						   TWISTRasterFileFormat out_format,
						   ITWISTSquareGeoSpaceGrid* geo_space_grid,
						   TWISTGeoGridDataType out_data_type,
						   BSTR out_nodata_val,
						   TWISTRasterResampleAlgorithm resample_algo);

	/*! Read data from a raster file, convert it to a differen format and/or perform some operations such as subsetting, 
		resampling and rescaling pixels, and save the converted data to a new raster file.
		This is the equivalent of the gdal_translate utility.	
		\note Only a subset of the functionality of the gdal_translate utility is implemented at the moment.
		\param[in] src_path  The path of the input raster file
		\param[in] target_dir_path  The path of the directory where the output raster file is stored; the name of the 
		                            output raster is the same as input raster. If the file already exists, an exception 
									is thrown
		\param[in] out_format  The output raster file format
		\param[in] geo_space_grid  The spatial grid underlying the output raster; the input raster data will be clipped 
		                           and resampled based on this; if null, no re-rasterising takes place
		\param[in] out_data_type  The data type of the output raster values; "data_type_none" means the data type stays
		                          unchanged
		\param[in] resample_algo  The algorithm to be used for resampling
	 */
	STDMETHOD(translate_raster)(BSTR src_path, 
							    BSTR target_dir_path,
							    TWISTRasterFileFormat out_format,
							    ITWISTSquareGeoSpaceGrid* geo_space_grid,
							    TWISTGeoGridDataType out_data_type,
							    TWISTRasterResampleAlgorithm resample_algo);
		                        //+TODO: Add compression paramater

	// --- Other ---

	TWIST_COM_CLASS_DECLARATIONS(TWISTGdalToolbox, ITWISTGdalToolbox, IDR_TWISTGDALTOOLBOX)

private:
	TWIST_CHECK_INVARIANT_DECL
};

OBJECT_ENTRY_AUTO(__uuidof(TWISTGdalToolbox), TWISTGdalToolbox)

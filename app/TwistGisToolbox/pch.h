// pch.h : include file for standard system include files,
// or project specific include files that are used frequently,
// but are changed infrequently

#pragma once

// C and C++ standard library essentials
#include <algorithm>
#include <array>
#include <cassert>
#include <cstdint>
#include <filesystem>
#include <functional>
#include <future>
#include <map>
#include <memory>
#include <optional>
#include <ranges>
#include <set>
#include <sstream>
#include <string_view>
#include <tuple>
#include <unordered_set>
#include <vector>

// Throw a gsl::fail_fast exception when pre/post conditions on the GSL types are violated
#define GSL_THROW_ON_CONTRACT_VIOLATION 
// The entire GSL ("Guidelines Support Library")
#include <gsl/gsl>

#ifndef STRICT
#define STRICT
#endif

#include "TwistGisToolbox/targetver.h"

#define NOMINMAX

// For the single-threaded apartament model:
//  (a) Replace "#define _ATL_FREE_THREADED" with "#define _ATL_APARTMENT_THREADED" below 
//  (b) Replace "val ThreadingModel = s 'Free'" with "val ThreadingModel = s 'Apartment'" in the .rgs files 
//  (c) Replace "CComMultiThreadModel" with "CComSingleThreadModel" in the COM class header files

#define _ATL_FREE_THREADED
#define _ATL_NO_AUTOMATIC_NAMESPACE
#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS // some CString constructors will be explicit
#define  ATL_NO_ASSERT_ON_DESTROY_NONEXISTENT_WINDOW

#include <atlbase.h>
#include <atlcom.h>
#include <atlctl.h>

// "twist" library essentials
#include "twist/cast.hpp"
#include "twist/globals.hpp"
#include "twist/IndexRange.hpp"
#include "twist/namespace_aliases.hpp"
#include "twist/RuntimeError.hpp"
#include "twist/string_utils.hpp"
#include "twist/com/atl_utils.hpp"
#include "twist/com/com_class_macros.hpp"
#include "twist/com/com_utils.hpp"
#include "twist/com/safearray_utils.hpp"
#include "twist/gis/gis_globals.hpp"

// "TwistGisToolbox" library essentials
#include "TwistGisToolbox/resource.h"
#include "TwistGisToolbox/globals.h"

/// @file TWISTGeotiffProjection.cpp
/// Implementation file for "TWISTGeotiffProjection.h"

//  Copyright (c) 2007-2022, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "TwistGisToolbox/pch.h"

#include "TwistGisToolbox/TWISTGeotiffProjection.h"

#include "twist/gis/geotiff_utils.hpp"

using namespace twist::com;
using namespace twist::gis;

TWIST_COM_CLASS_IMPLEMENTATIONS(TWISTGeotiffProjection, IID_ITWISTGeotiffProjection)

TWISTGeotiffProjection::TWISTGeotiffProjection()
{
}

TWISTGeotiffProjection::~TWISTGeotiffProjection()
{
}

STDMETHODIMP TWISTGeotiffProjection::matches_known_refsys(TWISTKnownGeoRefsysId refsys_id, VARIANT_BOOL* match)
{
	COM_METHOD_IMPL_BEGIN
	*match = to_varbool(twist::gis::match(*proj_, static_cast<KnownGeoRefsysId>(refsys_id)));
	COM_METHOD_IMPL_END
}

auto TWISTGeotiffProjection::create(GeotiffProjection proj) -> CComPtr<ITWISTGeotiffProjection>
{
	auto [icom, com] = create_com_atl<ITWISTGeotiffProjection, TWISTGeotiffProjection>();
	com->proj_ = std::make_unique<GeotiffProjection>(std::move(proj));
	return icom;
}

auto TWISTGeotiffProjection::projection_info() const -> const twist::gis::GeotiffProjection&
{
	TWIST_CHECK_INVARIANT
	return *proj_;
}

#ifdef _DEBUG
void TWISTGeotiffProjection::check_invariant() const noexcept
{
	assert(proj_);
}
#endif


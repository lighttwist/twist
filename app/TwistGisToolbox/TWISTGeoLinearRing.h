/// @file TWISTGeoLinearRing.h
/// TWISTGeoLinearRing COM class definition

//  Copyright (c) 2007-2022, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#pragma once

#include "TwistGisToolbox/resource.h" 
#include "TwistGisToolbox/TwistGisToolbox_i.h"

#include "twist/gis/gis_geometry.hpp"

using namespace ATL;

/*! A "linear ring" (a line connecting a series of points on a map, with the last point being the same as the first) in
    any coordinates. 
 */
class ATL_NO_VTABLE TWISTGeoLinearRing :
	public CComObjectRootEx<CComMultiThreadModel>,
	public CComCoClass<TWISTGeoLinearRing, &CLSID_TWISTGeoLinearRing>,
	public ISupportErrorInfo,
	public IDispatchImpl<ITWISTGeoLinearRing, &IID_ITWISTGeoLinearRing, &LIBID_TwistGisToolboxLib, 1, 0> {
public:
	// --- ITWISTGeoLineString ---

	//! The number \p count of points connected by the linear ring.
	STDMETHOD(nof_points)(LONG* count);

	/*! Get the coordinates \p x and \p y of the point with index \p idx in the list of points connected by the 
	    linear ring.
	 */
	STDMETHOD(point)(LONG idx, DOUBLE* x, DOUBLE* y);

	// --- Other ---

	[[nodiscard]] static auto create(std::vector<twist::gis::GeoPoint> points) -> CComPtr<ITWISTGeoLinearRing>;

	[[nodiscard]] auto linear_ring() const -> twist::gis::GeoLinearRing;

	TWIST_COM_CLASS_DECLARATIONS_NO_MAP(TWISTGeoLinearRing, ITWISTGeoLinearRing, IDR_TWISTGEOLINEARRING)

	BEGIN_COM_MAP(TWISTGeoLinearRing)
		COM_INTERFACE_ENTRY(ITWISTGeoLinearRing)
		COM_INTERFACE_ENTRY(ITWISTGeoLineString)
		COM_INTERFACE_ENTRY(ITWISTGeoShape)
		COM_INTERFACE_ENTRY(IDispatch)
		COM_INTERFACE_ENTRY(ISupportErrorInfo)
	END_COM_MAP()

private:
	std::optional<twist::gis::GeoLinearRing> linear_ring_;

	TWIST_CHECK_INVARIANT_DECL
};

OBJECT_ENTRY_NON_CREATEABLE_EX_AUTO(__uuidof(TWISTGeoLinearRing), TWISTGeoLinearRing)

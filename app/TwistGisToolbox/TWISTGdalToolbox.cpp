/// @file TWISTGdalToolbox.Cpp
/// Implementation file for "TWISTGdalToolbox.h"

//  Copyright (c) 2007-2022, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "TwistGisToolbox/pch.h"

#include "TwistGisToolbox/TWISTGdalToolbox.h"

#include "TwistGisToolbox/TWISTSquareGeoSpaceGrid.h"

#include "twist/gis/twist_gdal_utils.hpp"

TWIST_COM_CLASS_IMPLEMENTATIONS(TWISTGdalToolbox, IID_ITWISTGdalToolbox)

using namespace twist::com;
using namespace twist::gis;

TWISTGdalToolbox::TWISTGdalToolbox()
{
}

TWISTGdalToolbox::~TWISTGdalToolbox()
{
}

STDMETHODIMP TWISTGdalToolbox::polygonise_raster(BSTR in_path, BSTR out_path, BSTR out_field_name, 
		TWISTFeatureFieldType out_field_type)
{
	COM_METHOD_IMPL_BEGIN
	twist::gis::polygonise_raster(bstr_to_path(in_path), bstr_to_path(out_path), 
			{bstr_to_ansi(out_field_name), static_cast<FeatureFieldType>(out_field_type)});
	COM_METHOD_IMPL_END
}

STDMETHODIMP TWISTGdalToolbox::warp_raster(BSTR src_path, 
										   BSTR out_path,
										   TWISTRasterFileFormat out_format,
										   ITWISTSquareGeoSpaceGrid* geo_space_grid,
										   TWISTGeoGridDataType out_data_type,
										   BSTR nodata_val,
										   TWISTRasterResampleAlgorithm resample_algo)
{
	COM_METHOD_IMPL_BEGIN
	const auto dest_path_warp = bstr_to_path(out_path);
	create_directories(dest_path_warp.parent_path());

	twist::gis::warp_raster(src_path, 
							dest_path_warp, 
							RasterFileFormat{out_format}, 
							bstr_to_ansi(nodata_val),
							dynamic_cast<TWISTSquareGeoSpaceGrid*>(geo_space_grid)->grid(),
							out_data_type != TWISTGeoGridDataType::data_type_none 
									? std::optional{GeoGridDataType{out_data_type}}
									: std::nullopt,
							RasterResampleAlgorithm{resample_algo});

	// GDAL's warp() may create a temporary file
	const auto dest_path_warp_temp = fs::path{dest_path_warp.wstring() + L".aux.xml"};
	remove(dest_path_warp_temp);

	COM_METHOD_IMPL_END
}

STDMETHODIMP TWISTGdalToolbox::translate_raster(BSTR src_path, 
												BSTR target_dir_path,
												TWISTRasterFileFormat out_format,
												ITWISTSquareGeoSpaceGrid* geo_space_grid,
												TWISTGeoGridDataType out_data_type,
												TWISTRasterResampleAlgorithm resample_algo)
{
	COM_METHOD_IMPL_BEGIN
	create_directories(bstr_to_path(target_dir_path));

	const auto dest_path_translate = target_dir_path / bstr_to_path(src_path).filename();

	twist::gis::translate_raster(src_path, 
								 dest_path_translate, 
								 RasterFileFormat{out_format}, 
								 dynamic_cast<TWISTSquareGeoSpaceGrid*>(geo_space_grid)->grid(),
								 out_data_type != TWISTGeoGridDataType::data_type_none 
										? std::optional{GeoGridDataType{out_data_type}}
										: std::nullopt,
								 RasterResampleAlgorithm{resample_algo});

	COM_METHOD_IMPL_END
}

#ifdef _DEBUG
void TWISTGdalToolbox::check_invariant() const noexcept
{
}
#endif

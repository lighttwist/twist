/// @file TWISTShapefile.h
/// TWISTShapefile COM class definition

//  Copyright (c) 2007-2022, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#pragma once

#include "TwistGisToolbox/resource.h"       
#include "TwistGisToolbox/TwistGisToolbox_i.h"

namespace twist::gis {
class Shapefile;
}

using namespace ATL;

/*! Class which represents an open connection to an ESRI ShapeFile, a geospatial vector data format. 
    An ESRI shapefile consists of (at least) three files, stored in the same directory: the main shape file (.shp), 
	the shapefile index file (.shx) and a DBF file (.dbf).
 */
class ATL_NO_VTABLE TWISTShapefile :
	public CComObjectRootEx<CComMultiThreadModel>,
	public CComCoClass<TWISTShapefile, &CLSID_TWISTShapefile>,
	public ISupportErrorInfo,
	public IDispatchImpl<ITWISTShapefile, &IID_ITWISTShapefile, &LIBID_TwistGisToolboxLib, 1, 0> {
public:
	// --- ITWISTShapefile ---

	//! Add the "point" feature \p point to the active layer and get its feature ID in \p feature_id. 
	STDMETHOD(add_point_feature)(TWISTGeoPoint point, LONG* feature_id);

	//! Add the "string line" feature \p line_string to the active layer and get its feature ID in \p feature_id. 
	STDMETHOD(add_line_string_feature)(ITWISTGeoLineString* line_string, LONG* feature_id);

	//! Add the "polygon" feature \p polygon to the active layer and get its feature ID in \p feature_id. 
	STDMETHOD(add_polygon_feature)(ITWISTGeoPolygon* polygon, LONG* feature_id);

	/*! Add a new column/field to the attribute table. The values in the new column for any existing features will 
	    be NULL.
		\note Only the field types "int32" and "string" are supported at the moment.
	    \param[in] field_name  The field name; be aware that the current shapefile specification imposes a maximum 
		                       field name size of 10 charaters
	    \param[in] field_type  The field data type
	 */
	STDMETHOD(add_attribute_column)(BSTR field_name, TWISTFeatureFieldType field_type);

	/*! Get the number of features in the active layer.
	    \param[in] force  Whether the count should be computed even if it is expensive
	    \param[out,retval] count  Feature count, or -1 if the count is not known 
     */
	STDMETHOD(nof_features)(VARIANT_BOOL force, LONG* count);

	/*! Read the feature with a specific feature ID, from the active layer, as a polygon. 
	    The layer is expected to be a polygon layer. 
	    \param[in] feature_id  The feature ID; if not found in the active layer, an exception is thrown
	    \param[out,retval] polygon  The polygon
	 */
	STDMETHOD(read_feature_as_polygon)(LONG feature_id, ITWISTGeoPolygon** polygon);

	/*! Read all the features in the active layer as polygons. 
	    The layer is expected to be a polygon layer. 
        \param[in] multipolygon_option  What to do when encountering a multi-polygon feature
		\param[out,retval] polygons  The list of polygons; all elements in the list are ITWISTPolygon
	 */
	STDMETHOD(read_all_features_as_polygons)(TWISTMultiPolygonOption multipolygon_option, 
                                             ITWISTGeoShapeCollection** polygons);

	/*! Read all the features from the active layer as polygons, toghether with the values of one string attribute 
        field. The layer is expected to be a polygon layer. 
		\param[in] col_name  The name of the string attribute column
        \param[in] multipolygon_option  What to do when encountering a multi-polygon feature
		\param[out] polygons  The list of polygons; all elements in the list are ITWISTPolygon
		\param[out] attr_values  The list of attribute values; all elements in the list are BSTR and match the elements
                                 in the polygons list
	 */
	STDMETHOD(read_all_features_as_polygons_with_string_attribute)(BSTR col_name, 
                                                                   TWISTMultiPolygonOption multipolygon_option,
                                                                   ITWISTGeoShapeCollection** polygons,
                                                                   SAFEARRAY** attr_values);

	/*! Set the value in the \p col_name column of the attribute table, for the feature with ID \p feature_id, to 
	    \p value. The field data type should be compatible with "int32". The feature is looked for in the active layer.
	 */
	STDMETHOD(set_feature_attribute_int32)(LONG feature_id, BSTR col_name, INT value);

	/*! Set the value in the \p col_name column of the attribute table, for the feature with ID \p feature_id, to 
	    \p value. The field data type should be compatible with "string". The feature is looked for in the active 
		layer.
	 */
	STDMETHOD(set_feature_attribute_string)(LONG feature_id, BSTR col_name, BSTR value);

	/* Set the projection used by the shapefile to the "known reference system" with ID \p refsys_id. 
	   If a projection is already specified for the shapefile, it is overwritten.
	 */
	STDMETHOD(set_known_projection)(TWISTKnownGeoRefsysId refsys_id);

	//! Save to file all pending changes to the active layer.
	STDMETHOD(save)();

	/*! Close the connection to the file. 
	    \note  Calling any other COM method afterwards will result in a COM error.
	 */
	STDMETHOD(close)();

	// --- Other ---

	[[nodiscard]] static auto create(std::unique_ptr<twist::gis::Shapefile> shapefile) -> CComPtr<ITWISTShapefile>;

	TWIST_COM_CLASS_DECLARATIONS(TWISTShapefile, ITWISTShapefile, IDR_TWISTSHAPEFILE)

private:
	std::unique_ptr<twist::gis::Shapefile> shapefile_;
	bool is_closed_{false};

	TWIST_CHECK_INVARIANT_DECL
};

OBJECT_ENTRY_NON_CREATEABLE_EX_AUTO(__uuidof(TWISTShapefile), TWISTShapefile)

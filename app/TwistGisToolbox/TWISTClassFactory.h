/// @file TWISTClassFactory.h
/// TWISTClassFactory COM class definition

//  Copyright (c) 2007-2022, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#pragma once

#include "TwistGisToolbox/resource.h"
#include "TwistGisToolbox/TwistGisToolbox_i.h"

using namespace ATL;

/*! Class responsible for creating instances of other COM classes in the library (to avoid two-step construction of 
    instances of those classes from outside).
 */
class ATL_NO_VTABLE TWISTClassFactory :
	public CComObjectRootEx<CComMultiThreadModel>,
	public CComCoClass<TWISTClassFactory, &CLSID_TWISTClassFactory>,
	public ISupportErrorInfo,
	public IDispatchImpl<ITWISTClassFactory, &IID_ITWISTClassFactory, &LIBID_TwistGisToolboxLib, 1, 0> {
public:
	// --- ITWISTClassFactory methods ---

	/*! Create an instance of TWISTSquareGeoSpaceGrid.
	    \param[in] left  The left coordinate of the grid perimeter
	    \param[in] bottom  The bottom coordinate of the grid perimeter
	    \param[in] cell_size  The cell size
	    \param[in] nof_rows  The number of rows
	    \param[in] nof_cols  The number of columns
		\param[out,retval] grid  The "space grid" instance
     */
	STDMETHOD(make_geo_space_grid)(DOUBLE left, DOUBLE bottom, DOUBLE cell_size, INT nof_rows, INT nof_cols, 
	                               ITWISTSquareGeoSpaceGrid** grid);

	/*! Create an instance of TWISTGeoLineString.
	    \param[in] point_coords  Coordinates of the points connected by the line string; if there are less than two 
		                         points in the list, an exception is thrown. The argument type is a 2-dimensional 
								 SAFEARRAY with DOUBLE elements, such that element [n,0] is the x coordinate of the 
								 n-th point, and [n,1] is the y coordinate.
	    \param[out,retval] line_string  The "line string" instance
	 */
	STDMETHOD(make_geo_line_string)(VARIANT point_coords, ITWISTGeoLineString** line_string);

	/*! Create an instance of TWISTGeoLinarRing.
	    \param[in] point_coords  Coordinates of the points connected by the linear ring; if the last point does not 
		                         match the first, or if there are less than three points in the list, an exception is 
								 thrown. The argument type is a 2-dimensional SAFEARRAY with DOUBLE elements, such that
								 element [n,0] is the x coordinate of the n-th point, and [n,1] is the y coordinate.
	    \param[out,retval] linear_ring  The "linear ring" instance
	 */
	STDMETHOD(make_geo_linear_ring)(VARIANT point_coords, ITWISTGeoLinearRing** linear_ring);

	/*! Create an instance of TWISTGeoPolygon.
		\param[in] outer_ring  The outer ring 
		\param[in] inner_rings  The inner rings, which define the holes in the polygon; pass in NULL or an empty 
		                        collection to create a "simple" polygon (ie a polygon without holes). For a "holey" 
								polygon, the elements the collection must be ITWISTGeoLinearRing interfaces.
	    \param[out,retval] poly  The "polygon" instance
	 */
	STDMETHOD(make_geo_polygon)(ITWISTGeoLinearRing* outer_ring, ITWISTGeoShapeCollection* inner_rings, 
	                            ITWISTGeoPolygon** poly);

	/*! Create an instance of TWISTGeoCoordProjector whose "projected" coordinate system is based on an Esri 
	    ".prj" file. 
	    \param[in] path  The ".prj" file path
		\param[out,retval]  The projector instance
	 */
	STDMETHOD(make_geo_coord_projector_from_prj_file)(BSTR path, ITWISTGeoCoordProjector** projector);

	/*! Create ageotiff projection object matching a known geospatial coordinate system.
		\param[in] refsys_id  The known reference system ID
		\param[out,retval] projection  The projection info object
	 */
	STDMETHOD(make_geotiff_projection_from_known_geo_refsys)(TWISTKnownGeoRefsysId refsys_id, 
	                                                         ITWISTGeotiffProjection** projection);

	// --- Other ---

	TWIST_COM_CLASS_DECLARATIONS(TWISTClassFactory, ITWISTClassFactory, IDR_TWISTCLASSFACTORY)

private:
	TWIST_CHECK_INVARIANT_DECL
};

OBJECT_ENTRY_AUTO(__uuidof(TWISTClassFactory), TWISTClassFactory)

/// @file TWISTShapefile.cpp
/// Implementation file for "TWISTShapefile.h"

//  Copyright (c) 2007-2022, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "TwistGisToolbox/pch.h"

#include "TwistGisToolbox/TWISTShapefile.h"

#include "TwistGisToolbox/TWISTGeoLineString.h"
#include "TwistGisToolbox/TWISTGeoPolygon.h"
#include "TwistGisToolbox/TWISTGeoShapeCollection.h"

#include "twist/std_vector_utils.hpp"
#include "twist/gis/Shapefile.hpp"

TWIST_COM_CLASS_IMPLEMENTATIONS(TWISTShapefile, IID_ITWISTShapefile)

#define ENSURE_OPEN if (is_closed_) { TWIST_THROW(L"The file connection has been closed."); }

using namespace twist;
using namespace twist::com;
using namespace twist::gis;

TWISTShapefile::TWISTShapefile()
{
}

TWISTShapefile::~TWISTShapefile()
{
}

STDMETHODIMP TWISTShapefile::add_point_feature(TWISTGeoPoint point, LONG* feature_id)
{
	COM_METHOD_IMPL_BEGIN
	ENSURE_OPEN
	*feature_id = static_cast<LONG>(shapefile_->add_feature(GeoPoint{point.x, point.y}));
	COM_METHOD_IMPL_END
}

STDMETHODIMP TWISTShapefile::add_line_string_feature(ITWISTGeoLineString* line_string, LONG* feature_id)
{
	COM_METHOD_IMPL_BEGIN
	ENSURE_OPEN
	*feature_id = static_cast<LONG>(shapefile_->add_feature(
												dyn_nonull_cast<TWISTGeoLineString*>(line_string)->line_string()));
	COM_METHOD_IMPL_END
}

STDMETHODIMP TWISTShapefile::add_polygon_feature(ITWISTGeoPolygon* polygon, LONG* feature_id)
{
	COM_METHOD_IMPL_BEGIN
	ENSURE_OPEN
	*feature_id = static_cast<LONG>(shapefile_->add_feature(dyn_nonull_cast<TWISTGeoPolygon*>(polygon)->polygon()));
	COM_METHOD_IMPL_END
}

STDMETHODIMP TWISTShapefile::add_attribute_column(BSTR field_name, TWISTFeatureFieldType field_type)
{
	COM_METHOD_IMPL_BEGIN
	ENSURE_OPEN
	shapefile_->add_attribute_column(FeatureFieldInfo{bstr_to_ansi(field_name), FeatureFieldType{field_type}});
	COM_METHOD_IMPL_END
}

STDMETHODIMP TWISTShapefile::nof_features(VARIANT_BOOL force, LONG* count)
{
	COM_METHOD_IMPL_BEGIN
	ENSURE_OPEN
	const auto opt_count = shapefile_->nof_features(varbool_to_bool(force));
	*count = opt_count ? static_cast<LONG>(*opt_count) : -1;
	COM_METHOD_IMPL_END
}

STDMETHODIMP TWISTShapefile::read_feature_as_polygon(LONG feature_id, ITWISTGeoPolygon** polygon)
{
	COM_METHOD_IMPL_BEGIN
	ENSURE_OPEN
	auto poly= shapefile_->read_feature_as_polygon(feature_id);
	auto polygon_ptr = TWISTGeoPolygon::create(std::move(poly));
	polygon_ptr.CopyTo(polygon);
	COM_METHOD_IMPL_END
}

STDMETHODIMP TWISTShapefile::read_all_features_as_polygons(TWISTMultiPolygonOption multipolygon_option,
                                                           ITWISTGeoShapeCollection** polygons)
{
	COM_METHOD_IMPL_BEGIN
	ENSURE_OPEN
    if (!is_valid(static_cast<MultiPolygonOption>(multipolygon_option))) {
        TWIST_THRO2(L"Invalid TWISTMultiPolygonOption value {}.", static_cast<int>(multipolygon_option));
    }

	auto polys = shapefile_->read_all_features_as_polygons(static_cast<MultiPolygonOption>(multipolygon_option));
	auto shapes = reserve_vector<CComPtr<ITWISTGeoShape>>(polys.size());
	for (auto& poly : polys) {
		auto out_poly = TWISTGeoPolygon::create(std::move(poly));
		shapes.push_back(query_com_atl<ITWISTGeoShape>(out_poly));
	}
	auto shapes_ptr = TWISTGeoShapeCollection::create(move(shapes));
	shapes_ptr.CopyTo(polygons);
	COM_METHOD_IMPL_END
}

STDMETHODIMP TWISTShapefile::read_all_features_as_polygons_with_string_attribute(
                     BSTR col_name,
                     TWISTMultiPolygonOption multipolygon_option,
                     ITWISTGeoShapeCollection** polygons,
                     SAFEARRAY** attr_values)
{
	COM_METHOD_IMPL_BEGIN
	ENSURE_OPEN
    if (!is_valid(static_cast<MultiPolygonOption>(multipolygon_option))) {
        TWIST_THRO2(L"Invalid TWISTMultiPolygonOption value {}.", static_cast<int>(multipolygon_option));
    }

	auto polys_and_attr_vals = shapefile_->read_all_features_as_polygons_with_attribute<std::string>(
                                       bstr_to_ansi(col_name), static_cast<MultiPolygonOption>(multipolygon_option));

	auto shapes = reserve_vector<CComPtr<ITWISTGeoShape>>(ssize(polys_and_attr_vals));
	auto attr_vals = reserve_vector<std::string>(ssize(polys_and_attr_vals));
	for (auto& [poly, attr_val] : polys_and_attr_vals) {
		auto out_poly = TWISTGeoPolygon::create(std::move(poly));
		shapes.push_back(query_com_atl<ITWISTGeoShape>(out_poly));
        attr_vals.push_back(move(attr_val));
	}

	auto shapes_ptr = TWISTGeoShapeCollection::create(move(shapes));
	shapes_ptr.CopyTo(polygons);
    *attr_values = transform_to_1d_rawsafearray<BSTR, VT_BSTR>(attr_vals, ansi_to_bstr);
	COM_METHOD_IMPL_END
}

STDMETHODIMP TWISTShapefile::set_feature_attribute_int32(LONG feature_id, BSTR col_name, INT value)
{
	COM_METHOD_IMPL_BEGIN
	ENSURE_OPEN
	shapefile_->set_feature_attribute_value(feature_id, bstr_to_ansi(col_name), Shapefile::AttributeValue{value});
	COM_METHOD_IMPL_END
}

STDMETHODIMP TWISTShapefile::set_feature_attribute_string(LONG feature_id, BSTR col_name, BSTR value)
{
	COM_METHOD_IMPL_BEGIN
	ENSURE_OPEN
	shapefile_->set_feature_attribute_value(feature_id, bstr_to_ansi(col_name), 
	                                        Shapefile::AttributeValue{bstr_to_ansi(value)});
	COM_METHOD_IMPL_END
}

STDMETHODIMP TWISTShapefile::set_known_projection(TWISTKnownGeoRefsysId refsys_id)
{
	COM_METHOD_IMPL_BEGIN
	ENSURE_OPEN
	shapefile_->set_known_projection(static_cast<KnownGeoRefsysId>(refsys_id));
	COM_METHOD_IMPL_END
}

STDMETHODIMP TWISTShapefile::save()
{
	COM_METHOD_IMPL_BEGIN
	ENSURE_OPEN
	shapefile_->save();
	COM_METHOD_IMPL_END
}

STDMETHODIMP TWISTShapefile::close()
{
	COM_METHOD_IMPL_BEGIN
	ENSURE_OPEN
	shapefile_.reset();
	is_closed_ = true;
	COM_METHOD_IMPL_END
}

auto TWISTShapefile::create(std::unique_ptr<twist::gis::Shapefile> shapefile) -> CComPtr<ITWISTShapefile>
{
	auto [icom, com] = create_com_atl<ITWISTShapefile, TWISTShapefile>();
	com->shapefile_ = move(shapefile);
	return icom;
}

#ifdef _DEBUG
void TWISTShapefile::check_invariant() const noexcept
{
	assert((shapefile_ == nullptr) == is_closed_);
}
#endif


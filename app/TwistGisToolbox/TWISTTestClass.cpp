
#include "TwistGisToolbox/pch.h"

#include "TwistGisToolbox/TWISTTestClass.h"

#include "twist/gis/twist_gdal_utils.hpp"

TWIST_COM_CLASS_IMPLEMENTATIONS(TWISTTestClass, IID_ITWISTTestClass)

using namespace twist::com;
using namespace twist::gis;

TWISTTestClass::TWISTTestClass()
{
}

TWISTTestClass::~TWISTTestClass()
{
}

STDMETHODIMP TWISTTestClass::method1(UINT_PTR shared_mem_addr, INT row_count, INT col_count)
{
	COM_METHOD_IMPL_BEGIN
	auto buffer = reinterpret_cast<const float*>(shared_mem_addr);
	for (auto i = 0; i < row_count; ++i) {
		for (auto j = 0; j < col_count; ++j) {
			auto val = buffer[i * col_count + j];
			val = val;
		}	
	}
	COM_METHOD_IMPL_END
}

#ifdef _DEBUG
void TWISTTestClass::check_invariant() const noexcept
{
}
#endif


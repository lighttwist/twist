/// @file TWISTErdasImagineFile.h
/// TWISTErdasImagineFile COM class definition

//  Copyright (c) 2007-2022, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#pragma once

#include "TwistGisToolbox/resource.h"       
#include "TwistGisToolbox/TwistGisToolbox_i.h"

using namespace ATL;

namespace twist::gis {
class ErdasImagineFile;
}

class ATL_NO_VTABLE TWISTErdasImagineFile :
	public CComObjectRootEx<CComMultiThreadModel>,
	public CComCoClass<TWISTErdasImagineFile, &CLSID_TWISTErdasImagineFile>,
	public ISupportErrorInfo,
	public IDispatchImpl<ITWISTErdasImagineFile, &IID_ITWISTErdasImagineFile, &LIBID_TwistGisToolboxLib, 1, 0> {
public:
	// --- ITWISTErdasImagineFile ---

	STDMETHOD(width)(INT* thewidth);
	
	STDMETHOD(height)(INT* theheight);

	/*! Get the spatial grid underlying the IMG file raster.
	    If the IMG file does not contain geographic info, a COM error occurs.
	    \param grid  [out,retval] The grid
	*/
	STDMETHOD(space_grid)(ITWISTSquareGeoSpaceGrid** grid);	

	STDMETHOD(data_type)(TWISTDataValueType* datatype);

	STDMETHOD(read_uint8)(VARIANT_BOOL* has_nodata_value, INT* nodata_value, VARIANT* data_grid);

	STDMETHOD(read_int16)(VARIANT_BOOL* has_nodata_value, INT* nodata_value, VARIANT* data_grid);

	STDMETHOD(read_uint16)(VARIANT_BOOL* has_nodata_value, INT* nodata_value, VARIANT* data_grid);

	STDMETHOD(read_int32)(VARIANT_BOOL* has_nodata_value, INT* nodata_value, VARIANT* data_grid);

	STDMETHOD(write_uint8)(VARIANT data_grid);

	STDMETHOD(write_uint8_with_nodata_val)(VARIANT data_grid, unsigned char nodata_value);

	STDMETHOD(write_uint16)(VARIANT data_grid);

	STDMETHOD(write_uint16_with_nodata_val)(VARIANT data_grid, unsigned short nodata_value);

	STDMETHOD(write_int32)(VARIANT data_grid);

	STDMETHOD(write_int32_with_nodata_val)(VARIANT data_grid, INT nodata_value);

	/*! Close the connection to the file. 
	    \note  Calling any other COM method afterwards will result in a COM error.
	 */
	STDMETHOD(close)();
	 
	// --- Other ---

	[[nodiscard]] static auto create_file(const fs::path& path, int width, int height, 
	                                      twist::gis::GeoGridDataType data_type) -> CComPtr<ITWISTErdasImagineFile>;
	
	[[nodiscard]] static auto open_file(const fs::path& path) -> CComPtr<ITWISTErdasImagineFile>;

	TWIST_COM_CLASS_DECLARATIONS(TWISTErdasImagineFile, ITWISTErdasImagineFile, IDR_TWISTERDASIMAGINEFILE)

private:
	friend class TWISTRasterFileIOManager;

	template<class DataVal>
	void read_impl(VARTYPE vartype, VARIANT* data_grid, VARIANT_BOOL* has_nodata_value, INT* nodata_value);

	std::unique_ptr<twist::gis::ErdasImagineFile> file_;
	bool is_closed_{false};

	TWIST_CHECK_INVARIANT_DECL
};

OBJECT_ENTRY_NON_CREATEABLE_EX_AUTO(__uuidof(TWISTErdasImagineFile), TWISTErdasImagineFile)

///  @file  TWISTGisToolboxLibInfo.cpp
///  Implementation file for "TWISTGisToolboxLibInfo.h"

//   Copyright (c) 2010-2019 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "TwistGisToolbox/pch.h"

#include "TwistGisToolbox/TWISTGisToolboxLibInfo.h"

#include "twist/FileVersionInfo.hpp"
#include "twist/os_utils.hpp"
#include "twist/gis/twist_gdal_utils.hpp"

TWIST_COM_CLASS_IMPLEMENTATIONS(TWISTGisToolboxLibInfo, IID_ITWISTGisToolboxLibInfo)

using namespace twist;
using namespace twist::com;
using namespace twist::gis;

TWISTGisToolboxLibInfo::TWISTGisToolboxLibInfo()
{
}

TWISTGisToolboxLibInfo::~TWISTGisToolboxLibInfo()
{
}

STDMETHODIMP TWISTGisToolboxLibInfo::get_dll_path(BSTR* path)
{
	COM_METHOD_IMPL_BEGIN
	*path = path_to_bstr(get_running_module_path());
	COM_METHOD_IMPL_END
}

STDMETHODIMP TWISTGisToolboxLibInfo::get_dll_version(LONG* major_ver_no, LONG* minor_ver_no, LONG* release_no, 
		LONG* build_no)
{
	COM_METHOD_IMPL_BEGIN
	const auto version_info = get_file_version_info(get_running_module_path());
	if (!version_info) {
		TWIST_THROW(L"Unable to retrieve DLL version info.");
	}
		
	*major_ver_no = version_info->major_ver_no();
	*minor_ver_no = version_info->minor_ver_no();
	*release_no   = version_info->release_no();
	*build_no     = version_info->build_no();
	COM_METHOD_IMPL_END
}

STDMETHODIMP TWISTGisToolboxLibInfo::get_dll_version_string(BSTR* version_str)
{
	COM_METHOD_IMPL_BEGIN
	const auto version_info = get_file_version_info(get_running_module_path());
	if (!version_info) {
		TWIST_THROW(L"Unable to retrieve DLL version info.");
	}
	*version_str = string_to_bstr(to_str(*version_info));
	COM_METHOD_IMPL_END
}

#ifdef _DEBUG
void TWISTGisToolboxLibInfo::check_invariant() const noexcept
{
}
#endif 



#include "TwistGisToolbox/pch.h"

#include "TwistGisToolbox/globals.h"

#include "twist/std_vector_utils.hpp"

using namespace twist;
using namespace twist::gis;
using namespace twist::math;

auto to_geogrid_data_type(VARTYPE variant_type) -> GeoGridDataType
{
	switch (variant_type) {		
	case VT_I1:  return GeoGridDataType::int8;
	case VT_UI1: return GeoGridDataType::uint8; 
	case VT_I2:  return GeoGridDataType::int16; 
	case VT_UI2: return GeoGridDataType::uint16; 
	case VT_I4:  return GeoGridDataType::int32;
	case VT_UI4: return GeoGridDataType::uint32;
	case VT_R4:  return GeoGridDataType::float32;
	case VT_R8:  return GeoGridDataType::float64;
	default: TWIST_THROW(L"The variant type (value %d) cannot be converted to a \"geo-grid data value type\".", 
			             variant_type);
	}
}

auto to_points(const FlatMatrix<double>& point_coords) -> std::vector<GeoPoint>
{
	const auto nof_points = point_coords.nof_rows();
	auto ret = reserve_vector<GeoPoint>(nof_points);
	for (auto i : IndexRange{nof_points}) {
		ret.emplace_back(point_coords(i, 0), point_coords(i, 1));
	}
	return ret;
}

/// @file dllmain.h
/// Declaration of module class

//  Copyright (c) 2007-2022, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/gis/twist_gdal_utils.hpp"

class CTwistGisToolboxModule : public ATL::CAtlDllModuleT<CTwistGisToolboxModule> {
public:
    CTwistGisToolboxModule();

    ~CTwistGisToolboxModule() final;

	DECLARE_LIBID(LIBID_TwistGisToolboxLib)
	DECLARE_REGISTRY_APPID_RESOURCEID(IDR_TWISTGISTOOLBOX, "{a171ef39-f22c-4117-ae3a-f13414ade267}")

private:
    twist::gis::GdalInitialiser gdal_init_;
};

extern class CTwistGisToolboxModule _AtlModule;

// TWISTGeoCoordProjector.cpp : Implementation of TWISTGeoCoordProjector

#include "TwistGisToolbox/pch.h"

#include "TwistGisToolbox/TWISTGeoCoordProjector.h"

#include "twist/gis/GeoCoordProjectorGdal.hpp"
#include "twist/gis/SpatialReferenceSystem.hpp"
#include "twist/gis/twist_gdal_utils.hpp"

TWIST_COM_CLASS_IMPLEMENTATIONS(TWISTGeoCoordProjector, IID_ITWISTGeoCoordProjector)

using namespace twist::com;
using namespace twist::gis;

TWISTGeoCoordProjector::TWISTGeoCoordProjector()
{
}

TWISTGeoCoordProjector::~TWISTGeoCoordProjector()
{
}

STDMETHODIMP TWISTGeoCoordProjector::degree_to_metre(TWISTGeoPointDecDegree point_degrees,
		TWISTGeoPointMetre* point_metres)
{
	COM_METHOD_IMPL_BEGIN
	const auto pt_metres = projector_->degree_to_metre(GeoPointDecDegree{point_degrees.longitude, 
	                                                                     point_degrees.latitude});
    *point_metres = TWISTGeoPointMetre{pt_metres.x(), pt_metres.y()};
	COM_METHOD_IMPL_END
}

STDMETHODIMP TWISTGeoCoordProjector::metre_to_degree(TWISTGeoPointMetre point_metres, 
		TWISTGeoPointDecDegree* point_degrees)
{
	COM_METHOD_IMPL_BEGIN
	const auto pt_degrees = projector_->metre_to_degree(GeoPointMetre{point_metres.x, point_metres.y});
	*point_degrees = TWISTGeoPointDecDegree{pt_degrees.longitude(), pt_degrees.latitude()};
	COM_METHOD_IMPL_END
}

auto TWISTGeoCoordProjector::init_from_prj_file(const fs::path& path) -> void
{
	if (projector_) {
		TWIST_THROW(L"The projector has already been initialised.");
	}
	auto wgs84_refsys = refsys_from_wgs84();
	auto file_refsys = refsys_from_prj_file(path);
	projector_ = std::make_unique<GeoCoordProjectorGdal>(wgs84_refsys, file_refsys);
	TWIST_CHECK_INVARIANT
}

#ifdef _DEBUG
void TWISTGeoCoordProjector::check_invariant() const noexcept
{
	assert(projector_);
}
#endif


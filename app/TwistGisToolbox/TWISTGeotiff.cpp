/// @file TWISTGeotiff.cpp
/// Implementation file for "TWISTGeotiff.h"

//  Copyright (c) 2007-2022, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "TwistGisToolbox/pch.h"

#include "TwistGisToolbox/TWISTGeotiff.h"

#include "TwistGisToolbox/TWISTGeotiffProjection.h"
#include "TwistGisToolbox/TWISTSquareGeoSpaceGrid.h"

#include "twist/gis/geotiff_utils.hpp"
#include "twist/img/Tiff.hpp"

#define ENSURE_OPEN if (!file_) { TWIST_THROW(L"The file connection has been closed."); }

TWIST_COM_CLASS_IMPLEMENTATIONS(TWISTGeotiff, IID_ITWISTGeotiff)

using namespace twist;
using namespace twist::com;
using namespace twist::gis;

TWISTGeotiff::TWISTGeotiff()
{
}

TWISTGeotiff::~TWISTGeotiff()
{
}

STDMETHODIMP TWISTGeotiff::width(LONG* val)
{
	COM_METHOD_IMPL_BEGIN
	ENSURE_OPEN
	*val = static_cast<LONG>(file_->width());
	COM_METHOD_IMPL_END
}

STDMETHODIMP TWISTGeotiff::height(LONG* val)
{
	COM_METHOD_IMPL_BEGIN
	ENSURE_OPEN
	*val = static_cast<LONG>(file_->height());
	COM_METHOD_IMPL_END
}

STDMETHODIMP TWISTGeotiff::get_data_grid_info(ITWISTSquareGeoSpaceGrid** space_grid, TWISTDataValueType* data_value_type, 
	                                          BSTR* nodata_value)
{
	COM_METHOD_IMPL_BEGIN
	ENSURE_OPEN
	auto info = read_geo_data_grid_info(*file_);
	if (!info) {
		TWIST_THRO2(L"TIFF file \"{}\" does not contain geographic information.", file_->path().filename().c_str());
	}
	auto space_grid_ptr = TWISTSquareGeoSpaceGrid::create(to_square_grid(info->space_grid()));
	space_grid_ptr.CopyTo(space_grid);
	*data_value_type = static_cast<TWISTDataValueType>(info->data_value_type());
	*nodata_value = ansi_to_bstr(info->nodata_value());
	COM_METHOD_IMPL_END
}

STDMETHODIMP TWISTGeotiff::write_uint16_scanlined(UINT_PTR pixel_values, INT plane)
{
	COM_METHOD_IMPL_BEGIN
	ENSURE_OPEN
	file_->write_scanlined(reinterpret_cast<const uint16_t*>(pixel_values), std::nullopt, plane);
	COM_METHOD_IMPL_END
}

STDMETHODIMP TWISTGeotiff::read_projection(ITWISTGeotiffProjection** proj)
{
	COM_METHOD_IMPL_BEGIN
	ENSURE_OPEN
	if (auto proj_info = read_projection_info(*file_)) {
		auto proj_ptr = TWISTGeotiffProjection::create(std::move(*proj_info));
		proj_ptr.CopyTo(proj);
	}
	else {
		*proj = nullptr; 
	}
	COM_METHOD_IMPL_END
}

STDMETHODIMP TWISTGeotiff::write_projection(ITWISTGeotiffProjection* proj)
{
	COM_METHOD_IMPL_BEGIN
	ENSURE_OPEN
	write_projection_info(*file_, dyn_nonull_cast<TWISTGeotiffProjection*>(proj)->projection_info());
	COM_METHOD_IMPL_END
}

STDMETHODIMP TWISTGeotiff::close()
{
	COM_METHOD_IMPL_BEGIN
	ENSURE_OPEN
	file_.reset();
	COM_METHOD_IMPL_END
}

auto TWISTGeotiff::create(std::unique_ptr<twist::img::Tiff> file) -> CComPtr<ITWISTGeotiff>
{
	auto [icom, com] = create_com_atl<ITWISTGeotiff, TWISTGeotiff>();
	com->file_ = move(file);
	return icom;
}

#ifdef _DEBUG
void TWISTGeotiff::check_invariant() const noexcept
{
}
#endif


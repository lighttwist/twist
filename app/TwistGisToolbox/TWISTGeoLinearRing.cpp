// TWISTGeoLinearRing.cpp : Implementation of TWISTGeoLinearRing

#include "TwistGisToolbox/pch.h"

#include "TwistGisToolbox/TWISTGeoLinearRing.h"

#include "twist/std_vector_utils.hpp"

TWIST_COM_CLASS_IMPLEMENTATIONS(TWISTGeoLinearRing, IID_ITWISTGeoLinearRing)

using namespace twist;
using namespace twist::com;
using namespace twist::gis;
using namespace twist::math;

TWISTGeoLinearRing::TWISTGeoLinearRing()
{
}

TWISTGeoLinearRing::~TWISTGeoLinearRing()
{
}

STDMETHODIMP TWISTGeoLinearRing::nof_points(LONG* count)
{
	COM_METHOD_IMPL_BEGIN
	*count = static_cast<LONG>(linear_ring_->nof_points());
	COM_METHOD_IMPL_END
}

STDMETHODIMP TWISTGeoLinearRing::point(LONG idx, DOUBLE* x, DOUBLE* y)
{
	COM_METHOD_IMPL_BEGIN
	const auto point = linear_ring_->point(idx);
	*x = point.x();
	*y = point.y();
	COM_METHOD_IMPL_END
}

auto TWISTGeoLinearRing::create(std::vector<GeoPoint> points) -> CComPtr<ITWISTGeoLinearRing>
{
	auto [icom, com] = create_com_atl<ITWISTGeoLinearRing, TWISTGeoLinearRing>();
	com->linear_ring_.emplace(move(points));
	return icom;
}

auto TWISTGeoLinearRing::linear_ring() const -> GeoLinearRing
{
	TWIST_CHECK_INVARIANT
	return *linear_ring_;
}

#ifdef _DEBUG
auto TWISTGeoLinearRing::check_invariant() const noexcept -> void
{
	assert(linear_ring_);
}
#endif

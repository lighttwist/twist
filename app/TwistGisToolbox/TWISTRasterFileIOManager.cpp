/// @file TWISTRasterFileIOManager.cpp
//  Implementation file for "TWISTRasterFileIOManager.h"

//  Copyright (c) 2007-2022, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 
#include "TwistGisToolbox/pch.h"

#include "TwistGisToolbox/TWISTRasterFileIOManager.h"

#include "TwistGisToolbox/TWISTErdasImagineFile.h"
#include "TwistGisToolbox/TWISTSquareGeoSpaceGrid.h"
#include "TwistGisToolbox/TWISTGeotiff.h"

#include "twist/cast.hpp"
#include "twist/gis/ErdasImagineFile.hpp"
#include "twist/gis/twist_gdal_utils.hpp"
#include "twist/img/Tiff.hpp"

TWIST_COM_CLASS_IMPLEMENTATIONS(TWISTRasterFileIOManager, IID_ITWISTRasterFileIOManager)

using namespace twist;
using namespace twist::com;
using namespace twist::gis;
using namespace twist::img;

TWISTRasterFileIOManager::TWISTRasterFileIOManager()
{
}

TWISTRasterFileIOManager::~TWISTRasterFileIOManager()
{
}

STDMETHODIMP TWISTRasterFileIOManager::create_geotiff_file(BSTR path, ITWISTSquareGeoSpaceGrid* space_grid, 
                                                           TWISTDataValueType data_type, BSTR nodata_value, 
														   ITWISTGeotiff** file)
{
	COM_METHOD_IMPL_BEGIN
	auto file_path = bstr_to_path(path);
	if (exists(file_path)) {
		TWIST_THROW(L"GeoTIFF file \"%s\" already exists.", file_path.c_str());
	}
	const auto& spacegrid = dyn_nonull_cast<TWISTSquareGeoSpaceGrid*>(space_grid)->grid();

	auto tiff = std::make_unique<Tiff>(Tiff::open_write, std::move(file_path), GeoGridDataType{data_type}, 
									   spacegrid.nof_columns(), spacegrid.nof_rows());
	write_geo_data_grid_info(*tiff, GeoDataGridInfo{spacegrid, bstr_to_ansi(nodata_value), 
	                         GeoGridDataType{data_type}});

	auto file_ptr = TWISTGeotiff::create(move(tiff));
	file_ptr.CopyTo(file);
	COM_METHOD_IMPL_END
}

STDMETHODIMP TWISTRasterFileIOManager::open_geotiff_file(BSTR path, VARIANT_BOOL read_only, ITWISTGeotiff** file)
{
	COM_METHOD_IMPL_BEGIN
	auto tiff = varbool_to_bool(read_only) ? std::make_unique<Tiff>(Tiff::open_read, bstr_to_path(path))
	                                       : std::make_unique<Tiff>(Tiff::open_read_write, bstr_to_path(path));
	auto file_ptr = TWISTGeotiff::create(move(tiff));
	file_ptr.CopyTo(file);
	COM_METHOD_IMPL_END
}

STDMETHODIMP TWISTRasterFileIOManager::create_erdas_imagine_file(BSTR path, int width, int height, 
		TWISTDataValueType data_type, ITWISTErdasImagineFile** file)
{
	COM_METHOD_IMPL_BEGIN
	auto file_ptr = TWISTErdasImagineFile::create_file(bstr_to_string(path), width, height, 
	                                                   GeoGridDataType{data_type});
	file_ptr.CopyTo(file);
	COM_METHOD_IMPL_END
}

STDMETHODIMP TWISTRasterFileIOManager::open_erdas_imagine_file(BSTR path, ITWISTErdasImagineFile** file)
{
	COM_METHOD_IMPL_BEGIN
	auto file_ptr = TWISTErdasImagineFile::open_file(bstr_to_string(path));
	file_ptr.CopyTo(file);
	COM_METHOD_IMPL_END
}

#ifdef _DEBUG
void TWISTRasterFileIOManager::check_invariant() const noexcept
{
}
#endif 

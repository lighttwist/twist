/// @file globals.h
/// Globals for the "TwistGisToolbox" library

//  Copyright (c) 2007-2022, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#pragma once

#include "twist/gis/gis_geometry.hpp"
#include "twist/math/FlatMatrix.hpp"

/*! Convert a "variant type" value to a "geo-grid data value type" value.
    \param[in] variant_type  The "variant type" value; an exception is thrown if it does not correspond to a valid 
	                         "geo-grid data value type"
    \return  The resut of the conversion
 */
[[nodiscard]] auto to_geogrid_data_type(VARTYPE variant_type) -> twist::gis::GeoGridDataType;

/*! Create a vector of GeoPoint objects from the point coordinates stored in \p point_coords (element[n, 0] is the x 
    coordinate of the n-th point, and [n,1] is the y coordinate).
 */
[[nodiscard]] auto to_points(const twist::math::FlatMatrix<double>& point_coords) -> std::vector<twist::gis::GeoPoint>;

#define COM_METHOD_IMPL_BEGIN  \
			TWIST_CHECK_INVARIANT  \
			HRESULT ret = S_OK;   \
			try {

#define COM_METHOD_IMPL_END  \
			}  \
			catch (const std::exception& ex) {  \
				return TWIST_GENERATE_COM_ERROR(ex);  \
			}  \
			return ret;

/// @file TWISTVectorFileIOManager.h
/// TWISTVectorFileIOManager COM class

//  Copyright (c) 2007-2022, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#pragma once

#include "TwistGisToolbox/resource.h"      
#include "TwistGisToolbox/TwistGisToolbox_i.h"

using namespace ATL;

//! Manager for creating and opening geographic vector files.
class ATL_NO_VTABLE TWISTVectorFileIOManager :
	public CComObjectRootEx<CComMultiThreadModel>,
	public CComCoClass<TWISTVectorFileIOManager, &CLSID_TWISTVectorFileIOManager>,
	public ISupportErrorInfo,
	public IDispatchImpl<ITWISTVectorFileIOManager, &IID_ITWISTVectorFileIOManager, &LIBID_TwistGisToolboxLib, 1, 0> {
public:
	// --- ITWISTVectorFileIOManager ---

	/*! Create a new ESRI shapefile, containing one (default) layer.
        \param[in] shapefile_dir_path  Path to the new shapefile directory
		\param[in] layer_geom_type  The geometry type of the default layer
		\param[in] layer_refsys  The spatial reference system to use for the default layer; pass in 
		                         TWISTKnownGeoRefsysId::no_refsys for none
		\param[out,retval] file  Open connection to the file
     */
	STDMETHOD(create_shapefile)(BSTR shapefile_dir_path, 
	                            TWISTVectorGeometryType layer_geom_type, 
								TWISTKnownGeoRefsysId refsys_id,
	                            ITWISTShapefile** file); 

	/*! Open a connection to an existing ESRI shapefile.
		\param[in] path  Path to the main (.shp) file in the shapefile
		\param[in] read_only  Whether the shapefile shuould be open in read-only mode
		\param[out,retval] file  Open connection to the file
	 */
	STDMETHOD(open_shapefile)(BSTR path, VARIANT_BOOL read_only, ITWISTShapefile** file);

	TWIST_COM_CLASS_DECLARATIONS(TWISTVectorFileIOManager, ITWISTVectorFileIOManager, IDR_TWISTVECTORFILEIOMANAGER)

private:
	TWIST_CHECK_INVARIANT_DECL
};

OBJECT_ENTRY_AUTO(__uuidof(TWISTVectorFileIOManager), TWISTVectorFileIOManager)

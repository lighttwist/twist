/// @file TWISTVectorFileIOManager.cpp
//  Implementation file for "TWISTVectorFileIOManager.h"

//  Copyright (c) 2007-2022, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "TwistGisToolbox/pch.h"

#include "TwistGisToolbox/TWISTVectorFileIOManager.h"

#include "TwistGisToolbox/TWISTShapefile.h"

#include "twist/gis/Shapefile.hpp"
#include "twist/gis/twist_gdal_utils.hpp"

TWIST_COM_CLASS_IMPLEMENTATIONS(TWISTVectorFileIOManager, IID_ITWISTVectorFileIOManager)

using namespace twist::com;
using namespace twist::gis;

TWISTVectorFileIOManager::TWISTVectorFileIOManager()
{
}

TWISTVectorFileIOManager::~TWISTVectorFileIOManager() 
{
}

STDMETHODIMP TWISTVectorFileIOManager::create_shapefile(BSTR shapefile_dir_path, 
                                                        TWISTVectorGeometryType layer_geom_type, 
														TWISTKnownGeoRefsysId refsys_id,
                                                        ITWISTShapefile** file)
{
	COM_METHOD_IMPL_BEGIN
	const auto opt_refsys_id = (refsys_id != TWISTKnownGeoRefsysId::no_refsys)
						               ? std::optional{refsys_from_known(static_cast<KnownGeoRefsysId>(refsys_id))}
						               : std::optional<SpatialReferenceSystem>{};
	auto shapefile = std::make_unique<Shapefile>(Shapefile::create, 
	                                             bstr_to_path(shapefile_dir_path),
												 VectorGeometryType{layer_geom_type},
												 opt_refsys_id);
	auto icom = TWISTShapefile::create(move(shapefile));
	icom.CopyTo(file);
	COM_METHOD_IMPL_END
}

STDMETHODIMP TWISTVectorFileIOManager::open_shapefile(BSTR path, VARIANT_BOOL read_only, ITWISTShapefile** file)
{
	COM_METHOD_IMPL_BEGIN
	auto shapefile = std::make_unique<Shapefile>(Shapefile::open, bstr_to_path(path), varbool_to_bool(read_only));
	auto icom = TWISTShapefile::create(move(shapefile));
	icom.CopyTo(file);
	COM_METHOD_IMPL_END
}

#ifdef _DEBUG
void TWISTVectorFileIOManager::check_invariant() const noexcept
{
}
#endif 

///  @file  TWISTGisToolboxLibInfo.h
///  TWISTGisToolboxLibInfo COM class definition

//   Copyright (c) 2010-2019 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#pragma once

#include "TwistGisToolbox/resource.h"       
#include "TwistGisToolbox/TwistGisToolbox_i.h"

using namespace ATL;

/// COM class which provides information about the "TwistGisToolboxLib" library "dll" file which this class 
/// is part of.
class ATL_NO_VTABLE TWISTGisToolboxLibInfo :
	public CComObjectRootEx<CComMultiThreadModel>,
	public CComCoClass<TWISTGisToolboxLibInfo, &CLSID_TWISTGisToolboxLibInfo>,
	public ISupportErrorInfo,
	public IDispatchImpl<ITWISTGisToolboxLibInfo, &IID_ITWISTGisToolboxLibInfo, &LIBID_TwistGisToolboxLib, 1, 0> {
public:
	// --- ITWISTGisToolboxLibInfo methods ---

	/// Get the path of this dll module.
	///
	/// @param  path  [out,retval] The path
	///
	STDMETHOD(get_dll_path)(BSTR* path);

	/// Get the version numbers of this dll module.
	///
	/// @param  major_ver_no  [out] The major version number
	/// @param  minor_ver_no  [out] The minor version number
	/// @param  release_no  [out] Release number
	/// @param  build_no  [out] Build number
	///
	STDMETHOD(get_dll_version)(LONG* major_ver_no, LONG* minor_ver_no, LONG* release_no, LONG* build_no);

	/// Get the version numbers of this dll module as a string.
	///
	/// @param  version_str  [out,retval] The version string, in the format "major.minor.release.build"
	///
	STDMETHOD(get_dll_version_string)(BSTR* version_str);

	// --- Other ---

	TWIST_COM_CLASS_DECLARATIONS(TWISTGisToolboxLibInfo, ITWISTGisToolboxLibInfo, IDR_TWISTGISTOOLBOXLIBINFO)

private:
	TWIST_CHECK_INVARIANT_DECL
};

OBJECT_ENTRY_AUTO(__uuidof(TWISTGisToolboxLibInfo), TWISTGisToolboxLibInfo)

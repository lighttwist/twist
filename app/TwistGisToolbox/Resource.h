//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by TwistGisToolbox.rc
//
#define IDS_PROJNAME                    100
#define IDR_TWISTGISTOOLBOX             101
#define IDR_TWISTRASTERFILEIOMANAGER    106
#define IDR_TWISTERDASIMAGINEFILE       107
#define IDR_TWISTGDALTOOLBOX            108
#define IDR_TWISTGISTOOLBOXLIBINFO      109
#define IDR_TWISTTESTCLASS              110
#define IDR_TWISTGEOCOORDPROJECTOR      111
#define IDR_TWISTCLASSFACTORY           114
#define IDR_TWISTGEOLINESTRING          115
#define IDR_TWISTGEOLINEARRING          116
#define IDR_TWISTGEOPOLYGON             119
#define IDR_TWISTSHAPEFILE              120
#define IDR_TWISTVECTORFILEIOMANAGER    121
#define IDR_TWISTGEOSHAPECOLLECTION     122
#define IDR_TWISTGEOTIFF                123
#define IDR_TWIST_SQUARE_GEO_SPACE_GRID           124
#define IDR_TWIST_GEOTIFF_PROJECTION    125

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        201
#define _APS_NEXT_COMMAND_VALUE         32768
#define _APS_NEXT_CONTROL_VALUE         201
#define _APS_NEXT_SYMED_VALUE           126
#endif
#endif

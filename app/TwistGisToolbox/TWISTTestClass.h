
#pragma once

#include "TwistGisToolbox/resource.h"
#include "TwistGisToolbox/TwistGisToolbox_i.h"

using namespace ATL;

class ATL_NO_VTABLE TWISTTestClass :
	public CComObjectRootEx<CComMultiThreadModel>,
	public CComCoClass<TWISTTestClass, &CLSID_TWISTTestClass>,
	public ISupportErrorInfo,
	public IDispatchImpl<ITWISTTestClass, &IID_ITWISTTestClass, &LIBID_TwistGisToolboxLib, 1, 0> {
public: 
	// --- ITWISTTestClass methods ---

	STDMETHOD(method1)(UINT_PTR shared_mem_addr, INT row_count, INT col_count);

	// --- Other ---

	TWIST_COM_CLASS_DECLARATIONS(TWISTTestClass, ITWISTTestClass, IDR_TWISTTESTCLASS)

private:
	TWIST_CHECK_INVARIANT_DECL
};

OBJECT_ENTRY_AUTO(__uuidof(TWISTTestClass), TWISTTestClass)

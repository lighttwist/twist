/// @file TWISTSquareGeoSpaceGrid.h
/// TWISTSquareGeoSpaceGrid COM class definition

//  Copyright (c) 2007-2022, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#pragma once

#include "TwistGisToolbox/resource.h"      
#include "TwistGisToolbox/TwistGisToolbox_i.h"

#include "twist/gis/gis_geometry.hpp"

using namespace ATL;

class ATL_NO_VTABLE TWISTSquareGeoSpaceGrid :
	public CComObjectRootEx<CComMultiThreadModel>,
	public CComCoClass<TWISTSquareGeoSpaceGrid, &CLSID_TWISTSquareGeoSpaceGrid>,
	public ISupportErrorInfo,
	public IDispatchImpl<ITWISTSquareGeoSpaceGrid, &IID_ITWISTSquareGeoSpaceGrid, &LIBID_TwistGisToolboxLib, 1, 0> {
public:
	// --- ITWISTSquareGeoSpaceGrid ---

	/*! The X coordinate of the left size of the grid perimeter.
	    \param[out,retval] val  The X coordinate
	 */
	STDMETHOD(left)(DOUBLE* val);

	/*! The Y coordinate of the bottom side of the grid perimeter.
	    \param[out,retval] val  The Y coordinate
	 */
	STDMETHOD(bottom)(DOUBLE* val);

	/*! The size of a cell's side (cells are square) in the spatial units used by the grid coordinates.
	    \param[out,retval] val  The cell size
	 */	
	STDMETHOD(cell_size)(DOUBLE* val);

	/*! The number of rows in the grid.
	    \param[out,retval] val  The number of rows
	 */
	STDMETHOD(nof_rows)(INT* val);

	/*! The number of columns in the grid.
	    \param[out,retval] val  The number of columns
	 */
	STDMETHOD(nof_columns)(INT* val);

	// --- Other ---

	[[nodiscard]] static auto create(const twist::gis::SquareGeoSpaceGrid& grid) -> CComPtr<ITWISTSquareGeoSpaceGrid>;

	[[nodiscard]] auto grid() const -> const twist::gis::SquareGeoSpaceGrid&;

	TWIST_COM_CLASS_DECLARATIONS(TWISTSquareGeoSpaceGrid, ITWISTSquareGeoSpaceGrid, IDR_TWIST_SQUARE_GEO_SPACE_GRID)

private:
	std::optional<twist::gis::SquareGeoSpaceGrid> grid_;

	TWIST_CHECK_INVARIANT_DECL
};

OBJECT_ENTRY_NON_CREATEABLE_EX_AUTO(__uuidof(TWISTSquareGeoSpaceGrid), TWISTSquareGeoSpaceGrid)

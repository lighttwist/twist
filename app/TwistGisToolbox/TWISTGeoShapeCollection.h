/// @file TWISTGeoShapeCollection.h
/// TWISTGeoShapeCollection COM class definition

//  Copyright (c) 2007-2022, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#pragma once

#include "TwistGisToolbox/resource.h"       
#include "TwistGisToolbox/TwistGisToolbox_i.h"

#include "twist/com/InterfaceCollectionBase.hpp"

using namespace ATL;

//! Collection of IGeoShape interfaces.
class ATL_NO_VTABLE TWISTGeoShapeCollection : 
		public twist::com::InterfaceCollectionBase<TWISTGeoShapeCollection, 
												   ITWISTGeoShapeCollection, 
												   ITWISTGeoShape,
												   &CLSID_TWISTGeoShapeCollection,
												   &IID_ITWISTGeoShapeCollection, 
												   &LIBID_TwistGisToolboxLib> {
public:
	TWIST_COM_CLASS_DECLARATIONS(TWISTGeoShapeCollection, ITWISTGeoShapeCollection, IDR_TWISTGEOSHAPECOLLECTION)
};

OBJECT_ENTRY_AUTO(__uuidof(TWISTGeoShapeCollection), TWISTGeoShapeCollection)

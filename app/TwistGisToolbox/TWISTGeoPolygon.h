/// @file TWISTGeoPolygon.h
/// TWISTGeoPolygon COM class definition

//  Copyright (c) 2007-2022, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#pragma once

#include "TwistGisToolbox/resource.h"       
#include "TwistGisToolbox/TwistGisToolbox_i.h"

#include "twist/gis/gis_geometry.hpp"

using namespace ATL;

//! A "polygon" in any map coordinates. A polygon consists of one outer ring and zero or more inner rings (the holes).
class ATL_NO_VTABLE TWISTGeoPolygon :
	public CComObjectRootEx<CComMultiThreadModel>,
	public CComCoClass<TWISTGeoPolygon, &CLSID_TWISTGeoPolygon>,
	public ISupportErrorInfo,
	public IDispatchImpl<ITWISTGeoPolygon, &IID_ITWISTGeoPolygon, &LIBID_TwistGisToolboxLib, 1, 0> {
public:
	// --- ITWISTGeoPolygon ---

	/*! Get the outer ring.
        \param[out,retval] out_ring  The outer ring
     */
	STDMETHOD(outer_ring)(ITWISTGeoLinearRing** out_ring);

	/*! Get the inner rings.
        \param[out,retval] inner_rings  List of inner rings; all elements in the list are ITWISTGeoLinearRing
     */
	STDMETHOD(inner_rings)(ITWISTGeoShapeCollection** inner_rings);

    /*! Find out whether the polygon is "holey", ie has any inner rings.
        \param[out,retval] is  Whether the polygon is holey
     */ 
    STDMETHOD(is_holey)(VARIANT_BOOL* is);

	// --- Other ---

	[[nodiscard]] static auto create(twist::gis::GeoPolygon poly) -> CComPtr<ITWISTGeoPolygon>;

	[[nodiscard]] auto polygon() const -> twist::gis::GeoPolygon;

	TWIST_COM_CLASS_DECLARATIONS_NO_MAP(TWISTGeoPolygon, ITWISTGeoPolygon, IDR_TWISTGEOPOLYGON)

	BEGIN_COM_MAP(TWISTGeoPolygon)
		COM_INTERFACE_ENTRY(ITWISTGeoPolygon)
		COM_INTERFACE_ENTRY(ITWISTGeoShape)
		COM_INTERFACE_ENTRY(IDispatch)
		COM_INTERFACE_ENTRY(ISupportErrorInfo)
	END_COM_MAP()

private:
	std::optional<twist::gis::GeoPolygon> poly_;

	TWIST_CHECK_INVARIANT_DECL
};

OBJECT_ENTRY_NON_CREATEABLE_EX_AUTO(__uuidof(TWISTGeoPolygon), TWISTGeoPolygon)

/// @file Chronometer.hpp
/// Chronometer class definition

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_CHRONOMETER_HPP
#define TWIST_CHRONOMETER_HPP

#include "twist/globals.hpp"

#include <ctime>
#include <string>

namespace twist {

//+TODO: The new utilities introduced in std::chrono should be used internally.

/*! This class represents a chronometer which can be started and stopped. The time elapsed (and/or a string describing 
    it) can be obtained upon/after stopping the chronometer. The time unit (milliseconds, seconds, hours) is set in the 
	constructor. 
    \note  If you need a high-resolution clock, it is better to use HiResClock.
*/
class Chronometer {
public:
	//! The type of time unit used.
	enum class Unit {		
		millisecs, ///< Milliseconds 
		seconds,   ///< Seconds		
		hours	   ///< Hours
	};

	/*! Constructor.        
        \param[in] unit  The unit for the output time.  
     */
	Chronometer(Unit unit);

	Chronometer(const Chronometer&) = delete;
	
	Chronometer(Chronometer&&) noexcept;
	
	auto operator=(const Chronometer&) -> Chronometer& = delete; 

	auto operator=(Chronometer&&) noexcept -> Chronometer& = delete;
	
	//! Access to the unit for this chronometer (one of the unit constants).
	[[nodiscard]] auto unit() const -> Unit;

	/*! Start the chronometer. The chronometer must have been started previously (calls to start() and stop() must 
	    be paired).
     */
	auto start() -> void;

	/*! Stop the chronometer. The chronometer must not be currently stopped (calls to start() and stop() must 
	    be paired).
		\return  The time elapsed, in the units used by the chronometer
     */
	auto stop() -> double;

	/*! Get the last recorded time (i.e. the period between the last call to start() and the subsequent call to 
	    stop()). Only call this method after a call to stop().
	    \return  The time, in the units used by the chronometer
     */
	[[nodiscard]] auto last_recorded_time() const -> double;

	/*! Get the usual textual symbol for the timer's unit.
	    \param[in] singular  Whether the symbol should be amended for singular (only has effect for unit "hours")
	    \return  The symbol
     */
	[[nodiscard]] auto get_unit_symbol(bool singular = false) const -> std::wstring;

private:	       
	static const double invalid_time;

    const Unit unit_;
    clock_t start_time_{0};
    double last_rec_time_{invalid_time};

	TWIST_CHECK_INVARIANT_DECL
};

//! Create and start a chronometer usign milliseconds as units.
[[nodiscard]] auto start_millisec_chronometer() -> Chronometer;

//! Create and start a chronometer usign seconds as units.
[[nodiscard]] auto start_second_chronometer() -> Chronometer;

} 

#endif 

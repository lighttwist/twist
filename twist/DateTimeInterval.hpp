/// @file DateTimeInterval.hpp
/// DateTimeInterval class definition

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_DATE_TIME_INTERVAL_HPP
#define TWIST_DATE_TIME_INTERVAL_HPP

#include "twist/DateTime.hpp"

namespace twist {

//! A time interval between two date-time values.
class DateTimeInterval {
public:
	/*! Constructor. 
	    \param[in] lo  The lower bound of the interval
	    \param[in] hi  The upper bound of the interval; must be equal or after the lower bound
	 */
	DateTimeInterval(const DateTime& lo, const DateTime& hi);

	//! The lower bound of the interval.
	[[nodiscard]] auto lo() const -> DateTime;
	
	//! The upper bound of the interval.
	[[nodiscard]] auto hi() const -> DateTime;

	[[nodiscard]] friend auto operator==(const DateTimeInterval& lhs, const DateTimeInterval& rhs) -> bool;

private:	
	DateTime lo_;
	DateTime hi_;

	TWIST_CHECK_INVARIANT_DECL
};

// --- Non-member functions ---

/*! Whether the date-time interval \p interval contains the date-time point \p time (if \p time matches one of the 
    interval bounds, it is considered inside the interval).
 */
[[nodiscard]] auto contains(const DateTimeInterval& interval, const DateTime& time) -> bool;

//! Get the difference, in whole hours, between the bounds of the date-time interval \p interval.
[[nodiscard]] auto length_hours(const DateTimeInterval& interval) -> int;

} 

#endif 

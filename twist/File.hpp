///  @file  File.hpp
///  FileStd class definition
///  FileWin class definition
///  File class typedef

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_FILE_HPP
#define TWIST_FILE_HPP

#include <fstream>

#include "meta_ranges.hpp"

namespace twist {

/// File open modes
enum class FileOpenMode {
	/// Open the file for reading. Other processes are allowed to read and write to the file.
	read = 1,
	/// Open the file for writing. Other processes are allowed to read the file. 
	write = 2,
	/// Open the file for reading and writing. Other processes are allowed to read the file. 
	read_write = 3,
	/// Create a new file for reading and writing. If the file already exists, it will be truncated. 
	/// Other processes are not allowed access to the file.
	create_write = 4,
	/// Create a new file for reading and writing. If the file already exists, it will be truncated. 
	/// Other processes are not allowed access to the file.
	create_read_write = 5	
};

/// File seek directions
enum FileSeekDir {
	/// The seek offset is measured from the beginning of the file.
	seek_begin = 1,		
	/// The seek offset is measured from the current position of the file pointer.
	seek_current = 2,	
	/// The seek offset is measured from the end of the file.
	seek_end = 3		
};


///  Represents a generic file in a file system. 
///	 During the lifetime of an instance of this class, the associated file is guaranteed to exist and be open 
///    (the associated file is open in the constructor, and is closed in the destructor).
///	 This class facilitates raw binary operations (byte operations) on the associated file.
///	 The class implementation uses only standard C++, and so is platform independent.
class FileStd {
public:
	/// Constructor.
	///
	/// @param[in] path  The path of the associated file 
	/// @param[in] open_mode  The file open mode
	///
	FileStd(fs::path path, FileOpenMode open_mode);

	/// Destructor.
	///
	~FileStd();
	
	/// Get the file path.
	///
	/// @return  The path
	///
	fs::path path() const;
	
	/// Return the size of the file, in bytes.
	///
	/// @return  The file size.
	///
	std::int64_t get_file_size() const;
	
	/// Get the current position of the file pointer.
	///
	/// @return  The file pointer position (i.e. the distance, in bytes, from the beginning of the file).
	///
	std::int64_t get_file_pos() const;

	/// Move the file pointer to a certain position within the file.
	///
	/// @param[in] offset  The file position, i.e. the distance, in bytes (from a certain position in the 
	///					file) where the next read/write operation will start from. The position in the file 
	///					from where we measure the distance is determined by the value of the seek_dir 
	///					parameter.
	/// @param[in] seek_dir  The file seek direction. Negative means seek backwards.
	///
	void seek(long long offset, FileSeekDir seek_dir) const;

	/// Read from the file into a memory buffer, starting with the current file position.
	///
	/// @param[in] buffer  The buffer
	/// @param[in] count  The number of bytes to be read from the file	
	///
	void read_buf(char* buffer, Ssize count) const;

	/// Write the contents of a memory buffer to the file, starting with the current file position.
	/// This is the fastest implementation that I could work out.
	///
	/// @param[in] buffer  The buffer.
	/// @param[in] count  The number of bytes to be written to the file.
	///
	void write_buf(const char* buffer, Ssize count);

	TWIST_NO_COPY_NO_MOVE(FileStd)
	
private:		
	std::wstring wfilename() const;

	const fs::path path_;
	const FileOpenMode open_mode_;
	mutable std::fstream stream_;

	TWIST_CHECK_INVARIANT_DECL
};

#if TWIST_OS_WIN

///  Represents a generic file in a file system. 
///	 During the lifetime of an instance of this class, the associated file is guaranteed to exist and be open 
///    (the associated file is open in the constructor, and is closed in the destructor).
///	 This class facilitates raw binary operations (byte operations) on the associated file.
///	 The class interface is platform independent, while the implementation is Windows specific.
class FileWin {
public:
	/// Constructor.
	///
	/// @param[in] path  The path of the associated file 
	/// @param[in] open_mode  The file open mode
	///
	FileWin(fs::path path, FileOpenMode open_mode);

	/// Destructor.
	///
	~FileWin();
	
	/// Get the file path.
	///
	/// @return  The path
	///
	fs::path path() const;
	
	/// Return the size of the file, in bytes.
	///
	/// @return  The file size.
	///
	std::int64_t get_file_size() const;
	
	/// Get the current position of the file pointer.
	///
	/// @return  The file pointer position (i.e. the distance, in bytes, from the beginning of the file).
	///
	std::int64_t get_file_pos() const;

	/// Move the file pointer to a certain position within the file.
	///
	/// @param[in] offset  The file position, i.e. the distance, in bytes (from a certain position in the 
	///					file) where the next read/write operation will start from. The position in the file 
	///					from where we measure the distance is determined by the value of the seek_dir 
	///					parameter.
	/// @param[in] seek_dir  The file seek direction. Negative means seek backwards.
	///
	void seek(long long offset, FileSeekDir seek_dir) const;

	/// Read from the file into a memory buffer, starting with the current file position.
	///
	/// @param[in] buffer  The buffer
	/// @param[in] count  The number of bytes to be read from the file	
	///
	void read_buf(char* buffer, Ssize count) const;

	/// Write the contents of a memory buffer to the file, starting with the current file position.
	/// This is the fastest implementation that I could work out.
	///
	/// @param[in] buffer  The buffer.
	/// @param[in] count  The number of bytes to be written to the file.
	///
	void write_buf(const char* buffer, Ssize count);

	TWIST_NO_COPY_NO_MOVE(FileWin)
	
private:		
	using Handle = void*;

	std::wstring wfilename() const;

	const fs::path  path_;
	const FileOpenMode  open_mode_;
	Handle  handle_{};

	TWIST_CHECK_INVARIANT_DECL
};

#endif

#if TWIST_OS_WIN
  // Until exhaustive testing and comparison is done, for now it is unclear whether this can be more efficient
  // than FileStd on Windows
  using File = FileWin;
#elif TWIST_OS_LINUX
  using File = FileStd;
#endif

using FilePtr = std::unique_ptr<FileStd>;
using FilePtrList = std::vector<FilePtr>;

//
//  Free functions
//

/// Read data from a file into a memory buffer, starting with the current file position.
///
/// @tparam  Cont  The type of the container which represents the memory buffer
/// @param[in] file  Connection to the file  
/// @param[in] cont  The container which represents the memory buffer; the data is assumed to be an array of 
///					elements of have the same type as the container elements; the amount of data read from the 
///					file will be exactly the amount of data necessary to fill the container
///
template<class Cont,
         class = EnableIfContigContainer<Cont>>
void read_buf(const File& file, Cont& cont);

/// Write the contents of a memory buffer to a file, starting with the current file position.
///
/// @tparam  Cont  The type of the container which represents the memory buffer
/// @param[in] file  Connection to the file  
/// @param[in] cont  The container which represents the memory buffer; the file is assumed to store elements 
///					of the same type as the container elements; all the elements in the container will be 
///					written to the file
///
template<class Cont,
         class = EnableIfContigContainer<Cont>>
void write_buf(File& file, const Cont& cont);

} 

#include "File.ipp"

#endif 

/// @file feedback_providers.cpp
/// Implementation file for feedback_providers.hpp

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/feedback_providers.hpp"

#include "twist/DateTime.hpp"

namespace twist {

// --- MessageFeedbackProv class ---

MessageFeedbackProv::MessageFeedbackProv(std::wstring indent_str)
	: indent_str_{move(indent_str)}
{
	TWIST_CHECK_INVARIANT
}

MessageFeedbackProv::~MessageFeedbackProv()
{
	TWIST_CHECK_INVARIANT
}

auto MessageFeedbackProv::set_prog_msg(std::wstring_view msg, bool try_replace_prev) -> void
{
	TWIST_CHECK_INVARIANT
	if (indent_str_.empty() || indent_steps_ == 0) {
		do_set_prog_msg(msg, try_replace_prev);
	}
	else {
		auto total_indent_str = std::wstring{};
		total_indent_str.reserve(indent_steps_ * ssize(indent_str_));
		for ([[maybe_unused]] auto i : IndexRange{indent_steps_}) {
			total_indent_str += indent_str_;	
		}
		do_set_prog_msg(total_indent_str + msg, try_replace_prev);
	}
}

auto MessageFeedbackProv::set_prog_msg(std::string_view msg, bool try_replace_prev) -> void
{
	TWIST_CHECK_INVARIANT
    set_prog_msg(ansi_to_string(msg), try_replace_prev);
}

auto MessageFeedbackProv::prog_msg_indent() const -> int
{
	TWIST_CHECK_INVARIANT
	return indent_steps_;
}

auto MessageFeedbackProv::set_prog_msg_indent(int indent_steps) -> void
{
	TWIST_CHECK_INVARIANT
	indent_steps_ = indent_steps;
}

auto MessageFeedbackProv::set_fatal_error_msg(std::wstring_view msg) -> void
{
	TWIST_CHECK_INVARIANT
	do_set_fatal_error_msg(msg);
}

#ifdef _DEBUG
auto MessageFeedbackProv::check_invariant() const noexcept -> void
{
	assert(indent_steps_ >= 0);
}
#endif

// --- ExceptionFeedbackProv class ---

ExceptionFeedbackProv::~ExceptionFeedbackProv()
{
}

void ExceptionFeedbackProv::report_uncaught_exception(std::exception_ptr exptr)
{
	do_report_uncaught_exception(exptr);
}

// --- LogFileFeedbackProvider class ---

LogFileFeedbackProvider::LogFileFeedbackProvider(fs::path path, bool use_timestamps)
	: path_{std::move(path)}
	, use_timestamps_{use_timestamps}
{
	file_stream_.open(path_.c_str());
	if (!file_stream_.good()) {
		TWIST_THRO2(L"Cannot open log file \"{}\".", path_.c_str());
	}
	TWIST_CHECK_INVARIANT
}

auto LogFileFeedbackProvider::do_set_prog_msg(std::wstring_view msg, bool /*try_replace_prev*/) -> void
{
	TWIST_CHECK_INVARIANT
	if (use_timestamps_) {
		const auto stamp = L"[" + date_time_to_str(get_system_datetime(), DateTimeStrFormat::dd_mm_yyyy_hh_mm_ss) + 
		                   L"] ";
		auto stamped_msg = std::wstring{};
		auto pos = msg.find_first_not_of(L"\n");
		if (pos != std::wstring_view::npos) {
			stamped_msg = std::wstring(pos, L'\n') + stamp + msg.substr(pos);
		}
		else {
			stamped_msg = stamp + msg;		
		}
		file_stream_ << stamped_msg;
	}
	else {
		file_stream_ << msg;	
	}

	file_stream_ << L"\n\n";	
	file_stream_.flush();
}

auto LogFileFeedbackProvider::do_set_fatal_error_msg(std::wstring_view msg) -> void 
{
	TWIST_CHECK_INVARIANT
	do_set_prog_msg(std::wstring{L"FATAL ERROR:  "} + msg, false/*try_replace_prev*/);
}

#ifdef _DEBUG
auto LogFileFeedbackProvider::check_invariant() const noexcept -> void
{
	assert(!is_whitespace(path_));
}
#endif	

// --- MultipleFeedbackProvider class ---

MultipleFeedbackProvider::MultipleFeedbackProvider(Providers providers)
	: providers_{std::move(providers)}
{
	TWIST_CHECK_INVARIANT
}

auto MultipleFeedbackProvider::do_set_prog_msg(std::wstring_view msg, bool try_replace_prev) -> void 
{
	TWIST_CHECK_INVARIANT
	for (const auto& prov : providers_) {
		prov->set_prog_msg(msg, try_replace_prev);
	}
}

auto MultipleFeedbackProvider::do_set_fatal_error_msg(std::wstring_view msg) -> void 
{
	TWIST_CHECK_INVARIANT
	for (const auto& prov : providers_) {
		prov->set_fatal_error_msg(msg);
	}
}

#ifdef _DEBUG
auto MultipleFeedbackProvider::check_invariant() const noexcept -> void
{
	assert(!providers_.empty());
}
#endif	

// --- DummyMessageFeedbackProv class ---

auto DummyMessageFeedbackProv::do_set_prog_msg(std::wstring_view, bool) -> void
{
}

auto DummyMessageFeedbackProv::do_set_fatal_error_msg(std::wstring_view) -> void
{
}

// --- ScopedProgressMessageIndenter class - 

ScopedProgressMessageIndenter::ScopedProgressMessageIndenter(MessageFeedbackProv& feedback_prov)
	: feedback_prov_{feedback_prov}
{
	inc_prog_msg_indent(feedback_prov_);
}

ScopedProgressMessageIndenter::~ScopedProgressMessageIndenter()
{
	dec_prog_msg_indent(feedback_prov_);
}

// --- Free functions ---

auto inc_prog_msg_indent(MessageFeedbackProv& feedback_prov) -> int
{
	const auto new_indent = feedback_prov.prog_msg_indent() + 1;
	feedback_prov.set_prog_msg_indent(new_indent);
	return new_indent;
}

auto dec_prog_msg_indent(MessageFeedbackProv& feedback_prov) -> int
{
	const auto new_indent = feedback_prov.prog_msg_indent() - 1;
	feedback_prov.set_prog_msg_indent(new_indent);
	return new_indent;
}

auto make_cout_feedback_prov() -> StreamMessageFeedbackProv<std::wostream>
{
	return StreamMessageFeedbackProv<std::wostream>{std::wcout};
}

}

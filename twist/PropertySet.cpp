/// @file PropertySet.cpp
/// Implementation file for "PropertySet.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/PropertySet.hpp"

namespace twist {

PropertySet::PropertySet() 
{
	TWIST_CHECK_INVARIANT
}


PropertySet::PropertySet(const PropertySet& src)
	: prop_values_(src.prop_values_)
{
	TWIST_CHECK_INVARIANT
}


PropertySet::PropertySet(PropertySet&& src)
	: prop_values_(move(src.prop_values_))
{
	TWIST_CHECK_INVARIANT
}


PropertySet::~PropertySet()
{
	TWIST_CHECK_INVARIANT
}


PropertySet& PropertySet::operator=(const PropertySet& rhs)
{
	TWIST_CHECK_INVARIANT
	if (&rhs != this) {
		prop_values_ = rhs.prop_values_;
	}
	TWIST_CHECK_INVARIANT
	return *this;
}


PropertySet& PropertySet::operator=(PropertySet&& rhs)
{
	TWIST_CHECK_INVARIANT
	if (&rhs != this) {
		prop_values_ = move(rhs.prop_values_);
	}
	TWIST_CHECK_INVARIANT
	return *this;
}


void PropertySet::define_int_prop(PropertyId prop_id, int val)
{
	TWIST_CHECK_INVARIANT
	define_prop(prop_id, val);
	TWIST_CHECK_INVARIANT
}


void PropertySet::define_float_prop(PropertyId prop_id, double val)
{
	TWIST_CHECK_INVARIANT
	define_prop(prop_id, val);
	TWIST_CHECK_INVARIANT
}


void PropertySet::define_string_prop(PropertyId prop_id, const std::wstring& val)
{
	TWIST_CHECK_INVARIANT
	define_prop(prop_id, val);
	TWIST_CHECK_INVARIANT
}


void PropertySet::define_bool_prop(PropertyId prop_id, bool val)
{
	TWIST_CHECK_INVARIANT
	define_prop(prop_id, val);
	TWIST_CHECK_INVARIANT
}


int PropertySet::get_int_value(PropertyId prop_id) const
{
	TWIST_CHECK_INVARIANT
	return get_value(prop_id).int_val();
}


void PropertySet::set_int_value(PropertyId prop_id, int val)
{
	TWIST_CHECK_INVARIANT
	set_value(prop_id, k_prop_type_int, val);
}


double PropertySet::get_float_value(PropertyId prop_id) const
{
	TWIST_CHECK_INVARIANT
	return get_value(prop_id).float_val();
}


void PropertySet::set_float_value(PropertyId prop_id, double val)
{
	TWIST_CHECK_INVARIANT
	set_value(prop_id, k_prop_type_float, val);
}


std::wstring PropertySet::get_string_value(PropertyId prop_id) const
{
	TWIST_CHECK_INVARIANT
	return get_value(prop_id).str_val();
}


void PropertySet::set_string_value(PropertyId prop_id, std::wstring val)
{
	TWIST_CHECK_INVARIANT
	set_value(prop_id, k_prop_type_string, val);
}


bool PropertySet::get_bool_value(PropertyId prop_id) const
{
	TWIST_CHECK_INVARIANT
	return get_value(prop_id).bool_val();
}


void PropertySet::set_bool_value(PropertyId prop_id, bool val)
{
	TWIST_CHECK_INVARIANT
	set_value(prop_id, k_prop_type_bool, val);
	TWIST_CHECK_INVARIANT
}


bool PropertySet::prop_exists(PropertyId prop_id) const
{
	TWIST_CHECK_INVARIANT
	return prop_values_.count(prop_id) != 0;
}


PropertyType PropertySet::get_prop_type(PropertyId prop_id) const
{
	TWIST_CHECK_INVARIANT
	const PropertySet::PropValue& prop_val = get_value(prop_id);
	return prop_val.type();
}


std::map<PropertyId, PropertyType> PropertySet::get_props() const
{
	TWIST_CHECK_INVARIANT
	std::map<PropertyId, PropertyType> props;
	transform(prop_values_, asso_inserter(props), [this](const auto& el) {
		return std::make_pair(el.first, get_prop_type(el.first));
	});

	return props;
}


const PropertySet::PropValue& PropertySet::get_value(PropertyId prop_id) const
{
	TWIST_CHECK_INVARIANT
	auto it = prop_values_.find(prop_id);
	if (it == end(prop_values_)) {
		TWIST_THROW(L"A property with ID %d is not defined.", prop_id.value());
	}
	return it->second;
}


#ifdef _DEBUG
void PropertySet::check_invariant() const noexcept
{
}
#endif


//
//   PropertySet::PropValue  class
//

PropertySet::PropValue::PropValue(int val)
	: type_(k_prop_type_int)
	, int_val_(val)
{
	TWIST_CHECK_INVARIANT
}


PropertySet::PropValue::PropValue(double val)
	: type_(k_prop_type_float)
	, float_val_(val)
{
	TWIST_CHECK_INVARIANT
}


PropertySet::PropValue::PropValue(std::wstring val)
	: type_(k_prop_type_string)
	, str_val_(new std::wstring(val))
{
	TWIST_CHECK_INVARIANT
}


PropertySet::PropValue::PropValue(bool val)
	: type_(k_prop_type_bool)
	, bool_val_(val)
{
	TWIST_CHECK_INVARIANT
}


PropertySet::PropValue::~PropValue()
{
	TWIST_CHECK_INVARIANT
	if (type_ == k_prop_type_string) {
		TWIST_DELETE(str_val_);
	}
}


PropertySet::PropValue::PropValue(const PropertySet::PropValue& src)
	: type_(src.type_)
{
	if (type_ == k_prop_type_int) {
		int_val_ = src.int_val_;
	}
	else if (type_ == k_prop_type_float) {
		float_val_ = src.float_val_;
	}
	else if (type_ == k_prop_type_string) {
		str_val_ = new std::wstring(*src.str_val_);
	}
	else if (type_ == k_prop_type_bool) {
		bool_val_ = src.bool_val_;
	}
	TWIST_CHECK_INVARIANT
}


PropertySet::PropValue::PropValue(PropertySet::PropValue&& src)
	: type_(src.type_)
{
	if (type_ == k_prop_type_int) {
		int_val_ = src.int_val_;
	}
	else if (type_ == k_prop_type_float) {
		float_val_ = src.float_val_;
	}
	else if (type_ == k_prop_type_string) {
		str_val_ = src.str_val_;  // No need to create a new std::wstring on the heap, we just steal the pointer from the source 
		src.str_val_ = nullptr;
	}
	else if (type_ == k_prop_type_bool) {
		bool_val_ = src.bool_val_;
	}
	TWIST_CHECK_INVARIANT
}


PropertySet::PropValue& PropertySet::PropValue::operator=(const PropertySet::PropValue& rhs)
{
	TWIST_CHECK_INVARIANT
	if (this != &rhs) {
		type_ = rhs.type_;
		if (type_ == k_prop_type_int) {
			int_val_ = rhs.int_val_;
		}
		else if (type_ == k_prop_type_float) {
			float_val_ = rhs.float_val_;
		}
		else if (type_ == k_prop_type_string) {
			delete str_val_;
			str_val_ = new std::wstring(*rhs.str_val_);
		}
		else if (type_ == k_prop_type_bool) {
			bool_val_ = rhs.bool_val_;
		}
	}
	TWIST_CHECK_INVARIANT
	return *this;
}


PropertySet::PropValue& PropertySet::PropValue::operator=(PropertySet::PropValue&& rhs)
{
	TWIST_CHECK_INVARIANT
	if (this != &rhs) {
		type_ = rhs.type_;
		if (type_ == k_prop_type_int) {
			int_val_ = rhs.int_val_;
		}
		else if (type_ == k_prop_type_float) {
			float_val_ = rhs.float_val_;
		}
		else if (type_ == k_prop_type_string) {
			delete str_val_;
			str_val_ = rhs.str_val_;  // No need to create a new std::wstring on the heap, we just steal the pointer from the right-hand side object 
			rhs.str_val_ = nullptr;
		}
		else if (type_ == k_prop_type_bool) {
			bool_val_ = rhs.bool_val_;
		}
	}
	TWIST_CHECK_INVARIANT
	return *this;
}


PropertyType PropertySet::PropValue::type() const
{
	TWIST_CHECK_INVARIANT
	return type_;
}


int PropertySet::PropValue::int_val() const
{
	TWIST_CHECK_INVARIANT
	if (type_ != k_prop_type_int) {
		TWIST_THROW(L"The property type is not \"integer\".");
	}
	return int_val_;
}


double PropertySet::PropValue::float_val() const
{
	TWIST_CHECK_INVARIANT
	if (type_ != k_prop_type_float) {
		TWIST_THROW(L"The property type is not \"floating-point number\".");
	}
	return float_val_;
}


std::wstring PropertySet::PropValue::str_val() const
{
	TWIST_CHECK_INVARIANT
	if (type_ != k_prop_type_string) {
		TWIST_THROW(L"The property type is not \"string\".");
	}
	return *str_val_;
}


bool PropertySet::PropValue::bool_val() const
{
	TWIST_CHECK_INVARIANT
	if (type_ != k_prop_type_bool) {
		TWIST_THROW(L"The property type is not \"boolean\".");
	}
	return bool_val_;
}


#ifdef _DEBUG
void PropertySet::PropValue::check_invariant() const noexcept
{
	assert(type_ >= k_prop_type_int && type_ <= k_prop_type_bool);
}
#endif

} // namespace twist



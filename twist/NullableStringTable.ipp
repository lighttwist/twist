///  @file  NullableStringTable.ipp
///  Inline implementation file for "NullableStringTable.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

namespace twist {

template<typename Chr>
NullableStringTable<Chr>::NullableStringTable(Ssize num_cols)
	: rows_{}
	, num_cols_{ num_cols }
{
	TWIST_CHECK_INVARIANT
}


template<typename Chr>
Ssize NullableStringTable<Chr>::nof_rows() const
{
	TWIST_CHECK_INVARIANT
	return ssize(rows_);
}


template<typename Chr>
Ssize NullableStringTable<Chr>::nof_columns() const
{
	TWIST_CHECK_INVARIANT
	return num_cols_;
}


template<typename Chr>
const Chr* NullableStringTable<Chr>::get_cell(Ssize row, Ssize col) const
{
	TWIST_CHECK_INVARIANT
	if (row >= nof_rows()) {
		TWIST_THROW(L"Invalid row index %d.", row);
	}
	if (col >= num_cols_) {
		TWIST_THROW(L"Invalid column index %d.", col);
	}	

	return get_cell(rows_[row][col]);
}


template<typename Chr>
void NullableStringTable<Chr>::set_cell(Ssize row, Ssize col, const Chr* data)
{
	TWIST_CHECK_INVARIANT
	if (row >= nof_rows()) {
		TWIST_THROW(L"Invalid row index %d.", row);
	}
	if (col >= num_cols_) {
		TWIST_THROW(L"Invalid column index %d.", col);
	}
	Cell& cell = rows_[row][col];

	cell.string.clear();
	if (data == nullptr) {		
		cell.is_null = true;
	}
	else {
		cell.string = data;
		cell.is_null = false;
	}
}


template<typename Chr>
std::vector<const Chr*> NullableStringTable<Chr>::get_row_cells(Ssize row) const
{
	TWIST_CHECK_INVARIANT
	if (row >= nof_rows()) {
		TWIST_THROW(L"Invalid row index %d.", row);
	}

	std::vector<const Chr*> cells(num_cols_);
	for (auto j = 0; j < num_cols_; ++j) {
		cells[j] = get_cell(row, j);
	}

	return cells;
}


template<typename Chr>
Ssize NullableStringTable<Chr>::add_row()
{
	TWIST_CHECK_INVARIANT
	rows_.push_back(Row());
	auto& new_row = rows_.back();
	
	for (auto j = 0; j < num_cols_; ++j) {
		new_row.push_back(Cell());
	}
	return ssize(rows_) - 1;
}


template<typename Chr>
const Chr* NullableStringTable<Chr>::get_cell(const Cell& cell) const
{
	TWIST_CHECK_INVARIANT
	return !cell.is_null ? cell.string.data() : nullptr;
}


#ifdef _DEBUG
template<typename Chr>
void NullableStringTable<Chr>::check_invariant() const noexcept
{
	assert(num_cols_ > 0);
}
#endif 

} 

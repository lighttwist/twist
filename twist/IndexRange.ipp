/// @file IndexRange.ipp
/// Inline implementation file for "IndexRange.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace twist {

// --- IndexRange::Iterator class ---

template<std::integral T>
IndexRange<T>::Iterator::Iterator() 
	: value_{std::numeric_limits<T>::max()} 
{
}

template<std::integral T>
IndexRange<T>::Iterator::Iterator(T value) 
	: value_{value} 
{
}

template<std::integral T>
auto IndexRange<T>::Iterator::operator*() const -> reference 
{ 
	return value_; 
} 

template<std::integral T>
auto IndexRange<T>::Iterator::operator->() const -> pointer 
{ 
	return &value_; 
}

template<std::integral T>
auto IndexRange<T>::Iterator::operator++() -> Iterator& 
{
	++value_; 
	return *this;	
}

template<std::integral T>
auto IndexRange<T>::Iterator::operator++(int) -> Iterator 
{
	Iterator temp{*this};
	++(*this);
	return temp;	
}

template<std::integral T>
auto IndexRange<T>::Iterator::operator--() -> Iterator& 
{
	--value_; 
	return *this;	
}

template<std::integral T>
auto IndexRange<T>::Iterator::operator--(int) -> Iterator 
{
	Iterator temp{*this};
	--(*this);
	return temp;	
}

// --- IndexRange class ---

template<std::integral T>
IndexRange<T>::IndexRange(T lo, T hi) 
	: lo_{lo}
	, hi_{hi}
{
	check_bounds();
	TWIST_CHECK_INVARIANT
}
	
template<std::integral T>
IndexRange<T>::IndexRange(T hi) 
	: lo_{0}
	, hi_{hi}
{
	check_bounds();
	TWIST_CHECK_INVARIANT
}

template<std::integral T>
auto IndexRange<T>::begin() const -> Iterator
{
	TWIST_CHECK_INVARIANT
	return Iterator{lo_};
}

template<std::integral T>
auto IndexRange<T>::end() const -> Iterator
{
	TWIST_CHECK_INVARIANT
	return Iterator{hi_};
}

template<std::integral T>
auto IndexRange<T>::hi() const -> T
{
	TWIST_CHECK_INVARIANT
	return hi_;
} 
	
template<std::integral T>
auto IndexRange<T>::lo() const -> T
{
	TWIST_CHECK_INVARIANT
	return lo_;
}
				
template<std::integral T>
auto IndexRange<T>::check_bounds() const -> void 
{
	if (lo_ > hi_) {
		TWIST_THRO2(L"Invalid index range bounds {}, {}.", lo_, hi_);
	}
}

#ifdef _DEBUG
template<std::integral T>
auto IndexRange<T>::check_invariant() const noexcept -> void
{
	assert(lo_ <= hi_);
}
#endif

// --- Non-member functions ---

template<std::integral T>
[[nodiscard]] auto size(IndexRange<T> range) -> Ssize
{
	return range.hi() - range.lo();
}

template<std::integral T>
[[nodiscard]] auto includes(IndexRange<T> range1, IndexRange<T> range2) -> bool
{			
	return range1.lo() <= range2.lo() && range2.hi() <= range1.hi();
}

template<std::integral T>
[[nodiscard]] auto contains(IndexRange<T> range, T point) -> bool 
{
	return range.lo() <= point && point < range.hi();
}

} 

/// @file Singleton.hpp
/// Singleton  class template

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_SINGLETON_HPP
#define TWIST_SINGLETON_HPP

#include <memory>

namespace twist {

/*! Template for the abstract base class of classes which implement the "Singleton" pattern.
    The class is not safe to use between different DLLs (as each client DLL gets a different copy of the static data 
    member). 
    \tparam T  The derived, singleton class
 */
template<class T>
class Singleton {
public:
	/*! Initialise the singleton object.
	    An exception is thrown if the singleton hasn't already been initialised (and not terminated).
	    \tparam Args  T constructor argument types 
	    \param[in] args  T constructor arguments (they must be ordered the same as they are in the T constructor, which 
		                 needs to be visible from this class (typically T will declare Singleton<T> as a friend)
	 */
	template<typename... Args> 
	static auto init(Args... args) -> void;

	/// Terminate the singleton object.
	/// An exception is thrown if the singleton hasn't been initialised (or has been terminated since).
	///
	static void term();

	/// Get the singleton object. An exception is thrown if it has not been created.
	///
    /// @return  The object
	///
	static T& get(); 

	/// Check whether the singleton object currently exists.
	///
    /// @return  true if it does
	///
	static bool exists();

protected:
	/// Constructor.
	/// An exception is thrown if the singleton object has already been created.
	///
	Singleton();

	/// Destructor.
	///
	virtual ~Singleton() = 0;

	/// Set the singleton object.
	/// An exception is thrown if the singleton object has already been set.
	///
	/// @param[in] singleton  The singleton object
	///
	static void set_singleton(std::unique_ptr<T> singleton);

	/// Reset the singleton object.
	/// An exception is thrown if the singleton object has not been set.
	///
	static void reset_singleton(); 

private:
	static std::unique_ptr<T>  singleton__;

	TWIST_NO_COPY_NO_MOVE(Singleton)
};

} 

#include "Singleton.ipp"

#endif 

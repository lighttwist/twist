///  @file  Date.cpp
///  Implementation file for "Date.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "Date.hpp"

#include "chrono_utils.hpp"

using gsl::not_null;
using namespace std::chrono;

namespace twist {

static int min_year = 1;
static int max_year = 9999;
static int kMinDOW = 1;
static int kMaxDOW = 7;

static const wchar_t* dow_names[] = { L"Sunday", L"Monday", L"Tuesday", L"Wednesday", L"Thursday", 
		L"Friday", L"Saturday" };

static const wchar_t* month_names[] = { L"January", L"February", L"March", L"April", L"May", L"June", 
		L"July", L"August", L"September", L"October", L"November", L"December" }; 

static const wchar_t* month_short_names[] = { L"Jan", L"Feb", L"Mar", L"Apr", L"May", L"Jun", L"Jul", 
		L"Aug", L"Sep", L"Oct", L"Nov", L"Dec" }; 

static int month_day_counts[] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

//
//  Local functions
//

void stream_day_month(int x, bool padded, std::wstring_view suffix, std::wstringstream& stream) 
{
	if (padded && x < 10) {
		stream << L"0";
	}
	stream << x << suffix;
}


template<class SmallerInt> 
SmallerInt to_smaller_int(int x)
{
	if (x < 0 || x > std::numeric_limits<SmallerInt>::max()) {
		TWIST_THROW(L"Value %d cannot be stored as a smaller date/time value.", x);
	}
	return static_cast<SmallerInt>(x);
}


int count_days_in_month(int month, int year) 
{
	if (month != 2 || !is_leap_year(year)) {
		return month_day_counts[ month - 1 ];
	} 
	return 29;
}


bool check_valid_date(int day, int month, int year, bool throw_if_invalid = true)
{
	bool ret = false;

	if (year >= min_year && year <= max_year) {
		if (month >= 1 && month <= 12) {

			if ((day >= 1 && day <= count_days_in_month(month, year))	|| 
				 (day == 29 && month == 2 && is_leap_year(year))) {
				
				if (year != 1752 || month != 9 || day == 1 || day >= 14) {
					// Switch to the Gregorian calendar in England, the 2nd to the 13th of September didn't 
					// exist that year
					ret = true;
				}
			}
		}
	}
	if (!ret && throw_if_invalid) {
		TWIST_THROW(L"Invalid date %d-%d-%d.", day, month, year);
	}
	return ret;
}

// Returns number of days since civil 1970-01-01.  Negative values indicate days prior to 1970-01-01.
// Preconditions:  y-m-days_since_epoch represents a date in the civil (Gregorian) calendar
//                 m is in [1, 12]
//                 days_since_epoch is in [1, last_day_of_month(y, m)]
//                 y is "approximately" in
//                   [numeric_limits<int>::min()/366, numeric_limits<int>::max()/366]
//                 Exact range of validity is:
//                 [civil_from_days(numeric_limits<int>::min()),
//                  civil_from_days(numeric_limits<int>::max()-719468)]
constexpr int days_from_civil(int y, unsigned m, unsigned d) noexcept
{
	// Based on code by Howard Hinnant, in stackoverflow answer
	// https://stackoverflow.com/questions/16773285/how-to-convert-stdchronotime-point-to-stdtm-without-using-time-t

    y -= m <= 2;
    const int era = (y >= 0 ? y : y-399) / 400;
    const unsigned yoe = static_cast<unsigned>(y - era * 400);      // [0, 399]
    const unsigned doy = (153*(m + (m > 2 ? -3 : 9)) + 2)/5 + d-1;  // [0, 365]
    const unsigned doe = yoe * 365 + yoe/4 - yoe/100 + doy;         // [0, 146096]
    return era * 146097 + static_cast<int>(doe) - 719468;
}

//
//  Date  class
//

Date::Date() 
	: day_{ 1 }
	, month_{ 1 }
	, year_{ 1 }
{
	TWIST_CHECK_INVARIANT
}


Date::Date(int day, int month, int year) 
	: day_{ to_tiny_int(day) }
	, month_{ to_tiny_int(month) }
	, year_{ to_small_int(year) }
{
	check_valid_date(day, month, year);
	TWIST_CHECK_INVARIANT
}


Date::Date(const Date& src) 
	: day_{ src.day_ }
	, month_{ src.month_ }
	, year_{ src.year_ }
{
	TWIST_CHECK_INVARIANT
}


Date::~Date()
{
	TWIST_CHECK_INVARIANT
}


Date& Date::operator=(const Date& rhs)
{
	TWIST_CHECK_INVARIANT
	if (this != &rhs) {
		day_	= rhs.day_;
		month_	= rhs.month_;
		year_	= rhs.year_;
	}
	return *this;
}


int Date::day() const
{
	TWIST_CHECK_INVARIANT
	return day_;
}


int Date::month() const
{
	TWIST_CHECK_INVARIANT
	return month_;
}


int Date::year() const
{
	TWIST_CHECK_INVARIANT
	return year_;
}


bool Date::is_leap_year() const
{
	TWIST_CHECK_INVARIANT
	return twist::is_leap_year(year_);
}


bool Date::tomorrow()
{
	TWIST_CHECK_INVARIANT
	bool ret = true;

	if (max_year == year_ && 12 == month_ && 31 == day_) {		
		ret = false;
	}
	else if (1752 == year_ && 9 == month_ && 1 == day_) {
		// Gregorian calendar change
		day_ = 14;
	}
	else if (day_ < count_days_in_month(month_, year_)) {
		day_++;
	} 
	else if (month_ < 12) {
		month_++;
		day_ = 1;
	} 
	else {
		year_++;
		month_ = 1;
		day_ = 1;
	}

	return ret;
}


bool Date::yesterday()
{
	TWIST_CHECK_INVARIANT
	bool ret = true;
	
	if (min_year == year_ && 1 == month_ && 1 == day_) {
		ret = false;
	}
	else if (1752 == year_ && 9 == month_ && 14 == day_) {
		// Gregorian calendar change.
		day_ = 1;		
	}
	else if (day_ > 1) {
		day_--;
	} 
	else if (month_ > 1) {
		month_--;
		day_ = to_tiny_int(count_days_in_month(month_, year_));
	} 
	else {
		year_--;
		month_ = 12;
		day_ = 31;
	}

	return ret;
}


Date::TinyInt Date::to_tiny_int(int x)
{
	return to_smaller_int<TinyInt>(x);
}


Date::SmallInt Date::to_small_int(int x)
{
	return to_smaller_int<SmallInt>(x);
}


#ifdef _DEBUG
void Date::check_invariant() const noexcept
{
	assert(day_ > 0);
	assert(month_ > 0);
	assert(year_ > 0);
}
#endif 

//
//  Free functions
//

bool operator==(const Date& lhs, const Date& rhs)
{
	return lhs.day() == rhs.day() && lhs.month() == rhs.month() && lhs.year() == rhs.year();
}


bool operator!=(const Date& lhs, const Date& rhs)
{
	return !(lhs == rhs);
}


bool operator<(const Date& lhs, const Date& rhs)
{
	if (lhs.year() > rhs.year()) {
		return false;
	} 
	if (lhs.year() == rhs.year()) {
		if (lhs.month() > rhs.month()) {
			return false;
		} 
		if (lhs.month() == rhs.month()) {
			if (lhs.day() >= rhs.day()) {
				return false;
			}
		}
	}
	return true;
}


bool operator<=(const Date& lhs, const Date& rhs)
{
	return lhs < rhs || lhs == rhs;
}


bool operator>(const Date& lhs, const Date& rhs)
{
	if (lhs.year() < rhs.year()) {
		return false;
	} 
	if (lhs.year() == rhs.year()) {
		if (lhs.month() < rhs.month()) {
			return false;
		} 
		if (lhs.month() == rhs.month()) {
			if (lhs.day() <= rhs.day()) {
				return false;
			}
		}
	}
	return true;
}


bool operator>=(const Date& lhs, const Date& rhs)
{
	return lhs > rhs || lhs == rhs;
}


bool same_date(const Date& date1, const Date& date2)
{
	return date1 == date2;
}


Date get_system_date()
{
	const auto now_time = system_clock::to_time_t(system_clock::now());
	const auto now_time_info = not_null{ localtime(&now_time) };

	return {now_time_info->tm_mday,
			now_time_info->tm_mon + 1,
			now_time_info->tm_year + 1900};
}


int get_dow(const Date& date)
{
	const int diff = get_diff_in_days({1, 1, 1970}, date);  

	return abs(diff >= -4 ? (diff + 4) % 7 : (diff + 5) % 7 + 6);
}


std::wstring get_dow_name(const Date& date)
{
	return dow_names[get_dow(date)];
}


std::wstring get_month_name(const Date & date)
{
	return month_names[date.month() - 1];
}


std::wstring get_month_short_name(const Date & date)
{
	return month_short_names[date.month() - 1];
}

auto get_diff_in_days(const Date& date1, const Date& date2) -> int
{
	return date_to_days_since_unix_epoch(date2) - date_to_days_since_unix_epoch(date1);
}

Date add_days(const Date& date, int days)
{	//+TODO: Adding days, using this method, through the Gregorian change, is problematic; while repeated 
	//       calls to tomorrow() work fine
	auto days_since_epoch = date_to_days_since_unix_epoch(date);
	return days_since_unix_epoch_to_date(days_since_epoch + days);
}


Date add_months(const Date& date, int months)
{	//+TODO: Adding months, using this method, throught the Gregorian change, is problematic; while 
	//       repeated calls to tomorrow() work fine
	auto [quot, rem] = div((date.month() - 1) + months, 12);
	if (rem < 0) {
		rem = rem + 12;
	}
	return {date.day(), rem + 1, date.year() + quot};
}


Date add_years(const Date& date, int years)
{
	int new_year = date.year() + years;
	if (new_year < min_year || max_year < new_year) {
		TWIST_THROW(L"The resulting date is outside the bounds of valid dates.");
	}
	return {date.day(), date.month(), new_year};
}


int get_julian_day_number(const Date& date) 
{
	const int year = date.year();
	const int month = date.month();
	const int day = date.day();

    int a = (14 - month) / 12;
    int y = year + 4800 - a;
    int m = month + 12 * a - 3;
    
    int jdn = 0;
    
    if ((year > 1582) || (year == 1582 && month > 10) || 
			(year == 1582 && month == 10 && day < 15)) {
        
        jdn = day + (153 * m + 2) / 5 + 365 * y + y / 4 - y / 100 + y / 400 - 32045;
    } 
	else {        
        jdn = day + (153 * m + 2) / 5 + 365 * y + y / 4 - 32045;
    }

    return jdn;
}


int date_to_days_since_unix_epoch(const Date& date)
{
	return days_from_civil(date.year(), date.month(), date.day());
}


Date days_since_unix_epoch_to_date(int days)
{
	// Based on code by Howard Hinnant, in stackoverflow answer
	// https://stackoverflow.com/questions/16773285/how-to-convert-stdchronotime-point-to-stdtm-without-using-time-t

	// Preconditions:  days is number of days since 1970-01-01 and is in the range:
	//                   [numeric_limits<int>::min(), numeric_limits<int>::max() - 719468]

    days += 719468;
    const int era = (days >= 0 ? days : days - 146096) / 146097;
    const unsigned doe = static_cast<unsigned>(days - era * 146097);             // [0, 146096]
    const unsigned yoe = (doe - doe / 1460 + doe / 36524 - doe / 146096) / 365;  // [0, 399]
    const int y = static_cast<int>(yoe) + era * 400;
    const unsigned doy = doe - (365 * yoe + yoe / 4 - yoe / 100);                // [0, 365]
    const unsigned mp = (5 * doy + 2) / 153;                                     // [0, 11]
    const unsigned d = doy - (153 * mp + 2) / 5 + 1;                             // [1, 31]
    const unsigned m = mp + (mp < 10 ? 3 : -9);                                  // [1, 12]
    return { static_cast<int>(d), static_cast<int>(m), y + (m <= 2) };	
}


std::chrono::system_clock::time_point date_to_utc_timepoint(const Date& date)
{
	// Compute the number of days since 1Jan1970 00:00:00 to the date passed in
	const auto days_since_epoch = date_to_days_since_unix_epoch(date);

	// Add the days to 1Jan1970 00:00:00, to get a timepoint with the date passed in
	std::chrono::system_clock::time_point timept{};  // Initialised to 1Jan1970 00:00:00
	timept += DurationDays{ days_since_epoch };

	return timept;
}


Date utc_timepoint_to_date(const std::chrono::system_clock::time_point& timept)
{
	// Based on code by Howard Hinnant, in stackoverflow answer
	// https://stackoverflow.com/questions/16773285/how-to-convert-stdchronotime-point-to-stdtm-without-using-time-t

	using namespace std::chrono;

    // Compute time duration since 1Jan1970 00:00:00 (the "system clock" epoch)
    auto time_since_epoch = timept.time_since_epoch();

    // Compute the number of days since 1Jan1970 00:00:00
    const auto days_since_epoch = floor<DurationDays>(time_since_epoch);

    // time_since_epoch is now time duration since midnight of day days_since_epoch
    time_since_epoch -= days_since_epoch;

    return days_since_unix_epoch_to_date(days_since_epoch.count());
}


std::vector<Date> get_dates_in_year(int year)
{
	const int date_count = is_leap_year(year) ? 366 : 365;
	std::vector<Date> dates(date_count);

	Date date{1, 1, year};
	for (int i = 0; i < date_count; ++i) {
		dates[i] = date;
		date = add_days(date, 1);
	}

	return dates;
}


std::wstring date_to_str(const Date& date, DateStrFormat format, const std::wstring& sep)
{
	static const auto allowed_formats = {
			DateStrFormat::dd_mm_yyyy, DateStrFormat::d_m_yyyy, DateStrFormat::yyyy_mm_dd};
	if (!has(allowed_formats, format)) {
		TWIST_THROW(L"Only the formats 'dd_mm_yyyy', 'd_m_yyyy' and 'yyyy_mm_dd' are allowed by this function.");
	} 

	const bool padded = format == DateStrFormat::dd_mm_yyyy || format == DateStrFormat::yyyy_mm_dd;
	std::wstringstream stream;
	auto add_day_month = [&](auto x, const auto& sep) { stream_day_month(x, padded, sep, stream); }; 

	if (format == DateStrFormat::dd_mm_yyyy || format == DateStrFormat::d_m_yyyy) {
		add_day_month(date.day(), sep);
		add_day_month(date.month(), sep);
		stream << date.year();
	}
	else {
		stream << date.year() << sep;
		add_day_month(date.month(), sep);
		add_day_month(date.day(), L"");
	}
	return stream.str();
}


std::wstring date_to_str(const Date& date, DateStrFormat format)
{
	static const auto allowed_formats = {DateStrFormat::dayth_month_year, DateStrFormat::day_mon_year, 
			DateStrFormat::dow_day_month_year, DateStrFormat::dow_day_month, DateStrFormat::yyyymmdd};
	if (!has(allowed_formats, format)) {
		TWIST_THROW(L"Only the formats 'dayth_month_year', 'day_mon_year', 'dow_day_month_year', 'dow_day_month' and 'yyyymmdd' are allowed by this function.");
	} 

	std::wstringstream stream;

	if (format == DateStrFormat::yyyymmdd) {
		stream << date.year();
		stream_day_month(date.day(), true/*padded*/, L"", stream);
		stream_day_month(date.month(), true/*padded*/, L"", stream);
		return stream.str();
	}

	// Add the day of week
	if (DateStrFormat::dow_day_month_year == format || DateStrFormat::dow_day_month == format) {
		stream << get_dow_name(date) << " ";
	}
	
	// Add the day of month
	stream << date.day(); 

	if (format != DateStrFormat::day_mon_year) {
		const auto div_ten_result = div(static_cast<long>(date.day()), static_cast<long>(10));
		if ((div_ten_result.rem == 1) && (date.day() != 11)) {
			stream << L"st";
		} 
		else if ((div_ten_result.rem == 2) && (date.day() != 12)) {
			stream << L"nd";
		} 
		else if ((div_ten_result.rem == 3) && (date.day() != 13)) {
			stream << L"rd";
		} 
		else {
			stream << L"th";
		}
	}

	// Add the month
	if (DateStrFormat::dow_day_month_year == format || DateStrFormat::dow_day_month == format) {
		stream << " of";
	}
	stream << " " << (format != DateStrFormat::day_mon_year 
			? std::wstring(month_names[date.month() - 1]) 
			: std::wstring(month_names[date.month() - 1], 3));
	
	if (format == DateStrFormat::dayth_month_year || format == DateStrFormat::dow_day_month_year ||
			format == DateStrFormat::day_mon_year) {
		// Add the year
		stream << " " << date.year();
	}

	return stream.str();
}


Date date_from_str(std::wstring_view str, DateStrFormat format, std::wstring_view sep)
{
	static const auto allowed_formats = {
			DateStrFormat::dd_mm_yyyy, DateStrFormat::d_m_yyyy, DateStrFormat::yyyy_mm_dd};

	if (!has(allowed_formats, format)) {
		TWIST_THROW(L"This function can only be called if the text follows one of the following formats: dd_mm_yyyy, d_m_yyyy, yyyy_mm_dd.");
	}

	// First check that the string is well formed.
	const size_t sep_len = sep.size();
	if (sep_len == 0) {
		TWIST_THROW(L"The separator cannot be empty.");
	}
	if ((format == DateStrFormat::dd_mm_yyyy || format == DateStrFormat::yyyy_mm_dd) && 
			str.size() != 8 + 2 * sep_len) {
		TWIST_THROW(L"The date string \"%s\" is ill-formed: it has the wrong length.", 
				std::wstring{ str }.c_str());
	}
	else if (str.size() < 6 + 2 * sep_len || str.size() > 8 + 2 * sep_len) {
		TWIST_THROW(L"The date string \"%s\" is ill-formed: it has the wrong length.", 
				std::wstring{ str }.c_str());
	}
	const auto sep_positions = find_all_substr(str, sep);
	if (sep_positions.size() != 2) {
		TWIST_THROW(L"The date string \"%s\" is ill-formed: the separator appears %d times; "
				"it is expected twice.", std::wstring{ str }.c_str(), sep_positions.size());
	}

	std::wstring day_str;
	std::wstring month_str;
	std::wstring year_str;
	if (format == DateStrFormat::dd_mm_yyyy || format == DateStrFormat::d_m_yyyy) {

		if ((format == DateStrFormat::dd_mm_yyyy && 
				(sep_positions[0] != 2 || sep_positions[1] != 4 + sep_len)) ||
		    (format == DateStrFormat::d_m_yyyy && 
				(sep_positions[0] < 1 || sep_positions[0] > 2 || str.size() - sep_positions[1] != 5))) {
			TWIST_THROW(L"The date string \"%s\" is ill-formed: separator appears in the wrong place(s).", 
					std::wstring{ str }.c_str());				
		}

		day_str   = str.substr(0, sep_positions[0]);
		month_str = str.substr(sep_positions[0] + sep_len, sep_positions[1] - sep_positions[0] - sep_len);
		year_str  = str.substr(sep_positions[1] + sep_len);
	} 
	else if (format == DateStrFormat::yyyy_mm_dd) {
		if (sep_positions[0] != 4 || sep_positions[1] != 6 + sep_len) {
			TWIST_THROW(L"The date string \"%s\" is ill-formed: separator appears in the wrong place(s).", 
					std::wstring{ str }.c_str());
		}
		year_str  = str.substr(0, sep_positions[0]);
		month_str = str.substr(sep_positions[0] + sep_len, sep_positions[1] - sep_positions[0] - sep_len);
		day_str   = str.substr(sep_positions[1] + sep_len);
	}

	return { to_int_checked(day_str), to_int_checked(month_str), to_int_checked(year_str) };
}


Date date_from_str(const std::wstring& str, DateStrFormat format)
{
	if (format != DateStrFormat::yyyymmdd) TWIST_THROW(L"Only format 'yyyymmdd' is allowed for this function.");
	if (str.size() != 8) TWIST_THROW(L"Invalid date string \"%s\": Wrong length.");

	size_t pos = 0;

	const int year = stoi(str.substr(0, 4), &pos);
	if (pos != 4) TWIST_THROW(L"Invalid date string \"%s\": invalid year substring.", str.c_str());

	const int month = stoi(str.substr(4, 2), &pos);
	if (pos != 2) TWIST_THROW(L"Invalid date string \"%s\": invalid month substring.", str.c_str());

	const int day = stoi(str.substr(6, 2), &pos);
	if (pos != 2) TWIST_THROW(L"Invalid date string \"%s\": invalid day substring.", str.c_str());

	return {day, month, year};
}

} 


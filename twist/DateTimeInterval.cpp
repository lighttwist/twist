/// @file DateTimeInterval.cpp
/// Implementation file for "DateTimeInterval.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/DateTimeInterval.hpp"

namespace twist {

DateTimeInterval::DateTimeInterval(const DateTime& lo, const DateTime& hi)
	: lo_{lo}
	, hi_{hi}
{
	if (lo_ > hi_) {
		TWIST_THRO2(L"Invalid datetime interval [{}, {}].", 
		            date_time_to_str(lo_, DateTimeStrFormat::dd_mm_yyyy_hh_mm_ss),
					date_time_to_str(hi_, DateTimeStrFormat::dd_mm_yyyy_hh_mm_ss));
	}
	TWIST_CHECK_INVARIANT
}

auto DateTimeInterval::lo() const -> DateTime
{
	TWIST_CHECK_INVARIANT
	return lo_;
}

auto DateTimeInterval::hi() const -> DateTime
{
	TWIST_CHECK_INVARIANT
	return hi_;
}

#ifdef _DEBUG
void DateTimeInterval::check_invariant() const noexcept
{
	assert(lo_ <= hi_);
}
#endif 

// --- Non-member functions ---

auto operator==(const DateTimeInterval& lhs, const DateTimeInterval& rhs) -> bool
{
	return (lhs.lo_ == rhs.lo_) && (lhs.hi_ == rhs.hi_);
}

auto contains(const DateTimeInterval& interval, const DateTime& time) -> bool
{
	return interval.lo() <= time && time <= interval.hi();
}

auto length_hours(const DateTimeInterval& interval) -> int
{
	return diff_hours(interval.lo(), interval.hi());
}

}


///  @file  UuidObjectMap.hpp
///  UuidObjectMap  class

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_UUID_OBJECT_MAP_HPP
#define TWIST_UUID_OBJECT_MAP_HPP

#include "os_utils.hpp"

namespace twist {

///
///  Map providing storage of values keyed by UUID's.
///
template<class Value>
class UuidObjectMap : public NonCopyable {
public:
	using const_iterator = typename std::map<Uuid, Value>::const_iterator;

	/// Constructor.
	///	
	UuidObjectMap();
	
	/// Destructor.
	///
	virtual ~UuidObjectMap();

	/// Insert a new value to the map.
	///
	/// @param[in] value  The value.
	/// @param[in] key  The UUID key. An exception is thrown if this key already exists in the map.
	///
	void insert(Value value, Uuid key);

	/// Try to insert a new value to the map.
	///
	/// @param[in] value  The value.
	/// @param[in] key  The UUID key. Insertion fails if this key already exists in the map.
	/// @return  true if insertion was successful.
	///
	bool try_insert(Value value, Uuid key);

	/// Get an value from the map.
	///
	/// @param[in] key  The UUID key. An exception is thrown if this key does not exist in the map.
	/// @return  The value.
	///
	const Value& get(Uuid key) const;

	/// Find out whether a key exists in the map.
	///
	/// @param[in] key  The UUID key. 
	/// @return  true if it does.
	///
	bool has(Uuid key) const;

	/// Erase an value from the map.
	///
	/// @param[in] key  The UUID key. An exception is thrown if this key does not exist in the map.
	///
	void erase(Uuid key);

	/// Get a constant iterator addressing the first element in the map.
	///
	/// @return  The iterator
	///
	const_iterator begin() const;

	/// Get a constant iterator addressing one past the final element in the map.
	///
	/// @return  The iterator
	///
	const_iterator end() const;

private:	
	std::map<Uuid, Value>  map_;

	TWIST_CHECK_INVARIANT_DECL
};

} 

#include "UuidObjectMap.ipp"

#endif 


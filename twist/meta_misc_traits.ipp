/// @file meta_misc_traits.ipp
/// Inline implementation file for "meta_misc_traits.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include <type_traits>

namespace twist {

namespace detail {

// Selects the appropriate base type (true_type or false_type) to make defining our own predicates easier.
template<bool> 
struct PredBase 
	: std::false_type {};

template<> 
struct PredBase<true> 
	: std::true_type {};

template<class T>
std::add_lvalue_reference_t<std::remove_reference_t<T>> as_lvalue_ref();

// Are the decayed versions of "T" and "O" the same basic type?
// Gets around the fact that std::is_same will treat, say "bool" and "bool&" as
// different types and using std::decay all over the place gets really verbose
template <class T, class O>
struct SameDecayed : PredBase<std::is_same_v<std::decay_t<T>, std::decay_t<O>>> {};

// Is it a number?  i.e. true for floats and integrals but not bool
template<class T>
struct IsNumeric : PredBase<std::is_arithmetic_v<T> && !SameDecayed<bool, T>::value> {};

// Less verbose way to determine if two types both meet a single predicate
template<class A, class B, template<typename> class Pred>
struct Both : PredBase<Pred<A>::value && Pred<B>::value> {};

// Some simple typedefs of both (above) for common conditions
template<class A, class B> 
struct BothNumeric : Both<A, B, IsNumeric> {};  // Are both A and B numeric types?

template<class A, class B> 
struct BothFloating : Both<A, B, std::is_floating_point> {};  // Are both A and B floating point types?

template<class A, class B> 
struct BothIntegral : Both<A, B, std::is_integral> {};  // Are both A and B integral types?

template<class A, class B> 
struct BothSigned : Both<A, B, std::is_signed> {};  // Are both A and B signed types?

template<class A, class B> 
struct BothUnsigned : Both<A, B, std::is_unsigned> {};  // Are both A and B unsigned types?

// Are both number types signed or are they both unsigned
template<class T, class F>
struct SameSignage
    : PredBase<(BothSigned<T, F>::value) || (BothUnsigned<T, F>::value)> {};

template<class From, class To>
struct IsSafeNumericCastImpl : detail::PredBase<
	// From and To must be numerical
	detail::BothNumeric<To, From>::value &&  
	// To is floating-point: From must be integral or smaller/equal float-type
	(std::is_floating_point_v<To> && (std::is_integral_v<From> || sizeof(To) >= sizeof(From))) ||  
	// To is integer: From must be integral and (smaller/equal & same signage) or (smaller & different signage)
	(detail::BothIntegral<To, From>::value &&  
	// Size and signage
	(sizeof(To) > sizeof(From) || (sizeof(To) == sizeof(From) && detail::SameSignage<To, From>::value)))> {};

//  EnableIfEqComparable<> helpers

template<class T, class U, class = std::void_t<>>
struct AreEqComparableImpl
    : std::false_type {};

template<class T, class U>
struct AreEqComparableImpl<T, U, std::void_t<
		decltype(bool{ std::declval<T>() == std::declval<U>() }),
		decltype(bool{ std::declval<U>() == std::declval<T>() })>>
	: std::true_type {};

//  EnableIfIneqComparable<> helpers

template<class T, class U, class = std::void_t<>>
struct AreIneqComparableImpl
    : std::false_type {};

template<class T, class U>
struct AreIneqComparableImpl<T, U, std::void_t<
		decltype(bool{ std::declval<T>() != std::declval<U>() }),
		decltype(bool{ std::declval<U>() != std::declval<T>() })>>
	: std::true_type {};

//  HasPreincrement<> helpers

template<class T, class = std::void_t<>>
struct HasPreincrementImpl 
	: std::false_type {}; 

template<class T>
struct HasPreincrementImpl<T, std::void_t<decltype(++(as_lvalue_ref<T>()))>> 
	: std::true_type {};

//  HasPostincrement<> helpers

template<class T, class = std::void_t<>>
struct HasPostincrementImpl 
	: std::false_type {}; 

template<class T>
struct HasPostincrementImpl<T, std::void_t<decltype((as_lvalue_ref<T>())++)>> 
	: std::true_type {};

//  HasDereference<> helpers

template<class T, class = std::void_t<>>
struct HasDereferenceImpl 
	: std::false_type {}; 

template<class T>
struct HasDereferenceImpl<T, std::void_t<decltype(*std::declval<T>())>> 
	: std::true_type {};

//  EnableIfAnyInputIterator<> helpers

template<class, class = std::void_t<>>
struct IsAnyInputIteratorImpl 
	: std::false_type {};

template<class Iter>
struct IsAnyInputIteratorImpl<Iter, std::void_t<
		std::enable_if_t<HasPreincrementImpl<Iter>::value>,
		std::enable_if_t<HasPostincrementImpl<Iter>::value>,
		std::enable_if_t<HasDereferenceImpl<Iter>::value>,
		std::enable_if_t<AreEqComparableImpl<Iter, Iter>::value>,
		std::enable_if_t<AreIneqComparableImpl<Iter, Iter>::value>,
		decltype( *as_lvalue_ref<Iter>()++ )>>
	: std::true_type {};

//  EnableIfInputIterator<> helpers

template<class, class, class = std::void_t<>>
struct IsInputIteratorImpl 
	: std::false_type {};

template<class Iter, class Val>
struct IsInputIteratorImpl<Iter, Val, std::void_t<
		std::enable_if_t<IsAnyInputIteratorImpl<Iter>::value>,
		decltype( Val{ *std::declval<Iter>() } )>>
	: std::true_type {};

//  EnableIfOutIterator<> helpers

template<class, class, class = std::void_t<>>
struct IsOutIteratorImpl 
	: std::false_type {};

template<class Iter, class Val>
struct IsOutIteratorImpl<Iter, Val, std::void_t<
		std::enable_if_t<HasPreincrementImpl<Iter>::value>,
		decltype( *as_lvalue_ref<Iter>() = std::declval<Val>() )>>
	: std::true_type {};

//  EnableIfHaveLessThan<> helpers

template<class, class, class = std::void_t<>>
struct HaveLessThanImpl 
	: std::false_type {};

template<class T, class U>
struct HaveLessThanImpl<T, U, std::void_t<
		decltype( bool{ std::declval<T>() < std::declval<U>() } ),
		decltype( bool{ std::declval<U>() < std::declval<T>() } )>>
	: std::true_type {};

// NotNullPointee<> helpers

template<class T>
struct NotNullPointeeImpl {};

template<class T>
struct NotNullPointeeImpl<gsl::not_null<T*>> { using type = T; };

// IsConstPointer<> helpers

template<class T>
struct IsConstPointerImpl : std::false_type {};
 
template<class T>
struct IsConstPointerImpl<const T*> : std::true_type {};

// IsSpecialisationOf<> helpers

template<template<typename...> class Template, typename T>
struct IsSpecializationOfImpl : std::false_type{};

template<template<typename...> class Template, typename... Args>
struct IsSpecializationOfImpl<Template, Template<Args...>> : std::true_type{};

}

template<class To, class From>
constexpr bool is_creatable_from()
{
	return std::is_convertible_v<From, To> || std::is_constructible_v<To, From>;
}

}

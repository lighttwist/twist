///  @file  meta_ranges.ipp
///  Inline implementation file for "meta_ranges.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

namespace twist::detail {

// Tell the compiler to consider std::begin() and std::end() when looking up unqualified begin() and end(),
// which is useful as the std functions delegate to begin() and end() class members too.
using std::begin;
using std::end;

// IsAnyRange<> helpers

template<class, class = std::void_t<>>
struct IsAnyRangeImpl 
	: std::false_type {};

template<class Rng>
struct IsAnyRangeImpl<Rng, std::void_t<
			EnableIfAnyInputIterator<decltype(begin(std::declval<Rng>()))>,
			EnableIfSame<decltype(begin(std::declval<Rng>())),
			             decltype(end(std::declval<Rng>()))>>>
	: std::true_type {};

template<class Arr, unsigned long sz>
struct IsAnyRangeImpl<Arr[sz]>
	: std::true_type {};

// IsRangeAndGives<> helpers

template<class, class, class = std::void_t<>>
struct IsRangeAndGivesImpl 
	: std::false_type {};

template<class Rng, class Elem>
struct IsRangeAndGivesImpl<Rng, Elem, std::void_t<
			IsAnyRangeImpl<Rng>,
			decltype(Elem{ *begin(std::declval<Rng>()) })>>
	: std::true_type {};

template<class T, unsigned long sz, class Elem>
struct IsRangeAndGivesImpl<T[sz], Elem, std::void_t<
			decltype(Elem{ std::declval<T>() })>>
	: std::true_type {};

// IsRangeAndTakes<> helpers

template<class, class, class = std::void_t<>>
struct IsRangeAndTakesImpl 
	: std::false_type {};

template<class Rng, class Elem>
struct IsRangeAndTakesImpl<Rng, Elem, std::void_t<
			IsAnyRangeImpl<Rng>,
			decltype( decltype(*begin(std::declval<Rng>())){ std::declval<Elem>() } )>>
	: std::true_type {};

template<class T, unsigned long sz, class Elem>
struct IsRangeAndTakesImpl<T[sz], Elem, std::void_t<
			decltype( T{ std::declval<Elem>() } )>>
	: std::true_type {};

// RangeElement<> helpers

template<class Rng, 
         bool = IsAnyRangeImpl<Rng>::value>
struct RangeElementImpl {
	static_assert(always_false<Rng>, "Rng is not a range.");
};

template<class Rng>
struct RangeElementImpl<Rng, true> {
	using Type = std::decay_t<decltype(*begin(std::declval<Rng>()))>;
};

template<class Arr, unsigned long sz>
struct RangeElementImpl<Arr[sz], true> {
	using Type = Arr; //+TODO: test this!
};

// IsContigContainer<> helpers 

template<class, class = std::void_t<>>
struct IsContigContainerImpl 
	: std::false_type {};

template<class Cont>
struct IsContigContainerImpl<Cont, std::void_t<
			std::enable_if_t<
					IsAnyRangeImpl<Cont>::value>, 
			std::enable_if_t<
					std::is_integral_v<decltype( rg::size(std::declval<Cont>()) )>>,
			decltype( std::data(std::declval<Cont>()) )>>
	: std::true_type {};

// IsContigContainerAndGives<> helpers 

template<class, class, class = std::void_t<>>
struct IsContigContainerAndGivesImpl 
	: std::false_type {};

template<class Cont, class Elem>
struct IsContigContainerAndGivesImpl<Cont, Elem, std::void_t<
			std::enable_if_t<
					IsRangeAndGivesImpl<Cont, Elem>::value>, 
			std::enable_if_t<
					std::is_integral_v<decltype( rg::size(std::declval<Cont>()) )>>,
			decltype( std::add_pointer_t<std::add_const_t<Elem>>{ std::data(std::declval<Cont>()) } )>>
	: std::true_type {};

}

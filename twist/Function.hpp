///  @file  Function.hpp
///  Generalised functor classes
///	    Function classes (functors) that can wrap either a static or a member function with a given 
///         signature.
///		Significantly, when a function class wraps a member function, the type of the class which the function 
///         is a member of is erased (that is, it is not a template argument for the function class). This 
///         means that the user of a functor obj does not need to even know whether they are calling a static 
///         or a member function, let alone the obj type in the latter case. 
///      Type erasure is achieved by using the inner  ImplBase  abstract base class and deriving  MemFuncImpl  
///         from it, which does know the obj type. In the case where a static function is wrapped,  
///         StaticFuncImpl, also derived from ImplBase, is used instead.
///	    The class names are FunctionN where N is the number of arguments of the (wrapped) function.
///		The template arguments are
///		   Ret   The return type
///        ArgN   Type of the Nth argument

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_FUNCTION_HPP
#define TWIST_FUNCTION_HPP

#include <memory>

namespace twist {

///
///  Functor wrapping a static or class member function taking one argument.
///
template<typename Ret, typename Arg1>
class Function1 {
public:
	Function1() 
	{
		reset();
	}

	template<typename StaticFunc>
	Function1(StaticFunc static_func) 
	{
		static_assert(sizeof(StaticFuncImpl) <= k_impl_buf_size, "Implementation object too big.");
		new (pimpl_) StaticFuncImpl(static_func);  // placement new	
	}

	template<typename MemFunc, class Obj> 
	Function1(MemFunc mem_func, Obj& obj) 
	{
		static_assert(sizeof(MemFuncImpl<Obj>) <= k_impl_buf_size, "Implementation object too big.");
		new (pimpl_) MemFuncImpl<Obj>(mem_func, obj);  // placement new
	}

	Ret operator()(Arg1 arg1) 
	{
		if (is_empty()) {
			TWIST_THROW(L"This function object is empty.");
		}
		return reinterpret_cast<ImplBase*>(pimpl_)->call(arg1);
	}

	bool operator==(const Function1& rhs) const 
	{
		return !is_empty() && !rhs.is_empty() && 
				reinterpret_cast<const ImplBase*>(pimpl_)->compare(
						*reinterpret_cast<const ImplBase*>(rhs.pimpl_));
	}

	bool is_empty() const 
	{
		return *pimpl_ == 0;
	}
	
	void reset() 
	{
		*pimpl_ = 0;
	}

private:
	class ImplBase {
	public:
		virtual Ret call(Arg1 arg1) = 0;
		virtual bool compare(const ImplBase& other) const = 0;
	};

	class StaticFuncImpl : public ImplBase {
	public:
		typedef Ret (*StaticFunc)(Arg1);
		
		StaticFuncImpl(StaticFunc static_func) 
			: ImplBase()
			, static_func_(static_func) 
		{
		}
		
		virtual Ret call(Arg1 arg1) 
		{ 
			return (*static_func_)(arg1);		
		}

		virtual bool compare(const ImplBase& other) const 
		{
			const StaticFuncImpl* otherd = dynamic_cast<const StaticFuncImpl*>(&other);
			return (otherd != nullptr) && (static_func_ == otherd->static_func_);
		}
		
	private:
		StaticFunc  static_func_;
	};
	
	template <class Obj> 
	class MemFuncImpl : public ImplBase {
	public:
		typedef Ret (Obj::*MemFunc)(Arg1);
		
		MemFuncImpl(MemFunc mem_func, Obj& obj) 
			: ImplBase() 
			, mem_func_(mem_func)
			, obj_(&obj) 
		{
		}
		
		virtual Ret call(Arg1 arg1) 
		{
			return (obj_->*mem_func_)(arg1);
		}

		virtual bool compare(const ImplBase& other) const 
		{
			const MemFuncImpl* otherd = dynamic_cast<const MemFuncImpl*>(&other);
			return (otherd != nullptr) && (mem_func_ == otherd->mem_func_) && (obj_ == otherd->obj_);
		}

	private:
		MemFunc  mem_func_;
		Obj*  obj_;
	};
	
	static const size_t  k_impl_buf_size = 24;  //+ This may be larger than Visual Studio's "small object optimisation" limit for std::function 
	char  pimpl_[k_impl_buf_size];
};

///
///  Functor wrapping a static or class member function taking two arguments.
///
template<typename Ret, typename Arg1, typename Arg2>
class Function2 {
public:
	Function2()
	{
		reset();
	}

	template<typename StaticFunc>
	Function2(StaticFunc static_func) 
	{
		static_assert(sizeof(StaticFuncImpl) <= k_impl_buf_size, "Implementation object too big.");
		new (pimpl_) StaticFuncImpl(static_func);  // placement new	
	}

	template<typename MemFunc, class Obj> 
	Function2(MemFunc mem_func, Obj& obj) 
	{
		static_assert(sizeof(MemFuncImpl<Obj>) <= k_impl_buf_size, "Implementation object too big.");
		new (pimpl_) MemFuncImpl<Obj>(mem_func, obj);  // placement new
	}

	Ret operator()(Arg1 arg1, Arg2 arg2) 
	{
		if (is_empty()) {
			TWIST_THROW(L"This function obj is empty.");
		}
		return reinterpret_cast<ImplBase*>(pimpl_)->call(arg1, arg2);
	}

	bool operator==(const Function2& rhs) const 
	{
		return !is_empty() && !rhs.is_empty() && 
				reinterpret_cast<const ImplBase*>(pimpl_)->compare(
						*reinterpret_cast<const ImplBase*>(rhs.pimpl_));
	}

	bool is_empty() const 
	{
		return *pimpl_ == 0;
	}
	
	void reset() 
	{
		*pimpl_ = 0;
	}
	
private:
	class ImplBase {
	public:
		virtual Ret call(Arg1 arg1, Arg2 arg2) = 0;
		virtual bool compare(const ImplBase& other) const = 0;
	};

	class StaticFuncImpl : public ImplBase {
	public:
		typedef Ret (*StaticFunc)(Arg1, Arg2);
		
		StaticFuncImpl(StaticFunc static_func) 
			: ImplBase()
			, static_func_(static_func) 
		{
		}
		
		virtual Ret call(Arg1 arg1, Arg2 arg2) 
		{ 
			return (*static_func_)(arg1, arg2);		
		}

		virtual bool compare(const ImplBase& other) const 
		{
			const StaticFuncImpl* otherd = dynamic_cast<const StaticFuncImpl*>(&other);
			return (otherd != nullptr) && (static_func_ == otherd->static_func_);
		}
		
	private:
		StaticFunc  static_func_;
	};

	template<class Obj> 
	class MemFuncImpl : public ImplBase {
	public:
		typedef Ret (Obj::*MemFunc)(Arg1, Arg2);
		
		MemFuncImpl(MemFunc mem_func, Obj& obj) 
			: ImplBase()
			, mem_func_(mem_func)
			, obj_(&obj) 
		{
		}
		
		virtual Ret call(Arg1 arg1, Arg2 arg2) 
		{
			return (obj_->*mem_func_)(arg1, arg2);
		}

		virtual bool compare(const ImplBase& other) const 
		{
			const MemFuncImpl* otherd = dynamic_cast<const MemFuncImpl*>(&other);
			return (otherd != nullptr) && (mem_func_ == otherd->mem_func_) && (obj_ == otherd->obj_);
		}

	private:
		MemFunc  mem_func_;
		Obj*  obj_;
	};
	
	static const size_t  k_impl_buf_size = 24;  //+ This may be larger than Visual Studio's "small object optimisation" limit for std::function 
	char  pimpl_[k_impl_buf_size];
};

/*
///
///  Functor wrapping a static or non-const member function taking no arguments.
///
template<typename Ret>
class Function0 {
public:
	Function0() : pimpl_() {
	}

	template<typename StaticFunc>
	Function0(StaticFunc static_func) : pimpl_(new StaticFuncImpl(static_func)) {
	}

	template<typename MemFunc, class Obj> 
	Function0(MemFunc mem_func, Obj& obj) : pimpl_(new MemFuncImpl<Obj>(mem_func, obj)) {
	}

	Ret operator()() {
		if (is_empty()) {
			TWIST_THROW(L"This function obj is empty.");
		}
		return pimpl_->call();
	}

	bool operator==(const Function0& rhs) const {
		return !is_empty() && !rhs.is_empty() && pimpl_->compare(*rhs.pimpl_);
	}

	bool is_empty() const {
		return pimpl_.get() == nullptr;
	}
	
	void reset() {
		pimpl_.reset();
	}
	
private:
	class ImplBase {
	public:
		virtual Ret call() = 0;
		virtual bool compare(const ImplBase& other) const = 0;
	};

	class StaticFuncImpl : public ImplBase {
	public:
		typedef Ret (*StaticFunc)();
		
		StaticFuncImpl(StaticFunc static_func) : ImplBase(), static_func_(static_func) {
		}
		
		virtual Ret call() { 
			return (*static_func_)();		
		}
		
		virtual bool compare(const ImplBase& other) const {
			const StaticFuncImpl* otherd = dynamic_cast<const StaticFuncImpl*>(&other);
			return (otherd != nullptr) && (static_func_ == otherd->static_func_);
		}

	private:
		StaticFunc  static_func_;
	};

	template<class Obj> 
	class MemFuncImpl : public ImplBase {
	public:
		typedef Ret (Obj::*MemFunc)();
		
		MemFuncImpl(MemFunc mem_func, Obj& obj) : ImplBase(), mem_func_(mem_func), obj_(&obj) {
		}
		
		virtual Ret call() {
			return (obj_->*mem_func_)();
		}

		virtual bool compare(const ImplBase& other) const {
			const MemFuncImpl* otherd = dynamic_cast<const MemFuncImpl*>(&other);
			return (otherd != nullptr) && (mem_func_ == otherd->mem_func_) && (obj_ == otherd->obj_);
		}
		
	private:
		MemFunc  mem_func_;
		Obj*  obj_;
	};
	
	std::shared_ptr<ImplBase>  pimpl_;
};


///
///  Functor wrapping a static or non-const member function taking one argument.
///
template<typename Ret, typename Arg1>
class Function1 {
public:
	Function1() 
		: pimpl_() 
	{
	}

	template<typename StaticFunc>
	Function1(StaticFunc static_func) 
		: pimpl_(new StaticFuncImpl(static_func)) 
	{
	}

	template<typename MemFunc, class Obj> 
	Function1(MemFunc mem_func, Obj& obj) 
		: pimpl_(new MemFuncImpl<Obj>(mem_func, obj)) 
	{
	}

	Ret operator()(Arg1 arg1) 
	{
		if (is_empty()) {
			TWIST_THROW(L"This function object is empty.");
		}
		return pimpl_->call(arg1);
	}

	bool operator==(const Function1& rhs) const 
	{
		return !is_empty() && !rhs.is_empty() && pimpl_->compare(*rhs.pimpl_);
	}

	bool is_empty() const 
	{
		return pimpl_.get() == nullptr;
	}
	
	void reset() 
	{
		pimpl_.reset();
	}
	
private:
	class ImplBase {
	public:
		virtual Ret call(Arg1 arg1) = 0;
		virtual bool compare(const ImplBase& other) const = 0;
	};

	class StaticFuncImpl : public ImplBase {
	public:
		typedef Ret (*StaticFunc)(Arg1);
		
		StaticFuncImpl(StaticFunc static_func) : ImplBase(), static_func_(static_func) {
		}
		
		virtual Ret call(Arg1 arg1) { 
			return (*static_func_)(arg1);		
		}

		virtual bool compare(const ImplBase& other) const {
			const StaticFuncImpl* otherd = dynamic_cast<const StaticFuncImpl*>(&other);
			return (otherd != nullptr) && (static_func_ == otherd->static_func_);
		}
		
	private:
		StaticFunc  static_func_;
	};
	
	template <class Obj> 
	class MemFuncImpl : public ImplBase {
	public:
		typedef Ret (Obj::*MemFunc)(Arg1);
		
		MemFuncImpl(MemFunc mem_func, Obj& obj) : ImplBase(), mem_func_(mem_func), obj_(&obj) {
		}
		
		virtual Ret call(Arg1 arg1) {
			return (obj_->*mem_func_)(arg1);
		}

		virtual bool compare(const ImplBase& other) const {
			const MemFuncImpl* otherd = dynamic_cast<const MemFuncImpl*>(&other);
			return (otherd != nullptr) && (mem_func_ == otherd->mem_func_) && (obj_ == otherd->obj_);
		}

	private:
		MemFunc  mem_func_;
		Obj*  obj_;
	};
	
	std::shared_ptr<ImplBase> pimpl_;
};

///
///  Functor wrapping a static or non-const member function taking two arguments.
///
template<typename Ret, typename Arg1, typename Arg2>
class Function2 {
public:
	Function2() : pimpl_() {
	}

	template<typename StaticFunc>
	Function2(StaticFunc static_func) : pimpl_(new StaticFuncImpl(static_func)) {
	}

	template<typename MemFunc, class Obj> 
	Function2(MemFunc mem_func, Obj& obj) : pimpl_(new MemFuncImpl<Obj>(mem_func, obj)) {
	}

	Ret operator()(Arg1 arg1, Arg2 arg2) {
		if (is_empty()) {
			TWIST_THROW(L"This function obj is empty.");
		}
		return pimpl_->call(arg1, arg2);
	}

	bool operator==(const Function2& rhs) const {
		return !is_empty() && !rhs.is_empty() && pimpl_->compare(*rhs.pimpl_);
	}

	bool is_empty() const {
		return pimpl_.get() == nullptr;
	}
	
	void reset() {
		pimpl_.reset();
	}
	
private:
	class ImplBase {
	public:
		virtual Ret call(Arg1 arg1, Arg2 arg2) = 0;
		virtual bool compare(const ImplBase& other) const = 0;
	};

	class StaticFuncImpl : public ImplBase {
	public:
		typedef Ret (*StaticFunc)(Arg1, Arg2);
		
		StaticFuncImpl(StaticFunc static_func) : ImplBase(), static_func_(static_func) {
		}
		
		virtual Ret call(Arg1 arg1, Arg2 arg2) { 
			return (*static_func_)(arg1, arg2);		
		}

		virtual bool compare(const ImplBase& other) const {
			const StaticFuncImpl* otherd = dynamic_cast<const StaticFuncImpl*>(&other);
			return (otherd != nullptr) && (static_func_ == otherd->static_func_);
		}
		
	private:
		StaticFunc static_func_;
	};

	template<class Obj> 
	class MemFuncImpl : public ImplBase {
	public:
		typedef Ret (Obj::*MemFunc)(Arg1, Arg2);
		
		MemFuncImpl(MemFunc mem_func, Obj& obj) : ImplBase(), mem_func_(mem_func), obj_(&obj) {
		}
		
		virtual Ret call(Arg1 arg1, Arg2 arg2) {
			return (obj_->*mem_func_)(arg1, arg2);
		}

		virtual bool compare(const ImplBase& other) const {
			const MemFuncImpl* otherd = dynamic_cast<const MemFuncImpl*>(&other);
			return (otherd != nullptr) && (mem_func_ == otherd->mem_func_) && (obj_ == otherd->obj_);
		}

	private:
		MemFunc  mem_func_;
		Obj*  obj_;
	};
	
	std::shared_ptr<ImplBase>  pimpl_;
};

///
///  Functor wrapping a static or non-const member function taking three arguments.
///
template<typename Ret, typename Arg1, typename Arg2, typename Arg3>
class Function3 {
public:
	Function3() : pimpl_() {
	}

	template<typename StaticFunc>
	Function3(StaticFunc static_func) : pimpl_(new StaticFuncImpl(static_func)) {
	}

	template<typename MemFunc, class Obj> 
	Function3(MemFunc mem_func, Obj& obj) : pimpl_(new MemFuncImpl<Obj>(mem_func, obj)) {
	}

	Ret operator()(Arg1 arg1, Arg2 arg2, Arg3 arg3) {
		if (is_empty()) {
			TWIST_THROW(L"This function obj is empty.");
		}
		return pimpl_->call(arg1, arg2, arg3);
	}

	bool operator==(const Function3& rhs) const {
		return !is_empty() && !rhs.is_empty() && pimpl_->compare(*rhs.pimpl_);
	}

	bool is_empty() const {
		return pimpl_.get() == nullptr;
	}
	
	void reset() {
		pimpl_.reset();
	}
	
private:
	class ImplBase {
	public:
		virtual Ret call(Arg1 arg1, Arg2 arg2, Arg3 arg3) = 0;
		virtual bool compare(const ImplBase& other) const = 0;
	};

	class StaticFuncImpl : public ImplBase {
	public:
		typedef Ret (*StaticFunc)(Arg1, Arg2, Arg3);
		
		StaticFuncImpl(StaticFunc static_func) : ImplBase(), static_func_(static_func) {
		}
		
		virtual Ret call(Arg1 arg1, Arg2 arg2, Arg3 arg3) { 
			return (*static_func_)(arg1, arg2, arg3);		
		}

		virtual bool compare(const ImplBase& other) const {
			const StaticFuncImpl* otherd = dynamic_cast<const StaticFuncImpl*>(&other);
			return (otherd != nullptr) && (static_func_ == otherd->static_func_);
		}
		
	private:
		StaticFunc  static_func_;
	};

	template<class Obj> 
	class MemFuncImpl : public ImplBase {
	public:
		typedef Ret (Obj::*MemFunc)(Arg1, Arg2, Arg3);
		
		MemFuncImpl(MemFunc mem_func, Obj& obj) : ImplBase(), mem_func_(mem_func), obj_(&obj) {
		}
		
		virtual Ret call(Arg1 arg1, Arg2 arg2, Arg3 arg3) {
			return (obj_->*mem_func_)(arg1, arg2, arg3);
		}

		virtual bool compare(const ImplBase& other) const {
			const MemFuncImpl* otherd = dynamic_cast<const MemFuncImpl*>(&other);
			return (otherd != nullptr) && (mem_func_ == otherd->mem_func_) && (obj_ == otherd->obj_);
		}

	private:
		MemFunc  mem_func_;
		Obj*  obj_;
	};
	
	std::shared_ptr<ImplBase>  pimpl_;
};
*/

} 

#endif 

/// @file commands.hpp
/// Generic utilities for implementing the classic "Command" pattern (usable eg for undoable actions)

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_COMMANDS_HPP
#define TWIST_COMMANDS_HPP

#include <stack>

namespace twist {

///
///  The command class in the classic "Command" pattern.
///
class Command : public NonCopyable {
public:
	/// Constructor
	///
	Command();

	/// Destructor
	///
	virtual ~Command() = 0;

	/// Do (execute) the command. An exception is thrown if the command has already been done (executed).
	///
	void do_it();

	/// Undo (reverse) the command. An exception is thrown if the command has not yet been done (executed).
	///
	void undo_it();

	/// Get a short description of the command.
	///
	/// @return  The short description
	///
	virtual std::wstring get_short_descr() const = 0;

protected:
	// Command state
	enum State {
		k_not_done = 1,  // Either initial state, or immediately after successful call to  undo_it()
		k_doing    = 2,  // During a call to  do_it()
		k_done     = 3,  // Immediately after successful call to  do_it() 
		k_undoing  = 4   // During a call to  undo_it()
	};

	/// Get the command state.
	///
	/// @return  The state
	///
	State get_state() const;

private:
	/// Do (execute) the command.
	///
	virtual void do_command() = 0;

	/// Undo (reverse) the command.
	///
	virtual void undo_command() = 0;
	
	State  state_;

	TWIST_CHECK_INVARIANT_DECL
};

///
///  The command manager class in the classic "Command" pattern.
///
class CommandMgr : public NonCopyable {
public:
	/// Constructor
	///
	CommandMgr();

	/// Destructor
	///
	~CommandMgr();

	/// Execute a comand and store it in the command stack.
	///
	/// @param[in] command  The command 
	///
	void do_and_store_command(std::unique_ptr<Command> command);

	/// Reverse (undo) the last command in the command stack and remove it from the stack.
	/// An exception is thrown if the commans stack is empty.
	///
	void undo_and_remove_last_command();

	/// Get the last command in the command stack.
	/// An exception is thrown if the commans stack is empty.
	///
	const Command& get_last_command() const;

private:
	std::stack<std::unique_ptr<Command>>  commands_done_;

	TWIST_CHECK_INVARIANT_DECL
};

} 

#endif 


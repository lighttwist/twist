///  @file  ProgFeedbackProv.hpp
///  ProgIndicatorProgProv abstract class

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_PROG_FEEDBACK_PROV_HPP
#define TWIST_PROG_FEEDBACK_PROV_HPP

namespace twist {

/// Abstract base class for feedback providers which provide progress feedback using a progress indicator. 
class ProgFeedbackProv {
public:
	/// Destructor.
	virtual ~ProgFeedbackProv() = 0;

	/// Get the progress indicator range.
	///
	/// @param[in] min_pos  The minimum position
	/// @param[in] max_pos  The maximum position
	///
	virtual void get_prog_range(Ssize& min_pos, Ssize& max_pos) const = 0;

	/// Set the progress indicator range.
	///
	/// @param[in] min_pos  The minimum position
	/// @param[in] max_pos  The maximum position
	///
	virtual void set_prog_range(Ssize min_pos, Ssize max_pos) = 0;

	/// Get the current progress indicator position.
	///
	/// @return  The position
	///
	virtual Ssize get_prog_pos() const = 0;

	/// Set the current progress indicator position.
	///
	/// @param[in] pos  The position
	/// @param[in] force_update  Whether this method should try and force the UI to update
	///
	virtual void set_prog_pos(Ssize pos, bool force_update = false) = 0;

	/// Increment the current progress indicator position.
	///
	/// @param[in] force_update  Whether this method should try and force the UI to update
	///
	virtual void inc_prog_pos(bool force_update = false) = 0;

protected:	
	/// Constructor.
	ProgFeedbackProv();
};

} 

#endif 


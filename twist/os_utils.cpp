/// @file os_utils.cpp
/// Implementation file for "os_utils.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/os_utils.hpp"

#include "twist/DateTime.hpp"
#include "twist/string_utils.hpp"

#include <cstdarg>

#if TWIST_COMPILER_MSVC
#	include <guiddef.h>
#	include <rpc.h>
#	include <ShlObj.h>
#	pragma comment(lib, "RpcRT4.lib")
#	pragma comment(lib, "Version.lib")
#	define RPC_CHECK(ret, msg)  if (ret != RPC_S_OK) { TWIST_THROW(msg); }
#endif

namespace twist {

// --- Uuid class ---

#if TWIST_COMPILER_MSVC
// Define the internal UUID type to be the Windows GUID type
struct Uuid::UuidImpl : public GUID {};
#else
struct Uuid::UuidImpl {};
#endif

Uuid::Uuid() 
	: uuid_impl_{new UuidImpl{}}
{
#if TWIST_COMPILER_MSVC
	// Generate a new UUID
	RPC_STATUS rpc_status = ::UuidCreate(uuid_impl_.get());
	RPC_CHECK(rpc_status, L"Failed to generate a global unique identifier");

#else
	TWIST_THROW(L"Only implemented for MSVC for now.");
#endif
	TWIST_CHECK_INVARIANT
}

Uuid::Uuid(const std::wstring& uuid_str) 
	: uuid_impl_{new UuidImpl()}
{
#if TWIST_COMPILER_MSVC
	// Sometimes Uuid strings enclosed in curly brackets are considered valid; make sure we strip any curly brackets.
	std::wstring final_uuid_str = uuid_str;
	final_uuid_str = replace_all_substr<wchar_t>(final_uuid_str, L"{", L"");
	final_uuid_str = replace_all_substr<wchar_t>(final_uuid_str, L"}", L"");

	RPC_WSTR buffer = reinterpret_cast<unsigned short*>(const_cast<wchar_t*>(final_uuid_str.c_str()));
	RPC_CHECK(::UuidFromStringW(buffer, uuid_impl_.get()), L"Failed to create a UUID object.");

#else
	TWIST_THROW(L"Only implemented for MSVC for now.");
#endif
	TWIST_CHECK_INVARIANT
}

Uuid::Uuid(const Uuid& src)
	: uuid_impl_{new UuidImpl{*src.uuid_impl_}}
{
	TWIST_CHECK_INVARIANT
}

Uuid::Uuid(Uuid&& src)
	: uuid_impl_{move(src.uuid_impl_)}
{
	TWIST_CHECK_INVARIANT
}

Uuid::~Uuid()
{
	// No state testing due to move semantics
}

Uuid& Uuid::operator=(const Uuid& rhs)
{
	TWIST_CHECK_INVARIANT
	if (&rhs != this) {
		*uuid_impl_ = *rhs.uuid_impl_;
	}
	return *this;
}

Uuid& Uuid::operator=(Uuid&& rhs)
{
	TWIST_CHECK_INVARIANT
	uuid_impl_ = move(rhs.uuid_impl_);
	return *this;
}

bool Uuid::operator==(const Uuid& rhs) const
{
	TWIST_CHECK_INVARIANT
	return memcmp(uuid_impl_.get(), rhs.uuid_impl_.get(), sizeof(UuidImpl)) == 0;
}

bool Uuid::operator!=(const Uuid& rhs) const
{
	TWIST_CHECK_INVARIANT
	return memcmp(uuid_impl_.get(), rhs.uuid_impl_.get(), sizeof(UuidImpl)) != 0;
}

bool Uuid::operator<(const Uuid& rhs) const
{
	TWIST_CHECK_INVARIANT
	return memcmp(uuid_impl_.get(), rhs.uuid_impl_.get(), sizeof(UuidImpl)) < 0;
}

#ifdef _DEBUG
void Uuid::check_invariant() const noexcept
{
	assert(uuid_impl_);
}
#endif 

// --- Global constants and functions ---

std::wstring to_str(const Uuid& uuid, bool brackets) 
{
#if TWIST_COMPILER_MSVC
	RPC_WSTR buffer = nullptr;
	RPC_STATUS rpc_status = ::UuidToStringW(uuid.uuid_impl_.get(), &buffer);
	RPC_CHECK(rpc_status, L"Failed to convert the UUID to a string.");
	
	std::wstring uuid_str = reinterpret_cast<wchar_t*>(buffer);
	::RpcStringFreeW(&buffer);

	if (brackets) {
		uuid_str = L"{" + uuid_str + L"}";
	}
	return uuid_str;

#else
	TWIST_THROW(L"Only implemented for MSVC for now.");
#endif
}

Uuid no_uuid()
{
	return Uuid{ L"00000000-0000-0000-0000-000000000000" };
}

wchar_t get_os_decimal_sep()
{
#if TWIST_COMPILER_MSVC
	static const size_t k_buffer_size = 2;
	wchar_t buffer[k_buffer_size];

	const int result = ::GetLocaleInfoW(LOCALE_USER_DEFAULT, LOCALE_SDECIMAL, buffer, k_buffer_size);
	TWIST_ASSERT(result == k_buffer_size);

	return buffer[0];

#else
	TWIST_THROW(L"Only implemented for MSVC for now.");
#endif
}

void native_terminate_thread(std::thread& thread)
{
#if TWIST_COMPILER_MSVC
	// We need to first detach the running thread from the std::thread object, before "natively" terminating 
	//   the thread.
	// It seems that the native thread handle changes after the running thread is detached from the 
	//   std::thread object.
	// The Windows thread ID remains the same, though, so we first obtain that, then detach the running 
	//   thread, and then get the new thread handle using the thread ID.  Only then can we terminate 
	//   the thread.

	const auto native_thread_handle = thread.native_handle();
	assert(native_thread_handle != nullptr);

	const auto thread_id = ::GetThreadId(native_thread_handle);
	assert(thread_id != 0);

	thread.detach();

	const auto thread_handle = ::OpenThread(THREAD_ALL_ACCESS, TRUE/*inherit_handle*/, thread_id);
	if (thread_handle == nullptr) {
		// Seems the thread is unopenable by the OS, so we consider it terminated
		return;
	}

	auto result = ::TerminateThread(thread_handle, 0/*exit_code*/);
	if (!result) {
		TWIST_THROW(L"Unable to terminate thread with ID %d: %s", thread_id, get_last_os_err().c_str());
	}

	result = ::CloseHandle(thread_handle);
	if (!result) {
		TWIST_THROW(L"Unable to close thread with ID %d: %s", thread_id, get_last_os_err().c_str());
	}

#else
	TWIST_THROW(L"Only implemented for MSVC for now.");
#endif
}

void exit_process(unsigned int exit_code)
{
#if TWIST_COMPILER_MSVC
	::ExitProcess(exit_code);
#else
	TWIST_THROW(L"Only implemented for MSVC for now.");
#endif
}

std::optional<FileVersionInfo> get_file_version_info(const fs::path& path)
{
#if TWIST_COMPILER_MSVC
	DWORD dummy_param = 0;
	const DWORD ver_info_size = ::GetFileVersionInfoSize(path.c_str(), &dummy_param);
	if (ver_info_size == 0) {
		return {};
	}
	std::vector<char> ver_info_buffer(ver_info_size);
	if (!::GetFileVersionInfo(path.c_str(), 0, ver_info_size, ver_info_buffer.data())) {
		return {};
	}
	VS_FIXEDFILEINFO* ver_info_ptr = nullptr;
	unsigned int result_size = 0;
	if (!::VerQueryValue(ver_info_buffer.data(), L"\\", reinterpret_cast<void**>(&ver_info_ptr), &result_size) 
			|| ver_info_ptr == nullptr) {
		return {};
	}
	return FileVersionInfo{ 
			HIWORD(ver_info_ptr->dwFileVersionMS),
			LOWORD(ver_info_ptr->dwFileVersionMS),
			HIWORD(ver_info_ptr->dwFileVersionLS),
			LOWORD(ver_info_ptr->dwFileVersionLS) };

#else
	TWIST_THROW(L"get_file_version_info() is only implemented for MSVC for now.");
#endif
}

auto get_running_module_path() -> fs::path
//+TODO: Test what happens if called from a dll 
{
#if TWIST_OS_WIN

	HMODULE module_hnd{};
	if (::GetModuleHandleExW(GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS | GET_MODULE_HANDLE_EX_FLAG_UNCHANGED_REFCOUNT, 
			                 L"twist::get_running_module_path", &module_hnd) != TRUE) {

		TWIST_THROW(get_last_os_err().c_str());
	}

	wchar_t module_path[MAX_PATH];
	::GetModuleFileNameW(module_hnd, module_path, sizeof(module_path));

	return module_path;

#elif TWIST_OS_LINUX

    char result[PATH_MAX];
    const auto count = readlink("/proc/self/exe", result, PATH_MAX);
	if (count <= 0) {
		TWIST_THRO2(L"readlink(\"/proc/self/exe\", ...) failed.");
	}
    return std::string{result, static_cast<size_t>(count)};

#endif
}

bool file_is_read_only(const fs::path& path)
{
	if (!exists(path)) {
		TWIST_THROW(L"Cannot find file \"%s\"", path.c_str());
	}
	if (is_directory(path)) {
		TWIST_THROW(L"Path \"%s\" is a directory path.", path.c_str());
	}

#if TWIST_COMPILER_MSVC
	const DWORD fileAttr = ::GetFileAttributes(path.c_str());
	if ((fileAttr != INVALID_FILE_ATTRIBUTES) && ((FILE_ATTRIBUTE_DIRECTORY & fileAttr) == 0)) {
		return (fileAttr & FILE_ATTRIBUTE_READONLY) != 0;
	}
	return false;

#else
	TWIST_THROW(L"file_is_read_only() is only implemented for MSVC for now.");
#endif
}

bool make_win_file_hidden(const fs::path& path)
{
	if (!exists(path)) {
		TWIST_THROW(L"Cannot find file \"%s\"", path.c_str());
	}
	if (is_directory(path)) {
		TWIST_THROW(L"Path \"%s\" is a directory path.", path.c_str());
	}

#if TWIST_COMPILER_MSVC
	const DWORD fileAttr = ::GetFileAttributes(path.c_str());
	if ((fileAttr != INVALID_FILE_ATTRIBUTES) && ((FILE_ATTRIBUTE_DIRECTORY & fileAttr) == 0)) {
		if ((fileAttr & FILE_ATTRIBUTE_HIDDEN) != 0) {
			// The file's "hidden" attribute is already on
			return true;
		}
		// The file's "hidden" attribute is off; turn it on
		return ::SetFileAttributes(path.c_str(), fileAttr | FILE_ATTRIBUTE_HIDDEN);
	}
#endif

	return false;
}

void file_get_time(const fs::path& filename, DateTime* creationTime, DateTime* lastAccessTime, 
		DateTime* lastWriteTime)
{
#if TWIST_COMPILER_MSVC
	auto to_datetime = [](const FILETIME& win_file_time) {
		SYSTEMTIME stUTC;
		SYSTEMTIME stLocal;

		// Convert the last-write time to local time.
		::FileTimeToSystemTime(&win_file_time, &stUTC);
		::SystemTimeToTzSpecificLocalTime(nullptr, &stUTC, &stLocal);

		return DateTime{ stLocal.wDay, stLocal.wMonth, stLocal.wYear,
				stLocal.wHour, stLocal.wMinute, stLocal.wSecond };
	};

	HANDLE hFile = ::CreateFile(filename.c_str(), FILE_READ_ATTRIBUTES, FILE_SHARE_READ, nullptr, 
			OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);
	if (hFile == INVALID_HANDLE_VALUE) {
		TWIST_THROW(L"Error reading file \"%s\". %s", filename.c_str(), get_last_os_err().c_str());
	}
	
	FILETIME winCreationTime; 
	FILETIME winLastAccessTime;
	FILETIME winLastWriteTime;
	
	if (!::GetFileTime(hFile, &winCreationTime, &winLastAccessTime, &winLastWriteTime)) {
		::CloseHandle(hFile);
		TWIST_THROW(L"Error reading last access time for file \"%s\". %s", filename.c_str(), get_last_os_err().c_str());
	}

	if (creationTime != nullptr) {
		*creationTime = to_datetime(winCreationTime);
	}
	if (lastAccessTime != nullptr) {
		*lastAccessTime = to_datetime(winLastAccessTime);
	}
	if (lastWriteTime != nullptr) {
		*lastWriteTime = to_datetime(winLastWriteTime);
	}

	::CloseHandle(hFile);

#else
	TWIST_THROW(L"file_get_time() is only implemented for MSVC for now.");
#endif
}

auto get_special_dir(SpecialDir dir_id) -> fs::path
{
#if TWIST_COMPILER_MSVC
	fs::path special_dir;
	bool success = false;

	switch (dir_id) {
	case SpecialDir::common_app_data : {
		wchar_t* buffer = nullptr;
		success = ::SHGetKnownFolderPath(FOLDERID_ProgramData, 0, nullptr, &buffer) == S_OK;
		special_dir = buffer;
		::CoTaskMemFree(buffer);
		break;
	}
	case SpecialDir::sys_temp : {
		std::unique_ptr<wchar_t[]> buffer = std::make_unique<wchar_t[]>(MAX_PATH);
		const size_t path_len = ::GetTempPath(MAX_PATH, buffer.get());
		if (path_len > 0) {
			special_dir.assign(buffer.get(), buffer.get() + path_len);
			success = true;
		}
		break;
	}
	default: 
		TWIST_THROW(L"Unrecognised special directory ID value %d.", dir_id);
	}	

	if (!success) {
		TWIST_THROW(L"Failed to acquire special directory with ID value %d.", dir_id);
	}

	return special_dir;

#else
	TWIST_THROW(L"get_special_dir() is only implemented for MSVC for now.");
#endif
}

std::wstring get_last_os_err() 
{
#if TWIST_COMPILER_MSVC
	unsigned long error_no = ::GetLastError();

	const size_t k_max_len = 2048;
	std::vector<wchar_t> msg_buffer(k_max_len);

	const BOOL success = ::FormatMessageW(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS, nullptr, 
			error_no, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), msg_buffer.data(), k_max_len, nullptr);

	std::wstringstream err_descr;
	assert(success != FALSE);
	if (success != FALSE) {
		err_descr << msg_buffer.data() << L"  Error no: \"" << error_no << L"\"";
	}

	return err_descr.str();

#else
	return L"get_last_os_err() is only implemented for MSVC for now.";
#endif
} 

auto wchar_to_multibyte_string(const wchar_t* wstr) -> std::vector<char>
{ 
#if TWIST_COMPILER_MSVC
#ifndef UNICODE
	TWIST_THRO2(Only call this function if UNICODE is defined.);
#endif
    const auto required_mb_buf_size = WideCharToMultiByte(CP_ACP/*code_page*/,
														  0/*flags*/,
														  wstr/*wide_shar_str*/,
														  -1/*wide_char_char_count*/,
														  nullptr/*multi_byte_str*/,
														  0/*multi_byte_byte_count*/,
														  nullptr/*default_char*/,   
														  nullptr/*used_default_char*/);
   
	auto mbstr = std::vector<char>(required_mb_buf_size);
    
    [[maybe_unused]] const auto bytes_written = WideCharToMultiByte(CP_ACP/*code_page*/,
																	0/*flags*/,
																	wstr/*wide_shar_str*/,
																	-1/*wide_char_count*/,
																	mbstr.data()/*multi_byte_str*/,
																	required_mb_buf_size/*multi_byte_byte_count*/,
																	nullptr/*default_char*/,   
																	nullptr/*used_default_char*/);

	assert(required_mb_buf_size == bytes_written);
	return mbstr;
#else
	TWIST_THROW(L"Only implemented for MSVC for now.");
#endif
}

auto out_debug_str(const std::wstring& msg) -> void
{
#if TWIST_COMPILER_MSVC
	::OutputDebugStringW(msg.c_str());
#else
	TWIST_THROW(L"Only implemented for MSVC for now.");
#endif
}

auto out_debug_str(const std::string& msg) -> void
{
#if TWIST_COMPILER_MSVC
	::OutputDebugStringA(msg.c_str());
#else
	TWIST_THROW(L"Only implemented for MSVC for now.");
#endif
}

void outf_debug_str(const wchar_t* format, ...)
{
	static const size_t k_buf_size = 2048;
	wchar_t buffer[k_buf_size];

	va_list args;
	va_start(args, format);
	vswprintf(buffer, k_buf_size, format, args);
	va_end(args);

	out_debug_str(buffer);
}

void outf_debug_str(const char* format, ...)
{
	static const size_t k_buf_size = 2048;
	char buffer[k_buf_size];

	va_list args;
	va_start(args, format);
	vsnprintf(buffer, k_buf_size, format, args);
	va_end(args);

	out_debug_str(buffer);
}

} 

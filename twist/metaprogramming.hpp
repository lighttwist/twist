///  @file  metaprogramming.hpp
///  Template metaprogramming tidbits 

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_METAPROGRAMMING_HPP
#define TWIST_METAPROGRAMMING_HPP

#include <tuple>
#include <type_traits>
#include <utility>

#include "is_valid_lambda_decl.hpp"

namespace twist {

/// Invalid tuple element index
constexpr size_t tuple_npos = static_cast<size_t>(-1);

/// Class template which stores a refence to tuple's element, together with the index of that element in 
/// the tuple, as a compile time constant.
///
/// @tparam  Idx  The index of the element in the tuple
/// @tparam  Elem  The tuple element type
///
template<size_t Idx, class Elem>
class IndexedTupleElement {
public:
	using ElemType = Elem;  ///< The tuple element type

	static const size_t elem_index = Idx;  ///< The index of the element in the tuple

	/// Constructor.
	///
	/// @param[in] elem  The tuple element, whose reference will be stored in the instance
	///
	IndexedTupleElement(const Elem& elem) : elem_(elem) {}
		
	/// Get a reference to the tuple element.
	///
	/// @return  Reference to the tuple element
	///
	const Elem& elem() const { return elem_; }

private:
	const Elem&  elem_;
};
 
/// Invoke a function-like (callable) object, once, with all the elements of a tuple, which will be passed to 
/// the function as a parameter pack; the function must be a variadic function template, taking a parameter 
/// pack and no other parameters.
///
/// @tparam  Tuple  The tuple type 
/// @tparam  Func  The variadic function template type
/// @return  The same thing as the function passed in
///
template<class Tuple, class Func>
decltype(auto) tuple_apply(Tuple&& tup, Func&& func);
 
/// Invoke a function-like (callable) object with the elements of a tuple, either multiple times (once for 
/// each element), or once with all elements as a parameter pack; the function must be either a variadic 
/// function template, taking a parameter pack (the tuple elements) and no other parameters, or a function 
/// template taking one parameter (the tuple element).
///
/// @tparam  Tuple  The tuple type 
/// @tparam  Func  The function template type
/// @param[in] tup  The tuple 
/// @param[in] func  The function 
///
template<class Tuple, class Func>
void tuple_apply_to_each(Tuple&& tup, Func&& func);

/// Invoke a function-like (callable) object with the elements of a tuple, each wrapped in a 
/// IndexedTupleElement object, either multiple times (once for each element), or once with all elements as a 
/// parameter pack; the function must be either a variadic function template, taking a parameter pack (the 
/// wrapped tuple elements) and no other parameters, or a function template taking one parameter (the wrapped 
/// tuple element).
///
/// @tparam  Tuple  The tuple type 
/// @tparam  Func  The function template type
/// @param[in] tup  The tuple 
/// @param[in] func  The function 
///
template<class Tuple, class Func>
void tuple_apply_to_each_with_idx(Tuple&& tup, Func&& func);

/// Invoke a function-like (callable) object on each element of a tuple, and store the results in a tuple;
/// the function must be a function template taking one parameter, which is the tuple element.
///
/// @tparam  Tuple  The input tuple type
/// @tparam  Func  Function type; must be a function template or generic lambda with one parameter compatible 
///				with each element of the input tuple
/// @param[in] tup  The input tuple 
/// @param[in] func  The tranformation function
/// @return  Tuple containing the results of the transformation
///
template<class Tuple, class Func>
auto tuple_transform_each(Tuple&& tup, Func&& func);

/// Get the index of the first tuple element which has a specific type.
///
/// @tparam  Tuple  The tuple type
/// @tparam  Ty  The tuple element type to search for
/// @return  The index of the first matching tuple element type; or tuple_npos if there is no match
/// 
template<class Tuple, class Ty> 
constexpr size_t tuple_element_index();

/// A flag which specifies whether a specific type exists in a list of types.
///
/// @tparam  Ty  The type we are looking for in the list
/// @tparam  Types  The list of types
///
template<class Ty, class ...Types> 
constexpr bool is_one_of = tuple_element_index<std::tuple<Types...>, Ty>() != tuple_npos;

/// A variable template whose value is always false; it is a dependent name if used inside a template which 
/// can be useful eg for static_asserts which should not fire when the template is initially parsed.
template<class T>
constexpr bool always_false = false;

}

//
//  Macros
//

/// Creates a function with the declaration 
///   template<class T> constexpr bool has_member_membername();
/// where 'membername' is the string passed as an argument to the macro.
/// The function determines whether type T has a data or function member named 'membername' in it or not. 
/// Note that the type of the data member 'membername' does not matter nor does the return value and arguments 
///   of the member function (if it is one), and nor does the visibility of the member. 
/// Devnote: It is preferable to use is_valid_lambda_decl() instead, if possible
#define TWIST_GENERATE_HAS_MEMBER(membername)  \
	namespace detail {  \
	template<class T>  \
	class HasMemberImpl_##membername {  \
	/* Based on https://en.wikibooks.org/wiki/More_C%2B%2B_Idioms/Member_Detector */  \
	private:  \
		using Yes = char[2];  \
		using No = char[1];  \
		struct Fallback { int membername; };  \
		struct Derived : T, Fallback {};  \
		template<class U> static No& test(decltype(U::membername)*);  \
		template<class U> static Yes& test(U*);  \
	public:  \
		static constexpr bool result = sizeof(test<Derived>(nullptr)) == sizeof(Yes);  \
	};  \
	}  \
	  \
	template<class T>  \
		constexpr bool has_member_##membername() { return detail::HasMemberImpl_##membername<T>::result; } 

/// Declare a new class type which wraps a specific integer value.
///
/// @tparam  ClassName  The new class name; must be unused.
/// @tparam  IntType  The integer type (e.g. int, char, unsigned short, etc. or perhaps an enum type)
/// @param[in] value  The value; must be a valid integer
///
#define TWIST_DECL_NEW_META_INT(ClassName, IntType, value)  \
	struct ClassName : public std::integral_constant<IntType, value> { };	

#include "metaprogramming.ipp"

#endif 


///  @file  ObjectMarker.hpp 
///  ObjectMarker class template

//   Written by Dan Ababei
//   Copyright (c) 2007-2020 Delft University of Technology

#ifndef TWIST_OBJECT_MARKER_HPP
#define TWIST_OBJECT_MARKER_HPP

namespace twist {

/// Class which marks and unmarks subsets of a set of objects for some purpose.
///
/// @tparam  Obj  The object type
///
template<class Obj>
class ObjectMarker {
public:
	/// A scoped marker for a single object. When an instance of this class is created, the object it takes 
	///   care of is marked, and when it is destroyed, the object is unmarked.  
	/// An object cannot have multiple marks.
	class ScopedMarker {
	public:	
		/// Destructor.
		~ScopedMarker();

		TWIST_NO_COPY_NO_MOVE(ScopedMarker)

	private:
		ScopedMarker(std::set<Obj>& objects, const Obj& obj);

		std::set<Obj>&  objects_;
		const Obj&  obj_;

		friend class ObjectMarker;
	};

	/// Constructor.
	ObjectMarker();

	/// Create a scoped marker which takes care of a specific object. The object is marked when this method
	/// is called, and it is unmarked when the returned marker goes out of scope.
	///
	/// @param[in] obj  The object to be marked; if the object is already marked, an exception is thrown
	/// @return  The marker
	///
	[[nodiscard]] ScopedMarker make_scoped_marker(const Obj& obj);

	/// Find out whether a specific object is currently marked.
	///
	/// @param[in] obj  The object
	/// @return  true if the object is marked
	///
	bool is_marked(const Obj& obj) const;

	TWIST_NO_COPY_NO_MOVE(ObjectMarker)

private:
	std::set<Obj>  objects_{};
};

}

#include "ObjectMarker.ipp"

#endif 

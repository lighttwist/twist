///  @file  Date.hpp
///  Date  class

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_DATE_HPP
#define TWIST_DATE_HPP

#include "metaprogramming.hpp"

namespace twist {

/*! Date class.
    The month number is one-based: 1 = January, 12 = December.
    The day-of-the-month number is one-based: 1 = the first, 31 = the thirty-first.
    The range of dates representable by this class is 01-Jan-0001 to 31-Dec-9999.
 */
class Date {
public:
	/// Constructor. Initialises the date to the 1st of January, year 1.
	///
	Date();

	/// Constructor.
	///
	/// @param[in] day  The day-of-the-month; between 1 and 31
	/// @param[in] month  The month; 1 = January, 2 = February, 12 = December
	/// @param[in] year  The year
	///
	Date(int day, int month, int year);

	/// Copy constructor.	
	Date(const Date& src);

	/// Destructor.
	virtual ~Date();

	/// Assignment operator.
	virtual Date& operator=(const Date& rhs);

	/// Get the day-of-the-month; between 1 and 31.
	[[nodiscard]] int day() const;  

	/// The month as a number; 1 = January, 2 = February, 12 = December.
	[[nodiscard]] int month() const;  

	/// The year.
	[[nodiscard]] int year() const;

	/// Whether the date is in a leap year.
	[[nodiscard]] bool is_leap_year() const;

protected:
	using TinyInt = unsigned char;

	using SmallInt = unsigned short;

	static TinyInt to_tiny_int(int x); 

	static SmallInt to_small_int(int x);

private:
	bool tomorrow();

	bool yesterday();

	TinyInt day_;
	TinyInt month_;
	SmallInt year_;

	TWIST_CHECK_INVARIANT_DECL
};

// --- Free functions ---

/// Date string formats
enum class DateStrFormat { 		
	/// eg 25[S]03[S]2007, where [S] is a customisable, non-empty, separator string
    dd_mm_yyyy = 1,  
	/// eg 5[S]03[S]2007 or 05[S]03[S]2007 or 5[S]3[S]2007, where [S] is a customisable, non-empty, 
	/// separator string
    d_m_yyyy = 2,  	
	/// eg 2007[S]03[S]25, where [S] is a customisable, non-empty, separator string
	yyyy_mm_dd = 3,  
	/// eg 25th March 2007
	dayth_month_year = 5,  
	/// eg 25 Mar 2007
	day_mon_year = 6,  
	/// eg Sunday 25th of March 2007
	dow_day_month_year = 7,  		
	/// eg Sunday 25th of March
	dow_day_month = 8,  
	/// eg 20070325
	yyyymmdd = 9   
};

/// Equality operator.
bool operator==(const Date& lhs, const Date& rhs);

/// Inequality operator.
bool operator!=(const Date& lhs, const Date& rhs);

/// Smaller-than operator; returns true if the left-hand-side date falls before the right-hand-side date.
bool operator<(const Date& lhs, const Date& rhs);

/// Smaller-or-equal operator; returns true if the left-hand-side date falls before, or is the same as, the 
/// right-hand-side date.
bool operator<=(const Date& lhs, const Date& rhs);

/// Greater-than operator; returns true if the left-hand-side date falls after the right-hand-side date.
bool operator>(const Date& lhs, const Date& rhs);

/// Greater-or-equal operator; returns true if the left-hand-side date falls after, or is the same as, the 
/// right-hand-side date.
bool operator>=(const Date& lhs, const Date& rhs);

/// Find out whether two dates are the same. Same as the equality operator.
///
/// @param[in] date1  The first date  
/// @param[in] date2  The second date  
/// @return  true if the two dates are identical
///
bool same_date(const Date& date1, const Date& date2);

/// Get the system date.
[[nodiscard]] Date get_system_date();

/// Get the day-of-the-week number for a specific date.
///
/// @param[in] date  The date
/// @return  The day-of-the-week number: 0 is Sunday, 6 is Saturday
///
[[nodiscard]] int get_dow(const Date& date);

/// Get the day-of-the-week name (in English) for a specific date.
///
/// @param[in] date  The date
/// @return  The day-of-the-week name, eg Sunday, Saturday
///
[[nodiscard]] std::wstring get_dow_name(const Date& date);

/// Get the month name (in English) for a specific date.
///
/// @param[in] date  The date
/// @return  The month name, eg January, December
///
[[nodiscard]] std::wstring get_month_name(const Date& date);

/// Get the short month name (three-letter, in English) for a specific date.
///
/// @param[in] date  The date
/// @return  The short month name, eg Jan, Dec
///
[[nodiscard]] std::wstring get_month_short_name(const Date& date);

//! Whether the year \p year is a leap year.
[[nodiscard]] constexpr auto is_leap_year(int year) -> bool;

/*! Get the difference in days between two dates.
    If the two dates are the same, the difference is zero. 
    \param[in] date1  The first date
    \param[in] date2  The second date
    \return  The difference in days; negative if the second date is before the first 
 */
[[nodiscard]] auto get_diff_in_days(const Date& date1, const Date& date2) -> int;

/// Calculate the date obtained by adding or subtracting a number of days to/from a specific date.
///
/// @param[in] date  The input date
/// @param[in] days  The number of days to add to (if positive) or subtract from (if negative) the date
/// @return  The calculted date
///
[[nodiscard]] Date add_days(const Date& date, int days);  

/// Calculate the date obtained by adding or subtracting a number of full months to/from a specific date.
///
/// @param[in] date  The input date
/// @param[in] months The number of full months to add to (if positive) or subtract from (if negative) 
///					the date
/// @return  The calculted date
///
[[nodiscard]] Date add_months(const Date& date, int months);  

/// Calculate the date obtained by adding or subtracting a number of years to/from a specific date.
///
/// @param[in] date  The input date
/// @param[in] years  The number of years to add to (if positive) or subtract from (if negative) the date;
///					an exception is thrown if the resuting date is out of the valid date bounds
/// @return  The calculted date
///
[[nodiscard]] Date add_years(const Date& date, int years); 

/// Get the "Julian day number" (JDN) corresponding to a specific date.
/// A Julian day is the number of days since the beginning of the Julian Period. It is used primarily by 
/// astronomers (and should not be confused with the Julian calendar). The Julian Period started in 4713 BC. 
/// The Julian day number 0 is assigned to the day starting at noon on January 1, 4713 BC. The "Julian Day 
/// Number" (JDN) is the number of days elapsed since the beginning of this period.
///
/// @param[in] date  The date
/// @return  The Julian day number
///
[[nodiscard]] int get_julian_day_number(const Date& date);

/// Convert a date value to the number of days since the "Unix epoch", ie 1 Jan 1970. 
///
/// @param[in] date  The date value
/// @return  The number of days since 1 Jan 1970; zero for 1 Jan 197, negative for dates before that, positive
///					for dates after 
///
[[nodiscard]] int date_to_days_since_unix_epoch(const Date& date);

/// Convert the number of days since the "Unix epoch", ie 1 Jan 1970, to a date value.
///
/// @param[in] days  The number of days since 1 Jan 1970; zero for 1 Jan 197, negative for dates before that, positive
///					for dates after 
/// @return  The date value
///
[[nodiscard]] Date days_since_unix_epoch_to_date(int days);

/// Convert a date value to a standard timepoint based on the (UTC) system clock (with all time components 
/// set to zero). 
///
/// @param[in] date  The date value
/// @return  The timepoint
///
[[nodiscard]] std::chrono::system_clock::time_point date_to_utc_timepoint(const Date& date);

/// Convert the date part of a standard timepoint based on the (UTC) system clock to a date value. 
///
/// @param[in] timept  The timepoint
/// @return  The date value
///
[[nodiscard]] Date utc_timepoint_to_date(const std::chrono::system_clock::time_point& timept);

/// Get the dates corresponding to each day in a specific year.
///
/// @param[in] year  The year
/// @return  The dates in the year, ordered  
///
std::vector<Date> get_dates_in_year(int year);

/// For each date in a specific year, call a user-supplied function with that date as argument.
///
/// @tparam  Fn  Callable type compatible with signature (Date) -> auto
/// @param[in] year  The year
/// @param[in] func  Callable object to be invoked with each date in the year, in order  
///
template<class Fn, class = EnableIfFunc<Fn, Date>> 
void for_each_date_in_year(int year, Fn func);

/// Convert a date into a string, following a format with numeric values separated by a sperator string.
///
/// @param[in] date  The date object
/// @param[in] format  The string format; only the formats dd_mm_yyyy, d_m_yyyy and yyyy_mm_dd are allowed by 
///					this function
/// @param[in] sep  Separator substring; cannot be empty
/// @return  The string
///
[[nodiscard]] std::wstring date_to_str(const Date& date, DateStrFormat format, const std::wstring& sep);

/// Convert a date into a string, following a format which does not use a sperator string.
///
/// @param[in] date  The date object
/// @param[in] format  The string format; only the formats dayth_month_year, day_mon_year, dow_day_month_year, 
///					dow_day_month and yyyymmdd are allowed by this function
/// @return  The string
///
[[nodiscard]] std::wstring date_to_str(const Date& date, DateStrFormat format);

/// Convert a string, which follows a format where numeric values are separated by a sperator string, into a 
/// date. The separator string cannot be empty.
///
/// @param[in] str  The string; an exception is thrown if it is not a valid textual representation of a date 
///					using the format specified 
/// @param[in] format  The string format; only the values dd_mm_yyyy, d_m_yyyy and yyyy_mm_dd are allowed
/// @param[in] sep  The substring which separates the numeric date values in the string
/// @return  The date
/// 
[[nodiscard]] Date date_from_str(std::wstring_view str, DateStrFormat format, std::wstring_view sep);

/// Convert a string, which follows a format where numeric values are not separated, into a date. 
/// The separator string cannot be empty.
///
/// @param[in] str  The string; an exception is thrown if it is not a valid textual representation of a date 
///					using the format specified 
/// @param[in] format  The string format; only the value yyyymmdd is currently allowed
/// @return  The date
/// 
[[nodiscard]] Date date_from_str(const std::wstring& str, DateStrFormat format);

} 

#include "Date.ipp"

#endif 

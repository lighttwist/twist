/// @file mdf5_utils.cpp
/// Implementation file for "mdf5_utils.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/md5_utils.hpp"

#include "twist/md5.hpp"

#include <cstdio>
#include <cctype>
#include <cstring>

#ifdef _WIN32
#include <fcntl.h>
#include <io.h>
#endif

namespace twist {

auto file_md5(const fs::path& path) -> std::string
{  
    /*	Build parameter quality control.  Verify machine
    	properties were properly set in md5.h and refuse
	    to run if they're not correct.  */
	
#ifdef CHECK_HARDWARE_PROPERTIES
    /*	Verify unit32 is, in fact, a 32 bit data type.  */
    if constexpr (sizeof(uint32) != 4) {
		TWIST_THRO2(L"** Configuration error.  Setting for uint32 in file md5.h\n"
		            L"   is incorrect.  This must be a 32 bit data type, but it\n"
		            L"   is configured as a {} bit data type.\n", ((int) sizeof(uint32) * 8));
    }
    
    /*	If HIGHFIRST is not defined, verify that this machine is,
    	in fact, a little-endian architecture.  */
	
#ifndef HIGHFIRST
    {	uint32 t = 0x12345678;
    
    	if (*((char *) &t) != 0x78) {
    	    TWIST_THRO2(L"** Configuration error.  Setting for HIGHFIRST in file md5.h\n"
			            L"   is incorrect.  This symbol has not been defined, yet this\n"
			            L"   machine is a big-endian (most significant byte first in\n"
			            L"   memory) architecture.  Please modify md5.h so HIGHFIRST is\n"
			            L"   defined when building for this machine.\n");
		}
    }
#endif
#endif    

	auto* in = (FILE*)nullptr;
	if ((in = _wfopen(path.c_str(), L"rb")) == NULL) {
		TWIST_THRO2(L"Cannot open input file {}", path.c_str());
	}

#ifdef _WIN32

	/** Warning!  On systems which distinguish text mode and
	binary I/O (MS-DOS, Macintosh, etc.) the modes in the open
        statement for "in" should have forced the input file into
        binary mode.  But what if we're reading from standard
	input?  Well, then we need to do a system-specific tweak
        to make sure it's in binary mode.  While we're at it,
        let's set the mode to binary regardless of however fopen
	set it.

	The following code, conditional on _WIN32, sets binary
	mode using the method prescribed by Microsoft Visual C 7.0
        ("Monkey C"); this may require modification if you're
	using a different compiler or release of Monkey C.	If
        you're porting this code to a different system which
        distinguishes text and binary files, you'll need to add
	the equivalent call for that system. */

	_setmode(_fileno(in), _O_BINARY);
#endif
    
    auto md5c = MD5Context{};
    MD5Init(&md5c);

	auto j = 0;
	auto buffer = std::vector<unsigned char>(16384);
	while ((j = static_cast<int>(fread(buffer.data(), 1, sizeof buffer, in))) > 0) {
		MD5Update(&md5c, buffer.data(), static_cast<unsigned int>(j));
	}
	    
   	fclose(in);

	auto signature = std::array<unsigned char, 16>{};
	MD5Final(signature.data(), &md5c);

	auto hash_stream = std::stringstream{};
	for (j = 0; j < sizeof signature; ++j) {
		hash_stream << std::setw(2) << std::setfill('0') << std::hex;
		hash_stream << static_cast<unsigned int>(signature[j]);
	}
	return hash_stream.str();		
}

}

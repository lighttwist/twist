///  @file  zlib_utils.cpp
///  Implementation file for "zlib_utils.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#pragma warning (disable : 5105)

#include "zlib_utils.hpp"

#include <fstream>

#include "external/zlib/include/zlib.h"

//+TODO: Possibly useful in the implementations of the *compress_file* family of functions. Dan 26Oct'23
//#include "external/zlib/include/zip.h"
//#include "external/zlib/include/unzip.h"

namespace twist::zlib {

CompressResult k_compress_ok{ Z_OK };
CompressResult k_compress_err_not_enough_mem{ Z_MEM_ERROR };
CompressResult k_compress_err_out_buf_too_small{ Z_BUF_ERROR };
CompressResult k_compress_err_invalid_level{ Z_STREAM_ERROR };

UncompressResult k_uncompress_ok{ Z_OK };
UncompressResult k_uncompress_err_not_enough_mem{ Z_MEM_ERROR };
UncompressResult k_uncompress_err_out_buf_too_small{ Z_BUF_ERROR };
UncompressResult k_uncompress_err_bad_input_data{ Z_DATA_ERROR };

//+TODO: Possibly useful in the implementations of the *compress_file* family of functions. Dan 26Oct'23
//extern "C" void gz_compress(FILE* in, gzFile out);
//extern "C" void gz_uncompress(gzFile in, FILE* out);
//extern "C" int main_minizip(int argc, char* argv[]);

// --- Local functions ---

void check_filename_len(const fs::path& filename)
{
	static const size_t k_max_filename_len = 1024;

    if (filename.wstring().size() >= k_max_filename_len) {
        TWIST_THROW(L"fs::path too long: \"%s\".", filename.c_str());
    }
}

// --- Global functions ---

const wchar_t* get_descr(CompressResult result)
{
	if (result == k_compress_ok) { 
		return L"Success";
	}
	if (result == k_compress_err_not_enough_mem) {
		return L"Not enough memory";
	}
	if (result == k_compress_err_out_buf_too_small) {
		return L"Not enough room in the output buffer";
	}
	if (result == k_compress_err_invalid_level) {
		return L"Level parameter invalid";
	}
	return L"Invalid result value";  
}

const wchar_t* get_descr(UncompressResult result)
{
	if (result == k_uncompress_ok) { 
		return L"Success";
	}
	if (result == k_uncompress_err_not_enough_mem) {
		return L"Not enough memory";
	}
	if (result == k_uncompress_err_out_buf_too_small) {
		return L"Not enough room in the output buffer";
	}
	if (result == k_uncompress_err_bad_input_data) {
		return L"The input data corrupted or incomplete";
	}
	return L"Invalid result value";  
}

CompressResult compress_buf(const unsigned char* src, unsigned long src_len, unsigned char* dest, 
		unsigned long& dest_len, int level)
{
	uLongf dest_len_z = dest_len;
	const CompressResult ret( compress2(dest, &dest_len_z, src, src_len, level) );
	assert(ret == k_compress_ok);
	dest_len = dest_len_z;
	return ret;
}

UncompressResult uncompress_buf(const unsigned char* src, unsigned long src_len, unsigned char* dest, 
		unsigned long& dest_len)
{
	uLongf dest_len_z = dest_len;
	const UncompressResult ret( uncompress(dest, &dest_len_z, src, src_len) );
	assert(ret == k_uncompress_ok);
	dest_len = dest_len_z;
	return ret;
}

/* +TODO: The experimental implementation(s) below do not currently compile. Dan 26Oct'23
void gz_compress_file(const fs::path& in_filename, const fs::path& out_filename)
{
    check_filename_len(in_filename);
    check_filename_len(out_filename);

	FILE* in{};
	gzFile out{};

#if TWIST_COMPILER_MSVC
    in = _wfopen(in_filename.c_str(), L"rb");
    out = gzopen_w(out_filename.c_str(), "wb");  // See gzopen() in zlib.h for the mode parameter
#else
    in = fopen(in_filename.c_str(), "rb");
    out = gzopen(out_filename.c_str(), "wb");  // See gzopen() in zlib.h for the mode parameter
#endif

    if (in == nullptr) {
        TWIST_THROW(L"Cannot open input file \"%s\".", in_filename.c_str());
    }
    if (out == nullptr) {
        TWIST_THROW(L"Cannot gzopen output file \"%s\".", out_filename.c_str());
    }
    gz_compress(in, out);
}

void gz_uncompress_file(const fs::path& in_filename, const fs::path& out_filename)
{
    check_filename_len(in_filename);
    check_filename_len(out_filename);

	gzFile in{};
	FILE* out{};

#if TWIST_COMPILER_MSVC
    in = gzopen_w(in_filename.c_str(), "rb");
    out = _wfopen(out_filename.c_str(), L"wb");
#else
    in = gzopen(in_filename.c_str(), "rb");
    out = fopen(out_filename.c_str(), "wb");
#endif

    gz_uncompress(in, out);
}

void zip_compress_file_or_dir(const fs::path& in_filename, const fs::path& out_filename)
{
	std::vector<std::string> args{
			"minizip", string_to_ansi(in_filename.c_str()), string_to_ansi(out_filename.c_str())};
	
	char* args_raw[3];
	args_raw[0] = args[0].data();
	args_raw[1] = args[1].data();
	args_raw[2] = args[2].data();

	main_minizip(3, args_raw);
}

int zip_compress_files(const std::vector<fs::path>& in_filenames, const fs::path& out_filename)
{
    zipFile zf = zipOpen(path_to_ansi(out_filename).c_str(), APPEND_STATUS_CREATE);
    if (zf == nullptr)
        return 1;

    bool result = true;
    for (size_t i = 0; i < in_filenames.size(); ++i) {

        std::fstream file(in_filenames[i].c_str(), std::ios::binary | std::ios::in);
        if (file.is_open()) {

            file.seekg(0, std::ios::end);
            const auto size = static_cast<unsigned int>(file.tellg());
            file.seekg(0, std::ios::beg);

            std::vector<char> buffer(size);
            if (size == 0 || file.read(&buffer[0], size)) {
                zip_fileinfo zfi = { 0 };
                std::wstring fileName = in_filenames[i].wstring().substr(
						in_filenames[i].wstring().rfind('\\') + 1);

                if (ZIP_OK == zipOpenNewFileInZip(zf, string_to_ansi(fileName).c_str(), 
						&zfi, nullptr, 0, nullptr, 0, nullptr, Z_DEFLATED, Z_DEFAULT_COMPRESSION)) {

                    if (zipWriteInFileInZip(zf, size == 0 ? "" : &buffer[0], size)) {
                        result = false;
					}
                    if (zipCloseFileInZip(zf)) {
                        result = false;
					}
                    file.close();
                    continue;
                }
            }
            file.close();
        }
        result = false;
    }

    if (zipClose(zf, nullptr))
        return 3;

    if (!result)
        return 4;

    return ZIP_OK;
}

int zip_uncompress_file(const fs::path& in_filename)
{
	static const size_t  k_max_filename = 512;  //+?
	static const char    k_dir_delimter = '/';  //+?
	static const size_t  k_read_size  = 8192;   //+?

    // Open the zip file
    unzFile zipfile = unzOpen(path_to_ansi(in_filename).c_str());
    if (zipfile == nullptr) {
        printf("%s: not found\n", path_to_ansi(in_filename).c_str());
        return -1;
    }

    // Get info about the zip file
    unz_global_info global_info;
    if (unzGetGlobalInfo( zipfile, &global_info ) != UNZ_OK) {
        printf("could not read file global info\n");
        unzClose( zipfile );
        return -1;
    }

    // Buffer to hold data read from the zip file.
    char read_buffer[ k_read_size ];

    // Loop to extract all files
    uLong i = 0;
    for (; i < global_info.number_entry; ++i) {
        // Get info about current file.
        unz_file_info file_info;
        char filename[k_max_filename];
        if (unzGetCurrentFileInfo(zipfile, &file_info, filename, k_max_filename, nullptr, 0, nullptr, 0) != 
				UNZ_OK) {
            printf( "could not read file info\n" );
            unzClose( zipfile );
            return -1;
        }

        // Check if this entry is a directory or file.
        const size_t filename_length = strlen( filename );
        if (filename[ filename_length-1 ] == k_dir_delimter) {
            // Entry is a directory, so create it.
            printf( "dir:%s\n", filename );
            fs::create_directory( filename );
        }
        else {
            // Entry is a file, so extract it.
            printf( "file:%s\n", filename );
            if (unzOpenCurrentFile( zipfile ) != UNZ_OK) {
                printf( "could not open file\n" );
                unzClose( zipfile );
                return -1;
            }

            // Open a file to write out the data.
            FILE *out = fopen( filename, "wb" );
            if (out == nullptr) {
                printf( "could not open destination file\n" );
                unzCloseCurrentFile( zipfile );
                unzClose( zipfile );
                return -1;
            }

            int error = UNZ_OK;
            do {
                error = unzReadCurrentFile( zipfile, read_buffer, k_read_size );
                if (error < 0) {
                    printf( "error %d\n", error );
                    unzCloseCurrentFile( zipfile );
                    unzClose( zipfile );
                    return -1;
                }

                // Write data to file.
                if (error > 0) {
                    fwrite( read_buffer, error, 1, out ); // You should check return of fwrite...
                }
            } 
			while (error > 0);

            fclose(out);
        }

        unzCloseCurrentFile( zipfile );

        // Go the the next entry listed in the zip file
        if ((i + 1) < global_info.number_entry) {
            if (unzGoToNextFile( zipfile ) != UNZ_OK) {
                printf( "cound not read next file\n" );
                unzClose( zipfile );
                return -1;
            }
        }
    }

    unzClose(zipfile);

    return 0;
}
*/

}

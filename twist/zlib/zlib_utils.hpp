///  @file  zlib_utils.hpp
///  Utilities for working with the "zlib" compression library

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_ZLIB_ZLIB__UTILS_HPP
#define TWIST_ZLIB_ZLIB__UTILS_HPP

#include "twist/ExplicitLibType.hpp"

namespace twist::zlib {

// Result of a compression action
typedef ExplicitLibType<int, -1, k_twist_lib_id, k_typeid_zlib_compress_result>  CompressResult;

// Result of an un-compression action
typedef ExplicitLibType<int, -1, k_twist_lib_id, k_typeid_zlib_uncompress_result>  UncompressResult;

extern CompressResult  k_compress_ok;                       // success
extern CompressResult  k_compress_err_not_enough_mem;       // error: there was not enough memory
extern CompressResult  k_compress_err_out_buf_too_small;    // error: not enough room in the output buffer
extern CompressResult  k_compress_err_invalid_level;        // error: level parameter is invalid

extern UncompressResult  k_uncompress_ok;                     // success
extern UncompressResult  k_uncompress_err_not_enough_mem;     // error: there was not enough memory
extern UncompressResult  k_uncompress_err_out_buf_too_small;  // error: not enough room in the output buffer
extern UncompressResult  k_uncompress_err_bad_input_data;     // error: the input data was corrupted or incomplete

/// Get a textual description of the result of a compression action.
///
/// @param[in] result  The result value
/// @return  The description
///
const wchar_t* get_descr(CompressResult result);

/// Get a textual description of the result of an un-compression action.
///
/// @param[in] result  The result value
/// @return  The description
///
const wchar_t* get_descr(UncompressResult result);

/// Compress a memory buffer.  
///
/// @param[in] src  The source (un-compressed) memory buffer
/// @param[in] src_len   The length (in bytes) of the source buffer
/// @param[in] dest  The destination (compressed) memory buffer
/// @param[in,out] dest_len  The length (in bytes) of the destination buffer; the value will be updated with the 
///                          length of compressed data 
/// @param[in] level  The compression level; 0 = no compression, 1 = best speed, 9 = best compression, 
///                   -1 = default level
/// @return  The compression result
///
CompressResult compress_buf(const unsigned char* src, unsigned long src_len, unsigned char* dest, 
		unsigned long& dest_len, int level = -1);

/// Un-compress a memory buffer.  
///
/// @param[in] src  The source (compressed) memory buffer
/// @param[in] src_len   The length (in bytes) of the source buffer
/// @param[in] dest  The destination (un-compressed) memory buffer
/// @param[in,out] dest_len  The length (in bytes) of the destination buffer; the value will be updated with the 
///                           length of un-compressed data 
/// @return  The un-compression result
///
UncompressResult uncompress_buf(const unsigned char* src, unsigned long src_len, unsigned char* dest, 
		unsigned long& dest_len);

/* +TODO: The experimental implementation(s) do not currently compile Dan 26Oct'23
// Work in progress   Dan 16Feb'15   
void gz_compress_file(const fs::path& in_filename, const fs::path& out_filename);

// Work in progress. Dan 16Feb'15   
void gz_uncompress_file(const fs::path& in_filename, const fs::path& out_filename);

// Work in progress. Dan 16Feb'15   
void zip_compress_file_or_dir(const fs::path& in_filename, const fs::path& out_filename);

// Work in progress. Dan 16Feb'15   
int zip_compress_files(const std::vector<fs::path>& in_filenames, const fs::path& out_filename);

// Work in progress. Dan 16Feb'15   
int zip_uncompress_file(const fs::path& in_filename);
*/

}

#endif 

/// @file ranges.ipp
/// Inline implementation file for "ranges.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/concepts.hpp"

namespace twist {

namespace detail {

template<class Pos, class Elem, class Iter>
[[nodiscard]] auto get_bounded_pos_range_impl(Iter first, 
                                              Iter last,
		                                      const twist::math::ClosedInterval<Elem>& interv) 
                    -> std::optional<twist::math::ClosedInterval<Pos>>
{
	using IterCat = typename std::iterator_traits<Iter>::iterator_category;
	static_assert(std::is_same_v<IterCat, std::random_access_iterator_tag>, "Not implemented for "
			      "non-random access iterators, as the number of iterator increments would be linear.");

	auto lo_bound = std::lower_bound(first, last, interv.lo());
	if (lo_bound == last) {
		return std::nullopt;
	}

	auto hi_bound = std::upper_bound(first, last, interv.hi());
	if (hi_bound == first) {
		return std::nullopt;
	}

	return std::make_optional<twist::math::ClosedInterval<Pos>>(lo_bound - first, hi_bound - first - 1);
}

template<rg::input_range Rng,
         InvocableR<bool, rg::range_value_t<Rng>, rg::range_value_t<Rng>> Comp>
[[nodiscard]] auto is_equally_spaced_impl(const Rng& range, Comp compare) 
                    -> std::tuple<bool, rg::range_value_t<Rng>>
{
    using std::begin;
    using std::end;
    using std::ssize;

    if (ssize(range) < 2) {
        TWIST_THRO2(L"The range must have at least two elements.");
    }

    auto step = std::optional<rg::range_value_t<Rng>>{};
    for (auto it = ++begin(range); it != end(range); ++it) {
        auto prev_it = std::prev(it);
        if (!step) {
            step = *it - *prev_it;
        }
        else {
            if (!compare(*it - *prev_it, *step)) {
                return {false, rg::range_value_t<Rng>{}};
            }
        }
    }
    return {true, *step};
}

}

template<class Rng, class> 
auto iter_at(const Rng& range, Ssize pos)
{
	auto iter = range.begin();
	std::advance(iter, pos);
	return iter;
}

template<class Rng, class> 
RangeElementCref<Rng> elem_at(const Rng& range, Ssize pos)
{
	auto it = iter_at(range, pos);
	assert(it != end(range));
	return *it;
}

template<class Rng, class> 
RangeElementCref<Rng> last_elem(const Rng& range)
{
	auto it = end(range);
	--it;  //+TODO: Test that it supports this operation first
	return *it;
}

template<class Rng1, class Rng2, class> 
RangeCompResult compare_ranges(const Rng1& range1, const Rng2& range2)
{
	if (rg::size(range1) != rg::size(range2)) {
		return RangeCompResult::different;
	}
	if (equal(range1, range2)) {
		return RangeCompResult::identical;
	}
	// The ranges are not identical. Check whether they contain the same elements, but in a different order.
	if (is_permutation(range1, range2)) {
		return RangeCompResult::permutation;
	}
	return RangeCompResult::different;
}

template<class Rng, class Pred, class>
auto find_nth_if(const Rng& range, Ssize match_idx, Pred pred)
{
	auto it = begin(range);
	const auto end_it = end(range);
	while (it != end_it) {
		it = std::find_if(it, end_it, pred);
		if (match_idx == 0) {
			break;
		}
		--match_idx;
		++it;
	}
	return it;
}

template<class Pos, class Rng, class, class>
[[nodiscard]] auto get_bounded_pos_range(const Rng& range,
		                                 const twist::math::ClosedInterval<RangeElement<Rng>>& interv)
                    -> std::optional<twist::math::ClosedInterval<Pos>>
{
	return detail::get_bounded_pos_range_impl<Pos, RangeElement<Rng>>(begin(range), end(range), interv);	
}

template<rg::input_range Rng>
[[nodiscard]] auto is_equally_spaced(const Rng& range) -> std::tuple<bool, rg::range_value_t<Rng>>
{
    return detail::is_equally_spaced_impl(range, std::equal_to{});
}

template<rg::input_range Rng>
[[nodiscard]] auto is_equally_spaced_tol(const Rng& range, rg::range_value_t<Rng> tol) 
                    -> std::tuple<bool, rg::range_value_t<Rng>>
{
    return detail::is_equally_spaced_impl(range, 
                                          [tol](auto lhs, auto rhs) { return twist::math::equal_tol(lhs, rhs, tol); });
}

}

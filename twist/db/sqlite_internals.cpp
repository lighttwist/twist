///  @file  sqlite_internals.cpp
///  Implementation file for "sqlite_internals.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "sqlite_internals.hpp"

namespace twist::db {

//
//  SqlitePreparedStatement::Impl class
//

SqlitePreparedStatement::Impl::Impl(sqlite3_stmt* stmt, sqlite3* conn)
	: stmt_{stmt}
	, conn_{conn}
{
	TWIST_CHECK_INVARIANT
}


sqlite3_stmt* SqlitePreparedStatement::Impl::stmt() const
{
	TWIST_CHECK_INVARIANT
	return stmt_;
}


sqlite3* SqlitePreparedStatement::Impl::conn() const
{
	TWIST_CHECK_INVARIANT
	return conn_;
}


#ifdef _DEBUG
void SqlitePreparedStatement::Impl::check_invariant() const noexcept
{
	assert(stmt_);
	assert(conn_);
}
#endif

//
//  Free functions
//

std::wstring get_sqlite3_error_descr(int err_no, sqlite3* conn)
{
	std::stringstream msg;                                                                 
	if (err_no != SQLITE_OK) {                                                                 								                                                             \
		
		msg << "An Sqlite3 error occurred: \"" << get_error_code_descr(err_no) << "\".";

		const char* err_descr = sqlite3_errmsg(conn);                                             
		if (err_descr != nullptr && strlen(err_descr) > 0) {                       
			msg << "\nDescription : " << err_descr << ".";                                                  
		}
	}
	else {
		msg << "No error occurred.";
	}

	return ansi_to_string(msg.str());
}


std::string get_error_code_descr(int err_no)
{
	switch (err_no) {
		case SQLITE_ERROR      : return "SQL error or missing database";
		case SQLITE_INTERNAL   : return "Internal logic error in SQLite";
		case SQLITE_PERM       : return "Access permission denied";
		case SQLITE_ABORT      : return "Callback routine requested an abort";
		case SQLITE_BUSY       : return "The database file is locked";
		case SQLITE_LOCKED     : return "A table in the database is locked";
		case SQLITE_NOMEM      : return "A malloc() failed";
		case SQLITE_READONLY   : return "Attempt to write a readonly database";
		case SQLITE_INTERRUPT  : return "Operation terminated by sqlite3_interrupt()";
		case SQLITE_IOERR      : return "Some kind of disk I/O error occurred";
		case SQLITE_CORRUPT    : return "The database disk image is malformed";
		case SQLITE_NOTFOUND   : return "Unknown opcode in sqlite3_file_control()";
		case SQLITE_FULL       : return "Insertion failed because database is full";
		case SQLITE_CANTOPEN   : return "Unable to open the database file";
		case SQLITE_PROTOCOL   : return "Database lock protocol error";
		case SQLITE_EMPTY      : return "Database is empty";
		case SQLITE_SCHEMA     : return "The database schema changed";
		case SQLITE_TOOBIG     : return "String or BLOB exceeds size limit";
		case SQLITE_CONSTRAINT : return "Abort due to constraint violation";
		case SQLITE_MISMATCH   : return "Data type mismatch";
		case SQLITE_MISUSE     : return "Library used incorrectly";
		case SQLITE_NOLFS      : return "Uses OS features not supported on host";
		case SQLITE_AUTH       : return "Authorization denied";
		case SQLITE_FORMAT     : return "Auxiliary database format error";
		case SQLITE_RANGE      : return "2nd parameter to sqlite3_bind out of range";
		case SQLITE_NOTADB     : return "File opened that is not a database file";
		case SQLITE_ROW        : return "sqlite3_step() has another row ready";
		case SQLITE_DONE       : return "sqlite3_step() has finished executing";
		default                : return "Unknown error";
	}
}

}

/// @file HeterogenousNumericTable.ipp
/// Inline implementation file for "HeterogenousNumericTable.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/std_variant_utils.hpp"

namespace twist::db {

// --- HeterogenousNumericTable class ---

template<class CellVal>
auto HeterogenousNumericTable::get_column_data(Ssize col_idx) const -> const std::vector<CellVal>&
{
    TWIST_CHECK_INVARIANT
    return columns_.at(col_idx).data<CellVal>();
}

template<class CellVal>
auto HeterogenousNumericTable::get_column_data(std::wstring_view col_name) const -> const std::vector<CellVal>&
{
    TWIST_CHECK_INVARIANT
    return columns_.at(get_column_index(col_name)).data<CellVal>();
}

template<class ...CellVals>
auto HeterogenousNumericTable::add_row(CellVals... cell_values) -> void
{
    TWIST_CHECK_INVARIANT
    static const auto N = sizeof...(CellVals);
    assert(columns_.size() == N);
    add_row_impl(std::make_index_sequence<N>{}, cell_values...);
    ++nof_rows_;
}

template<size_t... I, class... CellVals>
auto HeterogenousNumericTable::add_row_impl(std::index_sequence<I...>, CellVals... cell_values) -> void
{
    TWIST_CHECK_INVARIANT
    (columns_[I].add(cell_values), ...);
}

// --- HeterogenousNumericTable::Column class ---

template<class CellVal>
auto HeterogenousNumericTable::Column::data() const -> const std::vector<CellVal>&
{
    TWIST_CHECK_INVARIANT
    return get_vector<CellVal>(data_);
}

template<class CellVal>
auto HeterogenousNumericTable::Column::add(CellVal cell_val) -> void
{
    TWIST_CHECK_INVARIANT
    get_vector<CellVal>(data_).push_back(cell_val);
}

// --- Free functions ---

[[nodiscard]] constexpr auto is_valid(HeterogenousNumericTableColumnType type) -> bool
{
    return HeterogenousNumericTableColumnType::int32 <= type && type <= HeterogenousNumericTableColumnType::float64;
}

} 

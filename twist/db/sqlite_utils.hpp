///  @file  sqlite_utils.hpp
///  Utilities for working with SQLite databases

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_DB_SQLITE__UTILS_HPP
#define TWIST_DB_SQLITE__UTILS_HPP

namespace twist {
class Date;
}
namespace twist::db {
class SqliteRecord;
}

namespace twist::db {

/// Functor which will be called while reading a recordset, exactly once for each record in the recordset
typedef std::function<void (const SqliteRecord&)>  SqliteRecordCallback;

/// Possible results returned by evaluating a compiled (prepared) statement once
enum class SqliteStepResult {
	/// The statement has finished executing successfully; step should not be called again without first 
	/// calling reset
	done = 1,  
	/// The statement has returned data, which is ready for processing by the caller  
	row = 2
};

/// The type of storage of a value being bound to statement; for "static" storage, the value must be alive 
/// when step() is called; for "transient", the Sqlite engine keeps a copy of it.
enum class SqliteBoundValueStorage {
	statiq = 1,
	transient = 2
};

/// The type of compression used for compressing a BLOB stored in a database
enum class SqliteBlobCompression {
	none = 0,  ///< No compression
	zlib = 1   ///< Compression using the "zlib" compression library
};

/// Convert a date to an ANSI string usable as a value in an Sqlite database.
///
/// @param[in] date  The date
/// @return  The date string
///
std::string date_to_sqlite_ansi(const Date& date);

}  

#include "twist/db/sqlite_utils.ipp"

#endif  

///  @file  DatabaseColumnIndexer.hpp
///  DatabaseColumnIndexer class definition

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_DB_DATABASE_COLUMN_INDEXER_HPP
#define TWIST_DB_DATABASE_COLUMN_INDEXER_HPP

#include "twist/meta_ranges.hpp"

namespace twist::db {

///
///  Class which takes a list of database column names and indexes them so that retrieving a column's index is 
///  efficient; also provides a few related facilities.
///
class DatabaseColumnIndexer {
public:
	/// Constructor.
	///
	/// @tparam  Strings  Range type holding strings
	/// @param[in] col_names  The list of column names; an exception is thrown if the list is empty or if a 
	///					column name appears twice 
	///
	template<class Strings, 
	         class = EnableIfRangeGives<Strings, std::string_view>>
	explicit DatabaseColumnIndexer(const Strings& col_names);
	
	/// Get the index the column corresponding to a specific cell in the record.
	///
	/// @param[in] col_name  The name of the column; an exception is thrown if it is invalid
	/// @return  The column index
	///
	unsigned int get_col_idx(std::string_view col_name) const;

	/// Get the name the column corresponding to a specific cell in the record. 
	///
	/// @param[in] col_idx  The index of the column; an exception is thrown if it is invalid
	/// @return  The column name, or a blank string if index is invalid
	///
	std::string get_col_name(unsigned int col_idx) const;

	/// Get the name the column corresponding to a specific cell in the record, as a wide string. This method does not throw 
	/// exceptions.
	///
	/// @param[in] col_idx  The index of the column
	/// @return  The column name, or a blank string if index is invalid
	///
	std::wstring try_get_col_wname(unsigned int col_idx) const;

	/// Get the number of columns managed by this object.
	///
	/// @return  The number of columns
	///
	unsigned int nof_columns() const;

	TWIST_NO_COPY_DEF_MOVE(DatabaseColumnIndexer)

private:	
    std::map<std::string, unsigned int, std::less<>> col_name_indexes_;
	unsigned int num_cols_{0};

	TWIST_CHECK_INVARIANT_DECL
};

} 

#include "DatabaseColumnIndexer.ipp"

#endif 

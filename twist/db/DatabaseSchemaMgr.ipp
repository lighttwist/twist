///  @file   DatabaseSchemaMgr.ipp
///  Inline implementation file for "DatabaseSchemaMgr.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/file_io.hpp"

namespace twist::db {

//
//  DatabaseSchemaMgr class template
//

template<class Conn>
DatabaseSchemaMgr<Conn>::DatabaseSchemaMgr(const fs::path& filename, unsigned int cur_db_version, 
		unsigned int oldest_supported_db_version)
	: DatabaseSchemaMgrBase{ filename, cur_db_version, oldest_supported_db_version }
	, reorganise_listener_{ nullptr }
{
	TWIST_CHECK_INVARIANT
}


template<class Conn>
DatabaseSchemaMgr<Conn>::~DatabaseSchemaMgr()
{
	TWIST_CHECK_INVARIANT
}


template<class Conn>
void DatabaseSchemaMgr<Conn>::create_db()
{
	TWIST_CHECK_INVARIANT
	const auto db_path = path();

	if (exists(db_path)) {
		// Delete the existing file.
		remove(db_path);
	}

	try {
		// Create the new, empty database file
		std::unique_ptr<Conn> connection = create_db_file();
		// Create the database schema 
		create_schema(*connection);
		// Write the current version number to the database
		write_db_version(get_cur_db_version(), *connection);
	}
	catch (RuntimeError& ex) {
		ex.prepend(std::format(L"Error creating database file \"{}\". ", db_path.c_str()));
		throw;
	}

	TWIST_CHECK_INVARIANT
}
    

template<class Conn>
void DatabaseSchemaMgr<Conn>::reorganise_db(unsigned int& old_version)
{
	TWIST_CHECK_INVARIANT
	const auto db_path = path();

	if (!exists(db_path)) {
		TWIST_THRO2(L"Database file \"{}\" does not exist.", db_path.wstring().c_str());
	}
    
    // Get current database version
    {
		std::unique_ptr<Conn> connection = open_db_file_read();
		old_version = read_db_version(*connection);
    }

	const unsigned int cur_version = get_cur_db_version();

    if (old_version < get_oldest_supported_db_version()) {
		TWIST_THROW(L"File \"%s\" has schema version \"%d\", which is unsupported. "
				L"Version \"%d\" is the oldest supported.", db_path.wstring().c_str(), old_version, 
				get_oldest_supported_db_version());
	}
    else if (old_version > cur_version) {
		TWIST_THROW(L"The format of file \"%s\" is newer than the application. Cannot open file.", 
				db_path.wstring().c_str());
	}
    else if (old_version < cur_version) {
		// Reorganise the database to the current version
		
        // Save a backup copy in case the reorganization goes wrong
		const auto bkp_path = append_extension(db_path, L".backup");
        copy(db_path, bkp_path);

        try {			
			// Open a database connection to be used throughout the reorganise
			std::unique_ptr<Conn> connection = open_db_file_write_exclusive();
			
			for (unsigned int version = old_version + 1; version <= cur_version; ++version) {
				
				if (reorganise_listener_ != nullptr) {
					reorganise_listener_->before_reorganise_to_version(version, *connection);
				}
				
				reorganise_db_to_version(version, *connection);
				
				if (reorganise_listener_ != nullptr) {
					reorganise_listener_->after_reorganise_to_version(version, *connection);
				}
			}

            // Reorganise done. Update the database version to the latest version.
			write_db_version(cur_version, *connection);
        }
        catch (std::exception&) {
			try {
				copy_file(bkp_path, db_path);
				remove(bkp_path);
			}
			catch (...) {/*Make sure nothing else throws*/}
            throw;
        }

        remove(bkp_path);        
	}
	
	TWIST_CHECK_INVARIANT
}


template<class Conn>
unsigned int DatabaseSchemaMgr<Conn>::get_db_version()
{
	TWIST_CHECK_INVARIANT
	if (!exists(path())) {
		TWIST_THROW(L"Database file \"%s\" does not exist.", path().wstring().c_str());
	}    
	auto connection = open_db_file_read();
	return read_db_version(*connection);
}


template<class Conn>
void DatabaseSchemaMgr<Conn>::reorganise_db(unsigned int& old_version, 
		DatabaseReorganiseListener<Conn>& listener)
{
	TWIST_CHECK_INVARIANT
	reorganise_listener_ = &listener;
	reorganise_db(old_version);
	reorganise_listener_ = nullptr;
}


template<class Conn>
bool DatabaseSchemaMgr<Conn>::is_db_version_up_to_date(std::wstring& special_reorganise_msg)
{
	TWIST_CHECK_INVARIANT
	if (!exists(path())) {
		TWIST_THROW(L"Database file \"%s\" does not exist.", path().wstring().c_str());
	}

    // Read the version number from the database.
	std::unique_ptr<Conn> connection = open_db_file_read();
	const unsigned int db_version = read_db_version(*connection);

	const bool ret = db_version == get_cur_db_version();
	if (!ret) {
		special_reorganise_msg = get_special_reorganise_message(db_version);
	}

	TWIST_CHECK_INVARIANT
	return ret;
}


template<class Conn>
void DatabaseSchemaMgr<Conn>::reorganise_db_to_version(unsigned int /*version*/, Conn& /*connection*/)
{
	TWIST_CHECK_INVARIANT
	TWIST_THROW(L"This method must be overridden for databases which need reorganising!");
}


#ifdef _DEBUG
template<class Conn>
void DatabaseSchemaMgr<Conn>::check_invariant() const noexcept
{
}
#endif 

//
//  DatabaseReorganiseListener class template
//

template<class Conn>
DatabaseReorganiseListener<Conn>::~DatabaseReorganiseListener()
{
}

} 

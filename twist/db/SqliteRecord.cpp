///  @file  SqliteRecord.cpp
///  Implementation file for "SqliteRecord.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "SqliteRecord.hpp"

#include "DatabaseColumnIndexer.hpp"

namespace twist::db {

SqliteRecord::SqliteRecord()
{
	TWIST_CHECK_INVARIANT 
}


SqliteRecord::~SqliteRecord()
{
	TWIST_CHECK_INVARIANT
}


unsigned long long SqliteRecord::count_cells() const
{	
	TWIST_CHECK_INVARIANT
	return col_indexer_ ? col_indexer_->nof_columns() : 0;
}


bool SqliteRecord::is_null(std::string_view col_name) const
{
	TWIST_CHECK_INVARIANT
	return is_null(get_col_idx(col_name));
}

	
bool SqliteRecord::is_null(unsigned int col_idx) const
{
	TWIST_CHECK_INVARIANT
	if (col_idx >= count_cells()) {
		TWIST_THROW(L"Column index %d out of bounds.", col_idx);
	}
	return cell_data_[col_idx] == nullptr;
}


std::wstring SqliteRecord::get_text(std::string_view col_name) const
{
	TWIST_CHECK_INVARIANT
	return get_text(get_col_idx(col_name));
}
	

std::wstring SqliteRecord::get_text(unsigned int col_idx) const
{
	return ansi_to_string(get_raw_non_null_value(col_idx));
}


std::string SqliteRecord::get_ansi(std::string_view col_name) const
{
	TWIST_CHECK_INVARIANT
	return get_ansi(get_col_idx(col_name));
}


std::string SqliteRecord::get_ansi(unsigned int col_idx) const
{
	TWIST_CHECK_INVARIANT
	return get_raw_non_null_value(col_idx);
}

	
long SqliteRecord::get_int(std::string_view col_name) const
{
	TWIST_CHECK_INVARIANT
	return get_int(get_col_idx(col_name));
}


long SqliteRecord::get_int(unsigned int col_idx) const
{
	TWIST_CHECK_INVARIANT
	return atol(get_raw_non_null_value(col_idx));
}

	
double SqliteRecord::get_real(std::string_view col_name) const
{
	TWIST_CHECK_INVARIANT
	return get_real(get_col_idx(col_name));
}


double SqliteRecord::get_real(unsigned int col_idx) const
{
	TWIST_CHECK_INVARIANT
	return atof(get_raw_non_null_value(col_idx));
}

	
bool SqliteRecord::get_bool(std::string_view col_name) const
{
	TWIST_CHECK_INVARIANT
	return get_bool(get_col_idx(col_name));
}


bool SqliteRecord::get_bool(unsigned int col_idx) const
{
	TWIST_CHECK_INVARIANT
	const char* value = get_raw_non_null_value(col_idx);
	if (strcmp(value, "0") == 0) {
		return false;
	}
	if (strcmp(value, "1") == 0) {
		return true;
	}
	TWIST_THROW(L"The record cell %d (column \"%s\") does not store a BOOLEAN value.", col_idx, col_indexer_->try_get_col_wname(col_idx).c_str());
}


void SqliteRecord::set_cols(unsigned int num_cols, char** col_names)
{
	if (col_indexer_) {
		TWIST_THROW(L"The recordset column information has already been set.");
	}
	if (num_cols > 0) {
		std::vector<std::string> column_names;
		std::copy(col_names, col_names + num_cols, back_inserter(column_names));
		col_indexer_ = std::make_unique<DatabaseColumnIndexer>(column_names);
	}
}


unsigned int SqliteRecord::get_col_idx(std::string_view col_name) const
{
	TWIST_CHECK_INVARIANT
	if (!col_indexer_) {
		TWIST_THROW(L"The recordset is empty.");
	}
	return col_indexer_->get_col_idx(col_name);
}


const char* SqliteRecord::get_raw_non_null_value(unsigned int col_idx) const
{
	TWIST_CHECK_INVARIANT
	if (!col_indexer_) {
		TWIST_THROW(L"The recordset is empty.");
	}
	if (col_idx >= count_cells()) {
		TWIST_THROW(L"Column index %d out of bounds.", col_idx);
	}

	const char* value = cell_data_[col_idx];
	if (value == nullptr) {
		TWIST_THROW(L"The value in record cell %d (column \"%s\") is NULL.", col_idx, col_indexer_->try_get_col_wname(col_idx).c_str());
	}
	return value;
}


#ifdef _DEBUG
void SqliteRecord::check_invariant() const noexcept
{
	assert((col_indexer_.get() == nullptr) == (cell_data_ == nullptr));
}
#endif

}

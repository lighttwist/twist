///  @file  SqliteRecord.hpp
///  SqliteRecord class

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_DB_SQLITE_RECORD_HPP
#define TWIST_DB_SQLITE_RECORD_HPP

namespace twist::db {

class DatabaseColumnIndexer;

///
///  Class which holds data from a number of cells - each cell corresponding to a column - belonging to one or more of an Sqlite 
///  database's tables; the format of the record itself is that of a row (which can have zero cells). 
///
class SqliteRecord : NonCopyable {
public:
	~SqliteRecord();

	/// Get the number of cells in the record.
	///
	/// @return  The number of cells
	///
	unsigned long long count_cells() const;
	
	/// Find out whether the value in a specific cell is NULL.
	///
	/// @param[in] col_name  The name of the column corresponding to the cell; an exception is thrown if it is invalid
	/// @return  true if the cell holds a NULL value
	///
	bool is_null(std::string_view col_name) const;
	
	/// Find out whether the value in a specific cell is NULL.
	///
	/// @param[in] col_idx  The index of the column corresponding to the cell; an exception is thrown if it is invalid
	/// @return  true if the cell holds a NULL value
	///
	bool is_null(unsigned int col_idx) const;
	
	/// Get the text value stored in a specific cell.
	///
	/// @param[in] col_name  The name of the column corresponding to the cell; an exception is thrown if it is invalid
	/// @return  The text value
	///
	std::wstring get_text(std::string_view col_name) const;
	
	/// Get the text value stored in a specific cell.
	///
	/// @param[in] col_idx  The index of the column corresponding to the cell; an exception is thrown if it is invalid
	/// @return  The text value
	///
	std::wstring get_text(unsigned int col_idx) const;

	/// Get the ANSI text value stored in a specific cell.
	///
	/// @param[in] col_name  The name of the column corresponding to the cell; an exception is thrown if it is invalid
	/// @return  The text value
	///
	std::string get_ansi(std::string_view col_name) const;
	
	/// Get the ANSI text value stored in a specific cell.
	///
	/// @param[in] col_idx  The index of the column corresponding to the cell; an exception is thrown if it is invalid
	/// @return  The text value
	///
	std::string get_ansi(unsigned int col_idx) const;
	
	/// Get the integer number value stored in a specific cell.
	///
	/// @param[in] col_name  The name of the column corresponding to the cell; an exception is thrown if it is invalid
	/// @return  The integer number value
	///
	long get_int(std::string_view col_name) const;
	
	/// Get the integer number value stored in a specific cell.
	///
	/// @param[in] col_idx  The index of the column corresponding to the cell; an exception is thrown if it is invalid
	/// @return  The integer number value
	///
	long get_int(unsigned int col_idx) const;
	
	/// Get the floating-point number value stored in a specific cell.
	///
	/// @param[in] col_name  The name of the column corresponding to the cell; an exception is thrown if it is invalid
	/// @return  The floating-point number value
	///
	double get_real(std::string_view col_name) const;
	
	/// Get the floating-point number value stored in a specific cell.
	///
	/// @param[in] col_idx  The index of the column corresponding to the cell; an exception is thrown if it is invalid
	/// @return  The floating-point number value
	///
	double get_real(unsigned int col_idx) const;

	/// Get the boolean value stored in a specific cell. An exception is thrown if the cell does not store a value convertible 
	/// to boolean.
	///
	/// @param[in] col_name  The name of the column corresponding to the cell; an exception is thrown if it is invalid
	/// @return  The boolean value
	///
	bool get_bool(std::string_view col_name) const;
	
	/// Get the boolean value stored in a specific cell. An exception is thrown if the cell does not store a value convertible 
	/// to boolean.
	///
	/// @param[in] col_idx  The index of the column corresponding to the cell; an exception is thrown if it is invalid
	/// @return  The boolean value
	///
	bool get_bool(unsigned int col_idx) const;

private:
	/// Constructor
	///
	SqliteRecord();

	/// Set the recordset columns.
	/// 
	/// @param[in] num_cols  The number of columns in the recordset
	/// @param[in] col_names  An array containing the column names 
	///
	void set_cols(unsigned int num_cols, char** col_names);

	/// Get the index the column corresponding to a specific cell in the record.
	///
	/// @param[in] col_name  The name of the column; an exception is thrown if it is invalid
	/// @return  The column index
	///
	unsigned int get_col_idx(std::string_view col_name) const;

	/// Get the raw, non-NULL value stored in a specific cell. An exception is thrown if the cell contains a NULL value.
	///
	/// @param[in] col_idx  The index of the column corresponding to the cell; an exception is thrown if it is invalid
	/// @return  The raw value
	///
	const char* get_raw_non_null_value(unsigned int col_idx) const;

	std::unique_ptr<DatabaseColumnIndexer> col_indexer_;
    char** cell_data_{nullptr};

	friend class SqliteConnection;	
	
	TWIST_CHECK_INVARIANT_DECL
};

}

#endif 


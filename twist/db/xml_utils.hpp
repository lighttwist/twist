/// \file xml_utils_new.hpp
/// Utilities for working with XML documents. 

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_DB_XML__UTILS_HPP
#define TWIST_DB_XML__UTILS_HPP

#include <fstream>

namespace twist::db {
class XmlElemNode;
}

namespace twist::db {

//! XML document class.
class XmlDoc final {
public:
	//! Create a new, empty XML document.
	[[nodiscard]] static auto make_empty() -> std::unique_ptr<XmlDoc>; 

	/*! Creates a new XML document and read an XML string into it.
	    \param[in] xml_string  The XML string
	    \return  The document
	 */
	[[nodiscard]] static auto read_from_string(const std::wstring& xml_string) -> std::unique_ptr<XmlDoc>;

	/*! Create a new XML document and read an XML file into it.
	    \param[in] path  The file path
	    \return  The document
	 */
	[[nodiscard]] static auto read_from_file(const fs::path& path) -> std::unique_ptr<XmlDoc>; 

	~XmlDoc();

	//! Get the root element of the document. If the document does not contain a root element, an exception is thrown. 
	[[nodiscard]] auto root_elem() -> XmlElemNode;
	
	/// Create the root element node of the document. An exception is thrown if the root element already exists.
	///
	/// \param[in] tag  The root element node tag.
	/// \return  The root node.
	///
	XmlElemNode create_root_elem(const std::wstring& tag);

	/// save the document to an XML file.
	///
	/// \param[in] path  The XML file path; if a file with this filename already exists, it is deleted
	///
	void save(const fs::path& path) const;
	
	/// Get the whole XML representation of the document as a string.
	///
	/// \return  The XML text.
	///
	std::wstring get_xml_text() const;

	/*! Create a new XML element node. 
	    \param[in] tag  The element node's tag
	    \param[in] parent  The parent of the new element node
	    \return  The new element node
	 */
	[[nodiscard]] auto create_elem_node(const std::wstring& tag, const XmlElemNode& parent) -> XmlElemNode;

	/*! Create a new XML element node containing a "text subnode". 
	    \param[in] tag  The element node's tag.
	    \param[in] value  The text subnode's value.
	    \param[in] parent  The parent node of the new element node.
	    \return  The new element node.
	 */
	auto create_elem_node_with_text(const std::wstring& tag, const std::wstring& value, const XmlElemNode& parent)
          -> XmlElemNode;	
	
	/// Create a new XML element node containing a "text subnode" which describes an integer value.
	///
	/// \param[in] tag  The element node's tag.
	/// \param[in] value  The text subnode's value.
	/// \param[in] parent  The parent node of the new element node.
	/// \return  The new element node.
	///
	XmlElemNode create_elem_node_with_int(const std::wstring& tag, int value, const XmlElemNode& parent);

	/// Create a new XML element node containing a "text subnode" which describes a floating-point value.
	///
	/// \param[in] tag  The element node's tag.
	/// \param[in] value  The text subnode's value.
	/// \param[in] parent  The parent node of the new element node.
	/// \return  The new element node.
	///
	XmlElemNode create_elem_node_with_float(const std::wstring& tag, double value, const XmlElemNode& parent);	

	/// Create a new XML element node containing a "text subnode" which describes a boolean value.
	///
	/// \param[in] tag  The element node's tag.
	/// \param[in] value  The text subnode's value.
	/// \param[in] parent  The parent node of the new element node.
	/// \return  The new element node.
	///
	XmlElemNode create_elem_node_with_bool(const std::wstring& tag, bool value, const XmlElemNode& parent);	

	/// Given a parent XML node, remove a specific child XML node from its list of children.
	///
	/// \param[in] parent  The parent node
	/// \param[in] child  The child node to be removed
	///
	void remove_child_node(const XmlElemNode& parent, const XmlElemNode& child);

	/// Given a parent XML node, delete all its child XML nodes which have a specific tag.
	///
	/// \param[in] parent  The parent node
	/// \param[in] tag  The children's tag
	/// \return  The number of child nodes deleted
	///
	size_t remove_child_nodes_with_tag(const XmlElemNode& parent, const std::wstring& tag);

private:
	/// Constructor.
	///
	XmlDoc();

	class Impl;

	std::unique_ptr<Impl>  pimpl_;
	
	TWIST_NO_COPY_NO_MOVE(XmlDoc)
	TWIST_CHECK_INVARIANT_DECL
};

/*! Simple class representing an XML attribute node, with two fields: name, and value.
    An XML node cannot have two attributes with the same name.
 */
class XmlNodeAttr {
public:
	/*! Constructor; use it when the attribute value is a string value.
        \param[in] name  The attribute name
	    \param[in] value  The attribute value
	 */
	explicit XmlNodeAttr(std::wstring_view name, std::wstring_view value);

	/*! Constructor; use it when the attribute value is an integer value.
        \param[in] name  The attribute name
	    \param[in] value  The attribute value
	 */
	explicit XmlNodeAttr(std::wstring_view name, int value);
	
	//! Get the attribute name.
	[[nodiscard]] auto get_name() const -> std::wstring;

	//! Get the attribute value.
	[[nodiscard]] auto get_value() const -> std::wstring;

	//! Get the attribute value as an integer.
	[[nodiscard]] auto get_int_value() const -> int;

	//! Get the attribute value as a floating-point numerical value.
	[[nodiscard]] auto get_float_value() const -> double;

private:	
	std::wstring name_;
	std::wstring value_;

	TWIST_CHECK_INVARIANT_DECL
};

using XmlNodeAttrList = std::vector<XmlNodeAttr>;
using XmlElemNodeList = std::vector<XmlElemNode>;

/*! This class represents a XML element node: a node that always has a tag and, optionally, can have text (ie one text 
    subnode) and a list of attributes (i.e. attribute subnodes).
 */
class XmlElemNode {
public:	
	//! XML node multiplicity
	enum NodeMult {
		k_zero,			// No nodes
		k_zero_or_one,	// Either none or one node
		k_unique,		// Exactly one node
		k_one_or_more	// One or more modes
	};
	
	/// Get the node's tag.
	///
	/// \return  The tag
	///
	std::wstring tag() const;  //+DEPRECATED use tag()	

	/// Get the node's tag.
	///
	/// \return  The tag.
	///
	std::wstring get_tag() const;
	
	/// Find out whether the node has a text subnode.
	///
	/// \return  true only if so
	///
	bool has_text() const;
	
	/// Get the value of the node's text subnode. An exception is thrown if the node has no text subnode.
	///
	/// \return  The text
	///
	std::wstring text() const;
	
	/// Get the value of the node's text subnode. An exception is thrown if the node has no text subnode.
	///
	/// \return  The text
	///
	std::wstring get_text() const;  //+DEPRECATED use text()

	/// Get the value of the node's text subnode, as an integer number. An exception is thrown if the node has no 
	/// text subnode, or if the text does not represent an integer number.
	///
	/// \return  The number.
	///
	int get_text_as_int() const;  //+DEPRECATED use free function text_as_int()

	/// Get the value of the node's text subnode, as a boolean. An exception is thrown if the node has no 
	/// text subnode, or if the text does not represent a boolean.
	///
	/// \return  The boolean.
	///
	bool get_text_as_bool() const;  //+DEPRECATED use free function text_as_bool()

	/// Set the value of the node's text subnode. If the node has no text subnode, a text subnoded is added.
	///
	/// \param[in] text  The text.
	///
	void set_text(const std::wstring& text);

	/// Set the value of the node's text subnode to a string encoding a boolean value. If the node has no text subnode, a text 
	/// subnoded is added.
	///
	/// \param[in] value  The value.
	///
	void set_text_from_bool(bool value);

	/// Set the value of the node's text subnode to a string encoding an integer value. If the node has no text subnode, a text 
	/// subnoded is added.
	///
	/// \param[in] value  The value.
	///
	void set_text_from_int(int value);
		
	/// Get the number of element nodes which are children of this node, and have a specific tag.
	///
	/// \param[in] tag  The tag.
	/// \return  The count.
	///
	int count_children_with_tag(const std::wstring& tag) const;

	/// Get a list of the element nodes which are children of this node, and have a specific tag (if any).
	///
	/// \param[in] tag  The tag
	/// \return  The matching child nodes
	///
	XmlElemNodeList get_children_with_tag(const std::wstring& tag) const;
	
	/// Get the unique element node that is a child of this node, and has a specific tag.
	/// If no such unique node exists, an exception is thrown.
	///
	/// \param[in] tag  The tag.
	/// \return  The child element node.
	///
	XmlElemNode get_unique_child_with_tag(const std::wstring& tag) const;

	/// Search for an unique element node that is a child of this node, and has a specific tag.
	/// If multiple nodes with the specified tag exist as children of this node, then an exception is thrown.
	///
	/// \param[in] tag  The tag
	/// \return  The matching child element node, or an empty pointer if there is no such unique node
	///
	std::unique_ptr<XmlElemNode> find_unique_child_with_tag(const std::wstring& tag) const;
	
	/// Validate children element nodes with a specific tag. The multiplicity of the children is examined, and optionally 
	/// whether they all contain text.
	///
	/// \param[in] tag  Child nodes tag.
	/// \param[in] mult  The multiplicity of the child nodes. The validation fails if the real multiplicity does not match 
	///				this (eg if  mult  is  k_zero_or_one  and there are three child nodes with the given tag).
	/// \param[in] checkText  If this flag is true and any of the child nodes with the given tag do not contain text, the
	///				validation fails.
	/// \param[out]  err  If non-nullptr and the validation failed, this is a description of the reason why.
	/// \return  true if the validation succeeds.
	///
	bool validate_children(const std::wstring& tag, NodeMult mult, bool checkText, 
			std::wstring* err = nullptr) const;

	/// Get a list of the element nodes which are descendants of this node, and have a specific tag (if any).
	///
	/// \param[in] tag  The tag
	/// \return  The matching descendant nodes
	///
	XmlElemNodeList get_descendants_with_tag(const std::wstring& tag) const;

	/// Find out whether this node has an attribute with a specific name.
	///
	/// \param[in] attr_name  The attribute name.
	/// \return  true if so.
	///
	bool has_attr(const std::wstring& attr_name) const;
	
	/// Get an attribute of this node, with a specific name. An exception is thrown if this node has no 
	/// attribute with the specified name. 
	///
	/// \param[in] attr_name  The attribute name.
	/// \return  The attribute node.
	///
	XmlNodeAttr get_attr(const std::wstring& attr_name) const;

	/// Set an attribute of the node. If an attribute with the same name as the new attribute already exists 
	/// for the node, it will be replaced with the new attribute.
	///
	/// \param[in] attr  The attribute.
	/// 
	void set_attr(const XmlNodeAttr& attr);	
	
private:
	class Impl;
	
	/// Constructor.
	///
	/// \param[in] pimpl  The internal implementation object (using the PIMPL idiom)
	///	
	XmlElemNode(std::shared_ptr<Impl> pimpl);

	std::shared_ptr<Impl>  pimpl_; 

	friend class XmlDoc;

	TWIST_CHECK_INVARIANT_DECL
};

// --- Free functions ---

/*! Create a new XML document, containing only the root XML node with one attribute, the version number. 
    The attribute name is "version".
    \param[in] root_xnode_tag  The tag of the root XML node
    \param[in] version  The document version
    \return  The XML document
 */
[[nodiscard]] auto init_xdoc(std::wstring_view root_xnode_tag, unsigned int version) -> std::unique_ptr<XmlDoc>;

/*! Read an XML file into an XML document. The root XML node is expected to have a document version attribute, named 
    "version".
    \param[in] path  The XML file path
    \param[in] root_xnode_tag  The expected tag of the root XML node; if the root node in the XML string has a 
				               different name, then an exception will be thrown
    \param[out] version  The document version; if the top XML node either does not have a version attribute, then an 
	                     exception will be thrown
    \return  The XML document
 */
[[nodiscard]] auto read_xdoc_and_version_from_file(const fs::path& path, 
                                                   std::wstring_view root_xnode_tag,
		                                           unsigned int& version) -> std::unique_ptr<XmlDoc>;

/*! Read an XML file into an XML document. The root XML node is expected to have a document version attribute, named 
    "version", which is expected to have a specific value.
    \param[in] path  The XML file path
    \param[in] root_xnode_tag  The expected tag of the root XML node; if the root node in the XML string has a 
	                           different name, then an exception will be thrown
    \param[in] version  The expected document version; if the top XML node either does not have a version attribute, or 
	                    the attribute's value differs from this argument, then an exception will be thrown
    \return  The XML document
 */
[[nodiscard]] auto read_xdoc_from_file_with_version(const fs::path& path, 
                                                    std::wstring_view root_xnode_tag, 
                                                    unsigned int version) -> std::unique_ptr<XmlDoc>;

/*! Read an XML string into an XML document. The root XML node is expected to have a document version attribute, named 
    "version".
    \param[in] xml_str  The XML string
    \param[in] root_xnode_tag  The expected tag of the root XML node; if the root node in the XML string has a 
	                           different name, then an exception will be thrown
    \param[in] version  The expected document version; if the top XML node either does not have a version attribute, an 
	                    exception will be thrown
    \return  The XML document
 */
[[nodiscard]] auto read_xdoc_and_version_from_string(const std::wstring& xml_str, 
                                                     std::wstring_view root_xnode_tag, 
		                                             unsigned int& version) -> std::unique_ptr<XmlDoc>;

/// Read an XML string into an XML document. The root XML node is expected to have a document version 
/// attribute, named "version", which is expected to have a specific value.
///
/// \param[in] xml_str  The XML string
/// \param[in] root_xnode_tag  The expected tag of the root XML node; if the root node in the XML string has 
///					a different name, an exception will be thrown
/// \param[in] version  The expected document version; if the top XML node either does not have a version 
///					attribute, or the attribute's value differs from this argument, then an exception will 
///					be thrown
/// \return  The XML document
///
std::unique_ptr<XmlDoc> read_xdoc_from_string_with_version(const std::wstring& xml_str, 
		                                                   std::wstring_view root_xnode_tag, 
                                                           unsigned int version);

/// Get the version number for an XML document which has a specific structure, that is, it has a root XML node 
/// with a specific tag, which has an attribute named "version". An exception is thrown if the document does 
/// not have the expected structure.
///
/// \param[in] xdoc  The XML document
/// \param[in] root_xnode_tag  The expected root XML node tag
/// \param[out]  version  The document version
///
void get_xml_doc_version(XmlDoc& xdoc, std::wstring_view root_xnode_tag, unsigned int& version);

/// Set the version number for an XML document which has a specific structure, that is, it has a root XML node with a specific 
/// tag. An attribute an attribute named "version" is added to (or updated in) the root XML node. An exception is thrown if the 
/// document does not have the expected structure.
///
/// \param[in] xdoc  The XML document
/// \param[in] root_xnode_tag  The expected root XML node tag
/// \param[in] version  The document version
///
void set_xml_doc_version(XmlDoc& xdoc, std::wstring_view root_xnode_tag, unsigned int version);

/*! Get the value of an XML element node's text subnode, as an integer number. 
    \param[in] xnode  The element node; an exception is thrown if it has no text subnode, or if the text does not 
				      represent an integer number
    \return  The number
 */
[[nodiscard]] auto text_as_int(const XmlElemNode& xnode) -> int;

/*! Get the value of an XML element node's text subnode, as an unsigned 32-bit integer number. 
    \param[in] xnode  The element node; an exception is thrown if it has no text subnode, or if the text does not 
				      represent such a number
    \return  The number
 */
[[nodiscard]] auto text_as_uint32(const XmlElemNode& xnode) -> uint32_t;

/*! Get the value of an XML element node's text subnode, as a Ssize value. 
    \param[in] xnode  The element node; an exception is thrown if it has no text subnode, or if the text does not 
				      represent an integer number
    \return  The Ssize value
 */
[[nodiscard]] auto text_as_ssize(const XmlElemNode& xnode) -> Ssize;

/*! Get the value of an XML element node's text subnode, as an enum value. 
    \tparam Enum  The enum type
    \param[in] xnode  The element node; an exception is thrown if it has no text subnode, or if the text does not 
				      represent an integer number
    \return  The corresponding enum value (which may or may not be a defined enum value)
 */
template<class Enum> 
requires std::is_enum_v<Enum>
[[nodiscard]] auto text_as_enum(const XmlElemNode& xnode) -> Enum;

/*! Get the value of an XML element node's text subnode, as a 32-bit floating-point number. 
    \param[in] xnode  The element node; an exception is thrown if it has no text subnode, or if the text does not 
				      represent a floating-point number
    \return  The number
 */
[[nodiscard]] auto text_as_float(const XmlElemNode& xnode) -> float;

/*! Get the value of an XML element node's text subnode, as a 64-bit floating-point number. 
    \param[in] xnode  The element node; an exception is thrown if it has no text subnode, or if the text does not 
				      represent a floating-point number
    \return  The number
 */
[[nodiscard]] auto text_as_double(const XmlElemNode& xnode) -> double;

/*! Get the value of an XML element node's text subnode, as a boolean. 
    \param[in] xnode  The element node; an exception is thrown if it has no text subnode, or if the text does not 
  			          represent a boolean 
    \return  The boolean value
 */
[[nodiscard]] auto text_as_bool(const XmlElemNode& xnode) -> bool;

/*! Get the value of an XML element node's text subnode. 
    \return  The text; or a blank string if the node has not text subnode
 */
[[nodiscard]] auto text_or_blank(const XmlElemNode& xnode) -> std::wstring;

/*! Look among the child elements of \p elem for an element with the tag \p child_tag.
	If exactly one is found, its text value is returned, or a blank string if the element has no text subnode.
    If none, or more than one are found, an exception is thrown.
*/
[[nodiscard]] auto get_unique_child_text(const XmlElemNode& elem, const std::wstring& child_tag) -> std::wstring;

/*! Look among the child elements of \p elem for an element with the tag \p child_tag.
    If none is found, \p def_val is returned.
	If one is found, its text value is returned, or a blank string if the element has no text subnode.
	If more than one are found, an exception is thrown.
*/
[[nodiscard]] auto get_unique_child_text_or_default(const XmlElemNode& elem, 
                                                    const std::wstring& child_tag, 
                                                    std::wstring_view def_val) -> std::wstring;

/*! Look among the child elements of \p elem for an element with the tag \p child_tag.
	If exactly one is found, its text value is returned as a bool; if the conversion fails, an exception 
	is thrown. If none, or more than one are found, an exception is thrown.
 */
[[nodiscard]] auto get_unique_child_bool(const XmlElemNode& elem, const std::wstring& child_tag) -> bool;

/*! Look among the child elements of \p elem for an element with the tag \p child_tag.
    If none is found, \p def_val is returned.
	If one is found, its text value is returned as a boolean; if the conversion fails, an exception is thrown.
	If more than one are found, an exception is thrown.
*/
[[nodiscard]] auto get_unique_child_bool_or_default(const XmlElemNode& elem, 
                                                    const std::wstring& child_tag,
													bool def_val) -> bool;

/*! Look among the child elements of \p elem for an element with the tag \p child_tag.
	If exactly one is found, its text value is returned as an int; if the conversion fails, an exception 
	is thrown. If none, or more than one are found, an exception is thrown.
 */
[[nodiscard]] auto get_unique_child_int(const XmlElemNode& elem, const std::wstring& child_tag) -> int;

/*! Look among the child elements of \p elem for an element with the tag \p child_tag.
	If exactly one is found, its text value is returned as an int; if the conversion fails, an exception 
	is thrown. If none are found, nullopt is returned. If more than one are found, an exception is thrown.
*/
[[nodiscard]] auto get_optional_unique_child_int(const XmlElemNode& elem, const std::wstring& child_tag) 
                    -> std::optional<int>;

/*! Look among the child elements of \p elem for an element with the tag \p child_tag.
	If exactly one is found, its text value is returned as an unsigned 32-bitinteger; if the conversion fails, an 
    exception is thrown. If none are found, nullopt is returned. If more than one are found, an exception is thrown.
*/
[[nodiscard]] auto get_optional_unique_child_uint32(const XmlElemNode& elem, const std::wstring& child_tag) 
                    -> std::optional<uint32_t>;

/*! Look among the child elements of \p elem for an element with the tag \p child_tag.
	If exactly one is found, its text value is returned as an integer converted to enum type \p Enum; if the conversion 
    to integer fails, an exception is thrown. If none, or more than one children are found, an exception is thrown.
    The returned value may or may not be a defined enum value.
*/
template<class Enum> 
requires std::is_enum_v<Enum>
[[nodiscard]] auto get_unique_child_enum(const XmlElemNode& elem, const std::wstring& child_tag) -> Enum;

/*! Look among the child elements of \p elem for an element with the tag \p child_tag.
	If exactly one is found, its text value is returned as an integer converted to enum type \p Enum; if the conversion 
    to integer fails, an exception is thrown. If none are found, nullpt is returned. If more than one children are 
    found, an exception is thrown.
    The returned value may or may not be a defined enum value.
*/
template<class Enum> 
requires std::is_enum_v<Enum>
[[nodiscard]] auto get_optional_unique_child_enum(const XmlElemNode& elem, const std::wstring& child_tag) 
                    -> std::optional<Enum>;

/*! Look among the child elements of \p elem for an element with the tag \p child_tag.
	If exactly one is found, its text value is returned as a double; if the conversion fails, an exception is thrown.
    If none, or more than one children are found, an exception is thrown.
*/
[[nodiscard]] auto get_unique_child_double(const XmlElemNode& elem, const std::wstring& child_tag) -> double;

/*! Look among the child elements of \p elem for an element with the tag \p child_tag.
	If exactly one is found, its text value is returned as a double; if the conversion fails, an exception 
	is thrown. If none are found, nullopt is returned. If more than one are found, an exception is thrown. 
    If one child elemnt exists, but it has no text value, then if \p empty_child_allowed is true then nullopt is 
    returned, and if \p empty_child_allowed is false, an exception is thrown.
*/
[[nodiscard]] auto get_optional_unique_child_double(const XmlElemNode& elem, 
                                                    const std::wstring& child_tag, 
                                                    bool empty_child_allowed = false) -> std::optional<double>;

} 

#include "twist/db/xml_utils.ipp"

#endif 

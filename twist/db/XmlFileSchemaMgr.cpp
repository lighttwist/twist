///  @file  XmlFileSchemaMgr.cpp
///  Implementation file for "XmlFileSchemaMgr.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "XmlFileSchemaMgr.hpp"

#include "xml_utils.hpp"

namespace twist::db {

XmlFileSchemaMgr::XmlFileSchemaMgr(const fs::path& path, std::wstring_view root_xnode_tag, 
		unsigned int cur_db_version, unsigned int oldest_supported_db_version)
	: DatabaseSchemaMgr<XmlDoc>{ path, cur_db_version, oldest_supported_db_version }
	, root_xnode_tag_{ root_xnode_tag }
{
	TWIST_CHECK_INVARIANT
}

XmlFileSchemaMgr::~XmlFileSchemaMgr()
{
	TWIST_CHECK_INVARIANT
}

std::unique_ptr<XmlDoc> XmlFileSchemaMgr::create_db_file()
{
	TWIST_CHECK_INVARIANT
	auto xdoc = init_xdoc(root_xnode_tag_, get_cur_db_version());
	xdoc->save(path());
	return xdoc;
}

std::unique_ptr<XmlDoc> XmlFileSchemaMgr::open_db_file_read()
{
	TWIST_CHECK_INVARIANT
	// Open the XML file, and check that it has the correct structure (check the root XML node name and the 
	// existence of the version attribute)
	unsigned int version = 0;
	return read_xdoc_and_version_from_file(path(), root_xnode_tag_, version);
}

std::unique_ptr<XmlDoc> XmlFileSchemaMgr::open_db_file_write_exclusive()
{
	TWIST_CHECK_INVARIANT
	return open_db_file_read();
}

unsigned int XmlFileSchemaMgr::read_db_version(XmlDoc& connection)
{
	TWIST_CHECK_INVARIANT
	unsigned int version = 0;
	get_xml_doc_version(connection, root_xnode_tag_, version);
	return version;
}

void XmlFileSchemaMgr::write_db_version(unsigned int version, XmlDoc& connection)
{
	TWIST_CHECK_INVARIANT
	set_xml_doc_version(connection, root_xnode_tag_, version);
}

void XmlFileSchemaMgr::create_schema(XmlDoc& connection)
{
	TWIST_CHECK_INVARIANT
	// Schema does not really exist independent of the data in an XML doc; simply check that the document has 
	// the correct structure (check the root XML node name and the existence of the version attribute)
	unsigned int version = 0;
	get_xml_doc_version(connection, root_xnode_tag_, version);
}

#ifdef _DEBUG
void XmlFileSchemaMgr::check_invariant() const noexcept
{
	assert(!root_xnode_tag_.empty());
}
#endif 

} 


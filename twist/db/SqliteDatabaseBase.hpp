/// @file SqliteDatabaseBase.hpp
/// SqliteDatabaseBase abstract base class template definition

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_DB_SQLITE_DATABASE_BASE_HPP
#define TWIST_DB_SQLITE_DATABASE_BASE_HPP

#include "twist/tags.hpp"
#include "twist/db/SqliteConnection.hpp"
#include "twist/db/SqliteSelector.hpp"
#include "twist/db/SqliteInserter.hpp"

namespace twist::db {

/*! Abstract base class for classes which represent versioned SQLite databases, that is databases which contain a 
    "Version" table with an integer "version_no" column. 
    \tparam  Database  The derived database class
 */
template<class Database>
class SqliteDatabaseBase {
public:
	virtual ~SqliteDatabaseBase() = 0;

	/*! Whether the database is an "in-memory" database, ie a database stored purely in memory, rather than based on 
	    a file.
	 */
	[[nodiscard]] auto is_in_memory_database() const -> bool;

	/*! The database file path; or blank if the database is an "in-memory" database, ie a database stored purely 
	    in memory.
	 */
	[[nodiscard]] auto path() const -> fs::path;

	/*! The database file name; or blank if the database is an "in-memory" database, ie a database stored purely 
	    in memory.
	 */
	[[nodiscard]] auto filename() const -> fs::path; 

	TWIST_NO_COPY_NO_MOVE(SqliteDatabaseBase)

protected:
	/*! Constructor; call this constructor to create a new SQLite database file and open it.	   
	    \param[in] tag  Tag value used for tag dispatching
	    \param[in] path  The database file path; an exception is thrown if the file already exists
	 */
	explicit SqliteDatabaseBase(CreateMode tag, fs::path path);  

	/*! Constructor; call this constructor to open an existing Sqlite database file.	   
	    \param[in] tag  Tag value used for tag dispatching
	    \param[in] path  The database file path; an exception is thrown if the file does not exist
	    \param[in] allow_reorganise  If the version of the database is older than the "current version number", then if 
		                             this argument is true, then the constructor will attempt to reorganise the 
									 database to the current version, and if this argument is false, an exception will 
									 be thrown
	 */
	explicit SqliteDatabaseBase(OpenMode tag, fs::path path, bool allow_reorganise);

	/*! Constructor; call this constructor to open an existing Sqlite database file in read-only mode.	   
	    \param[in] tag  Tag value used for tag dispatching
	    \param[in] path  The database file path; an exception is thrown if the file does not exist
	 */
	explicit SqliteDatabaseBase(OpenReadOnlyMode tag, fs::path path);

	/*! Constructor; call this constructor to create a new "in-memory" SQLite database, ia a database stored purely in 
	    memory. The database is automatically deleted and memory is reclaimed when the last connection to the database 
		closes. 
		\param[in] tag  Tag value used for tag dispatching
	    \param[in] db_name  The database name
		\param[in] shared  Whether other database connections can connect to this memory database, using the name 
		                   provided
	 */
	explicit SqliteDatabaseBase(CreateInMemoryMode tag, std::string_view db_name, bool shared);

	/*! Constructor; call this constructor to open an "in-memory" SQLite database, ie a database stored purely in 
	    memory, in read-only mode.  
		\note  We can only connect to an existing in-memory databaase if the connection which created it used the 
		       "shared" mode.
		\param[in] tag  Tag value used for tag dispatching
	    \param[in] db_name  The database name; must match the name used by the connection which created the in-memory 
		                    database
	 */
	explicit SqliteDatabaseBase(OpenInMemoryReadOnlyMode tag, std::string_view db_name);

	//! The open database connection.
	[[nodiscard]] auto conn() const -> SqliteConnection&;

	/// Read the records returned by an SQL query on the database.
	///
	/// @tparam  Fn  Callable type which will be invoked for a record returned by the query
	/// @param[in] sql  The SQL query
	/// @param[in] get_values  Functor which gets the values from each record returned by the query 
	///
	template<class Fn, 
	         class = EnableIfFunc<Fn, const SqliteSelector&>> 
	void read_records(const std::string& sql, Fn&& get_values) const;

	/// Read the first record (if any) returned by an SQL query on the database.
	///
	/// @tparam  Fn  Callable type which will be invoked for the record returned by the query
	/// @param[in] sql  The SQL query
	/// @param[in] get_values  Functor which will be called for the first record returned by the query; if 
	///					the query returns no records then the function will not be called
	/// @return  true if the query returns at least one record
	///
	template<class Fn, 
	         class = EnableIfFunc<Fn, const SqliteSelector&>> 
	bool try_read_first_record(const std::string& sql, Fn&& get_values) const;

	/// Read the first record returned by an SQL query on the database. 
	///
	/// @tparam  Fn  Callable type which will be invoked for the record returned by the query
	/// @param[in] sql  The SQL query
	/// @param[in] get_values  Callable object which will be invoked for the first record returned by the 
	///					query; if the query returns no records then an exception will be thrown and the object
	///					will not be called
	///
	template<class Fn, 
	         class = EnableIfFunc<Fn, SqliteSelector&>> 
	void read_first_record(const std::string& sql, Fn&& get_values) const;

	/// Insert a record into a database table.
	///
	/// @tparam  Fn  Callable type which will be invoked for the record to be inserted
	/// @param[in] table_name  The table_name
	/// @param[in] col_names  The names of the columns whose values will be set (or can be set, for nullable 
	///					columns)
	/// @param[in] set_values  Functor which sets the values from for the record about to be inserted in 
	///					the table
	///
	template<class Fn, 
	         class = EnableIfFunc<Fn, SqliteInserter&>> 
	void insert_record(std::string_view table_name, std::initializer_list<const char*> col_names, 
			Fn&& set_values);

	/// Insert several records into a specific table.
	///
	/// @tparam  Fn  Callable type which sets the record values for each record to be inserted
	/// @param[in] table_name  The table name
	/// @param[in] col_names  The names of the columns whose values will be set (or can be set, for nullable 
	///					columns)
	/// @param[in] set_values  Functor which sets the values for each record about to be inserted in the 
	///					table; it will be called by this method while it returns true
	///
	template<class Fn,  
			 class = EnableIfFuncR<bool, Fn, SqliteInserter&>>
	void insert_records(std::string_view table_name, std::initializer_list<const char*> col_names, 
			Fn&& set_values);

	/// Given a range, insert a record into a specific table for each element in the range.
	///
	/// @tparam  Rng  The input range type
	/// @tparam  Fn  Callable type which sets the record values for each range element 
	/// @param[in] range  The input range
	/// @param[in] table_name  The table name
	/// @param[in] col_names  The names of the columns whose values will be set (or can be set, for nullable 
	///					columns)
	/// @param[in] set_values  Callable object which sets the values for each record about to be inserted in 
	///					the table; it will be called once for each element in the input range
	///
	template<class Rng,
	         class Fn, 
			 class = EnableIfFunc<Fn, const RangeElement<Rng>&, SqliteInserter&>>
	void insert_records_from_range(const Rng& range, std::string_view table_name, 
			std::initializer_list<const char*> col_names, Fn&& set_values);

	/// Start a transaction on the connection to the database, and run user-provided actions on the 
	/// connection; the transction is then commited, if the actions were successful, or rolled-back otherwise.
	///
	/// @tparam  Fn  Callable type invoked for the connection
	/// @param[in] func  Callable object which contains the actions to be performed on the connection, which 
	///					is passed into the function
	///
	template<class Fn, class = EnableIfFunc<Fn, SqliteConnection&>> 
	void transact(Fn&& func);

	//! Whether the table named \p table_name exists in the database.
	[[nodiscard]] auto table_exists(std::string_view table_name) const -> bool;

private:
	void ensure_current_db_version() const;

	int read_db_version() const;

	void write_db_version(int db_version);

	void reorganise();

	template<int to_version>
	void reorganise_to_version(int from_version);

	static constexpr auto schema_creation_sql_member_check();

	static constexpr auto current_db_version_member_check();

	static constexpr auto oldest_supp_db_version_check();

	std::unique_ptr<SqliteConnection> conn_;

	TWIST_CHECK_INVARIANT_DECL
};

}

#include "twist/db/SqliteDatabaseBase.ipp"

#endif

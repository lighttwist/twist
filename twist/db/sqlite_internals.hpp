///  @file  sqlite_internals.hpp
///  Utilities, internal to the twist::db namespace, for working with the "sqlite3" library

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_DB_SQLITE__INTERNALS_HPP
#define TWIST_DB_SQLITE__INTERNALS_HPP

#include <external/sqlite/sqlite3.h>

#include "SqlitePreparedStatement.hpp"

namespace twist::db {

///
///  Class holding internal implementation details for the SqlitePreparedStatement.
///
class SqlitePreparedStatement::Impl {
public:
	Impl(sqlite3_stmt* stmt, sqlite3* conn);

	sqlite3_stmt* stmt() const;

	sqlite3* conn() const;

private:
	sqlite3_stmt*  stmt_;
	sqlite3*  conn_;

	TWIST_CHECK_INVARIANT_DECL
}; 

/// Given an error code returned by a call into the "sqlite3" library, if does not indicate success then 
/// get a description of the error comprising a short description of the error code and error details 
/// retrieved from the connection to the SQLite database (it such details are available).
///
/// @param[in] err_no  The error code
/// @param[in] conn  The connection to the database
/// @return  The error description
///
std::wstring get_sqlite3_error_descr(int err_no, sqlite3* conn);

/// Given an error code returned by a call into the "sqlite3" library, if does not indicate success then 
/// get a short description of the error code.
///
/// @param[in] err_no  The error code
/// @return  The error code description
///
std::string get_error_code_descr(int err_no);

}

#define  TWIST_DB_CHECK_SQLITE3_RESULT(sqlite_expression, conn)  \
			const int sqlite_expression_result = sqlite_expression;  \
			if (sqlite_expression_result != SQLITE_OK) {  \
				const auto msg = get_sqlite3_error_descr(sqlite_expression_result, conn);  \
				TWIST_THROW(msg.c_str());  \
			}

#endif  

///  @file  SqliteInserter.ipp
///  Inline implementation file for "SqliteInserter.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

namespace twist::db {

template<class Enum, class>
void SqliteInserter::set_enum_int(std::string_view col_name, Enum val)
{
	TWIST_CHECK_INVARIANT
	set_int(col_name, static_cast<int>(val));
}


template<class Enum, class>
void SqliteInserter::set_enum_int(unsigned int col_idx, Enum val)
{
	TWIST_CHECK_INVARIANT
	set_int(col_idx, static_cast<int>(val));
}


template<class Cont, class>
void SqliteInserter::set_array_blob(std::string_view col_name, const Cont* cont)
{
	TWIST_CHECK_INVARIANT
	set_array_blob(col_indexer_.get_col_idx(col_name), cont);
}


template<class Cont, class>
void SqliteInserter::set_array_blob(unsigned int col_idx, const Cont* cont)
{
	TWIST_CHECK_INVARIANT
	statement_->bind_array_blob(col_idx + 1, cont);
}


template<class Cont, class> 
bool SqliteInserter::set_compress_array_blob(std::string_view col_name, const Cont* cont, 
		gsl::not_null<std::vector<unsigned char>*> compressed_buf, SqliteBlobCompression compression)
{
	TWIST_CHECK_INVARIANT
	return set_compress_array_blob(col_indexer_.get_col_idx(col_name), cont, compressed_buf, compression);
}


template<class Cont, class> 
bool SqliteInserter::set_compress_array_blob(unsigned int col_idx, const Cont* cont, 
		gsl::not_null<std::vector<unsigned char>*> compressed_buf, SqliteBlobCompression compression)
{
	TWIST_CHECK_INVARIANT
	return statement_->bind_compress_array_blob(col_idx + 1, cont, compressed_buf, compression);
}

} 



/// @file CsvFileReader.ipp
/// Inline implementation file for "CsvFileReader.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace twist::db {

template<typename Chr>
CsvFileReader<Chr>::CsvFileReader(fs::path path, std::basic_string<Chr> delimiter)
	: delimiter_{move(delimiter)}
	, line_buffer_(line_buffer_len)
	, cell_buffer_(cell_buffer_len)
{
	file_stream_.emplace(std::make_unique<FileStd>(std::move(path), FileOpenMode::read));

	delimiter_begin_ = delimiter_.data();
	delimiter_end_ = delimiter_begin_ + delimiter_.size();

	// Read the first (non-blank) line of the file
	Ssize line_len = 0;
	if (!get_non_blank_line_to_buffer(line_len)) {
		TWIST_THROW(L"File \"%s\" is empty.", file_stream_->path().filename().c_str());
	}
	
	// Parse the line for the column names
	const auto col_count = count_delimited_fields<const Chr*, const Chr*>(
			line_buffer_.data(), line_buffer_.data() + line_len, delimiter_begin_, delimiter_end_);
	for (auto i = 0; i < col_count; ++i) {
		auto field = get_delimited_field(line_buffer_.data(), line_buffer_.data() + line_len, 
				delimiter_begin_, delimiter_end_, i);
		String col_name{field.first, field.second};
		col_name = trim_whitespace(col_name);
		col_names_.push_back(col_name);
	}

	last_read_row_cells_.resize(col_count);

	TWIST_CHECK_INVARIANT
}

template<typename Chr>
auto CsvFileReader<Chr>::nof_columns() const -> int
{
	TWIST_CHECK_INVARIANT
	return ssize32(col_names_);
}

template<typename Chr>
auto CsvFileReader<Chr>::view_col_names() const -> const Strings& 
{
	TWIST_CHECK_INVARIANT
	return col_names_;
}

template<typename Chr> 
auto CsvFileReader<Chr>::read_next_row() -> bool  
{
	TWIST_CHECK_INVARIANT
	// Read the current line
	Ssize line_len = 0;
	if (!get_non_blank_line_to_buffer(line_len)) {
		return false;
	}

	// Parse the line for the column names
	const auto cell_count = count_delimited_fields<const Chr*, const Chr*>(
			line_buffer_.data(), line_buffer_.data() + line_len, delimiter_begin_, delimiter_end_);
	if (cell_count != ssize(col_names_)) {
		TWIST_THROW(L"Unrecognised CSV file format: unexpected number of cells on line %lld.", cur_line_idx_);
	}

	for (auto i = 0; i < cell_count; ++i) {
		auto field = get_delimited_field(line_buffer_.data(), line_buffer_.data() + line_len, 
				delimiter_begin_, delimiter_end_, i);
		auto cell = make_string_view(field.first, field.second);
		last_read_row_cells_[i] = trim_whitespace_to_view(cell);
	}
		
	return true;
}

template<typename Chr>
auto CsvFileReader<Chr>::cell_ref(Ssize col) const -> const String&
{
	TWIST_CHECK_INVARIANT
	if (cur_line_idx_ < 2) {
		TWIST_THROW(L"No data rows have been read.");
	}
	return last_read_row_cells_.at(col);
}

template<typename Chr>
auto CsvFileReader<Chr>::row_ref() const -> const Strings& 
{
	TWIST_CHECK_INVARIANT
	if (cur_line_idx_ < 2) {
		TWIST_THROW(L"No data rows have been read.");
	}
	return last_read_row_cells_;
}

template<typename Chr>
auto CsvFileReader<Chr>::path() const -> fs::path
{
	TWIST_CHECK_INVARIANT
	return file_stream_->path();
}

template<typename Chr>
auto CsvFileReader<Chr>::get_non_blank_line_to_buffer(Ssize& line_len) -> bool
{
	if (file_stream_->is_eof()) return false;

	bool non_blank_line_read = false;

	auto get_next_line = [&]() {
		file_stream_->get_line(line_buffer_.data(), line_buffer_len, line_len);
		++cur_line_idx_;	
		non_blank_line_read = !is_whitespace(line_buffer_.data(), line_buffer_.data() + line_len);
	};

	get_next_line();
	while (!non_blank_line_read && !file_stream_->is_eof()) {
		get_next_line();
	}

	return non_blank_line_read;
}


#ifdef _DEBUG
template<typename Chr>
void CsvFileReader<Chr>::check_invariant() const noexcept
{
	assert(!delimiter_.empty());
	assert(delimiter_begin_);
	assert(delimiter_end_);
	assert(file_stream_);
	assert(!line_buffer_.empty());
	assert(!cell_buffer_.empty());
	assert(!col_names_.empty());
	assert(size(last_read_row_cells_) == size(col_names_));
}
#endif 

}

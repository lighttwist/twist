/// @file SqliteDatabaseBase.ipp
/// Inline implementation file for "SqliteDatabaseBase.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include <type_traits>

namespace twist::db {

template<class Database> 
SqliteDatabaseBase<Database>::SqliteDatabaseBase(CreateMode, fs::path path)
{
	static_assert(schema_creation_sql_member_check()(type_to_val<Database>),
			      "In order to use this constructor, the derived 'Database' class must contain "
			      "the method 'schema_creation_sql() -> std::string', accessible from the base class.");

	static_assert(current_db_version_member_check()(type_to_val<Database>),
			      "The derived 'Database' class must contain the data member 'const int current_db_version', "
			      "accessible from the base class.");

	conn_ = SqliteConnection::new_file_db(std::move(path));
	conn_->exec(static_cast<Database*>(this)->schema_creation_sql());
	conn_->exec(string_to_ansi(L"CREATE TABLE Version (version_no INTEGER PRIMARY KEY);"));
	write_db_version(Database::current_db_version);

	TWIST_CHECK_INVARIANT
}

template<class Database> 
SqliteDatabaseBase<Database>::SqliteDatabaseBase(OpenMode, fs::path path, bool allow_reorganise)
{
	static_assert(current_db_version_member_check()(type_to_val<Database>),
			      "The derived 'Database' class must contain the data member 'const int current_db_version', "
			      "accessible from the base class.");

    static_assert(oldest_supp_db_version_check()(type_to_val<Database>),
			      "The derived 'Database' class must contain the data member 'const int oldest_supported_db_version', "
			      "accessible from the base class.");

	conn_ = SqliteConnection::open_file_db(std::move(path), false/*read_only*/);
	if (allow_reorganise) {
		reorganise();
	}
	ensure_current_db_version();

	TWIST_CHECK_INVARIANT
}

template<class Database> 
SqliteDatabaseBase<Database>::SqliteDatabaseBase(OpenReadOnlyMode, fs::path path)
{
	static_assert(current_db_version_member_check()(type_to_val<Database>),
			      "The derived 'Database' class must contain the data member 'const int current_db_version', "
			      "accessible from the base class.");

    static_assert(oldest_supp_db_version_check()(type_to_val<Database>),
			      "The derived 'Database' class must contain the data member 'const int oldest_supported_db_version', "
			      "accessible from the base class.");

	conn_ = SqliteConnection::open_file_db(std::move(path), true/*read_only*/);
	ensure_current_db_version();

	TWIST_CHECK_INVARIANT
}

template<class Database> 
SqliteDatabaseBase<Database>::SqliteDatabaseBase(CreateInMemoryMode, std::string_view db_name, bool shared)
{
	static_assert(schema_creation_sql_member_check()(type_to_val<Database>),
			      "In order to use this constructor, the derived 'Database' class must contain "
			      "the method 'schema_creation_sql() -> std::string', accessible from the base class.");

	static_assert(current_db_version_member_check()(type_to_val<Database>),
			      "The derived 'Database' class must contain the data member 'const int current_db_version', "
			      "accessible from the base class.");

	conn_ = SqliteConnection::new_memory_db(db_name, shared);
	conn_->exec(static_cast<Database*>(this)->schema_creation_sql());
	conn_->exec(string_to_ansi(L"CREATE TABLE Version (version_no INTEGER PRIMARY KEY);"));
	write_db_version(Database::current_db_version);

	TWIST_CHECK_INVARIANT
}

template<class Database> 
SqliteDatabaseBase<Database>::SqliteDatabaseBase(OpenInMemoryReadOnlyMode, std::string_view db_name)
{
	static_assert(current_db_version_member_check()(type_to_val<Database>),
			      "The derived 'Database' class must contain the data member 'const int current_db_version', "
			      "accessible from the base class.");

    static_assert(oldest_supp_db_version_check()(type_to_val<Database>),
			      "The derived 'Database' class must contain the data member 'const int oldest_supported_db_version', "
			      "accessible from the base class.");

	conn_ = SqliteConnection::open_memory_db(db_name, true/*read_only*/);
	ensure_current_db_version();

	TWIST_CHECK_INVARIANT
}

template<class Database> 
SqliteDatabaseBase<Database>::~SqliteDatabaseBase()
{
	TWIST_CHECK_INVARIANT
}

template<class Database> 
auto SqliteDatabaseBase<Database>::is_in_memory_database() const -> bool
{
	TWIST_CHECK_INVARIANT
	return conn_->is_in_memory_database();
}

template<class Database> 
auto SqliteDatabaseBase<Database>::path() const -> fs::path
{
	TWIST_CHECK_INVARIANT
	return conn_->path();
}

template<class Database>
auto SqliteDatabaseBase<Database>::filename() const -> fs::path
{
	TWIST_CHECK_INVARIANT
	return path().filename();
}

template<class Database> 
auto SqliteDatabaseBase<Database>::conn() const -> SqliteConnection&
{
	TWIST_CHECK_INVARIANT
	return *conn_;
}


template<class Database> 
template<class Fn, class> 
void SqliteDatabaseBase<Database>::read_records(const std::string& sql, Fn&& get_values) const
{
	TWIST_CHECK_INVARIANT
	SqliteSelector sel{*conn_, sql};
	while (sel.select()) {
		get_values(sel);
	}
}


template<class Database> 
template<class Fn, class> 
bool SqliteDatabaseBase<Database>::try_read_first_record(const std::string& sql, Fn&& get_values) const
{
	TWIST_CHECK_INVARIANT
	SqliteSelector sel{*conn_, sql};
	if (sel.select()) {
		get_values(sel);
		return true;
	}
	return false;
}


template<class Database> template<class Fn, class> 
void SqliteDatabaseBase<Database>::read_first_record(const std::string& sql, Fn&& get_values) const
{
	TWIST_CHECK_INVARIANT
	if (!try_read_first_record(sql, std::forward<Fn>(get_values))) {
		TWIST_THROW(L"Query \"%s\" returned no records from database \"%s\".", 
				ansi_to_string(sql).c_str(), path().filename().c_str());
	}
}


template<class Database> template<class Fn, class> 
void SqliteDatabaseBase<Database>::insert_record(std::string_view table_name, 
		std::initializer_list<const char*> col_names, Fn&& set_values)
{
	TWIST_CHECK_INVARIANT
	SqliteInserter inserter{ conn(), table_name, col_names };
	std::forward<Fn>(set_values)(inserter);
	inserter.insert();
}


template<class Database> 
template<class Fn, class> 
void SqliteDatabaseBase<Database>::insert_records(std::string_view table_name, 
		std::initializer_list<const char*> col_names, Fn&& set_values)
{
	TWIST_CHECK_INVARIANT
	SqliteInserter inserter{ conn(), table_name, col_names };
	while (std::forward<Fn>(set_values)(inserter)) {
		inserter.insert();
	}
}


template<class Database> 
template<class Rng, class Fn, class>
void SqliteDatabaseBase<Database>::insert_records_from_range(const Rng& range, std::string_view table_name, 
		std::initializer_list<const char*> col_names, Fn&& set_values)
{
	TWIST_CHECK_INVARIANT
	SqliteInserter inserter{ conn(), table_name, col_names };
	for (const auto& el : range) {
		std::forward<Fn>(set_values)(el, inserter);
		inserter.insert();
	}
}


template<class Database>
template<class Fn, class>
void SqliteDatabaseBase<Database>::transact(Fn&& func)
{
	TWIST_CHECK_INVARIANT
	twist::db::transact(*conn_, std::forward<Fn>(func));
}

template<class Database>
auto SqliteDatabaseBase<Database>::table_exists(std::string_view table_name) const -> bool
{
	TWIST_CHECK_INVARIANT
	return conn_->table_exists(table_name);
}

template<class Database>
void SqliteDatabaseBase<Database>::ensure_current_db_version() const
{
	TWIST_CHECK_INVARIANT
	const auto db_version = read_db_version();
	if (db_version != Database::current_db_version) {
		TWIST_THROW(L"The database file \"%s\" has the wrong version %d; expected version number is %d.", 
				path().filename().c_str(), db_version, Database::current_db_version);
	}
}


template<class Database>
int SqliteDatabaseBase<Database>::read_db_version() const
{
	TWIST_CHECK_INVARIANT
	int db_version{0};
	read_first_record("SELECT version_no FROM Version ORDER BY version_no DESC", [&](const auto& sel) {
		db_version = sel.get_int(0);
	}); 
	return db_version;
}


template<class Database>
void SqliteDatabaseBase<Database>::write_db_version(int db_version)
{
	TWIST_CHECK_INVARIANT
    conn_->exec(std::format("INSERT INTO Version (version_no) VALUES ({})", db_version));
}


template<class Database>
void SqliteDatabaseBase<Database>::reorganise()
{
	TWIST_CHECK_INVARIANT
	const auto from_version = read_db_version();
	if (from_version == Database::current_db_version) {
		return;  // No reorganise necessary
	}

	if (from_version < Database::oldest_supported_db_version) {
		TWIST_THROW(L"Cannot reorganise database from version %d; " 
				"the oldest supported version is %d.", from_version, Database::oldest_supported_db_version);
	}
	if (from_version > Database::current_db_version) {
		TWIST_THROW(L"Cannot reorganise database from version %d; " 
				"the newest supported version is %d.", from_version, Database::current_db_version);
	}
	
	reorganise_to_version<Database::oldest_supported_db_version + 1>(from_version);
	write_db_version(Database::current_db_version);
}


template<class Database>
template<int to_version>
void SqliteDatabaseBase<Database>::reorganise_to_version(int from_version)
{
	TWIST_CHECK_INVARIANT
	if constexpr (to_version < Database::oldest_supported_db_version) {
		TWIST_THROW(L"Reorganisation to version %d unsupported; oldest supported version is %d", 
				to_version < Database::oldest_supported_db_version);
	}

	auto& db = static_cast<Database&>(*this); // CRTP

	if (from_version < to_version) {
		if constexpr (to_version <= Database::current_db_version) {
			if constexpr (to_version == 2) {
				db.reorganise_to_version_2(conn());
			}
			else if constexpr (to_version == 3) {
				db.reorganise_to_version_3(conn());
			}
			else if constexpr (to_version == 4) {
				db.reorganise_to_version_4(conn());
			}
			else if constexpr (to_version == 5) {
				db.reorganise_to_version_5(conn());
			}
			else if constexpr (to_version == 6) {
				db.reorganise_to_version_6(conn());
			}
			else if constexpr (to_version == 7) {
				db.reorganise_to_version_7(conn());
			}
			else if constexpr (to_version == 8) {
				db.reorganise_to_version_8(conn());
			}
			else if constexpr (to_version == 9) {
				db.reorganise_to_version_9(conn());
			}
			else if constexpr (to_version == 10) {
				db.reorganise_to_version_10(conn());
			}
			else if constexpr (to_version == 11) {
				db.reorganise_to_version_11(conn());
			}
			else if constexpr (to_version == 12) {
				db.reorganise_to_version_12(conn());
			}
			else if constexpr (to_version == 13) {
				db.reorganise_to_version_13(conn());
			}
			else if constexpr (to_version == 14) {
				db.reorganise_to_version_14(conn());
			}
			else if constexpr (to_version == 15) {
				db.reorganise_to_version_15(conn());
			}
			else if constexpr (to_version == 16) {
				db.reorganise_to_version_16(conn());
			}
			// Add more as needed
		}
	}

	if constexpr (to_version < Database::current_db_version) {
		reorganise_to_version<to_version + 1>(from_version);
	}
}

template<class Database>
constexpr auto SqliteDatabaseBase<Database>::schema_creation_sql_member_check()
{
	return is_valid_lambda_decl([](auto x) -> decltype(
				std::string{ type_from_val(x).schema_creation_sql() }) {});
}

template<class Database>
constexpr auto SqliteDatabaseBase<Database>::current_db_version_member_check()
{
	return is_valid_lambda_decl([](auto x) -> decltype(
				int{ type_from_val(x).current_db_version }) {});
}

template<class Database>
constexpr auto SqliteDatabaseBase<Database>::oldest_supp_db_version_check()
{
	return is_valid_lambda_decl([](auto x) -> decltype(
				int{ type_from_val(x).oldest_supported_db_version }) {});
}


#ifdef _DEBUG
template<class Database>
void SqliteDatabaseBase<Database>::check_invariant() const noexcept
{
	assert(conn_);
}
#endif

}

/// @file HomogenousTable.ipp
/// Inline implementation file for "HomogenousTable.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/ranges.hpp"

namespace twist::db {

// --- HomogenousTable class template ---

template<class T, HomogenousTableStorageType storage_type>
HomogenousTable<T, storage_type>::HomogenousTable(std::vector<std::wstring> col_names)
	: col_names_{move(col_names)}
{
	if (rg::any_of(col_names_, [](const auto& name) { return is_whitespace(name); })) {
		TWIST_THROW(L"Column names cannot be empty.");
	}
	for (auto i : IndexRange{ssize(col_names_)}) {
		const auto& col_name = col_names_[i];
		if (auto [_, ok] = col_indexes_.emplace(col_name, i); !ok) {
			TWIST_THROW(L"Column name \"%s\" is not unique.", col_name.c_str());
		}
	}
	TWIST_CHECK_INVARIANT
}

template<class T, HomogenousTableStorageType storage_type>
auto HomogenousTable<T, storage_type>::clone() const -> HomogenousTable
{
	auto clone = HomogenousTable{col_names_};
	clone.cell_data_ = cell_data_.clone();
	return clone;
}

template<class T, HomogenousTableStorageType storage_type>
auto HomogenousTable<T, storage_type>::get_column_name(Ssize col_idx) const -> std::wstring
{
	TWIST_CHECK_INVARIANT
	if (col_idx >= ssize(col_names_)) {
		TWIST_THROW(L"Invalid column index %lld.", col_idx);
	}	
	return col_names_[col_idx];
}

template<class T, HomogenousTableStorageType storage_type>
auto HomogenousTable<T, storage_type>::column_names() const -> const std::vector<std::wstring>&
{
	TWIST_CHECK_INVARIANT
	return col_names_;
}

template<class T, HomogenousTableStorageType storage_type>
auto HomogenousTable<T, storage_type>::get_column_index(const std::wstring& col_name) const -> Ssize
{
	TWIST_CHECK_INVARIANT
	if (auto it = col_indexes_.find(col_name); it != end(col_indexes_)) {
		return it->second;
	}
	TWIST_THROW(L"Column name \"%s\" not found in the table.", col_name.c_str());
}

template<class T, HomogenousTableStorageType storage_type>
auto HomogenousTable<T, storage_type>::nof_rows() const -> Ssize
{
	TWIST_CHECK_INVARIANT
	return cell_data_.nof_rows();
}

template<class T, HomogenousTableStorageType storage_type>
auto HomogenousTable<T, storage_type>::nof_columns() const -> Ssize
{
	TWIST_CHECK_INVARIANT
	return ssize(col_names_);
}

template<class T, HomogenousTableStorageType storage_type>
auto HomogenousTable<T, storage_type>::nof_cells() const -> Ssize
{
	TWIST_CHECK_INVARIANT
	return cell_data_.nof_cells();
}

template<class T, HomogenousTableStorageType storage_type>
auto HomogenousTable<T, storage_type>::operator()(Ssize row_idx, Ssize col_idx) const -> const Value&
{
	TWIST_CHECK_INVARIANT
	return cell_data_(row_idx, col_idx);
}

template<class T, HomogenousTableStorageType storage_type>
auto HomogenousTable<T, storage_type>::operator()(Ssize row_idx, Ssize col_idx) -> Value&
{
	TWIST_CHECK_INVARIANT
	return cell_data_(row_idx, col_idx);
}

template<class T, HomogenousTableStorageType storage_type>
auto HomogenousTable<T, storage_type>::get_cell(Ssize row_idx, Ssize col_idx) const -> const Value&
{
	TWIST_CHECK_INVARIANT
	return cell_data_.get_cell(row_idx, col_idx);
}

template<class T, HomogenousTableStorageType storage_type>
auto HomogenousTable<T, storage_type>::set_cell(Ssize row_idx, Ssize col_idx, Value value) -> void
{
	TWIST_CHECK_INVARIANT
	cell_data_.set_cell(row_idx, col_idx, std::move(value));
}

template<class T, HomogenousTableStorageType storage_type>
auto HomogenousTable<T, storage_type>::get_row(Ssize row_idx) const -> OutRowValues
{
	TWIST_CHECK_INVARIANT
	return cell_data_.get_row(row_idx);
}

template<class T, HomogenousTableStorageType storage_type>
auto HomogenousTable<T, storage_type>::add_row(InRowValues row_values) -> Ssize
{
	TWIST_CHECK_INVARIANT
	return cell_data_.add_row(move(row_values));
}

template<class T, HomogenousTableStorageType storage_type>
auto HomogenousTable<T, storage_type>::remove_row(Ssize row_idx) -> void
{
	TWIST_CHECK_INVARIANT
	cell_data_.remove_row(row_idx);
}

template<class T, HomogenousTableStorageType storage_type>
auto HomogenousTable<T, storage_type>::rename_column(const std::wstring& old_col_name, std::wstring new_col_name) 
                                        -> void
{
	TWIST_CHECK_INVARIANT
	if (is_whitespace(new_col_name)) {
		TWIST_THROW(L"Column names cannot be empty.");
	}
	assert(old_col_name != new_col_name);

	const auto col_idx = get_column_index(old_col_name);
	col_names_.erase(begin(col_names_) + col_idx);
	col_names_.insert(begin(col_names_) + col_idx, new_col_name);
	col_indexes_.erase(old_col_name);
	col_indexes_.emplace(move(new_col_name), col_idx);
}

template<class T, HomogenousTableStorageType storage_type>
auto HomogenousTable<T, storage_type>::add_column(std::wstring col_name, InColumnValues col_values) -> Ssize
{
	TWIST_CHECK_INVARIANT
	const auto col_idx = nof_columns();
	insert_column(col_idx, move(col_name), std::move(col_values));
	return col_idx;
}

template<class T, HomogenousTableStorageType storage_type>
auto HomogenousTable<T, storage_type>::insert_column(Ssize col_idx, std::wstring col_name, InColumnValues col_values) 
                                        -> void
{
	TWIST_CHECK_INVARIANT
	if (is_whitespace(col_name)) {
		TWIST_THROW(L"Column names cannot be empty.");
	}
	if (col_indexes_.count(col_name) != 0) {
		TWIST_THROW(L"Column names must be unique.");
	}
	cell_data_.insert_column(col_idx, move(col_values));
	
	col_names_.insert(begin(col_names_) + col_idx, col_name);

	col_indexes_.emplace(std::move(col_name), col_idx);
	for (auto i : IndexRange{col_idx + 1, ssize(col_names_)}) {
		++col_indexes_[col_names_[i]];
	}
}

template<class T, HomogenousTableStorageType storage_type>
auto HomogenousTable<T, storage_type>::remove_column(const std::wstring& col_name) -> void
{
	TWIST_CHECK_INVARIANT
	remove_column(get_column_index(col_name));
}

template<class T, HomogenousTableStorageType storage_type>
auto HomogenousTable<T, storage_type>::remove_column(Ssize col_idx) -> void
{
	TWIST_CHECK_INVARIANT
	cell_data_.remove_column(col_idx);
	const auto it = begin(col_names_) + col_idx;

	col_indexes_.erase(*it);
	for (auto i : IndexRange{col_idx + 1, ssize(col_names_)}) {
		--col_indexes_[col_names_[i]];
	}	

	col_names_.erase(it);
}

template<class T, HomogenousTableStorageType storage_type>
auto HomogenousTable<T, storage_type>::extract_column(const std::wstring& col_name) -> std::vector<T>
{
	TWIST_CHECK_INVARIANT
	return extract_column(get_column_index(col_name));
}

template<class T, HomogenousTableStorageType storage_type>
auto HomogenousTable<T, storage_type>::extract_column(Ssize col_idx) -> std::vector<T>
{
	TWIST_CHECK_INVARIANT
	auto col_values = cell_data_.extract_column(col_idx);
	const auto it = begin(col_names_) + col_idx;

	col_indexes_.erase(*it);
	for (auto i : IndexRange{col_idx + 1, ssize(col_names_)}) {
		--col_indexes_[col_names_[i]];
	}	

	col_names_.erase(it);	

	return col_values; 
}

#ifdef _DEBUG
template<class T, HomogenousTableStorageType storage_type>
auto HomogenousTable<T, storage_type>::check_invariant() const noexcept -> void
{
	assert(ssize(col_names_) == ssize(col_indexes_));
	assert(equal_same_size(sort_to_vector(col_names_), vw::keys(col_indexes_)));
	assert(ssize(col_names_) == cell_data_.nof_columns() || cell_data_.nof_columns() == 0);
	assert(rg::none_of(col_names_, [](const auto& name) { return is_whitespace(name); }));
	assert(!has_duplicates(std::vector<std::wstring>{col_names_}));
}
#endif 

// --- Free functions ---

template<class T, 
         HomogenousTableStorageType storage_type, 
		 rg::input_range Rng>
auto equal_to_range(const HomogenousTable<T, storage_type>& table, const Rng& range) -> bool
{
	using std::begin;
	using std::end;

	if (rg::ssize(range) != table.nof_cells()) {
		TWIST_THROW(L"Range has wrong size %lld, expected %lld.", rg::ssize(range), table.nof_cells());
	}
	const auto nof_cols = table.nof_columns();
	for (auto i : IndexRange{table.nof_rows()}) {
		const auto& row = table.get_row(i);
		if (!std::equal(begin(row), end(row), begin(range) + nof_cols * i, begin(range) + nof_cols * (i + 1))) {
			return false;
		}
	}
	return true;
}

} 

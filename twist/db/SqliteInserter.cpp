///  @file  SqliteInserter.hpp
///  Implementation file for "SqliteInserter.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/db/SqliteInserter.hpp"

#include "twist/DateTime.hpp"

#include "twist/db/sql_assembler.hpp"
#include "twist/db/sqlite_internals.hpp"

namespace twist::db {

SqliteInserter::SqliteInserter(SqliteConnection& conn, std::string_view table_name, 
		                       std::initializer_list<const char*> col_names)
	: conn_{conn}
    , col_indexer_{col_names}
	, true_str_{"1"}
	, false_str_{"0"}
#ifdef _DEBUG
	, table_name_{table_name}
#endif 
{
	// Assemble the SQL INSERT query, with placeholders for each cell value
	std::string col_names_sql;
	std::string cell_values_sql;
	auto first_it = begin(col_names);
	auto last_it = end(col_names);
	for (auto it = first_it; it != last_it; ++it) {
		if (it != first_it) {
			col_names_sql  += ", ";
			cell_values_sql += ", ";
		}
		col_names_sql  += *it;
		cell_values_sql += "?";
	}
	const auto sql = std::string{ "INSERT INTO " } + table_name + " (" + col_names_sql + ") " 
	               + "VALUES (" + cell_values_sql + ");";	

	statement_ = conn_.prepare(sql);
	TWIST_CHECK_INVARIANT
}


void SqliteInserter::insert()
{
	TWIST_CHECK_INVARIANT
	conn_.step(*statement_);
	statement_->reset();
}


void SqliteInserter::set_text(std::string_view col_name, gsl::not_null<const std::wstring*> val)
{
	TWIST_CHECK_INVARIANT
	set_text(col_indexer_.get_col_idx(col_name), val);
}


void SqliteInserter::set_text(std::string_view col_name, std::wstring&& val)
{
	TWIST_CHECK_INVARIANT
	set_text(col_indexer_.get_col_idx(col_name), std::forward<std::wstring&&>(val));
}


void SqliteInserter::set_text(unsigned int col_idx, gsl::not_null<const std::wstring*> val)
{
	TWIST_CHECK_INVARIANT

#if TWIST_OS_WIN

	statement_->bind_text16(col_idx + 1, val->c_str());

#elif TWIST_OS_LINUX

	statement_->bind_blob(col_idx + 1, val->c_str(), val->size() * sizeof(wchar_t));

#endif
}


void SqliteInserter::set_text(unsigned int col_idx, std::wstring&& val)
{
	TWIST_CHECK_INVARIANT

#if TWIST_OS_WIN

	statement_->bind_text16(col_idx + 1, val.c_str(), SqliteBoundValueStorage::transient);

#elif TWIST_OS_LINUX

	statement_->bind_blob(col_idx + 1, val.c_str(), val.size() * sizeof(wchar_t), 
			SqliteBoundValueStorage::transient);

#endif
}


void SqliteInserter::set_text(unsigned int col_idx, const fs::path& val)
{
	TWIST_CHECK_INVARIANT

#if TWIST_OS_WIN

	statement_->bind_text16(col_idx + 1, val.wstring().c_str(), SqliteBoundValueStorage::transient);

#elif TWIST_OS_LINUX

	statement_->bind_blob(col_idx + 1, val.c_str(), strlen(val.c_str()));

#endif
}


void SqliteInserter::set_ansi(std::string_view col_name, gsl::not_null<const std::string*> val)
{
	TWIST_CHECK_INVARIANT
	set_ansi(col_indexer_.get_col_idx(col_name), val);
}


void SqliteInserter::set_ansi(std::string_view col_name, std::string&& val)
{
	TWIST_CHECK_INVARIANT
	set_ansi(col_indexer_.get_col_idx(col_name), std::forward<std::string&&>(val));
}


void SqliteInserter::set_ansi(unsigned int col_idx, gsl::not_null<const std::string*> val)
{
	TWIST_CHECK_INVARIANT
	statement_->bind_text(col_idx + 1, val->c_str());
}


void SqliteInserter::set_ansi(unsigned int col_idx, std::string&& val)
{
	TWIST_CHECK_INVARIANT
	statement_->bind_text(col_idx + 1, val.c_str(), SqliteBoundValueStorage::transient);
}


void SqliteInserter::set_int(std::string_view col_name, int val)
{
	TWIST_CHECK_INVARIANT
	set_int(col_indexer_.get_col_idx(col_name), val);
}


void SqliteInserter::set_int(unsigned int col_idx, int val)
{
	TWIST_CHECK_INVARIANT
	statement_->bind_integer(col_idx + 1, val);
}


void SqliteInserter::set_int64(std::string_view col_name, int64_t val)
{
	TWIST_CHECK_INVARIANT
	set_int64(col_indexer_.get_col_idx(col_name), val);
}


void SqliteInserter::set_int64(unsigned int col_idx, int64_t val)
{
	TWIST_CHECK_INVARIANT
	statement_->bind_int64(col_idx + 1, val);
}


void SqliteInserter::set_real(std::string_view col_name, double val)
{
	TWIST_CHECK_INVARIANT
	set_real(col_indexer_.get_col_idx(col_name), val);
}


void SqliteInserter::set_real(unsigned int col_idx, double val)
{
	TWIST_CHECK_INVARIANT
	statement_->bind_real(col_idx + 1, val);
}


void SqliteInserter::set_bool(std::string_view col_name, bool val)
{
	TWIST_CHECK_INVARIANT
	set_bool(col_indexer_.get_col_idx(col_name), val);
}


void SqliteInserter::set_bool(unsigned int col_idx, bool val)
{
	TWIST_CHECK_INVARIANT
	set_ansi(col_idx, val ? &true_str_ : &false_str_);
}


void SqliteInserter::set_date(std::string_view col_name, const Date& val)
{
	TWIST_CHECK_INVARIANT
	set_date(col_indexer_.get_col_idx(col_name), val);
}


void SqliteInserter::set_date(unsigned int col_idx, const Date& val)
{
	TWIST_CHECK_INVARIANT
	const auto date_str = date_to_str(val, DateStrFormat::yyyy_mm_dd, L"-");
	set_text(col_idx, date_str);
}


void SqliteInserter::set_datetime(std::string_view col_name, const DateTime& val)
{
	TWIST_CHECK_INVARIANT
	set_datetime(col_indexer_.get_col_idx(col_name), val);
}


void SqliteInserter::set_datetime(unsigned int col_idx, const DateTime& val)
{
	TWIST_CHECK_INVARIANT
	set_text(col_idx, date_time_to_str(val, DateTimeStrFormat::yyyy_mm_dd_hh_mm_ss, L"-", L" ", L":"));
}


void SqliteInserter::set_blob(std::string_view col_name, gsl::not_null<const void*> blob, size_t blob_size)
{
	TWIST_CHECK_INVARIANT
	set_blob(col_indexer_.get_col_idx(col_name), blob, blob_size);
}


void SqliteInserter::set_blob(unsigned int col_idx, gsl::not_null<const void*> blob, size_t blob_size)
{
	TWIST_CHECK_INVARIANT
	statement_->bind_blob(col_idx + 1, blob, blob_size);
}



#ifdef _DEBUG
void SqliteInserter::check_invariant() const noexcept
{
	assert(statement_);
	assert(!table_name_.empty());
}
#endif 

// --- Free functions ---

auto set_nullable_datetime(SqliteInserter& ins, std::string_view col_name, std::optional<DateTime> datetime) -> bool
{
	if (datetime) {
		ins.set_datetime(col_name, *datetime);
		return true;
	}
	return false;
}

auto set_nullable_date(SqliteInserter& ins, std::string_view col_name, std::optional<Date> date) -> bool
{
	if (date) {
		ins.set_date(col_name, *date);
		return true;
	}
	return false;
}

} 

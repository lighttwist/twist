/// @file CsvFileReader.hpp
/// CsvFileReader class template

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_DB_CSV_FILE_READER_HPP
#define TWIST_DB_CSV_FILE_READER_HPP

#include "twist/string_utils.hpp"
#include "twist/InTextFileStream.hpp"

namespace twist::db {

/*! Class for opening and efficiently reading text files which follow the CSV (comma-separated values) file format.
    Delimiters other than a comma are supported. 
    The CSV files readable by this class are assumed to have a header line, containing non-empty column names; quoted 
	cell values (eg for cell values containing delimiters) are not supported.
    Adjacent delimiters (successive delimiters with no characters, including whitespace, between them) are considered
    to be a single separator.  //+TODO: why? 	 
    \tparam Chr  The character type in the input text file; only the char and wchar_t types are currenty supported
 */   
template<typename Chr>
class CsvFileReader {
public:
	using String = std::basic_string<Chr>;
	using Strings = std::vector<String>;

	/*! Constructor.
	    \param[in] path  The CSV file path
		\param[in] delimiter  The string which delimits two adjacent values on the same line
	 */
	CsvFileReader(fs::path path, std::basic_string<Chr> delimiter = comma_str<Chr>());

	//! The number of columns in the file.
	[[nodiscard]] auto nof_columns() const -> int;

	//! The name of columns in the file, as specified in the header line.
	[[nodiscard]] auto view_col_names() const -> const Strings&;

	/*! Read the next row from the data file.	
	    \return  true if the (non-blank) row has been read successfully; false otherwise (eg if the end-of-file has 
		         been reached, or a blank line encountered)
	 */	
	auto read_next_row() -> bool;

	/*! Get a reference to the string in a specific cell of the row which was last read successfully.
	    An exception is thrown if no line has been read yet (the header does not count).	
	    \param[in] col  The cell's column index; an exception is thrown if it is out of bounds
	    \return  A reference to the string in the cell; only valid while the reader object is alive and until the next 
		         row is read
     */
	[[nodiscard]] auto cell_ref(Ssize col) const -> const String&;

	/*! Get a reference to the list of cell values in the row which was last read successfully.
	    An exception is thrown if no line has been read yet (the header does not count).	
	    \return  A reference to the list of strings in all cells; only valid while the reader object is alive and until 
		         the next row is read
	 */
	[[nodiscard]] auto row_ref() const -> const Strings&; 

	//! The CSV file path.
	[[nodiscard]] auto path() const -> fs::path;

	TWIST_NO_COPY_NO_MOVE(CsvFileReader)

private:	
	static constexpr Ssize line_buffer_len = 131072;
	static constexpr Ssize cell_buffer_len = 1024;

	[[nodiscard]] auto get_non_blank_line_to_buffer(Ssize& line_len) -> bool;

	const String delimiter_;
	const Chr* delimiter_begin_{nullptr};
	const Chr* delimiter_end_{nullptr};

	std::optional<InTextFileStream<Chr>> file_stream_;
	std::vector<Chr> line_buffer_;
	std::vector<Chr> cell_buffer_;

	Strings col_names_;
	Strings last_read_row_cells_;
	Ssize cur_line_idx_{0};

	TWIST_CHECK_INVARIANT_DECL
};

}

#include "CsvFileReader.ipp"

#endif 

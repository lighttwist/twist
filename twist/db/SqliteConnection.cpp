///  @file  SqliteConnection.cpp
///  Implementation file for "SqliteConnection.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "SqliteConnection.hpp"

#include <external/sqlite/sqlite3.h>

#include "twist/string_utils.hpp"

#include "SqlitePreparedStatement.hpp"
#include "SqliteRecord.hpp"
#include "sqlite_internals.hpp"

#define  TWIST_DB_CHECK_SQLITE3_RESULT_LOCAL(sqlite_expression)  \
			const int sqlite_expression_result = sqlite_expression;  \
			if (sqlite_expression_result != SQLITE_OK) {  \
				const auto msg = get_error_descr(sqlite_expression_result);  \
				sqlite3_free(err_msg_);  \
				err_msg_ = nullptr;  \
				TWIST_THROW(msg.c_str());  \
			}

namespace twist::db {

typedef std::pair<SqliteRecord*, SqliteRecordCallback*>  RecordCallbackParam;

// --- SqliteConnection::Impl struct ---

struct SqliteConnection::Impl {
	sqlite3* conn;
};

// --- SqliteConnection class ---

auto SqliteConnection::new_file_db(fs::path path) -> std::unique_ptr<SqliteConnection>
{
	if (exists(path)) {
		TWIST_THRO2(L"File \"{}\" already exists.", path.c_str());
	}
	return std::unique_ptr<SqliteConnection>{new SqliteConnection{DbFile{}, std::move(path), false/*read_only*/}};
}

auto SqliteConnection::open_file_db(fs::path path, bool read_only) -> std::unique_ptr<SqliteConnection>
{
	if (!exists(path)) {
		TWIST_THRO2(L"File \"{}\" not found.", path.c_str());
	}
	return std::unique_ptr<SqliteConnection>{new SqliteConnection{DbFile{}, std::move(path), read_only}};
}

auto SqliteConnection::new_memory_db(std::string_view db_name, bool shared) -> std::unique_ptr<SqliteConnection>
{
	return std::unique_ptr<SqliteConnection>{new SqliteConnection{MemoryDb{}, db_name, shared, false/*read_only*/}};
}

auto SqliteConnection::open_memory_db(std::string_view db_name, bool read_only) -> std::unique_ptr<SqliteConnection>
{
	return std::unique_ptr<SqliteConnection>{new SqliteConnection{MemoryDb{}, db_name, true/*shared*/, read_only}};
}

SqliteConnection::SqliteConnection(DbFile, fs::path path, bool read_only)
	: path_{std::move(path)}
	, is_in_memory_db_{false}
	, pimpl_{std::make_unique<Impl>()}
{
	try {
		if (!read_only) {
			TWIST_DB_CHECK_SQLITE3_RESULT_LOCAL(sqlite3_open(path_.string().c_str(), &pimpl_->conn));
		}
		else {
			TWIST_DB_CHECK_SQLITE3_RESULT_LOCAL(sqlite3_open_v2(path_.string().c_str(), 
			                                                    &pimpl_->conn,
																SQLITE_OPEN_READONLY,
																nullptr/*vfs*/));
		}
	}
	catch (RuntimeError& ex) {
		ex.prepend(std::format(L"Cannot open database file: \"{}\".\n", path_.c_str()));
		throw;
	}
	TWIST_CHECK_INVARIANT
}

#pragma warning(push)
#pragma warning(disable: 4702)
SqliteConnection::SqliteConnection(MemoryDb, std::string_view name, bool shared, bool read_only)
	: is_in_memory_db_{true}
	, pimpl_{std::make_unique<Impl>()}
{
#if (!defined SQLITE_USE_URI) || (SQLITE_USE_URI != 1)
	name; shared; read_only;
	TWIST_THRO2(L"The Sqlite library source must be compiled with SQLITE_USE_URI defined as 1 for this constructor to "
	            L"work, as it uses the URI form to open the in-memory database connection.");
#else 
	assert(!is_whitespace(name));
	try {
		auto uri = std::format("file:{}?mode=memory", name);
		if (shared) {
			uri += "&cache=shared";
		}
		TWIST_DB_CHECK_SQLITE3_RESULT_LOCAL(sqlite3_open_v2(uri.c_str(), 
			                                                &pimpl_->conn,
															(read_only ? SQLITE_OPEN_READONLY
															           : (SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE)),
															nullptr/*vfs*/));
	}
	catch (RuntimeError& ex) {
		ex.prepend(std::format(L"Cannot create memory database: \"{}\".\n", ansi_to_string(name)));
		throw;
	}
	TWIST_CHECK_INVARIANT
#endif
}
#pragma warning(pop)

SqliteConnection::~SqliteConnection()
{
	TWIST_CHECK_INVARIANT
	if (err_msg_ != nullptr) {
		sqlite3_free(err_msg_);
	}
	if (pimpl_->conn != nullptr) {
		sqlite3_close(pimpl_->conn);
	}
}

auto SqliteConnection::is_in_memory_database() const -> bool
{
	TWIST_CHECK_INVARIANT
	return is_in_memory_db_;
}

auto SqliteConnection::path() const -> fs::path
{
	TWIST_CHECK_INVARIANT
	return path_;
}

void SqliteConnection::exec(const std::string& sql)
{
	TWIST_CHECK_INVARIANT
	try {
		TWIST_DB_CHECK_SQLITE3_RESULT_LOCAL(sqlite3_exec(pimpl_->conn, sql.c_str(), nullptr, nullptr, &err_msg_));
	}
	catch (RuntimeError& ex) {
		ex.append(std::format(L"\nCommand failed: \"{}\".", ansi_to_string(sql)));
		throw;
	}
}


void SqliteConnection::read_rec_set(const std::string& sql, SqliteRecordCallback rec_callback) const
{
	TWIST_CHECK_INVARIANT
	try {
		SqliteRecord rec;
		auto param = std::make_pair(&rec, &rec_callback);
		TWIST_DB_CHECK_SQLITE3_RESULT_LOCAL( sqlite3_exec(
				pimpl_->conn, sql.c_str(), read_rec_callback, &param, &err_msg_) );
	}
	catch (RuntimeError& ex) {
		ex.append(std::format(L"\nCommand failed: \"{}\".", ansi_to_string(sql)));
		throw;
	}
}


Ssize SqliteConnection::count_records(const std::string& table_name, const std::string& where_clause) const
{
	TWIST_CHECK_INVARIANT
	auto ret = k_no_size;

	auto count_sql = "SELECT COUNT(*) AS rec_count FROM " + table_name;
	if (!where_clause.empty()) {
		count_sql += " WHERE " + where_clause;
	}

	read_rec_set(count_sql, [&ret](const auto& rec) {
		ret = rec.get_int(0);		
	});

	if (ret == k_no_size) {
		TWIST_THRO2(L"Database error reading results of query \"{}\".", ansi_to_string(count_sql));
	}

	return ret;
}

auto SqliteConnection::table_exists(std::string_view table_name) const -> bool
{
	TWIST_CHECK_INVARIANT
	auto query_result = std::string{};
	auto count_sql = std::format("SELECT name FROM sqlite_master WHERE type='table' AND name='{}'", table_name);
	read_rec_set(count_sql, [&query_result](const auto& rec) {
		query_result = rec.get_ansi(0);		
	});
	return query_result == table_name;
}

long long SqliteConnection::last_insert_rowid() const
{
	TWIST_CHECK_INVARIANT
	return sqlite3_last_insert_rowid(pimpl_->conn);
}

auto SqliteConnection::prepare(const std::string& sql) -> std::unique_ptr<SqlitePreparedStatement>
{
	TWIST_CHECK_INVARIANT
	sqlite3_stmt* statement = nullptr;    
	try {
        TWIST_DB_CHECK_SQLITE3_RESULT_LOCAL(sqlite3_prepare_v2(pimpl_->conn,
                                                               sql.c_str(),
                                                               static_cast<int>(sql.size()),
                                                               &statement,
                                                               nullptr));
	}
	catch (RuntimeError& ex) {
		ex.append(std::format(L"Statement failed: \"{}\".", ansi_to_string(sql)));
		throw;
	}	
	return std::unique_ptr<SqlitePreparedStatement>{
			new SqlitePreparedStatement{
					std::make_unique<SqlitePreparedStatement::Impl>(statement, pimpl_->conn)}};
}


SqliteStepResult SqliteConnection::step(const SqlitePreparedStatement& statement)
{
	TWIST_CHECK_INVARIANT
	const int result = sqlite3_step(statement.pimpl_->stmt());
	if (result != SQLITE_DONE && result != SQLITE_ROW) { //+ How about SQLITE_BUSY? That is legitimate if the statement is a COMMIT or occurs outside of an explicit transaction, then you can retry the statement.
		TWIST_DB_CHECK_SQLITE3_RESULT_LOCAL(result);
	}
	return result == SQLITE_DONE ? SqliteStepResult::done : SqliteStepResult::row;
}


int SqliteConnection::column_count(const SqlitePreparedStatement& statement)
{
	TWIST_CHECK_INVARIANT
	return sqlite3_column_count(statement.pimpl_->stmt());
}


int SqliteConnection::column_bytes(const SqlitePreparedStatement& statement, int col_idx)
{
	TWIST_CHECK_INVARIANT
	return sqlite3_column_bytes(statement.pimpl_->stmt(), col_idx);
}


std::string SqliteConnection::column_text(const SqlitePreparedStatement& statement, int col_idx, 
		bool null_text_as_empty)
{
	TWIST_CHECK_INVARIANT
	const unsigned char* buffer = sqlite3_column_text(statement.pimpl_->stmt(), col_idx);
	if (buffer == nullptr) {
		if (null_text_as_empty) {
			return "";
		}
		TWIST_THRO2(L"The cell corresponding to column {} is NULL.", col_idx);
	}
	std::string text;
	while (*buffer != 0) {
		text += *buffer++;
	}
	return text;
}


std::wstring SqliteConnection::column_text16(const SqlitePreparedStatement& statement, int col_idx, 
		bool null_text_as_empty)
{
	TWIST_CHECK_INVARIANT
	const wchar_t* buffer = static_cast<const wchar_t*>(
			sqlite3_column_text16(statement.pimpl_->stmt(), col_idx));
	if (buffer == nullptr) {
		if (null_text_as_empty) {
			return L"";
		}
		TWIST_THRO2(L"The cell corresponding to column {} is NULL.", col_idx);
	}
	std::wstring text;
	while (*buffer != 0) {
		text += *buffer++;
	}
	return text;
}


int SqliteConnection::column_int(const SqlitePreparedStatement& statement, int col_idx)
{
	TWIST_CHECK_INVARIANT
	return sqlite3_column_int(statement.pimpl_->stmt(), col_idx);
}


std::int64_t SqliteConnection::column_int64(const SqlitePreparedStatement& statement, int col_idx)
{
	TWIST_CHECK_INVARIANT
	return sqlite3_column_int64(statement.pimpl_->stmt(), col_idx);
}


double SqliteConnection::column_real(const SqlitePreparedStatement& statement, int col_idx)
{
	TWIST_CHECK_INVARIANT
	return sqlite3_column_double(statement.pimpl_->stmt(), col_idx);
}


const void* SqliteConnection::column_blob(const SqlitePreparedStatement& statement, int col_idx)
{
	TWIST_CHECK_INVARIANT
	return sqlite3_column_blob(statement.pimpl_->stmt(), col_idx);
}


bool SqliteConnection::column_is_null(const SqlitePreparedStatement& statement, int col_idx) const
{
	TWIST_CHECK_INVARIANT
	return sqlite3_column_type(statement.pimpl_->stmt(), col_idx) == SQLITE_NULL;
}


void SqliteConnection::begin_transaction()
{
	TWIST_CHECK_INVARIANT
	if (in_transaction_) {
		TWIST_THRO2(L"This database connection is already in a transaction.");
	}
	exec("BEGIN");
	in_transaction_ = true;
}


void SqliteConnection::commit_transaction()
{
	TWIST_CHECK_INVARIANT
	if (!in_transaction_) {
		TWIST_THRO2(L"This database connection is not in a transaction.");
	}
	exec("COMMIT");
	in_transaction_ = false;
}

void SqliteConnection::rollback_transaction()
{
	TWIST_CHECK_INVARIANT
	if (!in_transaction_) {
		TWIST_THRO2(L"This database connection is not in a transaction.");
	}
	exec("ROLLBACK");
	in_transaction_ = false;
}

int SqliteConnection::read_rec_callback(void* param, int num_cols, char** col_data, char** col_names)
{
	assert(param != nullptr);
	assert(num_cols > 0);

	auto* rec = static_cast<RecordCallbackParam*>(param)->first;
	assert(rec != nullptr);

	auto* callback = static_cast<RecordCallbackParam*>(param)->second;
	assert(callback != nullptr);

	if (!rec->col_indexer_) {
		// This is the very first row, so set the recordset column information
		rec->set_cols(num_cols, col_names);
	}
	
	// Set the new row data
	rec->cell_data_ = col_data;
	
	// Fire the callback
	(*callback)(*rec);

	return 0;
}


std::wstring SqliteConnection::get_error_descr(int err_no) const
{
	TWIST_CHECK_INVARIANT
	std::stringstream msg;                                                                 
	if (err_no != SQLITE_OK) {                                                                 								                                                             \
		
		msg << "An Sqlite3 error occurred: \"" << get_error_code_descr(err_no) << "\".";

		const char* err_descr = sqlite3_errmsg(pimpl_->conn);                                             
		if (err_descr != nullptr && strlen(err_descr) > 0) {                       
			msg << "\nDescription : " << err_descr << ".";                                                  
			if (err_msg_ != nullptr && strlen(err_msg_) > 0 && strcmp(err_descr, err_msg_) != 0) {
				msg << "\nDetails : " << err_msg_ << ".";
			}
		}
		else if (err_msg_ != nullptr && strlen(err_msg_) > 0) {
			msg << "\nDescription : " << err_msg_ << ".";                                                  
		}                                                                                     
	}
	else {
		msg << "No error occurred.";
	}

	return ansi_to_string(msg.str());
}


#ifdef _DEBUG
void SqliteConnection::check_invariant() const noexcept
{
	assert(is_in_memory_db_ == path_.empty());
	assert(pimpl_);
	if (pimpl_) {
		assert(pimpl_->conn);
	}
}
#endif 

} 


/// @file sql_assembler.cpp
/// Implementation file for "sql_assembler.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/db/sql_assembler.hpp"

#include "twist/string_utils.hpp"

namespace twist::db {

SqlNameValuePair::SqlNameValuePair(std::string name, std::string value)
	: name_{move(name)}
	, value_str_("'" + value + "'")
{
	TWIST_CHECK_INVARIANT
}

SqlNameValuePair::SqlNameValuePair(std::string name, std::wstring value)
	: name_{move(name)}
	, value_str_("'" + string_to_ansi(value) + "'")
{
	TWIST_CHECK_INVARIANT
}

SqlNameValuePair::SqlNameValuePair(std::string name, int value)
	: name_{move(name)}
	, value_str_{std::to_string(value)}
{
	TWIST_CHECK_INVARIANT
}

SqlNameValuePair::SqlNameValuePair(std::string name, unsigned int value)
	: name_{move(name)}
	, value_str_{std::to_string(value)}
{
	TWIST_CHECK_INVARIANT
}

SqlNameValuePair::SqlNameValuePair(std::string name, long value)
	: name_{move(name)}
	, value_str_{std::to_string(value)}
{
	TWIST_CHECK_INVARIANT
}

SqlNameValuePair::SqlNameValuePair(std::string name, unsigned long value)
	: name_{move(name)}
	, value_str_{std::to_string(value)}
{
	TWIST_CHECK_INVARIANT
}

SqlNameValuePair::SqlNameValuePair(std::string name, double value)
	: name_{move(name)}
{
	char buffer[34];
	sprintf(buffer, "%.7f", value);
	value_str_ = buffer;
	TWIST_CHECK_INVARIANT
}

std::string SqlNameValuePair::get_name() const
{
	TWIST_CHECK_INVARIANT
	return name_;
}

std::string SqlNameValuePair::get_value_str() const
{
	TWIST_CHECK_INVARIANT
	return value_str_;
}

#ifdef _DEBUG
void SqlNameValuePair::check_invariant() const noexcept
{
	assert(!name_.empty());
}
#endif

// --- Global functions ---

std::string assemble_insert_sql(const std::string& table_name, const SqlNameValuePairs& values)
{
	if (table_name.empty()) {
		TWIST_THROW(L"Database table name cannot be blank.");
	}
	if (values.empty()) {
		TWIST_THROW(L"At least one value must be present in the INSERT query.");
	}
	std::string col_names;
	std::string row_values;
	auto first_it = begin(values);
	auto last_it = end(values);
	for (auto it = first_it; it != last_it; ++it) {
		if (it != first_it) {
			col_names  += ", ";
			row_values += ", ";
		}
		col_names  += it->get_name();
		row_values += it->get_value_str();
	}
	return "INSERT INTO " + table_name + " (" + col_names + ") VALUES (" + row_values + ");";	
}


bool remove_sql_value(std::string_view col_name, SqlNameValuePairs& values)
{
	bool ret = false;
	auto it = find_if(values, [col_name](const SqlNameValuePair& el) {
		return el.get_name() == col_name;
	});
	if (it != end(values)) {
		values.erase(it);
		ret = true;
	}
	return ret;
}

}

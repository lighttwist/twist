/// @file HeterogenousNumericTable.cpp
/// Implementation file for "HeterogenousNumericTable.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/db/HeterogenousNumericTable.hpp"

#include "twist/cast.hpp"
#include "twist/std_vector_utils.hpp"

namespace twist::db {

// --- HeterogenousNumericTable class ---

HeterogenousNumericTable::HeterogenousNumericTable(
        const Tuples<std::wstring, HeterogenousNumericTableColumnType>& col_names_types,
        Ssize estimated_nof_rows)
{
    const auto nof_cols = ssize(col_names_types);
    if (nof_cols == 0) {
        TWIST_THRO2(L"The table needs at least one column.");
    }
    assert(estimated_nof_rows >= 0);

    auto j = 0;
    for (const auto& [col_name, col_type] : col_names_types) {
        auto col_data = std::optional<Column::Data>{};
        switch (col_type) {
            using enum HeterogenousNumericTableColumnType;
        case int32:
            col_data.emplace(reserve_vector<int32_t>(estimated_nof_rows));
            break;
        case int64:
            col_data.emplace(reserve_vector<int64_t>(estimated_nof_rows));
            break;
        case float32:
            col_data.emplace(reserve_vector<float>(estimated_nof_rows));
            break;
        case float64:
            col_data.emplace(reserve_vector<double>(estimated_nof_rows));
            break;
        default:
            TWIST_THRO2(L"Invalid HeterogenousNumericTableColumnType value {}.", as_underlying(col_type));
        }
        columns_.emplace_back(col_name, col_type, move(*col_data));
        if (auto [_, ok] = col_indexes_by_name_.emplace(col_name, j++); !ok) {
            TWIST_THRO2(L"Duplicate column name \"{}\".", col_name);
        }
    }
    TWIST_CHECK_INVARIANT
}

auto HeterogenousNumericTable::get_column_name(Ssize col_idx) const -> std::wstring
{
    TWIST_CHECK_INVARIANT
    return columns_.at(col_idx).name();
}

auto HeterogenousNumericTable::get_column_index(std::wstring_view col_name) const -> Ssize
{
    TWIST_CHECK_INVARIANT
    if (auto it = col_indexes_by_name_.find(col_name); it != end(col_indexes_by_name_)) {
        return it->second;
    }
    TWIST_THRO2(L"Column name \"{}\" not found.", col_name);
}

auto HeterogenousNumericTable::get_column_type(Ssize col_idx) const -> HeterogenousNumericTableColumnType
{
    TWIST_CHECK_INVARIANT
    return columns_.at(col_idx).type();
}

auto HeterogenousNumericTable::nof_rows() const -> Ssize
{
    TWIST_CHECK_INVARIANT
    return nof_rows_;
}

#ifdef _DEBUG
auto HeterogenousNumericTable::check_invariant() const noexcept -> void
{
    assert(!columns_.empty());
    assert(ssize(col_indexes_by_name_) == ssize(columns_));
    assert(nof_rows_ >= 0);
}
#endif

// --- HeterogenousNumericTable::Column class ---

HeterogenousNumericTable::Column::Column(std::wstring name, HeterogenousNumericTableColumnType type, Data data)
    : name_{move(name)} 
    , type_{type}
    , data_{move(data)}
{
    TWIST_CHECK_INVARIANT
}

auto HeterogenousNumericTable::Column::name() const -> std::wstring
{
    TWIST_CHECK_INVARIANT
    return name_;
}

auto HeterogenousNumericTable::Column::type() const -> HeterogenousNumericTableColumnType
{
    TWIST_CHECK_INVARIANT
    return type_;
}

auto HeterogenousNumericTable::Column::size() const -> Ssize
{
    return Ssize();
}

#ifdef _DEBUG
auto HeterogenousNumericTable::Column::check_invariant() const noexcept -> void
{
    assert(!is_whitespace(name_));
    assert(is_valid(type_));
}
#endif

}

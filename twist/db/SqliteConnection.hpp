/// @file SqliteConnection.hpp
/// SqliteConnection class definition

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_DB_SQLITE_CONNECTION_HPP
#define TWIST_DB_SQLITE_CONNECTION_HPP

#include "twist/metaprogramming.hpp"
#include "twist/db/sqlite_utils.hpp"

namespace twist::db {
class SqlitePreparedStatement;
}

namespace twist::db {

//! An open connection to an Sqlite database.
class SqliteConnection {
public:
	/*! Create a new Sqlite database file and open it.
	    \param[in] path  The database file path; an exception is thrown if the file already exists
	    \return  A database connection object attached to the file
	 */
	[[nodiscard]] static auto new_file_db(fs::path path) -> std::unique_ptr<SqliteConnection>;

	/*! Open an existing SQLite database file.
	    \param[in] path  The dabase file path; an exception is thrown if the file does not exist
	    \param[in] read_only  Whether the database file should be opened in read-only mode
	    \return  A database object attached to the file
	 */
	[[nodiscard]] static auto open_file_db(fs::path path, bool read_only) -> std::unique_ptr<SqliteConnection>;

	/*! Create a new SQLite "in-memory" database, ie a database stored purely in memory.
	    The database is automatically deleted and memory is reclaimed when the last connection to the database closes. 
		\param[in] tag  Tag value used for tag dispatching
	    \param[in] db_name  The database name
		\param[in] shared  Whether other database connections can connect to this memory database, using the name 
		                   provided
	 */
	[[nodiscard]] static auto new_memory_db(std::string_view db_name, bool shared) 
	                           -> std::unique_ptr<SqliteConnection>;

	/*! Open an "in-memory" SQLite database, ie a database stored purely in memory, in read-only mode.  
		\note  We can only connect to an existing in-memory databaase if the connection which created it used the 
		       "shared" mode.
		\param[in] tag  Tag value used for tag dispatching
	    \param[in] db_name  The database name; must match the name used by the connection which created the in-memory 
		                    database
	 */
	[[nodiscard]] static auto open_memory_db(std::string_view db_name, bool read_only)
	                           -> std::unique_ptr<SqliteConnection>;

	~SqliteConnection();

	/*! Whether the database is an "in-memory" database, ie a database stored purely in memory, rather than based on 
	    a file.
	 */
	[[nodiscard]] auto is_in_memory_database() const -> bool;

	/*! The database file path; or blank if the database is an "in-memory" database, ie a database stored purely 
	    in memory.
	 */
	[[nodiscard]] auto path() const -> fs::path;

	/// Execute an SQL command. An exception is thrown if the command is not executed successfully.
	/// Note that only one SQL command should be passed into this method (multiple commands separated by 
	/// semicolon are not accepted).
	///
	/// @param[in] sql  The SQL command
	/// 
	void exec(const std::string& sql);

	/// Read the data returned by an SQL query into a recordset.
	///
	/// @param[in] sql  The SQL query
	/// @param[in] rec_callback  Functor which will be called exactly once for each record in the recordset
	///
	void read_rec_set(const std::string& sql, SqliteRecordCallback rec_callback) const;

	/// Count (a subset of) the records in a specific database table.
	///
	/// @param[in] table_name  The table name
	/// @param[in] where_clause  An optional WHERE clause, for filtering for a subset of the records in a table; the keyword 
	///				WHERE should not be included, eg a valid clause would be "id=2 AND name='bob'"; pass in an empty string to 
	///				count all the records in the table
	/// @return  The number of records
	///
	Ssize count_records(const std::string& table_name, const std::string& where_clause = "") const;

	//! Whether the table named \p table_name exists in the database.
	[[nodiscard]] auto table_exists(std::string_view table_name) const -> bool;

	/// Get the "rowid" (the value of the integer primary key) of the most recent successful INSERT into the database.
	///
	/// @return  The "rowid" value
	///
	long long last_insert_rowid() const;

	/// Compile an SQL statement into a virtual machine. 
	///
	/// @param[in] sql  The statement
	///
	std::unique_ptr<SqlitePreparedStatement> prepare(const std::string& sql);

	/// Evaluate a compiled (prepared) statement.
	///
	/// @param[in] statement  The compiled statement
	/// @return  The evaluation result
	///
	SqliteStepResult step(const SqlitePreparedStatement& statement);

	/// Get the number of columns returned by evaluating a compiled (prepared) statement. 
	///
	/// @param[in] statement  The compiled statement
	/// @return  The number of columns
	/// 
	int column_count(const SqlitePreparedStatement& statement);

	/// Get the number of bytes in a specific column returned by evaluating a compiled (prepared) statement.
	///
	/// @param[in] statement  The compiled statement
	/// @param[in] col_idx  The column index
	///
	int column_bytes(const SqlitePreparedStatement& statement, int col_idx);

	/// Get the ANSI text value in a specific column returned by evaluating a compiled (prepared) statement.
	///
	/// @param[in] statement  The compiled statement
	/// @param[in] col_idx  The column index
	/// @param[in] null_text_as_empty  if true, retrieving a NULL value will return an empty string; if 
	///					false, it will throw an exception
	/// @return  The value
	///
	std::string column_text(const SqlitePreparedStatement& statement, int col_idx, 
			bool null_text_as_empty = true);

	/// Get the text value in a specific column returned by evaluating a compiled (prepared) statement.
	/// Devnote: the function was not tested for exotic character sets.
	///
	/// @param[in] statement  The compiled statement
	/// @param[in] col_idx  The column index
	/// @param[in] null_text_as_empty  if true, retrieving a NULL value will return an empty string; if 
	///					false, it will throw an exception
	/// @return  The value
	///
	std::wstring column_text16(const SqlitePreparedStatement& statement, int col_idx, 
			bool null_text_as_empty = true);

	/// Get the 32-bit integer value in a specific column returned by evaluating a compiled (prepared) 
	/// statement.
	///
	/// @param[in] statement  The compiled statement
	/// @param[in] col_idx  The column index
	/// @return  The value
	///
	int column_int(const SqlitePreparedStatement& statement, int col_idx);

	/// Get the 64-bit integer value in a specific column returned by evaluating a compiled (prepared) 
	/// statement.
	///
	/// @param[in] statement  The compiled statement
	/// @param[in] col_idx  The column index
	/// @return  The value
	///
	std::int64_t column_int64(const SqlitePreparedStatement& statement, int col_idx);

	/// Get the 64-bit floating-point number value in a specific column returned by evaluating a compiled 
	/// (prepared) statement.
	///
	/// @param[in] statement  The compiled statement
	/// @param[in] col_idx  The column index
	/// @return  The value
	///
	double column_real(const SqlitePreparedStatement& statement, int col_idx);

	/// Get access to the buffer associated with a specific column returned by evaluating a compiled 
	/// (prepared) statement.
	///
	/// @param[in] statement  The compiled statement
	/// @param[in] col_idx  The column index
	/// @return  The buffer
	///
	const void* column_blob(const SqlitePreparedStatement& statement, int col_idx);

	/// Read the buffer associated with a specific column returned by evaluating a compiled (prepared) 
	/// statement into an array.
	///
	/// @tparam  BlobElem  The type of the array elements, as they are stored in the buffer to be read 
	/// @tparam  Cont  The type of the STL-like container which will hold the array
	/// @param[in] statement  The compiled statement
	/// @param[in] col_idx  The column index
	/// @param[in] cont  The container which will be filled with the array values; it will be resized 
	///					appropriately and any existing contents will be lost
	///
	template<typename BlobElem, class Cont> 
	void column_array_blob(const SqlitePreparedStatement& statement, int col_idx, Cont& cont);

	/// Read the compressed buffer associated with a specific column returned by evaluating a compiled 
	/// (prepared) statement, then uncompressed the buffer into an array container.
	///
	/// @tparam  Cont  The type of the STL-like container which will hold the array
	/// @param[in] statement  The compiled statement
	/// @param[in] col_idx  The column index
	/// @param[in] cont  The container which will be filled with the array values; it must be large enough 
	///					to hold all the elements in the array resulting from decompressing the database buffer
	/// @param[in] compression  The compression type
	/// @return  The number of elements in the array resulting from decompressing the database buffer
	///
	template<class Cont> 
	size_t column_uncompress_array_blob(const SqlitePreparedStatement& statement, int col_idx, Cont& cont, 
			SqliteBlobCompression compression = SqliteBlobCompression::zlib);

	/// Find out whether a specific column returned by evaluating a compiled (prepared) statement contains the 
	/// NULL value.
	///
	/// @param[in] statement  The compiled statement
	/// @param[in] col_idx  The index of the column within the statement
	/// @return  true if the column contains NULL
	///
	bool column_is_null(const SqlitePreparedStatement& statement, int col_idx) const;

	/// Begin a database transaction on this connection. An exception is thrown if the connection is already 
	/// in a transaction.
	///
	void begin_transaction();

	/// Commit the database transaction on this connection. An exception is thrown if the connection is not in 
	/// a transaction.
	///
	void commit_transaction();

	/// Rollback the database transaction on this connection. An exception is thrown if the connection is not 
	/// in a transaction.
	///
	void rollback_transaction();

	TWIST_NO_COPY_NO_MOVE(SqliteConnection)

private:
	struct Impl;
	struct DbFile {};
	struct MemoryDb {};

	explicit SqliteConnection(DbFile, fs::path path, bool read_only); 

	explicit SqliteConnection(MemoryDb, std::string_view name, bool shared, bool read_only); 

	/// Function called by the framework for each record returned by a query run in read_rec_set().
	///
	/// @param[in] param  Custom parameter, in this case a pair of the record wrapper object and the record 
	///					callback functor
	/// @param[in] num_cols  The number of cells/columns in th record
	/// @param[in] col_data  The raw cell/column data
	/// @param[in] col_names  The column names
	/// @return  Zero if successful
	///
	static int read_rec_callback(void* param, int num_cols, char** col_data, char** col_names);

	std::wstring get_error_descr(int err_no) const;
	
	const fs::path path_;
	const bool is_in_memory_db_;
	std::unique_ptr<Impl> pimpl_; 
	mutable char* err_msg_{nullptr};
	bool in_transaction_{false};

	TWIST_CHECK_INVARIANT_DECL
};

// --- Free functions ---

/// Start a transaction on a connection to an Sqlite database, and run user-provided actions on that 
/// connection; the transction is then commited, if the actions were successful, or rolled-back otherwise.
///
/// @tparam  Fn  Callable type compatible with signature (SqliteConnection&) -> auto
/// @param[in] conn  The Sqlite connection
/// @param[in] func  Callable object which contains the actions to be performed on the connection, which is
///					passed into the function
///
template<class Fn, class = EnableIfFunc<Fn, SqliteConnection&>>
void transact(SqliteConnection& conn, Fn&& func);

}

#include "twist/db/SqliteConnection.ipp"

#endif 

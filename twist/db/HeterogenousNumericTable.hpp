/// @file HeterogenousNumericTable.hpp
/// HeterogenousNumericTable class definition

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_DB_HETEROGENOUS_NUMERIC_TABLE_HPP
#define TWIST_DB_HETEROGENOUS_NUMERIC_TABLE_HPP

#include <variant>

namespace twist::db {

/*! Data type associated with a "heterogenous numeric table" column.
    All values in a column have the same (numeric) type. 
 */
enum class HeterogenousNumericTableColumnType {
    int32, ///< 32-bit signed integer 
    int64, ///< 64-bit signed integer 
    float32, ///< 32-bit floating-point number
    float64 ///< 64-bit floating-point number
};

/*! A data table whose cells contain numeric values. The cell values in a column must have the same type, but different 
    columns can have different associated value types. Each column has a unique, non-blank name (name comparisons are 
    case-sensitive). The data values for each column are stored in a contiguous memory buffer.
 */
class HeterogenousNumericTable {
public:
    /*! Constructor.
        \param[in] col_names_types  The names and associated data types of each column; there must be at least one 
                                    column 
        \param[in] estimated_nof_rows  If the final number of rows in the table is known, or suspected, pass it in here
                                       to pre-allocate memory in one go (this prevents further re-allocations as the 
                                       rows are added to the table); zero means unkonwn
     */
    explicit HeterogenousNumericTable(const Tuples<std::wstring, HeterogenousNumericTableColumnType>& col_names_types, 
                                      Ssize estimated_nof_rows = 0);

    //! Get the name of the column with index \p col_idx.
    [[nodiscard]] auto get_column_name(Ssize col_idx) const -> std::wstring;

    //! Get the index of the column with name \p col_name.
    [[nodiscard]] auto get_column_index(std::wstring_view col_name) const -> Ssize;

    //! Get the data type associated with the column with index \p col_idx.
    [[nodiscard]] auto get_column_type(Ssize col_idx) const -> HeterogenousNumericTableColumnType;

    /*! Get read-only access to the contiguous data buffer associated with the column with index \p col_idx.
        \tparam  The type of the values stored in the the column; if it does not match the column data type an 
                 exception is thrown
     */ 
    template<class CellVal>
    [[nodiscard]] auto get_column_data(Ssize col_idx) const -> const std::vector<CellVal>&;

    /*! Get read-only access to the contiguous data buffer associated with the column with name \p col_name.
        \tparam  The type of the values stored in the the column; if it does not match the column data type an 
                 exception is thrown
     */ 
    template<class CellVal>
    [[nodiscard]] auto get_column_data(std::wstring_view col_name) const -> const std::vector<CellVal>&;

    //! The number of rows in the table.
    [[nodiscard]] auto nof_rows() const -> Ssize;

    /*! Add a row to the table.
        \tparam CellVals  The type of values of the values passed in for each column; if they do not match the column 
                          data types (in the order of the columns in the table), an exception is thrown
        \param[in] cell_values  The values in the new row, one for each column (in the ordet of the columns in the 
                                table)
     */
    template<class ...CellVals>
    auto add_row(CellVals... cell_values) -> void;

    TWIST_NO_COPY_DEF_MOVE(HeterogenousNumericTable)

private:
    class Column {
    public:
        using Data = std::variant<std::vector<int32_t>, 
                                  std::vector<int64_t>, 
                                  std::vector<float>, 
                                  std::vector<double>>;
        
        explicit Column(std::wstring name, HeterogenousNumericTableColumnType type, Data data);

        [[nodiscard]] auto name() const -> std::wstring;

        [[nodiscard]] auto type() const -> HeterogenousNumericTableColumnType;

        template<class CellVal>
        [[nodiscard]] auto data() const -> const std::vector<CellVal>&;

        template<class CellVal>
        auto add(CellVal cell_val) -> void;

        [[nodiscard]] auto size() const -> Ssize;

        TWIST_NO_COPY_DEF_MOVE(Column)

    private:
        std::wstring name_;
        HeterogenousNumericTableColumnType type_;
        Data data_;

        TWIST_CHECK_INVARIANT_DECL
    };

    template<size_t... I, class... CellVals>
    auto add_row_impl(std::index_sequence<I...>, CellVals... cell_values) -> void;
    
    std::vector<Column> columns_;
    std::map<std::wstring, Ssize, std::less<>> col_indexes_by_name_;
    twist::Ssize nof_rows_{0};

    TWIST_CHECK_INVARIANT_DECL
};

//! Whether \p type is a valid HeterogenousNumericTableColumnType value.
[[nodiscard]] constexpr auto is_valid(HeterogenousNumericTableColumnType type) -> bool;

}

#include "twist/db/HeterogenousNumericTable.ipp"

#endif 

/// @file DimensionInfo.hpp
/// DimensionInfo class definition

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_DB_NETCDF_DIMENSION_INFO_HPP
#define TWIST_DB_NETCDF_DIMENSION_INFO_HPP

namespace twist::db::netcdf {

/*! Stores information about a netCDF file dimension (a regular, "single dimension", as opposed to a a "dual 
    dimension"). 
    A dimension is a measurement "ruler" necessary for anchoring (by itself or together with other dimensions) the 
	values of any variable in a netCDF file. The spatial dimensions in Euclidian or non-Euclidian space and the time 
	dimension are examples of netCDF dimensions.
    A dimension is characterised by a name and a length.
 */
class DimensionInfo {
public:
	/*! Constructor.
		\param[in] name  The dimension name
		\param[in] length  The dimension length, that is, the number of values of any variable which are anchored by 
		                   this dimension
	 */
	DimensionInfo(const std::string& name, size_t length);

	//! The dimension name.
	[[nodiscard]] auto name() const -> std::string;
	
	//! The dimension length, that is, the number of values of any variable which are anchored by this dimension.
	[[nodiscard]] auto length() const -> size_t;

	TWIST_NO_COPY_NO_MOVE(DimensionInfo)
	
private:
	friend class NetcdfFile;
	
	std::string name_;
	size_t length_;

	TWIST_CHECK_INVARIANT_DECL
};

}

#endif

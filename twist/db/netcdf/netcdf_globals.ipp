/// @file netcdf_globals.ipp
/// Inline implementation file for "netcdf_globals.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace twist::db::netcdf {

template<typename T> 
void check_data_value_type(DataType data_type)
{
	const auto& templ_type_id = typeid(T);
	const auto templ_type_name = get_name(templ_type_id);

	switch (data_type) {
	case DataType::int8: 
		if (templ_type_id != typeid(int8_t)) {
			TWIST_THROW(L"Incompatible types \"int8\" and \"%s\".", templ_type_name.c_str());
		}
		break;
	case DataType::chr: 
		if (templ_type_id != typeid(char)) {
			TWIST_THROW(L"Incompatible types \"chr\" and \"%s\".", templ_type_name.c_str());
		}
		break;
	case DataType::int16: 
		if (templ_type_id != typeid(int16_t)) {
			TWIST_THROW(L"Incompatible types \"int16\" and \"%s\".", templ_type_name.c_str());
		}
		break;
	case DataType::int32: 
		if (templ_type_id != typeid(int32_t)) {
			TWIST_THROW(L"Incompatible types \"int32\" and \"%s\".", templ_type_name.c_str());
		}
		break;
	case DataType::float32: 
		if (templ_type_id != typeid(float)) {
			TWIST_THROW(L"Incompatible types \"float32\" and \"%s\".", templ_type_name.c_str());
		}
		break;
	case DataType::float64: 
		if (templ_type_id != typeid(double)) {
			TWIST_THROW(L"Incompatible types \"float64\" and \"%s\".", templ_type_name.c_str());
		}
		break;
	case DataType::uint16: 
		if (templ_type_id != typeid(uint16_t)) {
			TWIST_THROW(L"Incompatible types \"uint16\" and \"%s\".", templ_type_name.c_str());
		}
		break;
	case DataType::uint8: 
		if (templ_type_id != typeid(uint8_t)) {
			TWIST_THROW(L"Incompatible types \"uint8\" and \"%s\".", templ_type_name.c_str());
		}
		break;
	case DataType::uint32: 
		if (templ_type_id != typeid(uint32_t)) {
			TWIST_THROW(L"Incompatible types \"uint32\" and \"%s\".", templ_type_name.c_str());
		}
		break;
	case DataType::int64: 
		if (templ_type_id != typeid(int64_t)) {
			TWIST_THROW(L"Incompatible types \"int64\" and \"%s\".", templ_type_name.c_str());
		}
		break;
	case DataType::uint64: 
		if (templ_type_id != typeid(uint64_t)) {
			TWIST_THROW(L"Incompatible types \"uint64\" and \"%s\".", templ_type_name.c_str());
		}
		break;
	case DataType::string: 
		if (templ_type_id != typeid(std::string)) {
			TWIST_THROW(L"Incompatible types \"string\" and \"%s\".", templ_type_name.c_str());
		}
		break;
	default: 
		TWIST_THROW(L"Invalid data value type ID %d.", data_type);
	}
}

}


/// @file DualDimensionData.hpp
/// DualDimensionData class template definition and non-member function declarations

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_DB_NETCDF_DUAL_DIMENSION_DATA_HPP
#define TWIST_DB_NETCDF_DUAL_DIMENSION_DATA_HPP

#include "twist/db/netcdf/netcdf_globals.hpp"
#include "twist/db/netcdf/DimensionInfo.hpp"
#include "twist/db/netcdf/DualDimensionInfo.hpp"
#include "twist/db/netcdf/DualDimensionPosRanges.hpp"
#include "twist/db/netcdf/DualDimensionValueRanges.hpp"
#include "twist/db/netcdf/VariableInfo.hpp"

namespace twist::db::netcdf {
class DualDimensionInfo;
}

namespace twist::db::netcdf {

/*! Class which stores information about a netCDF "dual dimension" (as opposed to a regular, "single dimension") 
    together with the data associated with its measurement rulers.
    The two dimensions making up a "dual dimension" store their data in combination with one another, as the data of 
	the two variables corresponding to the "dual dimension"; both variables use both dimensions (in the same order) and 
	no other, and the data length for each of the two variables is the product of the two dimension lengths. The data 
	stored by one or both of the variables may not be uniformly spaced, nor monotonic. 
    \tparam Dim1Val  The value type of the data associated with the first dimension, ie the data stored by the first 
	                 variable
    \tparam Dim2Val  The value type of the data associated with the second dimension, ie the data stored by the second 
	                 variable
 */
template<class Dim1Val, class Dim2Val>
class DualDimensionData {
public:
	using DdimInfoPtr = std::shared_ptr<DualDimensionInfo>;
	using VarInfoPtr = std::shared_ptr<VariableInfo>;	
	
	//! Information about the dual dimension.
	DdimInfoPtr dual_dim_info() const;

	//! Information about the first variable corresponding to the "dual dimension".
	VarInfoPtr var1_info() const;
	
	//! Information about the second variable corresponding to the "dual dimension".
	VarInfoPtr var2_info() const;

	//! The data associated with the first dimension, ie the data stored by the first variable. 
	const std::vector<Dim1Val>& data1() const;

	//! The data associated with the second dimension, ie the data stored by the second variable.	
	const std::vector<Dim2Val>& data2() const;

	TWIST_NO_COPY_DEF_MOVE(DualDimensionData)

private:
	friend class NetcdfFile;

	DualDimensionData(DdimInfoPtr ddim_info, VarInfoPtr var1_info, VarInfoPtr var2_info, 
			          std::vector<Dim1Val>&& data1, std::vector<Dim2Val>&& data2);

	DdimInfoPtr ddim_info_;
	VarInfoPtr var1_info_; 
	VarInfoPtr var2_info_; 
	std::vector<Dim1Val> data1_;
	std::vector<Dim2Val> data2_;
	
	TWIST_CHECK_INVARIANT_DECL
};

// --- Non-member functions ---

/*! Given a pair of dimension value ranges, within the value ranges of the two dimensions composing a "dual dimension", 
    get the corresponding ranges of positions along the two dimensions. 
    Each returned position range is "safe", in that it may include values outside the desired value range, but not 
    exclude values inside the value range.
    \tparam Dim1Val  The value type of the data associated with the first dimension, ie the data stored by the first 
                     variable
    \tparam Dim2Val  The value type of the data associated with the second dimension, ie the data stored by the second 
                     variable
    \param[in] ddim_data  The "dual dimension" data
    \param[in] ddim_val_ranges  The dimension value ranges
    \param[in] border  If not nullptr, each position range will extend a number postions equal to this value, before 
                       the calculated lower bound and the same number of possitions after the calculated upper bound;
                       if it is not possible to extend this number of positions on both sides for both dimensions, the 
                       value is shrunk until it becomes possible 
    \return  The dimension position ranges, or nullptr if for one or both of the dimension value ranges, there are no 
             dimension values at all which fall within that range
 */
template<class Dim1Val, class Dim2Val> 
[[nodiscard]] auto get_dual_dim_pos_ranges(const DualDimensionData<Dim1Val, Dim2Val>& ddim_data,
			                               const DualDimensionValueRanges<Dim1Val, Dim2Val>& ddim_val_ranges, 
			                               size_t* border = nullptr) -> std::optional<DualDimensionPosRanges>;

/*! Given a pair of ranges of positions along the two dimensions composing a "dual dimension", get the 
    dimension data subset defined by the position ranges, for the first dimension in the "dual dimension".
    \tparam Dim1Val  The value type of the data associated with the first dimension, ie the data stored by 
   					 the first variable
    \tparam Dim2Val  The value type of the data associated with the second dimension, ie the data stored by 
   					 the second variable
    \param[in] ddim_data  The "dual dimension" data
    \param[in] ddim_pos_ranges  The dimension position ranges
    \return  The cropped dimension data
 */
template<class Dim1Val, class Dim2Val> 
[[nodiscard]] auto crop_data1(const DualDimensionData<Dim1Val, Dim2Val>& ddim_data,
			                  const DualDimensionPosRanges& ddim_pos_ranges) -> std::vector<Dim1Val>;

/*! Given a pair of ranges of positions along the two dimensions composing a "dual dimension", get the 
    dimension data subset defined by the position ranges, for the second dimension in the "dual dimension".
    \tparam Dim1Val  The value type of the data associated with the first dimension, ie the data stored by 
   					 the first variable
    \tparam Dim2Val  The value type of the data associated with the second dimension, ie the data stored by 
   					 the second variable
    \param[in] ddim_data  The "dual dimension" data
    \param[in] ddim_pos_ranges  The dimension position ranges
    \return  The cropped dimension data
 */
template<class Dim1Val, class Dim2Val> 
[[nodiscard]] auto crop_data2(const DualDimensionData<Dim1Val, Dim2Val>& ddim_data,
			                  const DualDimensionPosRanges& ddim_pos_ranges) -> std::vector<Dim2Val>;

/*! Given a "dual dimension"'s data, go through all "points" defined by the paired values of the two 
    dimensions (the "point coordinates"), considering the points as the values in a grid, with the first 
    dimension representing the grid rows (the "vertical" dimension) and the second dimension representing the 
    grid colums (the "horizontal" dimesnion). For each point, call the supplied function with its "coordinate" 
    values (the values of the dimension variables).
    \tparam Dim1Val  The value type of the data associated with the first dimension, ie the data stored by 
   					 the first variable
    \tparam Dim2Val  The value type of the data associated with the second dimension, ie the data stored by 
   					 the second variable
    \tparam Fn   Function-like type, must be callable with parameter types Dim1Val, Dim2Val, int, int; the 
   				 last two parameters are the grid row and column
    \param[in] ddim_data  The "dual dimension" data
    \param[in] func  Function-like object to be called with the dimension values and grid row and column
 */
template<class Dim1Val, class Dim2Val, class Fn> 
auto for_each_grid_point(const DualDimensionData<Dim1Val, Dim2Val>& ddim_data, Fn func) -> void;

}

#include "twist/db/netcdf/DualDimensionData.ipp"

#endif

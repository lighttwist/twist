/// @file netcdf_globals.hpp
/// Globals for the twist::db::netcfd namespace

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_DB_NETCDF_GLOBALS_HPP
#define TWIST_DB_NETCDF_GLOBALS_HPP

#include "twist/type_info_utils.hpp"

namespace twist::db::netcdf {

/// netCDF variable data types
enum class DataType {
	int8 = 1,    ///< signed 1 byte integer 
	chr = 2,     ///< ISO/ASCII character
	int16 = 3,   ///< signed 2 byte integer 
	int32 = 4,   ///< signed 4 byte integer 
	float32 = 5, ///< single precision floating point number 
	float64 = 6, ///< double precision floating point number 
	uint8 = 7,   ///< unsigned 1 byte int 
	uint16 = 8,  ///< unsigned 2-byte int 
	uint32 = 9,  ///< unsigned 4-byte int 
	int64 = 10,  ///< signed 8-byte int 
	uint64 = 11, ///< unsigned 8-byte int 
	string = 12  ///< string 
};

constexpr size_t netcdf_no_idx = static_cast<size_t>(-1);  ///< Invalid index in netCDF collections

/// Check whether a netCDF "data value type" ID matches a C++ type, that is, if a variable having the 
/// C++ type would be the best match to read a value from, or write a value to, a netCDF variable with 
/// the data type ID. If they match, nothing happens; otherwise, an exception is thrown.
///
/// @tparam  T  The C++ type
/// @param[in] data_type  The value type ID
///
template<typename T> void check_data_value_type(DataType data_type);  

/// Verify whether a DataType value is one of the defined enum values.
///
/// @param[in] data_type  The value
/// @return  true if the value is one of the defined enum values
///
bool is_defined(DataType data_type);

}

#include "twist/db/netcdf/netcdf_globals.ipp"

#endif

/// @file Attribute.hpp
/// Attribute class definition

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_DB_NETCDF_ATTRIBUTE_HPP
#define TWIST_DB_NETCDF_ATTRIBUTE_HPP

#include "twist/db/netcdf/netcdf_globals.hpp"

#include <variant>

namespace twist::db::netcdf {

/*! Class representing a netCDF attribute.
    An attribute has a a name, a data type, and a value. Multi-value attributes are not currently supported, except for 
    attributes with textual data types.
    An attribute is either attached to a variable, or to the netCDF file itself (global attribute).  
 */
class Attribute {
public:
	//! Attribute value type
	using Value = std::variant<std::string, float, double, char, unsigned char, short, unsigned short, 
							   int, unsigned int, long, long long>;

	/*! Constructor.
	    \param[in] name  The attribute name  
	    \param[in] data_type  The attribute data type  
	    \param[in] value  The attribute data value
	 */
	explicit Attribute(std::string name, DataType data_type, Value value);
 	
	//! The attribute name (unique per variable/file).
	[[nodiscard]] auto name() const -> std::string;

	//! The attribute data type.
	[[nodiscard]] auto data_type() const -> DataType;
	
	/*! The attribute data value: a variant holding the attribute value; query the variant according to the attribute 
	    data type.
	 */
	[[nodiscard]] auto value() const -> Value;
	
private:
	std::string name_;	
	DataType data_type_;
	Value value_;

	TWIST_CHECK_INVARIANT_DECL
};

}

#endif

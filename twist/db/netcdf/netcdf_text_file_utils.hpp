/// @file netcdf_text_file_utils.hpp
/// Utilities for exporting data from netCDF files to text files

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_DB_NETCDF__TEXT__FILE__UTILS_HPP
#define TWIST_DB_NETCDF__TEXT__FILE__UTILS_HPP

#include "twist/db/CsvFileWriter.hpp"
#include "twist/db/netcdf/NetcdfFile.hpp"

namespace twist::db::netcdf {

/// Given variable which uses two dimensions defined in netCDF file, read its data from the file and write it
/// out to a CSV file as a table, with one dimension on the horizontal and the other on the vertical.
///
/// @tparam  VarVal  The value type of the variable data; it must match the variable data type as 
///					specified in the file 
/// @tparam  Fn  Callable type compatible with signature (VarVal) -> std::string 
/// @param[in] file  Connection to the netCDF file
/// @param[in] var_name  The variable name
/// @param[in] x_dim_name  The name of the dimension on the horizontal (this dimension's length determines 
///					the number of columns) 
/// @param[in] y_dim_name  The name of the dimension on the vertical (this dimension's length determines 
///					the number of rows)
/// @param[in] csv_path  Path of the output CSV file
/// @param[in] data_val_to_str  Callable object which converts a data value to an ANSI string  
///
template<class VarVal, class Fn, class = EnableIfFuncR<std::string, Fn, VarVal>> 
void export_2d_var_data_to_csv(const NetcdfFile& file, const std::string& var_name, 
		const std::string& x_dim_name, const std::string& y_dim_name, const fs::path& csv_path,
		Fn&& data_val_to_str);

}

#include "twist/db/netcdf/netcdf_text_file_utils.ipp"

#endif

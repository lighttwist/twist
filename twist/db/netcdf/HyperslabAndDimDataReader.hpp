/// @file HyperslabAndDimDataReader.hpp
/// HyperslabAndDimDataReader class definition

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_DB_NETCDF_HYPERSLAB_AND_DIM_DATA_READER_HPP
#define TWIST_DB_NETCDF_HYPERSLAB_AND_DIM_DATA_READER_HPP

#include "twist/metaprogramming.hpp"
#include "twist/std_vector_utils.hpp"
#include "twist/db/netcdf/NetcdfFile.hpp"
#include "twist/db/netcdf/HyperslabReaderBase.hpp"
#include "twist/db/netcdf/HyperslabAndDimData.hpp"
#include "twist/db/netcdf/HyperslabAndDimParams.hpp"

namespace twist::db::netcdf {

/// 
///  Class which facilitates reading, from a netCDF file, information about a "hyperslab" which uses a 
///    number of "single dimensions", and the hyperslab data together with the data for each dimension used. 
///  See class template HyperslabAndDimData for details about the hyperslab data structure.
///  The hyperslab data being read is a part of the data (or the whole data) associated with a variable 
///    defined in the file. The hyperslab is defined by a subrange of values along each of the dimensions 
///    used by the hyperslab. 
///  See class template HyperslabAndDimParams for details about how the the hyperlab is specified along the
///	   dimensions.
///
///  @tparam  VarVal  The value type of the "hyperslab variable" data 
///  @tparam  DimVals...  The value type of each of the dimension's data, ie the data types of the variables 
///					associated with each of the "single dimensions" used by the hyperslab; the ordering of the
///					templates making up this variadic parameter matches the order of the dimensions for 
///					this class
///
template<class VarVal, class... DimVals>
class HyperslabAndDimDataReader : private HyperslabReaderBase {
public:
	using FilePtr = std::shared_ptr<NetcdfFile>;

	using Params = HyperslabAndDimParams<DimVals...>;

	using HyslabWithDimsData = HyperslabAndDimData<VarVal, DimVals...>;
	using HyslabWithDimsDataPtr = std::shared_ptr<HyslabWithDimsData>;

	/// The number of dimensions used by the hyperslab
	static const size_t dim_count = sizeof...(DimVals);

	/// Constructor.
	///
	/// @param[in] file  The netCDF file
	/// @param[in] params  The parameters which specify the hyperslab along each dimension, but not the 
	///					"hypeslab variable" (these parameters can be applied to multiple variables, 
	///					provided they use the appropriate dimensions); this class will use the dimension 
	///					ordering defined in the parameters
	///
	HyperslabAndDimDataReader(FilePtr file, const Params& params);

	/*! Read the hyperslab data from the file.
	    \param[in] var_name  The "hyperslab variable" name
	    \return  The hyperslab data
	 */
	[[nodiscard]] auto read_hyperslab_with_dims(std::string_view var_name) const -> HyslabWithDimsDataPtr;

	TWIST_NO_COPY_NO_MOVE(HyperslabAndDimDataReader)

private:
	using DimDataPtrTuple = std::tuple<DimDataPtr<DimVals>...>;

	template<size_t DimIdx, class DimVal> using DimRangeTupleElem = 
			IndexedTupleElement<DimIdx, DimensionValueRange<DimVal>>;

	const Params params_;

	TWIST_CHECK_INVARIANT_DECL
};

}

#include "twist/db/netcdf/HyperslabAndDimDataReader.ipp"

#endif


/// @file DimensionPosRange.hpp
/// DimensionPosRange class definition

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_DB_NETCDF_DIMENSION_POS_RANGE_HPP
#define TWIST_DB_NETCDF_DIMENSION_POS_RANGE_HPP

#include "twist/math/ClosedInterval.hpp"

namespace twist::db::netcdf {

class DimensionInfo;

/*! This class defines a range of positions along a netCDF dimension. 
    A usable netCDF dimension has, at any given time, a known non-zero length: the size of its measurement ruler (which 
	anchors, by itself or together with other dimensions, the values of netCDF variables). 
    If the size is n, the positions on the ruler, or along the dimension, fall in the closed interval [0, n-1]. 
	This class defines an interval [l, u] included in [0, n], generally used for cropping variable data.
 */
class DimensionPosRange {
public:
	using PosRange = twist::math::ClosedInterval<size_t>;
	using DimInfoPtr = std::shared_ptr<DimensionInfo>;

	/// Constructor.
	///
	/// @param[in] dim_info  Information about the dimension
	/// @param[in] pos_range  The range of positions along the dimension
	///
	DimensionPosRange(DimInfoPtr dim_info, PosRange pos_range);

	/// Get information about the dimension.
	///
	/// @return  The dimension information
	///
	DimInfoPtr dim_info() const;

	/// Get the dimension's name.
	///
	/// @return  The dimension name
	///
	std::string dim_name() const;
	
	/// Get the range of positions along the dimension.
	///
	/// @return  The position range
	///
	PosRange pos_range() const;
	
private:
	DimInfoPtr dim_info_;
	PosRange pos_range_;

	TWIST_CHECK_INVARIANT_DECL
};

}

#endif

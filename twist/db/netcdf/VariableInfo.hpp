/// @file VariableInfo.hpp
/// VariableInfo class definition and non-member function declarations

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_DB_NETCDF_VARIABLE_INFO_HPP
#define TWIST_DB_NETCDF_VARIABLE_INFO_HPP

#include "twist/db/netcdf/netcdf_globals.hpp"
#include "twist/db/netcdf/Attribute.hpp"

namespace twist::db::netcdf {
class DimensionInfo;
}

namespace twist::db::netcdf {

/*! Class which stored information about a netCDF variable.
    A netCDF variable has a name and associated data, which can be single or multi-dimensional.
    One or more dimensions are associated with a variable, and the order in which they are listed for the variable 
	  determines the way the variable data is indexed. In other words, the variable is defined along one or more 
	  dimensions, and its data is specified along those dimensions, taken in a specific order.
    One or more netCDF attributes can be associated with a variable.
 */
class VariableInfo {
public:
	using Attr = Attribute;

	using DimInfoPtr = std::shared_ptr<DimensionInfo>;

	/*! Constructor.
	    \param[in] name  The variable name
	    \param[in] data_type  The variable data type
	    \param[in] dims_info  A list of information objects describing the dimensions used by the variable; the 
	                          variable data is specified along those dimensions, in the order they are listed
	    \param[in] attributes  A list of attributes to be associated with the dimension
	 */
	VariableInfo(std::string name, 
	             DataType data_type, 
			     std::vector<DimInfoPtr> dims_info, 
				 std::vector<Attr> attributes);
	
	//! The variable name.
	[[nodiscard]] auto name() const -> std::string;
	
	//! The type of a variable data value.
	[[nodiscard]] auto data_type() const -> DataType;

	/*! Get the length of the variable data. This will always equal the product of the lengths of the dimensions along 
	    which the variable is defined.
	 */
	[[nodiscard]] auto data_length() const -> size_t;
	
	//! The number of dimensions along which the variable is defined. 
	[[nodiscard]] auto dim_count() const -> int;
	
	/*! Get a list of dimension information objects describing the dimensions along which the variable is defined. 
	    The order in which the dimensions are listed determines the way the variable data is indexed.
	 */
	[[nodiscard]] auto dims_info() const -> const std::vector<DimInfoPtr>&;

	//! The attributes associated with the variable.
	[[nodiscard]] auto attributes() const -> const std::vector<Attr>&;

	TWIST_NO_COPY_NO_MOVE(VariableInfo)

private:
	[[nodiscard]] static auto calc_data_length(const std::vector<DimInfoPtr>& dims_info) -> size_t;
	
	const std::string name_;
	const DataType data_type_;
	const std::vector<DimInfoPtr> dims_info_;
	const std::vector<Attr> attributes_;
	size_t data_length_;
	
	TWIST_CHECK_INVARIANT_DECL
};

// --- Non-member functions ---

/*! Get the names of the dimensions associated with a netCDF variable; the variable data is specified along those 
    dimensions, in the order their names appear in the returned list.
 */
[[nodiscard]] auto get_dim_names(const VariableInfo& var_info) -> std::vector<std::string>;

/*! Find out whether the list of dimensions associated with a netCDF variable contains a dimension with a specific 
    name.
    \param[in] var_info  Information about the variable
    \param[in] dim_name  The name of the dimension to search for; the matching is case-sensitive
    \return  true if a match is found
 */
[[nodiscard]] auto has_dim(const VariableInfo& var_info, std::string_view dim_name) -> bool;

/*! Find the index of a dimension with a specific name within the list of dimensions associated with a netCDF variable.
    \param[in] var_info  Information about the variable
    \param[in] dim_name  The name of the dimension to search for; the matching is case-sensitive
    \return  The index of the matching variable, or k_no_index if no match is found
 */
[[nodiscard]] auto get_dim_index(const VariableInfo& var_info, std::string_view dim_name) -> size_t;  

}

#endif

/// @file netcdf_utils.hpp
/// Miscelaneous utilities for the db::netcfd namespace

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_DB_NETCDF_UTILS_HPP
#define TWIST_DB_NETCDF_UTILS_HPP

#include "twist/db/netcdf/Attribute.hpp"
#include "twist/db/netcdf/DimensionValueRange.hpp"
#include "twist/db/netcdf/DimensionPosRange.hpp"
#include "twist/db/netcdf/DualDimensionValueRanges.hpp"
#include "twist/db/netcdf/NetcdfFile.hpp"

namespace twist::db::netcdf {

/*! Create a "dimension value range" object for a specific dimension defined in a netCDF file. 
    \tparam DimVal  The dimension variable's data value type
    \param[in] dim_name  The dimension name
    \param[in] lo_val  The lower bound of the dimension value range  
    \param[in] hi_val  The upper bound of the dimension value range  
    \param[in] file  The netCDF file
    \return  The "dimension value range" object
 */
template<class DimVal>
[[nodiscard]] auto make_dim_val_range(const std::string& dim_name, DimVal lo_val, DimVal hi_val,
			                          const NetcdfFile& file) -> DimensionValueRange<DimVal>;

/*! Create a "dual dimension value ranges" object for a specific "dual dimension" defined in a netCDF file. 
    \tparam Dim1Val  The value type of the data associated with the first dimension composing the "dual dimension", 
                     ie the data stored by the first dimension variable
    \tparam Dim2Val  The value type of the data associated with the second dimension composing the "dual dimension", 
                     ie the data stored by the second dimension variable
    \param[in] var1_name  The name of the first dimension variable used by the "dual dimension"
    \param[in] var2_name  The name of the second dimension variable used by the "dual dimension"
    \param[in] var1_val_range  Data value range for the first dimension variable used by the "dual dimension"
    \param[in] var2_val_range  Data velue range for the second dimension variable used by the "dual dimension"
    \param[in] file  The netCDF file
    \return  The "dual dimension value ranges" object
 */
template<class Dim1Val, class Dim2Val> 
[[nodiscard]] auto make_dual_dim_ranges(const std::string& var1_name, 
			                            const std::string& var2_name, 
										math::ClosedInterval<Dim1Val> var1_val_range, 
			                            math::ClosedInterval<Dim2Val> var2_val_range, 
										const NetcdfFile& file) -> DualDimensionValueRanges<Dim1Val, Dim2Val>; 

/*! Create a list of "dimension position ranges" based on a list of tuples of the form "dimension name", "first 
    position", "last position", using the netCDFfile \p file.
 */
[[nodiscard]] auto make_dim_pos_ranges(Tuples<std::string_view, Ssize, Ssize> positions_by_dim_name,
                                       const NetcdfFile& file) -> std::vector<DimensionPosRange>;

/*! Given the list of attributes \p attributes and the list of names \p names, returns all attributes whose name 
    matches one of the names in \p names. The matching is case-sensitive.
 */
template<rg::input_range Rng>
[[nodiscard]] auto find_by_name(const std::vector<Attribute>& attributes, const Rng& names) -> std::vector<Attribute>;

}

#include "twist/db/netcdf/netcdf_utils.ipp"

#endif

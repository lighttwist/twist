/// @file HyperslabAndDualDimDataReader.ipp
/// Inline implementation file for "HyperslabAndDualDimDataReader.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#define TWIST_CLASSTEMPL template<class VarVal, class Ddim1Val, class Ddim2Val, class... SdimVals>
#define TWIST_CLASSNAME HyperslabAndDualDimDataReader<VarVal, Ddim1Val, Ddim2Val, SdimVals...>
#define TWIST_CLASSTYPENAME typename TWIST_CLASSNAME

namespace twist::db::netcdf {

TWIST_CLASSTEMPL
TWIST_CLASSNAME::HyperslabAndDualDimDataReader(FilePtr file, const Params& params)
	: HyperslabReaderBase{file}
	, params_{params}
{
	TWIST_CHECK_INVARIANT
}

TWIST_CLASSTEMPL
auto TWIST_CLASSNAME::read_hyperslab_with_dims(std::string_view var_name, size_t* ddim_pos_border) const 
      -> HyslabDataPtr
{
	TWIST_CHECK_INVARIANT
	const auto [var_info, var_id] = get_file_var_info_id(var_name);

	// Make sure the variable is compatible with the hyperslab parameters
	verify_var(*var_info);

	// Read the data associated with the dimensions in the "dual dimension" from the file, and crop it for
	// the hyperslab
	auto [hyslab_ddim_data, ddim_pos_ranges] = calc_hyslab_dual_dim_data(ddim_pos_border);

	const auto ddim_dim1_pos_range = ddim_pos_ranges.dim1_pos_range();
	const auto ddim_dim2_pos_range = ddim_pos_ranges.dim2_pos_range();

	// Put together the following vectors, each containing info related to all dimensions (the two dimensions 
	// in the "dual dimension" and the "single dimensions"), in the order in which these dimensions appear in 
	// the hyperslab variable's list of dimensions
	//   * a vector of "dimension info" pointers
	//   * a vector of dimension position range starting positions
	//   * a vector of dimension position range lengths
	auto hyslab_dims_info = std::vector<DimInfoPtr>(all_dim_count);
	auto dim_start_positions = std::vector<size_t>(all_dim_count, netcdf_no_idx);
	auto dim_lengths = std::vector<size_t>(all_dim_count, netcdf_no_idx);
	
	const auto ddim_dim1_idx = get_dim_index(*var_info, params_.dual_dim_dim1_name());
	hyslab_dims_info[ddim_dim1_idx] = hyslab_ddim_data->dual_dim_info()->dim1_info();
	dim_start_positions[ddim_dim1_idx] = ddim_dim1_pos_range.lo();
	dim_lengths[ddim_dim1_idx] = static_cast<size_t>(count_elements(ddim_dim1_pos_range));
	
	const auto ddim_dim2_idx = get_dim_index(*var_info, params_.dual_dim_dim2_name());
	hyslab_dims_info[ddim_dim2_idx] = hyslab_ddim_data->dual_dim_info()->dim2_info();
	dim_start_positions[ddim_dim2_idx] = ddim_dim2_pos_range.lo();
	dim_lengths[ddim_dim2_idx] = static_cast<size_t>(count_elements(ddim_dim2_pos_range));

	SdimDataPtrTuple hyslabs_sdims_data;
	if constexpr (single_dim_count > 0) {
		// "Single dimensions" exist
		const auto& sdim_val_ranges = params_.single_dims_params().dim_val_ranges();
		
		// Read the "single dimensions" data; this also fills in the vectors of dimension start positions and
		// the vector of dimension lengths with the information related to the "single dimensions"
		hyslabs_sdims_data = tuple_transform_each(sdim_val_ranges, [&](const auto& dr) { 
			return make_hyslab_sdim_data(dr, *var_info, hyslab_dims_info, dim_start_positions, dim_lengths); 
		});
	}

	// Read the hyperslab variable data, truncated for the hyperslab, from the file
	auto hyslab_var_data = read_var_hyperslab_data<VarVal>(var_id, dim_start_positions, dim_lengths); 

	// Create the hyperslab "variable info"
	auto hyslab_var_info = make_hyslab_var_info(*var_info, hyslab_dims_info);			

	// Create the hyperslab data
	if constexpr (single_dim_count == 0) {
		// No "single dimensions"
		return std::make_shared<HyslabData>(hyslab_var_info, move(hyslab_var_data), move(hyslab_ddim_data));
	}
	else {
		// "Single dimensions" exist
		return tuple_apply(hyslabs_sdims_data, [&](const auto&... sdims_data) {
			return std::make_shared<HyslabData>(
					hyslab_var_info, move(hyslab_var_data), move(hyslab_ddim_data), sdims_data...);
		});			
	}
}

TWIST_CLASSTEMPL  
auto TWIST_CLASSNAME::calc_hyslab_dual_dim_data(size_t* ddim_pos_border) const 
                  -> std::tuple<DdimDataPtr<Ddim1Val, Ddim2Val>, DdimPosRanges>
{
	TWIST_CHECK_INVARIANT
	const auto dim1_name = params_.dual_dim_dim1_name();
	const auto dim2_name = params_.dual_dim_dim2_name();
	const auto var1_name = params_.dual_dim_var1_name();
	const auto var2_name = params_.dual_dim_var2_name();

	// Read the "dual dimension" data from the file, that is read the information about the two dimensions and 
	// the two variables making up the "dual dimension" and the full data of the two variables from the file
	auto file_dual_dim_data = file().read_dual_dim_data<Ddim1Val, Ddim2Val>(var1_name, var2_name);
	
	// Calculate the dimension position ranges for each of the two positions, corresponding to the dimension 
	// value ranges specified by the hyperslab parameters
	auto ddim_pos_ranges = get_dual_dim_pos_ranges(*file_dual_dim_data, 
	                                               params_.dual_dim_val_ranges(), 
												   ddim_pos_border); 
	if (!ddim_pos_ranges) {
		TWIST_THRO2(L"Invalid hyperslab parameters; "
				    L"no dimension values fall within the dual dimension value ranges.");
	}

	// Create a new "dual dimension" data object, containing the variable data cropped according to the 
	// dimension position ranges
	const auto hyslab_dim1_len = static_cast<size_t>(count_elements(ddim_pos_ranges->dim1_pos_range()));
	const auto hyslab_dim2_len = static_cast<size_t>(count_elements(ddim_pos_ranges->dim2_pos_range()));
	auto hyslab_dim1_info = make_hyslab_dim_info(*file().get_dim_info(dim1_name), hyslab_dim1_len);
	auto hyslab_dim2_info = make_hyslab_dim_info(*file().get_dim_info(dim2_name), hyslab_dim2_len);

	auto hyslab_var1_info = make_hyslab_var_info(
			*file().get_var_info(var1_name), {hyslab_dim1_info, hyslab_dim2_info});
	auto hyslab_var2_info = make_hyslab_var_info(
			*file().get_var_info(var2_name), {hyslab_dim1_info, hyslab_dim2_info});

	auto file_ddim_info = file().get_dual_dim_info(var1_name, var2_name);
	auto hyslab_ddim_info = make_hyslab_dual_dim_info(hyslab_dim1_info, hyslab_dim2_info, 
			var1_name, var2_name);

	auto hyslab_dim1_data = crop_data1<Ddim1Val, Ddim2Val>(*file_dual_dim_data, *ddim_pos_ranges);
	auto hyslab_dim2_data = crop_data2<Ddim1Val, Ddim2Val>(*file_dual_dim_data, *ddim_pos_ranges);

	auto hyslab_ddim_data = make_dual_dim_data(hyslab_ddim_info, hyslab_var1_info, hyslab_var2_info, 
			move(hyslab_dim1_data), move(hyslab_dim2_data));

	return {move(hyslab_ddim_data), *ddim_pos_ranges};
}

TWIST_CLASSTEMPL
void TWIST_CLASSNAME::verify_var(const VarInfo& var_info) const
{
	TWIST_CHECK_INVARIANT

	auto throw_excep = [&](const auto& msg) {
		const auto err_prefix = format_str(
				L"Variable \"%s\" is not compatible with the hyperslab parameters: ", 
				ansi_to_string(var_info.name()).c_str());
		TWIST_THROW((err_prefix + msg).c_str());
	};

	if (var_info.dims_info().size() != all_dim_count) {
		throw_excep(format_str(
				L"Variable has the wrong number (%d) of dimensions; expected %d dimensions.",
				var_info.dims_info().size(), all_dim_count));
	}

	const auto ddim_dim1_idx = get_dim_index(var_info, params_.dual_dim_dim1_name());
	const auto ddim_dim2_idx = get_dim_index(var_info, params_.dual_dim_dim2_name());

	if (ddim_dim1_idx == netcdf_no_idx || ddim_dim2_idx == netcdf_no_idx || 
			ddim_dim1_idx != ddim_dim2_idx - 1) {		
		throw_excep(L"The variable must reference both of the \"dual dimension\"'s dimensions, "
				"in the right order.");		
	}
}

#ifdef _DEBUG
TWIST_CLASSTEMPL 
void TWIST_CLASSNAME::check_invariant() const noexcept
{
}
#endif

}

#undef TWIST_CLASSTYPENAME
#undef TWIST_CLASSNAME
#undef TWIST_CLASSTEMPL

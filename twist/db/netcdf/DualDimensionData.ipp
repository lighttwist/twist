/// @file DualDimensionData.ipp
/// Inline implementation file for "DualDimensionData.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace twist::db::netcdf {

template<class Dim1Val, class Dim2Val>
DualDimensionData<Dim1Val, Dim2Val>::DualDimensionData(DdimInfoPtr ddim_info, 
		VarInfoPtr var1_info, VarInfoPtr var2_info, 
		std::vector<Dim1Val>&& data1, std::vector<Dim2Val>&& data2)
	: ddim_info_{ddim_info}
	, var1_info_{var1_info}
	, var2_info_{var2_info}
	, data1_{move(data1)}
	, data2_{move(data2)}
{
	TWIST_CHECK_INVARIANT
}


template<class Dim1Val, class Dim2Val>
typename DualDimensionData<Dim1Val, Dim2Val>::DdimInfoPtr 
DualDimensionData<Dim1Val, Dim2Val>::dual_dim_info() const
{
	TWIST_CHECK_INVARIANT
	return ddim_info_;
}


template<class Dim1Val, class Dim2Val>
typename DualDimensionData<Dim1Val, Dim2Val>::VarInfoPtr 
DualDimensionData<Dim1Val, Dim2Val>::var1_info() const
{
	TWIST_CHECK_INVARIANT
	return var1_info_;
}


template<class Dim1Val, class Dim2Val>
typename DualDimensionData<Dim1Val, Dim2Val>::VarInfoPtr 
DualDimensionData<Dim1Val, Dim2Val>::var2_info() const
{
	TWIST_CHECK_INVARIANT
	return var2_info_;
}


template<class Dim1Val, class Dim2Val>
const std::vector<Dim1Val>& DualDimensionData<Dim1Val, Dim2Val>::data1() const
{
	TWIST_CHECK_INVARIANT
	return data1_;
}


template<class Dim1Val, class Dim2Val>
const std::vector<Dim2Val>& DualDimensionData<Dim1Val, Dim2Val>::data2() const
{
	TWIST_CHECK_INVARIANT
	return data2_;
}


#ifdef _DEBUG
template<class Dim1Val, class Dim2Val>
void DualDimensionData<Dim1Val, Dim2Val>::check_invariant() const noexcept
{
	auto check_var_dims = [this](const auto& var_info) {
		assert(var_info.dims_info().size() == 2);
		if (var_info.dims_info().size() == 2) {
			const auto ddim_dim1_idx = get_dim_index(var_info, ddim_info_->dim1_info()->name());
			const auto ddim_dim2_idx = get_dim_index(var_info, ddim_info_->dim2_info()->name());
			// Each variable must reference both of the "dual dimension"'s dimensions, in the right order
			assert(ddim_dim1_idx == ddim_dim2_idx - 1);	
		}
	};

	check_var_dims(*var1_info_);
	check_var_dims(*var2_info_);

	assert(var1_info_->data_length() == data1_.size());
	assert(var2_info_->data_length() == data2_.size());
	assert(data1_.size() == ddim_info_->var_data_size());
	assert(data2_.size() == ddim_info_->var_data_size());
}
#endif

//
//  Local functions
//

namespace detail {

template<class DimVal> 
std::vector<DimVal> crop_data_impl(const std::vector<DimVal>& data, size_t dim2_len,	
		const DualDimensionPosRanges& ddim_pos_ranges)
{
	const auto cropped_dim1_len = static_cast<size_t>(count_elements(ddim_pos_ranges.dim1_pos_range()));
	const auto cropped_dim2_len = static_cast<size_t>(count_elements(ddim_pos_ranges.dim2_pos_range()));
	assert(cropped_dim2_len <= dim2_len);

	std::vector<DimVal> cropped_data(cropped_dim1_len * cropped_dim2_len);

	const auto dim1_lo_pos = ddim_pos_ranges.dim1_pos_range().lo();
	const auto dim2_lo_pos = ddim_pos_ranges.dim2_pos_range().lo();
	for (auto i = 0u; i < cropped_dim1_len; ++i) {
		auto src_row_begin = begin(data) + (dim1_lo_pos + i) * dim2_len + dim2_lo_pos;
		auto src_row_end = src_row_begin + cropped_dim2_len;
		auto dest_row_begin = begin(cropped_data) + i * cropped_dim2_len;
		std::copy(src_row_begin, src_row_end, dest_row_begin);
	}

	return cropped_data;
}

}

// --- Free functions ---

template<class Dim1Val, class Dim2Val> 
[[nodiscard]] auto get_dual_dim_pos_ranges(const DualDimensionData<Dim1Val, Dim2Val>& ddim_data,
							               const DualDimensionValueRanges<Dim1Val, Dim2Val>& ddim_val_ranges, 
							               size_t* border) -> std::optional<DualDimensionPosRanges>
{
	if (ddim_data.dual_dim_info() != ddim_val_ranges.dual_dim_info()) {
		TWIST_THROW(L"The dimension value ranges do not match the \"dual dimension\".");
	}

	const auto ddim_info = ddim_data.dual_dim_info();

	const auto dim1_len = ddim_info->dim1_info()->length();
	const auto dim2_len = ddim_info->dim2_info()->length();
	const auto dim1_val_range = ddim_val_ranges.var1_val_range();
	const auto dim2_val_range = ddim_val_ranges.var2_val_range();
	const auto& data1 = ddim_data.data1();
	const auto& data2 = ddim_data.data2();

	// Go through all "points" defined by the paired values of the two dimensions (the "point coordinates"), 
	//   and consider the position along each dimension for each point which fits the dimesnion ranges.
	// We are interested in the minimum and the maximum position of such points along each dimension.
	const auto noval = std::numeric_limits<size_t>::max();
	auto dim1_lo_pos = noval;
	auto dim1_hi_pos = noval;
	auto dim2_lo_pos = noval;
	auto dim2_hi_pos = noval;
	for (auto i = 0u; i < dim1_len; ++i) {
		for (auto j = 0u; j < dim2_len; ++j) {
			const auto dim1_val = data1[dim2_len * i + j];
			const auto dim2_val = data2[dim2_len * i + j];
			if (contains(dim1_val_range, dim1_val) && contains(dim2_val_range, dim2_val)) {
				if (dim1_lo_pos > i || dim1_lo_pos == noval) {
					dim1_lo_pos = i;
				}
				else if (dim1_hi_pos < i || dim1_hi_pos == noval) {
					dim1_hi_pos = i;
				}
				if (dim2_lo_pos > j || dim2_lo_pos == noval) {
					dim2_lo_pos = j;
				}
				else if (dim2_hi_pos < j || dim2_hi_pos == noval) {
					dim2_hi_pos = j;
				}
			}
		}
	}
	if (dim1_lo_pos == noval || dim2_lo_pos == noval) {
		// For one (or both) of the dimension value ranges, there are no dimension values at all which fall 
		// within that range
		return {};
	}
	if (border) {
		auto borders = std::set<size_t>{*border, 
		                                dim1_lo_pos, 
										dim1_len - 1 - dim1_hi_pos,
										dim2_lo_pos,
										dim2_len - 1 - dim2_hi_pos};
		*border = *borders.begin();
		if (*border > 0) {
			dim1_lo_pos -= *border;
			dim1_hi_pos += *border;
			dim2_lo_pos -= *border;
			dim2_hi_pos += *border;
		}
	}

	return DualDimensionPosRanges{ddim_info, {dim1_lo_pos, dim1_hi_pos}, {dim2_lo_pos, dim2_hi_pos}};
}

template<class Dim1Val, class Dim2Val> 
[[nodiscard]] auto crop_data1(const DualDimensionData<Dim1Val, Dim2Val>& ddim_data,	
		                      const DualDimensionPosRanges& ddim_pos_ranges) -> std::vector<Dim1Val>
{
	if (ddim_data.dual_dim_info() != ddim_pos_ranges.dual_dim_info()) {
		TWIST_THROW(L"The dimension position ranges do not match the \"dual dimension\".");
	}

	return detail::crop_data_impl(
			ddim_data.data1(), ddim_data.dual_dim_info()->dim2_info()->length(), ddim_pos_ranges);
}

template<class Dim1Val, class Dim2Val> 
[[nodiscard]] auto crop_data2(const DualDimensionData<Dim1Val, Dim2Val>& ddim_data,	
		                      const DualDimensionPosRanges& ddim_pos_ranges) -> std::vector<Dim2Val>
{
	if (ddim_data.dual_dim_info() != ddim_pos_ranges.dual_dim_info()) {
		TWIST_THROW(L"The dimension position ranges do not match the \"dual dimension\".");
	}

	return detail::crop_data_impl(
			ddim_data.data2(), ddim_data.dual_dim_info()->dim2_info()->length(), ddim_pos_ranges);
}

template<class Dim1Val, class Dim2Val, class Fn> 
auto for_each_grid_point(const DualDimensionData<Dim1Val, Dim2Val>& ddim_data, Fn func) -> void
{
	static_assert(std::is_invocable_v<Fn, Dim1Val, Dim2Val, int, int>, 
			"Type 'Fn' must be callable with parameter types (Dim1Val, Dim2Val, int, int).");

	const auto ddim_info = ddim_data.dual_dim_info();

	const auto dim1_len = static_cast<int>(ddim_info->dim1_info()->length());
	const auto dim2_len = static_cast<int>(ddim_info->dim2_info()->length());
	const auto& data1 = ddim_data.data1();
	const auto& data2 = ddim_data.data2();

	for (auto i = 0; i < dim1_len; ++i) {
		for (auto j = 0; j < dim2_len; ++j) {
			const auto data_idx = dim2_len * i + j;
			const auto dim1_val = data1[data_idx];
			const auto dim2_val = data2[data_idx];
			func(dim1_val, dim2_val, i, j);
		}
	}
}

}


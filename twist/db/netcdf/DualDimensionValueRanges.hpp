/// @file DualDimensionValueRanges.hpp
/// DualDimensionValueRanges class template definition

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_DB_NETCDF_DUAL_DIMENSION_VALUE_RANGES_HPP
#define TWIST_DB_NETCDF_DUAL_DIMENSION_VALUE_RANGES_HPP

#include "twist/db/netcdf/DimensionValueRange.hpp"

namespace twist::db::netcdf {

class DualDimensionInfo;

/*! This class defines two ranges of dimension values along the two dimensions making up a netCDF "dual dimension".  
    The first range is defined on the data values of the first dimension variable used by the "dual dimension" and 
    the second range on the values of the second variable.
    See class DualDimensionInfo for details about "dual dimensions" and class DimensionValueRange for details about 
    defining a range of dimension values along a netCDF dimension.
    \tparam Dim1Val  The value type of the data associated with the first dimension, ie the data stored by the first 
                     variable
    \tparam Dim2Val  The value type of the data associated with the second dimension, ie the data stored by the second 
                     variable
 */
template<class Dim1Val, class Dim2Val> 
class DualDimensionValueRanges {
public:
	using DdimInfoPtr = std::shared_ptr<DualDimensionInfo>;

	using Var1ValRange = twist::math::ClosedInterval<Dim1Val>;
	using Var2ValRange = twist::math::ClosedInterval<Dim2Val>;

	/// Constructor.
	///
	/// @param[in] ddim_info  Information about the "dual dimension"
	/// @param[in] var1_val_range  Data value range for the first dimension variable used by the "dual 
	///					dimension"
	/// @param[in] var2_val_range  Data velue range for the second dimension variable used by the "dual 
	///					dimension"
	///
	DualDimensionValueRanges(DdimInfoPtr ddim_info, Var1ValRange var1_val_range, Var2ValRange var2_val_range);

	/// Get information about the "dual dimension".
	///
	/// @return  The "dual dimension" information
	///
	DdimInfoPtr dual_dim_info() const;
	
	/// Get the data value range for the first dimension variable used by the "dual dimension".
	///
	/// @return  The first dimension variable's value range
	///
	Var1ValRange var1_val_range() const;
	
	/// Get the data value range for the second dimension variable used by the "dual dimension".
	///
	/// @return  The second dimension variable's value range
	///
	Var2ValRange var2_val_range() const;

	TWIST_DEF_COPY_NO_MOVE(DualDimensionValueRanges)
	
private:
	DdimInfoPtr ddim_info_;
	Var1ValRange var1_val_range_;
	Var2ValRange var2_val_range_;

	TWIST_CHECK_INVARIANT_DECL
};

}

#include "twist/db/netcdf/DualDimensionValueRanges.ipp"

#endif

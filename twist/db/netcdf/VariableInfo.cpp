/// @file VariableInfo.cpp
/// Implementation file for "VariableInfo.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/db/netcdf/VariableInfo.hpp"

#include "twist/std_vector_utils.hpp"
#include "twist/db/netcdf/DimensionInfo.hpp"

namespace twist::db::netcdf {

// --- VariableInfo class ---

VariableInfo::VariableInfo(std::string name, DataType data_type, std::vector<DimInfoPtr> dims_info, 
                           std::vector<Attr> attributes)
	: name_{move(name)}
	, data_type_{data_type}
	, dims_info_{move(dims_info)}
	, attributes_{move(attributes)}
	, data_length_{calc_data_length(dims_info_)}
{
	TWIST_CHECK_INVARIANT
}

auto VariableInfo::name() const -> std::string
{
	TWIST_CHECK_INVARIANT
	return name_;
}

auto VariableInfo::data_type() const -> DataType
{
	TWIST_CHECK_INVARIANT
	return data_type_;
}

auto VariableInfo::data_length() const -> size_t
{
	TWIST_CHECK_INVARIANT
	return data_length_;
}


int VariableInfo::dim_count() const
{
	TWIST_CHECK_INVARIANT
	return size32(dims_info_);
}

auto VariableInfo::dims_info() const -> const std::vector<DimInfoPtr>&
{
	TWIST_CHECK_INVARIANT
	return dims_info_;
}

auto VariableInfo::attributes() const -> const std::vector<Attr>&
{
	TWIST_CHECK_INVARIANT
	return attributes_;
}

auto VariableInfo::calc_data_length(const std::vector<DimInfoPtr>& dims_info) -> size_t
{
	if (dims_info.empty()) return 0;

	return accumulate(dims_info, std::size_t{1}, 
			[&](auto len, const auto& dim_info) { return len * dim_info->length(); });
}
	
#ifdef _DEBUG
void VariableInfo::check_invariant() const noexcept
{
	assert(!is_whitespace(name_));
	if (has_if(dims_info_, [this](const auto& d){ return d->name() == name_; } )) {
		// One of the associated dimensions has the same name as this variable; this means this is a 
		// "dimension variable" and so only that dimension should be associated with it
		assert(dims_info_.size() == 1);
	}
	assert(dims_info_.empty() == (data_length_ == 0));
}
#endif

// --- Non-member functions ---

auto get_dim_names(const VariableInfo& var_info) -> std::vector<std::string>
{
	return var_info.dims_info() | vw::transform([](const auto& dim_info) { return dim_info->name(); }) 
	                            | rg::to<std::vector>();
}

auto has_dim(const VariableInfo& var_info, std::string_view dim_name) -> bool
{
	return has_if(var_info.dims_info(), [dim_name](const auto& dim_info) { return dim_info->name() == dim_name; });
}

auto get_dim_index(const VariableInfo& var_info, std::string_view dim_name)-> size_t
{
	return position_of_if(var_info.dims_info(), 
	                      [dim_name](const auto& dim_info) { return dim_info->name() == dim_name; });
}

}

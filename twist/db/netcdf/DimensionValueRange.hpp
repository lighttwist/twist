/// @file DimensionValueRange.hpp
/// DimensionValueRange class template definition

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_DB_NETCDF_DIMENSION_VALUE_RANGE_HPP
#define TWIST_DB_NETCDF_DIMENSION_VALUE_RANGE_HPP

#include "twist/math/ClosedInterval.hpp"

namespace twist::db::netcdf {
class DimensionInfo;
}

namespace twist::db::netcdf {

/*! This class defines a range of dimension values along a netCDF dimension (ie along the data of a dimension 
    variable). 
    One variable - for "single dimensions" - or two variables - for "dual dimensions" - provide the data values along a 
	dimension's ruler; we refer here to such variables as dimension variables. (See class DualDimensionInfo for details 
	about "dual dimensions".)
    If the values in a dimension variable data fall in the closed interval [min_val, max_val], this class defines a 
	closed interval [lo_val, hi_val] included in [min_val, max_val]. The interval bounds are of the same numeric type 
	as the dimension variable's data value type. 
    Note that in the case of "dual dimensions", dimension variable data may not be uniformly spaced, nor monotonic, 
	and so in those cases there isn't a straightforward relation between the dimension value range and dimension 
    position range.   
    \tparam DimVal  The dimension variable's data value type
 */   
template<class DimVal> 
class DimensionValueRange {
public:
	using DimInfoPtr = std::shared_ptr<DimensionInfo>;
	using ValRange = twist::math::ClosedInterval<DimVal>;

	/*! Constructor.	   
	    \param[in] dim_info  The dimension information
	    \param[in] lo_val  The lower bound of the dimension value range  
	    \param[in] hi_val  The upper bound of the dimension value range  
	 */  
	DimensionValueRange(DimInfoPtr dim_info, DimVal lo_val, DimVal hi_val);

	/*! Constructor.	   
	    \param[in] dim_info  The dimension information
	    \param[in] val_range  The dimension value range  
	 */  	   
	DimensionValueRange(DimInfoPtr dim_info, ValRange val_range);

	DimensionValueRange(const DimensionValueRange&) = default;

	//! The dimension information.
	DimInfoPtr dim_info() const;

	//! The dimension value range.	   	   
	ValRange val_range() const;

	TWIST_NO_COPY_ASSIGN_NO_MOVE(DimensionValueRange<DimVal>)

private:
	DimInfoPtr dim_info_;
	ValRange val_range_;

	TWIST_CHECK_INVARIANT_DECL
};

}

#include "twist/db/netcdf/DimensionValueRange.ipp"

#endif

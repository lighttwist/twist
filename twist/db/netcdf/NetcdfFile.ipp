/// @file NetcdfFile.ipp
/// Inline implementation file for "NetcdfFile.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace twist::db::netcdf {

template<class... Args> 
NetcdfFile::DimInfoPtr NetcdfFile::make_dim_info(Args&&... args) 
{
	return DimInfoPtr{ new DimInfo{ std::forward<Args>(args)... } };
}

template<class... Args> 
NetcdfFile::DdimInfoPtr NetcdfFile::make_dual_dim_info(Args&&... args)
{
	return DdimInfoPtr{ new DdimInfo{ std::forward<Args>(args)... } };
}

template<class DimVal, class... Args> 
NetcdfFile::DimDataPtr<DimVal> NetcdfFile::make_dim_data(Args&&... args)
{
	return DimDataPtr<DimVal>{ new DimData<DimVal>{ std::forward<Args>(args)... } };
}

template<class Dim1Val, class Dim2Val, class... Args> 
NetcdfFile::DdimDataPtr<Dim1Val, Dim2Val> NetcdfFile::make_dual_dim_data(Args&&... args)
{
	return DdimDataPtr<Dim1Val, Dim2Val>{ new DdimData<Dim1Val, Dim2Val>{ std::forward<Args>(args)... } };	
}

template<class VarVal>
void NetcdfFile::read_var_hyperslab_data(int var_id, const std::vector<size_t>& dim_start_positions, 
		const std::vector<size_t>& dim_lengths, std::vector<VarVal>& buffer) const
{
	int err_no = 0;
	if constexpr (std::is_same_v<VarVal, float>) {
		err_no = read_var_float32_hyperslab_data(
				var_id, dim_start_positions.data(), dim_lengths.data(), buffer.data());
	}
	else if constexpr (std::is_same_v<VarVal, double>) {
		err_no = read_var_float64_hyperslab_data(
				var_id, dim_start_positions.data(), dim_lengths.data(), buffer.data());
	}
	else if constexpr (std::is_same_v<VarVal, int>) {
		err_no = read_var_int32_hyperslab_data(
				var_id, dim_start_positions.data(), dim_lengths.data(), buffer.data());
	}
	else {
		TWIST_THROW(L"This method is not implemented for variable data of type \"%s\".", 
				type_wname<VarVal>().c_str());
	}

	if (err_no) TWIST_THROW(get_native_err_msg(err_no).c_str());
}

template<class DimVal> 
NetcdfFile::DimDataPtr<DimVal> NetcdfFile::read_dim_data(std::string_view dim_name) const
{
	TWIST_CHECK_INVARIANT
	const auto it = dims_info_.find(dim_name);
	if (it == end(dims_info_)) {
		TWIST_THROW(L"Dimension named \"%s\" not found in file \"%s\".", 
				ansi_to_string(dim_name).c_str(), path_.filename().c_str());
	}
	auto dim_info = std::get<tup_dim>(it->second);

	auto var_info = get_var_for_dim(dim_name);
	auto var_data = read_var_data<DimVal>(var_info->name());

	return DimDataPtr<DimVal>{ new DimData<DimVal>{ dim_info, var_info, move(var_data) } };
}

template<class DimVal> 
auto NetcdfFile::write_dim_data(DimDataPtr<DimVal> dim_data) -> void
{
	TWIST_CHECK_INVARIANT
	write_var_data(dim_data->var_info()->name(), dim_data->data());
}

template<class VarVal> 
auto NetcdfFile::read_var_data(std::string_view var_name) const -> std::vector<VarVal>
{
	TWIST_CHECK_INVARIANT
	const auto it = vars_info_.find(var_name);
	if (it == end(vars_info_)) {
		TWIST_THRO2(L"Variable named \"{}\" not found in file \"{}\".", 
				    ansi_to_string(var_name), path_.filename().c_str());
	}
	const auto& var_info = *std::get<tup_var>(it->second);
	const auto var_id = std::get<tup_id>(it->second);

	// Make sure the template type matches the variable data type 
	check_data_value_type<VarVal>(var_info.data_type());

	std::vector<VarVal> data(var_info.data_length());

	int err_no = 0;
	if constexpr (std::is_same_v<VarVal, float>) {
		err_no = read_var_float32_data(var_id, data.data());
	}
	else if constexpr (std::is_same_v<VarVal, double>) {
		err_no = read_var_float64_data(var_id, data.data());
	}
	else if constexpr (std::is_same_v<VarVal, int>) {
		err_no = read_var_int32_data(var_id, data.data());
	}
	else {
		TWIST_THROW(L"This method is not implemented for variable data of type \"%s\".", 
				type_wname<VarVal>().c_str());
	}
	if (err_no) {
		TWIST_THROW(get_native_err_msg(err_no).c_str());
	}

	return data;
}

template<class VarVal> 
auto NetcdfFile::read_var_hyperslab_data(std::string_view var_name, 
			                             const std::vector<DimensionPosRange>& dim_pos_ranges) const
      -> std::vector<VarVal>
{
	TWIST_CHECK_INVARIANT
	const auto it = vars_info_.find(var_name);
	if (it == end(vars_info_)) {
		TWIST_THRO2(L"Variable named \"{}\" not found in file \"{}\".", 
				    ansi_to_string(var_name), path_.filename().c_str());
	}
	const auto& var_info = *std::get<tup_var>(it->second);
	const auto var_id = std::get<tup_id>(it->second);

	const auto dim_count = var_info.dim_count();
	if (dim_pos_ranges.size() != dim_count) {
		TWIST_THRO2(L"Wrong number ({}) of dimension position ranges supplied; variable \"{}\" is defined "
				    L"along {} dimensions.", dim_pos_ranges.size(), ansi_to_string(var_name), dim_count);
	}

	auto dim_start_positions = std::vector<size_t>(dim_count);
	auto dim_lengths = std::vector<size_t>(dim_count);
	for (auto i : IndexRange{dim_count}) {
		if (dim_pos_ranges[i].dim_info()->name() != var_info.dims_info()[i]->name()) {
			TWIST_THRO2(L"The supplied dimension position ranges must follow the order in which the "
					    L"dimensions are defined for variable \"{}\".", ansi_to_string(var_name));
		}
		const auto pos_range = dim_pos_ranges[i].pos_range();
		dim_start_positions[i] = pos_range.lo();
		dim_lengths[i] = count_elements(pos_range);
	}

	const auto hyslab_data_length = accumulate(dim_lengths, size_t{1}, std::multiplies<size_t>{});
	auto var_data = std::vector<VarVal>(hyslab_data_length);
	read_var_hyperslab_data(var_id, dim_start_positions, dim_lengths, var_data);

	return var_data;
}

template<class VarVal> 
void NetcdfFile::write_var_data(std::string_view var_name, const std::vector<VarVal>& data)
{
	TWIST_CHECK_INVARIANT
	const auto it = vars_info_.find(var_name);
	if (it == end(vars_info_)) {
		TWIST_THROW(L"Variable named \"%s\" not found in file \"%s\".", 
				ansi_to_string(var_name).c_str(), path_.filename().c_str());
	}
	const auto& var_info = *std::get<tup_var>(it->second);
	const auto var_id = std::get<tup_id>(it->second);

	// Make sure the template type matches the variable data type 
	check_data_value_type<VarVal>(var_info.data_type());

	// Make sure that the data has the right length
	if (data.size() != var_info.data_length()) {
		TWIST_THROW(L"The data to be written for variable \"%s\" has the wrong length %d; expected %d.", 
				ansi_to_string(var_name).c_str(), data.size(), var_info.data_length());
	}

	int err_no = 0;
	if constexpr (std::is_same_v<VarVal, float>) {
		err_no = write_var_float32_data(var_id, data.data());
	}
	else if constexpr (std::is_same_v<VarVal, double>) {
		err_no = write_var_float64_data(var_id, data.data());
	}
	else if constexpr (std::is_same_v<VarVal, int>) {
		err_no = write_var_int32_data(var_id, data.data());
	}
	else {
		TWIST_THROW(L"This method is not implemented for variable data of type \"%s\".", 
				type_wname<VarVal>().c_str());
	}

	if (err_no) {
		TWIST_THROW(get_native_err_msg(err_no).c_str());
	}
}

template<class Dim1Val, class Dim2Val> 
NetcdfFile::DdimDataPtr<Dim1Val, Dim2Val> NetcdfFile::read_dual_dim_data(std::string_view var1_name, 
		std::string_view var2_name) const
{
	TWIST_CHECK_INVARIANT
	auto ddim_info = get_dual_dim_info(var1_name, var2_name);
	auto var1_info = get_var_info(var1_name);
	auto var2_info = get_var_info(var2_name);

	auto data1 = read_var_data<Dim1Val>(var1_name);
	auto data2 = read_var_data<Dim2Val>(var2_name);

	return DdimDataPtr<Dim1Val, Dim2Val>{ new DdimData<Dim1Val, Dim2Val>{
			ddim_info, var1_info, var2_info, move(data1), move(data2) } };	
}

template<class VarVal, class... DimVals> 
void NetcdfFile::write_hyperslab_with_dims(HyperslabAndDimDataPtr<VarVal, DimVals...> hyslab_data) 
{
	TWIST_CHECK_INVARIANT
	// 1. Write the dimensions data
	tuple_apply_to_each(hyslab_data->dims_data(), [this](const auto& dim_data) {
		write_dim_data(dim_data);		
	});

	// 2. Write the variable data
	write_var_data(hyslab_data->var_info()->name(), hyslab_data->var_data());
}

template<class VarVal, class Ddim1Val, class Ddim2Val, class... SdimVals> 
void NetcdfFile::write_hyperslab_with_dual_dims(
		HyperslabWithDdimsDataPtr<VarVal, Ddim1Val, Ddim2Val, SdimVals...> hyslab_data)
{
	TWIST_CHECK_INVARIANT
	// 1. Write the dimensions data for the "single dimensions"
	tuple_apply_to_each(hyslab_data->single_dims_data(), [this](const auto& dim_data) {
		write_dim_data(dim_data);		
	});
		
	// 2. Write the variable data for the variables which are part of the "dual dimension"
	const auto& ddim_data = *hyslab_data->dual_dim_data();
	write_var_data(ddim_data.var1_info()->name(), ddim_data.data1());
	write_var_data(ddim_data.var2_info()->name(), ddim_data.data2());

	// 3. Write the hyperslab variable data
	write_var_data(hyslab_data->var_info()->name(), hyslab_data->var_data());
}

}

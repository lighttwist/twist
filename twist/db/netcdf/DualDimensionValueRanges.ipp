/// @file DualDimensionValueRanges.ipp
/// Inline implementation file for "DualDimensionValueRanges.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#define TWIST_CLASSTEMPL  template<class Dim1Val, class Dim2Val>

#define TWIST_CLASSNAME  DualDimensionValueRanges<Dim1Val, Dim2Val>

#define TWIST_CLASSTYPENAME  typename TWIST_CLASSNAME

namespace twist::db::netcdf {

TWIST_CLASSTEMPL 
TWIST_CLASSNAME::DualDimensionValueRanges(DdimInfoPtr ddim_info, Var1ValRange var1_val_range, 
		Var2ValRange var2_val_range)
	: ddim_info_{ddim_info}
	, var1_val_range_{var1_val_range}
	, var2_val_range_{var2_val_range}
{
	TWIST_CHECK_INVARIANT
}


TWIST_CLASSTEMPL 
TWIST_CLASSTYPENAME::DdimInfoPtr TWIST_CLASSNAME::dual_dim_info() const
{
	TWIST_CHECK_INVARIANT
	return ddim_info_;
}
	

TWIST_CLASSTEMPL 
TWIST_CLASSTYPENAME::Var1ValRange TWIST_CLASSNAME::var1_val_range() const
{
	TWIST_CHECK_INVARIANT
	return var1_val_range_;
}
	

TWIST_CLASSTEMPL 
TWIST_CLASSTYPENAME::Var2ValRange TWIST_CLASSNAME::var2_val_range() const
{
	TWIST_CHECK_INVARIANT
	return var2_val_range_;
}


#ifdef _DEBUG
TWIST_CLASSTEMPL 
void TWIST_CLASSNAME::check_invariant() const noexcept
{
	assert(ddim_info_);
}
#endif

}

#undef  TWIST_CLASSTYPENAME
#undef  TWIST_CLASSNAME
#undef  TWIST_CLASSTEMPL

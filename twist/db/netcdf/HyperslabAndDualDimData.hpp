/// @file HyperslabAndDualDimData.hpp
/// HyperslabAndDualDimData class template definition

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_DB_NETCDF_HYPERSLAB_AND_DUAL_DIM_DATA_HPP
#define TWIST_DB_NETCDF_HYPERSLAB_AND_DUAL_DIM_DATA_HPP

#include "twist/metaprogramming.hpp"

#include "twist/db/netcdf/DimensionData.hpp"
#include "twist/db/netcdf/HyperslabDataBase.hpp"

namespace twist::db::netcdf {

/*! Class which stored information about a "hyperslab", which uses a "dual dimension" and any number of "single 
    dimensions", and the hyperslab data together with the data for each dimension used. 
    A hyperslab is a multidimensional chunk of data, representing a specific variable's data, which (in this class) 
	uses a "dual dimension" (see DualDimensionInfo class for details about "dual dimensions") and optionally a number 
	of "single dimensions". We refer here to the variable as the "hyperslab variable".    
    \tparam VarVal  The value type of the "hyperslab variable" data 
    \tparam Ddim1Val  The data value type of the first "single dimension" in the "dual dimension" (ie the type of the 
	                  data stored by first variable corresponding to the "dual dimension")
    \tparam Ddim1Val  The data value type of the second "single dimension" in the "dual dimension" (ie the type of the 
	                  data stored by second variable corresponding to the "dual dimension")
    \tparam SdimVals...  The value type of each of the "single dimension"'s data, ie the data types of the variables 
	                     associated with each of the "single dimensions" used by the hyperslab (outside the "dual 
						 dimension"); the ordering of the templates making up this variadic parameter, together with 
						 the constructor arguments, establish the order of the "single dimensions" for this class
 */
template<class VarVal, class Ddim1Val, class Ddim2Val, class... SdimVals>
class HyperslabAndDualDimData : public HyperslabDataBase<VarVal> {
public:
	using VarInfoPtr = std::shared_ptr<VariableInfo>;
	using DimInfoPtr = std::shared_ptr<DimensionInfo>;

	template<class DimVal> using SdimDataPtr = std::shared_ptr<DimensionData<DimVal>>;

	using DdimDataPtr = std::shared_ptr<DualDimensionData<Ddim1Val, Ddim2Val>>;

	using SdimDataPtrTuple = std::tuple<SdimDataPtr<SdimVals>...>;

	/// The type of a specific "single dimension data" object stored by this class.
	/// @tparam  SdimIdx  The "single dimension" index, in the ordering established by the class variadic 
	///				template parameter
	template<size_t SdimIdx> 
	using SdimDataPtrType = std::tuple_element_t<SdimIdx, SdimDataPtrTuple>;

	/// The number of "single dimensions" (outside of the "dual dimension") used by the hyperslab; if zero 
	/// then the hyperslab only uses the "dual dimension"
	static constexpr size_t single_dim_count = sizeof...(SdimVals);

	/// The total number of dimensions used by the hyperslab (the two dimensions used by the "dual dimension"
	/// and the "single dimensions")
	static constexpr size_t all_dim_count = single_dim_count + 2;

	/// Constructor.
	///
	/// @param[in] var_info  The "hyperlab variable" info
	/// @param[in] var_data  The "hyperlab variable" data
	/// @param[in] dual_dim_data  The "dual dimension" data
	/// @param[in] single_dims_data  A set of "dimension data" objects, one for each "single dimension" 
	///					(outside of the "dual dimension") used by the hyperslab
	///
	HyperslabAndDualDimData(VarInfoPtr var_info, 
	                        std::vector<VarVal>&& var_data, 
			                DdimDataPtr dual_dim_data, 
							SdimDataPtr<SdimVals>... single_dims_data)
		: HyperslabDataBase<VarVal>{var_info, move(var_data)}  
		, dual_dim_data_{dual_dim_data}
		, single_dims_data_{single_dims_data...}
	{	// Visual Studio 2017 from version 15.9.3 onwards seems to dislike the separate definition of 
		// method templates with variadic arguments
		TWIST_CHECK_INVARIANT
	}

	//! The data associated with the "dual dimension" used by the hyperslab.
	DdimDataPtr dual_dim_data() const;

	/// Get a list of dimension info objects, one for each of the "single dimensions" used by the hyperslab 
	/// (outside of the "dual dimension").
	///
	/// @return  List of dimension information objects; the list follows the "single dimension" ordering 
	///					established by the class variadic template parameter
	///
	std::vector<DimInfoPtr> single_dims_info() const;

	/// Get a list of dimension info objects, describing all dimensions used by the hyperslab, ie the two 
	/// dimensions used by the "dual dimension" and the "single dimensions" (outside of the "dual dimension").
	///
	/// @return  List of dimension information objects; the first dimension of the "dual dimension" appears 
	///					first, then the second, after which the list follows the "single dimension" ordering 
	///					established by the class variadic template parameter
	///
	std::vector<DimInfoPtr> all_dims_info() const;  

	/// Get the dimension data for one of the "single dimensions" (outside of the "dual dimension") used by 
	/// the hyperslab.
	/// @tparam  SdimIdx  The index of the "single dimension", in the "single dimension" ordering established 
	///					by the constructor
	/// @return  The dimension data
	///
	template<size_t SdimIdx> 
	SdimDataPtrType<SdimIdx> single_dim_data() const;

	/// Get a reference to a tuple containing dimension data objects, one for each of the "single dimensions" 
	/// used (outside the "dual dimension") by the hyperslab. The dimension data objects appear in the tuple 
	/// in the "single dimension" ordering established by the constructor.
	/// @return  Reference to the dimension data tuple
	///
	const SdimDataPtrTuple& single_dims_data() const;

	/// Get variable information for all the variables associated with the hyperslab, that is the "hyperslab 
	/// variable", which appears first in the returned list, followed by the first veriable associated with 
	/// the "dual dimension", then the second, and then the variables associated to each "single dimension" 
	/// used (outside the "dual dimension") by the hyperslab (which appear in the "single dimension" order 
	/// established by the class variadic template parameter).
	/// @return  The variables information
	///
	std::vector<VarInfoPtr> all_vars_info() const;  

	TWIST_NO_COPY_NO_MOVE(HyperslabAndDualDimData)

private:
	DdimDataPtr dual_dim_data_; 
	SdimDataPtrTuple single_dims_data_;

	TWIST_CHECK_INVARIANT_DECL
};

}

#include "HyperslabAndDualDimData.ipp"

#endif


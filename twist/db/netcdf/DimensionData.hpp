/// @file DimensionData.hpp
/// DimensionData class definition

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_DB_NETCDF_DIMENSION_DATA_HPP
#define TWIST_DB_NETCDF_DIMENSION_DATA_HPP

#include "twist/db/netcdf/netcdf_globals.hpp"
#include "twist/db/netcdf/DimensionInfo.hpp"
#include "twist/db/netcdf/VariableInfo.hpp"

namespace twist::db::netcdf {

///
///  Class which stores information about a netCDF dimension (a regular, "single dimension", as opposed to a 
///    a "dual dimension") together with the data associated with its measurement ruler.
///  A dimension has a corresponding variable, stored in the same netCDF file, whose data size is equal to the 
///    dimension's length; it typically has the same name as the dimension, and uses this dimension, and no 
///    other. When we refer to the dimension data, we refer to that variable's data.
///
///  @tparam  DimVal  The value type of the dimension data (ie the data stored by variable corresponding to 
///			the dimension)
///
template<typename DimVal>
class DimensionData {
public:
	using DimInfoPtr = std::shared_ptr<DimensionInfo>;
	using VarInfoPtr = std::shared_ptr<VariableInfo>;	
	
	//! The dimension info.
	DimInfoPtr dim_info() const;

	//! The variable info for the variable corresponding to the dimension.
	VarInfoPtr var_info() const;

	//! Read-only access to the dimension data (ie the data stored by variable corresponding to the dimension).
	[[nodiscard]] auto data() const -> const std::vector<DimVal>&;

	TWIST_NO_COPY_DEF_MOVE(DimensionData)

private:
	friend class NetcdfFile;
	
	DimensionData(DimInfoPtr dim_info, VarInfoPtr var_info, std::vector<DimVal>&& data);

	DimInfoPtr dim_info_;
	VarInfoPtr var_info_; 
	std::vector<DimVal> data_;
	
	TWIST_CHECK_INVARIANT_DECL
};

}

#include "twist/db/netcdf/DimensionData.ipp"

#endif

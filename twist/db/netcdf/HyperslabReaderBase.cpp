/// @file HyperslabReaderBase.cpp
/// Implementation file for "HyperslabReaderBase.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/db/netcdf/HyperslabReaderBase.hpp"

#include "twist/db/netcdf/NetcdfFile.hpp"

namespace twist::db::netcdf {

HyperslabReaderBase::HyperslabReaderBase(std::shared_ptr<NetcdfFile> file)
	: file_{file}
{
	TWIST_CHECK_INVARIANT
}


const NetcdfFile& HyperslabReaderBase::file() const
{
	TWIST_CHECK_INVARIANT
	return *file_;
}


HyperslabReaderBase::DimInfoPtr HyperslabReaderBase::make_hyslab_dim_info(
		const DimInfo& file_dim_info, size_t hyslab_dim_length)
{
	assert(hyslab_dim_length <= file_dim_info.length());
	return NetcdfFile::make_dim_info(file_dim_info.name(), hyslab_dim_length);
}


HyperslabReaderBase::VarInfoPtr HyperslabReaderBase::make_hyslab_var_info(
		const VarInfo& file_var_info, const std::vector<DimInfoPtr>& hyslab_dims_info)
{
	// The hyperslab version of the variable should have the same number of dimmensions as the original one in 
	// the file, with the same names and smaller or equal lenghts  
	assert(hyslab_dims_info.size() == file_var_info.dims_info().size());
	if (hyslab_dims_info.size() == file_var_info.dims_info().size()) {
		assert(twist::equal(hyslab_dims_info, file_var_info.dims_info(), [](auto di1, auto di2) {
			return di1->name() == di2->name();
		}));
		assert(twist::equal(hyslab_dims_info, file_var_info.dims_info(), [](auto di1, auto di2) {
			return di1->length() <= di2->length();
		}));
	}

	return std::make_shared<VariableInfo>(
			file_var_info.name(), file_var_info.data_type(), hyslab_dims_info, file_var_info.attributes());			
}


HyperslabReaderBase::DdimInfoPtr HyperslabReaderBase::make_hyslab_dual_dim_info(
		DimInfoPtr hyslab_dim1_info, DimInfoPtr hyslab_dim2_info, 
		std::string_view var1_name, std::string_view var2_name) 
{
	return NetcdfFile::make_dual_dim_info(
			hyslab_dim1_info, hyslab_dim2_info, var1_name, var2_name);
}


int HyperslabReaderBase::get_file_dim_id(std::string_view dim_name) const
{
	TWIST_CHECK_INVARIANT
	auto it = file_->dims_info_.find(dim_name);
	if (it == end(file_->dims_info_)) {
		TWIST_THROW(L"No dimension named \"%s\" found in the NetCDF file.", to_string(dim_name).c_str());	
	}
	return std::get<NetcdfFile::tup_id>(it->second);
}


std::tuple<HyperslabReaderBase::VarInfoPtr, int> 
HyperslabReaderBase::get_file_var_info_id(std::string_view var_name) const
{
	TWIST_CHECK_INVARIANT
	auto it = file_->vars_info_.find(var_name);
	if (it == end(file_->vars_info_)) {
		TWIST_THROW(L"No variable named \"%s\" found in the NetCDF file.", to_string(var_name).c_str());
	}
	return it->second;
}


#ifdef _DEBUG
void HyperslabReaderBase::check_invariant() const noexcept
{
	assert(file_);
}
#endif

}

/// @file DimensionData.ipp
/// Inline implementation file for "DimensionData.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace twist::db::netcdf {

template<typename DimVal>
DimensionData<DimVal>::DimensionData(DimInfoPtr dim_info, VarInfoPtr var_info, std::vector<DimVal>&& data)
	: dim_info_{dim_info}
	, var_info_{var_info}
	, data_{move(data)}
{
	TWIST_CHECK_INVARIANT
}

template<typename DimVal>
typename DimensionData<DimVal>::DimInfoPtr DimensionData<DimVal>::dim_info() const
{
	TWIST_CHECK_INVARIANT
	return dim_info_;
}

template<typename DimVal>
typename DimensionData<DimVal>::VarInfoPtr DimensionData<DimVal>::var_info() const
{
	TWIST_CHECK_INVARIANT
	return var_info_;
}

template<typename DimVal>
auto DimensionData<DimVal>::data() const -> const std::vector<DimVal>&
{
	TWIST_CHECK_INVARIANT
	return data_;
}

#ifdef _DEBUG
template<typename DimVal>
void DimensionData<DimVal>::check_invariant() const noexcept
{
	assert(var_info_->dims_info().size() == 1);
	if (var_info_->dims_info().size() == 1) {
		assert(var_info_->dims_info().front() == dim_info_);
	}
	assert(dim_info_->length() == data_.size());
}
#endif

}

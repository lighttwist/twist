/// @file netcdf_utils.ipp
/// Inline implementation file for "netcdf_utils.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace twist::db::netcdf {

template<class DimVal>
[[nodiscard]] auto make_dim_val_range(const std::string& dim_name, 
                                      DimVal lo_val, 
									  DimVal hi_val,
		                              const NetcdfFile& file) -> DimensionValueRange<DimVal>
{
	return DimensionValueRange<DimVal>{file.get_dim_info(dim_name), lo_val, hi_val};
}

template<class Dim1Val, class Dim2Val> 
[[nodiscard]] auto make_dual_dim_ranges(const std::string& var1_name, 
                                        const std::string& var2_name, 
                                        twist::math::ClosedInterval<Dim1Val> var1_val_range, 
		                                twist::math::ClosedInterval<Dim2Val> var2_val_range, 
										const NetcdfFile& file) -> DualDimensionValueRanges<Dim1Val, Dim2Val> 
{
	return DualDimensionValueRanges<Dim1Val, Dim2Val>{file.get_dual_dim_info(var1_name, var2_name), 
	                                                  var1_val_range, 
													  var2_val_range};
}

template<rg::input_range Rng>
[[nodiscard]] auto find_by_name(const std::vector<Attribute>& attributes, const Rng& names) -> std::vector<Attribute>
{
	return attributes | vw::filter([&names](const auto& attr) { return has(names, attr.name()); })
	                  | rg::to<std::vector>();
}

}

/// @file DualDimensionInfo.hpp
/// DualDimensionInfo class definition

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_DB_NETCDF_DUAL_DIMENSION_INFO_HPP
#define TWIST_DB_NETCDF_DUAL_DIMENSION_INFO_HPP

namespace twist::db::netcdf {
class DimensionInfo;
}

namespace twist::db::netcdf {

/*! Class storing information about a "dual dimension".
    Usually, a dimension defined in a netCDF file has an associated variable, with the same name and whose data length 
	  matches the dimension length, and that variable defines the dimension data (we refer to those dimensions as 
	  "single dimensions" here).
    Some dimensions however store their data in combination with one other dimension, and we call those dimensions 
	  "dual dimensions". 
    There are always two "single dimensions" associated with a "dual dimension", and two variables, both of which use 
	  both dimensions (in the same order) and no others. The data stored by one or both of the variables may may not be 
	  uniformly spaced, nor monotonic
    An example of "dual dimensions" is non-Euclidian spatial dimensions, where the data points of the variables using 
	  the two dimensions do not fall into a grid, and the coordinates of each have to be specified individually.
    In some spatial dimension cases, you can think of dimension and variable 1 being associated with the Y spatial axis 
	  and dimension and variable 2 associated with the X spatial axis (so that the data is indexed like a matrix).
 */
class DualDimensionInfo {
public:
	using DimInfoPtr = std::shared_ptr<DimensionInfo>;

	/// Get information about the first dimension composing the "dual dimension".
	DimInfoPtr dim1_info() const;

	/// Get information about the second dimension composing the "dual dimension".
	DimInfoPtr dim2_info() const;
	
	/*! Get the name of the first variable corresponding to the "dual dimension". It uses the first and second 
	    dimensions (in that order) and no others.
	 */
	std::string var1_name() const;
	
	/*! Get the name of the second variable corresponding to the "dual dimension". It uses the first and second 
	    dimensions (in that order) and no others.
	 */
	std::string var2_name() const;
	
	/// Get the data size for either of the first or the second variable; it equals the product of the two 
	/// dimensions' lengths.
	size_t var_data_size() const;

	TWIST_NO_COPY_NO_MOVE(DualDimensionInfo)
	
private:
	friend class NetcdfFile;
	
	DualDimensionInfo(DimInfoPtr dim1_info, DimInfoPtr dim2_info, 
			          std::string_view var1_name, std::string_view var2_name);
	
	DimInfoPtr dim1_info_;
	DimInfoPtr dim2_info_; 
	std::string var1_name_;
	std::string var2_name_;

	TWIST_CHECK_INVARIANT_DECL
};

}

#endif

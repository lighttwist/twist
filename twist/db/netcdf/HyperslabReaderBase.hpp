/// @file HyperslabReaderBase.hpp
/// HyperslabReaderBase class definition

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_DB_NETCDF_HYPERSLAB_READER_BASE_HPP
#define TWIST_DB_NETCDF_HYPERSLAB_READER_BASE_HPP

#include "twist/db/netcdf/DimensionData.hpp"
#include "twist/db/netcdf/DimensionPosRange.hpp"
#include "twist/db/netcdf/DimensionValueRange.hpp"
#include "twist/db/netcdf/DualDimensionInfo.hpp"
#include "twist/db/netcdf/DualDimensionData.hpp"

namespace twist::db::netcdf {
class DimensionInfo;
class NetcdfFile;
class VariableInfo;
}

namespace twist::db::netcdf {

/*! Abstract base for classes which facilitates reading, from a netCDF file, information about a "hyperslab" and the 
    hyperslab data together with the data for each dimension used (that is, each dimension along which the hyperslab 
	is defined). 
    This class is an implemetation detail for the derived classes and is not to be used directly.
 */
class HyperslabReaderBase {
public:
	TWIST_NO_COPY_NO_MOVE(HyperslabReaderBase)

protected:
/// @cond  	
	using DimInfo = DimensionInfo;
	using DimInfoPtr = std::shared_ptr<DimInfo>;

	using VarInfo = VariableInfo;
	using VarInfoPtr = std::shared_ptr<VarInfo>;

	using DdimInfoPtr = std::shared_ptr<DualDimensionInfo>;

	template<class DimVal> using DimData = DimensionData<DimVal>;
	template<class DimVal> using DimDataPtr = std::shared_ptr<DimData<DimVal>>;

	template<class DimVal> using DimValRange = DimensionValueRange<DimVal>;

	using DimPosRange = DimensionPosRange;

	template<class Dim1Val, class Dim2Val> using DdimData = DualDimensionData<Dim1Val, Dim2Val>;
	template<class Dim1Val, class Dim2Val> using DdimDataPtr = std::shared_ptr<DdimData<Dim1Val, Dim2Val>>;

	HyperslabReaderBase(std::shared_ptr<NetcdfFile> file);

	const NetcdfFile& file() const;

	template<class DimVal> 
	DimDataPtr<DimVal> make_hyslab_sdim_data(const DimValRange<DimVal>& dim_val_range, 
			const VarInfo& var_info, std::vector<DimInfoPtr>& hyslab_dims_info, 
			std::vector<size_t>& dim_start_positions, std::vector<size_t>& dim_lengths) const;

	template<class DimVal> 
	std::tuple<DimDataPtr<DimVal>, DimPosRange> calc_hyslab_dim_data(
			const DimValRange<DimVal>& dim_val_range) const;

	template<class DimVal>
	DimDataPtr<DimVal> make_hyslab_dim_data(const DimensionData<DimVal>& file_dim_data,
			const DimPosRange& file_dim_pos_range) const;

	template<class Dim1Val, class Dim2Val>		
	static DdimDataPtr<Dim1Val, Dim2Val> make_dual_dim_data(DdimInfoPtr ddim_info, VarInfoPtr var1_info, 
			VarInfoPtr var2_info, std::vector<Dim1Val>&& data1, std::vector<Dim2Val>&& data2);

	static DimInfoPtr make_hyslab_dim_info(const DimInfo& file_dim_info, size_t hyslab_dim_length);

	static VarInfoPtr make_hyslab_var_info(const VarInfo& file_var_info, 
			const std::vector<DimInfoPtr>& hyslab_dims_info);	

	static DdimInfoPtr make_hyslab_dual_dim_info(DimInfoPtr hyslab_dim1_info, DimInfoPtr hyslab_dim2_info, 
			std::string_view var1_name, std::string_view var2_name); 

	template<class VarVal> 
	[[nodiscard]] auto read_var_hyperslab_data(int var_id, 
			                                   const std::vector<size_t>& dim_start_positions, 
								               const std::vector<size_t>& dim_lengths) const -> std::vector<VarVal>;

	int get_file_dim_id(std::string_view dim_name) const;

	std::tuple<VarInfoPtr, int> get_file_var_info_id(std::string_view var_name) const;
/// @endcond  

private:
	std::shared_ptr<NetcdfFile> file_;

	TWIST_CHECK_INVARIANT_DECL
};

}

#include "twist/db/netcdf/HyperslabReaderBase.ipp"

#endif

/// @file DualDimensionPosRanges.hpp
/// DualDimensionPosRanges class definition

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_DB_NETCDF_DUAL_DIMENSION_POS_RANGES_HPP
#define TWIST_DB_NETCDF_DUAL_DIMENSION_POS_RANGES_HPP

#include "twist/math/ClosedInterval.hpp"

namespace twist::db::netcdf {
class DualDimensionInfo;
}

namespace twist::db::netcdf {

/*! This class defines two ranges of positions along the two dimension components of a netCDF "dual dimension". 
    Both these ranges are necessary for cropping any variable data associated with the "dual dimension", including the 
    data of the two dimension variables.
    See class DualDimensionInfo for details about "dual dimensions" and class DimensionPosRange for details about 
	defining a range of positions along a netCDF dimension.
 */
class DualDimensionPosRanges {
public:
	using DdimInfoPtr = std::shared_ptr<DualDimensionInfo>;
	using PosRange = twist::math::ClosedInterval<size_t>;

	/// Constructor.
	///
	/// @param[in] dual_dim_info  Information about the "dual dimension"
	/// @param[in] dim1_pos_range  The range of positions along the first dimension composing the 
	///					"dual dimension"  
	/// @param[in] dim2_pos_range  The range of positions along the second dimension composing the 
	///					"dual dimension"  
	///
	DualDimensionPosRanges(DdimInfoPtr dual_dim_info, PosRange dim1_pos_range, 
			PosRange dim2_pos_range);

	/// Get information about the "dual dimension".
	///
	/// @return  The "dual dimension" information
	///
	DdimInfoPtr dual_dim_info() const;

	/// Get the name of the first dimension composing the "dual dimension".
	///
	/// @return  The first dimension name
	///
	std::string dim1_name() const;

	/// Get the name of the second dimension composing the "dual dimension".
	///
	/// @return  The second dimension name
	///
	std::string dim2_name() const;
	
	/// Get the range of positions along the first dimension composing the "dual dimension".  
	///
	/// @return  The position range along the first dimension
	///
	PosRange dim1_pos_range() const;
	
	/// Get the range of positions along the second dimension composing the "dual dimension".  
	///
	/// @return  The position range along the second dimension
	///
	PosRange dim2_pos_range() const;
	
private:
	DdimInfoPtr dual_dim_info_;
	PosRange dim1_pos_range_;
	PosRange dim2_pos_range_;

	TWIST_CHECK_INVARIANT_DECL
};

}

#endif

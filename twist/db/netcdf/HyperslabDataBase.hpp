/// @file HyperslabDataBase.hpp 
/// HyperslabDataBase abstract base class definition

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_DB_NETCDF_HYPERSLAB_DATA_BASE_HPP
#define TWIST_DB_NETCDF_HYPERSLAB_DATA_BASE_HPP

#include "twist/db/netcdf/VariableInfo.hpp"

namespace twist::db::netcdf {

/*! Abstract base for classes which store information about a "hyperslab", which uses a number of dimensions, and the 
    hyperslab data together with the data for each dimension used. 
    A hyperslab is a multidimensional chunk of data, representing a specific variable's data. We refer here to the 
	variable as the "hyperslab variable". 
    \tparam  VarVal  The value type of the "hyperslab variable" data 
 */
template<class VarVal>
class HyperslabDataBase {
public:
	using VarInfoPtr = std::shared_ptr<VariableInfo>;
	using DimInfoPtr = std::shared_ptr<DimensionInfo>;

	//! Variable information for the "hyperslab variable".
	[[nodiscard]] auto var_info() const -> VarInfoPtr;

	//! Get the "hyperslab variable" data, which is the core data of the hyperslab.
	[[nodiscard]] auto var_data() const -> const std::vector<VarVal>&;

	TWIST_NO_COPY_NO_MOVE(HyperslabDataBase)

protected:
	/*! Constructor. 
	    \param[in] var_info  The "hyperlab variable" info
	    \param[in] var_data  The "hyperlab variable" data
	 */
	HyperslabDataBase(VarInfoPtr var_info, std::vector<VarVal>&& var_data);

private:
	VarInfoPtr var_info_;
	std::vector<VarVal> var_data_;

	TWIST_CHECK_INVARIANT_DECL
};

}

#include "twist/db/netcdf/HyperslabDataBase.ipp"

#endif

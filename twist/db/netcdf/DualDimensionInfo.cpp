/// @file DualDimensionInfo.cpp
/// Implementation file for "DualDimensionInfo.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/db/netcdf/DualDimensionInfo.hpp"

#include "twist/db/netcdf/DimensionInfo.hpp"

#include "twist/math/ClosedInterval.hpp"

namespace twist::db::netcdf {

DualDimensionInfo::DualDimensionInfo(DimInfoPtr dim1_info, DimInfoPtr dim2_info,
		std::string_view var1_name, std::string_view var2_name)
	: dim1_info_{move(dim1_info)}
	, dim2_info_{move(dim2_info)}
	, var1_name_{var1_name}
	, var2_name_{var2_name}
{
	TWIST_CHECK_INVARIANT
}

DualDimensionInfo::DimInfoPtr DualDimensionInfo::dim1_info() const
{
	TWIST_CHECK_INVARIANT
	return dim1_info_;
}

DualDimensionInfo::DimInfoPtr DualDimensionInfo::dim2_info() const
{
	TWIST_CHECK_INVARIANT
	return dim2_info_;
}

std::string DualDimensionInfo::var1_name() const
{
	TWIST_CHECK_INVARIANT
	return var1_name_;
}

std::string DualDimensionInfo::var2_name() const
{
	TWIST_CHECK_INVARIANT
	return var2_name_;
}

size_t DualDimensionInfo::var_data_size() const
{
	TWIST_CHECK_INVARIANT
	return dim1_info_->length() * dim2_info_->length();
}

#ifdef _DEBUG
void DualDimensionInfo::check_invariant() const noexcept
{
	assert(dim1_info_->name() != dim2_info_->name());
	assert(!is_whitespace(var1_name_));
	assert(!is_whitespace(var2_name_));
	assert(var1_name_ != var2_name_);
	assert(var1_name_ != dim1_info_->name() && var1_name_ != dim2_info_->name());
	assert(var2_name_ != dim1_info_->name() && var2_name_ != dim2_info_->name());
}
#endif

}

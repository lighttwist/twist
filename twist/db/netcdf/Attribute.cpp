/// @file Attribute.cpp
/// Implementation file for "Attribute.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/db/netcdf/Attribute.hpp"

#include "twist/db/netcdf/DimensionInfo.hpp"

using namespace twist;

namespace twist::db::netcdf {

Attribute::Attribute(std::string name, DataType data_type, Value value)
	: name_{move(name)}
	, data_type_{data_type}
	, value_{move(value)}
{
	TWIST_CHECK_INVARIANT
}

auto Attribute::name() const -> std::string
{
	TWIST_CHECK_INVARIANT
	return name_;
}

auto Attribute::data_type() const -> DataType
{
	TWIST_CHECK_INVARIANT
	return data_type_;
}

auto Attribute::value() const -> Value
{
	TWIST_CHECK_INVARIANT
	return value_;
}

#ifdef _DEBUG
void Attribute::check_invariant() const noexcept
{
	assert(!is_whitespace(name_));
	assert(is_defined(data_type_));
}
#endif

}

/// @file HyperslabAndDualDimParams.hpp
/// HyperslabAndDualDimParams class template definition

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_DB_NETCDF_HYPERSLAB_AND_DUAL_DIM_PARAMS_HPP
#define TWIST_DB_NETCDF_HYPERSLAB_AND_DUAL_DIM_PARAMS_HPP

#include "twist/db/netcdf/DimensionValueRange.hpp"
#include "twist/db/netcdf/DualDimensionValueRanges.hpp"
#include "twist/db/netcdf/HyperslabAndDualDimParams.hpp"

namespace twist::db::netcdf {

///
///  Class which stores the parameters which specify a "hyperslab" which uses a "dual dimension" and any 
///    number of "single dimensions".
///  The hyperslab is specified along each dimension it uses (the dimensions composing the "dual dimension" 
///    and the "single dimensions"), however the "hyperslab variable" is not specified, as this set of 
///    parameters can be applied to multiple variables, provided they use the appropriate dimensions.
///  See class DualDimensionInfo for details about "dual dimensions" and class HyperslabAndDualDimData for 
///    details about the hyperslab structure.
///
///  @tparam  Ddim1Val  The data value type of the first "single dimension" in the "dual dimension" (ie the 
///					type of the data stored by first variable corresponding to the "dual dimension")
///  @tparam  Ddim1Val  The data value type of the second "single dimension" in the "dual dimension" (ie the 
///					type of the data stored by second variable corresponding to the "dual dimension")
///  @tparam  SdimVals...  The value type of each of the "single dimension"'s data, ie the data types of the 
///					variables associated with each of the "single dimensions" used by the hyperslab (outside 
///                 the "dual dimension"); the ordering of the templates making up this variadic parameter, 
///					together with the constructor arguments, establish the order of the "single dimensions" 
///					for this class
///
template<class Ddim1Val, class Ddim2Val, class... SdimVals>
class HyperslabAndDualDimParams {
public:
	using DdimInfoPtr = std::shared_ptr<DualDimensionInfo>;

	using DdimValRanges = DualDimensionValueRanges<Ddim1Val, Ddim2Val>;

	template<class DimVal> using SdimValRange = DimensionValueRange<DimVal>;

	using SdimsParams = HyperslabAndDimParams<SdimVals...>;

	/// The number of "single dimensions" (outside of the "dual dimension") used by the hyperslab; if zero 
	/// then the hyperslab only uses the "dual dimension"
	static constexpr size_t single_dim_count = sizeof...(SdimVals);

	/// The total number of dimensions used by the hyperslab (the two dimensions used by the "dual dimension"
	/// and the "single dimensions")
	static constexpr size_t all_dim_count = single_dim_count + 2;

	/// Constructor.
	///
	/// @param[in] ddim_val_ranges  The ranges of dimension values along the two dimensions making up the 
	///					"dual dimension"    
	/// @param[in] sdim_val_ranges  A list of "dimension value range" objects, one for each "single dimension" 
	///					used (outside of the "dual dimension") by the hyperslab
	///
	HyperslabAndDualDimParams(const DdimValRanges& ddim_val_ranges, 
			                  const SdimValRange<SdimVals>&... sdim_val_ranges)
		: ddim_val_ranges_{ddim_val_ranges}
		, sdims_params_{}
	{	// Visual Studio 2017 from version 15.9.3 onwards seems to dislike the separate definition of 
		// method templates with variadic arguments
		if constexpr (single_dim_count > 0) {
			sdims_params_.emplace(sdim_val_ranges...);
		}
		TWIST_CHECK_INVARIANT
	}

	/// Get the name of the first dimension composing the "dual dimension".
	///
	/// @return  The name of the first dimension in the "dual dimension"
	///
	std::string dual_dim_dim1_name() const;

	/// Get the name of the seconnd dimension composing the "dual dimension".
	///
	/// @return  The name of the second dimension in the "dual dimension"
	///
	std::string dual_dim_dim2_name() const;

	/// Get the name of the first dimension variable used by the "dual dimension".
	///
	/// @return  The name of the first dimension variable in the "dual dimension"
	///
	std::string dual_dim_var1_name() const;

	/// Get the name of the second dimension variable used by the "dual dimension".
	///
	/// @return  The name of the second dimension variable in the "dual dimension"
	///
	std::string dual_dim_var2_name() const;

	/// Get information about the "dual dimension".
	///
	/// @return  The "dual dimension" information
	///
	DdimInfoPtr dual_dim_info() const; 

	/// Get the dimension value ranges which define the hyperslab along the dimensions composing the "dual 
	/// dimension".
	///
	/// @return  The dimension value ranges for the "dual dimension" 
	///
	const DdimValRanges& dual_dim_val_ranges() const;

	/// Find out whether the hyperslab uses (is defined along) any "single dimensions" (outside the "dual 
	/// dimension").
	///
	/// @return  true if the hyperslab uses any "single dimensions"
	///
	bool has_single_dims_params() const;

	/// Get the set of parameters which specify the hyperslab along the "single dimensions" (outside the "dual 
	/// dimension"). An exception is thrown if the hyperslab does not use "single dimensions".
	///
	/// @return  The set of parameters which specify the hyperslab along the "single dimensions"
	///
	const SdimsParams& single_dims_params() const;  

	TWIST_DEF_COPY_NO_MOVE(HyperslabAndDualDimParams)

private:
	DdimValRanges  ddim_val_ranges_;
	std::optional<SdimsParams>  sdims_params_;

	TWIST_CHECK_INVARIANT_DECL
};

}

#include "twist/db/netcdf/HyperslabAndDualDimParams.ipp"

#endif

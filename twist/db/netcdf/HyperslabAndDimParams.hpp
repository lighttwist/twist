/// @file HyperslabAndDimParams.hpp
/// HyperslabAndDimParams class template definition

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_DB_NETCDF_HYPERSLAB_AND_DIM_PARAMS_HPP
#define TWIST_DB_NETCDF_HYPERSLAB_AND_DIM_PARAMS_HPP

#include "twist/db/netcdf/DimensionValueRange.hpp"

namespace twist::db::netcdf {

///
///  Class which stores the parameters which specify a "hyperslab" which uses "single dimensions" (as opposed 
///    to "dual dimensions").
///  The hyperslab is specified along each dimension it uses, however the "hyperslab variable" is not 
///    specified, as this set of parameters can be applied to multiple variables, provided they use the 
///    appropriate dimensions.
///  See class DualDimensionInfo for details about "dual dimensions" and class HyperslabAndDimData for details 
///    about the hyperslab structure.
///
///  @tparam  DimVals...  The value type of each of the dimension's data, ie the data types of the variables 
///					associated with each of the "single dimensions" used by the hyperslab; the ordering of the
///					template arguments making up this variadic parameter, together with the constructor 
///					arguments, establish the order of the dimensions for this class
///
template<class... DimVals>
class HyperslabAndDimParams {
public:
	template<class DimVal> 
	using DimValRange = DimensionValueRange<DimVal>;

	using DimRangeTuple = std::tuple<DimValRange<DimVals>...>;

	/// The type of a specific "dimension value range" object stored by this class.
	/// @tparam  DimIdx  The dimension index, in the ordering established by the class variadic template 
	///				parameter
	template<size_t DimIdx> 
	using DimRangeType = std::tuple_element_t<DimIdx, DimRangeTuple>;

	/// The number of dimensions used by the hyperslab
	static const size_t dim_count = sizeof...(DimVals);

	/// Constructor.
	///
	/// @param[in] dim_ranges  A list of "dimension value range" objects, one for each dimension used by 
	///					the hyperslab
	///
	HyperslabAndDimParams(const DimValRange<DimVals>&... dim_ranges)
		: dim_val_ranges_{ dim_ranges... }  
	{	// Visual Studio 2017 from version 15.9.3 onwards seems to dislike the separate definition of 
		// method templates with variadic arguments
		static_assert(dim_count > 0, "At least one dimension must be provided");
		TWIST_CHECK_INVARIANT
	}

	/// Get the dimension value range for one of the dimensions used by the hyperslab.
	///
	/// @tparam  DimIdx  The index of the dimension, in the dimension ordering established by the constructor
	/// @return  The dimension value range
	///
	template<size_t DimIdx> 
	const DimRangeType<DimIdx>& dim_val_range() const;

	/// Get a reference to a tuple containing "dimension value range" objects, one for each of the dimensions 
	/// used by the hyperslab. The elements of the tuple follow the dimension ordering established by  
	/// the constructor.
	///
	/// @return  Reference to the dimension value range tuple
	///
	const DimRangeTuple& dim_val_ranges() const;

	TWIST_DEF_COPY_NO_MOVE(HyperslabAndDimParams)

private:
	DimRangeTuple  dim_val_ranges_;

	TWIST_CHECK_INVARIANT_DECL
};

}

#include "HyperslabAndDimParams.ipp"

#endif

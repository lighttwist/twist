/// @file HyperslabAndDualDimDataReader.hpp
/// HyperslabAndDualDimDataReader class definition

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_DB_NETCDF_HYPERSLAB_AND_DUAL_DIM_DATA_READER_HPP
#define TWIST_DB_NETCDF_HYPERSLAB_AND_DUAL_DIM_DATA_READER_HPP

#include "twist/metaprogramming.hpp"
#include "twist/std_vector_utils.hpp"

#include "twist/db/netcdf/DualDimensionInfo.hpp"
#include "twist/db/netcdf/NetcdfFile.hpp"
#include "twist/db/netcdf/HyperslabAndDualDimData.hpp"
#include "twist/db/netcdf/HyperslabAndDualDimParams.hpp"
#include "twist/db/netcdf/HyperslabReaderBase.hpp"

namespace twist::db::netcdf {

/*!  Class which facilitates reading, from a netCDF file, information about a "hyperslab" which uses a "dual dimension" 
     and any number of "single dimensions", and the hyperslab data together with the data for each dimension used. 
     See class template HyperslabAndDualDimData for details about the hyperslab data structure.
     The hyperslab data being read is a part of the data (or the whole data) associated with a variable defined in the 
     file. The hyperslab is defined by a subrange of values along each of the dimensions used by the hyperslab (two 
	 ranges for the "dual dimension" and one for each of the "single dimensions"). 
     See class template HyperslabAndDualDimParams for details about how the the hyperlab is specified along the 
     dimensions.
     \tparam VarVal  The value type of the "hyperslab variable" data 
     \tparam Ddim1Val  The data value type of the first "single dimension" in the "dual dimension" (ie the type of the 
	                   data stored by first variable corresponding to the "dual dimension")
     \tparam Ddim1Val  The data value type of the second "single dimension" in the "dual dimension" (ie the type of the 
	                   data stored by second variable corresponding to the "dual dimension")
     \tparam SdimVals...  The value type of each of the "single dimension"'s data, ie the data types of the variables 
	                      associated with each of the "single dimensions" used by the hyperslab (outside the "dual 
						  dimension"); the ordering of the templates making up this variadic parameter, together with 
						  the constructor arguments, establish the order of the "single dimensions" for this class
 */
template<class VarVal, class Ddim1Val, class Ddim2Val, class... SdimVals>
class HyperslabAndDualDimDataReader : private HyperslabReaderBase {
public:
	using FilePtr = std::shared_ptr<NetcdfFile>;

	using Params = HyperslabAndDualDimParams<Ddim1Val, Ddim2Val, SdimVals...>;

	using HyslabData = HyperslabAndDualDimData<VarVal, Ddim1Val, Ddim2Val, SdimVals...>;
	using HyslabDataPtr = std::shared_ptr<HyslabData>;

	/// The number of "single dimensions" (outside of the "dual dimension") used by the hyperslab; if zero 
	/// then the hyperslab only uses the "dual dimension"
	static constexpr size_t single_dim_count = sizeof...(SdimVals);

	/// The total number of dimensions used by the hyperslab (the two dimensions used by the "dual dimension"
	/// and the "single dimensions")
	static constexpr size_t all_dim_count = single_dim_count + 2;

	/*! Constructor.
	    \param[in] file  The netCDF file
	    \param[in] params  The parameters which specify the hyperslab along each dimension, but not the "hypeslab 
                           variable" (these parameters can be applied to multiple variables, provided they use the 
                           appropriate dimensions); this class will use the dimension ordering defined in the 
                           parameters, both for the two dimensions composing the "dual dimension" and for the "single 
                           dimensions"
	 */
	HyperslabAndDualDimDataReader(FilePtr file, const Params& params);

	/*! Read the hyperslab data from the file.
	    \param[in] var_name  The "hyperslab variable" name
	    \param[in] ddim_pos_border  If not nullptr, the position range for each dimension in the dual dimension will 
		                            extend a number postions equal to this value, before the calculated lower bound and 
									the same number of positions after the calculated upper bound; if it is not 
									possible to extend this number of positions on both sides for both dimensions, the 
									value is shrunk until it becomes possible
	    \return  The hyperslab data
	 */
	[[nodiscard]] auto read_hyperslab_with_dims(std::string_view var_name, size_t* ddim_pos_border) const 
	                    -> HyslabDataPtr;

	TWIST_NO_COPY_NO_MOVE(HyperslabAndDualDimDataReader)

private:
	using SdimDataPtrTuple = std::tuple<DimDataPtr<SdimVals>...>;

	using DimPosRange = DimensionPosRange;

	using DdimPosRanges = DualDimensionPosRanges; 

	using VarInfo = VariableInfo;
	using VarInfoPtr = std::shared_ptr<VarInfo>;

	// Read the data associated with the "dual dimension" from the file, and truncate it according to the 
	// dual dimension value ranges specified in the hyperslab paramaters, creating a new set of "dual 
	// dimension" data, corresponding to the hyperlab.
	[[nodiscard]] auto calc_hyslab_dual_dim_data(size_t* ddim_pos_border) const 
	                    -> std::tuple<DdimDataPtr<Ddim1Val, Ddim2Val>, DdimPosRanges>;

	void verify_var(const VarInfo& var_info) const;

	const Params  params_;

	TWIST_CHECK_INVARIANT_DECL
};

}

#include "twist/db/netcdf/HyperslabAndDualDimDataReader.ipp"

#endif


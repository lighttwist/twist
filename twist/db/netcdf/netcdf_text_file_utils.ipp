/// @file netcdf_text_file_utils.ipp
/// Inline implementation file for "netcdf_text_file_utils.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace twist::db::netcdf {

template<class VarVal, class Fn, class> 
void export_2d_var_data_to_csv(const NetcdfFile& file, const std::string& var_name, 
		const std::string& x_dim_name, const std::string& y_dim_name, const fs::path& csv_path, 
		Fn&& data_val_to_str)
{
	auto var_info = file.get_var_info(var_name);
	if (var_info->dims_info().size() != 2) {
		TWIST_THROW(L"Variable \"%s\" does not use 2 dimensions.", ansi_to_string(var_name).c_str());
	}
	const auto dim1_length = file.get_dim_info(x_dim_name)->length();  // the number of columns in the csv
	const auto dim2_length = file.get_dim_info(y_dim_name)->length();  // the number of rows in the csv 
	if (var_info->data_length() != dim1_length * dim2_length) {
		TWIST_THROW(L"Data for variable \"%s\" has the wrong length, given the 2 dimensions it uses.", 
				ansi_to_string(var_name).c_str());
	}

	// All good, read the variable data
	const auto var_data = file.read_var_data<VarVal>(var_name);

	// Write it out to the CSV file
	CsvFileWriter<char> csv_file{csv_path};
	for (auto i = 0u; i < dim2_length; ++i) {
		const Range row_value_range{
				var_data.begin() + dim1_length * i, 
				var_data.begin() + dim1_length * (i + 1)};
		csv_file.write_row(transform_to_vector(row_value_range, data_val_to_str));
	}
}

}

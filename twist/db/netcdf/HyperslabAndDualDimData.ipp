/// @file HyperslabAndDualDimData.ipp
/// Inline implementation file for "HyperslabAndDualDimData.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#define TWIST_CLASSTEMPL  template<class VarVal, class Ddim1Val, class Ddim2Val, class... SdimVals>	
#define TWIST_CLASSNAME  HyperslabAndDualDimData<VarVal, Ddim1Val, Ddim2Val, SdimVals...>
#define TWIST_CLASSTYPENAME  typename TWIST_CLASSNAME  

namespace twist::db::netcdf {

TWIST_CLASSTEMPL	
TWIST_CLASSTYPENAME::DdimDataPtr TWIST_CLASSNAME::dual_dim_data() const
{
	TWIST_CHECK_INVARIANT
	return dual_dim_data_;
}

TWIST_CLASSTEMPL	
std::vector<TWIST_CLASSTYPENAME::DimInfoPtr> TWIST_CLASSNAME::single_dims_info() const 
{
	TWIST_CHECK_INVARIANT
	std::vector<DimInfoPtr> dims_info;
	tuple_apply_to_each(single_dims_data_, [&dims_info](const auto& dd) {
		dims_info.push_back(dd->dim_info());
	});
	return dims_info;
}

TWIST_CLASSTEMPL	
std::vector<TWIST_CLASSTYPENAME::DimInfoPtr> TWIST_CLASSNAME::all_dims_info() const
{
	TWIST_CHECK_INVARIANT
	auto dims_info = single_dims_info();
	dims_info.push_back(dual_dim_data_->dual_dim_info()->dim1_info());
	dims_info.push_back(dual_dim_data_->dual_dim_info()->dim2_info());
	return dims_info;
}

TWIST_CLASSTEMPL template<size_t SingleDimIdx>
TWIST_CLASSNAME::SdimDataPtrType<SingleDimIdx> TWIST_CLASSNAME::single_dim_data() const
{
	TWIST_CHECK_INVARIANT
	return std::get<SingleDimIdx>(single_dims_data_);
}

TWIST_CLASSTEMPL
const TWIST_CLASSTYPENAME::SdimDataPtrTuple& TWIST_CLASSNAME::single_dims_data() const
{
	TWIST_CHECK_INVARIANT
	return single_dims_data_;
}

TWIST_CLASSTEMPL
std::vector<TWIST_CLASSTYPENAME::VarInfoPtr> TWIST_CLASSNAME::all_vars_info() const
{
	TWIST_CHECK_INVARIANT
	std::vector<VarInfoPtr> all_vars_info{
			this->var_info(), dual_dim_data_->var1_info(), dual_dim_data_->var2_info()};

	tuple_apply_to_each(single_dims_data_, [&all_vars_info](const auto& dd) {
		all_vars_info.push_back(dd->var_info());
	});
	return all_vars_info;
}

#ifdef _DEBUG
TWIST_CLASSTEMPL
void TWIST_CLASSNAME::check_invariant() const noexcept
{
	assert(dual_dim_data_);
	assert(this->var_info()->dims_info().size() == single_dim_count + 2);
}
#endif

}

#undef TWIST_CLASSTYPENAME  
#undef TWIST_CLASSNAME  
#undef TWIST_CLASSTEMPL 

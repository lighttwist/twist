/// @file NetcdfFile.hpp
/// NcdfFile class definition 

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_DB_NETCDF_NETCDF_FILE_HPP
#define TWIST_DB_NETCDF_NETCDF_FILE_HPP

#include "twist/db/netcdf/Attribute.hpp"
#include "twist/db/netcdf/DimensionData.hpp"
#include "twist/db/netcdf/DimensionPosRange.hpp"
#include "twist/db/netcdf/DualDimensionData.hpp"
#include "twist/db/netcdf/HyperslabAndDimData.hpp"
#include "twist/db/netcdf/HyperslabAndDimParams.hpp"
#include "twist/db/netcdf/HyperslabAndDualDimData.hpp"

namespace twist::db::netcdf {
class DimensionInfo;
class DualDimensionInfo;
class VariableInfo;
}

namespace twist::db::netcdf {

/*! Class which represents an open connection to a netCDF-compatible file, that is, a file which follows a 
    self-describing, machine-independent data format and which supports the creation, access, and sharing of 
    array-oriented scientific data.
	This class, like all code in the "twist::netcdf" namespace, is not threadsafe.
 */
class NetcdfFile {
public:
	//! Tag type for constructor tag dispatching
	enum class OpenMode {};  

	//! Tag type for constructor tag dispatching 
	enum class CreateMode {};  

	using Attr = Attribute;

	using DimInfo = DimensionInfo;
	using DimInfoPtr = std::shared_ptr<DimInfo>;

	using VarInfo = VariableInfo;
	using VarInfoPtr = std::shared_ptr<VarInfo>;

	using DdimInfo = DualDimensionInfo;
	using DdimInfoPtr = std::shared_ptr<DualDimensionInfo>;

	template<class DimVal> using DimData = DimensionData<DimVal>;
	template<class DimVal> using DimDataPtr = std::shared_ptr<DimData<DimVal>>;

	template<class Dim1Val, class Dim2Val> using DdimData = DualDimensionData<Dim1Val, Dim2Val>;
	template<class Dim1Val, class Dim2Val> using DdimDataPtr = std::shared_ptr<DdimData<Dim1Val, Dim2Val>>;

	template<class VarVal, class... DimVals> 
	using HyperslabAndDimDataPtr = std::shared_ptr<HyperslabAndDimData<VarVal, DimVals...>>;

	template<class VarVal, class Ddim1Val, class Ddim2Val, class... SdimVals> 
	using HyperslabWithDdimsDataPtr = 
			std::shared_ptr<HyperslabAndDualDimData<VarVal, Ddim1Val, Ddim2Val, SdimVals...>>;

	static const OpenMode open{};  ///< Tag value for constructor tag dispatching
	static const CreateMode create{};  ///< Tag value for constructor tag dispatching
		
	/*! Constructor; use this constructor to open an existing netCDF file.
	    \param[in] tag  Tag value for tag dispatching; use NetcdfFile::open
	    \param[in] path  The file path  
		\param[in] read_only  Whether the file should be open in read-only mode
     */
	NetcdfFile(OpenMode tag, fs::path path, bool read_only);
		
	/*! Constructor; use this constructor to create a new netCDF file.
	    \param[in] tag  Tag value for tag dispatching; use NetcdfFile::create
	    \param[in] path  The file path  
	    \param[in] dims_info  Information about the dimensions to be defined in the file
	    \param[in] vars_info  Information about the variables to be defined in the file; it is expected that these 
		                      include the dimension variables
     */
	NetcdfFile(CreateMode tag, 
	           fs::path path, 
	           const std::vector<DimInfoPtr>& dims_info, 
			   const std::vector<VarInfoPtr>& vars_info);

	~NetcdfFile();

	//! The file path.
	[[nodiscard]] auto path() const -> fs::path;

	/*! Get the number of dimensions defined in the file.
	    \return  The number of dimensions
     */
	int dim_count() const;  

	/*! Get the number of variables defined in the file.
	    \return  The number of variables
     */
	int var_count() const;  

	//! The global attributes (attributes unattached to specific variables) defined in the file.
	[[nodiscard]] auto global_attributes() const -> const std::vector<Attr>&;

	/*! Get informatin about all dimensions defined in the file.
	    \return  A list of dimension information objects, in the order they are defined in the file
     */
    [[nodiscard]] auto get_dims_info() const -> std::vector<DimInfoPtr>;

	/*! Get informatin about a specific dimension defined in the file.
	    \param[in] dim_name  The dimension name; an exception is thrown if no dimension with this name is defined in 
		                     the file (the matching is case sensitive)
     */
	DimInfoPtr get_dim_info(std::string_view dim_name) const;

	/*! Get informatin about all variables defined in the file.
	    \return  A list of variable information objects, in the order they are defined in the file
     */
	std::vector<VarInfoPtr> get_vars_info() const;

	/*! Get informatin about a specific variable defined in the file.
	    \param[in] var_name  The variable name; an exception is thrown if no variable with this name is defined in the 
		                     file (the matching is case sensitive)
     */
	[[nodiscard]] auto get_var_info(std::string_view var_name) const -> VarInfoPtr;

	/*! Given a dimension, get information about the variable which corresponds to the dimension - and holds the 
	    dimension data. The dimension must be a regular, "single dimension", as opposed to one of the dimensions making 
		up a "dual dimension". The variable will have the same name as the dimension and the data size will be the 
		dimension length. See class DimensionInfo for details about "single dimensions".
	    \param[in] dim_name  The dimension name; an exception is thrown if the file does not contain a dimension, or a 
		                     variable, with this name
	    \return  The variable info
     */
	VarInfoPtr get_var_for_dim(std::string_view dim_name) const;  

	/*! Get information about a "dual dimensions" - an entity made up of two dimensions and two corresponding 
	    variables, different from a "single dimension". (See class DualDimensionInfo for details about "dual 
		dimensions".)
	    \param[in] var1_name  The name of the first variable corresponding to the "dual dimension" 
	    \param[in] var2_name  The name of the second variable corresponding to the "dual dimension" 
	    \return  The "dual dimension" information
     */
	DdimInfoPtr get_dual_dim_info(std::string_view var1_name, std::string_view var2_name) const;

	/*! Read information about a dimension (a regular, "single dimension", as opposed to a "dual dimension") together 
	    with the data associated with its measurement ruler, which is stored in an associated variable. See class 
		DimensionInfo for details about "single dimensions".
	    \tparam DimVal  The value type of the dimension data (ie the data stored by variable corresponding to the 
		                dimension) 
	    \param[in] dim_name  The dimension name; an exception is thrown if the file does not contain a dimension, or a 
		                     variable, with this name
	    \return  The dimension data
     */
	template<class DimVal> 
	DimDataPtr<DimVal> read_dim_data(std::string_view dim_name) const;

	/*! Given a dimension (a regular, "single dimension", as opposed to a "dual dimension") which exists in the file, 
	    write the data associated with its measurement ruler to the file; the data will be stored in the associated 
	    variable. See class DimensionInfo for details about "single dimensions". The existing variable data will be 
	    lost.
	    \tparam DimVal  The value type of the dimension data; that is, the data stored by variable corresponding to the 
	                     dimension; it must match the variable data type as specified in the file 
	    \param[in] dim_data  The dimension data; the data size must match the variable data size as specified in 
	                         the file
	 */
	template<class DimVal> 
	auto write_dim_data(DimDataPtr<DimVal> dim_data) -> void;

	/*! Read all the data associated with a variable defined in the file. 
	    \tparam VarVal  The value type of the variable data; it must match the variable data type as specified in the 
		                file 
	    \param[in] var_name  The variable name; an exception is thrown if the file does not contain a variable with 
		                     this name
	    \return  The variable data; the data values are read into consecutive locations with the last dimension 
		         varying fastest
	 */
	template<class VarVal> 
	[[nodiscard]] auto read_var_data(std::string_view var_name) const -> std::vector<VarVal>;

	/*! Read a hyperslab from the data associated with a variable defined in the file.
	    A hyperslab is a subset of the variable's data, defined by a subrange of contiguous dimension values along each 
		of the dimensions along which the variable is defined.
	    \tparam VarVal  The value type of the variable data; it must match the variable data type as specified in 
		                the file 
	    \param[in] var_name  The variable name; an exception is thrown if the file does not contain a variable with 
		                     this name
	    \param[in] dim_pos_ranges  A list of dimension position ranges; for each dimension used by the variable, the 
		                           range specifies the range of the positions of the desired values along the 
								   dimension ruler; the order of the ranges must match the order in which the 
								   dimensions are listed in the variable information 
	    \return  The variable hyperslab data; the data values are read into consecutive locations with the last 
		         dimension varying fastest
	 */
	template<class VarVal> 
	[[nodiscard]] auto read_var_hyperslab_data(std::string_view var_name,
			                                   const std::vector<DimensionPosRange>& dim_pos_ranges) const 
                        -> std::vector<VarVal>;

	/*! Write the data associated with a variable defined in the file. The existing variable data will be lost.
	    \tparam VarVal  The value type of the variable data; it must match the variable data type as specified in 
		                the file 
	    \param[in] var_name  The variable name; an exception is thrown if the file does not contain a variable with 
		                     this name
	    \param[in] data  The variable data; the data size must match the variable data size as specified in the file
	 */
	template<class VarVal> 
	void write_var_data(std::string_view var_name, const std::vector<VarVal>& data);

	/*! Read the data associated with a "dual dimension" defined in the file. See DualDimensionData class template for 
	    a description of the data structure.
	    \tparam Dim1Val  The value type of the first "single dimension" data, ie the data stored by the first variable 
		                 corresponding to the "dual dimension"
	    \tparam Dim2Val  The value type of the second "single dimension" data, ie the data stored by the second 
		                 variable corresponding to the "dual dimension"
	    \param[in] var1_name  The name of the first variable corresponding to the "dual dimension"; an exception is 
		                      thrown if the file does not contain a variable with this name or if the variable is not 
							  associated with a "dual dimension"
	    \param[in] var2_name  The name of the second variable corresponding to the "dual dimension"; an exception is 
		                      thrown if the file does not contain a variable with this name or if the variable is not 
							  associated with a "dual dimension"
	    \return  The "dual dimension" data
	 */
	template<class Dim1Val, class Dim2Val> 
	DdimDataPtr<Dim1Val, Dim2Val> read_dual_dim_data(std::string_view var1_name, std::string_view var2_name) const;

	/*! Write to the file a hyperslab's data together with the data for each dimension used by the hyperslab, which 
	    uses "single dimensions". (See class template HyperslabAndDimData for details of the structure of such a 
		hyperslab). The data will be written for the "hyperslab variable" and all the variables associated with the 
		hyperslab dimensions. All the dimensions and variables associated with the hyperslab must already be defined in 
		the file, with the correct data types and/or lengths.
	    \tparam VarVal  The value type of the "hyperslab variable" data 
	    \tparam DimVals...  The value type of each of the dimension's data, ie the data types of the variables 
		                    associated with each of the "single dimensions" used by the hyperslab
	    \param[in] hyslab_data  The hyperslab data
     */
	template<class VarVal, class... DimVals> 
	void write_hyperslab_with_dims(HyperslabAndDimDataPtr<VarVal, DimVals...> hyslab_data);

	/*! Write to the file a hyperslab's data together with the data for each dimension used by the hyperslab, which 
	    uses "dual dimension" and any number of "single dimensions". (See class template HyperslabAndDualDimData for 
		details of the structure of such a hyperslab). The data will be written for the "hyperslab variable" and all 
		the variables associated with the hyperslab dimensions. All the dimensions and variables associated with the 
		hyperslab must already be defined in the file, with the correct data types and/or lengths.
	    \tparam VarVal  The value type of the "hyperslab variable" data 
	    \tparam Ddim1Val  The data value type of the first "single dimension" in the "dual dimension" (ie the type of 
		                  the data stored by first variable corresponding to the "dual dimension")
	    \tparam Ddim1Val  The data value type of the second "single dimension" in the "dual dimension" (ie the type of 
		                  the data stored by second variable corresponding to the "dual dimension")
	    \tparam SdimVals...  The value type of each of the "single dimension"'s data, ie the data types of the 
		                     variables associated with each of the "single dimensions" used by the hyperslab (outside 
							 the "dual dimension")
	    \param[in] hyslab_data  The hyperslab data
     */
	template<class VarVal, class Ddim1Val, class Ddim2Val, class... SdimVals> 
	void write_hyperslab_with_dual_dims(
	             HyperslabWithDdimsDataPtr<VarVal, Ddim1Val, Ddim2Val, SdimVals...> hyslab_data);

	TWIST_NO_COPY_NO_MOVE(NetcdfFile)

private:
	friend class HyperslabReaderBase;

	using DimWithId = std::tuple<DimInfoPtr, int>;

	using VarWithId = std::tuple<VarInfoPtr, int>;

	static const int tup_dim = 0;
	static const int tup_var = 0;
	static const int tup_id = 1;

	template<class... Args> 
	static DimInfoPtr make_dim_info(Args&&... args);

	template<class... Args> 
	static DdimInfoPtr make_dual_dim_info(Args&&... args);

	template<class DimVal, class... Args> 
	static DimDataPtr<DimVal> make_dim_data(Args&&... args);

	template<class Dim1Val, class Dim2Val, class... Args> 
	static DdimDataPtr<Dim1Val, Dim2Val> make_dual_dim_data(Args&&... args);

	void read_dims_info(int dim_count) const;

	void create_dims_info(const std::vector<DimInfoPtr>& dims_info);

	void read_vars_info(int var_count) const;

	void create_vars_info(const std::vector<VarInfoPtr>& vars_info);

	std::vector<Attr> read_var_attributes(int var_id) const;

	void write_var_attributes(int var_id, const std::vector<Attr>& attrs);

	std::optional<Attr::Value> read_attr_value(int var_id, const char* attr_name, DataType data_type, 
			size_t value_count) const;

	int read_var_float32_data(int var_id, float* buffer) const;

	int write_var_float32_data(int var_id, const float* buffer);

	int read_var_float64_data(int var_id, double* buffer) const;

	int write_var_float64_data(int var_id, const double* buffer);

	int read_var_int32_data(int var_id, int* buffer) const;

	int write_var_int32_data(int var_id, const int* buffer);

	template<class VarVal> 
	void read_var_hyperslab_data(int var_id, const std::vector<size_t>& dim_start_positions, 
			const std::vector<size_t>& dim_lengths, std::vector<VarVal>& buffer) const;

	int read_var_float32_hyperslab_data(int var_id, const size_t* dim_start_positions, 
			const size_t* dim_lengths, float* buffer) const;

	int read_var_float64_hyperslab_data(int var_id, const size_t* dim_start_positions, 
			const size_t* dim_lengths, double* buffer) const;

	auto read_var_int32_hyperslab_data(int var_id, 
	                                   const size_t* dim_start_positions, 
			                           const size_t* dim_lengths, 
									   int* buffer) const -> int;

	DimInfoPtr get_dim_info_from_id(int id) const; 

	int get_dim_id_from_info(const DimInfoPtr& dim_info) const; 

	VarInfoPtr get_var_info_from_id(int id) const; 

	static std::wstring get_native_err_msg(int native_err_no);

	const fs::path path_;
	int file_id_{0};

	std::vector<Attr> gattrs_;  
	int unlim_dim_id_{0};   

	mutable std::map<std::string, DimWithId, std::less<>> dims_info_;
	mutable std::map<std::string, VarWithId, std::less<>> vars_info_;
	mutable std::map<std::tuple<std::string, std::string>, DdimInfoPtr, std::less<>> ddims_info_;

	TWIST_CHECK_INVARIANT_DECL
};

}

#include "twist/db/netcdf/NetcdfFile.ipp"

#endif 

/// @file HyperslabAndDimDataReader.ipp
/// Inline implementation file for "HyperslabAndDimDataReader.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#define TWIST_CLASSTEMPL  template<class VarVal, class... DimVals>

#define TWIST_CLASSNAME HyperslabAndDimDataReader<VarVal, DimVals...>

#define TWIST_CLASSTYPENAME  typename TWIST_CLASSNAME

namespace twist::db::netcdf {

TWIST_CLASSTEMPL
TWIST_CLASSNAME::HyperslabAndDimDataReader(FilePtr file, const Params& params)
	: HyperslabReaderBase{file}
	, params_{params}
{
	TWIST_CHECK_INVARIANT
}


TWIST_CLASSTEMPL
auto TWIST_CLASSNAME::read_hyperslab_with_dims(std::string_view var_name) const -> HyslabWithDimsDataPtr
{
	TWIST_CHECK_INVARIANT
	const auto [var_info, var_id] = get_file_var_info_id(var_name);

	// Put together the following vectors, each containing info related to all dimensions, in the order in 
	// which these dimensions appear in the hyperslab variable's list of dimensions
	//   * a vector of "dimension info" pointers
	//   * a vector of dimension position range starting positions
	//   * a vector of dimension position range lengths
	auto hyslab_dims_info = std::vector<DimInfoPtr>{dim_count};
	auto dim_start_positions = std::vector<size_t>(dim_count, netcdf_no_idx);
	auto dim_lengths = std::vector<size_t>(dim_count, netcdf_no_idx);

	const auto hyslab_dims_data_tuple = tuple_transform_each(params_.dim_val_ranges(), [&](const auto& dr) { 
		return make_hyslab_sdim_data(dr, *var_info, hyslab_dims_info, dim_start_positions, dim_lengths); 
	});

	// Read the hyperslab variable data, truncated for the hyperslab, from the file
	auto hyslab_var_data = read_var_hyperslab_data<VarVal>(var_id, dim_start_positions, dim_lengths); 

	auto hyslab_var_info = make_hyslab_var_info(*var_info, hyslab_dims_info);			

	auto hyslab_with_dims_data = tuple_apply(hyslab_dims_data_tuple, [&](const auto&... dims_data) {
		return std::make_shared<HyslabWithDimsData>(
				hyslab_var_info, move(hyslab_var_data), dims_data...);
	});

	return hyslab_with_dims_data;
}


#ifdef _DEBUG
TWIST_CLASSTEMPL
void TWIST_CLASSNAME::check_invariant() const noexcept
{
}
#endif

}

#undef  TWIST_CLASSTYPENAME
#undef  TWIST_CLASSNAME
#undef  TWIST_CLASSTEMPL

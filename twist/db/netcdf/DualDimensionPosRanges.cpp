/// @file DualDimensionInfo.cpp
/// Implementation file for "DualDimensionInfo.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/db/netcdf/DualDimensionPosRanges.hpp"

#include "twist/db/netcdf/DimensionInfo.hpp"
#include "twist/db/netcdf/DualDimensionInfo.hpp"

namespace twist::db::netcdf {

DualDimensionPosRanges::DualDimensionPosRanges(DdimInfoPtr dual_dim_info, 
                                               PosRange dim1_pos_range, 
											   PosRange dim2_pos_range)
	: dual_dim_info_{dual_dim_info}
	, dim1_pos_range_{dim1_pos_range}
	, dim2_pos_range_{dim2_pos_range}
{
	TWIST_CHECK_INVARIANT
}

DualDimensionPosRanges::DdimInfoPtr DualDimensionPosRanges::dual_dim_info() const
{
	TWIST_CHECK_INVARIANT
	return dual_dim_info_;
}

std::string DualDimensionPosRanges::dim1_name() const
{
	TWIST_CHECK_INVARIANT
	return dual_dim_info_->dim1_info()->name();
}

std::string DualDimensionPosRanges::dim2_name() const
{
	TWIST_CHECK_INVARIANT
	return dual_dim_info_->dim2_info()->name();
}
	
DualDimensionPosRanges::PosRange DualDimensionPosRanges::dim1_pos_range() const
{
	TWIST_CHECK_INVARIANT
	return dim1_pos_range_;
}
	
DualDimensionPosRanges::PosRange DualDimensionPosRanges::dim2_pos_range() const
{
	TWIST_CHECK_INVARIANT
	return dim2_pos_range_;
}

#ifdef _DEBUG
void DualDimensionPosRanges::check_invariant() const noexcept
{
	assert(dual_dim_info_);
}
#endif

}

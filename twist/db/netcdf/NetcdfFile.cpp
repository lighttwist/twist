/// @file NetcdfFile.cpp
/// Implementation file for "NetcdfFile.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/db/netcdf/NetcdfFile.hpp"

#include "twist/scope.hpp"
#include "twist/db/netcdf/DimensionInfo.hpp"
#include "twist/db/netcdf/DualDimensionInfo.hpp"
#include "twist/db/netcdf/VariableInfo.hpp"

#include <netcdf.h>

#define CHECK_ERR(err_no)  if (err_no) TWIST_THROW(get_native_err_msg(err_no).c_str());

using std::get;

namespace twist::db::netcdf {

// --- Locla functions ---

DataType to_netcdf_data_type(nc_type nctype)
{
	switch (nctype) {
	case NC_BYTE  : return DataType::int8;
	case NC_CHAR  : return DataType::chr;
	case NC_SHORT : return DataType::int16;
	case NC_INT   : return DataType::int32;
	case NC_FLOAT : return DataType::float32;
	case NC_DOUBLE: return DataType::float64;
	case NC_UBYTE : return DataType::uint8;
	case NC_USHORT: return DataType::uint16;
	case NC_UINT  : return DataType::uint32;
	case NC_INT64 : return DataType::int64;
	case NC_UINT64: return DataType::uint64;
	case NC_STRING: return DataType::string;
	case NC_NAT   :  // fall thru
	default: TWIST_THROW(L"Not a type.");
	}
}

nc_type to_native_data_type(DataType data_type)
{
	switch (data_type) {
	case DataType::int8   : return NC_BYTE;
	case DataType::chr    : return NC_CHAR;
	case DataType::int16  : return NC_SHORT;
	case DataType::int32  : return NC_INT;
	case DataType::float32: return NC_FLOAT;
	case DataType::float64: return NC_DOUBLE;
	case DataType::uint8  : return NC_UBYTE;
	case DataType::uint16 : return NC_USHORT;
	case DataType::uint32 : return NC_UINT;
	case DataType::int64  : return NC_INT64;
	case DataType::uint64 : return NC_UINT64;
	case DataType::string : return NC_STRING;
	default: TWIST_THROW(L"Not a type.");
	}
}

// --- NetcdfFile class ---

NetcdfFile::NetcdfFile(OpenMode, fs::path path, bool read_only)
	: path_{std::move(path)}
{
	if (!exists(path_)) {
		TWIST_THROW(L"NetCDF file \"%s\" does not exist.", path_.filename().c_str());
	}

	CHECK_ERR(nc_open(path_to_ansi(path_).c_str(), 
	                  read_only ? NC_NOWRITE : NC_WRITE | NC_NOCLOBBER, 
					  &file_id_)); 
	
	scope (failure) { nc_close(file_id_); };  // Close the file if the constructor fails

	auto nof_dims = -1;
	auto nof_vars = -1;
	auto nof_gattrs = -1;
	CHECK_ERR(nc_inq(file_id_, &nof_dims, &nof_vars, &nof_gattrs, &unlim_dim_id_));

	read_dims_info(nof_dims);
	read_vars_info(nof_vars);
	if (nof_gattrs > 0) {
		gattrs_ = read_var_attributes(NC_GLOBAL);
	}

	TWIST_CHECK_INVARIANT
}

NetcdfFile::NetcdfFile(CreateMode, fs::path path, const std::vector<DimInfoPtr>& dims_info, 
		               const std::vector<VarInfoPtr>& vars_info)
	: path_{std::move(path)}
{
	if (exists(path_)) {
		TWIST_THROW(L"NetCDF file \"%s\" already exists.", path_.filename().c_str());
	}

	CHECK_ERR(nc_create(path_to_ansi(path_).c_str(), NC_CLOBBER, &file_id_));

	scope (failure) { nc_close(file_id_); };  // Close the file if the constructor fails

	create_dims_info(dims_info);
	create_vars_info(vars_info);

	// End define mode: this tells netCDF we are done defining metadata
    CHECK_ERR(nc_enddef(file_id_));

	TWIST_CHECK_INVARIANT
}

NetcdfFile::~NetcdfFile()
{
	TWIST_CHECK_INVARIANT
	[[maybe_unused]] const auto err_no = nc_close(file_id_);
	assert(!err_no);
}

auto NetcdfFile::path() const -> fs::path
{
	TWIST_CHECK_INVARIANT
	return path_;
}

int NetcdfFile::dim_count() const
{
	TWIST_CHECK_INVARIANT
	return size32(dims_info_);
}

int NetcdfFile::var_count() const
{
	TWIST_CHECK_INVARIANT
	return size32(vars_info_);
}

auto NetcdfFile::global_attributes() const -> const std::vector<Attr>&
{
	TWIST_CHECK_INVARIANT
	return gattrs_;
}

auto NetcdfFile::get_dims_info() const -> std::vector<DimInfoPtr>
{
	TWIST_CHECK_INVARIANT
	auto dims_info = reserve_vector<DimInfoPtr>(dims_info_.size());
	for (auto i = 0u; i < dims_info_.size(); ++i) {
		dims_info.push_back(get_dim_info_from_id(i));
	}
	return dims_info;
}

NetcdfFile::DimInfoPtr NetcdfFile::get_dim_info(std::string_view dim_name) const
{
	if (auto it = dims_info_.find(dim_name); it != end(dims_info_)) {
		return get<tup_dim>(it->second);
	}
	TWIST_THRO2(L"There is no dimension named \"{}\".", ansi_to_string(dim_name));
}

std::vector<NetcdfFile::VarInfoPtr> NetcdfFile::get_vars_info() const
{
	TWIST_CHECK_INVARIANT
	std::vector<NetcdfFile::VarInfoPtr> vars_info;
	for (auto i = 0u; i < vars_info_.size(); ++i) {
		vars_info.push_back(get_var_info_from_id(i));
	}
	return vars_info;
}

auto NetcdfFile::get_var_info(std::string_view var_name) const -> VarInfoPtr
{
	TWIST_CHECK_INVARIANT
	if (auto it = vars_info_.find(var_name); it != end(vars_info_)) {
		return get<tup_var>(it->second);
	}
	TWIST_THRO2(L"There is no variable named \"{}\".", ansi_to_string(var_name));
}

NetcdfFile::VarInfoPtr NetcdfFile::get_var_for_dim(std::string_view dim_name) const
{
	TWIST_CHECK_INVARIANT
	DimInfoPtr dim_info;
	if (auto it = dims_info_.find(dim_name); it != end(dims_info_)) {
		dim_info = get<tup_dim>(it->second);
	}
	else {
		TWIST_THROW(L"There is no dimension named \"%s\".", ansi_to_string(dim_name).c_str());
	}

	VarInfoPtr var_info;
	if (auto it = vars_info_.find(dim_name); it != end(vars_info_)) {
		var_info = get<tup_var>(it->second);
	}
	else {
		TWIST_THROW(L"There is no variable corresponding to dimension \"%s\".", 
				ansi_to_string(dim_name).c_str());
	}

#ifdef  DEBUG
	// Sanity check
	if (var_info->dims_info().size() != 1 || var_info->dims_info().front() != dim_info) {
		TWIST_THROW(L"Invalid variable found for dimension \"%s\".", ansi_to_string(dim_name).c_str());
	}
#endif 

	return var_info;
}

NetcdfFile::DdimInfoPtr NetcdfFile::get_dual_dim_info(std::string_view var1_name, 
		std::string_view var2_name) const
{
	TWIST_CHECK_INVARIANT
	// Check whether the "dual dimension" info has been created (it is created on request)
	if (auto it = ddims_info_.find(make_tuple(var1_name, var2_name)); it != end(ddims_info_)) {
		return it->second;
	}

	// This is the first time the "dual dimension" info has been requested, create and store it

	auto var1_info = get_var_info(var1_name);
	if (var1_info->dims_info().size() != 2) {
		TWIST_THROW(L"Variable \"%s\" does not use 2 dimensions.", ansi_to_string(var1_name).c_str());
	}
	auto var2_info = get_var_info(var2_name);
	if (var2_info->dims_info().size() != 2) {
		TWIST_THROW(L"Variable \"%s\" does not use 2 dimensions.", ansi_to_string(var2_name).c_str());
	}

	auto dim1_info = var1_info->dims_info().at(0); 
	auto dim2_info = var1_info->dims_info().at(1); 
	if (var2_info->dims_info().at(0) != dim1_info || var2_info->dims_info().at(1) != dim2_info) {
		TWIST_THROW(L"Variables \"%s\" and \"%s\" do not use the same 2 dimensions in the same order.");
	}
	const auto data_length = dim1_info->length() * dim2_info->length();
	if (var1_info->data_length() != data_length) {
		TWIST_THROW(L"Variable \"%s\" has the wrong data length, given the 2 dimensions it uses.", 
				ansi_to_string(var1_name).c_str());		
	}
	if (var2_info->data_length() != data_length) {
		TWIST_THROW(L"Variable \"%s\" has the wrong data length, given the 2 dimensions it uses.", 
				ansi_to_string(var2_name).c_str());		
	}
	
	DdimInfoPtr ddim_info{ new DualDimensionInfo{dim1_info, dim2_info, var1_name, var2_name} };	
	ddims_info_.emplace(make_tuple(var1_name, var2_name), ddim_info);

	return ddim_info;
}

void NetcdfFile::read_dims_info(int dim_count) const
{
	// No invariant check: called from ctor
	assert(dims_info_.empty());
	assert(vars_info_.empty());

	for (int i = 0; i < dim_count; ++i) {

		char dim_name[NC_MAX_NAME];
		size_t dim_length = 0;
		CHECK_ERR( nc_inq_dim(file_id_, i, dim_name, &dim_length) );

		auto dim_info = make_dim_info(dim_name, dim_length);

		dims_info_.emplace(dim_name, std::make_tuple(dim_info, i));
	}
}

void NetcdfFile::create_dims_info(const std::vector<DimInfoPtr>& dims_info)
{
	// No invariant check: called from ctor
	assert(dims_info_.empty());
	assert(vars_info_.empty());

	for (const auto& di : dims_info) {
		int dim_id = -1;
		CHECK_ERR(nc_def_dim(file_id_, di->name().c_str(), di->length(), &dim_id));
		dims_info_.emplace(di->name(), std::make_tuple(di, dim_id));
	}
}

void NetcdfFile::read_vars_info(int var_count) const
{
	// No invariant check: called from ctor
	assert(!dims_info_.empty());
	assert(vars_info_.empty());

	for (int i = 0; i < var_count; ++i) {

		char var_name[NC_MAX_NAME];
		nc_type var_type = NC_NAT;
		int var_dim_count = 0;
		int var_dim_id_array[NC_MAX_VAR_DIMS];
		CHECK_ERR(nc_inq_var(file_id_, i, var_name, &var_type, &var_dim_count, var_dim_id_array, nullptr));

		std::vector<DimInfoPtr> dims_info;
		std::transform(var_dim_id_array, var_dim_id_array + var_dim_count, back_inserter(dims_info), 
				[this](auto id) { return get_dim_info_from_id(id); });

		auto var_attrs = read_var_attributes(i);

		VarInfoPtr var_ptr{ new VarInfo{
				var_name, to_netcdf_data_type(var_type), move(dims_info), move(var_attrs)} };

		vars_info_.emplace(var_name, std::make_tuple(var_ptr, i));
	}
}

void NetcdfFile::create_vars_info(const std::vector<VarInfoPtr>& vars_info)
{
	// No invariant check: called from ctor
	assert(!dims_info_.empty());
	assert(vars_info_.empty());

	for (const auto& vi : vars_info) {
		int var_id = -1;
		const auto dim_ids = transform_to_vector(vi->dims_info(), [this](const auto& di) {
			// Look for the dimension info pointer in the dimension info map, and retrieve the associated ID
			return get_dim_id_from_info(di);
		});
		CHECK_ERR( nc_def_var(file_id_, vi->name().c_str(), to_native_data_type(vi->data_type()), 
				static_cast<int>(dim_ids.size()), dim_ids.data(), &var_id) );
		vars_info_.emplace(vi->name(), std::make_tuple(vi, var_id));

		write_var_attributes(var_id, vi->attributes());
	}
}

std::vector<NetcdfFile::Attr> NetcdfFile::read_var_attributes(int var_id) const
{
	// No invariant check: called from ctor
	int attr_count = 0;
	CHECK_ERR(nc_inq_varnatts(file_id_, var_id, &attr_count));

	auto attrs = std::vector<Attr>{};	
	for (auto i : IndexRange{attr_count}) {
		
		char attr_name[NC_MAX_NAME + 1];
		CHECK_ERR(nc_inq_attname(file_id_, var_id, i, attr_name));

		nc_type attr_native_data_type = NC_NAT;
		size_t attr_value_count = 0;
		CHECK_ERR(nc_inq_att(file_id_, var_id, attr_name, &attr_native_data_type, &attr_value_count));

		const auto attr_data_type = to_netcdf_data_type(attr_native_data_type);

		auto attr_value = read_attr_value(var_id, attr_name, attr_data_type, attr_value_count);
		if (attr_value) {
			attrs.emplace_back(attr_name, attr_data_type, move(*attr_value));
		}
	}

	return attrs;
}

void NetcdfFile::write_var_attributes(int var_id, const std::vector<Attr>& attrs)
{
	// No invariant check: called from ctor
	for (const auto& a : attrs) {
		
		const auto dtype = to_native_data_type(a.data_type());

		switch (a.data_type()) {
		case DataType::float32: {
			const auto val = get<float>(a.value());
			CHECK_ERR(nc_put_att_float(file_id_, var_id, a.name().c_str(), dtype, 1, &val));
			break;
		}
		case DataType::float64: {
			const auto val = get<double>(a.value());
			CHECK_ERR(nc_put_att_double(file_id_, var_id, a.name().c_str(), dtype, 1, &val));
			break;
		}
		case DataType::int32: {
			const auto val = get<int>(a.value());
			CHECK_ERR(nc_put_att_int(file_id_, var_id, a.name().c_str(), dtype, 1, &val));
			break;
		}
		case DataType::chr: {
			const auto val = get<std::string>(a.value());
			CHECK_ERR(nc_put_att_text(file_id_, var_id, a.name().c_str(), val.size(), val.c_str()));
			break;
		}
		default: 
			assert(false);  // Unsupported attribute type
		}
	}
}

std::optional<NetcdfFile::Attr::Value> NetcdfFile::read_attr_value(int var_id, const char* attr_name, 
		DataType data_type, size_t value_count) const
{
	// No invariant check: called from ctor
	if (value_count != 1 && data_type != DataType::chr) {
		assert(false);  // Unsupported attribute type
		return {};
	} 

	std::optional<Attr::Value> attr_value;

	switch (data_type) {
	case DataType::chr: {
		std::vector<char> val(value_count + 1);
		CHECK_ERR(nc_get_att_text(file_id_, var_id, attr_name, val.data()));
		attr_value = val.data();
		break;
	}
	case DataType::float32: {
		auto val = 0.f;
		CHECK_ERR(nc_get_att_float(file_id_, var_id, attr_name, &val));
		attr_value = val;
		break;
	}
	case DataType::float64: {
		auto val = 0.0;
		CHECK_ERR(nc_get_att_double(file_id_, var_id, attr_name, &val));
		attr_value = val;
		break;
	}
	case DataType::int32: {
		int val = 0;
		CHECK_ERR(nc_get_att_int(file_id_, var_id, attr_name, &val));
		attr_value = val;
		break;
	}
	default: 
		assert(false);	// Unsupported attribute type
	}

	return attr_value;
}

int NetcdfFile::read_var_float32_data(int var_id, float* buffer) const
{
	TWIST_CHECK_INVARIANT
	return nc_get_var_float(file_id_, var_id, buffer);
}

int NetcdfFile::write_var_float32_data(int var_id, const float* buffer)
{
	TWIST_CHECK_INVARIANT
	return nc_put_var_float(file_id_, var_id, buffer);
}

int NetcdfFile::read_var_float64_data(int var_id, double* buffer) const
{
	TWIST_CHECK_INVARIANT
	return nc_get_var_double(file_id_, var_id, buffer);
}

int NetcdfFile::write_var_float64_data(int var_id, const double* buffer)
{
	TWIST_CHECK_INVARIANT
	return nc_put_var_double(file_id_, var_id, buffer);
}

int NetcdfFile::read_var_int32_data(int var_id, int* buffer) const
{
	TWIST_CHECK_INVARIANT
	return nc_get_var_int(file_id_, var_id, buffer);
}

int NetcdfFile::write_var_int32_data(int var_id, const int* buffer)
{
	TWIST_CHECK_INVARIANT
	return nc_put_var_int(file_id_, var_id, buffer);
}

int NetcdfFile::read_var_float32_hyperslab_data(int var_id, const size_t* dim_starts, 
		const size_t* dim_lengths, float* buffer) const
{
	TWIST_CHECK_INVARIANT
	return nc_get_vara_float(file_id_, var_id, dim_starts, dim_lengths, buffer);
}

int NetcdfFile::read_var_float64_hyperslab_data(int var_id, const size_t* dim_starts, 
		const size_t* dim_lengths, double* buffer) const
{
	TWIST_CHECK_INVARIANT
	return nc_get_vara_double(file_id_, var_id, dim_starts, dim_lengths, buffer);
}

auto NetcdfFile::read_var_int32_hyperslab_data(int var_id, const size_t* dim_starts, 
											   const size_t* dim_lengths, int* buffer) const -> int
{
	TWIST_CHECK_INVARIANT
	return nc_get_vara_int(file_id_, var_id, dim_starts, dim_lengths, buffer);
}

NetcdfFile::DimInfoPtr NetcdfFile::get_dim_info_from_id(int id) const
{
	// No invariant check: called from ctor
	auto it = find_if(dims_info_, [id](const auto& el) { 
		return get<tup_id>(el.second) == id; 
	});
	if (it == end(dims_info_)) {
		TWIST_THRO2(L"Dimension with ID {} does not exist.", id);
	}

	return get<tup_dim>(it->second);
}

int NetcdfFile::get_dim_id_from_info(const DimInfoPtr& dim_info) const 
{
	// No invariant check: called from ctor
	auto it = find_if(dims_info_, [&dim_info](const auto& el) { 
		return get<tup_dim>(el.second) == dim_info; 
	});
	if (it == end(dims_info_)) {
		TWIST_THRO2(L"Dimension \"{}\" could not be matched against the existing dimensions.", 
				    ansi_to_string(dim_info->name()));
	}
	return get<tup_id>(it->second);	
}

NetcdfFile::VarInfoPtr NetcdfFile::get_var_info_from_id(int id) const
{
	TWIST_CHECK_INVARIANT
	auto it = find_if(vars_info_, [id](const auto& el) { 
		return get<tup_id>(el.second) == id; 
	});
	if (it == end(vars_info_)) {
		TWIST_THRO2(L"Variable with ID {} does not exist.", id);
	}
	return get<tup_var>(it->second);
}

std::wstring NetcdfFile::get_native_err_msg(int native_err_no)
{
	return ansi_to_string(nc_strerror(native_err_no));
}

#ifdef _DEBUG
void NetcdfFile::check_invariant() const noexcept
{
	assert(file_id_ > 0);
	assert(dims_info_.size() > 0);
	assert(vars_info_.size() >= dims_info_.size());
}
#endif

}

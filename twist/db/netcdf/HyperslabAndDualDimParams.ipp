/// @file HyperslabAndDualDimParams.ipp
/// Inline implementation file for "HyperslabAndDualDimParams.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#define TWIST_CLASSTEMPL  template<class Ddim1Val, class Ddim2Val, class... SdimVals>
#define TWIST_CLASSNAME  HyperslabAndDualDimParams<Ddim1Val, Ddim2Val, SdimVals...>
#define TWIST_CLASSTYPENAME  typename TWIST_CLASSNAME

namespace twist::db::netcdf {

TWIST_CLASSTEMPL
std::string TWIST_CLASSNAME::dual_dim_dim1_name() const
{
	TWIST_CHECK_INVARIANT
	return ddim_val_ranges_.dual_dim_info()->dim1_info()->name();
}


TWIST_CLASSTEMPL
std::string TWIST_CLASSNAME::dual_dim_dim2_name() const
{
	TWIST_CHECK_INVARIANT
	return ddim_val_ranges_.dual_dim_info()->dim2_info()->name();
}


TWIST_CLASSTEMPL
std::string TWIST_CLASSNAME::dual_dim_var1_name() const
{
	TWIST_CHECK_INVARIANT
	return ddim_val_ranges_.dual_dim_info()->var1_name();
}


TWIST_CLASSTEMPL
std::string TWIST_CLASSNAME::dual_dim_var2_name() const
{
	TWIST_CHECK_INVARIANT
	return ddim_val_ranges_.dual_dim_info()->var2_name();
}


TWIST_CLASSTEMPL
TWIST_CLASSTYPENAME::DdimInfoPtr TWIST_CLASSNAME::dual_dim_info() const
{
	TWIST_CHECK_INVARIANT
	return ddim_val_ranges_.ddim_info();
}


TWIST_CLASSTEMPL
const TWIST_CLASSTYPENAME::DdimValRanges& TWIST_CLASSNAME::dual_dim_val_ranges() const
{
	TWIST_CHECK_INVARIANT
	return ddim_val_ranges_;
}


TWIST_CLASSTEMPL
bool TWIST_CLASSNAME::has_single_dims_params() const
{
	TWIST_CHECK_INVARIANT
	return sdims_params_.has_value();
}


TWIST_CLASSTEMPL
const TWIST_CLASSTYPENAME::SdimsParams& TWIST_CLASSNAME::single_dims_params() const
{
	TWIST_CHECK_INVARIANT
	if (!sdims_params_) {
		TWIST_THROW(L"These hyperslab parameters do not include parameters for any \"single dimensions\".");
	}
	return *sdims_params_;
}


#ifdef _DEBUG
TWIST_CLASSTEMPL
void TWIST_CLASSNAME::check_invariant() const noexcept
{
}
#endif

}

#undef  TWIST_CLASSTYPENAME
#undef  TWIST_CLASSNAME
#undef  TWIST_CLASSTEMPL


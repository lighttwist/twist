/// @file HyperslabAndDimParams.ipp
/// Inline implementation file for "HyperslabAndDimParams.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace twist::db::netcdf {

template<class... DimVals>
template<size_t DimIdx> 
const typename HyperslabAndDimParams<DimVals...>::template DimRangeType<DimIdx>& 
HyperslabAndDimParams<DimVals...>::dim_val_range() const
{
	TWIST_CHECK_INVARIANT
	return std::get<DimIdx>(dim_val_ranges_);
}


template<class... DimVals>
const typename HyperslabAndDimParams<DimVals...>::DimRangeTuple& 
HyperslabAndDimParams<DimVals...>::dim_val_ranges() const
{
	TWIST_CHECK_INVARIANT
	return dim_val_ranges_;
}


#ifdef _DEBUG
template<class... DimVals>
void HyperslabAndDimParams<DimVals...>::check_invariant() const noexcept
{
}
#endif

}



/// @file HyperslabAndDimData.hpp
/// HyperslabAndDimData class template definition

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_DB_NETCDF_HYPERSLAB_AND_DIM_DATA_HPP
#define TWIST_DB_NETCDF_HYPERSLAB_AND_DIM_DATA_HPP

#include "twist/metaprogramming.hpp"

#include "twist/db/netcdf/DimensionData.hpp"
#include "twist/db/netcdf/HyperslabDataBase.hpp"

namespace twist::db::netcdf {

///
///  Class which stores information about a "hyperslab", which uses a number of "single dimensions", and the 
///	   hyperslab data together with the data for each dimension used. 
///  A hyperslab is a multidimensional chunk of data, representing a specific variable's data, which (in this 
///    class) uses "single dimensions", as opposed to "dual dimensions" (see DualDimensionInfo class 
///	   for details about "dual dimensions"). We refer here to the variable as the "hyperslab variable". 
///
///  @tparam  VarVal  The value type of the "hyperslab variable" data 
///  @tparam  DimVals...  The value type of each of the dimension's data, ie the data types of the variables 
///					associated with each of the "single dimensions" used by the hyperslab; the ordering of the
///					template arguments making up this variadic parameter, together with the constructor 
///					arguments, establish the order of the dimensions for this class
///
template<class VarVal, class... DimVals>
class HyperslabAndDimData : public HyperslabDataBase<VarVal> {
public:
	using DimInfoPtr = std::shared_ptr<DimensionInfo>;
	using VarInfoPtr = std::shared_ptr<VariableInfo>;

	template<class DimVal> using DimDataPtr = std::shared_ptr<DimensionData<DimVal>>;

	using DimDataPtrTuple = std::tuple<DimDataPtr<DimVals>...>;

	template<size_t DimIdx> using DimDataPtrType = std::tuple_element_t<DimIdx, DimDataPtrTuple>;

	/// The number of dimensions used by the hyperslab
	static constexpr size_t dim_count = sizeof...(DimVals);
	
	/// Constructor. 
	///
	/// @param[in] var_info  The "hyperlab variable" info
	/// @param[in] var_data  The "hyperlab variable" data
	/// @param[in] dims_data  A list of "dimension data" objects, one for each dimension used by 
	///					the hyperslab
	///
	HyperslabAndDimData(VarInfoPtr var_info, std::vector<VarVal>&& var_data, 
			DimDataPtr<DimVals>... dims_data)
		: HyperslabDataBase<VarVal>{ var_info, move(var_data) }
		, dims_data_{ dims_data... }
	{	// Visual Studio 2017 from version 15.9.3 onwards seems to dislike the separate definition of 
		// method templates with variadic arguments
		TWIST_CHECK_INVARIANT
	}

	/// Get a list of dimension information objects, one for each of the dimensions used by the hyperslab.
	///
	/// @return  List of dimension information objects; the list follows the dimension ordering established by 
	///				the constructor
	///
	std::vector<DimInfoPtr> dims_info();

	/// Get the dimension data for one of the dimensions used by the hyperslab.
	///
	/// @tparam  DimIdx  The index of the dimension, in the dimension ordering established by the constructor
	/// @return  The dimension data
	///
	template<size_t DimIdx> DimDataPtrType<DimIdx> dim_data() const;

	/// Get a reference to a tuple containing dimension data objects, one for each of the dimensions used by 
	/// the hyperslab. The elements of the tuple follow the dimension ordering established by the constructor.
	///
	/// @return  Reference to the dimension data tuple
	///
	const DimDataPtrTuple& dims_data() const;

	/// Get variable information for all the variables associated with the hyperslab, that is the "hyperslab 
	/// variable" (which appears first in the returned list) and the variable associated to each dimension 
	/// used by the hyperslab (which appear in the dimension order established by the class variadic template 
	/// parameter).
	///
	/// @return  The variables information
	///
	std::vector<VarInfoPtr> all_vars_info() const;  

	TWIST_NO_COPY_NO_MOVE(HyperslabAndDimData)

private:
	DimDataPtrTuple  dims_data_;

	TWIST_CHECK_INVARIANT_DECL
};

}

#include "HyperslabAndDimData.ipp"

#endif


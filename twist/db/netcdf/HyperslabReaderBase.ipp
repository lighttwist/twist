/// @file HyperslabReaderBase.ipp
/// Inline implementation file for "HyperslabReaderBase.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/db/netcdf/NetcdfFile.hpp"

namespace twist::db::netcdf {

template<class DimVal> 
HyperslabReaderBase::DimDataPtr<DimVal> HyperslabReaderBase::make_hyslab_sdim_data(
		const DimValRange<DimVal>& dim_val_range, const VarInfo& var_info, 
		std::vector<DimInfoPtr>& hyslab_dims_info, std::vector<size_t>& dim_start_positions, 
		std::vector<size_t>& dim_lengths) const
{
	TWIST_CHECK_INVARIANT
	assert(hyslab_dims_info.size() == dim_start_positions.size() && 
			hyslab_dims_info.size() == dim_lengths.size());

	auto [hyslab_sdim_data, file_sdim_pos_range] = calc_hyslab_dim_data(dim_val_range);
			
	const auto dim_idx = get_dim_index(var_info, dim_val_range.dim_info()->name());
	assert(dim_idx < hyslab_dims_info.size());

	assert(!hyslab_dims_info[dim_idx]);
	hyslab_dims_info[dim_idx] = hyslab_sdim_data->dim_info();

	const auto pos_range = file_sdim_pos_range.pos_range();

	assert(dim_start_positions[dim_idx] == netcdf_no_idx);
	dim_start_positions[dim_idx] = pos_range.lo();

	assert(dim_lengths[dim_idx] == netcdf_no_idx);
	dim_lengths[dim_idx] = static_cast<size_t>(count_elements(pos_range));

	return hyslab_sdim_data; 
}

template<class DimVal> 
std::tuple<HyperslabReaderBase::DimDataPtr<DimVal>, HyperslabReaderBase::DimPosRange> 
HyperslabReaderBase::calc_hyslab_dim_data(const DimValRange<DimVal>& dim_val_range) const
{
	TWIST_CHECK_INVARIANT
	const auto file_dim_data = file_->read_dim_data<DimVal>(dim_val_range.dim_info()->name());

	const auto file_pos_range_for_val_range = get_bounded_pos_range<size_t>(file_dim_data->data(), 
	                                                                        dim_val_range.val_range());
	if (!file_pos_range_for_val_range) {
		TWIST_THROW(L"The range provided for dimension \"%\" does not intersect the dimension data.", 
				    dim_val_range.dim_info()->name().c_str());
	}

	auto file_dim_pos_range = DimensionPosRange{file_dim_data->dim_info(), *file_pos_range_for_val_range};

	return {make_hyslab_dim_data(*file_dim_data, file_dim_pos_range), file_dim_pos_range};
}


template<class DimVal>
HyperslabReaderBase::DimDataPtr<DimVal> HyperslabReaderBase::make_hyslab_dim_data(
		const DimensionData<DimVal>& file_dim_data, const DimPosRange& file_dim_pos_range) const
{
	TWIST_CHECK_INVARIANT
	const auto dim_length = static_cast<size_t>(count_elements(file_dim_pos_range.pos_range()));
	auto hyslab_dim_info = NetcdfFile::make_dim_info(file_dim_data.dim_info()->name(), dim_length);

	const auto file_var_info = file_dim_data.var_info();
	auto hyslab_var_info = std::make_shared<VariableInfo>(
			file_var_info->name(), file_var_info->data_type(),  
			std::vector<DimInfoPtr>{hyslab_dim_info}, file_var_info->attributes() );  

	auto hyslab_dim_data_buffer = transform_to_vector(
			enumerate_elements(file_dim_pos_range.pos_range()), [&file_dim_data](auto pos) {
		return file_dim_data.data().at(pos);
	});

	return NetcdfFile::make_dim_data<DimVal>( 
			hyslab_dim_info, hyslab_var_info, move(hyslab_dim_data_buffer));
}

template<class Dim1Val, class Dim2Val>		
HyperslabReaderBase::DdimDataPtr<Dim1Val, Dim2Val> HyperslabReaderBase::make_dual_dim_data(
		DdimInfoPtr ddim_info, VarInfoPtr var1_info, VarInfoPtr var2_info, 
		std::vector<Dim1Val>&& data1, std::vector<Dim2Val>&& data2)
{
	return NetcdfFile::make_dual_dim_data<Dim1Val, Dim2Val>(
			ddim_info, var1_info, var2_info, move(data1), move(data2));
}

template<class VarVal>
auto HyperslabReaderBase::read_var_hyperslab_data(int var_id, 
		                                          const std::vector<size_t>& dim_start_positions, 
												  const std::vector<size_t>& dim_lengths) const -> std::vector<VarVal>
{
	const auto hyslab_data_length = accumulate(dim_lengths, size_t{1}, std::multiplies<size_t>{});

	auto hyslab_var_data = std::vector<VarVal>(hyslab_data_length); 
	file().read_var_hyperslab_data(var_id, dim_start_positions, dim_lengths, hyslab_var_data);

	return hyslab_var_data;
}

}

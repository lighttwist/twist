///  @file  SqlitePreparedStatement.hpp
///  SqlitePreparedStatement  class

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_DB_SQLITE_PREPARED_STATEMENT_HPP
#define TWIST_DB_SQLITE_PREPARED_STATEMENT_HPP

#include "twist/db/sqlite_utils.hpp"

namespace twist::db {

///
///  Class which represents a single SQL statement that has been compiled into binary form and is ready to 
///  be evaluated.
///
class SqlitePreparedStatement {
public:
	/// Destructor.
	~SqlitePreparedStatement();

	/// Get the name of a specific column appearing in the statement.
	///
	/// @param[in] col_idx  The index of the column within the statement
	///
	std::string get_column_name(int col_idx) const;

	/// Set the text value of an "SQL parameter" existing in the statement; the text is assumed to be using 
	/// the UTF-8 character encoding.
	///
	/// @param[in] param_idx  The index of the SQL parameter to be set; the leftmost SQL parameter has an 
	///					index of 1
	/// @param[in] val  Pointer to the text value
	/// @param[in] val_storage  The type of storage of the value being bound; for static storage, the value 
	///					pointee must be alive when step() is called
	///
	void bind_text(int param_idx, const char* val, 
			SqliteBoundValueStorage val_storage = SqliteBoundValueStorage::statiq);

	/// Set the text value of an "SQL parameter" existing in a compiled ("prepared") statement; the text is 
	///   assumed to be using the UTF-16 character encoding.
	/// Devnote: The function has not been tested for exotic character sets.
	///
	/// @param[in] param_idx  The index of the SQL parameter to be set; the leftmost SQL parameter has an 
	///					index of 1
	/// @param[in] val  Pointer to the text value
	/// @param[in] val_storage  The type of storage of the value being bound; for static storage, the value 
	///					pointee must be alive when step() is called
	///
	void bind_text16(int param_idx, const wchar_t* val,
			SqliteBoundValueStorage val_storage = SqliteBoundValueStorage::statiq);

	/// Set the integer number value of an "SQL parameter" existing in the statement.
	///
	/// @param[in] param_idx  The index of the SQL parameter to be set; the leftmost SQL parameter has an 
	///					index of 1
	/// @param[in] val  The integer value
	///
	void bind_integer(int param_idx, int val);

	/// Set the 64-bit integer number value of an "SQL parameter" existing in the statement.
	///
	/// @param[in] param_idx  The index of the SQL parameter to be set; the leftmost SQL parameter has an 
	///					index of 1
	/// @param[in] val  The 64-bit integer value
	///
	void bind_int64(int param_idx, int64_t val);

	/// Set the 64-bit floating-point number value of an "SQL parameter" existing in the statement.
	///
	/// @param[in] param_idx  The index of the SQL parameter to be set; the leftmost SQL parameter has an 
	///					index of 1
	/// @param[in] val  The real value
	///
	void bind_real(int param_idx, double val);

	/// Set the BLOB value of an "SQL parameter" existing in the statement.
	///
	/// @param[in] param_idx  The index of the SQL parameter to be set; the leftmost SQL parameter has an 
	///					index of 1
	/// @param[in] blob  The BLOB value: the start of the binary data buffer
	/// @param[in] blob_size  The BLOB size: the size (in bytes) of the binary data buffer
	///
	void bind_blob(int param_idx, const void* blob, size_t blob_size);

	//+TODO: Comment
	void bind_blob(int param_idx, const void* blob, size_t blob_size, SqliteBoundValueStorage val_storage);

	/// Set the BLOB value of an "SQL parameter" existing in the statement to an array.
	///
	/// @tparam  Cont  The type of the STL-like container which holds the input array
	/// @param[in] param_idx  The index of the SQL parameter to be set; the leftmost SQL parameter has an 
	///					index of 1
	/// @param[in] cont  Pointer to the container holding the input array; the pointee must be alive when 
	///					step() is called
	///
	template<class Cont> void bind_array_blob(int param_idx, const Cont* cont);

	/// Set the BLOB value of an "SQL parameter" existing in the statement to a compressed array.
	/// The input, uncompressed array is first compressed and then bound to the parameter; if compression 
	/// fails, then the uncompressed array is bound to the parameter.
	///
	/// @tparam  Cont  The type of the STL-like container which holds the input, uncompressed array
	/// @param[in] param_idx  The index of the SQL parameter to be set; the leftmost SQL parameter has an 
	///					index of 1
	/// @param[in] cont  Pointer to the container holding the input, uncompressed array; the pointee must be 
	///					alive when step() is called
	/// @param[in] compressed_buf  Pointer to the container which will hold the compressed array; it will be 
	///					resized if needed; the pointee must be alive when step() is called
	/// @param[in] compression  The type of compression to be used   
	/// @return  true if compression was successful and the compressed array was bound to the parameter; 
	///					false if compression failed and the uncompressed array was bound to the parameter
	///
	template<class Cont> 
		bool bind_compress_array_blob(int param_idx, const Cont* cont, 
				std::vector<unsigned char>* compressed_buf, 
				SqliteBlobCompression compression = SqliteBlobCompression::zlib);

	/// Reset the statement back to its initial state, ready to be re-executed.  
	void reset();

	TWIST_NO_COPY_NO_MOVE(SqlitePreparedStatement)

private:
	class Impl;

	SqlitePreparedStatement(std::unique_ptr<Impl> pimpl);

	std::unique_ptr<Impl>  pimpl_;

	friend class SqliteConnection;

	TWIST_CHECK_INVARIANT_DECL
};

}

#include "twist/db/SqlitePreparedStatement.ipp"

#endif 

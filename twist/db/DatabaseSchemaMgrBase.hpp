///  @file  DatabaseSchemaMgrBase.hpp
///  DatabaseSchemaMgrBase abstract class definition

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_DB_DATABASE_SCHEMA_MGR_BASE_HPP
#define TWIST_DB_DATABASE_SCHEMA_MGR_BASE_HPP

namespace twist::db {

///
///  Generic, abstract base class for classes which manage the schema (structure) of a database based on a 
///    single file. 
///  A schema manager class keeps track of the database version information and reorganises older databases to 
///    the current version.
///
class DatabaseSchemaMgrBase : public NonCopyable {
public:
	/// Destructor.
	///
	virtual ~DatabaseSchemaMgrBase() = 0;

    /// Read the version number from the database.
    ///
	/// @return  The version number
	///
	virtual unsigned int get_db_version() = 0;

    /// Check if the database has the current (newest) version.
	/// An exception is thrown if the database file does not exist yet.
    ///
	/// @param[in] special_reorganise_msg  If the database is not up-to-date, this is an (optional) special 
	///					message related to reorganising it from its current version to the current version; 
	///					it can be blank
    /// @return  true if the database has the current version number.
    ///
    virtual bool is_db_version_up_to_date(std::wstring& special_reorganise_msg) = 0;    

protected:
	/// Constructor.
	///
    /// @param[in] path  The database file path
	/// @param[in] cur_db_version  The current (newest) database version number
	/// @param[in] oldest_supported_db_version  The oldest supposed database version number
	///
	DatabaseSchemaMgrBase(fs::path path, unsigned int cur_db_version, 
			unsigned int oldest_supported_db_version);

	/// Get the database filename
	///
	/// @return  The filename
	///
	fs::path path() const;

	/// Get The current (newest) database version number.
	///
	/// @return  The current database version number
	///
	unsigned int get_cur_db_version() const;

	/// Get the oldest supposed database version number.
	///
	/// @return  The oldest supposed database version number
	///
	unsigned int get_oldest_supported_db_version() const;

	/// If the database is not up-to-date, this method returns an (optional) message related to reorganising it from its current 
	/// version to the current version. This implementation returns a blank string.
	///
	/// @param[in] old_version  The current database version, from which it would be reorganised 
	/// @return  The reorganise message
	///
	virtual std::wstring get_special_reorganise_message(unsigned int old_version) const;

private:
    fs::path  path_;
	unsigned int  cur_db_version_;
	unsigned int  oldest_supported_db_version_;

	TWIST_CHECK_INVARIANT_DECL
};

}

#endif 

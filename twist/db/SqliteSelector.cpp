/// @file SqliteSelector.cpp
/// Implementation file for "SqliteSelector.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/db/SqliteSelector.hpp"

#include "twist/db/sql_assembler.hpp"
#include "twist/db/SqlitePreparedStatement.hpp"

#include "twist/DateTime.hpp"

namespace twist::db {

// --- SqliteSelector class ---

SqliteSelector::SqliteSelector(SqliteConnection& conn, const std::string& sql, bool null_text_as_empty)
	: conn_(conn)
	, null_text_as_empty_(null_text_as_empty)
    , col_indexer_()
{
	// Prepare (compile) the SQL statement
	statement_ = conn_.prepare(sql);

	// Retrieve the column names in the compiled statement
	std::vector<std::string> column_names;
	const unsigned int num_cols = conn_.column_count(*statement_);
	for (auto i = 0u; i < num_cols; ++i) {
		column_names.push_back(statement_->get_column_name(i));
	}

	// Initialise the column indexer
	col_indexer_ = std::make_unique<DatabaseColumnIndexer>(column_names);
	TWIST_CHECK_INVARIANT
}


SqliteSelector::~SqliteSelector()
{
	TWIST_CHECK_INVARIANT
}


bool SqliteSelector::select()
{
	TWIST_CHECK_INVARIANT
	const auto result = conn_.step(*statement_);
	TWIST_CHECK_INVARIANT
	return result == SqliteStepResult::row;
}


std::wstring SqliteSelector::get_text(std::string_view col_name) const
{
	TWIST_CHECK_INVARIANT
	return get_text(col_indexer_->get_col_idx(col_name));
}


std::wstring SqliteSelector::get_text(unsigned int col_idx) const
{
	TWIST_CHECK_INVARIANT

#if TWIST_OS_WIN

	return conn_.column_text16(*statement_, col_idx, null_text_as_empty_);

#elif TWIST_OS_LINUX

	size_t blob_size{};
	auto blob = get_blob(col_idx, blob_size);
	if (blob_size % sizeof(wchar_t) != 0) {
		TWIST_THROW(L"Database corruption in column %d: wrong field size for string character type.", 
				col_idx);
	}
	return std::wstring( static_cast<const wchar_t*>(blob), blob_size / sizeof(wchar_t) );

#endif
}


std::string SqliteSelector::get_ansi(std::string_view col_name) const
{
	TWIST_CHECK_INVARIANT
	return get_ansi(col_indexer_->get_col_idx(col_name));
}


std::string SqliteSelector::get_ansi(unsigned int col_idx) const
{
	TWIST_CHECK_INVARIANT
	return conn_.column_text(*statement_, col_idx, null_text_as_empty_);
}


int SqliteSelector::get_int(std::string_view col_name) const
{
	TWIST_CHECK_INVARIANT
	return get_int(col_indexer_->get_col_idx(col_name));
}


int SqliteSelector::get_int(unsigned int col_idx) const
{
	TWIST_CHECK_INVARIANT
	return conn_.column_int(*statement_, col_idx);
}


std::int64_t SqliteSelector::get_int64(std::string_view col_name) const
{
	TWIST_CHECK_INVARIANT
	return get_int64(col_indexer_->get_col_idx(col_name));
}


std::int64_t SqliteSelector::get_int64(unsigned int col_idx) const
{
	TWIST_CHECK_INVARIANT
	return conn_.column_int64(*statement_, col_idx);
}


double SqliteSelector::get_real(std::string_view col_name) const
{
	TWIST_CHECK_INVARIANT
	return get_real(col_indexer_->get_col_idx(col_name));
}


double SqliteSelector::get_real(unsigned int col_idx) const
{
	TWIST_CHECK_INVARIANT
	return conn_.column_real(*statement_, col_idx);
}


bool SqliteSelector::get_bool(std::string_view col_name) const
{
	TWIST_CHECK_INVARIANT
	return get_bool(col_indexer_->get_col_idx(col_name));
}


bool SqliteSelector::get_bool(unsigned int col_idx) const
{
	TWIST_CHECK_INVARIANT
	const auto val = get_ansi(col_idx);
	if (val == "0") {
		return false;
	}
	if (val == "1") {
		return true;
	}
	TWIST_THROW(L"The record cell %d (column \"%s\") does not store a BOOLEAN value.", 
			col_idx, col_indexer_->try_get_col_wname(col_idx).c_str());
}

auto SqliteSelector::get_date(std::string_view col_name) const -> Date
{
	TWIST_CHECK_INVARIANT
	return get_date(col_indexer_->get_col_idx(col_name));
}

auto SqliteSelector::get_date(unsigned int col_idx) const -> Date
{
	TWIST_CHECK_INVARIANT
	return date_from_str(get_text(col_idx), DateStrFormat::yyyy_mm_dd, L"-");
}

auto SqliteSelector::get_datetime(std::string_view col_name) const -> DateTime
{
	TWIST_CHECK_INVARIANT
	return get_datetime(col_indexer_->get_col_idx(col_name));
}

auto SqliteSelector::get_datetime(unsigned int col_idx) const -> DateTime
{
	TWIST_CHECK_INVARIANT
	return datetime_from_str(get_text(col_idx), DateTimeStrFormat::yyyy_mm_dd_hh_mm_ss, L"-", L" ", L":");
}

const void* SqliteSelector::get_blob(std::string_view col_name, size_t& blob_size) const
{
	TWIST_CHECK_INVARIANT
	return get_blob(col_indexer_->get_col_idx(col_name), blob_size);
}


const void* SqliteSelector::get_blob(unsigned int col_idx, size_t& blob_size) const
{
	TWIST_CHECK_INVARIANT
	blob_size = conn_.column_bytes(*statement_, col_idx);
	return conn_.column_blob(*statement_, col_idx);
}


bool SqliteSelector::is_null(std::string_view col_name) const
{
	TWIST_CHECK_INVARIANT
	return is_null(col_indexer_->get_col_idx(col_name));
}


bool SqliteSelector::is_null(unsigned int col_idx) const
{
	TWIST_CHECK_INVARIANT
	return conn_.column_is_null(*statement_, col_idx);
}


size_t SqliteSelector::count_bytes(std::string_view col_name) const
{
	TWIST_CHECK_INVARIANT
	return count_bytes(col_indexer_->get_col_idx(col_name));
}


size_t SqliteSelector::count_bytes(unsigned int col_idx) const
{
	TWIST_CHECK_INVARIANT
	return conn_.column_bytes(*statement_, col_idx);
}


#ifdef _DEBUG
void SqliteSelector::check_invariant() const noexcept
{
	assert(statement_ != nullptr);
	assert(col_indexer_);
}
#endif 

// --- Free functions ---

auto get_nullable_real(const SqliteSelector& sel, std::string_view col_name) -> std::optional<double>
{
	return !sel.is_null(col_name) ? std::make_optional(sel.get_real(col_name)) : std::nullopt;
}

auto get_nullable_date(const SqliteSelector& sel, std::string_view col_name) -> std::optional<Date>
{
	return !sel.is_null(col_name) ? std::make_optional(sel.get_date(col_name)) : std::nullopt;
}

auto get_nullable_datetime(const SqliteSelector& sel, std::string_view col_name) -> std::optional<DateTime>
{
	return !sel.is_null(col_name) ? std::make_optional(sel.get_datetime(col_name)) : std::nullopt;
}

} 

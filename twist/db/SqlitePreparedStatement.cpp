///  @file  SqlitePreparedStatement.cpp
///  Implementation file for "SqlitePreparedStatement.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "SqlitePreparedStatement.hpp"

#include "sqlite_internals.hpp"

#define TWIST_DB_CHECK_SQLITE3_RESULT_LOCAL(sqlite_expression)  \
			TWIST_DB_CHECK_SQLITE3_RESULT(sqlite_expression, pimpl_->conn());

namespace twist::db {

//
//  Local functions
//

/// Get the Sqlite destructor type corresponding to a specific Sqlite bound value storage type.
///
/// @param[in] val_storage  The storage type; an exception is thrown if the type is invalid
/// @return  The corresponding destructor type
///
sqlite3_destructor_type get_destructor_type(SqliteBoundValueStorage val_storage)
{
	switch (val_storage) {
		case SqliteBoundValueStorage::statiq    : return SQLITE_STATIC;
		case SqliteBoundValueStorage::transient : return SQLITE_TRANSIENT;
		default : TWIST_THROW(L"Invalid Sqlite bound value storage type %d.", val_storage);
	}
}

//
//  SqlitePreparedStatement class
//

SqlitePreparedStatement::SqlitePreparedStatement(std::unique_ptr<Impl> pimpl)
	: pimpl_{move(pimpl)}
{
	TWIST_CHECK_INVARIANT
}


SqlitePreparedStatement::~SqlitePreparedStatement()
{
	TWIST_CHECK_INVARIANT_NO_SCOPE
	// Finalise the virtual machine into which the SQL statement has been compiled
	[[maybe_unused]] int result = sqlite3_finalize(pimpl_->stmt());
	assert(result == SQLITE_OK);  // We do not throw in the destructor 
}


std::string SqlitePreparedStatement::get_column_name(int col_idx) const
{
	TWIST_CHECK_INVARIANT
	return sqlite3_column_name(pimpl_->stmt(), col_idx);
}


void SqlitePreparedStatement::bind_text(int param_idx, const char* val, SqliteBoundValueStorage val_storage)
{
	TWIST_CHECK_INVARIANT
	TWIST_DB_CHECK_SQLITE3_RESULT_LOCAL( sqlite3_bind_text(
			pimpl_->stmt(), param_idx, val, -1, get_destructor_type(val_storage)) );
}


void SqlitePreparedStatement::bind_text16(int param_idx, const wchar_t* val, 
		SqliteBoundValueStorage val_storage)
{
	TWIST_CHECK_INVARIANT
	TWIST_DB_CHECK_SQLITE3_RESULT_LOCAL( sqlite3_bind_text16(
			pimpl_->stmt(), param_idx, val, -1, get_destructor_type(val_storage)) );
}


void SqlitePreparedStatement::bind_integer(int param_idx, int val)
{
	TWIST_CHECK_INVARIANT
	TWIST_DB_CHECK_SQLITE3_RESULT_LOCAL( sqlite3_bind_int(pimpl_->stmt(), param_idx, val) );
}


void SqlitePreparedStatement::bind_int64(int param_idx, int64_t val)
{
	TWIST_CHECK_INVARIANT
	TWIST_DB_CHECK_SQLITE3_RESULT_LOCAL( sqlite3_bind_int64(pimpl_->stmt(), param_idx, val) );
}


void SqlitePreparedStatement::bind_real(int param_idx, double val)
{
	TWIST_CHECK_INVARIANT
	TWIST_DB_CHECK_SQLITE3_RESULT_LOCAL( sqlite3_bind_double(pimpl_->stmt(), param_idx, val) );
}


void SqlitePreparedStatement::bind_blob(int param_idx, const void* blob, size_t blob_size)
{
	TWIST_CHECK_INVARIANT
	TWIST_DB_CHECK_SQLITE3_RESULT_LOCAL( sqlite3_bind_blob(
			pimpl_->stmt(), param_idx, blob, static_cast<int>(blob_size), SQLITE_STATIC) );
}


void SqlitePreparedStatement::bind_blob(int param_idx, const void* blob, size_t blob_size, 
		SqliteBoundValueStorage val_storage)
{
	TWIST_CHECK_INVARIANT
	TWIST_DB_CHECK_SQLITE3_RESULT_LOCAL( sqlite3_bind_blob(pimpl_->stmt(), param_idx, blob, 
			static_cast<int>(blob_size), get_destructor_type(val_storage)) );
}


void SqlitePreparedStatement::reset()  
{
	TWIST_CHECK_INVARIANT
	TWIST_DB_CHECK_SQLITE3_RESULT_LOCAL( sqlite3_reset(pimpl_->stmt()) );
}


#ifdef _DEBUG
void SqlitePreparedStatement::check_invariant() const noexcept
{
	assert(pimpl_);
}
#endif 

} 


///  @file  XmlDocSchemaMgr.hpp
///  XmlDocSchemaMgr  abstract class

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_XML_DOC_SCHEMA_MGR_HPP
#define TWIST_XML_DOC_SCHEMA_MGR_HPP

namespace twist::db {
class XmlDoc;
}

namespace twist::db {

/*! Abstract base class for classes which manage the schema (structure) of an XML document. The root XML node is 
    expected to have a document version attribute, named "version".
 */
class XmlDocSchemaMgr {
public:
	virtual ~XmlDocSchemaMgr() = 0;

	/*! Reorganise, if necessary, the XML document.
        \param[out] old_version  The version that the database had before reorganising took place (if it did)
		\return  true if reorganisation took place
	 */
	auto reorganise(unsigned int& old_version) -> bool;

	TWIST_NO_COPY_NO_MOVE(XmlDocSchemaMgr)

protected:
	/*! Constructor.
		\param[in] xdoc  The XML document
		\param[in] root_xnode_tag  The tag of the root XML node
		\param[in] cur_doc_version  The current (newest) XML document version number
		\param[in] oldest_supported_doc_version  The oldest supposed XML document version number
	 */
	XmlDocSchemaMgr(XmlDoc& xdoc, 
	                std::wstring root_xnode_tag, 
					unsigned int cur_doc_version,
			        unsigned int oldest_supported_doc_version);

private:	
    /*! Reorganise the XML document from version k-1 to version k.
	    This implementation throws an exception (so derived classes needn't override it if no reorganising needs ever 
		take place).
		\param[in] version  The version k to which the database is to be reorganised
		\param[in] xdoc  The XML document
     */
    virtual auto reorganise_doc_to_version(unsigned int version, XmlDoc& xdoc) -> void;
	    
	XmlDoc& xdoc_;	
	const std::wstring root_xnode_tag_;
	const unsigned int cur_doc_version_;
	unsigned int oldest_supported_doc_version_;

	TWIST_CHECK_INVARIANT_DECL
};

}

#endif 

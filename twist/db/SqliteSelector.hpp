/// @file SqliteSelector.hpp
/// SqliteSelector class definition and declarations of the free functions which are part of the class interface

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_DB_SQLITE_SELECTOR_HPP
#define TWIST_DB_SQLITE_SELECTOR_HPP

#include "twist/db/DatabaseColumnIndexer.hpp"
#include "twist/db/sqlite_utils.hpp"
#include "twist/db/SqliteConnection.hpp"

namespace twist { 
class DateTime;
}
namespace twist::db {
class SqlitePreparedStatement;
}

namespace twist::db {

///
///  Class managing repeated data extractions from a recordset obtained by executing a pre-compiled SQL SELECT 
///  query an Sqlite database.
///
class SqliteSelector {
public:
	/// Constructor.
	///
	/// @param[in] conn  The database connection
	/// @param[in] sql  The SQL SELECT query to be compiled 
	/// @param[in] null_text_as_empty  if true, retrieving a NULL value as text will return an empty string; 
	///					if false, it will throw an exception
	///
	SqliteSelector(SqliteConnection& conn, const std::string& sql, bool null_text_as_empty = true);

	/// Destructor.
	~SqliteSelector();

	/// Select the next available record (if one is available); all the desired cell values must be read after 
	/// this call and before the next call to select().
	///
	/// @return  true if a new record has been successfully selected; false if the end of the recordset has 
	///					been reached
	///
	bool select();

	/// Get the text value stored in a specific cell.
	/// Devnote:  Function untested for exotic character sets
	///
	/// @param[in] col_name  The name of the column corresponding to the cell; an exception is thrown if it 
	///					is invalid
	/// @return  Pointer to the text value; the pointee must be alive when insert() is called!
	///
	std::wstring get_text(std::string_view col_name) const;
	
	/// Get the text value stored in a specific cell.
	///+FUNCTION UNTESTED FOR EXOTIC CHARACTER SETS
	///
	/// @param[in] col_idx  The index of the column corresponding to the cell; an exception is thrown if it 
	///					is invalid
	/// @return  Pointer to the text value; the pointee must be alive when insert() is called!
	///
	std::wstring get_text(unsigned int col_idx) const;

	/// Get the ANSI text value stored in a specific cell.
	///
	/// @param[in] col_name  The name of the column corresponding to the cell; an exception is thrown if it is invalid
	/// @return  Pointer to the text value; the pointee must be alive when insert() is called!
	///
	std::string get_ansi(std::string_view col_name) const;
	
	/// Get the ANSI text value stored in a specific cell.
	///
	/// @param[in] col_idx  The index of the column corresponding to the cell; an exception is thrown if it is invalid
	/// @return  Pointer to the text value; the pointee must be alive when insert() is called!
	///
	std::string get_ansi(unsigned int col_idx) const;
	
	/// Get the integer number value stored in a specific cell.
	///
	/// @param[in] col_name  The name of the column corresponding to the cell; an exception is thrown if it 
	///					is invalid
	/// @return  The integer number value
	///
	int get_int(std::string_view col_name) const;

	/// Get the integer number value stored in a specific cell.
	///
	/// @param[in] col_idx  The index of the column corresponding to the cell; an exception is thrown if it 
	///					is invalid
	/// @return  The integer number value
	///
	int get_int(unsigned int col_idx) const;
	
	/// Get the 64-bit integer number value stored in a specific cell.
	///
	/// @param[in] col_name  The name of the column corresponding to the cell; an exception is thrown if it 
	///					is invalid
	/// @return  The 64-bit integer number value
	///
	std::int64_t get_int64(std::string_view col_name) const;

	/// Get the 64-bit integer number value stored in a specific cell.
	///
	/// @param[in] col_idx  The index of the column corresponding to the cell; an exception is thrown if it 
	///					is invalid
	/// @return  The 64-bit integer number value
	///
	std::int64_t get_int64(unsigned int col_idx) const;
	
	/*! Get the integer number value stored in a specific cell as an enum value.
	    \tparam Enum  The enumeration type
	    \param[in] col_idx  The index of the column corresponding to the cell; an exception is thrown if it is invalid
	    \return  The enum value
	 */
	template<class Enum,
	         class = EnableIfEnum<Enum>>
	[[nodiscard]] auto get_enum_int(unsigned int col_idx) const -> Enum;
	
	/*! Get the integer number value stored in a specific cell as an enum value.
	    \tparam Enum  The enumeration type
	    \param[in] col_name  The name of the column corresponding to the cell; an exception is thrown if it is invalid
	    \return  The enum value
	 */
	template<class Enum,
	         class = EnableIfEnum<Enum>>
	[[nodiscard]] auto get_enum_int(std::string_view col_name) const -> Enum;
		
	/// Get the floating-point number value stored in a specific cell.
	///
	/// @param[in] col_name  The name of the column corresponding to the cell; an exception is thrown if it is invalid
	/// @return  The floating-point number value
	///
	double get_real(std::string_view col_name) const;
	
	/// Get the floating-point number value stored in a specific cell.
	///
	/// @param[in] col_idx  The index of the column corresponding to the cell; an exception is thrown if it is invalid
	/// @return  The floating-point number value
	///
	double get_real(unsigned int col_idx) const;
	
	/// Get the boolean value stored in a specific cell. An exception is thrown if the cell does not store a value convertible 
	/// to boolean.
	///
	/// @param[in] col_name  The name of the column corresponding to the cell; an exception is thrown if it is invalid
	/// @return  The boolean value
	///
	bool get_bool(std::string_view col_name) const;
	
	/// Get the boolean value stored in a specific cell. An exception is thrown if the cell does not store a value convertible 
	/// to boolean.
	///
	/// @param[in] col_idx  The index of the column corresponding to the cell; an exception is thrown if it is invalid
	/// @return  The boolean value
	///
	bool get_bool(unsigned int col_idx) const;

	/*! Get the date value stored in a specific cell. An exception is thrown if the cell does not store a value 
	    convertible to a date (a datetime value, including the time component, is considered invalid).
	    \param[in] col_name  The name of the column corresponding to the cell; an exception is thrown if it is invalid
	    \return  The date value
	 */
	[[nodiscard]] auto get_date(std::string_view col_name) const -> Date;
	
	/*! Get the date value stored in a specific cell. An exception is thrown if the cell does not store a value 
	    convertible to a date (a datetime value, including the time component, is considered invalid).
	    \param[in] col_idx  The index of the column corresponding to the cell; an exception is thrown if it is invalid
	    \return  The date value
	 */
	[[nodiscard]] auto get_date(unsigned int col_idx) const -> Date;

	/*! Get the datetime value stored in a specific cell. An exception is thrown if the cell does not store a value 
	    convertible to datetime (a date value, without the time component, is considered invalid).
	    \param[in] col_name  The name of the column corresponding to the cell; an exception is thrown if it is invalid
	    \return  The datetime value
	 */
	[[nodiscard]] auto get_datetime(std::string_view col_name) const -> DateTime;
	
	/*! Get the datetime value stored in a specific cell. An exception is thrown if the cell does not store a value 
	    convertible to datetime (a date value, without the time component, is considered invalid).
	    \param[in] col_idx  The index of the column corresponding to the cell; an exception is thrown if it is invalid
	    \return  The datetime value
	 */
	[[nodiscard]] auto get_datetime(unsigned int col_idx) const -> DateTime;

	/// Get the BLOB value stored in a specific cell. 
	///
	/// @param[in] col_name  The name of the column corresponding to the cell; an exception is thrown if it is invalid
	/// @param[in] blob_size  The BLOB size: the size (in bytes) of the binary data buffer
	/// @return  The BLOB value: the start of the binary data buffer
	///
	const void* get_blob(std::string_view col_name, size_t& blob_size) const;

	/// Get the BLOB value stored in a specific cell. 
	///
	/// @param[in] col_idx  The index of the column corresponding to the cell; an exception is thrown if it is invalid
	/// @param[in] blob_size  The BLOB size: the size (in bytes) of the binary data buffer
	/// @return  The BLOB value: the start of the binary data buffer
	///
	const void* get_blob(unsigned int col_idx, size_t& blob_size) const;

	/// Get the BLOB value stored in a specific cell, as an array. 
	///
	/// @tparam  BlobElem  The type of the array elements, as they are stored in the buffer to be read 
	/// @tparam  Cont  The type of contiguous container which will hold the array
	/// @param[in] col_name  The name of the column corresponding to the cell; an exception is thrown if it is invalid
	/// @param[in] cont  The container which will be filled with the array values; it will be resized appropriately and any 
	///				existing contents will be lost
	///
	template<class BlobElem, 
	         class Cont, 
	         class = EnableIfContigContainer<Cont>> 	
	void get_array_blob(std::string_view col_name, Cont& cont) const;

	/// Get the BLOB value stored in a specific cell, as an array. 
	///
	/// @tparam  BlobElem  The type of the array elements, as they are stored in the buffer to be read 
	/// @tparam  Cont  The type of contiguous container which will hold the array
	/// @param[in] col_idx  The index of the column corresponding to the cell; an exception is thrown if it is invalid
	/// @param[in] cont  The container which will be filled with the array values; it will be resized appropriately and any 
	///				existing contents will be lost
	///
	template<class BlobElem, 
	         class Cont,
	         class = EnableIfContigContainer<Cont>> 
	void get_array_blob(unsigned int col_idx, Cont& cont) const;

	/// Read the compressed buffer stored in a specific cell, then uncompressed the buffer into an array container.
	///
	/// @tparam  Cont  The type of contiguous container which will hold the array
	/// @param[in] col_name  The name of the column corresponding to the cell; an exception is thrown if it 
	///					is invalid
	/// @param[in] cont  The container which will be filled with the array values; it must be large enough 
	///					to hold all the elements in the array resulting from decompressing the database buffer
	/// @param[in] compression  The compression type
	/// @return  The number of elements in the array resulting from decompressing the database buffer
	///
	template<class Cont,
	         class = EnableIfContigContainer<Cont>> 	
	size_t get_uncompress_array_blob(std::string_view col_name, Cont& cont, 
			SqliteBlobCompression compression = SqliteBlobCompression::zlib) const;

	/// Read the compressed buffer stored in a specific cell, then uncompressed the buffer into an array container.
	///
	/// @tparam  Cont  The type of contiguous container which will hold the array
	/// @param[in] col_idx  The index of the column corresponding to the cell; an exception is thrown if it 
	///					is invalid
	/// @param[in] cont  The container which will be filled with the array values; it must be large enough 
	///					to hold all the elements in the array resulting from decompressing the database buffer
	/// @param[in] compression  The compression type
	/// @return  The number of elements in the array resulting from decompressing the database buffer
	///
	template<class Cont,
	         class = EnableIfContigContainer<Cont>> 
	size_t get_uncompress_array_blob(unsigned int col_idx, Cont& cont, 
			SqliteBlobCompression compression = SqliteBlobCompression::zlib) const;

	/// Find out whether the value in a specific cell is NULL.
	///
	/// @param[in] col_name  The name of the column corresponding to the cell; an exception is thrown if it 
	///					is invalid
	/// @return  true if the cell holds a NULL value
	///
	bool is_null(std::string_view col_name) const;
	
	/// Find out whether the value in a specific cell is NULL.
	///
	/// @param[in] col_idx  The index of the column corresponding to the cell; an exception is thrown if it 
	///					is invalid
	/// @return  true if the cell holds a NULL value
	///
	bool is_null(unsigned int col_idx) const;

	/// Get the number of bytes stored in a specific cell.
	///
	/// @param[in] col_name  The name of the column corresponding to the cell; an exception is thrown if it 
	///					is invalid
	/// @return  The byte count
	///
	size_t count_bytes(std::string_view col_name) const;
	
	/// Get the number of bytes stored in a specific cell.
	///
	/// @param[in] col_idx  The index of the column corresponding to the cell; an exception is thrown if it 
	///					is invalid
	/// @return  The byte count
	///
	size_t count_bytes(unsigned int col_idx) const;

private:
	SqliteConnection&  conn_;
	bool  null_text_as_empty_;

	std::unique_ptr<SqlitePreparedStatement>  statement_;
    std::unique_ptr<DatabaseColumnIndexer>  col_indexer_;

	TWIST_NO_COPY_NO_MOVE(SqliteSelector)
	TWIST_CHECK_INVARIANT_DECL
};

// --- Free functions ---

/*! Given a record selector, get the value stored in a specific cell which contains a nullable integer number, as an 
    enum value.
	\tparam Enum  The enumeration type
    \param[in] sel  The record selector
	\param[in] col_name  The name of the column corresponding to the cell; an exception is thrown if it is invalid
    \return  The enum value, or nullopt if the cell stores a NULL value
 */
template<class Enum,
	     class = EnableIfEnum<Enum>>
[[nodiscard]] auto get_nullable_enum_int(const SqliteSelector& sel, std::string_view col_name) -> std::optional<Enum>;

/*! Given a record selector, get the value stored in a specific cell which contains a nullable floating-point number, 
    from the record where the selector is positioned.
    \param[in] sel  The record selector
    \param[in] col_name  The name of the column corresponding to the cell; an exception is thrown if it is invalid
    \return  The floating-point number value, or nullopt if the cell stores a NULL value
 */
[[nodiscard]] auto get_nullable_real(const SqliteSelector& sel, std::string_view col_name) -> std::optional<double>;

/*! Given a record selector, get the value stored in a specific cell which contains a nullable date value, from the 
    record where the selector is positioned.
    \param[in] sel  The record selector
    \param[in] col_name  The name of the column corresponding to the cell; an exception is thrown if it is invalid
    \return  The date value, or nullopt if the cell stores a NULL value
 */
[[nodiscard]] auto get_nullable_date(const SqliteSelector& sel, std::string_view col_name) -> std::optional<Date>;

/*! Given a record selector, get the value stored in a specific cell which contains a nullable datetime value, 
    from the record where the selector is positioned.
    \param[in] sel  The record selector
    \param[in] col_name  The name of the column corresponding to the cell; an exception is thrown if it is invalid
    \return  The datetime value, or nullopt if the cell stores a NULL value
 */
[[nodiscard]] auto get_nullable_datetime(const SqliteSelector& sel, std::string_view col_name) 
                    -> std::optional<DateTime>;

}

#include "twist/db/SqliteSelector.ipp"

#endif 

///  @file  DatabaseSchemaMgr.hpp
///  DatabaseSchemaMgr abstract class template definition, inherited from DatabaseSchemaMgrBase\n
///  DatabaseReorganiseListener abstract class definition

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_DB_DATABASE_SCHEMA_MGR_HPP
#define TWIST_DB_DATABASE_SCHEMA_MGR_HPP

#include "DatabaseSchemaMgrBase.hpp"

namespace twist::db {

template<class Conn> class DatabaseReorganiseListener;

///
///  Abstract base class for classes which manage the schema (structure) of database based on a single file. 
///  A schema manager class creates the database file and database schema, keeps track of the database version 
///    information and reorganises older databases to the current version, bringing both the database schema 
///    and the data up to date.
///  The template parameter is the class representing a connection to the database file (the class which 
///    allows SQL commands to be run on the database, for example).
///
template<class Conn>
class DatabaseSchemaMgr : public DatabaseSchemaMgrBase {
public:
	/// Destructor.
	///
	virtual ~DatabaseSchemaMgr();

	virtual unsigned int get_db_version() override;  // DatabaseSchemaMgrBase  override

    virtual bool is_db_version_up_to_date(std::wstring& special_reorganise_msg) override;  // DatabaseSchemaMgrBase  override   
	
    /// Create a new database file. It will contain no data, except version information.
    /// If the database file already exists, it is first deleted.
    void create_db();

    /// Reorganise the database to the latest version (if its version is older).
    /// Make sure the database file is not used, as this method needs to be able to open it exclusively (as it may change 
	/// its schema). An exception is thrown if a problem occurs while reorganising.
    ///
    /// @param[in] old_version  The version that the database had before reorganising took place (if it did).
    ///
    void reorganise_db(unsigned int& old_version);

    /// Reorganise the database to the latest version (if its version is older).
	/// Call this overload if you wish to register a listener for reorganisation events.
    /// Make sure the database file is not used, as this method needs to be able to open it exclusively (as it may change 
	/// its schema). An exception is thrown if a problem occurs while reorganising.
    ///
    /// @param[in] old_version  The version that the database had before reorganising took place (if it did)
	/// @param[in] listener  A listener for database schema reorganisation events
    ///
    void reorganise_db(unsigned int& old_version, DatabaseReorganiseListener<Conn>& listener);

protected:
	/// Constructor.
	///
    /// @param[in] filename  The database filename
	/// @param[in] cur_db_version  The current (newest) database version number
	/// @param[in] oldest_supported_db_version  The oldest supposed database version number
	///
	DatabaseSchemaMgr(const fs::path& filename, unsigned int cur_db_version, 
			unsigned int oldest_supported_db_version);

private:	
	/// Create the new, empty database file, and open a read-write, exclusive connection to it.
	///
	/// @return  The connection.
	///
	virtual std::unique_ptr<Conn> create_db_file() = 0;

	/// Open a read-only, non-exclusive connection to the existing database file.
	///
	/// @return  An open connection to the new database.
	///
	virtual std::unique_ptr<Conn> open_db_file_read() = 0;

	/// Open a read-write, exclusive connection to the existing database file.
	///
	/// @return  An open connection to the new database.
	///
	virtual std::unique_ptr<Conn> open_db_file_write_exclusive() = 0;

    /// Read the version number from the database.
    ///
    /// @param[in] connection  An open connection to the database.
    ///
    virtual unsigned int read_db_version(Conn& connection) = 0;

    /// Write a version number to the database.
    ///
    /// @param[in] version  The version number.
    /// @param[in] connection  An open connection to the database.
    ///
    virtual void write_db_version(unsigned int version, Conn& connection) = 0;

    /// Create the schema (structure) in an existing model database. The database must contain no schema (must be
    /// empty of structure and, obviously, data).
    ///
    /// @param[in] connection  An open connection to the database.
    ///
    virtual void create_schema(Conn& connection) = 0;

    /// Reorganise database from version k-1 to version k.
	/// This implementation throws an exception (so derived classes needn't override it if no reorganising needs ever 
	/// take place).
    ///
	/// @param[in] version  The version k to which the database is to be reorganised.
    /// @param[in] connection  An open connection to the database.
    ///
    virtual void reorganise_db_to_version(unsigned int version, Conn& connection);

	DatabaseReorganiseListener<Conn>*  reorganise_listener_;

	TWIST_CHECK_INVARIANT_DECL
};

///
///  Abstract base class for classes which are interested in listening to database schema reorganisation events.
///
template<class Conn>
class DatabaseReorganiseListener {
public:
	virtual ~DatabaseReorganiseListener() = 0; 

	/// Called just before the database schema is reorganised from a version to the next version.
	/// This implementation does nothing.
	///
	/// @param[in] version  The new database schema version
	/// @param[in] connection  A connection to the database, which is opened exclusively
	/// 
	virtual void before_reorganise_to_version([[maybe_unused]] unsigned int version, 
			[[maybe_unused]] Conn& connection) 
	{
	}

	/// Called immediately after the database schema is reorganised from a version to the next version.
	/// This implementation does nothing.
	///
	/// @param[in] version  The new database schema version
	/// @param[in] connection  A connection to the database, which is opened exclusively
	/// 
	virtual void after_reorganise_to_version([[maybe_unused]] unsigned int version, 
			[[maybe_unused]] Conn& connection) 
	{
	}
};

}

#include "DatabaseSchemaMgr.ipp"

#endif 


/// @file CsvFileWriter.ipp
/// Inline implementation file for "CsvFileWriter.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace twist::db {

template<class Chr>
CsvFileWriter<Chr>::CsvFileWriter(fs::path path, std::basic_string<Chr> delimiter)
	: delimiter_{move(delimiter)}
{
	file_stream_.emplace(std::make_unique<FileStd>(std::move(path), FileOpenMode::create_write));
	TWIST_CHECK_INVARIANT
}

template<class Chr> 
template<rg::input_range Strings> 
auto CsvFileWriter<Chr>::write_row(Strings&& cells) -> void
{
	TWIST_CHECK_INVARIANT
	auto nof_cells = 0;
	for (const auto& c : cells) {
		if (nof_cells > 0) {
			(*file_stream_) << delimiter_;
		}
		(*file_stream_) << c;
		++nof_cells;
	}

	if (nof_cells == 0) {
		TWIST_THRO2(L"The range of cell values cannot be empty.");
	}

	if (nof_cols_ == 0) {
		nof_cols_ = nof_cells;
	}
	else if (nof_cells != nof_cols_) {
		TWIST_THRO2(L"Wrong number {} of cell values, expected {} cells.", nof_cells, nof_cols_);
	}

	(*file_stream_) << newline_char<Chr>();
}

template<class Chr> 
template<class String> 
auto CsvFileWriter<Chr>::write_row(std::initializer_list<String> cells) -> void
{
	TWIST_CHECK_INVARIANT
	write_row<std::initializer_list<String>>(move(cells)); 
}

template<class Chr>
auto CsvFileWriter<Chr>::nof_columns() const -> Ssize
{
	TWIST_CHECK_INVARIANT
	return nof_cols_;
}

#ifdef _DEBUG
template<class Chr>
auto CsvFileWriter<Chr>::check_invariant() const noexcept -> void
{
	assert(!delimiter_.empty());
	assert(file_stream_);
}
#endif 

}

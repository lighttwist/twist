///  @file  XmlFileSchemaMgr.hpp
///  XmlFileSchemaMgr abstract class, inherits DatabaseSchemaMgr<XmlDoc>

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_XML_FILE_SCHEMA_MGR_HPP
#define TWIST_XML_FILE_SCHEMA_MGR_HPP

#include "DatabaseSchemaMgr.hpp"

namespace twist::db {

class XmlDoc;

/// Abstract base class for classes which manage the schema (structure) of an XML file; partly implements the 
/// base class for XML file databases. The root XML node is expected to have a document version attribute, 
/// named "version".
class XmlFileSchemaMgr : public DatabaseSchemaMgr<XmlDoc> {
public:
	/// Constructor.
	///
    /// @param[in] path  The XML file path
	/// @param[in] root_xnode_tag  The tag of the root XML node
	/// @param[in] cur_db_version  The current (newest) XML document version number
	/// @param[in] oldest_supported_db_version  The oldest supposed XML document version number
	///
	XmlFileSchemaMgr(const fs::path& path, std::wstring_view root_xnode_tag, 
			unsigned int cur_db_version, unsigned int oldest_supported_db_version);

	/// Destructor.
	virtual ~XmlFileSchemaMgr();

private:	
	/// Create the new, empty database file, and open a read-write, exclusive connection to it.
	///
	/// @return  The connection.
	///
	virtual std::unique_ptr<XmlDoc> create_db_file() final;

	/// Open a read-only, non-exclusive connection to the existing database file.
	///
	/// @return  An open connection to the new database.
	///
	virtual std::unique_ptr<XmlDoc> open_db_file_read() final;

	/// Open a read-write, exclusive connection to the existing database file.
	///
	/// @return  An open connection to the new database.
	///
	virtual std::unique_ptr<XmlDoc> open_db_file_write_exclusive() final;

    /// Read the version number from the database.
    ///
    /// @param[in] connection  An open connection to the database.
    ///
    virtual unsigned int read_db_version(XmlDoc& connection) final;

    /// Write a version number to the database.
    ///
    /// @param[in] version  The version number.
    /// @param[in] connection  An open connection to the database.
    ///
    virtual void write_db_version(unsigned int version, XmlDoc& connection) final;

    /// Create the schema (structure) in an existing model database. The database must contain no schema (must 
	/// be empty of structure and, obviously, data).
    ///
    /// @param[in] connection  An open connection to the database.
    ///
    virtual void create_schema(XmlDoc& connection) final;
	    
	const std::wstring  root_xnode_tag_;

	TWIST_CHECK_INVARIANT_DECL
};

} 

#endif 

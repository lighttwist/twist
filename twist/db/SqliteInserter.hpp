///  @file  SqliteInserter.hpp
///  SqliteInserter class definition

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_DB_SQLITE_INSERTER_HPP
#define TWIST_DB_SQLITE_INSERTER_HPP

#include "twist/db/DatabaseColumnIndexer.hpp"
#include "twist/db/SqliteConnection.hpp"
#include "twist/db/SqlitePreparedStatement.hpp"
#include "twist/db/sqlite_utils.hpp"

struct sqlite3_stmt;

namespace twist { 
class DateTime;
}

namespace twist::db {

//! Class managing repeated insertions in the same table of an Sqlite database, using a pre-compiled SQL INSERT query.
class SqliteInserter {
public:
	/// Constructor.
	///
	/// @param[in] conn  The database connection
	/// @param[in] table_name  The table name
	/// @param[in] col_names  The names of the columns in which values will be inserted; the columns will be 
	///					indexed based on the order they have in this list
	///
	SqliteInserter(SqliteConnection& conn, std::string_view table_name, std::initializer_list<const char*> col_names);

	/// Execute an insertion; all the desired cell values must be set before this point; and, where necessary, 
	/// the pointee values must still be alive!
	///
	void insert();

	/// Set the text value to be stored in a specific cell.
	///
	/// @param[in] col_name  The name of the column corresponding to the cell; an exception is thrown if it 
	///					is not found in the list column names used by this object
	/// @param[in] val  Pointer to the text value; the pointee must be alive when insert() is called!
	///
	void set_text(std::string_view col_name, gsl::not_null<const std::wstring*> val);

	/// Set the text value to be stored in a specific cell. This overload deals with transient values (a copy 
	/// will be made of the text value passed in, and kept around for the insertion).
	///
	/// @param[in] col_name  The name of the column corresponding to the cell; an exception is thrown if it 
	///					is not found in the list column names used by this object
	/// @param[in] val  The text value
	///
	void set_text(std::string_view col_name, std::wstring&& val);

	/// Set the text value to be stored in a specific cell.
	///
	/// @param[in] col_idx  The index (in the list column names used by this object) of the column 
	///					corresponding to the cell; an exception is thrown if it is invalid
	/// @param[in] val  Pointer to the text value; the pointee must be alive when insert() is called!
	///
	void set_text(unsigned int col_idx, gsl::not_null<const std::wstring*> val);
	
	/// Set the text value to be stored in a specific cell. This overload deals with transient values (a copy 
	/// will be made of the text value passed in, and kept around for the insertion).
	///
	/// @param[in] col_idx  The index (in the list column names used by this object) of the column 
	///					corresponding to the cell; an exception is thrown if it is invalid
	/// @param[in] val  The text value
	///
	void set_text(unsigned int col_idx, std::wstring&& val);

	/// Set the text value to be stored in a specific cell, from a path value.
	///
	/// @param[in] col_idx  The index (in the list column names used by this object) of the column 
	///					corresponding to the cell; an exception is thrown if it is invalid
	/// @param[in] val  Pointer to the path value; the pointee must be alive when insert() is called!
	///
	void set_text(unsigned int col_idx, const fs::path& val);

	/// Set the ANSI text value to be stored in a specific cell.
	///
	/// @param[in] col_name  The name of the column corresponding to the cell; an exception is thrown if it 
	///					is not found in the list column names used by this object
	/// @param[in] val  Pointer to the text value; the pointee must be alive when insert() is called!
	///
	void set_ansi(std::string_view col_name, gsl::not_null<const std::string*> val);

	/// Set the ANSI text value to be stored in a specific cell. This overload deals with transient values (a 
	/// copy will be made of the text value passed in, and kept around for the insertion).
	///
	/// @param[in] col_name  The name of the column corresponding to the cell; an exception is thrown if it 
	///					is not found in the list column names used by this object
	/// @param[in] val  The text value
	///
	void set_ansi(std::string_view col_name, std::string&& val);
	
	/// Set the ANSI text value to be stored in a specific cell.
	///
	/// @param[in] col_idx  The index (in the list column names used by this object) of the column corresponding to the cell; 
	///					an exception is thrown if it is invalid
	/// @param[in] val  Pointer to the text value; the pointee must be alive when insert() is called!
	///
	void set_ansi(unsigned int col_idx, gsl::not_null<const std::string*> val);
	
	/// Set the ANSI text value to be stored in a specific cell. This overload deals with transient values (a copy will be made 
	/// of the text value passed in, and kept around for the insertion).
	///
	/// @param[in] col_idx  The index (in the list column names used by this object) of the column corresponding to the cell; 
	///					an exception is thrown if it is invalid
	/// @param[in] val  The text value
	///
	void set_ansi(unsigned int col_idx, std::string&& val);
	
	/// Set the integer number value to be stored in a specific cell.
	///
	/// @param[in] col_name  The name of the column corresponding to the cell; an exception is thrown if it 
	///					is not found in the list column names used by this object
	/// @param[in] val  The integer number value
	///
	void set_int(std::string_view col_name, int val);
	
	/// Set the integer number value to be stored in a specific cell.
	///
	/// @param[in] col_idx  The index (in the list column names used by this object) of the column 
	///					corresponding to the cell; an exception is thrown if it is invalid
	/// @param[in] val  The integer number value
	///
	void set_int(unsigned int col_idx, int val);
	
	/// Set the integer number value to be stored in a specific cell.
	///
	/// @param[in] col_name  The name of the column corresponding to the cell; an exception is thrown if it 
	///					is not found in the list column names used by this object
	/// @param[in] val  The integer number value
	///
	template<class Enum,
	         class = EnableIfEnum<Enum>>
	void set_enum_int(std::string_view col_name, Enum val);
	
	/// Set the integer number value to be stored in a specific cell.
	///
	/// @param[in] col_idx  The index (in the list column names used by this object) of the column 
	///					corresponding to the cell; an exception is thrown if it is invalid
	/// @param[in] val  The integer number value
	///
	template<class Enum,
	         class = EnableIfEnum<Enum>>
	void set_enum_int(unsigned int col_idx, Enum val);

	/// Set the 64-bit integer number value to be stored in a specific cell.
	///
	/// @param[in] col_name  The name of the column corresponding to the cell; an exception is thrown if it is not found in the 
	///					list column names used by this object
	/// @param[in] val  The 64-bit integer number value
	///
	void set_int64(std::string_view col_name, int64_t val);
	
	/// Set the 64-bit integer number value to be stored in a specific cell.
	///
	/// @param[in] col_idx  The index (in the list column names used by this object) of the column 
	///					corresponding to the cell; an exception is thrown if it is invalid
	/// @param[in] val  The 64-bit integer number value
	///
	void set_int64(unsigned int col_idx, int64_t val);
	
	/// Set the floating-point number value to be stored in a specific cell.
	///
	/// @param[in] col_name  The name of the column corresponding to the cell; an exception is thrown if it is not found in the 
	///				list column names used by this object
	/// @param[in] val  The floating-point number value
	///
	void set_real(std::string_view col_name, double val);
	
	/// Set the floating-point number value to be stored in a specific cell.
	///
	/// @param[in] col_idx  The index (in the list column names used by this object) of the column corresponding to the cell; 
	///				an exception is thrown if it is invalid
	/// @param[in] val  The floating-point number value
	///
	void set_real(unsigned int col_idx, double val);

	/// Set the boolean value to be stored in a specific cell. 
	///
	/// @param[in] col_name  The name of the column corresponding to the cell; an exception is thrown if it is not found in the 
	///				list column names used by this object
	/// @param[in] val  The boolean value
	///
	void set_bool(std::string_view col_name, bool val);
	
	/// Set the boolean value to be stored in a specific cell. 
	///
	/// @param[in] col_idx  The index (in the list column names used by this object) of the column corresponding to the cell; 
	///				an exception is thrown if it is invalid
	/// @param[in] val  The boolean value
	///
	void set_bool(unsigned int col_idx, bool val);

	/// Set the date value to be stored in a specific cell. 
	///
	/// @param[in] col_name  The name of the column corresponding to the cell; an exception is thrown if it 
	///					is not found in the list column names used by this object
	/// @param[in] val  The date value
	///
	void set_date(std::string_view col_name, const Date& val);

	/// Set the date value to be stored in a specific cell. 
	///
	/// @param[in] col_idx  The index (in the list column names used by this object) of the column 
	///					corresponding to the cell; an exception is thrown if it is invalid
	/// @param[in] val  The date value
	///
	void set_date(unsigned int col_idx, const Date& val);

	/// Set the date-time value to be stored in a specific cell. 
	///
	/// @param[in] col_name  The name of the column corresponding to the cell; an exception is thrown if it 
	///					is not found in the list column names used by this object
	/// @param[in] val  The date-time value
	///
	void set_datetime(std::string_view col_name, const DateTime& val);

	/// Set the date-time value to be stored in a specific cell. 
	///
	/// @param[in] col_idx  The index (in the list column names used by this object) of the column 
	///					corresponding to the cell; an exception is thrown if it is invalid
	/// @param[in] val  The date-time value
	///
	void set_datetime(unsigned int col_idx, const DateTime& val);

	/// Set the BLOB value to be stored in a specific cell. 
	///
	/// @param[in] col_name  The name of the column corresponding to the cell; an exception is thrown if it is not found in the 
	///				list column names used by this object
	/// @param[in] blob  The BLOB value: the start of the binary data buffer
	/// @param[in] blob_size  The BLOB size: the size (in bytes) of the binary data buffer
	///
	void set_blob(std::string_view col_name, gsl::not_null<const void*> blob, size_t blob_size);

	/// Set the BLOB value to be stored in a specific cell. 
	///
	/// @param[in] col_idx  The index (in the list column names used by this object) of the column 
	///					corresponding to the cell; an exception is thrown if it is invalid
	/// @param[in] blob  The BLOB value: the start of the binary data buffer
	/// @param[in] blob_size  The BLOB size: the size (in bytes) of the binary data buffer
	///
	void set_blob(unsigned int col_idx, gsl::not_null<const void*> blob, size_t blob_size);

	/// Set the BLOB value to be stored in a specific cell, from an array. 
	///
	/// @tparam  Cont  The type of contiguous container which holds the array
	/// @param[in] col_name  The name of the column corresponding to the cell; an exception is thrown if it 
	///					is not found in the list column names used by this object
	/// @param[in] cont  Pointer to the container holding the array; the pointee must be alive when insert() 
	///					is called!
	///
	template<class Cont,
	         class = EnableIfContigContainer<Cont>> 
	void set_array_blob(std::string_view col_name, const Cont* cont);

	/// Set the BLOB value to be stored in a specific cell, from an array. 
	///
	/// @tparam  Cont  The type of contiguous container which holds the array
	/// @param[in] col_idx  The index (in the list column names used by this object) of the column 
	///					corresponding to the cell; an exception is thrown if it is invalid
	/// @param[in] cont  Pointer to the container holding the array; the pointee must be alive when insert() 
	///					is called!
	///
	template<class Cont,
	         class = EnableIfContigContainer<Cont>> 
	void set_array_blob(unsigned int col_idx, const Cont* cont);

	/// Compress the contents of an array and set the compressed data as the BLOB value to be stored in a 
	/// specific cell. 
	///
	/// @tparam  Cont  The type of contiguous container which holds the array
	/// @param[in] col_name  The name of the column corresponding to the cell; an exception is thrown if it 
	///					is not found in the list column names used by this object
	/// @param[in] cont  Pointer to the container holding the (un-compressed)array; the pointee must be alive 
	///					when insert() is called!
	/// @param[in] compressed_buf  The buffer which will be used to stored the compressed data; it will be 
	///					resized appropriately; the pointee must be alive when insert() is called!
	/// @param[in] compression  The compression type
	/// @return  The number of elements in the array resulting from decompressing the database buffer
	///
	template<class Cont,
	         class = EnableIfContigContainer<Cont>> 
	bool set_compress_array_blob(
			std::string_view col_name, 
			const Cont* cont,
			gsl::not_null<std::vector<unsigned char>*> compressed_buf, 
			SqliteBlobCompression compression = SqliteBlobCompression::zlib);

	/// Compress the contents of an array and set the compressed data as the BLOB value to be stored in a 
	/// specific cell. 
	///
	/// @tparam  Cont  The type of contiguous container which holds the array
	/// @param[in] col_idx  The index (in the list column names used by this object) of the column 
	///					corresponding to the cell; an exception is thrown if it is invalid
	/// @param[in] cont  Pointer to the container holding the (un-compressed)array; the pointee must be alive 
	///					when insert() is called!
	/// @param[in] compressed_buf  The buffer which will be used to stored the compressed data; it will be 
	///					resized appropriately; the pointee must be alive when insert() is called!
	/// @param[in] compression  The compression type
	/// @return  The number of elements in the array resulting from decompressing the database buffer
	///
	template<class Cont,
	         class = EnableIfContigContainer<Cont>> 
	bool set_compress_array_blob(
			unsigned int col_idx, 
			const Cont* cont, 
			gsl::not_null<std::vector<unsigned char>*> compressed_buf, 
			SqliteBlobCompression compression = SqliteBlobCompression::zlib);

private:
	[[nodiscard]] static auto prepare(const std::string& table_name, 
			                          const std::vector<std::string>& col_names, 
									  SqliteConnection& conn) -> SqlitePreparedStatement;

	SqliteConnection& conn_;

    DatabaseColumnIndexer col_indexer_;
	std::unique_ptr<SqlitePreparedStatement> statement_;

	std::string true_str_;
	std::string false_str_;

#ifdef _DEBUG
	std::string table_name_;
#endif 

	TWIST_NO_COPY_NO_MOVE(SqliteInserter)

	TWIST_CHECK_INVARIANT_DECL
};

// --- Free functions ---

/*! Given a record inserter, optionally set the value in a specific cell, in the record where the selector is 
    positioned, from a a nullable datetime value.
    \param[in] ins  The record inserter
    \param[in] col_name  The name of the column corresponding to the cell; an exception is thrown if it is invalid
    \param[in] datetime  The datetime value; if nullopt, no action is taken
	\return  Whether the datetime value was non-null, and an action was taken 
 */
auto set_nullable_datetime(SqliteInserter& ins, std::string_view col_name, std::optional<DateTime> datetime) -> bool;

/*! Given a record inserter, optionally set the value in a specific cell, in the record where the selector is 
    positioned, from a a nullable date value.
    \param[in] ins  The record inserter
    \param[in] col_name  The name of the column corresponding to the cell; an exception is thrown if it is invalid
    \param[in] date  The date value; if nullopt, no action is taken
	\return  Whether the date value was non-null, and an action was taken 
 */
auto set_nullable_date(SqliteInserter& ins, std::string_view col_name, std::optional<Date> date) -> bool;

} 

#include "twist/db/SqliteInserter.ipp"

#endif 

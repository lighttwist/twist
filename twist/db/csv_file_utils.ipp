/// @file csv_file_utils.ipp
/// Inline implementation file for "csv_file_utils.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/OutTxtFileStream.hpp"
#include "twist/std_vector_utils.hpp"
#include "twist/db/CsvFileReader.hpp"
#include "twist/db/CsvFileWriter.hpp"

namespace twist::db {

template<class Chr, 
         class Transform>
requires std::invocable<Transform, const std::basic_string<Chr>&>
auto read_csv_file_column(const fs::path& csv_path, 
                          const std::basic_string<Chr>& column_name, 
                          Transform transform) 
	  -> std::vector<std::decay_t<std::invoke_result_t<Transform, const std::basic_string<Chr>&>>>
{
	auto reader = CsvFileReader<Chr>{csv_path};
	
	const auto col_pos = position_of(reader.view_col_names(), column_name);
	if (col_pos == k_no_pos) {
		TWIST_THROW(L"Column name \"%s\" not found in file \"%s\".", 
					to_string(column_name).c_str(), csv_path.filename().c_str());
	}

	auto ret = std::vector<std::decay_t<std::invoke_result_t<Transform, const std::basic_string<Chr>&>>>{};
	while (reader.read_next_row()) {
		ret.push_back(std::invoke(transform, reader.cell_ref(col_pos)));
	}

	return ret;
}

template<class Chr, 
         class Transform>
requires std::invocable<Transform, const std::basic_string<Chr>&>
[[nodiscard]] auto read_single_column_csv_file(const fs::path& csv_path, 
                                               Transform transform) 
	                -> std::vector<std::decay_t<std::invoke_result_t<Transform, const std::basic_string<Chr>&>>>
{
	auto reader = CsvFileReader<Chr>{csv_path};
	
	if (reader.nof_columns() != 1) {
        TWIST_THRO2(L"CSV file \"{}\" expected to have one column.", csv_path.filename().c_str());
    }

	auto ret = std::vector<std::decay_t<std::invoke_result_t<Transform, const std::basic_string<Chr>&>>>{};
	while (reader.read_next_row()) {
		ret.push_back(std::invoke(transform, reader.cell_ref(0)));
	}

	return ret;
}

template<class Chr, 
         rg::input_range Rng,
         class Transform>
requires std::invocable<Transform, rg::range_value_t<Rng>>
auto write_one_column_csv_file(const Rng& range, 
                               const fs::path& csv_path, 
                               const std::basic_string<Chr>& column_name,
                               Transform transform) -> void
{
	auto writer = CsvFileWriter<Chr>{csv_path};
	if (!column_name.empty()) {
		writer.write_row({column_name});
	}
	for (const auto& val : range) {
		writer.write_row({transform(val)});
	}
}

template<class Chr, rg::input_range Strings>
auto export_csv_file_columns(const fs::path& in_csv_path, const fs::path& out_csv_path, 
                             const Strings& column_names, std::basic_string<Chr> in_csv_delimiter,
                             std::basic_string<Chr> out_csv_delimiter) -> void
{
	auto reader = CsvFileReader<Chr>{in_csv_path, move(in_csv_delimiter)};
	const auto in_col_positions = transform_to_vector(column_names, [&](auto col_name) {
		if (const auto pos = position_of(reader.view_col_names(), convert_string<Chr>(col_name)); pos != k_no_pos) {
			return pos;
		}
		TWIST_THROW(L"Column name \"%s\" not found in file \"%s\".", 
					to_string(col_name).c_str(), in_csv_path.filename().c_str());
	});

	const auto out_col_count = rg::ssize(column_names);
	auto out_row = std::vector<std::basic_string<Chr>>(out_col_count);
	auto copy_cols_to_out_row = [&](const auto& src) {
		for (auto i : IndexRange{out_col_count}) {
			out_row[i] = src[in_col_positions[i]];
		}			
	};
	copy_cols_to_out_row(reader.view_col_names());

	auto writer = CsvFileWriter<Chr>{out_csv_path};
	writer.write_row(out_row);
	while (reader.read_next_row()) {
		copy_cols_to_out_row(reader.row_ref());
		writer.write_row(out_row);	
	}
}

namespace detail { 

template<class Chr, class Cont, class CellConv> 
requires std::is_invocable_r_v<std::basic_string<Chr>, CellConv, const typename Cont::Value&>
auto to_stream(const Cont& cont, OutTxtFileStream<Chr>& stream, CellConv cell_conv, 
               const std::basic_string<Chr>& col_sep) -> void
{
	const auto nof_cols = cont.nof_columns();
	for (auto i : IndexRange{cont.nof_rows()}) {
		for (auto j : IndexRange{nof_cols}) {
			stream << cell_conv(cont(i, j));
			if (j < nof_cols - 1) {
				stream << col_sep;
			}
		}
		stream << newline_char<Chr>();
	}
}

} 

template<class Chr, 
         class T,
         class CellConv>
requires std::is_invocable_r_v<T, CellConv, const std::basic_string<Chr>&>
auto read_csv_file(fs::path path,
				   std::vector<std::wstring> column_names,
				   CellConv cell_conv,
				   const std::basic_string<Chr>& col_sep) -> RowwiseHomogenousTable<T>
{
	auto reader = CsvFileReader<Chr>{std::move(path), col_sep};
	const auto in_col_positions = transform_to_vector(column_names, [&reader](const auto& col_name) {
		if (const auto pos = position_of(reader.view_col_names(), convert_string<Chr>(col_name)); pos != k_no_pos) {
			return pos;
		}
		TWIST_THROW(L"Column name \"%s\" not found in file \"%s\".", 
					to_string(col_name).c_str(), reader.path().filename().c_str());
	});

	auto table = RowwiseHomogenousTable<T>{move(column_names)};

	auto add_table_row = [&table, &in_col_positions, cell_conv](const auto& src) {
		auto row_values = std::vector<T>(table.nof_columns());
		for (auto i : IndexRange{table.nof_columns()}) {
			row_values[i] = cell_conv(src[in_col_positions[i]]);
		}			
		table.add_row(move(row_values));
	};

	while (reader.read_next_row()) {
		add_table_row(reader.row_ref());
	}

	return table;
}

template<class Chr, 
         class T, 
         class CellConv> 
requires std::is_invocable_r_v<std::basic_string<Chr>, CellConv, const T&>
auto write_csv_file(const twist::math::RowwiseMatrix<T>& matrix, fs::path path, CellConv cell_conv, 
                    const std::basic_string<Chr>& col_sep) -> void
{
	auto stream = OutTxtFileStream<Chr>{std::make_unique<FileStd>(std::move(path), FileOpenMode::create_write)};
	detail::to_stream<Chr>(matrix, stream, cell_conv, col_sep);
}

template<class Chr, 
         class T, 
         class CellConv> 
requires std::is_invocable_r_v<std::basic_string<Chr>, CellConv, const T&>
auto write_csv_file(const twist::math::ColumnwiseMatrix<T>& matrix, fs::path path, CellConv cell_conv, 
                    const std::basic_string<Chr>& col_sep) -> void
{
	auto stream = OutTxtFileStream<Chr>{std::make_unique<FileStd>(std::move(path), FileOpenMode::create_write)};
	detail::to_stream<Chr>(matrix, stream, cell_conv, col_sep);
}

template<class Chr, 
         class T, 
         HomogenousTableStorageType storage_type,
         class CellConv> 
requires std::is_invocable_r_v<std::basic_string<Chr>, CellConv, const T&>
auto write_csv_file(const HomogenousTable<T, storage_type>& table, fs::path path, CellConv cell_conv, 
                    const std::basic_string<Chr>& col_sep) -> void
{
	auto stream = OutTxtFileStream<Chr>{std::make_unique<FileStd>(std::move(path), FileOpenMode::create_write)};

	// Write header
	const auto nof_cols = table.nof_columns();
	const auto& col_names = table.column_names();
	for (auto j : IndexRange{nof_cols}) {
		stream << convert_string<Chr>(col_names[j]);
		if (j < nof_cols - 1) {
			stream << col_sep;
		}
	}
	stream << newline_char<Chr>();

	// Write cells
	detail::to_stream(table, stream, cell_conv, col_sep);
}

}

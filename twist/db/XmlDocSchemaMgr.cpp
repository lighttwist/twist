/// @file XmlDocSchemaMgr.cpp
/// Implementation file for "XmlDocSchemaMgr.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/db/XmlDocSchemaMgr.hpp"

#include "twist/db/xml_utils.hpp"

namespace twist::db {

XmlDocSchemaMgr::XmlDocSchemaMgr(XmlDoc& xdoc, 
                                 std::wstring root_xnode_tag, 
		                         unsigned int cur_doc_version, 
								 unsigned int oldest_supported_doc_version)
	: xdoc_{xdoc}
	, root_xnode_tag_{move(root_xnode_tag)}
	, cur_doc_version_{cur_doc_version}
	, oldest_supported_doc_version_{oldest_supported_doc_version}
{
	TWIST_CHECK_INVARIANT
}

XmlDocSchemaMgr::~XmlDocSchemaMgr()
{
	TWIST_CHECK_INVARIANT
}

auto XmlDocSchemaMgr::reorganise(unsigned int& old_version) -> bool
{
	TWIST_CHECK_INVARIANT
	// Open the XML file, check that it has the correct structure (check the root XML node name and the 
	// existence of the version attribute) and get the version number
	get_xml_doc_version(xdoc_, root_xnode_tag_, old_version);

    if (old_version < oldest_supported_doc_version_) {
		TWIST_THROW(L"The XML document has version \"%d\", which is unsupported. "
				"Version \"%d\" is the oldest supported.", old_version, oldest_supported_doc_version_);
	}
    if (old_version > cur_doc_version_) {
		TWIST_THROW(L"The format of the XML document is newer than the application. Cannot open document.");
	} 
	if (old_version < cur_doc_version_) {
		// Reorganise the document to the current version
		for (unsigned int version = old_version + 1; version <= cur_doc_version_; ++version) {
			reorganise_doc_to_version(version, xdoc_);
		}
		// Update the version number
		set_xml_doc_version(xdoc_, root_xnode_tag_, cur_doc_version_);	
		return true;
	}
	return false;
}

void XmlDocSchemaMgr::reorganise_doc_to_version(unsigned int /*version*/, XmlDoc& /*xdoc*/)
{
	TWIST_CHECK_INVARIANT
	TWIST_THROW(L"This method must be overridden for XML documents which need reorganising!");
}

#ifdef _DEBUG
void XmlDocSchemaMgr::check_invariant() const noexcept
{
	assert(!root_xnode_tag_.empty());
	assert(cur_doc_version_ > 0);
	assert(oldest_supported_doc_version_ > 0);
}
#endif 

}

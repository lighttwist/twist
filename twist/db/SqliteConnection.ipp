/// @file SqliteConnection.ipp
/// Inline implementation file for "SqliteConnection.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/zlib/zlib_utils.hpp"

namespace twist::db {

template<typename BlobElem, class Cont> 
void SqliteConnection::column_array_blob(const SqlitePreparedStatement& statement, int col_idx, Cont& cont)
{
	TWIST_CHECK_INVARIANT
	const int blob_buffer_size = column_bytes(statement, col_idx);
	if (blob_buffer_size % sizeof(BlobElem) != 0) {
		TWIST_THROW(L"Invalid buffer size %d for BLOB field.", blob_buffer_size);
	}

	const size_t blob_array_size = blob_buffer_size / sizeof(BlobElem);
	const BlobElem* blob_buffer = static_cast<const BlobElem*>(column_blob(statement, col_idx));

	cont.resize(blob_array_size);
	std::copy(blob_buffer, blob_buffer + blob_array_size, begin(cont));
}


template<typename Cont> 
size_t SqliteConnection::column_uncompress_array_blob(const SqlitePreparedStatement& statement, int col_idx, 
		Cont& cont, SqliteBlobCompression compression) 
{
	TWIST_CHECK_INVARIANT
	if (compression == SqliteBlobCompression::none) {
		TWIST_THROW(L"This function cannot be used for uncompressed BLOBs.");
	}
	if (compression != SqliteBlobCompression::zlib) {
		TWIST_THROW(L"Invalid BLOB compression type value %d.", compression);
	}

	const int compressed_buf_size = column_bytes(statement, col_idx);
	const auto* compressed_buf = reinterpret_cast<const unsigned char*>(column_blob(statement, col_idx));

	const auto value_size = sizeof(typename Cont::value_type);

	auto uncompressed_buf_size = static_cast<unsigned long>(cont.size() * value_size);
	auto* uncompressed_buf = reinterpret_cast<unsigned char*>(cont.data()); 

	const auto result = zlib::uncompress_buf(
			compressed_buf, compressed_buf_size, uncompressed_buf, uncompressed_buf_size);
	if (result != zlib::k_uncompress_ok) {
		TWIST_THROW(L"Error uncompressing database BLOB: %s.", zlib::get_descr(result));
	}
	if (uncompressed_buf_size % value_size != 0) {
		TWIST_THROW(L"Invalid uncompressed buffer size %d for BLOB field.", uncompressed_buf_size);
	}

	return uncompressed_buf_size / value_size;
}

// --- Free functions ---

template<class Fn, class>
void transact(SqliteConnection& conn, Fn&& func)
{
	try {
		conn.begin_transaction();
		std::forward<Fn>(func)(conn);
	}
	catch (...) {
		conn.rollback_transaction();
		throw;
	}
	conn.commit_transaction();
}

} 


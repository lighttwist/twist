///  @file  SqlitePreparedStatement.ipp
///  Inline implementation file for "SqlitePreparedStatement.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/zlib/zlib_utils.hpp"

namespace twist::db {

template<class Cont> 
void SqlitePreparedStatement::bind_array_blob(int param_idx, const Cont* cont)
{
	TWIST_CHECK_INVARIANT
	const size_t blob_size = cont->size() * sizeof(Cont::value_type);
	bind_blob(param_idx, cont->data(), blob_size);
}


template<class Cont> 
bool SqlitePreparedStatement::bind_compress_array_blob(int param_idx, const Cont* cont, 
		std::vector<unsigned char>* compressed_buf, SqliteBlobCompression compression)
{
	TWIST_CHECK_INVARIANT
	bool ret = false;

	if (compression == SqliteBlobCompression::none) {
		TWIST_THROW(L"This function cannot be used for uncompressed BLOBs.");
	}
	if (compression != SqliteBlobCompression::zlib) {
		TWIST_THROW(L"Invalid BLOB compression type value %d.", compression);
	}

	// Compress the input buffer
	const auto value_size = sizeof(typename Cont::value_type);
	const auto* uncompressed_buf = reinterpret_cast<const unsigned char*>(cont->data());
	const auto uncompressed_buf_size = static_cast<unsigned long>(cont->size() * value_size);

	compressed_buf->resize(uncompressed_buf_size);
	auto compressed_buf_size = uncompressed_buf_size;

	const auto result = zlib::compress_buf(uncompressed_buf, uncompressed_buf_size, 
			compressed_buf->data(), compressed_buf_size);
	assert(compressed_buf_size < uncompressed_buf_size);
	if (result == zlib::k_compress_ok) {
		compressed_buf->resize(compressed_buf_size);
		bind_blob(param_idx, compressed_buf->data(), compressed_buf_size);
		ret = true;
	}
	else {
		// Compression failed
		bind_blob(param_idx, uncompressed_buf, uncompressed_buf_size);
	}

	return ret;
}

} 


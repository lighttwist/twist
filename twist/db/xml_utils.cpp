/// @file xml_utils_new.cpp
/// Implementation file for "xml_utils_new.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/db/xml_utils.hpp"

//  https://pugixml.org/

#include "external/pugixml/pugixml.hpp"

static const wchar_t* k_xattr_version = L"version";

// --- Local functions ---

namespace pugi {

twist::Ssize size(const xml_object_range<xml_named_node_iterator>& range)
{
	return std::distance(range.begin(), range.end());
}

}

namespace twist::db {

// --- XmlDoc::Impl class ---

class XmlDoc::Impl {
public:
	const pugi::xml_document& raw() const
	{
		return raw_;
	}

	pugi::xml_document& raw() 
	{
		return raw_;
	}

private:
	pugi::xml_document  raw_{};
};

// --- XmlElemNode::Impl class ---

class XmlElemNode::Impl {
public:
	static XmlElemNode make_node(pugi::xml_node raw)
	{
		return XmlElemNode{ 
				std::unique_ptr<XmlElemNode::Impl>{ 
						new XmlElemNode::Impl{ raw } } };
	}

	pugi::xml_node raw() const
	{
		return raw_;
	}

private:
	Impl(pugi::xml_node raw)
		: raw_{ raw }
	{
	}

	pugi::xml_node  raw_;
};

// --- XmlDoc class ---

auto XmlDoc::make_empty() -> std::unique_ptr<XmlDoc>
{
	return std::unique_ptr<XmlDoc>{new XmlDoc{}};
}

auto XmlDoc::read_from_string(const std::wstring& xml_string) -> std::unique_ptr<XmlDoc>
{
	auto xdoc = std::unique_ptr<XmlDoc>{new XmlDoc{}};
	xdoc->pimpl_->raw().load_string(xml_string.c_str());
	return xdoc;
}

auto XmlDoc::read_from_file(const fs::path& path) -> std::unique_ptr<XmlDoc>
{
	if (!exists(path)) {
		TWIST_THRO2(L"XML file \"{}\" not found.", path.c_str());
	}
	auto xdoc = std::unique_ptr<XmlDoc>{new XmlDoc{}};
	xdoc->pimpl_->raw().load_file(path.c_str());
	return xdoc;
}

XmlDoc::XmlDoc() 
	: pimpl_{new Impl{}}
{
	TWIST_CHECK_INVARIANT
}

XmlDoc::~XmlDoc()
{
	TWIST_CHECK_INVARIANT
}

auto XmlDoc::root_elem() -> XmlElemNode
{
	TWIST_CHECK_INVARIANT
	if (auto root_node = pimpl_->raw().first_child()) {
		return XmlElemNode::Impl::make_node(root_node);
	}
	TWIST_THROW(L"The XML document has no root element.");
}

XmlElemNode XmlDoc::create_root_elem(const std::wstring& tag)
{
	TWIST_CHECK_INVARIANT
	if (pimpl_->raw().first_child()) {
		TWIST_THROW(L"The root element node for this XML document is already set.");	
	}
	auto root_node = pimpl_->raw().append_child(tag.c_str());
	return XmlElemNode::Impl::make_node(root_node);
}


void XmlDoc::save(const fs::path& path) const
{
	TWIST_CHECK_INVARIANT
	pimpl_->raw().save_file(path.c_str());
}


std::wstring XmlDoc::get_xml_text() const
{
	TWIST_CHECK_INVARIANT
	std::wstringstream stream;
	pimpl_->raw().save(stream);
	return stream.str();
}

auto XmlDoc::create_elem_node(const std::wstring& tag, const XmlElemNode& parent) -> XmlElemNode
{
	TWIST_CHECK_INVARIANT
	auto node = parent.pimpl_->raw().append_child(tag.c_str());
	return XmlElemNode::Impl::make_node(node);
}

 auto XmlDoc::create_elem_node_with_text(const std::wstring& tag, const std::wstring& value, const XmlElemNode& parent) 
       -> XmlElemNode
{
	TWIST_CHECK_INVARIANT
	auto new_node = parent.pimpl_->raw().append_child(tag.c_str());
	new_node.text().set(value.c_str());
	return XmlElemNode::Impl::make_node(new_node);
}


XmlElemNode XmlDoc::create_elem_node_with_int(const std::wstring& tag, int value, const XmlElemNode& parent)
{
	TWIST_CHECK_INVARIANT
	return create_elem_node_with_text(tag, std::to_wstring(value), parent);
}


XmlElemNode XmlDoc::create_elem_node_with_float(const std::wstring& tag, double value, 
		const XmlElemNode& parent)
{
	TWIST_CHECK_INVARIANT
	return create_elem_node_with_text(tag, to_str(value, 0, FloatStrFormat::exp), parent);
}


XmlElemNode XmlDoc::create_elem_node_with_bool(const std::wstring& tag, bool value, 
		const XmlElemNode& parent)
{
	TWIST_CHECK_INVARIANT
	return create_elem_node_with_text(tag, value ? L"true" : L"false", parent);
}


void XmlDoc::remove_child_node(const XmlElemNode& parent, const XmlElemNode& child)
{
	TWIST_CHECK_INVARIANT
	parent.pimpl_->raw().remove_child(child.pimpl_->raw());
}


size_t XmlDoc::remove_child_nodes_with_tag(const XmlElemNode& parent, const std::wstring& tag)
{
	TWIST_CHECK_INVARIANT
	size_t ret = 0;
	for (auto el : parent.get_children_with_tag(tag)) {
		remove_child_node(parent, el);
		++ret;
	}
	return ret;
}


#ifdef _DEBUG
void XmlDoc::check_invariant() const noexcept
{
	assert(pimpl_);
	if (pimpl_) {
		assert(pimpl_->raw());
	}
}
#endif 

// --- XmlNodeAttr class ---

XmlNodeAttr::XmlNodeAttr(std::wstring_view name, std::wstring_view value) 
	: name_{name}
	, value_{value}
{
	TWIST_CHECK_INVARIANT
}

XmlNodeAttr::XmlNodeAttr(std::wstring_view name, int value) 
	: name_{name}
	, value_{std::to_wstring(value)}
{
	TWIST_CHECK_INVARIANT
}

auto XmlNodeAttr::get_name() const -> std::wstring
{
	TWIST_CHECK_INVARIANT
	return name_;
}

auto XmlNodeAttr::get_value() const -> std::wstring
{
	TWIST_CHECK_INVARIANT
	return value_;
}

auto XmlNodeAttr::get_int_value() const -> int
{
	TWIST_CHECK_INVARIANT
	return to_int(value_);
}

auto XmlNodeAttr::get_float_value() const -> double
{
	TWIST_CHECK_INVARIANT
	return to_double(value_);
}

#ifdef _DEBUG
auto XmlNodeAttr::check_invariant() const noexcept -> void
{
	assert(!name_.empty());
	assert(!value_.empty());
}
#endif 

// --- XmlElemNode class ---

XmlElemNode::XmlElemNode(std::shared_ptr<Impl> pimpl) 
	: pimpl_{ pimpl }
{
	TWIST_CHECK_INVARIANT
}

std::wstring XmlElemNode::tag() const
{
	TWIST_CHECK_INVARIANT
	return pimpl_->raw().name();
}

std::wstring XmlElemNode::get_tag() const
{
	TWIST_CHECK_INVARIANT
	return tag();
}

bool XmlElemNode::has_text() const
{
	TWIST_CHECK_INVARIANT
	auto text_child = pimpl_->raw().find_child([](auto child) { 
		return child.type() == pugi::node_pcdata || child.type() == pugi::node_cdata;
	});
	return text_child;
}

std::wstring XmlElemNode::text() const
{
	TWIST_CHECK_INVARIANT
	auto text_child = pimpl_->raw().find_child([](auto child) { 
		return child.type() == pugi::node_pcdata || child.type() == pugi::node_cdata;
	});

	if (!text_child) {
		TWIST_THRO2(L"Cannot find plain text child node for element \"{}\".", tag());
	}
	return text_child.value();
}

std::wstring XmlElemNode::get_text() const
{
	TWIST_CHECK_INVARIANT
	return text();
}

int XmlElemNode::get_text_as_int() const
{
	TWIST_CHECK_INVARIANT
	return text_as_int(*this);  
}

bool XmlElemNode::get_text_as_bool() const
{
	TWIST_CHECK_INVARIANT
	return text_as_bool(*this);
}

void XmlElemNode::set_text(const std::wstring& text)
{
	TWIST_CHECK_INVARIANT
	pimpl_->raw().text().set(text.c_str());
}

void XmlElemNode::set_text_from_bool(bool value)
{
	TWIST_CHECK_INVARIANT
	set_text(value ? L"true" : L"false");
}

void XmlElemNode::set_text_from_int(int value)
{
	TWIST_CHECK_INVARIANT
	set_text(std::to_wstring(value));
}

int XmlElemNode::count_children_with_tag(const std::wstring& tag) const
{
	TWIST_CHECK_INVARIANT
	const auto child_range = pimpl_->raw().children(tag.c_str());
	return static_cast<int>(std::distance(std::begin(child_range), std::end(child_range)));
}

XmlElemNodeList XmlElemNode::get_children_with_tag(const std::wstring& tag) const
{
	TWIST_CHECK_INVARIANT
	return transform_to_vector(pimpl_->raw().children(tag.c_str()), [](auto n) {
		return XmlElemNode::Impl::make_node(n);
	});
}

XmlElemNode XmlElemNode::get_unique_child_with_tag(const std::wstring& tag) const
{
	TWIST_CHECK_INVARIANT
	if (auto child_node = find_unique_child_with_tag(tag)) {
		return *child_node;	
	}
	TWIST_THRO2(L"Parent XML node with tag \"{}\" does not have a child node with tag \"{}\".", this->tag(), tag);
}

std::unique_ptr<XmlElemNode> XmlElemNode::find_unique_child_with_tag(const std::wstring& tag) const
{
	TWIST_CHECK_INVARIANT
	std::unique_ptr<XmlElemNode> child_node;
	XmlElemNodeList child_nodes = get_children_with_tag(tag);
	if (child_nodes.size() > 1) {
		TWIST_THROW(L"Parent XML node with tag \"%s\" has multiple (%d) child nodes with tag \"%s\".", 
				get_tag().c_str(), child_nodes.size(), tag.c_str());
	}
	if (child_nodes.size() == 1) {
		child_node = std::make_unique<XmlElemNode>(child_nodes[0]);
	}
	return child_node;
}

bool XmlElemNode::validate_children(const std::wstring& tag, NodeMult mult, bool checkText, 
		std::wstring* err) const
{
	TWIST_CHECK_INVARIANT
	bool retVal = true;
	
	auto child_nodes = get_children_with_tag(tag);
	
	switch (mult) {
		case k_zero: {
			if (child_nodes.size() != 0) {
				retVal = false;
				if (err != nullptr) {
					const auto descr = format_str(
							L"XML node with tag \"%s\" should have no child XML nodes tagged \"%s\".", 
							get_tag().c_str(), tag.c_str());
					*err = descr.c_str();
				}	
			}
			break;
		}
		case k_zero_or_one: {
			if ((child_nodes.size() != 0) && (child_nodes.size() != 1)) {
				retVal = false;
				if (err != nullptr) {
					const auto descr = format_str(
							L"XML node with tag \"%s\" should have zero or one child XML nodes tagged \"%s\".", 
							get_tag().c_str(), tag.c_str());
					*err = descr.c_str();
				}
			}
			break;
		}
		case k_unique: {
			if (child_nodes.size() != 1) {
				retVal = false;
				if (err != nullptr) {
					const auto descr = format_str(
							L"XML node with tag \"%s\" should have a (unique) child XML node tagged \"%s\".", 
							get_tag().c_str(), tag.c_str());
					*err = descr.c_str();
				}
			}
			break;
		}
		case k_one_or_more: {
			if (child_nodes.size() == 0) {
				retVal = false;
				if (err != nullptr) {
					const auto descr = format_str(
							L"XML node with tag \"%s\" should have one or more child XML nodes tagged \"%s\".", 
							get_tag().c_str(), tag.c_str());
					*err = descr.c_str();							
				}
			}
			break;
		}
		default: {
			assert(false);
		}
	}
	
	if (retVal && checkText) {
		// So far so good, now check that all (if any) of the child nodes have text.
		for (auto it = child_nodes.begin(); it != child_nodes.end(); ++it) {
			if (!it->has_text()) {
				retVal = false;
				break;
			}
		}
		if (!retVal) {
			if (err != nullptr) {
				const auto descr = format_str(
						L"XML node with tag \"%s\" has at least one child XML node tagged \"%s\" which contains no text.", 
						get_tag().c_str(), tag.c_str());
				*err = descr.c_str();							
			}
		}
	}

	return retVal;
}

XmlElemNodeList XmlElemNode::get_descendants_with_tag(const std::wstring& tag) const
{
	TWIST_CHECK_INVARIANT
	XmlElemNodeList descend_elem_nodes;

	std::function<void(pugi::xml_node)> store_children_with_tag = [&](auto parent) {
		for (auto ch : parent.children(tag.c_str())) {
			if (ch.type() == pugi::node_element) {
				descend_elem_nodes.push_back(XmlElemNode::Impl::make_node(ch));
				store_children_with_tag(ch);
			}
		}		
	};

	store_children_with_tag(pimpl_->raw());

	return descend_elem_nodes;
}

bool XmlElemNode::has_attr(const std::wstring& attr_name) const
{
	TWIST_CHECK_INVARIANT
	return pimpl_->raw().attribute(attr_name.c_str());
}

XmlNodeAttr XmlElemNode::get_attr(const std::wstring& attr_name) const
{
	TWIST_CHECK_INVARIANT
	if (auto attr = pimpl_->raw().attribute(attr_name.c_str())) {
		return XmlNodeAttr(attr_name, attr.value());		
	}
	TWIST_THROW(L"XML node \"%s\" does not have an attribute named \"%s\".", 
			tag().c_str(), attr_name.c_str());
}

void XmlElemNode::set_attr(const XmlNodeAttr& attr)
{
	TWIST_CHECK_INVARIANT
	const auto attr_name = attr.get_name();

	auto attribute = pimpl_->raw().attribute(attr_name.c_str());
	if (!attribute) {
		attribute = pimpl_->raw().append_attribute(attr_name.c_str());
	}

	attribute.set_value(attr.get_value().c_str());
}

#ifdef _DEBUG
void XmlElemNode::check_invariant() const noexcept
{
	assert(pimpl_);
	if (pimpl_) {
		assert(pimpl_->raw());
	}
}
#endif

// --- Free functions ---

auto init_xdoc(std::wstring_view root_xnode_tag, unsigned int version) -> std::unique_ptr<XmlDoc>
{
	return XmlDoc::read_from_string(std::format(L"<?xml version=\"1.0\"?><{} {}=\"{}\"></{}>", 
			                                    root_xnode_tag, k_xattr_version, version, root_xnode_tag));
}

auto read_xdoc_and_version_from_file(const fs::path& path, 
									 std::wstring_view root_xnode_tag, 
									 unsigned int& version) -> std::unique_ptr<XmlDoc>
{
	auto xdoc = XmlDoc::read_from_file(path);	
	get_xml_doc_version(*xdoc, root_xnode_tag, version);
	return xdoc;
}

auto read_xdoc_from_file_with_version(const fs::path& path, std::wstring_view root_xnode_tag, unsigned int version)
      -> std::unique_ptr<XmlDoc>
{
	auto file_version = 0u;
	auto xdoc = read_xdoc_and_version_from_file(path, root_xnode_tag, file_version);	
	if (file_version != version) {
		TWIST_THRO2(L"Wrong XML document version {} in file \"{}\"; expected version number {}.", 
				    file_version, path.filename().c_str(), version);
	}
	return xdoc;
}

auto read_xdoc_and_version_from_string(const std::wstring& xml_str, 
									   std::wstring_view root_xnode_tag, 
									   unsigned int& version) -> std::unique_ptr<XmlDoc>
{
	auto xdoc = XmlDoc::read_from_string(xml_str);	
	get_xml_doc_version(*xdoc, root_xnode_tag, version);
	return xdoc;
}

std::unique_ptr<XmlDoc> read_xdoc_from_string_with_version(const std::wstring& xml_str, 
		std::wstring_view root_xnode_tag, unsigned int version)
{
	unsigned int xml_str_version = 0;
	auto xdoc = read_xdoc_and_version_from_string(xml_str, root_xnode_tag, xml_str_version);	

	if (xml_str_version != version) {
		TWIST_THROW(L"Wrong XML document version %d; expected version number %d.", xml_str_version, version);
	}

	return xdoc;
}


void get_xml_doc_version(XmlDoc& xdoc, std::wstring_view root_xnode_tag, unsigned int& version)
{
	auto xroot = xdoc.root_elem();	
	if (xroot.get_tag() != root_xnode_tag) {
		TWIST_THRO2(L"Invalid root XML node tag \"%s\"; the expected tag is \"%s\".", 
				    xroot.get_tag(), root_xnode_tag);	
	}
	version = xroot.get_attr(k_xattr_version).get_int_value();
}


void set_xml_doc_version(XmlDoc& xdoc, std::wstring_view root_xnode_tag, unsigned int version)
{
	XmlElemNode xroot = xdoc.root_elem();	
	if (xroot.get_tag() != root_xnode_tag) {
		TWIST_THROW(L"Invalid root XML node tag \"%s\"; the expected tag is \"%s\".", 
				xroot.get_tag().c_str(), std::wstring{ root_xnode_tag }.c_str());	
	}
	xroot.set_attr( XmlNodeAttr(k_xattr_version, std::to_wstring(version)) );
}

auto text_as_int(const XmlElemNode& xnode) -> int
{
	return to_int_checked(xnode.text());
}

auto text_as_uint32(const XmlElemNode& xnode) -> uint32_t
{
    return to_uint32_checked(xnode.text());
}

auto text_as_ssize(const XmlElemNode& xnode) -> Ssize
{
	return to_ssize_checked(xnode.text());
}

auto text_as_float(const XmlElemNode& xnode) -> float
{
	return to_float_checked(xnode.text());
}

auto text_as_double(const XmlElemNode& xnode) -> double
{
	return to_double_checked(xnode.text());
}

auto text_as_bool(const XmlElemNode& xnode) -> bool
{
	const auto str_value = xnode.text();
	if (wequal_no_case(str_value, L"true")) {
		return true;
	}
	if (wequal_no_case(str_value, L"false")) {
		return false;
	}
	TWIST_THRO2(L"XML node \"{}\" does not contain a boolean value.", xnode.tag());
}

auto text_or_blank(const XmlElemNode& xnode) -> std::wstring
{
	return xnode.has_text() ? xnode.text() : std::wstring{};
}

auto get_unique_child_text(const XmlElemNode& elem, const std::wstring& child_tag) -> std::wstring
{
	auto child_elem = elem.get_unique_child_with_tag(child_tag);
	return child_elem.has_text() ? child_elem.text() : L"";
}

auto get_unique_child_text_or_default(const XmlElemNode& elem,
                                      const std::wstring& child_tag,
                                      std::wstring_view def_val) -> std::wstring
{
	if (auto child_elem = elem.find_unique_child_with_tag(child_tag)) {
	    return child_elem->has_text() ? child_elem->text() : L"";
	}
	return std::wstring{def_val};	
}

auto get_unique_child_bool(const XmlElemNode& elem, const std::wstring& child_tag) -> bool
{
	return text_as_bool(elem.get_unique_child_with_tag(child_tag));	
}

auto get_unique_child_bool_or_default(const XmlElemNode& elem, const std::wstring& child_tag, bool def_val) -> bool
{
	if (auto child_node = elem.find_unique_child_with_tag(child_tag)) {
		return child_node->get_text_as_bool();	
	}
	return def_val;	
}

auto get_unique_child_int(const XmlElemNode& elem, const std::wstring& child_tag) -> int
{
	return text_as_int(elem.get_unique_child_with_tag(child_tag));
}

auto get_optional_unique_child_int(const XmlElemNode& elem, const std::wstring& child_tag) -> std::optional<int>
{
    if (auto child_xelem = elem.find_unique_child_with_tag(child_tag)) {
        return text_as_int(*child_xelem);
    }
    return std::nullopt;
}

auto get_optional_unique_child_uint32(const XmlElemNode& elem, const std::wstring& child_tag) 
      -> std::optional<uint32_t>
{
    if (auto child_xelem = elem.find_unique_child_with_tag(child_tag)) {
        return text_as_uint32(*child_xelem);
    }
    return std::nullopt;
}

auto get_unique_child_double(const XmlElemNode& elem, const std::wstring& child_tag) -> double
{
	return text_as_double(elem.get_unique_child_with_tag(child_tag));
}

auto get_optional_unique_child_double(const XmlElemNode& elem, const std::wstring& child_tag, bool empty_child_allowed) 
      -> std::optional<double>
{
    if (auto child_xelem = elem.find_unique_child_with_tag(child_tag)) {
        if (empty_child_allowed && !child_xelem->has_text()) {
            return std::nullopt;
        }
        return text_as_double(*child_xelem);
    }
    return std::nullopt;
}

} 

/// @file CsvFileWriter.hpp
/// CsvFileWriter class template

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_DB_CSV_FILE_WRITER_HPP
#define TWIST_DB_CSV_FILE_WRITER_HPP

#include "twist/OutTxtFileStream.hpp"

namespace twist::db {

/*! Class whose responsibility is to create a CSV file and write data to it.  
    \tparam Chr  The character type in the input text file; only the char and wchar_t types are currenty supported
 */
template<class Chr>
class CsvFileWriter {
public:
	/*! Constructor.
	    \param[in] path  The CSV file path; if the file already exists, it is overwritten
		\param[in] delimiter  The string which will delimit two adjacent values on the same line
	 */
	CsvFileWriter(fs::path path, std::basic_string<Chr> delimiter = comma_str<Chr>());

	/*! Write a row of data to the file.
	    \tparam Strings  Range type holding elements which can be streamed into OutTxtFileStream<Chr>
	    \param[in] cells  The cell values for the row; an exception is thrown if the number of elements in the range 
		                  does not match the number of cells in the previous row(s), if any
	 */   
	template<rg::input_range Strings> 
	auto write_row(Strings&& cells) -> void;

	/*! Write a row of data to the file.	
		\tparam String  Type which can be streamed into OutTxtFileStream<Chr>
	    \param[in] cells  The cell values for the row; an exception is thrown if the number of elements in the range 
		                  does not match the number of cells in the previous row(s), if any
	 */  
	template<class String> 
	auto write_row(std::initializer_list<String> cells) -> void;

	//! The number of columns, that is, the number of values in each row; or zero if no rows have been written yet. 
	[[nodiscard]] auto nof_columns() const -> Ssize;

	TWIST_NO_COPY_NO_MOVE(CsvFileWriter)

private:	
	const std::basic_string<Chr> delimiter_;
	std::optional<OutTxtFileStream<Chr>> file_stream_;
	Ssize nof_cols_{0};

	TWIST_CHECK_INVARIANT_DECL
};

}

#include "CsvFileWriter.ipp"

#endif 

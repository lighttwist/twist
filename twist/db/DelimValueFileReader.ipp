///  @file  DelimValueFileReader.ipp
///  Inline implementation file for "DelimValueFileReader.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include <numeric>

namespace twist::db {

static const unsigned int  line_buffer_len  = 131072;
static const unsigned int  field_buffer_len = 1024;

//
//  DelimValueFileVanillaHeaderReader struct template
//

template<class Chr, DelimValueFileFormat format>
int DelimValueFileVanillaHeaderReader<Chr, format>::header_line_count() const
{
	return 1;
}


template<class Chr, DelimValueFileFormat format>
std::vector<std::basic_string<Chr>> DelimValueFileVanillaHeaderReader<Chr, format>::get_column_names(
		const std::vector<String>& lines) const
{
	if (size(lines) != 1) {
		TWIST_THROW(L"Exactly 1 header line expected.");
	}
	return tokenise_line<Chr, format>(lines.front());
}

//
//  DelimValueFileReader class template
//

#define TWIST_CLASSTEMPL  template<class Chr, DelimValueFileFormat format>
#define TWIST_CLASSNAME  DelimValueFileReader<Chr, format>
#define TWIST_CLASSTYPENAME  typename DelimValueFileReader<Chr, format>

TWIST_CLASSTEMPL
TWIST_CLASSNAME::DelimValueFileReader(NoHeader, fs::path path)
{
	init(std::move(path));

	// Read the first data line
	if (!read_next_line()) {
		TWIST_THROW(L"Error reading file \"%s\": no lines below the header.", filename().c_str());
	}
	column_count_ = ssize(line_fields_);
	if (column_count_ == 0) {
		TWIST_THROW(L"Error reading file \"%s\": no valid data lines.", filename().c_str());	
	}
	TWIST_CHECK_INVARIANT
}


TWIST_CLASSTEMPL
template<class HeaderReader>
TWIST_CLASSNAME::DelimValueFileReader(WithHeader, fs::path path, const HeaderReader& header_reader)
{
	static_assert(header_reader_type_check()(type_to_val<HeaderReader>), 
			"The template argument supplied for HeaderReader does not satisfy "
			"the HeaderReader concept requiremets.");

	init(std::move(path));

	const auto header_line_count = header_reader.header_line_count();
	std::vector<String> header_lines(header_line_count);
	
	for (auto i = 0; i < header_line_count; ++i) {
		if (!read_next_line()) {
			TWIST_THROW(L"Error reading header from file \"%s\": not enough lines in the file.", 
					filename().c_str());
		}
		header_lines[i] = line_buffer_.get();
	}
	column_names_ = header_reader.get_column_names(header_lines);				
	column_count_ = ssize(column_names_);
	line_fields_.clear();

	// Read the first data line
	if (!read_next_line()) {
		TWIST_THROW(L"Error reading file \"%s\": no lines below the header.", filename().c_str());
	}
	TWIST_CHECK_INVARIANT
}


TWIST_CLASSTEMPL
TWIST_CLASSTYPENAME::StringRange TWIST_CLASSNAME::column_names() const
{
	TWIST_CHECK_INVARIANT
	if (column_names_.empty()) {
		TWIST_THROW(L"A header was not read from file \"%s\".", filename().c_str());
	}
	return StringRange{ column_names_ };
}


TWIST_CLASSTEMPL
template<class ReadDataLine, class>
void TWIST_CLASSNAME::read_data(const ReadDataLine& read_data_line)
{
	TWIST_CHECK_INVARIANT
	read_impl({}, read_data_line);
}


TWIST_CLASSTEMPL
template<class ReadDataLine, class>
void TWIST_CLASSNAME::read_data(std::vector<Ssize> column_index_filter, const ReadDataLine& read_data_line)
{
	TWIST_CHECK_INVARIANT
	if (column_index_filter.empty()) {
		TWIST_THROW(L"Column filter cannot be empty.");
	}
	read_impl(move(column_index_filter), read_data_line);
}

template<class Chr>
std::basic_string<Chr> view_to_string(std::basic_string_view<Chr> str_view)
{
	std::basic_string<Chr> str{ str_view };
	return str;
}



TWIST_CLASSTEMPL
template<class ReadDataLine, class>
void TWIST_CLASSNAME::read_data(const std::vector<StringView>& column_name_filter, 
		const ReadDataLine& read_data_line)
{
	TWIST_CHECK_INVARIANT
	const auto col_names = column_names();
	auto column_index_filter = transform_to_vector(column_name_filter, [&](auto name) { 
		const auto pos = position_of(col_names, name); 
		if (pos == twist::k_no_pos) {
			TWIST_THROW(L"Column with name \"%s\" not found in file \"%s\".",
					twist::to_string(name).c_str(), filename().c_str());
		}
		return pos;
	});
	read_data(move(column_index_filter), read_data_line);
}

TWIST_CLASSTEMPL
template<class ReadDataLine, class>
void TWIST_CLASSNAME::read_data(std::initializer_list<const Chr*> column_name_filter, 
		const ReadDataLine& read_data_line)
{
	TWIST_CHECK_INVARIANT
	auto v = column_name_filter | vw::transform([](auto* str) { return StringView{str}; })
	                            | rg::to<std::vector>();
	read_data(v, read_data_line);
}

TWIST_CLASSTEMPL
std::unique_ptr<NullableStringTable<Chr>> 
TWIST_CLASSNAME::read_data_into_string_table()
{
	init_column_filter({});

	const auto table_col_count = ssize(column_filter_);
	auto table = std::make_unique<NullableStringTable<Chr>>(table_col_count);

	do {
		const auto row = table->add_row();
		for (auto col = 0; col < table_col_count; ++col) {
			// Read the text field delimited by the value separators
			const auto value_field = format == DelimValueFileFormat::comma_sep ? 
					trim_whitespace(line_fields_[column_filter_[col]]) : line_fields_[column_filter_[col]];								
			// Store the value in the new data matrix row
			table->set_cell(row, col, value_field.c_str());	
		}	
	}
	while (read_next_line());

	return table;
}


TWIST_CLASSTEMPL
void TWIST_CLASSNAME::init(fs::path&& path) 
{
	auto file = std::make_unique<FileStd>(std::move(path), FileOpenMode::read);
	file_stream_ = std::make_unique<InTextFileStream<Chr>>(move(file)); 
	line_buffer_  = std::make_unique<Chr[]>(line_buffer_len  * sizeof(Chr));
	field_buffer_ = std::make_unique<Chr[]>(field_buffer_len * sizeof(Chr));
}


TWIST_CLASSTEMPL
template<class ReadDataLine>
void TWIST_CLASSNAME::read_impl(std::vector<Ssize>&& column_index_filter, const ReadDataLine& read_data_line)
{
	TWIST_CHECK_INVARIANT
	init_column_filter(move(column_index_filter));
	std::vector<StringView> filtered_fields( size(column_filter_) );
	do {
		for (auto i = 0; i < ssize(column_filter_); ++i) {
			filtered_fields[i] = line_fields_[column_filter_[i]];
		}	
		read_data_line(filtered_fields);
	}
	while (read_next_line());
}


TWIST_CLASSTEMPL
bool TWIST_CLASSNAME::read_next_line()
{
	TWIST_CHECK_INVARIANT
	bool valid_line_read{ false };
	Ssize line_len{};
	do {
		if (file_stream_->is_eof()) {
			return false;
		}
		file_stream_->get_line(line_buffer_.get(), line_buffer_len, line_len);		
		// Ignore blank lines
		if (!is_whitespace(line_buffer_.get(), line_buffer_.get() + line_len)) {			
			++line_no_;
			line_buffer_[line_len] = 0;  // Append null terminator
			line_fields_ = tokenise_line<Chr, format>(line_buffer_.get());
			if (column_count_ != 0 && column_count_ != ssize(line_fields_)) {
				TWIST_THROW(L"Error reading file \"%s\": invalid number (%d) of values on line %d.",
						filename().c_str(), ssize(line_fields_), line_no_);
			}
			valid_line_read = true;
		}
	}
	while (!valid_line_read);
	return true;
}


TWIST_CLASSTEMPL
void TWIST_CLASSNAME::init_column_filter(std::vector<Ssize>&& src)
{
	TWIST_CHECK_INVARIANT
	if (!src.empty()) {
		if (auto it = find_if(src, [this](auto idx) { return idx >= column_count_; }); 
				 it != end(src)) {
			TWIST_THROW(L"Invalid column index %d in column filter.", *it);
		}
		column_filter_ = move(src);
	}
	else {
		column_filter_.resize(column_count_);
		std::iota(begin(column_filter_), end(column_filter_), Ssize{ 0 });
	}
}


TWIST_CLASSTEMPL
fs::path TWIST_CLASSNAME::filename() const
{
	TWIST_CHECK_INVARIANT
	return file_stream_->path().filename();
}


TWIST_CLASSTEMPL
constexpr auto TWIST_CLASSNAME::header_reader_type_check()
{
	// The following member functions are required of type T of x:
	//   header_line_count() const;
	//   get_column_names(const std::vector<String>&) const -> std::vector<String>;

	return is_valid_lambda_decl([](auto x) -> std::void_t<
					decltype( int{ 
							const_type_from_val(x).header_line_count() } ),
					decltype( std::vector<String>{  
							const_type_from_val(x).get_column_names(std::vector<String>{}) } )> { });	
}


#ifdef _DEBUG
TWIST_CLASSTEMPL
void TWIST_CLASSNAME::check_invariant() const noexcept
{
	assert(file_stream_);
	assert(line_buffer_);
	assert(field_buffer_);
	if (!column_names_.empty()) {
		assert(column_count_ > 0);
		assert(ssize(column_names_) == column_count_);
	}
	if (!line_fields_.empty() && column_count_ > 0) {
		assert(ssize(line_fields_) == column_count_);
	}
}
#endif

#undef  TWIST_CLASSNAME
#undef  TWIST_CLASSTEMPL

//
//  Free functions
//

namespace detail {

template<class Chr>
constexpr const Chr* wspace_delims = nullptr;

template<>
constexpr const char* wspace_delims<char> = " \t";

template<>
constexpr const wchar_t* wspace_delims<wchar_t> = L" \t";

}

template<class Chr, 
         DelimValueFileFormat format>
std::vector<std::basic_string<Chr>> tokenise_line(std::basic_string_view<Chr> line)
{
	if constexpr (format == DelimValueFileFormat::comma_sep) {
		return split_with_quotes(line);
	}
	else if constexpr (format == DelimValueFileFormat::wspace_sep) {
		return tokenise<Chr>(line, gsl::not_null{ detail::wspace_delims<Chr> });
	}
	else {
		TWIST_THROW(L"Invalid \"delimited-values file format\" value %d.", format);
	}
}

}

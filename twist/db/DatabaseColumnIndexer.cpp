///  @file  DatabaseColumnIndexer.cpp
///  Implementation file for "DatabaseColumnIndexer.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "DatabaseColumnIndexer.hpp"

namespace twist::db {

unsigned int DatabaseColumnIndexer::get_col_idx(std::string_view col_name) const
{
	TWIST_CHECK_INVARIANT
	const auto it = col_name_indexes_.find(col_name);
	if (it == end(col_name_indexes_)) {
		TWIST_THROW(L"Column \"%s\" not found.", ansi_to_string(col_name).c_str());
	}
	return it->second;
}


std::string DatabaseColumnIndexer::get_col_name(unsigned int col_idx) const
{
	TWIST_CHECK_INVARIANT
	const auto it = find_if(col_name_indexes_, [=](const std::pair<std::string, unsigned int>& el) {
		return el.second == col_idx;	
	});
	if (it == end(col_name_indexes_)) {
		TWIST_THROW(L"Invalid column index %d.", col_idx);
	}
	return it->first;
}


std::wstring DatabaseColumnIndexer::try_get_col_wname(unsigned int col_idx) const
{
	TWIST_CHECK_INVARIANT
	try {
		return ansi_to_string(get_col_name(col_idx));
	}
	catch (std::exception&) {
		assert(false);
		return L"";
	}
}


unsigned int DatabaseColumnIndexer::nof_columns() const
{
	TWIST_CHECK_INVARIANT
	return num_cols_;
}


#ifdef _DEBUG
void DatabaseColumnIndexer::check_invariant() const noexcept
{
	assert(col_name_indexes_.size() == num_cols_);
	assert(num_cols_ > 0);
}
#endif 

}


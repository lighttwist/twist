/// @file BinmatFile.ipp
/// Inline implementation file for "BinmatFile.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/type_info_utils.hpp"

namespace twist::db {

// --- BinmatFile class template ---

template<class DataVal>
auto BinmatFile<DataVal>::create_for_single_matrix(fs::path path, 
												   Ssize nof_rows, 
												   Ssize nof_cols, 
												   std::string nodata_value) -> std::unique_ptr<BinmatFile>
{
	if (exists(path)) {
		TWIST_THRO2(L"File \"{}\" already exists.", path.c_str());
	}
	return std::unique_ptr<BinmatFile<DataVal>>{
		           new BinmatFile<DataVal>{std::make_unique<FileStd>(std::move(path), FileOpenMode::create_write), 
			                               nof_rows, 
									       nof_cols, 
										   1/*nof_mats*/,
									       nodata_value}};
}

template<class DataVal>
auto BinmatFile<DataVal>::open_for_single_matrix(fs::path path, 
												 Ssize nof_rows, 
												 Ssize nof_cols, 
												 std::string nodata_value, 
												 bool read_only) -> std::unique_ptr<BinmatFile>
{
	auto file = std::make_unique<FileStd>(std::move(path), read_only ? FileOpenMode::read : FileOpenMode::read_write);
	// Check that the file size is compatible with the matrix dimensions
	if (file->get_file_size() != nof_rows * nof_cols * ssizeof<DataVal>) {
		TWIST_THRO2(L"The size of binmat file \"{}\" is not compatible with the given matrix dimensions "
		            L"({}, {}).", file->path().filename().c_str(), nof_rows, nof_cols);
	}
	return std::unique_ptr<BinmatFile<DataVal>>{new BinmatFile<DataVal>{move(file), 
	                                                                    nof_rows, 
																		nof_cols, 
																		1/*nof_mats*/,
																		nodata_value}};
}

template<class DataVal>
auto BinmatFile<DataVal>::create_for_matrix_stack(fs::path path, 
												  Ssize nof_rows, 
												  Ssize nof_cols, 
												  Ssize nof_mats, 
												  std::string nodata_value) -> std::unique_ptr<BinmatFile>
{
	if (exists(path)) {
		TWIST_THRO2(L"File \"{}\" already exists.", path.c_str());
	}
	return std::unique_ptr<BinmatFile<DataVal>>{
		           new BinmatFile<DataVal>{std::make_unique<FileStd>(std::move(path), FileOpenMode::create_write), 
			                               nof_rows, 
									       nof_cols, 
										   nof_mats,
									       nodata_value}};
}

template<class DataVal>
auto BinmatFile<DataVal>::open_for_matrix_stack(fs::path path, 
												Ssize nof_rows, 
												Ssize nof_cols, 
												Ssize nof_mats, 
												std::string nodata_value, 
												bool read_only) -> std::unique_ptr<BinmatFile>
{
	auto file = std::make_unique<FileStd>(std::move(path), read_only ? FileOpenMode::read : FileOpenMode::read_write);
	// Check that the file size is compatible with the matrix dimensions
	if (file->get_file_size() != nof_rows * nof_cols * nof_mats * ssizeof<DataVal>) {
		TWIST_THRO2(L"The size of binmat file \"{}\" is not compatible with the given matrix stack dimensions "
		            L"({}, {}, {}).", file->path().filename().c_str(), nof_rows, nof_cols, nof_mats);
	}
	return std::unique_ptr<BinmatFile<DataVal>>{new BinmatFile<DataVal>{move(file), 
	                                                                    nof_rows, 
																		nof_cols, 
																		nof_mats,
																		nodata_value}};
}

template<class DataVal>
auto BinmatFile<DataVal>::nof_rows() const -> Ssize
{
	TWIST_CHECK_INVARIANT
	return nof_rows_;
}

template<class DataVal>
auto BinmatFile<DataVal>::nof_columns() const -> Ssize
{
	TWIST_CHECK_INVARIANT
	return nof_cols_;
}

template<class DataVal>
auto BinmatFile<DataVal>::nof_matrices() const -> Ssize
{
	TWIST_CHECK_INVARIANT
	return nof_mats_;
}

template<class DataVal>
auto BinmatFile<DataVal>::nodata_value() const -> std::string 
{
	TWIST_CHECK_INVARIANT
	return nodata_value_;
}

template<class DataVal>
BinmatFile<DataVal>::BinmatFile(std::unique_ptr<FileStd> file, Ssize nof_rows, Ssize nof_cols, Ssize nof_mats, 
		                        std::string nodata_value)
	: file_{move(file)}
	, nof_rows_{nof_rows}
	, nof_cols_{nof_cols}
	, nof_mats_{nof_mats}
	, nodata_value_{move(nodata_value)}
{
	check_dimensions();
	TWIST_CHECK_INVARIANT
}

template<class DataVal> 
auto BinmatFile<DataVal>::read_whole_to_single_matrix(const DataVal& init_val) const -> MatrixData<DataVal>
{
	TWIST_CHECK_INVARIANT
	auto data = MatrixData<DataVal>{nof_rows_, nof_cols_, init_val};
	file_->read_buf(reinterpret_cast<char*>(data.data()), data.size() * sizeof(DataVal));
	return data;
}

template<class DataVal>
auto BinmatFile<DataVal>::read_rows_to_single_matrix(Ssize begin_row, Ssize end_row, MatrixData<DataVal>& data) const 
      -> void
{
	TWIST_CHECK_INVARIANT
	check_begin_end_rows(begin_row, end_row);
	const auto nof_rows_to_read = end_row - begin_row + 1;

	if (data.nof_columns() != nof_cols_) {
		TWIST_THRO2(L"The output data matrix has the wrong number of columns.");
	}
	if (data.nof_rows() < nof_rows_to_read) {
		TWIST_THRO2(L"The output data matrix does not have sufficient rows to store the data.");
	}	
	
	const auto read_pos = begin_row * nof_cols_ * sizeof(DataVal);
	file_->seek(read_pos, FileSeekDir::seek_begin);
	file_->read_buf(reinterpret_cast<char*>(data.data()), nof_rows_to_read * nof_cols_ * sizeof(DataVal));
}

template<class DataVal> 
auto BinmatFile<DataVal>::read_submatrix_to_single_matrix(const SubgridInfo& submatrix_info, 
                                                          MatrixData<DataVal>& data) const -> void
{
	TWIST_CHECK_INVARIANT
	check_submatrix(submatrix_info);

	const auto nof_rows_to_read = twist::nof_rows(submatrix_info);
	const auto nof_cols_to_read = twist::nof_columns(submatrix_info);
	if (data.nof_rows() < nof_rows_to_read) {
		TWIST_THRO2(L"The output data matrix does not have enough rows to store the data.");
	}
	if (data.nof_columns() < nof_cols_to_read) {
		TWIST_THRO2(L"The output data matrix does not have enough columns to store the data.");
	}

	if (nof_cols_to_read == nof_cols_ && data.nof_columns() == nof_cols_) {
		read_rows_to_single_matrix(submatrix_info.begin_row(), submatrix_info.end_row(), data);
		return;
	}

	auto read_pos = (submatrix_info.begin_row() * nof_cols_ + submatrix_info.begin_column()) * sizeof(DataVal);
	auto write_addr = reinterpret_cast<char*>(data.data());
	for ([[maybe_unused]] auto i : IndexRange{nof_rows_to_read}) {
		file_->seek(read_pos, FileSeekDir::seek_begin);
		file_->read_buf(write_addr, nof_cols_to_read * sizeof(DataVal));	
		read_pos += nof_cols_ * sizeof(DataVal); 
		write_addr += data.nof_columns() * sizeof(DataVal);
	}
}

template<class DataVal> 
auto BinmatFile<DataVal>::read_whole_to_matrix_stack() const -> MatrixStackData<DataVal>
{
	TWIST_CHECK_INVARIANT
	auto data = MatrixStackData<DataVal>{nof_rows_, nof_cols_, nof_mats_};
	file_->read_buf(reinterpret_cast<char*>(data.data()), data.size() * sizeof(DataVal));
	return data;
}

template<class DataVal> 
auto BinmatFile<DataVal>::fill(DataVal value) -> void
{
	TWIST_CHECK_INVARIANT
	// Write the data, one row at a time
	auto buffer = std::vector<DataVal>(nof_cols_, value);
	for ([[maybe_unused]] auto i : IndexRange{nof_rows_}) {
		file_->write_buf(reinterpret_cast<const char*>(buffer.data()), nof_cols_ * sizeof(DataVal));	
	}
}

template<class DataVal> 
auto BinmatFile<DataVal>::write_whole_data_to_whole_file(const MatrixData<DataVal>& data) -> void
{
	TWIST_CHECK_INVARIANT
	if (data.nof_rows() != nof_rows_ || data.nof_columns() != nof_cols_) {
		TWIST_THRO2(L"Data buffer has the wrong dimensions {}, {} (expected {}, {}).", 
		            data.nof_rows(), data.nof_columns(), nof_rows_, nof_cols_);
	}
	file_->write_buf(reinterpret_cast<const char*>(data.data()), data.size() * sizeof(DataVal));
}

template<class DataVal> 
template<class Transformation, class>
void BinmatFile<DataVal>::write_whole_data_to_whole_file(MatrixData<DataVal>&& data, 
                                                         std::optional<Transformation> transform)
{
	TWIST_CHECK_INVARIANT
	if (data.nof_rows() != nof_rows_ || data.nof_columns() != nof_cols_) {
		TWIST_THROW(L"Data matrix has the wrong dimensions %d, %d (expected %d, %d).", 
				    data.nof_columns(), data.nof_rows(), nof_cols_, nof_rows_);
	}
	if (transform) {
		std::transform(data.begin(), data.end(), data.begin(), *transform);
	}
	file_->write_buf(reinterpret_cast<const char*>(data.data()), data.size() * sizeof(DataVal));
}

template<class DataVal> 
template<class T, class Transformation, class>
void BinmatFile<DataVal>::write_whole_data_to_whole_file(const MatrixData<T>& data, 
		std::optional<Transformation> transform)
{
	TWIST_CHECK_INVARIANT
	static_assert(!std::is_same_v<DataVal, T>, "Please call the overload, and std::move the first argument.");

	//+TODO: This can be optimised to write in chunks
	write_data_part_to_whole_file(data, 
			                      {0, data.nof_rows() - 1, 0, data.nof_columns() - 1},
			                      transform);
}

template<class DataVal>
template<class Cont, class>
void BinmatFile<DataVal>::write_whole_data_to_file_rows(Ssize begin_file_row, Ssize end_file_row, const Cont& data)
{
	TWIST_CHECK_INVARIANT
	check_begin_end_rows(begin_file_row, end_file_row);
	if (ssize(data) != (end_file_row - begin_file_row + 1) * nof_cols_) {
		TWIST_THROW(L"The input data has invalid size.");
	}
	const auto write_pos = begin_file_row * nof_cols_ * sizeof(DataVal);
	file_->seek(write_pos, FileSeekDir::seek_begin);
	file_->write_buf(reinterpret_cast<const char*>(data.data()), data.size() * sizeof(DataVal));
}

template<class DataVal> 
template<class Transformation, class>
void BinmatFile<DataVal>::write_data_part_to_whole_file(MatrixData<DataVal>&& data, 
		                                                const SubgridInfo& data_part_info, 
														std::optional<Transformation> transform)
{
	TWIST_CHECK_INVARIANT
	if (nof_rows(data_part_info) != nof_rows_ || nof_columns(data_part_info) != nof_cols_) {
		TWIST_THRO2(L"Data matrix has the wrong dimensions {}, {} (expected {}, {}).", 
					nof_columns(data_part_info), nof_rows(data_part_info), nof_cols_, nof_rows_);
	}

	for (auto i = data_part_info.begin_row(); i <= data_part_info.end_row(); ++i) {
		auto src_row = data.view_row_cells(i, data_part_info.begin_column(), nof_columns(data_part_info));
		if (transform) {
			transform(src_row, src_row.begin(), *transform);	
		}
		file_->write_buf(reinterpret_cast<const char*>(src_row.data()), src_row.size() * sizeof(DataVal));
	}
}

template<class DataVal> 
template<class T, class Transformation, class>
void BinmatFile<DataVal>::write_data_part_to_whole_file(const MatrixData<T>& data, 
		                                                const SubgridInfo& data_part_info, 
														std::optional<Transformation> transform)
{
	TWIST_CHECK_INVARIANT
	static_assert(!std::is_same_v<DataVal, T>, "Please call the overload, and std::move the first argument.");

	if (twist::nof_rows(data_part_info) != nof_rows_ || twist::nof_columns(data_part_info) != nof_cols_) {
		TWIST_THROW(L"Data matrix has the wrong dimensions %dx%d (expected %dx%d).", 
					twist::nof_columns(data_part_info), twist::nof_rows(data_part_info), 
				    nof_cols_, nof_rows_);
	}

	auto row_buffer = std::vector<DataVal>(nof_cols_);
	for (auto i = data_part_info.begin_row(); i <= data_part_info.end_row(); ++i) {
		
		const auto src_row = data.view_row_cells(i, data_part_info.begin_column(), twist::nof_columns(data_part_info));

		if (transform) {
			rg::transform(src_row, row_buffer.begin(), *transform);
		}
		else {
			rg::transform(src_row, row_buffer.begin(), [](auto x) { return static_cast<DataVal>(x); });
		}

		file_->write_buf(reinterpret_cast<const char*>(row_buffer.data()), 
				         row_buffer.size() * sizeof(DataVal));
	}
}

template<class DataVal>
auto BinmatFile<DataVal>::write_whole_data_to_whole_file(const MatrixStackData<DataVal>& data) -> void
{
	TWIST_CHECK_INVARIANT
	if (data.nof_rows() != nof_rows_ || data.nof_columns() != nof_cols_ || data.nof_matrices() != nof_mats_) {
		TWIST_THRO2(L"Data buffer has the wrong dimensions {}, {}, {} (expected {}, {}, {}).", 
		            data.nof_rows(), data.nof_columns(), data.nof_matrices(), nof_rows_, nof_cols_, nof_mats_);
	}
	file_->write_buf(reinterpret_cast<const char*>(data.data()), data.size() * sizeof(DataVal));
}

template<class DataVal>
auto BinmatFile<DataVal>::check_begin_end_rows(Ssize begin_row, Ssize end_row) const -> void
{
	TWIST_CHECK_INVARIANT
	if (begin_row < 0 || nof_rows_ <= begin_row) {
		TWIST_THRO2(L"Invalid begin row index {}.", begin_row);
	}
	if (end_row < 0 || nof_rows_ <= end_row) {
		TWIST_THRO2(L"Invalid end row index {}.", begin_row);
	}
	if (begin_row > end_row) {
		TWIST_THRO2(L"The end row index {} is smaller than the begin row index {}.", end_row, begin_row);
	}
}

template<class DataVal>
auto BinmatFile<DataVal>::check_submatrix(const SubgridInfo& submatrix_info) const -> void
{
	TWIST_CHECK_INVARIANT
	if (submatrix_info.end_row() >= nof_rows_ || submatrix_info.end_column() >= nof_cols_) {
		TWIST_THRO2(L"The submatrix does not fit within the binmat.");
	}
}

template<class DataVal>
auto BinmatFile<DataVal>::check_dimensions() const -> void
{
	if (nof_rows_ < 0 || nof_cols_ < 0 || nof_mats_ < 0) {
		TWIST_THRO2(L"Invalid matrix (stack) dimension sizes for binmat file: {}, {}, {}.", 
		            nof_rows_, nof_cols_, nof_mats_);
	}
	if (nof_rows_ != 0 || nof_cols_ != 0 || nof_mats_ != 0) {
		if (nof_rows_ == 0 || nof_cols_ == 0 || nof_mats_ == 0) {
			TWIST_THRO2(L"Invalid matrix (stack) dimension sizes for binmat file: {}, {}, {}.", 
			            nof_rows_, nof_cols_, nof_mats_);		
		}
	}
}

#ifdef _DEBUG
template<class DataVal>
auto BinmatFile<DataVal>::check_invariant() const noexcept -> void
{
	assert(file_);
	assert(nof_rows_ > 0);
	assert(nof_cols_ > 0);
	if (!nodata_value_.empty()) {
		assert(!is_whitespace(nodata_value_));
	}
}
#endif 

// --- Free functions ---

namespace detail { 

template<class DataVal>
auto calc_binmat_chunk_row_count(Ssize nof_binmat_cols) -> Ssize
{
	static const unsigned long long max_chunk_size = 5242880;  // 5MB in bytes

	auto row_size = nof_binmat_cols * sizeof(DataVal);
	return max_chunk_size / row_size;
}

template<class DataVal,
		 class Transformation, 
	     class = EnableIfFuncR<DataVal, Transformation, DataVal>>
auto transform_binmat_impl(BinmatFile<DataVal>& src_binmat, BinmatFile<DataVal>& dest_binmat, Transformation transform) 
      -> void
{
	using MatrixData = typename BinmatFile<DataVal>::template MatrixData<DataVal>;

	const auto nof_rows = src_binmat.nof_rows();
	assert(dest_binmat.nof_rows() == nof_rows);
	const auto nof_cols = src_binmat.nof_columns();
	assert(dest_binmat.nof_columns() == nof_cols);

	const auto full_chunk_row_count = calc_binmat_chunk_row_count<DataVal>(nof_cols);
	const auto data_row_count = std::min(full_chunk_row_count, nof_rows);

	auto data = MatrixData{data_row_count, nof_cols};
	auto data_begin = data.data();

	auto chunk_begin_row = Ssize{0};
	auto last_chunk = false;

	while (!last_chunk) {
		// Work out the rows in the current chunk (the last chunk is often smaller)
		auto chunk_end_row = chunk_begin_row + full_chunk_row_count - 1;
		if (chunk_end_row >= nof_rows) {
			chunk_end_row = nof_rows - 1;
			last_chunk = true;
		}
		// Read the current chunk from the file into the data matrix
		src_binmat.read_rows_to_single_matrix(chunk_begin_row, chunk_end_row, data);

		const auto chunk_row_count = chunk_end_row - chunk_begin_row + 1;

		// Apply the transformation to the rows which were read into the data matrix (but only if the transformtion is
		// not, verifiably, the identity transformation.
		if constexpr (!std::is_same_v<Transformation, std::identity>) {
			const auto chunk_val_count = chunk_row_count * nof_cols;
			const auto data_end = data_begin + chunk_val_count;
			std::transform(data_begin, data_end, data_begin, transform);
		}
		// Write the chunk back to the bitmat file
		dest_binmat.write_whole_data_to_file_rows(chunk_begin_row, chunk_end_row, 
				                                  data.view_rows(0, chunk_row_count - 1));
		chunk_begin_row = chunk_end_row + 1;
	}	
}

}

template<class DataVal, class Transformation, class>
auto transform_binmat(BinmatFile<DataVal>& binmat, Transformation transform) -> void
{
	detail::transform_binmat_impl(binmat, binmat, transform);
}

template<class DataVal, class Transformation, class>
auto transform_binmat(BinmatFile<DataVal>& src_binmat, fs::path dest_binmat_path, Transformation transform) -> void
{
	auto dest_binmat = BinmatFile<DataVal>::create_for_single_matrix(std::move(dest_binmat_path), 
																     src_binmat.nof_rows(),
																     src_binmat.nof_columns(),
																     src_binmat.nodata_value());
	detail::transform_binmat_impl(src_binmat, *dest_binmat, transform);
}

}

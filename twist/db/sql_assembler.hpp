/// @file sql_assembler.hpp
/// Utilities for assembling SQL queries

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_DB_SQL_ASSEMBLER_HPP
#define TWIST_DB_SQL_ASSEMBLER_HPP

#include <sstream>

namespace twist::db {

//! A pair consisting of the database table column name and the corresponding value, for assembling into an SQL query.
class SqlNameValuePair {
public:
	SqlNameValuePair(std::string name, std::string value);    // For TEXT columns; the value string sholdn't be between quotes
	SqlNameValuePair(std::string name, std::wstring value);   // For TEXT columns; the value string sholdn't be between quotes nor contain characters inconvertible to "narrow"
	SqlNameValuePair(std::string name, int value);            // For INTEGER columns
	SqlNameValuePair(std::string name, unsigned int value);   // For INTEGER columns
	SqlNameValuePair(std::string name, long value);           // For INTEGER columns
	SqlNameValuePair(std::string name, unsigned long value);  // For INTEGER columns
	SqlNameValuePair(std::string name, double value);         // For REAL columns
	std::string get_name() const;
	std::string get_value_str() const;

private:
	std::string name_;
	std::string value_str_;
	TWIST_CHECK_INVARIANT_DECL
};

using SqlNameValuePairs = std::vector<SqlNameValuePair>;

/// Assemble an SQL INSERT query.
///
/// @param[in] table_name  The table name
/// @param[in] values  List of column name - value pairs
/// @return  The SQL query
/// 
std::string assemble_insert_sql(const std::string& table_name, const SqlNameValuePairs& values);

/// Given a list of field name - value pairs, remove the first pair with a specific column name.
///
/// @param[in] col_name  The column name
/// @param[in] values  The list of column name - value pairs
/// @return  true if a matching pair was found and removed
///
bool remove_sql_value(std::string_view col_name, SqlNameValuePairs& values);

/// Assemble an SQL "IN" clause from a range of elements convertible to string.
/// Examples: 
///    {1, 5, 3} -> "IN ("='1', '5', '3')" if quoted = true, or "IN ("='1', '5', '3')" if quoted = false
///    {"Bob", "Rob", "Job"} -> "IN ('Bob', 'Rob', 'Job')" if quoted = true, or "IN (Bob, Rob, Job)" if quoted = false
///
/// @tparam  TRange  The range type; models the "Range" concept (from the twist library)
/// @param[in] range  The range of items
/// @param[in] quoted  Whether each range elment string should be enclosed in (double) quotes  
/// @return  The assembled "IN" clause
///
template<class TRange> 
std::string assemble_in_clause(const TRange& range, bool quoted);

/// Assemble an SQL "IN" clause from a range of elements convertible to string.
/// Examples: 
///    {1, 5, 3} -> "IN ("='1', '5', '3')" if quoted = true, or "IN ("='1', '5', '3')" if quoted = false
///    {"Bob", "Rob", "Job"} -> "IN ('Bob', 'Rob', 'Job')" if quoted = true, or "IN (Bob, Rob, Job)" if quoted = false
///
/// @tparam  TRange  The range type; models the "Range" concept (from the twist library)
/// @tparam  TConverter  Functor type which converts a range element to a text-streamable value
/// @param[in] range  The range of items
/// @param[in] quoted  Whether each range elment string should be enclosed in (double) quotes  
/// @param[in] converter  Functor which converts a range element to a text-streamable value
/// @return  The assembled "IN" clause
///
template<class TRange, class TConverter> 
std::string assemble_in_clause(const TRange& range, bool quoted, TConverter&& converter);

}  

#include "twist/db/sql_assembler.ipp"

#endif  

///  @file  DelimValueFileReader.hpp
///  DelimValueFileReader  class template

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_DB_DELIM_VALUE_FILE_READER_HPP
#define TWIST_DB_DELIM_VALUE_FILE_READER_HPP

#include "twist/InTextFileStream.hpp"
#include "twist/NullableStringTable.hpp"
#include "twist/std_vector_utils.hpp"

namespace twist::db {

/// The delimited-value text file format
enum class DelimValueFileFormat {  
	/// Comma separated values; text found between paired quote characters is considered one value
	comma_sep = 1,  
	/// Whitespace separated values; quote characters are not considered special
	wspace_sep = 2
};

/// Tag type for function tag dispatching; signifies a file format with a header
enum class WithHeader {};
/// Tag type for function tag dispatching; signifies a file format without a header
enum class NoHeader {};  
/// Tag value for function tag dispatching; signifies a file format with a header
const WithHeader with_header{};  
/// Tag value for function tag dispatching; signifies a file format without a header
const NoHeader no_header{};  

/// The default (or "vanilla") implementation of the "HeaderReader" concept related to the 
/// DelimValueFileReader class template: it reads one line at the top of the "delimited-value text" file and 
/// simply splits it into separate colum names based on the file format.
///
/// @tparam  Chr  The character type in the input text file; only the char and wchar_t types are currenty 
///			    supported
/// @tparam  format  The delimited-value text file format
///
template<class Chr, DelimValueFileFormat format>
struct DelimValueFileVanillaHeaderReader {

	using String = std::basic_string<Chr>;

	/// The number of lines in the header.
	int header_line_count() const;
	
	/// Given the line(s) making up the header at the top of the file, work out number and names of the 
	/// columns present in the file.
	///
	/// @param[in] lines  The header lines
	/// @return  The column names
	///
	std::vector<String> get_column_names(const std::vector<String>& lines) const;
};

/// Class which reads the contents of a "delimited-value text" file. The class requires that each line in the 
/// "data" part of the file (ie the part excluding the header line(s)) contain the same number of delimited
/// values, which is the number of columns in the file; that is, the file is required to contain a data grid.
///
/// @tparam  Chr  The character type in the input text file; only the char and wchar_t types are currenty 
///			    supported
/// @tparam  format  The delimited-value text file format
///
template<class Chr, 
         DelimValueFileFormat format>
class DelimValueFileReader {
public:
	using String = std::basic_string<Chr>;
	using StringView = std::basic_string_view<Chr>;
	using StringRange = VectorCelemRange<String>;

	/// Constructor. Use this constructor to open a "delimited-value text" file which does not contain a 
	/// header (ie one or more lines at the beginning of the file which specify the column names).
	///
	/// @param[in] tag  Tag value for tag dispatching; use no_header  
	/// @param[in] path  The file path; an exception is thrown if it does not exist
	///
	DelimValueFileReader(NoHeader tag, fs::path path);

	/// Constructor. Use this constructor to open a "delimited-value text" file which contains a header (ie 
	/// one or more lines at the beginning of the file which specify the column names).
	///
	/// @tparam  HeaderReader  Type for reading the file header line(s); it should satisfy the "HeaderReader"
	///					concept requirements; see DelimValueFileVanillaHeaderReader class declaration for an
	///					example
	/// @param[in] tag  Tag value for tag dispatching; use with_header  
	/// @param[in] path  The file path; an exception is throw if it does not exist
	/// @param[in] header_reader  Object for reading the file header line(s)
	///
	template<class HeaderReader = DelimValueFileVanillaHeaderReader<Chr, format>>
	DelimValueFileReader(WithHeader tag, fs::path path, 
			const HeaderReader& header_reader = DelimValueFileVanillaHeaderReader<Chr, format>{});

	/// Get the names of the columns present in the file, if the file has a header which specifies the column
	/// names. If the file was not opened as containing a header, an exception is thrown.
	///
	/// @return  The column names
	///
	StringRange column_names() const;  

	/// Read, row by row, the data contents (not including any header lines, if the file was opened as having 
	/// a header) of all columns from the file.
	///
	/// @tparam  ReadDataLine  Callback function type
	/// @param[in] read_data_line  Callback function which receives the cell values for each row in the 
	///					file; the values appear in the vector in the same order as the corresponding column 
	///					headers appear in the file
	///
	template<class ReadDataLine,
	         class = EnableIfFunc<ReadDataLine, const std::vector<StringView>&>>
	void read_data(const ReadDataLine& read_data_line);

	/// Read, row by row, the data contents (not including any header lines, if the file was opened as having 
	/// a header) of a set of columns from the file.
	///
	/// @tparam  ReadDataLine  Callback function type
	/// @param[in] column_index_filter  A filter specifying the indexes of those columns whose data should be 
	///					read from the file
	/// @param[in] read_data_line  Callback function which receives the cell values for each row in the 
	///					file; the values appear in the vector in the same order as the corresponding columns 
	///					appear in the filter
	///
	template<class ReadDataLine,
	         class = EnableIfFunc<ReadDataLine, const std::vector<StringView>&>>
	void read_data(std::vector<Ssize> column_index_filter, const ReadDataLine& read_data_line);

	/// Read, row by row, the data contents (not including the header line(s)) of all columns from the file.
	///
	/// @tparam  ReadDataLine  Callback function type
	/// @param[in] column_name_filter  A filter specifying the names of those columns whose data should be 
	///					read from the file; usable if the file has a header which specifies the column names; 
	///                 if the file was not opened as containing a header, an exception is thrown
	/// @param[in] read_data_line  Callback function which receives the cell values for each row in the 
	///					file; the values appear in the vector in the same order as the corresponding columns 
	///					appear in the filter
	///
	template<class ReadDataLine,
	         class = EnableIfFunc<ReadDataLine, const std::vector<StringView>&>>
	void read_data(const std::vector<StringView>& column_name_filter, const ReadDataLine& read_data_line);

	/// Read, row by row, the data contents (not including the header line(s)) of all columns from the file.
	///
	/// @tparam  ReadDataLine  Callback function type
	/// @param[in] column_name_filter  A filter specifying the names of those columns whose data should be 
	///					read from the file; usable if the file has a header which specifies the column names; 
	///                 if the file was not opened as containing a header, an exception is thrown
	/// @param[in] read_data_line  Callback function which receives the cell values for each row in the 
	///					file; the values appear in the vector in the same order as the corresponding columns 
	///					appear in the filter
	///
	template<class ReadDataLine,
	         class = EnableIfFunc<ReadDataLine, const std::vector<StringView>&>>
	void read_data(std::initializer_list<const Chr*> column_name_filter, const ReadDataLine& read_data_line);

	/// Read, row by row, the data contents (not including any header lines, if the file was opened as having 
	/// a header) of all columns from the file, into a string table.
	///
	/// @return  The string table
	///
	std::unique_ptr<NullableStringTable<Chr>> read_data_into_string_table();

	TWIST_NO_COPY_NO_MOVE(DelimValueFileReader)

private:
	using ReadHeader = std::function<void(const std::vector<String>&)>;

	template<class ReadDataLine>
	void read_impl(std::vector<Ssize>&& column_index_filter, const ReadDataLine& read_data_line);

	void init(fs::path&& path);

	bool read_next_line();

	fs::path filename() const;

	void init_column_filter(std::vector<Ssize>&& src);

	void filter_line_fields();

	static constexpr auto header_reader_type_check();

	std::unique_ptr<InTextFileStream<Chr>>  file_stream_{};
	std::unique_ptr<Chr[]>  line_buffer_{};
	std::unique_ptr<Chr[]>  field_buffer_{};
	std::vector<String>  line_fields_{};
	Ssize  line_no_{};

	Ssize  column_count_{};
	std::vector<String>  column_names_{};

	std::vector<Ssize>  column_filter_{};

	TWIST_CHECK_INVARIANT_DECL
};

//
//  Free functions
//

/// Split a line of text read from a "delimited-value text" file in separate values (tokens) based on the 
/// format of the file.
///
/// @tparam  Chr  The character type in the input text file; only the char and wchar_t types are currenty 
///			    supported
/// @tparam  format  The delimited-value text file format
/// @param[in] line  The line of text
/// @return  The separated text values  
///
template<class Chr, 
         DelimValueFileFormat format>
std::vector<std::basic_string<Chr>> tokenise_line(std::basic_string_view<Chr> line);

}

#include "DelimValueFileReader.ipp"

#endif 

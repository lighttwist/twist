/// @file HomogenousTable.hpp
/// HomogenousTable class template

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_DB_HOMOGENOUS_TABLE_HPP
#define TWIST_DB_HOMOGENOUS_TABLE_HPP

#include "twist/math/ColumnwiseMatrix.hpp"
#include "twist/math/RowwiseMatrix.hpp"

namespace twist::db {

//! Table cell values storage type
enum class HomogenousTableStorageType { rowwise, columnwise };

/*! A data table whose cells contain values of the same type, ie matrix whose columns have unique, non-blank names.
    Depending on the storage type (row-wise or column-wise) row operations (especially insertion and deletion) can be 
    efficient and column operations inefficient, or vice-versa. 
    \tparam T  The type of a cell value
	\tparam storage_type  Cell values storage type
 */
template<class T, HomogenousTableStorageType storage_type>
class HomogenousTable {
public:
	//! Cell value type
	using Value = T;

	using InRowValues = std::conditional_t<storage_type == HomogenousTableStorageType::rowwise,
	                                       std::vector<T>, const std::vector<T>&>;
	using InColumnValues = std::conditional_t<storage_type == HomogenousTableStorageType::rowwise,
	                                          const std::vector<T>&, std::vector<T>>;
	using OutRowValues = InColumnValues;
	using OutColumnValues = InRowValues;

	/*! Constructor.
	    \param[in] col_names  The names of the intial columns in the table; the names must be unique and non-blank; 
		                      if the vector is empty, the table will contain no columns
	 */
	HomogenousTable(std::vector<std::wstring> col_names = {});

	//! Get a clone of the table.
	[[nodiscard]] auto clone() const -> HomogenousTable;

	/*! Get the name of a the column named \p col_name. If the table does not contain a column with this name, an 
	    exception is thrown.
	 */
	[[nodiscard]] auto get_column_name(Ssize col_idx) const -> std::wstring;

	//! Get the names of a the columns, in the order they appear in the table. 
	[[nodiscard]] auto column_names() const -> const std::vector<std::wstring>&;

	/*! Get the index of the column named \p col_name. If the table does not contain a column with this name, an 
	    exception is thrown.
	 */
	[[nodiscard]] auto get_column_index(const std::wstring& col_name) const -> Ssize;

	/*! Get the number of rows in the table.
	    \return  The number of columns
	 */
	[[nodiscard]] auto nof_rows() const -> Ssize;

	/*! Get the number of columns in the table.
	    \return  The number of columns
	*/
	[[nodiscard]] auto nof_columns() const -> Ssize;

	/*! Get the number of cells in the table.
	    \return  The number of columns
	 */
	[[nodiscard]] auto nof_cells() const -> Ssize;

	/*! Get the value in a specific table cell.
	    \param[in] row_idx  The row index; the bahaviour is undefined if it is invalid (there is no validity check)
	    \param[in] col_idx  The column index; the bahaviour is undefined if it is invalid (there is no validity check)
	    \return  The cell value
	 */
	[[nodiscard]] auto operator()(Ssize row_idx, Ssize col_idx) const -> const T&;

	/*! Get the value in a specific table cell.
	    \param[in] row_idx  The row index; the bahaviour is undefined if it is invalid (there is no validity check)
	    \param[in] col_idx  The column index; the bahaviour is undefined if it is invalid (there is no validity check)
	    \return  The cell value
	 */
	[[nodiscard]] auto operator()(Ssize row_idx, Ssize col_idx) -> T&;

	/*! Get the value in a specific table cell.
	    \param[in] row_idx  The row index; an exception is thrown if it is invalid
	    \param[in] col_idx  The column index; an exception is thrown if it is invalid
	    \return  The cell value
	 */
	[[nodiscard]] auto get_cell(Ssize row_idx, Ssize col_idx) const -> const T&;

	/*! Set the value in a specific table cell.
	    \param[in] row_idx  The row index; an exception is thrown if it is invalid
	    \param[in] col_idx  The column index; an exception is thrown if it is invalid
	    \param[in] value  The cell value
	 */
	auto set_cell(Ssize row_idx, Ssize col_idx, Value value) -> void;

	/*! Get read-only access the values in a specific table row.
	    \param[in] row_idx  The row index; an exception is thrown if it is invalid
	    \return  The row values
	 */
	[[nodiscard]] auto get_row(Ssize row_idx) const -> OutRowValues;

	/*! Add a row at the bottom of the table, with the values of the elements in \p row_values.
	    Size of \p row must match the number of columns in the table.
	    \return  The new row's index
	*/
	auto add_row(InRowValues row_values) -> Ssize;

	//! Remove the row with index \p row_idx from the table. If the row index is invalid, an exception is thrown.
	auto remove_row(Ssize row_idx) -> void;

	//! Rename the table column named \p old_col_name to \p new_col_name, which cannot be blank.
	auto rename_column(const std::wstring& old_col_name, std::wstring new_col_name) -> void;

	/*! Add a new column into the table, as the rightmost column.
		\param[in] col_name  The new column name; cannot be blank
		\param[in] col_values  The values in the new column, from top to bottom; if the number of values does not 
		                       match the number of rows, an exception is thrown
		\return  The index of the new column 
	 */
	auto add_column(std::wstring col_name, InColumnValues col_values) -> Ssize;

	/*! Insert a new column into the table.
	   	\param[in] col_idx  The index which the new column will have; an exception is thrown if it is invalid 
		\param[in] col_name  The new column name; cannot be blank
		\param[in] col_values  The values in the new column, from top to bottom; if the number of values does not 
		                       match the number of rows, an exception is thrown
	 */
	auto insert_column(Ssize col_idx, std::wstring col_name, InColumnValues col_values) -> void;

	/*! Remove a column from the table.
	   	\param[in] col_name  The column name; if the table does not contain a column with this name, an exception is 
		                     thrown 
	 */
	auto remove_column(const std::wstring& col_name) -> void;

	/*! Remove a column from the table.
	   	\param[in] col_idx  The column index; an exception is thrown if it is invalid 
	 */
	auto remove_column(Ssize col_idx) -> void;

	/*! Remove a column from the table, after extraction its values into a container.
	   	\param[in] col_name  The column name; if the table does not contain a column with this name, an exception is 
		                     thrown 
		\return  The column values, top to bottom
	 */
	[[nodiscard]] auto extract_column(const std::wstring& col_name) -> std::vector<T>;

	/*! Remove a column from the table, after extraction its values into a container.
	   	\param[in] col_idx  The column index; an exception is thrown if it is invalid
		\return  The column values, top to bottom
	 */
	[[nodiscard]] auto extract_column(Ssize col_idx) -> std::vector<T>;

	TWIST_NO_COPY_DEF_MOVE(HomogenousTable)

private:	
	using Storage = std::conditional_t<storage_type == HomogenousTableStorageType::rowwise,
	                                   twist::math::RowwiseMatrix<T>, twist::math::ColumnwiseMatrix<T>>;

	std::vector<std::wstring> col_names_;
	std::map<std::wstring, Ssize> col_indexes_;
	Storage cell_data_;

	TWIST_CHECK_INVARIANT_DECL
};

template<class T>
using RowwiseHomogenousTable = HomogenousTable<T, HomogenousTableStorageType::rowwise>;

template<class T>
using ColumnwiseHomogenousTable = HomogenousTable<T, HomogenousTableStorageType::columnwise>;

// --- Free functions ---

/*! Compare the values in the cells of a table with those in a range. The size of the range is expected to match the
    number of cells in the table. The comparison is performed row-wise, starting from the top left of the table, 
	using operator==.
	\tparam T  The table element type 
	\tparam storage_type  The table cell values storage type
	\tparam Rng  The range type
	\return  true if all values match
 */
template<class T, 
         HomogenousTableStorageType storage_type, 
		 rg::input_range Rng>
[[nodiscard]] auto equal_to_range(const HomogenousTable<T, storage_type>& table, const Rng& range) -> bool;

}

#include "twist/db/HomogenousTable.ipp"

#endif 


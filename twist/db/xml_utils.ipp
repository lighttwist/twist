/// @file xml_utils_new.cpp
/// Inline implementation file for "xml_utils_new.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace twist::db {

template<class Enum> 
requires std::is_enum_v<Enum>
[[nodiscard]] auto text_as_enum(const XmlElemNode& xnode) -> Enum
{
    static_assert(sizeof(std::underlying_type_t<Enum>) <= sizeof(int));
    return static_cast<Enum>(text_as_int(xnode));
}

template<class Enum> 
requires std::is_enum_v<Enum>
[[nodiscard]] auto get_unique_child_enum(const XmlElemNode& elem, const std::wstring& child_tag) -> Enum
{
    return text_as_enum<Enum>(elem.get_unique_child_with_tag(child_tag));
}

template<class Enum> 
requires std::is_enum_v<Enum>
[[nodiscard]] auto get_optional_unique_child_enum(const XmlElemNode& elem, const std::wstring& child_tag) 
                    -> std::optional<Enum>
{
    if (auto opt_int = get_optional_unique_child_int(elem, child_tag)) {
        return static_cast<Enum>(*opt_int);
    }
    return std::nullopt;
}

} 

/// @file BinmatFile.hpp
/// BinmatFile class template definition

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_DB_BINMAT_FILE_HPP
#define TWIST_DB_BINMAT_FILE_HPP

#include "twist/File.hpp"
#include "twist/meta_ranges.hpp"
#include "twist/math/FlatMatrix.hpp"
#include "twist/math/FlatMatrixStack.hpp"
#include "twist/math/space_grid.hpp"

namespace twist::db {

/*! Class which represents a connection to a "binmat" file, ie a binary file which stores the cell values of a single 
    matrix or of a stack of matrices, laid out contiguosly in a "flat array" structure, with no compression.
	A binmat contains no other data (eg headers, metadata). All read-write operations are guaranteed to be very 
	efficient.
	A binmat file which stores the data for a single matrix is equivalent to a file storing the data for a matrix stack 
	containing a single matrix.
	See classes FlatMatrix and FlatMatrixStack for a description of the cell value layout in the file, which matches
	the layout of the "flat array" used internally by those classes.
    \tparam DataVal  The type of the data value in a matrix cell
 */
template<class DataVal>
class BinmatFile {
public:
	template<class T> 
	using MatrixData = twist::math::FlatMatrix<T>;

	template<class T> 
	using MatrixStackData = twist::math::FlatMatrixStack<T>;

	/*! Create a new "binmat" file for storing the cell values of a matrix. 
	    \param[in] path  Path to the file; an exception is raised if it already exists
	    \param[in] nof_rows  The number of rows in the matrix
	    \param[in] nof_cols  The number of columns in the matrix
	    \param[in] nodata_value  Textual representation of the "no-data" placeholder value; pass in an empty string if 
		                         the matrix does not use a "no-data" value
	    \return  An open connection to the new "binmat" file
     */
	[[nodiscard]] static auto create_for_single_matrix(fs::path path, 
													   Ssize nof_rows, 
													   Ssize nof_cols, 
													   std::string nodata_value) -> std::unique_ptr<BinmatFile>;	

	/*! Open an existing "binmat" file storing the cell values of a matrix.  
	    \param[in] path  Path to the file; an exception is raised if it does not exist
	    \param[in] nof_rows  The number of rows in the matrix
	    \param[in] nof_cols  The number of columns in the matrix
	    \param[in] nodata_value  Textual representation of the "no-data" placeholder value; pass in an empty string if 
		                         the matrix does not use a "no-data" value
	    \param[in] read_only  If true, the connection to the file will only allow read access; otherwise read-write 
		                      access is granted
	    \return  An open connection to the "binmat" file
     */
	[[nodiscard]] static auto open_for_single_matrix(fs::path path, 
													 Ssize nof_rows, 
													 Ssize nof_cols, 
													 std::string nodata_value, 
													 bool read_only) -> std::unique_ptr<BinmatFile>;
													 
	/*! Create a new "binmat" file for storing the cell values of a stack of matrices. 
	    \param[in] path  Path to the file; an exception is raised if it already exists
	    \param[in] nof_rows  The number of rows in each matrix
	    \param[in] nof_cols  The number of columns in each matrix
	    \param[in] nof_mats  The number of matrices in the stack
	    \param[in] nodata_value  Textual representation of the "no-data" placeholder value; pass in an empty string if 
		                         the matrix does not use a "no-data" value
	    \return  An open connection to the new "binmat" file
     */
	[[nodiscard]] static auto create_for_matrix_stack(fs::path path, 
													  Ssize nof_rows, 
													  Ssize nof_cols, 
													  Ssize nof_mats, 
													  std::string nodata_value) -> std::unique_ptr<BinmatFile>;	

	/*! Open an existing "binmat" file storing the cell values of a matrix.  
	    \param[in] path  Path to the file; an exception is raised if it does not exist
	    \param[in] nof_rows  The number of rows in the matrix
	    \param[in] nof_cols  The number of columns in the matrix
	    \param[in] nof_mats  The number of matrices in the stack
	    \param[in] nodata_value  Textual representation of the "no-data" placeholder value; pass in an empty string if 
		                         the matrix does not use a "no-data" value
	    \param[in] read_only  If true, the connection to the file will only allow read access; otherwise read-write 
		                      access is granted
	    \return  An open connection to the "binmat" file
     */
	[[nodiscard]] static auto open_for_matrix_stack(fs::path path, 
													Ssize nof_rows, 
													Ssize nof_cols, 
													Ssize nof_mats, 
													std::string nodata_value, 
													bool read_only) -> std::unique_ptr<BinmatFile>;

	//! The number of rows in the matrix; or in each matrix of the stack if the file stores a stack of matrices.
	[[nodiscard]] auto nof_rows() const -> Ssize;

	//! The number of columns in the matrix; or in each matrix of the stack if the file stores a stack of matrices.
	[[nodiscard]] auto nof_columns() const -> Ssize;

	/*! If the file stores a single matrix number of columns in the matrix, this method returns one; if the file stores 
	    a stack of matrices, it returns the number of matrices in the stack.
	 */
	[[nodiscard]] auto nof_matrices() const -> Ssize;

	/*! Textual representation of the "no-data" placeholder value; or an empty string if the file does not use a 
	    "no-data" value.
	 */
	[[nodiscard]] auto nodata_value() const -> std::string;

	/*! Read the whole contents of the binmat file into a matrix. 
	    The file must store the data for a single matrix; otheriwse an exception is thrown.
		\param[in] init_val  The value which will be used to initialise the data matrix before the values are read 
		                     into it from the binmat; useful for data value types without a default constructor
	 */
	[[nodiscard]] auto read_whole_to_single_matrix(const DataVal& init_val = DataVal{}) const -> MatrixData<DataVal>;

	/*! Read the cell values from several contiguous full rows in the binmat file. 
	    The file must store the data for a single matrix; otheriwse an exception is thrown.
	    \param[in] begin_file_row  The first row in the binmat to be read from
	    \param[in] end_file_row  The last row in the binmat to be read from
	    \param[in] data  Matrix which will hold the output data; it must have the same number of columns as the 
		                 binmat, and at least as many rows as the number of rows to be read; it will be written to 
						 starting with its first row
	 */
	auto read_rows_to_single_matrix(Ssize begin_row, Ssize end_row, MatrixData<DataVal>& data) const -> void;

	/*! Read the cell values from a submatrix of the binmat file. 
	    The file must store the data for a single matrix; otheriwse an exception is thrown.
	    \param[in] submatrix_info  Information about the binmat submatrix to be read 
	    \param[in] data  Matrix which will hold the output data; it must have at least as many rows and columns as the 
		                 submatrix to be read; it will be written to starting with its first row and column
     */
	auto read_submatrix_to_single_matrix(const SubgridInfo& submatrix_info, MatrixData<DataVal>& data) const -> void;

	/*! Read the whole contents of the binmat file into a matrix stack. 
	    If the file stores the data for a single matrix, the matrix stack returned will contain a single matrix.
	 */
	[[nodiscard]] auto read_whole_to_matrix_stack() const -> MatrixStackData<DataVal>;

	/*! Fill the whole binmat file (every cell in the binmat) with the same data value.
	    \param[in] value  The fill value
	 */
	auto fill(DataVal value) -> void;

	/*! Overwrite all the cell values of the single matrix stored in the file with the cell values read from the whole 
	    input matrix.
	    The file must store the data for a single matrix, or for a matrix stack containing a single matrix; 
		otheriwse an exception is thrown.
	    \param[in] data  Input matrix; its dimensions must match the binmat dimensions
	 */
	auto write_whole_data_to_whole_file(const MatrixData<DataVal>& data) -> void;

	/// Write data to all cells in the file, after applying an (optional) transformation to each value.
	/// This overload is called when the type of the input data values matches the binmat data type.
	/// The input data is destroyed; if you need to retain it make a copy of the data argument first.
	///
	/// @tparam  Transformation  The type of the transformation; a callable type
	/// @param[in] data  Matrix containing the data to be written; its dimensions must match the 
	///					binmat dimensions
	/// @param[in] transform   Transformation to be applied to the data before it is written to the file; 
	///					pass in nullopt if no transformation is to be applied
	///
	template<class Transformation, 
	         class = EnableIfFuncR<DataVal, Transformation, DataVal>>
	void write_whole_data_to_whole_file(MatrixData<DataVal>&& data, std::optional<Transformation> transform);

	/// Write data to all cells in the file, after applying an (optional) transformation to each value.
	/// This overload is called when the type of the input data does not match the bitmap data type.
	///
	/// @tparam  T  The input data value type, before the (optional) transformation
	/// @tparam  Transformation  The type of the transformation; a callable type
	/// @param[in] data  Matrix containing the data to be written; its dimensions must match the 
	///					binmat dimensions
	/// @param[in] transform   Transformation to be applied to the data before it is written to the file; 
	///					pass in nullopt if no transformation is to be applied, in which case the input values
	///				    will be transformed to the binmat data type via static_cast
	///
	template<class T, 
	         class Transformation, 
			 class = EnableIfFuncR<DataVal, Transformation, T>>
	void write_whole_data_to_whole_file(const MatrixData<T>& data, std::optional<Transformation> transform);

	/// Write data to several contiguous full rows in the binmat file.
	///
	/// @tparam  Cont  Container (or container view) type which holds the data to be written to the file
	/// @param[in] begin_file_row  The first row in the binmat to be written to
	/// @param[in] end_file_row  The last row in the binmat to be written to
	/// @param[in] data  Container holding the input data; its size must match the total number of cells 
	///					in the rows to be written to
	///
	template<class Cont,
	         class = EnableIfContigContainerGives<Cont, DataVal>>
	void write_whole_data_to_file_rows(Ssize begin_file_row, Ssize end_file_row, const Cont& data);

	/// Given a matrix of input data, and a submatrix thereof which exactly matches the binmat dimensions, 
	///   write the values in that submatrix to the file, after applying an (optional) transformation to each 
	///   value.  
	/// This overload is called when the type of the input data matches the binmat data type.
	/// The input data is destroyed; if you need to retain it make a copy of the data argument first.
	///
	/// @tparam Transformation  The type of the transformation; a callable type
	/// @param[in] data  Matrix containing the input data
	/// @param[in] data_part_info  Information about which subgrid of the input data grid is written to the file; 
	///                            its dimensions must match the binmat dimensions
	/// @param[in] transform   Transformation to be applied to the data before it is written to the file; 
	///					       pass in nullopt if no transformation is to be applied
	///
	template<class Transformation, 
	         class = EnableIfFuncR<DataVal, Transformation, DataVal>>
	void write_data_part_to_whole_file(MatrixData<DataVal>&& data, const SubgridInfo& data_part_info, 
			                           std::optional<Transformation> transform);

	/// Given a matrix of input data, and a submatrix thereof which exactly matches the binmat dimensions, 
	///   write the values in that submatrix to the file, after applying an (optional) transformation to each 
	///   value.  
	/// This overload is called when the type of the input data values does not match the binmat data type.
	///
	/// @tparam T  The input data value type, before the (optional) transformation
	/// @tparam  Transformation  The type of the transformation; a callable type
	/// @param[in] data  Matrix containing the input data
	/// @param[in] data_part_info  Information about which subgrid of the input data grid is written to the file; 
	///                            its dimensions must match the binmat dimensions
	/// @param[in] transform   Transformation to be applied to the data before it is written to the file; 
	///					       pass in nullopt if no transformation is to be applied, in which case the input values
	///				           will be transformed to the binmat data type via static_cast
	///
	template<class T, 
	         class Transformation, 
			 class = EnableIfFuncR<DataVal, Transformation, T>>
	void write_data_part_to_whole_file(const MatrixData<T>& data, 
			                           const SubgridInfo& data_part_info, 
									   std::optional<Transformation> transform);

	/*! Overwrite all the cell values of all the matrices in the stack stored in the file with the cell values read 
	    from the whole input matrix stack.
	    If the input stack contains more than one matrix, then the file must store the data for a matrix stack 
		containing the same number of matrices. 
	    \param[in] data  Input matrix stack; its dimensions must match the binmat dimensions
	 */
	auto write_whole_data_to_whole_file(const MatrixStackData<DataVal>& data) -> void;

	TWIST_NO_COPY_NO_MOVE(BinmatFile)

private:
	BinmatFile(std::unique_ptr<FileStd> file, Ssize nof_rows, Ssize nof_cols, Ssize nof_mats, 
	           std::string nodata_value);

	auto check_begin_end_rows(Ssize begin_row, Ssize end_row) const -> void;

	auto check_submatrix(const SubgridInfo& submatrix_info) const -> void;

	auto check_dimensions() const -> void;

	std::unique_ptr<FileStd> file_;
	Ssize nof_rows_;
	Ssize nof_cols_;
	Ssize nof_mats_;
	std::string nodata_value_;

	TWIST_CHECK_INVARIANT_DECL
};

// --- Free functions ---

/*! Transform each value in a "binmat file" using a custom transformation. Each cell data value is read from the file, 
    then the transformation is applied to it, and it is written back to the file. The implementation is very efficient
	and the amout of data read into memory never exceeds a small amount (currently 5MB).
    \tparam DataVal  The type of the data value in a binmat cell
    \tparam Transformation  The type of the transformation; a callable type
    \param[in] binmat  Connection to the binmat file  
    \param[in] transform  Transformation to be applied to each cell data value
 */
template<class DataVal,
		 class Transformation, 
	     class = EnableIfFuncR<DataVal, Transformation, DataVal>>
auto transform_binmat(BinmatFile<DataVal>& binmat, Transformation transform) -> void;

/*! Transform each value in a "binmat file" using a custom transformation and write the results to a new binmat file 
    with the same dimensions and cell data type. Each cell data value is read from the file, then the transformation is 
	applied to it, and it is written to the new file. The implementation is very efficient and the amout of data read 
	into memory never exceeds a small amount (currently 5MB).
    \tparam DataVal  The type of the data value in a binmat cell
    \tparam Transformation  The type of the transformation; a callable type
    \param[in] src_binmat  Connection to the source binmat file  
    \param[in] dest_binmat_path  Path to the destination binmat file; if the file already exists, an exception is 
	                             thrown
    \param[in] transform  Transformation to be applied to each cell data value; pass in std::identity to simply copy 
	                      the data across to the destination binmat file: this can be a lot faster than doing it via						  
						  std::filesystem::copy_file()!
 */
template<class DataVal,
		 class Transformation, 
	     class = EnableIfFuncR<DataVal, Transformation, DataVal>>
auto transform_binmat(BinmatFile<DataVal>& src_binmat, fs::path dest_binmat_path, Transformation transform) -> void;

}

#include "twist/db/BinmatFile.ipp"

#endif 

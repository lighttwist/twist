///  @file  DatabaseSchemaMgrBase.cpp
///  Implementation file for "DatabaseSchemaMgrBase.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "DatabaseSchemaMgrBase.hpp"

namespace twist::db {

DatabaseSchemaMgrBase::DatabaseSchemaMgrBase(fs::path path, unsigned int cur_db_version, 
		unsigned int oldest_supported_db_version)
	: path_{ std::move(path) }
	, cur_db_version_{ cur_db_version }
	, oldest_supported_db_version_{ oldest_supported_db_version }
{
	TWIST_CHECK_INVARIANT
}


DatabaseSchemaMgrBase::~DatabaseSchemaMgrBase()
{
	TWIST_CHECK_INVARIANT
}


fs::path DatabaseSchemaMgrBase::path() const
{
	TWIST_CHECK_INVARIANT
	return path_;
}


unsigned int DatabaseSchemaMgrBase::get_cur_db_version() const
{
	TWIST_CHECK_INVARIANT
	return cur_db_version_;
}


unsigned int DatabaseSchemaMgrBase::get_oldest_supported_db_version() const
{
	TWIST_CHECK_INVARIANT
	return oldest_supported_db_version_;
}


std::wstring DatabaseSchemaMgrBase::get_special_reorganise_message(unsigned int /*old_version*/) const
{
	TWIST_CHECK_INVARIANT
	return L"";
}


#ifdef _DEBUG
void DatabaseSchemaMgrBase::check_invariant() const noexcept
{
	assert(!path_.empty());
	assert(cur_db_version_ > 0);
	assert(oldest_supported_db_version_ > 0);
}
#endif 

}

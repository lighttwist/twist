/// @file SqliteSelector.ipp
/// Inline implementation file for "SqliteSelector.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace twist::db {

// --- SqliteSelector class ---

template<class Enum, class>
auto SqliteSelector::get_enum_int(unsigned int col_idx) const -> Enum
{
	TWIST_CHECK_INVARIANT
	const int int_val = get_int(col_idx);
	return static_cast<Enum>(int_val);
}

template<class Enum, class>
auto SqliteSelector::get_enum_int(std::string_view col_name) const -> Enum
{
	TWIST_CHECK_INVARIANT
	const int int_val = get_int(col_name);
	return static_cast<Enum>(int_val);
}


template<class BlobElem, class Cont, class>
void SqliteSelector::get_array_blob(std::string_view col_name, Cont& cont) const
{
	TWIST_CHECK_INVARIANT
	get_array_blob<BlobElem, Cont>(col_indexer_->get_col_idx(col_name), cont);
}


template<class BlobElem, class Cont, class> 
void SqliteSelector::get_array_blob(unsigned int col_idx, Cont& cont) const
{
	TWIST_CHECK_INVARIANT
	conn_.column_array_blob<BlobElem, Cont>(*statement_, col_idx, cont);
}


template<class Cont, class> 
size_t SqliteSelector::get_uncompress_array_blob(std::string_view col_name, Cont& cont, 
		SqliteBlobCompression compression) const
{
	TWIST_CHECK_INVARIANT
	return get_uncompress_array_blob(col_indexer_->get_col_idx(col_name), cont, compression);
}


template<class Cont, class> 
size_t SqliteSelector::get_uncompress_array_blob(unsigned int col_idx, Cont& cont, 
		SqliteBlobCompression compression) const
{
	TWIST_CHECK_INVARIANT
	return conn_.column_uncompress_array_blob(*statement_, col_idx, cont, compression);
}

// --- Free functions ---

template<class Enum, class>
auto get_nullable_enum_int(const SqliteSelector& sel, std::string_view col_name) -> std::optional<Enum>
{
	return !sel.is_null(col_name) ? std::make_optional(sel.get_enum_int<Enum>(col_name)) : std::nullopt;
}

} 


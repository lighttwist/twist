/// @file csv_file_utils.hpp
/// Utilities for working with CSV files (ie text file contining tabular data, with the values on the same row 
/// typically separated by commas)

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_DB_CSV__FILE__UTILS_HPP
#define TWIST_DB_CSV__FILE__UTILS_HPP

#include "twist/string_utils.hpp"
#include "twist/db/HomogenousTable.hpp"
#include "twist/math/RowwiseMatrix.hpp"

namespace twist::db {

/*! Given a text file following the CSV format (including column headers), read the values from one of the columns.
    A transformation can be applied to each read value before it is returned.
    \tparam Chr  The character type in the text file
    \tparam Transform  The type of the transformation to be applied to each value string read from the file
    \param[in] csv_path  The path of the CSV file
    \param[in] transform  (Optional) transformation to be applied to each value string read from the file before it 
                          is returned
    \return  Vector containing the value strings read from the file (if no transformation was supplied); or the results
             of the transformation applied to each value string
 */
template<class Chr, 
         class Transform = std::identity>
requires std::invocable<Transform, const std::basic_string<Chr>&>
[[nodiscard]] auto read_csv_file_column(const fs::path& csv_path, 
                                        const std::basic_string<Chr>& column_name, 
                                        Transform transform = {}) 
	                -> std::vector<std::decay_t<std::invoke_result_t<Transform, const std::basic_string<Chr>&>>>;

/*! Given a text file following the CSV format with a single column (including the column header), read the values from 
    that column. A transformation can be applied to each read value before it is returned.
    \tparam Chr  The character type in the text file
    \tparam Transform  The type of the transformation to be applied to each value string read from the file
    \param[in] csv_path  The path of the CSV file
    \param[in] transform  (Optional) transformation to be applied to each value string read from the file before it 
                          is returned
    \return  Vector containing the value strings read from the file (if no transformation was supplied); or the results
             of the transformation applied to each value string
 */
template<class Chr, 
         class Transform = std::identity>
requires std::invocable<Transform, const std::basic_string<Chr>&>
[[nodiscard]] auto read_single_column_csv_file(const fs::path& csv_path, 
                                               Transform transform = {}) 
	                -> std::vector<std::decay_t<std::invoke_result_t<Transform, const std::basic_string<Chr>&>>>;

/*! Write a range of values to a CSV file containing one column. A transformation can be applied to each value before 
    it is written.
    \tparam Chr  The character type in the text file
    \tparam Rng  Input range type; if no transformation is applied to its elements before being written, their type 
                 must one which can be streamed into OutTxtFileStream<Chr>
    \tparam Transform  The type of the transformation to be applied to each value string before it is written to the 
                       file; its return type must be one which can be streamed into OutTxtFileStream<Chr>
    \param[in] range  Input range
    \param[in] csv_path  The path of the CSV file; if the file already exists, it is overwritten
    \param[in] column_name  (Optional) column name, written as a header if non-empty
    \param[in] transform  (Optional) transformation to be applied to each avlue string read from the file before it 
                          is written
 */
template<class Chr, 
         rg::input_range Rng,
         class Transform = std::identity>
requires std::invocable<Transform, rg::range_value_t<Rng>>
auto write_one_column_csv_file(const Rng& range, 
                               const fs::path& csv_path, 
                               const std::basic_string<Chr>& column_name = {},
                               Transform transform = {}) -> void;

/*! Given a text file following the CSV format (including column headers), create another CSV file which will contain a 
    subset of the columns (header and data) of the original file.   
    \tparam Chr  The character type in the text files
    \tparam Strings  A range of strings convertible to the type std::basic_string<Chr>
    \param[in] in_csv_path  The path of the input CSV file
    \param[in] out_csv_path  The path of the output CSV file
    \param[in] column_names  The names of the columns to export; they will appear in the order they are 
   					         specified within the range (and can be repeated in the output)
    \param[in] in_csv_delimiter  The string which delimits two adjacent values on the same line in the input file
    \param[in] out_csv_delimiter  The string which will delimit two adjacent values on the same line in the output file 
 */   
template<class Chr, rg::input_range Strings>
auto export_csv_file_columns(const fs::path& in_csv_path, 
                             const fs::path& out_csv_path, 
                             const Strings& column_names,
                             std::basic_string<Chr> in_csv_delimiter = comma_str<Chr>(),
                             std::basic_string<Chr> out_csv_delimiter = comma_str<Chr>()) -> void;

/*! Given a text file following the CSV format (including column headers), read the values from a subset of its 
    columns into a homogenous table. The column separator is customisable.
    \tparam Chr  The character type in the text file
    \tparam T  The type of a cell value in the table
	\tparam CellConv  Callable type for converting a string into a table cell value
    \param[in] path  The path of the input CSV file
    \param[in] column_names  The names of the CSV file columns to be imported; the order of the columns in the returned 
                             table will match this list; the list cannot be empty and the names must be valid
    \param[in] cell_conv  Callable which converts a string representing a cell value, as read from the file, to a table 
                          cell value
    \param[in] col_sep  The column separator used in the file
 */
template<class Chr, 
         class T,
         class CellConv>
requires std::is_invocable_r_v<T, CellConv, const std::basic_string<Chr>&>
[[nodiscard]] auto read_csv_file(fs::path path,
                                 std::vector<std::wstring> column_names, 
                                 CellConv cell_conv,
                                 const std::basic_string<Chr>& col_sep = comma_str<Chr>()) 
                    -> RowwiseHomogenousTable<T>;

/*! Export a matrix to a text file (eg CSV). Each row in the matrix will be a line in the file, and there will be no 
    line of headers.
    \tparam Chr  The character type in the text file
    \tparam T  The type of a cell value in the matrix
	\tparam CellConv  Callable type for converting a matrix cell value into a string
    \param[in] matrix  The matrix
    \param[in] path  The text file path; if such a file already exists, it will be deleted
    \param[in] cell_conv  Callable which converts a cell value into a string, to be written to the file (the 
 					      parameter is the cell value)
    \param[in] col_sep  The column separator string to be used in the file
 */
template<class Chr, 
         class T, 
         class CellConv> 
requires std::is_invocable_r_v<std::basic_string<Chr>, CellConv, const T&>
auto write_csv_file(const twist::math::RowwiseMatrix<T>& matrix, 
                    fs::path path, 
		            CellConv cell_conv, 
                    const std::basic_string<Chr>& col_sep = comma_str<Chr>()) -> void;

/*! Export a matrix to a text file (eg CSV). Each row in the matrix will be a line in the file, and there will be no 
    line of headers.
    \tparam Chr  The character type in the text file
    \tparam T  The type of a cell value in the matrix
	\tparam CellConv  Callable type for converting a matrix value into a string
    \param[in] matrix  The matrix
    \param[in] path  The text path; if such a file already exists, it will be deleted
    \param[in] cell_conv  Callable which converts a cell value into a string, to be written to the file (the 
 					      parameter is the cell value)
    \param[in] col_sep  The column separator string to be used in the file
 */
template<class Chr, 
         class T, 
         class CellConv> 
requires std::is_invocable_r_v<std::basic_string<Chr>, CellConv, const T&>
auto write_csv_file(const twist::math::ColumnwiseMatrix<T>& matrix, 
                    fs::path path, 
		            CellConv cell_conv, 
                    const std::basic_string<Chr>& col_sep = comma_str<Chr>()) -> void;

/*! Export a table to a text file (eg CSV). Each row in the matrix will be a line in the file, and the column names 
    will be written on the first line.
    \tparam Chr  The character type in the text file
    \tparam Table  The table type 
	\tparam CellConv  Callable type for converting a matrix value into a string
    \param[in] matrix  The matrix
    \param[in] path  The text file path; if such a file already exists, it will be deleted
    \param[in] header_prov  Callable which provides the header for each column, to be written to the file (the 
                            parameter is the column index)
    \param[in] cell_conv  Functor which converts a cell value into a string, to be written to the file (the parameter 
                          is the cell value)
    \param[in] col_sep  The column separator string to be used in the file
 */
template<class Chr, 
         class T, 
         HomogenousTableStorageType storage_type,
         class CellConv> 
requires std::is_invocable_r_v<std::basic_string<Chr>, CellConv, const T&>
auto write_csv_file(const HomogenousTable<T, storage_type>& table, 
                    fs::path path, 
                    CellConv cell_conv, 
                    const std::basic_string<Chr>& col_sep = comma_str<Chr>()) -> void;

}

#include "twist/db/csv_file_utils.ipp"

#endif 

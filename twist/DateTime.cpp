/// @file DateTime.cpp
/// Implementation file for "DateTime.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/DateTime.hpp"

#include "twist/chrono_utils.hpp"

#include <ctime>

using gsl::not_null;
using namespace std::chrono;

namespace twist {

// --- DateTime class ---

DateTime::DateTime() 
	: Date{}
	, hour_{0}
	, minute_{0}
	, second_{0}
{
	TWIST_CHECK_INVARIANT
}


DateTime::DateTime(int day, int month, int year) 
	: Date{day, month, year}
	, hour_{0}
	, minute_{0}
	, second_{0}
{
	TWIST_CHECK_INVARIANT
}

DateTime::DateTime(int day, int month, int year, int hour, int minute, int second) 
	: Date{day, month, year}
	, hour_{to_tiny_int(hour)}
	, minute_{to_tiny_int(minute)}
	, second_{to_tiny_int(second)}
{
	check_valid_time();
	TWIST_CHECK_INVARIANT
}

DateTime::DateTime(const DateTime& src) 
	: Date{src}
	, hour_{src.hour_}
	, minute_{src.minute_}
	, second_{src.second_}
{
	TWIST_CHECK_INVARIANT
}

DateTime::DateTime(const Date& src)
	: Date{src}
	, hour_{0}
	, minute_{0}
	, second_{0 }
{
	TWIST_CHECK_INVARIANT
}

DateTime::DateTime(const Date& date, int hour, int minute, int second)
	: Date{date}
	, hour_{to_tiny_int(hour)}
	, minute_{to_tiny_int(minute)}
	, second_{to_tiny_int(second)}
{
	check_valid_time();
	TWIST_CHECK_INVARIANT
}

DateTime::~DateTime()
{
	TWIST_CHECK_INVARIANT
}

DateTime& DateTime::operator=(const Date& rhs)
{
	TWIST_CHECK_INVARIANT
	Date::operator=(rhs);	
	hour_ = 0;
	minute_ = 0;
	second_ = 0;
	return *this;
}

DateTime& DateTime::operator=(const DateTime& rhs)
{
	TWIST_CHECK_INVARIANT
	if (this != &rhs) {
		Date::operator=(rhs);
		hour_ = rhs.hour_;
		minute_ = rhs.minute_;     
		second_ = rhs.second_;
	}
	return *this;
}

int DateTime::hour() const
{
	TWIST_CHECK_INVARIANT
	return hour_;
}

int DateTime::minute() const
{
	TWIST_CHECK_INVARIANT
	return minute_;
}

int DateTime::second() const
{
	TWIST_CHECK_INVARIANT
	return second_;
}

bool DateTime::check_valid_time(bool throw_if_invalid) const
{
	TWIST_CHECK_INVARIANT
	bool ret = true;
	if (hour_ < 0 || hour_ > 23) {
		ret = false;
	} 
	else if (minute_ < 0 || minute_ > 59) {
		ret = false;
	}
	else if (second_ < 0 || second_ > 59) {
		ret = false;
	}
	if (!ret && throw_if_invalid) {
		TWIST_THROW(L"Invalid time %d:%d:%d.", hour_, minute_, second_);	
	}
	return ret;
}

#ifdef _DEBUG
void DateTime::check_invariant() const noexcept
{
	assert(hour_ >= 0 && hour_ <= 23);
	assert(minute_ >= 0 && minute_ <= 59);
	assert(second_ >= 0 && second_ <= 59);
}
#endif 

// --- Local functions ---

template<class Duration>
DateTime add_duration(const DateTime& datetime, int64_t duration_count)
{
	if (duration_count == 0) {
		return datetime;
	}
	auto timepoint = datetime_to_utc_timepoint(datetime);
	timepoint += Duration{duration_count};
	return utc_timepoint_to_datetime(timepoint);
}

template<class Duration>
auto diff_duration(const DateTime& datetime1, const DateTime& datetime2) -> Duration::rep
{
	auto timepoint1 = datetime_to_utc_timepoint(datetime1);
	auto timepoint2 = datetime_to_utc_timepoint(datetime2);
	auto diff = duration_cast<Duration>(timepoint2 - timepoint1);
	return diff.count();
}

void stream(std::wstringstream& str, int val, bool pad)
{
	if (pad && val < 10) {
		str << L"0";
	}
	str << val;
}

void time_components_from_str(std::wstring_view str, std::wstring_view sep, 
		int& hour, int& minute, int& second)
{
	const size_t sep_len = sep.size();
	if (sep_len == 0) {
		TWIST_THROW(L"The separator cannot be empty.");
	}
	if (str.size() < 3 + 2 * sep_len) {
		TWIST_THROW(L"The time string \"%s\" is ill-formed: it has the wrong length.", 
				std::wstring{ str }.c_str());
	}
	const auto sep_positions = find_all_substr(str, sep);
	if (sep_positions.size() != 2) {
		TWIST_THROW(L"The time string \"%s\" is ill-formed: the separator appears %d times; "
				"it is expected twice.", std::wstring{ str }.c_str(), sep_positions.size());
	}
	const std::wstring hour_str{ str.substr(0, sep_positions[0]) };
	const std::wstring min_str{ 
			str.substr(sep_positions[0] + sep_len, sep_positions[1] - sep_positions[0] - sep_len) };
	const std::wstring sec_str{ str.substr(sep_positions[1] + sep_len) };

	if (!is_numeric(hour_str) || !is_numeric(min_str) || !is_numeric(sec_str)) {
		TWIST_THROW(L"The time string \"%s\" is ill-formed: it contains unexpected non-numeric character(s).", 
				std::wstring{ str }.c_str());
	}

	hour = to_int(hour_str);
	minute = to_int(min_str);
	second = to_int(sec_str);
}

// --- Free functions ---

bool operator==(const DateTime& lhs, const DateTime& rhs) 
{
	if (static_cast<const Date&>(lhs) != static_cast<const Date&>(rhs)) return false;
	return lhs.second() == rhs.second() && lhs.minute() == rhs.minute() && lhs.hour() == rhs.hour();
}

bool operator!=(const DateTime& lhs, const DateTime& rhs) 
{
	return !(lhs == rhs);
}

bool operator<(const DateTime& lhs, const DateTime& rhs) 
{ 
	if (lhs.year() < rhs.year()) {
		return true;
	}
	if (lhs.year() > rhs.year()) {
		return false;
	}
	if (lhs.month() < rhs.month()) {
		return true;
	}
	if (lhs.month() > rhs.month()) {
		return false;
	}
	if (lhs.day() < rhs.day()) {
		return true;
	}
	if (lhs.day() > rhs.day()) {
		return false;
	}
	if (lhs.hour() < rhs.hour()) {
		return true;
	}
	if (lhs.hour() > rhs.hour()) {
		return false;
	}
	if (lhs.minute() < rhs.minute()) {
		return true;
	}
	if (lhs.minute() > rhs.minute()) {
		return false;
	}
	return lhs.second() < rhs.second();
}

bool operator<=(const DateTime& lhs, const DateTime& rhs) 
{
	return lhs < rhs || lhs == rhs;
}

auto add_hours(const DateTime& datetime, int hours) -> DateTime
{
	return add_duration<std::chrono::hours>(datetime, hours);
}

auto add_minutes(const DateTime& datetime, int minutes) -> DateTime
{
	return add_duration<std::chrono::minutes>(datetime, minutes);
}

auto add_seconds(const DateTime& datetime, int64_t seconds) -> DateTime 
{
	return add_duration<std::chrono::seconds>(datetime, seconds);
}

auto add(const DateTime& datetime, std::chrono::seconds seconds) -> DateTime
{
	return add_duration<std::chrono::seconds>(datetime, seconds.count());
}

auto diff_hours(const DateTime& datetime1, const DateTime& datetime2) -> int
{
	return diff_duration<std::chrono::hours>(datetime1, datetime2);
}

int diff_minutes(const DateTime& datetime1, const DateTime& datetime2)
{
	return diff_duration<std::chrono::minutes>(datetime1, datetime2);
}

auto diff_seconds(const DateTime& datetime1, const DateTime& datetime2) -> std::chrono::seconds::rep
{
	return diff_duration<std::chrono::seconds>(datetime1, datetime2);
}

auto diff_seconds(std::chrono::system_clock::time_point start_timept, DateTime end_time) 
	  -> std::chrono::seconds::rep
{
	const auto end_timept = datetime_to_utc_timepoint(end_time);
	const auto diff_secs = std::chrono::duration_cast<std::chrono::seconds>(end_timept - start_timept);
	return diff_secs.count();
}

DateTime utc_timepoint_to_datetime(const system_clock::time_point& timept)
{
	using namespace std::chrono;

    // Compute time duration since 1Jan1970 00:00:00 (the "system clock" epoch)
    auto time_since_epoch = timept.time_since_epoch();

    // Compute the number of days since 1Jan1970 00:00:00
    const auto days_since_epoch = floor<DurationDays>(time_since_epoch);

    // time_since_epoch is now time duration since midnight of day days_since_epoch
    time_since_epoch -= days_since_epoch;

    // Break days_since_epoch down into year/month/day
    const auto date = days_since_unix_epoch_to_date(days_since_epoch.count());

    // Compute the time
    const auto hour = duration_cast<hours>(time_since_epoch).count();
    time_since_epoch -= hours{ hour };
    const auto minute = duration_cast<minutes>(time_since_epoch).count();    
	time_since_epoch -= minutes{ minute };
    const auto second = duration_cast<seconds>(time_since_epoch).count();
  
    return { date, static_cast<int>(hour), static_cast<int>(minute), static_cast<int>(second) };
}

auto same_date(const DateTime& datetime1, const DateTime& datetime2) -> bool
{
	return static_cast<const Date&>(datetime1) == static_cast<const Date&>(datetime2);
}

auto is_exact_hour(const DateTime& datetime) -> bool
{
	return datetime.minute() == 0 && datetime.second() == 0;
}

auto floor_to_exact_hour(const DateTime& datetime) -> DateTime
{
	return DateTime{static_cast<const Date&>(datetime), datetime.hour(), 0, 0};
}

auto ceil_to_exact_hour(const DateTime& datetime) -> DateTime
{
	if (is_exact_hour(datetime)) {
		return datetime;
	}
	return add_hours(floor_to_exact_hour(datetime), 1);
}

auto get_system_datetime() -> DateTime
{
	const auto sys_date = get_system_date();

	const auto now_time =  system_clock::to_time_t(system_clock::now());
	const auto* now_time_info = localtime(&now_time);
	
	return DateTime{sys_date, now_time_info->tm_hour, now_time_info->tm_min, now_time_info->tm_sec};
}

auto datetime_to_utc_timepoint(const DateTime& datetime) -> std::chrono::system_clock::time_point
{
	// Compute the number of days since 1Jan1970 00:00:00 to the date passed in
	const auto days_from_epoch = date_to_days_since_unix_epoch(datetime);

	// Add the days to 1Jan1970 00:00:00, to get a timepoint with the date passed in
	auto timept = std::chrono::system_clock::time_point{}; // Initialised to 1Jan1970 00:00:00
	timept += DurationDays{days_from_epoch};

	// Add hour, minute and second to the timept
	timept += hours{datetime.hour()};
	timept += minutes{datetime.minute()};
	timept += seconds{datetime.second()};

	return timept;
}

std::tm to_tm(const DateTime& date_time)
{
	if (date_time.year() < 1900) {
		TWIST_THROW(L"std::tm may not work reliably for dates before 1Jan1900.");
	}
	std::tm timeinfo{};
	timeinfo.tm_year = date_time.year() - 1900;   
	timeinfo.tm_mon = date_time.month() - 1;     
	timeinfo.tm_mday = date_time.day();     
	timeinfo.tm_hour = date_time.hour();
	timeinfo.tm_min = date_time.minute();
	timeinfo.tm_sec = date_time.second();
	return timeinfo;
}

DateTime from_tm(const std::tm& timeinfo)
{
	if (timeinfo.tm_year < 0) {
		TWIST_THROW(L"std::tm may not work reliably for dates before 1Jan1900.");
	}
	return { timeinfo.tm_mday, timeinfo.tm_mon + 1, timeinfo.tm_year + 1900, 
			 timeinfo.tm_hour, timeinfo.tm_min, timeinfo.tm_sec };
}

std::wstring time_to_str(const DateTime& date_time, bool pad, const std::wstring& sep)
{
	std::wstringstream str;	

	stream(str, date_time.hour(), false/*pad*/);
	str << sep;
	stream(str, date_time.minute(), pad);
	str << sep;
	stream(str, date_time.second(), pad);

	return str.str();
}

std::wstring hour_min_to_str(const DateTime& date_time, bool pad, const std::wstring& sep)
{
	std::wstringstream str;	

	stream(str, date_time.hour(), false/*pad*/);
	str << sep;
	stream(str, date_time.minute(), pad);

	return str.str();
}

DateTime time_from_str(std::wstring_view str, std::wstring_view sep)
{
	int hour{};
	int minute{};
	int second{};
	time_components_from_str(str, sep, hour, minute, second);
	return {1, 1, 1, hour, minute, second};
}

auto time_from_str(const std::wstring& str, TimeStrFormat format) -> DateTime
{
	static const auto allowed_formats = { TimeStrFormat::hhcmm, TimeStrFormat::hhmm };
	if (!has(allowed_formats, format)) {
		TWIST_THROW(L"Only formats 'hhcmm' and 'hhmm' are allowed for this function.");
	}

	int hour = 0;
	int minute = 0;
	int second = 0;
	
	if (format == TimeStrFormat::hhcmm) {
		static const auto sep = L':';
		const auto pos = str.find(sep);
		if (pos == std::wstring::npos) {
			TWIST_THROW(L"Time string \"%s\" does not contain the separator character %c.", str.c_str(), sep);
		}
		hour = to_int_checked(str.substr(0, pos));
		minute = to_int_checked(str.substr(pos + 1));
	}	
	else if (format == TimeStrFormat::hhmm) {
		if (str.size() != 4) {
			TWIST_THROW(L"Invalid time string \"%s\": wrong length.", str.c_str());
		}	
		size_t pos = 0;
		hour = stoi(str.substr(0, 2), &pos);
		if (pos != 2) {
			TWIST_THROW(L"Invalid time string \"%s\": invalid hour substring.", str.c_str());
		}
		minute = stoi(str.substr(2, 2), &pos);
		if (pos != 2) {
			TWIST_THROW(L"Invalid time string \"%s\": invalid minute substring.", str.c_str());
		}
	}

	return {1, 1, 1, hour, minute, second};
}

auto date_time_to_str(const DateTime& datetime, 
                      DateTimeStrFormat format, 
					  const std::wstring& sep1, 
		              const std::wstring& sep2, 
					  const std::wstring& sep3) -> std::wstring
{
	auto date_time_str = std::wstringstream{};	
	
	switch (format) {
		case DateTimeStrFormat::dd_mm_yyyy_hh_mm_ss : {
			date_time_str << date_to_str(datetime, DateStrFormat::dd_mm_yyyy, sep1);
			date_time_str << sep2;
			date_time_str << time_to_str(datetime, true/*pad*/, sep3);
			return date_time_str.str();
		}
		case DateTimeStrFormat::yyyy_mm_dd_hh_mm_ss : {
			date_time_str << date_to_str(datetime, DateStrFormat::yyyy_mm_dd, sep1);
			date_time_str << sep2;
			date_time_str << time_to_str(datetime, true/*pad*/, sep3);
			return date_time_str.str();
		}
		default : {
			TWIST_THROW(L"Invalid date-time string format value %d.", format);
		}
	}
}

auto datetime_from_str(std::wstring_view str, 
                       DateTimeStrFormat format, 
					   std::wstring_view sep1, 
		               std::wstring_view sep2, 
					   std::wstring_view sep3) -> DateTime
{
	if (sep2 == sep1 || sep2 == sep3) {
		TWIST_THROW(L"The second separator \"%s\" must be different from the first and the third.", 
				std::wstring{ sep2 }.c_str());
	}
	const size_t sep2_pos = str.find(sep2);
	if (sep2_pos == std::wstring::npos) {
		TWIST_THROW(L"Date-time string \"%s\" is ill-formed: second separator \"%s\" not found.", 
				std::wstring{ str }.c_str(), std::wstring{ sep2 }.c_str());
	}
	const auto date_str{ str.substr(0, sep2_pos) };
	const auto time_str{ str.substr(sep2_pos + sep2.size()) };

	auto hour = 0;
	auto minute = 0;
	auto second = 0;
	time_components_from_str(time_str, sep3, hour, minute, second);

	switch (format) {
	case DateTimeStrFormat::dd_mm_yyyy_hh_mm_ss: {	
		const auto date = date_from_str(date_str, DateStrFormat::dd_mm_yyyy, sep1);
		return { date, hour, minute, second };
	}
	case DateTimeStrFormat::yyyy_mm_dd_hh_mm_ss: {
		const auto date = date_from_str(date_str, DateStrFormat::yyyy_mm_dd, sep1);
		return { date, hour, minute, second };
	}
	default: 
		TWIST_THROW(L"Invalid date-time string format value %d.", format);
	}
}

auto minutes_since_midnight(const DateTime& time) -> int
{
	return time.hour() * 60 + time.minute();
}

} 

/// @file integer_sequence_utils.hpp
/// Utilities for working with std::integer_sequence

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_INTEGER_SEQUENCE__UTILS_HPP
#define TWIST_INTEGER_SEQUENCE__UTILS_HPP

#include <utility>

namespace twist {

/// Get the position of a specific element in a compile-time sequence of integers.
///
/// @tparam  T  The sequence element type
/// @tparam  elems...  The elements in the sequence
/// @param[in] seq  The sequence
/// @param[in] elem  The element to search for  
/// @return  The position of the first occurrence of the element within the sequence; or -1 if no match 
///				is found
///
template<class T, T... elems>
constexpr int get_pos(std::integer_sequence<T, elems...> seq, T elem);

/// Get the element as a specific position in a compile-time sequence of integers.
///
/// @tparam  pos  The position; a compile error occurs if it is out of bounds
/// @tparam  T  The sequence element type
/// @tparam  elems...  The elements in the sequence
/// @param[in] seq  The sequence
/// @return  The element
///
template<std::size_t pos, class T, T... elems>
constexpr T get_element(std::integer_sequence<T, elems...> seq); 

/// Find out (at compile-time) whether the elements in a compile-time sequence of integers are sorted in 
/// increasing order, ie no element compares as "smaller" than the element before it. 
///
/// @tparam  T  The sequence element type
/// @tparam  elems...  The elements in the sequence
/// @param[in] seq  The sequence
/// @return  true if the sequence is sorted in increasing order
///
template<class T, T... elems>
constexpr bool is_increasing(std::integer_sequence<T, elems...> seq);

/// Find out (at compile-time) whether the elements in a compile-time sequence of integers are sorted in 
/// strictly increasing order, ie no element compares as "not smaller" that the element before it. 
///
/// @tparam  T  The sequence element type
/// @tparam  elems...  The elements in the sequence
/// @param[in] seq  The sequence
/// @return  true if the sequence is sorted in strictly increasing order
///
template<class T, T... elems>
constexpr bool is_strictly_increasing(std::integer_sequence<T, elems...>);

/// Find (at compile-time) an element in a sorted compile-time sequence of integers, using a binary search. 
/// The average performance is O(log n).
///
/// @tparam  T  The sequence element type
/// @tparam  elems...  The elements in the sequence
/// @param[in] seq  The sequence
/// @param[in] elem  The element to search for  
/// @return  The position of the occurrence of the matching element (or one of the matching elements if there 
///				are multiple, contiguous matching elements) within the sequence; or -1 if no match is found
///
template<class T, T... elems>
constexpr int binary_search(std::integer_sequence<T, elems...> seq, T elem);

}

#include "twist/integer_sequence_utils.ipp"

#endif

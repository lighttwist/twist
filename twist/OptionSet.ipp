/// @file OptionSet.ipp
/// Inline implementation file for "OptionSet.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace twist {

template<class Opt>
OptionSet<Opt>::OptionSet() 
{
	TWIST_CHECK_INVARIANT
}

template<class Opt>
OptionSet<Opt>::OptionSet(Opt opt) 
{
	set_.insert(opt);
	TWIST_CHECK_INVARIANT
}

template<class Opt>
OptionSet<Opt>::OptionSet(std::initializer_list<Opt> options) 
{
	for (auto opt : options) {
		add(opt);
	}
	TWIST_CHECK_INVARIANT
}

template<class Opt>
auto OptionSet<Opt>::add(Opt option) -> void
{
	TWIST_CHECK_INVARIANT
	if (auto [it, ok] = set_.insert(option); !ok) {
		TWIST_THRO2(L"The set already contains this option");
	}
}

template<class Opt>
auto OptionSet<Opt>::has(Opt option) const -> bool
{
	TWIST_CHECK_INVARIANT
	return set_.contains(option);
}

template<class Opt>
auto OptionSet<Opt>::size() const -> Ssize
{
	TWIST_CHECK_INVARIANT
	return ssize(set_);
}

template<class Opt>
auto OptionSet<Opt>::is_empty() const -> bool
{
	TWIST_CHECK_INVARIANT
	return set_.empty();
}

template<class Opt>	
auto OptionSet<Opt>::all() const -> OptionRange
{
	TWIST_CHECK_INVARIANT
	return set_;
}

template<class Opt>	
template<InvocableR<bool, Opt> Pred>
auto OptionSet<Opt>::test_all(Pred pred) const -> bool
{
	TWIST_CHECK_INVARIANT
	return rg::all_of(set_, pred);
}

#ifdef _DEBUG
template<class Opt>
auto OptionSet<Opt>::check_invariant() const noexcept -> void
{
}
#endif 

}

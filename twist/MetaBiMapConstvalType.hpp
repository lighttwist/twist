///  @file  MetaBiMapConstvalType.hpp
///  MetaBiMapConstvalType class definition

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_META_BI_MAP_CONSTVAL_TYPE_HPP
#define TWIST_META_BI_MAP_CONSTVAL_TYPE_HPP

#include "MetaConstvalSequence.hpp"
#include "metaprogramming.hpp"

namespace twist {

/// List of compile-time constant values to be used with MetaBiMapConstvalType.
///
/// @tparam  Cval  The constant value type; must be convertible to an integer type via static_cast
/// @tparam  constvals...  The constant value elements in the list 
///
template<class Cval, Cval... constvals>
using MetaMapConstvalList = MetaConstvalSequence<Cval, constvals...>; 

/// List of types to be used with MetaBiMapConstvalType.
///
/// @tparam  Cval  The constant value type; must be convertible to an integer type via static_cast
/// @tparam  Types...  The type elements in the list 
///
template<class... Types>
using MetaMapTypeList = std::tuple<Types...>;

/// @cond  Doxygen exclusion section
template<class Cvals, class Types>
class MetaBiMapConstvalType {
	// Disable the primary template to emit a compile error if MetaBiMapConstvalType is instantiated with  
	// arguments which are not themselves instantiations of MetaMapConstvalList and MetaMapTypeList; see the 
	// MetaBiMapConstvalType specialisation for the "real" class definition 
	static_assert(always_false<Cvals>, 
			"Cvals must be an instantiation of MetaMapConstvalList and Types of MetaMapTypeList");
};
/// @endcond  

/// Compile-time bi-directional map which associates unique compile-time constant values to types.
/// The constant values added to the map must be unique and orderable via the "<" operator.
/// The map can be searched on constant value, which will return the type corresponding to the occurrence of 
///   the value in the list of values (this search runs in logarithmic time), or on type, which will return 
///   the constant value corresponding to the first occurrence of the type in the list of types (this search 
///   runs in linear time).
///
/// @tparam  Cval  The compile-time constant value type; must be convertible to an integer type 
///				via static_cast
/// @tparam  constvals...  The list of compile-time constant values; must be sorted in strictly increasing 
///				order 
/// @tparam  Types...  The list of types; the order of elements in both list establishes the corresponding 
///				pairs (the first constant value corresponds to the first type, etc); the two list must have 
///				the same size  
///
template<class Cval, Cval... constvals, class... Types>
class MetaBiMapConstvalType<MetaMapConstvalList<Cval, constvals...>, MetaMapTypeList<Types...>> {
private:
	template<Cval constval>
	static constexpr int get_constval_pos();

	template<Cval constval>
	static constexpr int get_valid_constval_pos();
	
public:
	/// The constant value type
	using Constval = Cval;

	/// The list of constant values; has the same size as the map
	using ConstvalList = MetaMapConstvalList<Cval, constvals...>;

	/// The list of types (matches the list of constant values); has the same size as the map
	using TypeList = MetaMapTypeList<Types...>;

	/// The size of the map.
	static constexpr std::size_t size();

	/// Find out whether the map contains a specific constant value. The search runs in logarithmic time.
	///
	/// @tparam  constval  The constant value to search for
	/// @return  true if the value exists in the map's list of constant values
	///
	template<Cval constval>
	static constexpr bool contains_constval(); 

	/// Find out whether the map contains a specific type. The search runs in linear time.
	///
	/// @tparam  Type  The type to search for
	/// @return  true if the value exists in the map's list of constant values
	///
	template<class Type>
	static constexpr bool contains_type(); 

	/// Metafunction which resolves to the type corresponding to a specific constant value. The search runs in
	/// logarithmic time.
	///
	/// @tparam  constval  The constant value; if is not found in the list of constant values, a compile 
	///				error occurs
	///
	template<Cval constval>
	using FindType = std::tuple_element_t<get_valid_constval_pos<constval>(), TypeList>;

	/// Find the constant value corresponding to a specific type. If the list of types contains duplicates, 
	/// the function will consider the first occurrence of the type within the type list. The search runs in
	/// linear time.
	///
	/// @tparam  Type  The type to search for; if it is not found in the type list, a compile error occurs
	/// @return  The constant value
	///
	template<class Type>
	static constexpr Constval find_constval();

	static_assert(size() == std::tuple_size_v<TypeList>, 
			"The list of constant values must have the same size as the list of types.");

	static_assert(is_strictly_increasing(ConstvalList{}), 
			"The list of constant values must be sorted in strictly increasing order.");
};

/// Metafunction which, given an instantiation of MetaBiMapConstvalType and a compile-time constant value, 
/// evaluates to MetaBiMapConstvalType::FindType.
///
/// @tparam  Map  The metamap type, an instantiation of MetaBiMapConstvalType
/// @tparam  constval  The constant value which serves as a key in the search
///
template<class Map, typename Map::Constval constval>
using MetaMapFindTypeForConstval = typename Map::template FindType<constval>;  

/// Function which, given an instantiation of MetaBiMapConstvalType and a type, evaluates to 
/// MetaBiMapConstvalType::find_constval().
///
/// @tparam  Map  The metamap type, an instantiation of MetaBiMapConstvalType
/// @tparam  Type  The type which serves as a key in the search
///
template<class Map, class Type>
constexpr typename Map::Constval meta_map_find_constval();

} 

#include "MetaBiMapConstvalType.ipp"

#endif 

///  @file  LogFileProgressProv.cpp
///  Implementation file for "LogFileProgressProv.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "LogFileProgressProv.hpp"

namespace twist {

LogFileProgressProv::LogFileProgressProv(const fs::path& log_path) 
	: ProgressProv{}
	, log_path_{log_path}
	, log_file_open_{false}
{
	if (!open_log_file()) {
		TWIST_THROW(L"Cannot open log file \"%s\".", log_path_.c_str());
	}
	TWIST_CHECK_INVARIANT
}


LogFileProgressProv::~LogFileProgressProv()
{
	close_log_file();
	TWIST_CHECK_INVARIANT
}


void LogFileProgressProv::init_message_mode()
{
	TWIST_CHECK_INVARIANT
	// Do nothing
}


void LogFileProgressProv::init_hist_mode(long /*expected_num_lines*/)
{
	TWIST_CHECK_INVARIANT
	// Do nothing
}


void LogFileProgressProv::set_prog_msg(std::wstring_view msg, bool /*force_update*/)
{
	TWIST_CHECK_INVARIANT
	write_line_to_log_file(msg);
	TWIST_CHECK_INVARIANT
}


void LogFileProgressProv::add_new_hist_line(const std::wstring& hist_line)
{
	TWIST_CHECK_INVARIANT
	write_line_to_log_file(hist_line);
	TWIST_CHECK_INVARIANT
}


void LogFileProgressProv::set_present_hist_line(const std::wstring& hist_line)
{
	TWIST_CHECK_INVARIANT
	write_line_to_log_file(hist_line);
	TWIST_CHECK_INVARIANT
}


void LogFileProgressProv::get_prog_range(Ssize& min_pos, Ssize& max_pos) const
{
	TWIST_CHECK_INVARIANT
	min_pos = 0;
	max_pos = 0;
	TWIST_CHECK_INVARIANT
}


void LogFileProgressProv::set_prog_range(Ssize /*min_pos*/, Ssize /*max_pos*/)
{
	TWIST_CHECK_INVARIANT
	// Do nothing
}


Ssize LogFileProgressProv::get_prog_pos() const
{
	TWIST_CHECK_INVARIANT
	return 0;
}


void LogFileProgressProv::set_prog_pos(Ssize /*pos*/, bool /*force_update*/)
{
	TWIST_CHECK_INVARIANT
	// Do nothing
}


void LogFileProgressProv::inc_prog_pos(bool /*force_update*/)
{
	TWIST_CHECK_INVARIANT
	// Do nothing
}


void LogFileProgressProv::hide()
{
	TWIST_CHECK_INVARIANT
}


bool LogFileProgressProv::log_to_file() const
{
	TWIST_CHECK_INVARIANT
	return !log_path_.empty();
}


bool LogFileProgressProv::open_log_file() 
{
	TWIST_CHECK_INVARIANT
	if (log_path_.empty()) {
		TWIST_THROW(L"This provider does not use a log file");
	}
	if (log_file_open_) {
		TWIST_THROW(L"Log file already open");
	}
	log_file_stream_.open(log_path_.c_str());
	log_file_open_ = log_file_stream_.good();	
	return log_file_open_;
}


void LogFileProgressProv::close_log_file() 
{
	TWIST_CHECK_INVARIANT
	if (log_path_.empty()) {
		TWIST_THROW(L"This provider does not use a log file.");
	}
	if (log_file_open_) {
		log_file_stream_.close();
		log_file_open_ = false;
	}
}


void LogFileProgressProv::write_line_to_log_file(std::wstring_view line)
{
	TWIST_CHECK_INVARIANT
	if (log_path_.empty()) {
		TWIST_THROW(L"This provider does not use a log file.");
	}
	if (log_file_open_) {
		log_file_stream_ << line << L"\n";
	}
}


#ifdef _DEBUG
void LogFileProgressProv::check_invariant() const noexcept
{
	assert(!log_path_.empty());
}
#endif

} // namespace twist

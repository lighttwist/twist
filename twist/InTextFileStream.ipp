///  @file  InTextFileStream.ipp
///  Inline implementation file for "InTextFileStream.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

namespace twist {

template<typename Chr>
InTextFileStream<Chr>::InTextFileStream(std::unique_ptr<FileStd> in_file)
	: InFileStream<Chr>{ move(in_file) }
{
	TWIST_CHECK_INVARIANT
}


template<typename Chr>
InTextFileStream<Chr>::~InTextFileStream()
{
	TWIST_CHECK_INVARIANT
}


template<typename Chr>
void InTextFileStream<Chr>::get_line(Chr* line_buffer, Ssize buffer_len, Ssize& line_len)
{
	TWIST_CHECK_INVARIANT
	line_len = 0;
	while (true) {

		if (this->is_eof()) {
			break;
		}		

		const auto c = this->get();

		if (is_newline(c)) {
			if (!last_was_carriage_ret_) {
				break;
			}
			else {
				last_was_carriage_ret_ = false;			
				continue;
			}
		}
		if (is_carriage_return(c)) {
			last_was_carriage_ret_ = true;
			break;
		}
		if (line_len >= buffer_len) {
			last_was_carriage_ret_ = false;
			break;
		}

		line_buffer[line_len++] = c;
	}
}


template<typename Chr>
void InTextFileStream<Chr>::get_line(std::basic_string<Chr>& line)
{
	TWIST_CHECK_INVARIANT
	line.clear();
	for (auto c = this->get(); !is_newline(c) && !this->is_eof(); c = this->get()) {  
		line += c;
	} 
}


#ifdef _DEBUG
template<typename Chr>
void InTextFileStream<Chr>::check_invariant() const noexcept
{
}
#endif 

} 



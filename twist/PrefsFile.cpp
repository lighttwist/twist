///  @file  PrefsFile.cpp
///  Implementation file for "PrefsFile.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "PrefsFile.hpp"

#include "string_utils.hpp"

#include "db/xml_utils.hpp"

namespace twist {

static const wchar_t*  k_top_xnode_version_attr = L"version";


PrefsFile::PrefsFile(const fs::path& path, const std::wstring& root_xnode_name, int current_version, 
		bool must_exist)
	: path_{ path }
	, current_version_{ current_version }
	, xdoc_{}
	, root_xnode_{}
{
	if (!exists(path)) {
		if (must_exist) {
			TWIST_THROW(L"The application preferences file \"%s\" cannot be found.", path.c_str());	
		}
		// The file does not exist. Create a new XML document with the appropriate root node.
		const std::wstring xml_string = format_str(L"<?xml version=\"1.0\"?><%s %s =\"%d\"></%s>", 
				root_xnode_name.c_str(), k_top_xnode_version_attr, current_version, root_xnode_name.c_str());

		xdoc_ = twist::db::XmlDoc::read_from_string(xml_string);
	}
	else {
		try {
			xdoc_ = twist::db::XmlDoc::read_from_file(path);
		}
		catch (RuntimeError& e) {
			TWIST_THROW(L"Error reading the application preferences file \"%s\" : %s.", 
					    path.c_str(), error_message(e).c_str());
		}
	}

	// Check the root node name
	root_xnode_.reset( new db::XmlElemNode(xdoc_->root_elem()) );
	if (root_xnode_->get_tag() != root_xnode_name) {
		TWIST_THROW(L"The root XML node of the preference file \"%s\" has the wrong tag \"%s\" (expected tag \"%s\").", path.c_str(), root_xnode_->get_tag().c_str(), root_xnode_name.c_str());
	}

	// Check the file version
	const int file_version = root_xnode_->get_attr(k_top_xnode_version_attr).get_int_value();
	if (file_version < current_version_) {
		TWIST_THROW(L"The preference file \"%s\" has version %d, which is older than "
				"version %d supported by the program. File reorganisation is not yet implemented.", 
				path.c_str(), file_version, current_version_);
	}
	else if (file_version > current_version_) {
		TWIST_THROW(L"The preference file \"%s\" has version %d, which is newer than "
				"version %d supported by the program.", path.c_str(), file_version, current_version_);
	}

	TWIST_CHECK_INVARIANT
}


PrefsFile::~PrefsFile()
{
	TWIST_CHECK_INVARIANT
}


fs::path PrefsFile::path() const
{
	TWIST_CHECK_INVARIANT
	return path_;
}


const db::XmlDoc& PrefsFile::get_xdoc() const
{
	TWIST_CHECK_INVARIANT
	return *xdoc_;
}


db::XmlDoc& PrefsFile::get_xdoc()
{
	TWIST_CHECK_INVARIANT
	return *xdoc_;
}


const db::XmlElemNode& PrefsFile::get_root_xnode() const
{
	TWIST_CHECK_INVARIANT
	return *root_xnode_;
}


bool PrefsFile::get_unique_child_node_text(const db::XmlElemNode& parent, const std::wstring& child_tag, 
		std::wstring& child_val) const
{
	TWIST_CHECK_INVARIANT
	bool ret = false;

	auto child_xnode = parent.find_unique_child_with_tag(child_tag);
	if (child_xnode) {
		if (child_xnode->has_text()) {
			child_val = child_xnode->get_text();
		}
		ret = true;
	}
	return ret;
}


void PrefsFile::set_unique_child_node_text(const db::XmlElemNode& parent, const std::wstring& child_tag, 
		const std::wstring& child_val)
{
	TWIST_CHECK_INVARIANT
	ensure_unique_child_with_tag(parent, child_tag).set_text(child_val);
	TWIST_CHECK_INVARIANT
}


bool PrefsFile::get_unique_child_node_text(const db::XmlElemNode& parent, const std::wstring& child_tag, bool& child_val) const
{
	TWIST_CHECK_INVARIANT
	bool ret = false;

	auto child_xnode = parent.find_unique_child_with_tag(child_tag);
	if (child_xnode) {
		if (child_xnode->has_text()) {
			child_val = child_xnode->get_text_as_bool();
		}
		ret = true;
	}
	return ret;
}


void PrefsFile::set_unique_child_node_text(const db::XmlElemNode& parent, const std::wstring& child_tag, bool child_val)
{
	TWIST_CHECK_INVARIANT		
	ensure_unique_child_with_tag(parent, child_tag).set_text_from_bool(child_val);
	TWIST_CHECK_INVARIANT
}


bool PrefsFile::get_unique_child_node_text(const db::XmlElemNode& parent, const std::wstring& child_tag, int& child_val) const
{
	TWIST_CHECK_INVARIANT
	bool ret = false;

	auto child_xnode = parent.find_unique_child_with_tag(child_tag);
	if (child_xnode) {
		if (child_xnode->has_text()) {
			child_val = child_xnode->get_text_as_int();
		}
		ret = true;
	}
	return ret;
}


void PrefsFile::set_unique_child_node_text(const db::XmlElemNode& parent, const std::wstring& child_tag, int child_val)
{
	TWIST_CHECK_INVARIANT
	ensure_unique_child_with_tag(parent, child_tag).set_text_from_int(child_val);
	TWIST_CHECK_INVARIANT
}


twist::db::XmlElemNode PrefsFile::ensure_unique_child_with_tag(const db::XmlElemNode& parent, const std::wstring& tag)
{
	TWIST_CHECK_INVARIANT
	auto existing_child_xnode = parent.find_unique_child_with_tag(tag);
	if (existing_child_xnode) {

		TWIST_CHECK_INVARIANT
		return *existing_child_xnode;
	}

	TWIST_CHECK_INVARIANT
	return xdoc_->create_elem_node(tag, parent);
}


#ifdef _DEBUG
void PrefsFile::check_invariant() const noexcept
{
	assert(!path_.empty());
	assert(current_version_ > 0);
	assert(xdoc_);
	assert(root_xnode_);
}
#endif

} // namespace twist


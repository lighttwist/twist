/// @file FileVersionInfo.cpp
/// Implementation file for "FileVersionInfo.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/FileVersionInfo.hpp"

namespace twist {

FileVersionInfo::FileVersionInfo(long major_ver_no, long minor_ver_no, long release_no, long build_no)
	: major_ver_no_(major_ver_no)
    , minor_ver_no_(minor_ver_no)
    , release_no_(release_no)
	, build_no_(build_no)
{
	TWIST_CHECK_INVARIANT
}

long FileVersionInfo::major_ver_no() const
{
	TWIST_CHECK_INVARIANT
	return major_ver_no_;
}

long FileVersionInfo::minor_ver_no() const
{
	TWIST_CHECK_INVARIANT
	return minor_ver_no_;
}

long FileVersionInfo::release_no() const
{
	TWIST_CHECK_INVARIANT
	return release_no_;
}

long FileVersionInfo::build_no() const
{
	TWIST_CHECK_INVARIANT
	return build_no_;
}

bool FileVersionInfo::operator==(const FileVersionInfo& rhs) const 
{
	TWIST_CHECK_INVARIANT
	return major_ver_no_ == rhs.major_ver_no_ && minor_ver_no_ == rhs.minor_ver_no_ && 
		   release_no_ == rhs.release_no_ && build_no_ == rhs.build_no_;
}

bool FileVersionInfo::operator!=(const FileVersionInfo& rhs) const 
{
	TWIST_CHECK_INVARIANT
	return !operator==(rhs);
}

#ifdef _DEBUG
void FileVersionInfo::check_invariant() const noexcept
{
    assert(major_ver_no_ >= 0);
    assert(minor_ver_no_ >= 0);
    assert(release_no_ >= 0);
	assert(build_no_ >= 0);
}
#endif

// --- Free functions ---

auto to_str(const FileVersionInfo& info, bool zap_end_zeros) -> std::wstring
{
	auto version_str = std::wstringstream{};
	version_str << info.major_ver_no() << L"." << info.minor_ver_no();
	
	if (!zap_end_zeros || info.build_no() != 0) {
		version_str << L"." << info.release_no()<< L"." << info.build_no();
	}
	else if (info.release_no() != 0) {
		version_str << L"." << info.release_no();
	}
	
	return version_str.str();
}

auto file_version_info_from_str(const std::wstring& str) -> FileVersionInfo
{
	auto major_ver_no = long{0};
	auto minor_ver_no = long{0};
	auto release_no = long{0};
	auto build_no = long{0};

	auto version_no_strings = std::vector<std::wstring>{};
	const auto nof_version_no_strings = extract_delimited_fields(str, L".", back_inserter(version_no_strings));
	if (nof_version_no_strings == 0 || nof_version_no_strings > 4) {
		TWIST_THRO2(L"Cannot convert string \"{}\" to a file version.", str);
	}
	if (nof_version_no_strings >= 1) {
		major_ver_no = to_int_checked(version_no_strings[0]);
	}
	if (nof_version_no_strings >= 2) {
		minor_ver_no = to_int_checked(version_no_strings[1]);
	}
	if (nof_version_no_strings >= 3) {
		release_no = to_int_checked(version_no_strings[2]);
	}
	if (nof_version_no_strings == 4) {
		build_no = to_int_checked(version_no_strings[3]);
	}

	return FileVersionInfo{major_ver_no, minor_ver_no, release_no, build_no};
}

} 

///  @file  ObjectMarker.ipp
///  Inline implementation file for "ObjectMarker.hpp"

//   Written by Dan Ababei
//   Copyright (c) 2007-2020 Delft University of Technology

namespace twist {

//
//  ObjectMarker class template
//

template<class Obj>
ObjectMarker<Obj>::ObjectMarker()
{
}


template<class Obj>
typename ObjectMarker<Obj>::ScopedMarker ObjectMarker<Obj>::make_scoped_marker(const Obj& obj) 
{
	return ScopedMarker{ objects_, obj };
}


template<class Obj>
bool ObjectMarker<Obj>::is_marked(const Obj& obj) const
{
	return has(objects_, obj);
}

//
//  ObjectMarker::ScopedMarker class 
//

template<class Obj>
ObjectMarker<Obj>::ScopedMarker::ScopedMarker(std::set<Obj>& objects, const Obj& obj)
	: objects_{ objects }
	, obj_{ obj }
{
	if (auto result = objects_.insert(obj_); !result.second) {
		TWIST_THROW(L"This object is currently marked.");
	}
}


template<class Obj>
ObjectMarker<Obj>::ScopedMarker::~ScopedMarker()
{
	[[maybe_unused]] auto erased_count = objects_.erase(obj_);		
	assert(erased_count == 1);
}

} 

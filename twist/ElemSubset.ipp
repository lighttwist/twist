///  @file  ElemSubset.ipp
///  Inline implementation file for "ElemSubset.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

namespace twist {

//
//  ElemSubset<TElem>  class
//

template<typename TElem> 
ElemSubset<TElem>::ElemSubset()
	: type_(ElemSubsetType::k_no_elems)
	, elems_()
{
	TWIST_CHECK_INVARIANT
}


template<typename TElem> 
ElemSubset<TElem>::ElemSubset(const ElemSubset& src)
	: type_(src.type_)
	, elems_(src.elems_)
{
	TWIST_CHECK_INVARIANT
}


template<typename TElem> 
ElemSubset<TElem>::ElemSubset(ElemSubset&& src)
	: type_(src.type_)
	, elems_(move(src.elems_))
{
	src.type_ = ElemSubsetType::k_no_elems;
	TWIST_CHECK_INVARIANT
}


template<typename TElem> 
ElemSubset<TElem>& ElemSubset<TElem>::operator=(const ElemSubset& rhs)
{
	TWIST_CHECK_INVARIANT
	if (&rhs != this) {
		type_  = rhs.type_;
		elems_ = rhs.elems_;
	}
	TWIST_CHECK_INVARIANT
	return *this;
}


template<typename TElem> 
ElemSubset<TElem>& ElemSubset<TElem>::operator=(ElemSubset&& rhs)
{
	TWIST_CHECK_INVARIANT
	type_  = rhs.type_;
	elems_ = move(rhs.elems_);
	rhs.type_ = ElemSubsetType::k_no_elems;
	TWIST_CHECK_INVARIANT
	return *this;
}


template<typename TElem> 
ElemSubsetType ElemSubset<TElem>::get_type() const
{
	TWIST_CHECK_INVARIANT
	return type_;
}


template<typename TElem> 
void ElemSubset<TElem>::set_no_elems()
{
	TWIST_CHECK_INVARIANT
	type_ = ElemSubsetType::k_no_elems;
	elems_.clear();
	TWIST_CHECK_INVARIANT
}


template<typename TElem> 
const std::set<TElem>& ElemSubset<TElem>::get_specific_elems() const
{
	TWIST_CHECK_INVARIANT
	if (type_ != ElemSubsetType::k_specific_elems) {
		TWIST_THROW(L"The subset type is not \"specific elements\".");
	}
	return elems_;
}


template<typename TElem> 
void ElemSubset<TElem>::set_specific_elems(const std::set<Elem>& elems)
{
	TWIST_CHECK_INVARIANT
	type_ = ElemSubsetType::k_specific_elems;
	elems_ = elems;
	TWIST_CHECK_INVARIANT
}


template<typename TElem> 
void ElemSubset<TElem>::set_all_elems()
{
	TWIST_CHECK_INVARIANT
	type_ = ElemSubsetType::k_all_elems;
	elems_.clear();
	TWIST_CHECK_INVARIANT
}


#ifdef _DEBUG
template<typename TElem> 
void ElemSubset<TElem>::check_invariant() const noexcept
{
	assert(type_ >= ElemSubsetType::k_no_elems && type_ <= ElemSubsetType::k_all_elems);
	assert((type_ != ElemSubsetType::k_specific_elems) == elems_.empty());
}
#endif


//
//  Free functions
//

template<typename TElem> 
ElemSubset<TElem> make_subset_wih_all_elems()
{
	ElemSubset<TElem> subset;
	subset.set_all_elems();
	return subset;
}


template<typename TElem> 
ElemSubset<TElem> make_subset_wih_specific_elems(const std::set<TElem>& elems)
{
	ElemSubset<TElem> subset;
	subset.set_specific_elems(elems);
	return subset;
}


template<typename TElem> 
bool has(const ElemSubset<TElem>& subset, const TElem& elem)
{
	if (subset.get_type() == ElemSubsetType::k_all_elems) {
		return true;
	}
	else if (subset.get_type() == ElemSubsetType::k_specific_elems) {
		return subset.get_specific_elems().count(elem) != 0;
	}
	return false;
}

} // namespace twist


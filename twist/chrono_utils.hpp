/// @file chrono_utils.hpp
/// Utilities for working with the std::chrono sublibrary

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_CHRONO__UTILS_HPP
#define TWIST_CHRONO__UTILS_HPP

#include "DateTime.hpp"

namespace twist {

/// A number of ticks adding up to 24 hours 
using DurationDays = std::chrono::duration<int, 
		std::ratio_multiply<std::chrono::hours::period, std::ratio<24>>>;

/// Clock class which is based on another, existing clock class, but which uses another epoch, obtained 
/// by altering the original clock's epoch.
///
/// @tparam  Clock  The original clock class, must satisfy the standard "Clock" requirements
/// @tparam  secs_since_orig_epoch  The difference, in seconds, between the  original clock's epoch and the 
///				epoch of this clock
///
template<class Clock, typename Clock::rep secs_since_orig_epoch>
struct CustomClock {
    
	using rep = typename Clock::rep;
    
	using period = typename Clock::period;
    
	using duration = typename Clock::duration;

    using time_point = typename std::chrono::time_point<CustomClock>;

	/// Whether this clock is a "steady clock" (which is the same as asking whether the original clock is).
    static constexpr bool is_steady = Clock::is_steady;

	/// This clock's epoch, expressed as a time point of the original clock.
    static constexpr typename Clock::time_point epoch = 
			typename Clock::time_point{ std::chrono::seconds(secs_since_orig_epoch) };

	/// A time point representing the current time.
    static time_point now() 
	{
        return time_point{ Clock::now() - epoch };
    }
};

/// Clock class which is based on the system_vlock class, but which uses another epoch, obtained by altering 
/// the system clock's epoch (which is usually the Unix Time, always from C++20).
///
/// @tparam  secs_since_sys_epoch  The difference, in seconds, between the system_clock epoch and the 
///				epoch of this clock
///
template<std::chrono::system_clock::rep secs_since_sys_epoch> 
using CustomSysClock = CustomClock<std::chrono::system_clock, secs_since_sys_epoch>;

/// Get the duration, in milliseconds, between a time-point in the past and "now" (the moment when this 
/// function is called).
///
/// @tparam  Clock  The clock type used by the time-point
/// @tparam  Duration  The duration type used by the time-point
/// @param[in] timept  The past time-point
/// @return  The number of milliseconds elapsed since the time-point 
///
template<class Clock, class Duration>
long long millisecs_since(std::chrono::time_point<Clock, Duration> timept);

} 

#include "chrono_utils.ipp"

#endif 

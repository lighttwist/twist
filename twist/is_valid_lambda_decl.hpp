///  @file  is_valid_lambda_decl.hpp
///  is_valid_lambda_decl constexpr lambda variable

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_IS__VALID__LAMBDA__DECL_HPP
#define TWIST_IS__VALID__LAMBDA__DECL_HPP

#include <type_traits>

namespace twist {

namespace detail {

// Checks the validiy of fn(args...) for Fn f and Args... args
template<class Fn, class... Args,
		 class = decltype(std::declval<Fn>()(std::declval<Args&&>()...))>
std::true_type is_valid_lambda_decl_impl(void*);

// Fallback if overload is SFINAE'd out
template<class Fn, class... Args>
std::false_type is_valid_lambda_decl_impl(...);

// Helper class template to represent a type as a value.
template<class T>
struct TypeAsVal {
	using Type = T;
};

}

/// Generic lambda which, given a second lambda, generates a third lambda which can later be used to perform 
///   a compile-time check wheteher the declaration part of the second lambda expression is valid (in an 
///   unevaluated context) when invoked with a specific set of parameter types.
/// Inspired by Louis Dionne's is_valid() lambda from the Boost.Hana library 
///   (see www.boost.org/doc/libs/1_61_0/libs/hana/doc/html/index.html)
#pragma warning(push)
#pragma warning(disable: 4100)  // Get rid of spurious Visual Studio warning 
inline constexpr auto is_valid_lambda_decl = [](auto lambda2) {
	auto lambda3 = [](auto&& ...args) {
		return decltype(
				twist::detail::is_valid_lambda_decl_impl<decltype(lambda2), decltype(args)&&...>(nullptr)){};
	};
	return lambda3;
};
#pragma warning(pop)

/// Helper variable template to wrap a type as a value.
template<class T>
constexpr auto type_to_val = twist::detail::TypeAsVal<T>{};

/// Helper function template declaration to unwrap a wrapped type in unevaluated contexts.
template<class T>
T type_from_val(twist::detail::TypeAsVal<T>);  

/// Helper function template declaration to unwrap a wrapped type, as the const-qualified type, in unevaluated 
/// contexts.
template<class T>
const T const_type_from_val(twist::detail::TypeAsVal<T>);  

} 

#endif 


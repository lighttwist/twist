///  @file  meta_ranges.hpp
///  Metaprogramming utilities for working with "range" types

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_META__RANGES_HPP
#define TWIST_META__RANGES_HPP

#include <iterator>
#include <utility>

#include "metaprogramming.hpp"
#include "meta_misc_traits.hpp"

#include "meta_ranges.ipp"

namespace twist {

/// If Rng supports range semantics (ie both begin() and end() can be called on a value of type Rng 
/// and yield the same - iterator - type) then this struct inherits std::true_type; otherwise it inherits 
/// std::false_type. 
template<class Rng>
using IsAnyRange = twist::detail::IsAnyRangeImpl<Rng>; 

/// If IsAnyRange<Rng> resolves to std::true_type (or derived), this alias resolves to the type int. 
/// Otherwise the expression is ill-formed. 
template<class Rng>
using EnableIfAnyRange = std::enable_if_t<IsAnyRange<Rng>::value, int>;

/*! If Rng supports range semantics(ie both begin() and end() can be called on a value of type Rng
    and yield the same - iterator - type) and Elem is constructible from the result of dereferencing the 
    iterator type, then this struct inherits std::true_type; otherwise it inherits std::false_type. 
 */
template<class Rng, class Elem>
using IsRangeAndGives = twist::detail::IsRangeAndGivesImpl<Rng, Elem>; 

/*! This constant is true if Rng supports range semantics (ie both begin() and end() can be called on a value of type 
    Rng and yield the same - iterator - type) and Elem is constructible from the result of dereferencing the iterator 
    type; otherwise it is false. 
 */
template<class Rng, class Elem>
constexpr bool is_range_and_gives = twist::detail::IsRangeAndGivesImpl<Rng, Elem>::value; 

/// If IsRangeAndGives<Rng, Elem> resolves to std::true_type (or derived), this alias resolves to the type int. 
/// Otherwise the expression is ill-formed. 
template<class Rng, class Elem>
using EnableIfRangeGives = std::enable_if_t<IsRangeAndGives<Rng, Elem>::value, int>;

/// If Rng supports range semantics (ie both begin() and end() can be called on a value of type Rng 
/// and yield the same - iterator - type) and the type yielded by dereferencing the iterator type is 
/// constructible from Elem, then this struct inherits std::true_type; otherwise it inherits std::false_type. 
template<class Rng, class Elem>
using IsRangeAndTakes = twist::detail::IsRangeAndTakesImpl<Rng, Elem>; 

/// If IsRangeAndTakes<Rng, Elem> resolves to std::true_type (or derived), this alias resolves to the type int. 
/// Otherwise the expression is ill-formed. 
template<class Rng, class Elem>
using EnableIfRangeTakes = std::enable_if_t<IsRangeAndTakes<Rng, Elem>::value, int>;

/// If IsAnyRange<Rng> resolves to std::true_type (or derived), this alias resolves to the type of the 
/// range element. Otherwise a compile error occurs.
template<class Rng>
using RangeElement = typename twist::detail::RangeElementImpl<Rng>::Type;

/*! If IsAnyRange<Rng> resolves to std::true_type (or derived), this alias resolves to a lvalue reference type that 
    refers to the constant range element type. Otherwise a compile error occurs.
 */
template<class Rng>
using RangeElementCref = AddCref<RangeElement<Rng>>;

/*! If IsAnyRange<Rng> resolves to std::true_type (or derived), this alias resolves to a lvalue reference type 
    that refers to the range element type. Otherwise a compile error occurs.
 */
template<class Rng>
using RangeElementRef = AddRef<RangeElement<Rng>>;

/*! If IsAnyRange<Rng> resolves to std::true_type (or derived), this alias resolves to a pointer type that refers to 
    the constant range element type. Otherwise a compile error occurs.
 */
template<class Rng>
using RangeElementCptr = AddCptr<RangeElement<Rng>>;

/*! If IsAnyRange<Rng> resolves to std::true_type (or derived), this alias resolves to a pointer type that refers to 
    the range element type. Otherwise a compile error occurs.
 */
template<class Rng>
using RangeElementPtr = AddPtr<RangeElement<Rng>>;

/// If IsNumber<RangeElement<Rng>> resolves to std::true_type (or derived), this alias resolves to the 
/// type int. Otherwise the expression is ill-formed. 
template<class Rng>
using EnableIfNumberRange = std::enable_if_t<IsNumber<RangeElement<Rng>>::value, int>;

/// If Cont supports the semantics of a container which stored its elements contiguously in memory (ie it 
/// supports range semantics - see IsAnyRange - and rg::size() and std::data() can be called on it) then this 
/// struct inherits std::true_type; otherwise it inherits std::false_type. 
template<class Cont>
using IsContigContainer = twist::detail::IsContigContainerImpl<Cont>; 

/// If IsContigContainer<Cont> resolves to std::true_type (or derived), this alias resolves to 
/// the type int. Otherwise the expression is ill-formed. 
template<class Cont>
using EnableIfContigContainer = std::enable_if_t<IsContigContainer<Cont>::value, int>;

/// If Cont supports the semantics of a container which stored its elements contiguously in memory (ie it 
/// supports range semantics - see IsRangeAndGives - and rg::size() and std::data() can be called on it, and 
/// return appropriate types) then this struct inherits std::true_type; otherwise it inherits std::false_type. 
template<class Cont, class Elem>
using IsContigContainerAndGives = twist::detail::IsContigContainerAndGivesImpl<Cont, Elem>; 

/// If IsContigContainerAndGives<Cont, Elem> resolves to std::true_type (or derived), this alias resolves to 
/// the type int. Otherwise the expression is ill-formed. 
template<class Cont, class Elem>
using EnableIfContigContainerGives = std::enable_if_t<IsContigContainerAndGives<Cont, Elem>::value, int>;

} 

#endif 

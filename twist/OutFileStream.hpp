///  @file  OutFileStream.hpp
///  OutFileStream class template 

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_OUT_FILE_STREAM_HPP
#define TWIST_OUT_FILE_STREAM_HPP

#include "File.hpp"

namespace twist {

///
///  Class to be used for streaming values out to a file in an efficient way.
///	 
///  @tparam  TValue  The type of the values being streamed out to the file
///
template<typename TValue>
class OutFileStream {
public:
	/// Constructor.
	///
	/// @param[in] out_file  The output file. Must be open in write mode. The streaming will start from the 
	///					current position of its file pointer.
	///			
	OutFileStream(std::unique_ptr<FileStd> out_file);

	/// Destructor.
	///
	virtual ~OutFileStream();
	
	/// Stream a value out to the file.
	///
	/// @param[in] value  Value to be streamed out to the file
	///
	void stream(const TValue& value);

	TWIST_NO_COPY_NO_MOVE(OutFileStream<TValue>)

private:
	static const size_t  k_chunk_size  = 5242880;  //5MB
	static const size_t  k_buffer_len  = k_chunk_size / sizeof(TValue);

	/// Write the contents of the buffer (up to the current writing position) to the file and move back to its beginning.
	/// 
	void flush();
	
	std::unique_ptr<FileStd>  out_file_;
	
	// The memory buffer where values are streamed into, and which is periodically written to the file.
	std::unique_ptr<TValue[]>  buffer_; 
	
	// The current writing position into the buffer.
	size_t  buffer_pos_;
	
	TWIST_CHECK_INVARIANT_DECL
};

} 

#include "OutFileStream.ipp"

#endif 

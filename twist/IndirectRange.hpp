///  @file  IndirectRange.hpp
///  IndirectRange class template, inherits rg::view_facade<>

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_INDIRECT_RANGE_HPP
#define TWIST_INDIRECT_RANGE_HPP

#include <range/v3/core.hpp>

namespace twist {

///
///  A range over a container of pointer-like elements, which applies an extra dereference when dereference 
///  the container iterators. Based on an early VS port of Eric Niebler's "ranges" TS, which will probably
///  form part of C++20.
///
template<class TCont>
class IndirectRange : public rg::view_facade<IndirectRange<TCont>> {
public:
    IndirectRange() = default;

    explicit IndirectRange(const TCont& v)
		: first_(std::cbegin(v))
		, last_(std::cend(v))
    {
    }    

private:
	using Iter = typename TCont::const_iterator;
  
	struct Cursor {

		decltype(auto) get() const 
		{
			return **it_;
		}
    
		void next() 
		{
			++it_;
		}
    
		bool done() const 
		{
			return it_ == last_;
		}

		bool equal(const Cursor& that) const 
		{
			return it_ == that.it_;
		}

		Iter  it_;
		Iter  last_;
	};
    
	Cursor begin_cursor() 
	{
        return {first_, last_};
    }

	Iter  first_;
	Iter  last_;

    friend rg::range_access;
};

template<class TCont> IndirectRange<TCont> make_indirect_range(const TCont& cont)
{
	return IndirectRange<TCont>(cont);
}

}

#endif

// https://stackoverflow.com/questions/tagged/range-v3
// https://stackoverflow.com/questions/31420532/range-v3-use-view-facade-to-provide-both-const-and-non-const-iterators
// https://stackoverflow.com/questions/43355048/how-to-create-a-view-facade-using-range-v3


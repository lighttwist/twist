///  @file  ProgressProv.cpp
///  Implementation file for "ProgressProv.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "ProgressProv.hpp"

#include <iomanip>
#include <sstream>

namespace twist {

ProgressProv::~ProgressProv()
{
	TWIST_CHECK_INVARIANT
}


void ProgressProv::set_timer_output(Chronometer::Unit unit, unsigned int precision)
{
	TWIST_CHECK_INVARIANT
	timer_unit_ = unit;
	timer_precision_ = precision;
	TWIST_CHECK_INVARIANT
}


void ProgressProv::start_timed_hist_line(const std::wstring& text)
{
	TWIST_CHECK_INVARIANT
	timer_text_ = text;
	timer_.reset(new Chronometer(timer_unit_));
	timer_->start();

	add_new_hist_line(timer_text_ + L"...");
	TWIST_CHECK_INVARIANT
}


void ProgressProv::stop_timed_hist_line()
{
	TWIST_CHECK_INVARIANT
	if (!timer_) {
		TWIST_THROW(L"No timed history line is currently active.");
	}
	
	std::wstringstream text;
	text << std::setprecision(timer_precision_);
	text << timer_text_ << L"  (" << timer_->stop() << timer_->get_unit_symbol() << L")"; 

	set_present_hist_line(text.str());

	timer_.reset();
	timer_text_.clear();
	TWIST_CHECK_INVARIANT
}


#ifdef _DEBUG
void ProgressProv::check_invariant() const noexcept
{
}
#endif

} // namespace twist

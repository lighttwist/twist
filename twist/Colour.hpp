/// @file "Colour.hpp"
/// Colour class

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_COLOUR_HPP
#define TWIST_COLOUR_HPP

namespace twist {

/*! A 24-bit colour, encoding the red, green and blue component intensity as integers in the interval [0, 255].
    Conversion to and from a 32-bit integer, named "colour ref", encoding all three components is provided.
 */
class Colour {
public:
	//! The "colour ref" type
	using ColourRef = std::uint32_t;

	/*! Constructor. Creates the colour from the red, green and blue components.
	    \param[in] r  The red component
	    \param[in] g  The green component
	    \param[in] b  The blue component
	 */
	explicit constexpr Colour(std::uint8_t r, std::uint8_t g, std::uint8_t b);
	
	/*! Constructor. Creates the colour from a "colour ref" value.
	    \param[in] colour_ref  The value.
     */
	explicit constexpr Colour(ColourRef colour_ref);

	constexpr auto operator<=>(const Colour&) const = default;

	//! Get the red, green and blue components of the colour.
	[[nodiscard]] constexpr auto rgb() const -> std::tuple<std::uint8_t, std::uint8_t, std::uint8_t>;

	//! Get red component of the colour.
	[[nodiscard]] constexpr auto red() const -> std::uint8_t;

	//! Get green component of the colour.
	[[nodiscard]] constexpr auto green() const -> std::uint8_t;

	//! Get blue component of the colour.
	[[nodiscard]] constexpr auto blue() const -> std::uint8_t;

	//! Get the colour as a "colour ref" value.
	[[nodiscard]] constexpr auto colour_ref() const -> ColourRef;

	//! Create a "colour ref" value from the red \p r, green \p g and blue \p b components.
	[[nodiscard]] static constexpr auto colour_ref_from_rgb(std::uint8_t r, std::uint8_t g, std::uint8_t b) 
	                                     -> ColourRef;

private:
	[[nodiscard]] static constexpr auto lobyte(auto x) -> std::uint8_t;

	ColourRef colour_ref_;
};

//! Invalid "colour ref" value
constexpr auto no_colour_ref = static_cast<Colour::ColourRef>(-1);

} 

#include "twist/Colour.ipp"

#endif 

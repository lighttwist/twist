/// @file Font.cpp
/// Implementation file for "Font.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/Font.hpp"

#include "twist/string_utils.hpp"

namespace twist {

Font::Font(std::wstring family, int point_size, FontWeight weight, FontStyle style) 
	: family_{move(family)}
	, point_size_{point_size}
	, weight_{weight}
	, style_{style}
	, underlined_{false}
{
	TWIST_CHECK_INVARIANT
}

auto Font::family() const -> std::wstring
{
	TWIST_CHECK_INVARIANT
	return family_;
}

auto Font::point_size() const -> int
{
	TWIST_CHECK_INVARIANT
	return point_size_;
}

auto Font::weight() const -> FontWeight
{
	TWIST_CHECK_INVARIANT
	return weight_;
}	
	
auto Font::style() const -> FontStyle
{
	TWIST_CHECK_INVARIANT
	return style_;
}

auto Font::underlined() const -> bool
{
	TWIST_CHECK_INVARIANT
	return underlined_;
}

auto Font::set_underlined(bool underlined) -> void
{
	TWIST_CHECK_INVARIANT
	underlined_ = underlined;
}

#ifdef _DEBUG
auto Font::check_invariant() const noexcept -> void
{
	assert(!is_whitespace(family_));
	assert(point_size_ != 0);
	assert(FontWeight::normal <= weight_ && weight_ <= FontWeight::bold);
	assert(FontStyle::normal <= style_ && style_ <= FontStyle::italic);
}
#endif

}  

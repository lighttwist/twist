/// @file meta_misc_traits.hpp
/// Miscelaneous type traits

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


#ifndef TWIST_META__MISC__TRAITS_HPP
#define TWIST_META__MISC__TRAITS_HPP

#include "twist/meta_misc_traits.ipp"

namespace twist {

/// Local alias for std::enable_if_t.
template<bool Test, class T = void>
using EnableIf = std::enable_if_t<Test, T>;

/// Local alias for std::is_base_of_v.
template<class Base, class Derived>
constexpr bool is_base_of = std::is_base_of_v<Base, Derived>;

/// If Fn is a callable type which can be called with arguments of type Args..., this alias resolves to a 
/// the type int. Otherwise the expression is ill-formed. 
template<class Fn, class... Args> 
using EnableIfFunc = std::enable_if_t<std::is_invocable_v<Fn, Args...>, int>;

/// If Fn is a callable type which can be called with arguments of type Args... to yield a result that is 
/// convertible to Ret, this alias resolves to the type int. Otherwise the expression is ill-formed. 
template<class Ret, class Fn, class... Args> 
using EnableIfFuncR = std::enable_if_t<std::is_invocable_r_v<Ret, Fn, Args...>, int>;

/// If Fn is a callable type which can be called with arguments of type Args... to yield a result that is 
/// convertible to bool, this alias resolves to the type int. Otherwise the expression is ill-formed. 
template<class Fn, class... Args> 
using EnableIfPred = std::enable_if_t<std::is_invocable_r_v<bool, Fn, Args...>, int>;

/// If T and U name the same type (taking into account const/volatile qualifications), this alias resolves to 
/// the type int. Otherwise the expression is ill-formed. 
template<class T, class U> 
using EnableIfSame = std::enable_if_t<std::is_same_v<T, U>, int>;

/// If std::declval<From> can be converted to To using implicit conversions (or both From and To are void),
/// this alias resolves to the type int. Otherwise the expression is ill-formed. 
template<class From, class To>
using EnableIfConvertible = std::enable_if_t<std::is_convertible_v<From, To>, int>;

/// If T is a floating-point number type, this alias resolves to a the type int. Otherwise the 
/// expression is ill-formed. 
template<class T>
using EnableIfFloatingPoint = std::enable_if_t<std::is_floating_point_v<T>, int>;

/// If T is an integer number type (this does not include bool) this alias resolves to a the type int. 
/// Otherwise the expression is ill-formed. 
template<class T>
using EnableIfInteger = std::enable_if_t<std::is_integral_v<T> && !std::is_same_v<T, bool>, int>;

/// If T is a numeric type, that is, an integer or a floating-point number type, but not a bool, this type 
/// resolves std::true_type (or derived); otherwise to std::false_type. 
template<class T>
struct IsNumber : detail::PredBase<std::is_arithmetic_v<T> && !std::is_same_v<T, bool>> {};

/// Whether T is a numeric type, that is, an integer or a floating-point number type, but not a bool. 
template<class T>
constexpr bool is_number = IsNumber<T>::value;

/// If T is a numeric type, that is, an integer or a floating-point number type, but not a bool, this alias 
/// resolves to the type int. Otherwise the expression is ill-formed. 
template<class T>
using EnableIfNumber = std::enable_if_t<IsNumber<T>::value, int>;

/// If T is an enumeration type this alias resolves to the type int. Otherwise the expression is ill-formed. 
template<class T>
using EnableIfEnum = std::enable_if_t<std::is_enum_v<T>>;

/*! Given a source and a destination type, this variable is true if the types are numerical and the source type can be 
    safely cast to the destination type (the conversion is not narrowing); it is false otherwise.
    \tparam From  The source type
    \tparam To  The destination type
 */
template<class From, class To>
constexpr bool is_safe_numeric_cast = twist::detail::IsSafeNumericCastImpl<From, To>::value;

/// Find out if a value of a specific type can be created from a value of another specific type via either the 
/// former being constructed from the latter, or the latter being implicitly converted to the former.
///
/// @tparam  To  The type of the output value in the transformation
/// @tparam  From  The type of the input value in the transformation
/// @return  true if a value of type To can be created from a value of type From
///
template<class To, class From>
constexpr bool is_creatable_from();

/// If a value of type std::declval<To> can be created from std::declval<To> via either the construction or 
/// implicit conversion (or if both From and To are void), this alias resolves to a valid type (void). 
/// Otherwise the expression is ill-formed. 
template<class To, class From>
using EnableIfCreatable = std::enable_if_t<is_creatable_from<To, From>()>;

/*! If \p Derived is derived from \p Base or if both are the same non-union class, this alias resolves to a type \p T. 
    Otherwise the expression is ill-formed. 
 */
template<class Base, class Derived, class T = void>
using EnableIfBaseOf = std::enable_if_t<std::is_base_of_v<Base, Derived>, T>;

/*! If T is a pointer to object or a pointer to function (but not a pointer to member/member function), this alias 
    resolves to a valid type (int). Otherwise the expression is ill-formed.
 */
template<class T>
using EnableIfPointer = std::enable_if_t<std::is_pointer_v<T>, int>;

//! Trait resolving to true if T is of the form const U* and false otherwise.
template<class T>
using IsConstPointer = detail::IsConstPointerImpl<T>;

/// If operator == can be applied to two values of type T&& and U&&, and also to two values of type U&& and 
/// T&&, and in each case yield a value convertible to bool, then this alias resolves to the type int. 
/// Otherwise the expression is ill-formed. 
template<class T, class U>
using EnableIfEqComparable = std::enable_if_t<detail::AreEqComparableImpl<T, U>::value, int>;

/// If operator != can be applied to two values of type T&& and U&&, and also to two values of type U&& and 
/// T&&, and in each case yield a value convertible to bool, then this alias resolves to the type int. 
/// Otherwise the expression is ill-formed. 
template<class T, class U>
using EnableIfIneqComparable = std::enable_if_t<detail::AreIneqComparableImpl<T, U>::value, int>;

/// If the pre-increment operator can be applied to a lvalue reference of type T, this alias resolves to a 
/// std::true_type (or derived); otherwise it resolves to std::false_type. 
template<class T>
using HasPreincrement = detail::HasPreincrementImpl<T>;

/// If the dereference/indirection operator can be applied to a lvalue reference of type T, this alias 
/// resolves to a std::true_type (or derived); otherwise it resolves to std::false_type. 
template<class T>
using HasDereference = detail::HasDereferenceImpl<T>;

//  template<class Iter>
//  concept InputIterator {
//      { ++ Iter& }; 
//      { Iter& ++ };
//      { * Iter&& };
//      { * Iter& ++ };
//      { const Iter& == const Iter& } -> bool;
//      { const Iter& != const Iter& } -> bool;
//  };

/// If Iter satisfies the InputIterator concept requirements, this alias resolves to std::true_type (or 
/// derived); otherwise it resolves to std::false_type. 
template<class Iter>
using IsAnyInputIterator = detail::IsAnyInputIteratorImpl<Iter>;

/// If Iter satisfies the InputIterator concept requirements, this alias resolves to the type int. Otherwise 
/// the expression is ill-formed.
template<class Iter>
using EnableIfAnyInputIterator = std::enable_if_t<IsAnyInputIterator<Iter>::value, int>;

/// If Iter satisfies the InputIterator concept requirements, and the result of dereferencing it is 
/// covertible (without narrowing) to Val, this alias resolves to the type int. Otherwise the expression is 
/// ill-formed.
template<class Iter, class Val>
using EnableIfInputIterator = std::enable_if_t<twist::detail::IsInputIteratorImpl<Iter, Val>::value, int>;

//  template<class Iter, class Val>
//  concept OutputIterator {
//      { ++ Iter& }; 
//      { Iter& ++ };
//      { * Iter&& = Val&& };
//      { * Iter& ++ = Val&& };
//  };

/// If Iter satisfies the requirements of an OutputIterator taking a value of type Val, this alias resolves to 
/// the type int. Otherwise the expression is ill-formed.
template<class Iter, class Val>
using EnableIfOutIterator = std::enable_if_t<detail::IsOutIteratorImpl<Iter, Val>::value, int>;

/// If the expression T < T is valid and yields a value convertible to bool, then this alias resolves to 
/// the type int. Otherwise the expression is ill-formed.
template<class T>
using EnableIfHasLessThan = std::enable_if_t<detail::HaveLessThanImpl<T, T>::value>;

/// If both expressions T < U and U < T are valid and yield values convertible to bool, then this alias 
/// resolves to the type int. Otherwise the expression is ill-formed.
template<class T, class U>
using EnableIfHaveLessThan = std::enable_if_t<detail::HaveLessThanImpl<T, U>::value>;

//! Create a lvalue or rvalue reference type that refers to \p T.
template<class T>
using AddRef = std::add_lvalue_reference_t<T>;

//! Create a lvalue or rvalue reference type that refers to const-qualified \p T.
template<class T>
using AddCref = std::add_lvalue_reference_t<std::add_const_t<T>>;

//! Create a raw pointer type that refers to \p T.
template<class T>
using AddPtr = std::add_pointer_t<T>;

//! Create raw pointer type that refers to const-qualified \p T.
template<class T>
using AddCptr = std::add_pointer_t<std::add_const_t<T>>;

//! If T is of the form gsl::not_null<U*>, this resolves to U; otherwise it is ill-formed
template<class T> 
using NotNullPointee = typename detail::NotNullPointeeImpl<T>::type;

/*! Resolves to true if class \p T is a specialisation of class template \p Template.
    \note  This works only for template classes whose template parameters are all typenames
 */
template<template<class...> class Template, class T>
constexpr bool IsSpecializationOf = detail::IsSpecializationOfImpl<Template, T>::value;

} 

#endif 

/// @file functor.hpp
/// Generic function objects

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_FUNCTOR_HPP
#define TWIST_FUNCTOR_HPP

#include "twist/meta_misc_traits.hpp"

#include <gsl/gsl>

namespace twist {

//! Predicate: compares its argument to the value passed into its constructor.
template<class Val>
class EqualToValue {
public:
	explicit EqualToValue(Val val)
		: val_{std::move(val)}
	{
	}

	template<class T,
	         class = EnableIfEqComparable<T, Val>>
	auto operator()(const T& other) const -> bool 
	{
		return other == val_;
	}

private:
	Val val_;
};

/*! Binary predicate: whether the first value is smaller than the second (values are passed in by pointer, 
    which cannot be nullptr).
    \tparam T  The value type
 */
struct PointeeLessThan {
	template<typename T>
	auto operator()(const T* val1, const T* val2) const -> bool
	{
		using gsl::not_null;
		return *not_null{val1} < *not_null{val2};
	}
};

//! Equivalent to static_cast<Dest>(x) where x can be anything.
template<class Dest>
struct StaticCast {
	template<class Src>
	auto operator()(Src src) const -> Dest
	{
		return static_cast<Dest>(src);
	}
};

//! Equivalent to std::to_wstring(value).
struct ToString {
	template<class T>
	auto operator()(T value) const -> std::wstring
	{
		return std::to_wstring(value);
	}
};

//! Equivalent to std::to_string(value).
struct ToAnsiString {
	template<class T>
	auto operator()(T value) const -> std::string
	{
		return std::to_string(value);
	}
};

}

#endif

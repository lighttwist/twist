///  @file metaprogramming.ipp
///  Inline implementation file for "metaprogramming.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

namespace twist {

namespace detail {

template<class Tuple> using MakeIndexes = std::make_index_sequence<std::tuple_size_v<std::decay_t<Tuple>>>;

template<size_t Index, class Tuple> using ElementType = std::tuple_element_t<Index, std::decay_t<Tuple>>;


template<class Tuple, size_t... Indexes, class Func>
decltype(auto) tuple_apply_impl(Tuple&& tup, std::index_sequence<Indexes...>, Func&& func)
{
	return std::invoke(
			std::forward<Func>(func), 
			std::get<Indexes>(std::forward<Tuple>(tup))...);
}


template<class Tuple, size_t... Indexes, class Func>
decltype(auto) tuple_apply_to_each_with_idx_impl(Tuple&& tup, std::index_sequence<Indexes...>, 
		Func&& func)
{
	return std::invoke(
			std::forward<Func>(func), 
			IndexedTupleElement<Indexes, ElementType<Indexes, Tuple>>{
					std::get<Indexes>(tup)}...);
}


template <class Tuple, class Func, size_t... Indexes>
auto tuple_transform_each_impl(Tuple&& tup, std::index_sequence<Indexes...>, Func&& func) 
{
	return std::make_tuple(
			std::invoke(
					std::forward<Func>(func), 
					std::get<Indexes>(std::forward<Tuple>(tup)))...);
}


template<class Tuple, class Ty, size_t index> 
constexpr size_t tuple_element_index_impl()
{
	if constexpr (index == std::tuple_size_v<Tuple>) { 
		return tuple_npos;
	}
	else if constexpr (std::is_same_v<std::tuple_element_t<index, Tuple>, Ty>) { 
		return index; 
	} 
	else { 
		return tuple_element_index_impl<Tuple, Ty, index + 1>(); 
	}
}

}
 
 
template<class Tuple, class Func>
decltype(auto) tuple_apply(Tuple&& tup, Func&& func)
{
	using namespace detail;

    return tuple_apply_impl(
			std::forward<Tuple>(tup), MakeIndexes<Tuple>{}, std::forward<Func>(func));
}


template<class Tuple, class Func>
void tuple_apply_to_each(Tuple&& tup, Func&& func)
{
    return tuple_apply(
			std::forward<Tuple>(tup),
			[&func](auto&&... elems) { 
				(std::forward<Func>(func)(std::forward<decltype(elems)>(elems)), ...);
			});
}

 
template<class Tuple, class Func>
void tuple_apply_to_each_with_idx(Tuple&& tup, Func&& func)
{
	using namespace detail;

    return tuple_apply_to_each_with_idx_impl(
			std::forward<Tuple>(tup), 
			MakeIndexes<Tuple>{},
			[&func](auto&&... elems) { 
				(std::forward<Func>(func)(std::forward<decltype(elems)>(elems)), ...);
			});
}


template<class Tuple, class Func>
auto tuple_transform_each(Tuple&& tup, Func&& func) 
{
	using namespace detail;

    return tuple_transform_each_impl(
			std::forward<Tuple>(tup), MakeIndexes<Tuple>{}, std::forward<Func>(func));
}


template<class Tuple, class Ty> 
constexpr size_t tuple_element_index()
{
	return detail::tuple_element_index_impl<Tuple, Ty, 0>();
}


template<class T, std::size_t size>
constexpr int get_pos(T (&arr)[size], T elem)
{
	for (int i = 0; i < static_cast<int>(size); ++i) {
		if (arr[i] == elem) {
			return i;
		}
	}
	return -1;
}

}

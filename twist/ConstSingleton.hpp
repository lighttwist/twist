///  @file  ConstSingleton.hpp
///  ConstSingleton class template

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_COST_SINGLETON_HPP
#define TWIST_COST_SINGLETON_HPP

namespace twist {

/// 
///  Template for the base class of classes which implement a variation on the "Singleton" pattern where public access is only 
///    provided for the constant singleton object (protected access to the non-const object is possible).
///  The class is not safe to use between different DLLs (as each client DLL gets a different copy of the static data member).
///
template<class TClass>
class ConstSingleton : public NonCopyable {
public:
	/// Get the const singleton object. An exception is thrown if it has not been created.
	///
    /// @return  The object
	///
	static const TClass& get() 
	{
		if (!singleton__) {
			TWIST_THROW(L"The singleton object has not been set.");
		}
		return *singleton__;
	}

	/// Check whether the singleton object currently exists.
	///
    /// @return  true if it does
	///
	static bool exists() 
	{
		return singleton__;
	}

protected:
	/// Constructor.
	/// An exception is thrown if the singleton object has already been created.
	///
	ConstSingleton() 
	{
		if (singleton__) {
			TWIST_THROW(L"The singleton object has already been created.");
		}
	}

	/// Destructor.
	///
	virtual ~ConstSingleton() = 0
	{
	}

	/// Get the non-const singleton object. An exception is thrown if it has not been set.
	///
    /// @return  The object
	///
	static TClass& get_singleton() 
	{
		if (!singleton__) {
			TWIST_THROW(L"The singleton object has not been set.");
		}
		return *singleton__;
	}

	/// Set the singleton object.
	/// An exception is thrown if the singleton object has already been set.
	///
	/// @param[in] singleton  The singleton object
	///
	static void set_singleton(std::unique_ptr<TClass> singleton) 
	{
		if (singleton__) {
			TWIST_THROW(L"The singleton object has already been set.");
		}
		singleton__ = move(singleton);
	}

	/// Reset the singleton object.
	/// An exception is thrown if the singleton object has not been set.
	///
	static void reset_singleton() 
	{
		if (!singleton__) {
			TWIST_THROW(L"The singleton object has not been set.");
		}
		singleton__.reset();
	}

private:
	static std::unique_ptr<TClass>  singleton__;
};

template<class TClass>
std::unique_ptr<TClass> ConstSingleton<TClass>::singleton__;

} 

#endif 


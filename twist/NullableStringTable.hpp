///  @file  NullableStringTable.hpp
///  NullableStringTable class template

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_NULLABLE_STRING_TABLE_HPP
#define TWIST_NULLABLE_STRING_TABLE_HPP

namespace twist {

/// A table whose read-only cell values can be either null-terminated strings of characters or nullptr.
///
/// @tparam  Chr  The character type
///
template<typename Chr>
class NullableStringTable {
public:
	/// Constructor.
	///	
	/// @param[in] num_cols  The number of columns in the table
	///
	NullableStringTable(Ssize num_cols);

	/// Get the number of rows in the table.
	///
	/// @return  The number of columns
	///
	Ssize nof_rows() const;

	/// Get the number of columns in the table.
	///
	/// @return  The number of columns
	///
	Ssize nof_columns() const;  

	/// Get the data in a specific table cell.
	///
	/// @param[in] row  The row index; an exception is thrown if it is invalid
	/// @param[in] col  The column index; an exception is thrown if it is invalid
	/// @return  The cell data; can be nullptr (which is different from an empty string); do not hold on to 
	///				the pointer as it may be invalidated next time the table structure is modified
	///
	const Chr* get_cell(Ssize row, Ssize col) const;

	/// Set the data in a specific table cell.
	///
	/// @param[in] row  The row index; an exception is thrown if it is invalid
	/// @param[in] col  The column index; an exception is thrown if it is invalid
	/// @param[in] data  The cell data; can be nullptr (which is different from an empty string); the class 
	///				makes a copy of the string
	///
	void set_cell(Ssize row, Ssize col, const Chr* data);

	/// Get the data in the cells in a specific table row.
	///
	/// @param[in] row  The row index; an exception is thrown if it is invalid
	/// @return  Vector containing the cells' data; cell data can be nullptr (which is different from an empty 
	///				string); do not hold on to the pointers as they may be invalidated next time the table 
	///		        table structure is modified
	///
	std::vector<const Chr*> get_row_cells(Ssize row) const;  

	/// Add a row to the table. It will be populated with null cells.
	///
	/// @return  The new row's index
	///
	Ssize add_row();

	TWIST_NO_COPY_NO_MOVE(NullableStringTable<Chr>)

private:	
	/// 
	///  A table cell
	///
	struct Cell {
		Cell() 
			: string()
			, is_null(true)
		{
		}

		std::basic_string<Chr>  string;
		bool  is_null;
	};

	typedef std::vector<Cell>  Row;

	/// Get the data in a specific table cell.
	///
	/// @param[in] cell  The cell
	/// @return  The cell data; can be nullptr (which is different from an empty string)
	///
	const Chr* get_cell(const Cell& cell) const;

	const Ssize  num_cols_;
	std::vector<Row>  rows_;

	TWIST_CHECK_INVARIANT_DECL
};

} 

#include "NullableStringTable.ipp"

#endif 

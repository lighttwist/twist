///  @file  PopupMenu.cpp
///  Implementation file for "PopupMenu.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "PopupMenu.hpp"

#pragma warning(disable: 4244) // Disable Qt warning

#include <QToolButton>

#include "action_utils.hpp"
#include "qt_globals.hpp"

namespace twist::qt {

//
//  PopupMenu class
//

PopupMenu::PopupMenu(std::initializer_list<ItemInfo> items_info)
	: menu_{ std::make_unique<QMenu>() }
{
	TWIST_CHECK_INVARIANT
	using std::get;

	for_each(items_info, [&](const auto& ii) {
		
		const auto item_name{ to_qstring(ii.name()) };
		auto item_handler = ii.handler();
		auto action{ menu_->addAction(item_name, [=]{ item_handler(); }) };

		action->setObjectName(item_name);
		action->setParent(menu_.get()); 
		action->setEnabled(ii.enabled());
	});
}


bool PopupMenu::is_empty() const
{
	TWIST_CHECK_INVARIANT
	return !menu_;
}


void PopupMenu::show(QPoint global_pos)
{
	TWIST_CHECK_INVARIANT
    menu_->exec(global_pos);
}


void PopupMenu::set_menu_for(gsl::not_null<QToolButton*> button) const
{
	TWIST_CHECK_INVARIANT
	if (!menu_) {
		TWIST_THROW(L"The menu is empty.");
	}

	return button->setMenu(menu_.get());
}


void PopupMenu::enable_item(std::wstring_view name, bool enabled)
{
	TWIST_CHECK_INVARIANT
	if (!menu_) {
		TWIST_THROW(L"The menu is empty.");
	}
	auto action = get_action_with_name(menu_->actions(), name);
	action->setEnabled(enabled);
}


#ifdef _DEBUG
void PopupMenu::check_invariant() const noexcept
{
}
#endif

//
//  PopupMenu::ItemInfo class
//

PopupMenu::ItemInfo::ItemInfo(std::wstring name, ItemHandler handler, bool enabled)
	: name_{ name }
	, handler_{ handler }
	, enabled_{ enabled }
{
	TWIST_CHECK_INVARIANT
}


std::wstring PopupMenu::ItemInfo::name() const
{
	TWIST_CHECK_INVARIANT
	return name_;
}


PopupMenu::ItemHandler PopupMenu::ItemInfo::handler() const
{
	TWIST_CHECK_INVARIANT
	return handler_;
}


bool PopupMenu::ItemInfo::enabled() const
{
	TWIST_CHECK_INVARIANT
	return enabled_;
}


#ifdef _DEBUG
void PopupMenu::ItemInfo::check_invariant() const noexcept
{
	assert(!is_whitespace(name_));
	assert(handler_);
}
#endif


}

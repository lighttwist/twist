///  @file  button_utils.hpp
///  Utilities for working with Qt button widgets

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_QT_BUTTON__UTILS_HPP
#define TWIST_QT_BUTTON__UTILS_HPP

#include <QObject>

#include "PopupMenu.hpp"

class QToolButton;

namespace twist::qt {

///  Helper class for attaching a popup menu (a list of menu items and corresponding actions) to a 
///  "tool button". 
class ToolButtonPopupMenu : public QObject {
public:
	/// Attach a popup menu (a list of menu items and corresponding actions) to a "tool button".
	/// The button will then display a special arrow to indicate that a menu is present. The menu is displayed 
	/// when the arrow part of the button is pressed.
	///
	/// @param[in] button  The button
	/// @param[in] menu  The popup menu
	///
	static void attach(gsl::not_null<QToolButton*> button, PopupMenu&& menu);

private:
	ToolButtonPopupMenu(gsl::not_null<QToolButton*> button, QObject* parent, PopupMenu&& menu);

private:
	PopupMenu  menu_;
	Q_OBJECT
};

//
//  Free functions
//

/// Given a "tool button" which has an associated popup menu, enable or disable one of the menu items based 
/// on its text.
///
/// @param[in] button  The button; an exception is thrown if there is no associated menu
/// @param[in] item_text  The item text; matching is case-sensitive; an exception is thrown if no match 
///					is found
/// @param[in] enabled  Whether the item should be enabled (true) or disabled
///
void enable_menu_item(gsl::not_null<const QToolButton*> button, std::wstring_view item_text, bool enabled);

}

#endif

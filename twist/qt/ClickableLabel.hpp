/// @file ClickableLabel.hpp
/// ClickableLabel class, inherits QLabel

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_QT_CLICKABLE_LABEL_HPP
#define TWIST_QT_CLICKABLE_LABEL_HPP

#pragma warning(push)
#pragma warning(disable: 5054)  // Get rid of warnings inside QtGui/qpixelformat.h 

#include <QLabel>

namespace twist::qt {

/// An extension of the label widget (a widget which provides a text or image display) which emits a "clicked" 
/// signal.
class ClickableLabel : public QLabel {
public:
	/// Constructor.
	///
	/// @param[in] parent  The parent widget
	/// @param[in] f  Window creation flags
	///
    explicit ClickableLabel(QWidget* parent = nullptr, Qt::WindowFlags f = {});

	/// Constructor.
	///
	/// @param[in] text  The text to be displayed in the label  
	/// @param[in] parent  The parent widget
	/// @param[in] f  Window creation flags
	///
    explicit ClickableLabel(const QString& text, QWidget* parent = nullptr, Qt::WindowFlags f = {});
 
signals:
    void clicked();
 
protected:
    void mouseReleaseEvent(QMouseEvent* event) override;  // QLabel override

	Q_OBJECT
};

}

#pragma warning(pop)

#endif 

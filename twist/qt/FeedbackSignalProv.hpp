/// @file FeedbackSignalProv.hpp
/// FeedbackSignalProv class, inherits from MessageExceptionFeedbackProv, QObject 

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_QT_FEEDBACK_SIGNAL_PROV_HPP
#define TWIST_QT_FEEDBACK_SIGNAL_PROV_HPP

#include <QMetaType>
#include <QObject>

#include "twist/feedback_providers.hpp"

#include "Meta.hpp"

namespace twist::qt {

///
///  A message feedback provider which emits a Qt signal for each call to a public method:
///    - set_prog_msg() emits "send_prog_msg"
///    - set_prog_msg_indent() emits "send_set_prog_msg_indent"
///    - set_fatal_error_msg() emits "send_fatal_error_msg"
///    - report_uncaught_exception() emits "send_uncaught_exception"
///
///  A user of this class should connect slots to (some of) these signals.  
///
class FeedbackSignalProv : public QObject, public MessageExceptionFeedbackProv {
public:
	explicit FeedbackSignalProv();

signals:
	void send_prog_msg(QString msg, bool try_replace_prev);

	void send_set_prog_msg_indent(int indent_steps);  

	void send_fatal_error_msg(QString msg);  

	void send_uncaught_exception(std::exception_ptr exptr);  

private:
	void do_set_prog_msg(std::wstring_view msg, bool try_replace_prev) override; // MessageFeedbackProv override

	void do_set_fatal_error_msg(std::wstring_view msg) override; // MessageFeedbackProv override

	void do_report_uncaught_exception(std::exception_ptr exptr) override; // ExceptionFeedbackProv override

	int prog_msg_indent_steps_{0};
	static inline bool metatypes_registered__{ false };

	Q_OBJECT
};

// Declare std::exception_ptr to as a meta-type for Qt's meta-object system
Q_DECLARE_METATYPE(std::exception_ptr);

}

#endif

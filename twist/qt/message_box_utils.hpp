/// @file message_box_utils.hpp
/// Utilities for working with message boxes in Qt

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_QT_MESSAGE__BOX__UTILS_HPP
#define TWIST_QT_MESSAGE__BOX__UTILS_HPP

#pragma warning(push)	
#pragma warning(disable: 5054)  // Get rid of warnings inside QtGui/qpixelformat.h 

#include <QMessageBox>

namespace twist::qt {

/// Information about a custom (non-standard) message box button.
struct CustomMessageBoxButton {
	const std::wstring text;  ///< The button text
	const QMessageBox::ButtonRole role;  ///< The button role
};

/// Display an "information message box" with the default standard buttons.
///
/// @param[in] text  The text to be displayed in the message box
/// @param[in] title  The message box title; if empty, a default title will be used
/// @param[in] parent  The handle to the parent widget for the message box; if nullptr, the message box has 
///					no owner widget 
/// @return  The standard button that was clicked; if Esc was pressed instead, the escape button is returned
///
QMessageBox::StandardButton show_information_message_box(std::wstring_view text, std::wstring_view title, 
		QWidget* parent = nullptr);

/// Display a "critical message box" with the default standard buttons.
///
/// @param[in] text  The text to be displayed in the message box
/// @param[in] title  The message box title; if empty, a default title will be used
/// @param[in] parent  The handle to the parent widget for the message box; if nullptr, the message box has 
///					no owner widget 
/// @return  The standard button that was clicked; if Esc was pressed instead, the escape button is returned
///
QMessageBox::StandardButton show_critical_message_box(std::wstring_view text, std::wstring_view title, 
		QWidget* parent = nullptr);

/// Display a "question message box" with the "Yes" and "No" standard buttons.
///
/// @param[in] text  The text to be displayed in the message box
/// @param[in] title  The message box title; if empty, a default title will be used
/// @param[in] parent  The handle to the parent widget for the message box; if nullptr, the message box has 
///					no owner widget 
/// @return  The standard button that was clicked; if Esc was pressed instead, the escape button is returned
///
QMessageBox::StandardButton show_yesno_message_box(std::wstring_view text, std::wstring_view title, 
		QWidget* parent = nullptr);

/// Display a "question message box" with a specific set of standard buttons.
///
/// @param[in] text  The text to be displayed in the message box
/// @param[in] title  The message box title; if empty, a default title will be used
/// @param[in] buttons  The set of standard buttons to be displayed in the box  
/// @param[in] parent  The handle to the parent widget for the message box; if nullptr, the message box has 
///					no owner widget 
/// @return  The standard button that was clicked; if Esc was pressed instead, the escape button is returned
///
QMessageBox::StandardButton show_question_message_box(std::wstring_view text, std::wstring_view title, 
		const std::vector<QMessageBox::StandardButton>& buttons, QWidget* parent = nullptr);

/// Show a message box with no icon and custom set of buttons. The "close window" corner button and Esc key 
/// are only enabled if a compatible button is displayed (eg "No" or "Reject).
///
/// @param[in] text  The text to be displayed in the message box
/// @param[in] title  The message box title
/// @param[in] buttons  The set of custom buttons to be displayed in the box  
/// @param[in] parent  The handle to the parent widget for the message box; if nullptr, the message box has 
///					no owner widget 
/// @return  The role associated to the button that was clicked; Esc was pressed instead, the corresponding 
///					button is returned 
///
QMessageBox::ButtonRole show_custom_message_box(std::wstring_view text, std::wstring_view title, 
		const std::vector<CustomMessageBoxButton>& buttons, QWidget* parent = nullptr);

/// Display, in an "error message box", the error info contained in an exception object.
///
/// @param[in] ex  The exception; additional information can be displayed if the object type is  
///					twist::RuntimeError 
/// @param[in] title  The message box title; if empty, a default title will be used
/// @param[in] parent  The handle to the parent widget for the message box; if nullptr, the message box has 
///					no owner widget 
/// @param[in] print_func_name  Whether the function name, if available, should be printed on the message box
///
void display_except(const std::exception& ex, std::wstring_view title = {}, QWidget* parent = nullptr, 
		bool print_func_name = true);

}

#pragma warning(pop)	

#endif

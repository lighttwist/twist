/// @file widget_utils.hpp
/// Utilities for working with the QtWidget class

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_QT_WIDGET__UTILS_HPP
#define TWIST_QT_WIDGET__UTILS_HPP

class QWidget;

namespace twist::qt {

/*! Enable or disable a widget.
    \param[in] widget  The widget
    \param[in] enabled  Whether the widget should be enable (true) or disabled
 */
auto set_enabled(gsl::not_null<QWidget*> widget, bool enabled) -> void;

/*! Enable or disable a range of widgets.
    \param[in] widgets  The range of widgets
    \param[in] enabled  Whether the widgets should be enable (true) or disabled
 */
auto set_enabled(std::initializer_list<gsl::not_null<QWidget*>> widgets, bool enabled) -> void;

/*! Show or hide a widget.
    \param[in] widget  The widget
    \param[in] visible  Whether the widget should be visible (true) or hidden
 */
auto set_visible(gsl::not_null<QWidget*> widget, bool visible) -> void;

/*! Show or hide a range of widgets.
    \param[in] widgets  The range of widgets
    \param[in] visible  Whether the widget should be visible (true) or hidden
 */
auto set_visible(std::initializer_list<gsl::not_null<QWidget*>> widgets, bool visible) -> void;

}

#endif  


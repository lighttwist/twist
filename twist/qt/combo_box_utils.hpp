/// @file combo_box_utils.hpp
/// Utilities for working with Qt combo box widgets

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_QT_COMBO__BOX__UTILS_HPP
#define TWIST_QT_COMBO__BOX__UTILS_HPP

#include "twist/meta_ranges.hpp"

#pragma warning(push)
#pragma warning(disable: 5054)  // Get rid of warnings inside QtGui/qpixelformat.h 

#include <QComboBox>

class QVariant;

namespace twist::qt {

/* Add a new item at the end of a combo box's list.
   \param[in] combo  The combo box widget; the pointer mustn't be null
   \param[in] text  The item text
 */
auto add_item(gsl::not_null<QComboBox*> combo, std::wstring_view text) -> void;

/* Add a number of items to a combo box.
   \tparam Strings  Range type holding strings
   \param[in] combo  The combo box
   \param[in] item_strings  The strings of the items to be added
 */
template<class Strings,
         class = EnableIfRangeGives<Strings, std::wstring_view>> 
auto add_items(gsl::not_null<QComboBox*> combo, const Strings& item_strings) -> void;

/* Add a new item at the end of a combo box's list, and associate a data value with the item.
   \tparam Data  The data value type 
   \param[in] combo  The combo box widget; the pointer mustn't be null
   \param[in] text  The item text
   \param[in] data  The data value
 */
template<class Data,
         class = EnableIfCreatable<QVariant, Data>> 
auto add_item_with_data(gsl::not_null<QComboBox*> combo, std::wstring_view text, Data&& data) -> void;

/* Add a number of items to a combo box, and set each item's associated data value.
   \tparam StringsAndData  Range type holding pairs of strings and data values
   \param[in] combo  The combo box
   \param[in] item_strings_and_data  A collection of pairs defining the combo box items to be added; with 
					                  the first member being the item string and the second member the item data value
 */
template<class StringsAndData,
         class = EnableIfRangeGives<StringsAndData, std::pair<std::wstring_view, QVariant>>> 
auto add_items_with_data(gsl::not_null<QComboBox*> combo, const StringsAndData& item_strings_and_data) -> void;

/* Select the first item in a combo box whose data value matches a specific value.
   \param[in] combo  The combo box widget
   \param[in] data  The data value 
   \return  The index of the first matching item, which is now selected; or -1 if there is no match
 */
auto select_first_item_with_data(gsl::not_null<QComboBox*> combo, const QVariant& data) -> int;

/* Get the data value associated with the currently selected item in a combo box as an enum value.
   If the combo box is empty or no current item is set, an exception is thrown.
   If the data value is not convertible to an int, an exception is thrown.
   \tparam Enum  The enum type
   \param[in] combo  The combo box widget
   \return  The data value as an enum value
 */
template<class Enum>
requires std::is_enum_v<Enum> && 
         is_safe_numeric_cast<int, std::underlying_type_t<Enum>>
[[nodiscard]] auto current_data_as_enum(gsl::not_null<const QComboBox*> combo) -> Enum;

}

#include "twist/qt/combo_box_utils.ipp"

#pragma warning(pop)

#endif

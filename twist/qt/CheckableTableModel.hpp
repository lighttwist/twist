/// @file CheckableTableModel.hpp
/// CheckableTableModel class, inherited from QAbstractTableModel

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_QT_CHECKABLE_TABLE_MODEL_HPP
#define TWIST_QT_CHECKABLE_TABLE_MODEL_HPP

#include <QAbstractTableModel>

namespace twist::qt {

/*! Abstract base for "table model" classes (in accordance with Qt's model/view architecture) for table widgets whose 
    items are checkable via checkboxes displayed on each row.
	\note  The table model does not, at the moment, support headers and items can only be added and removed at the end 
	       of the table.
 */
class CheckableTableModel : public QAbstractTableModel {
public:
	/*! Constructor. 
	    \param[in] parent  The model's parent object; pass in nullptr if there is no parent  
	 */
	CheckableTableModel(QObject* parent = nullptr);

	//! The number of rows in the table.
	[[nodiscard]] virtual auto nof_rows() const -> twist::Ssize = 0;

	//! The number of columns in the table (the checkbox is not considered a separate column).
	[[nodiscard]] virtual auto nof_columns() const -> twist::Ssize = 0;

	//! Indexes of the rows whose checkbox state is "checked".
	[[nodiscard]] auto checked_row_indexes() const -> const std::set<twist::Ssize>&;

	/*! Set the checkbox state of the row with index \p row. 
	    The state is set to "checked" (if \p checked is true) or "unchecked" otherwise.
	 */
	auto set_row_check(twist::Ssize row, bool checked) -> void;

protected:
	//! Call right before inserting a set of rows at the end of the table.
	auto begin_insert_row() -> void; 

	//! Call right after inserting a set of rows.
	auto after_insert_rows() -> void; 

	//! Remove from the table the rows with indexes between and including \p first_row and \p last_row.
	auto remove_rows(twist::Ssize first_row, twist::Ssize last_row) -> void;

private:
	//! Get the text in the table cell with row index \p row and column index \p col.
	[[nodiscard]] virtual auto get_cell_text(twist::Ssize row, twist::Ssize col) const -> std::wstring = 0;

	[[nodiscard]] auto flags(const QModelIndex& index) const -> Qt::ItemFlags final; // QAbstractItemModel override

    [[nodiscard]] auto rowCount(const QModelIndex& parent = QModelIndex{}) const -> int final; // QAbstractTableModel override

    [[nodiscard]] auto columnCount(const QModelIndex& parent = QModelIndex{}) const -> int final; // QAbstractTableModel override

	[[nodiscard]] auto headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const -> QVariant final; // QAbstractTableModel override

    [[nodiscard]] auto data(const QModelIndex& index, int role = Qt::DisplayRole) const -> QVariant final; // QAbstractTableModel override

	auto setData(const QModelIndex& index, const QVariant& value, int role) -> bool final; // QAbstractTableModel override

	std::set<twist::Ssize> checked_rows_;

	TWIST_CHECK_INVARIANT_DECL
	Q_OBJECT
};

// --- Free functions ---

//! Set the "checked" state of all items in \p model to \p checked and return the number of items.
auto set_all_row_checks(CheckableTableModel& model, bool checked) -> Ssize;

}

#endif 

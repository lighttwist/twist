///  @file  TreeItem.hpp
///  TreeItem class

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_QT_TREE_ITEM_HPP
#define TWIST_QT_TREE_ITEM_HPP

#include <QVariant>

// Work in progress  Dan Ababei  18Apr2018

namespace twist::qt {

/// A tree item in the data model underlying a tree widget, in the model/view architecture.
class TreeItem {
public:
	/// Pointer to the parent item; or nullptr if this item is the tree root.
    TreeItem* parent_item();

	/// Get a specific child item from this item's the list of children.
	///
	/// @param[in] row  The row index of the child item, which is the index of the item within the child 
	///					item list; an exception is thrown if out of bounds
	/// @return  The child item
	///
    TreeItem& child(int row);

	/// Get the number of child items of this item.
	///
	/// @return  The number of children
	///
    int child_count() const;

	/// Get the data object associated with a specific column in the tree widget.
	///
	/// @param[in] col  The column index; an exception is thrown if it is out of bounds
	/// @return  The data object
	///
    QVariant data(int col) const;

	/// Get the row index of this item, which is the index of the item within the parent's list of child 
	/// items, or zero if this is the root item.
	///
	/// @return  The item's row index
	///					
    int row() const;

	TWIST_NO_COPY_NO_MOVE(TreeItem)

private:
	friend class TreeModelBase;

    explicit TreeItem(std::vector<QVariant>&& data_list, TreeItem* parent_item);

    std::vector<std::unique_ptr<TreeItem>>  child_items_;
    std::vector<QVariant>  item_data_list_;
    TreeItem*  parent_item_;

	TWIST_CHECK_INVARIANT_DECL
};

}

#endif

/// @file file_dialog_utils.hpp
/// Utilities for working with Qt file dialogs

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_QT_FILE__DIALOG__UTILS_HPP
#define TWIST_QT_FILE__DIALOG__UTILS_HPP

class QLineEdit;
class QWidget;

namespace twist::qt {

/*! Display (execute synchronously) a "file open" dialog for selecting an existing file path.
    \param[in] parent  The parent widget; if not nullptr, the dialog will be shown centered over it
    \param[in] caption  The dialog caption; if empty a default caption will be used
    \param[in] filter  Filename filter, eg "JPEG Files (*.jpg);;TIFF Files (*.tif *.tiff);;All files (*.*)"
    \param[in] init_dir  The directory which will be displayed first; if empty, the current directory will be used
    \return  The selected file path, or an empty string if no file was selected
 */
[[nodiscard]] auto do_select_file_dialog(QWidget* parent = nullptr, 
                                         std::wstring_view caption = {}, 
                                         std::wstring_view filter = {},
		                                 const fs::path& init_dir = {})-> fs::path;
                       
/*! Display (execute synchronously) a "file open" dialog for selecting an existing directory path.
    \param[in] parent  The parent widget; if not nullptr, the dialog will be shown centered over it
    \param[in] caption  The dialog caption; if empty a default caption will be used
    \param[in] init_dir  The directory which will be displayed first; if empty, the current directory will be used
    \return  The selected directory path, or an empty string if no directory was selected
 */
[[nodiscard]] auto do_select_dir_dialog(QWidget* parent = nullptr, 
                                        std::wstring_view caption = {}, 
		                                const fs::path& init_dir = {})-> fs::path;
                       
/*! Display (execute synchronously) a "file open" dialog for selecting an existing directory path, and display the 
    selected path (if any) in an edit widget. If the widget contains a path already, then it will be the initial 
    directory path; otherwise the current directory will be used.
    \param[in] widget  The edit widget
    \param[in] parent  The parent widget; if not nullptr, the dialog will be shown centered over it
    \param[in] caption  The dialog caption; if empty a default caption will be used
    \return  true if a directory was selected
 */
auto do_select_dir_dialog_to_widget(gsl::not_null<QLineEdit*> widget,
                                    QWidget* parent = nullptr,
                                    std::wstring_view caption = {}) -> bool;

}

#endif  


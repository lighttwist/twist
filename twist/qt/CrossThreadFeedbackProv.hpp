/// @file CrossThreadFeedbackProv.hpp
/// CrossThreadFeedbackProv class, inherits from QObject, MessageExceptionFeedbackProv 

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_QT_CROSS_THREAD_FEEDBACK_PROV_HPP
#define TWIST_QT_CROSS_THREAD_FEEDBACK_PROV_HPP

#include <mutex>

#include "twist/feedback_providers.hpp"
#include "twist/qt/FeedbackSignalProv.hpp"

#include <QObject>

namespace twist::qt {

/// A cross-thread message and exception feedback provider. The class is also thread-safe.
/// It relays its feedback messages to another, "receiver" message provider which (potentially) runs on 
///   a separate thread, in a safe manner, using Qt's signalling mechanisms. A typical use is to run the
///   instance of this provider on one (or several) worker threads while the "receiver" provider runs on 
///   a GUI thread.
class CrossThreadFeedbackProv : public QObject, public MessageExceptionFeedbackProv {
public:
	/// Constructor. Use this constructor to take ownership of the "receiver" message provider.
	///
	/// @param[in] feedback_receiver_prov  The "receiver" message provider
	///
	CrossThreadFeedbackProv(std::unique_ptr<MessageExceptionFeedbackProv> feedback_receiver_prov);

	/// Constructor. 
	///
	/// @param[in] feedback_receiver_prov  The "receiver" message provider
	///
	CrossThreadFeedbackProv(MessageExceptionFeedbackProv& feedback_receiver_prov);

private slots:
	void receive_prog_msg(QString msg, bool try_replace_prev);

	void receive_set_prog_msg_indent(int indent_steps);

	void receive_fatal_error_msg(QString msg);

	void receive_uncaught_exception(std::exception_ptr exptr);

private:
	void init();

	void do_set_prog_msg(std::wstring_view msg, bool try_replace_prev) override; // MessageFeedbackProv override

	void do_set_fatal_error_msg(std::wstring_view msg) override; // MessageFeedbackProv override

	void do_report_uncaught_exception(std::exception_ptr exptr) override; // ExceptionFeedbackProv override

	MessageExceptionFeedbackProv& feedback_receiver_prov_;
	std::unique_ptr<MessageExceptionFeedbackProv> owned_feedback_receiver_prov_;
	FeedbackSignalProv feedback_signal_prov_;
	int prog_msg_indent_steps_{0};
	std::mutex mutex_;

	TWIST_CHECK_INVARIANT_DECL
	Q_OBJECT
};

}

#endif

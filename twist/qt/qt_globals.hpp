/// @file qt_globals.hpp
/// Globals for the "twist::qt" namespace

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_QT_QT__GLOBALS_H
#define TWIST_QT_QT__GLOBALS_H

#include <QString>
#include <QVariant>
#include <QVersionNumber>

namespace twist {
class Date;
}

class QDate;

namespace twist::qt {

//! Convert the standard (wide-character) string \p str to a QString object.
[[nodiscard]] auto to_qstring(std::wstring_view str) -> QString;

/// Convert a QString object to a standard (wide-character) string.
///
/// @param[in] str  The input QString object
/// @return  The string
///
std::wstring to_string(const QString& str);

/// Convert a QString object to a standard file path.
///
/// @param[in] str  The input QString object
/// @return  The path
///
fs::path to_path(const QString& str);

/// Convert a QString object to a file path which has a "native" Windows form (eg uses backslash as directry 
/// separator).
///
/// @param[in] str  The input QString object
/// @return  The path
///
fs::path to_win_path(const QString& str);

/// Get a view of a Qstring object.
///
/// @param[in] str  The QString object
/// @return  The string view; only valid as long as the QString object is alive
///
std::wstring_view view(const QString& str);

/// Compare two strings, a QString and an std::wstring object. The comparison is case sensitive.
///
/// @param[in] str1  The first string
/// @param[in] str2  The second string
/// @return  true if the strings are identical
///
bool equal(const QString& str1, const std::wstring_view str2);

/// Compare two strings, an std::wstring and a QString object. The comparison is case sensitive.
///
/// @param[in] str1  The first string
/// @param[in] str2  The second string
/// @return  true if the strings are identical
///
bool equal(const std::wstring_view str1, const QString& str2);

/// Format a string.
///
/// @param[in] format  The format string; the following arguments (if any) must match the format; the 
///					formatting rules are the same as for printf()
/// @return  The formatted string; note that there is a maximum size for the string
/// 
QString format_qstring(const wchar_t* format, ...);

/// Convert a standard (wide-character) string to a QVariant object.
///
/// @param[in] str  The input string
/// @return  The QVariant object
///
QVariant to_qvariant(std::wstring_view str);

/// Convert a QDate object to a twist::Date object.
///
/// @param[in] date  The input QDate object
/// @return  The twist::Date object
///
Date to_date(const QDate& date);

/// Create a connection of the given type from the signal in a sender Qt object to a method in a Qt receiver 
/// object. The SIGNAL() and SLOT() macros must be used when specifying the signal and the method.
/// An exception is thrown if the connection fails.
/// Devnote: Consider using statically typed connect() overloads instead.
///
/// @param[in] sender  Sender object (cannot be nullptr) 
/// @param[in] signal  Signal name
/// @param[in] receiver  Receiver object (cannot be nullptr)
/// @param[in] method  The receiver object method
/// @param[in] conn_type  The Qt connection type
/// @return  A handle to the connection (that can be used to disconnect later)
///
QMetaObject::Connection connect_signal(const QObject* sender, const char* signal, const QObject* receiver, 
		const char* method, Qt::ConnectionType conn_type = Qt::AutoConnection);  

//! Get the version number of the Qt framework against which the application is compiled.
[[nodiscard]] auto get_static_qt_version() -> QVersionNumber;

}

#endif  

///  @file  TreeItem.cpp
///  Implementation file for "TreeItem.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "TreeItem.hpp"

#include "twist/std_vector_utils.hpp"

namespace twist::qt {

TreeItem::TreeItem(std::vector<QVariant>&& data_list, TreeItem* parent)
    : parent_item_{ parent }
    , item_data_list_{ move(data_list) }
{
	TWIST_CHECK_INVARIANT
}


TreeItem& TreeItem::child(int row)
{
	TWIST_CHECK_INVARIANT
    return *child_items_.at(row);
}


int TreeItem::child_count() const
{
	TWIST_CHECK_INVARIANT
    return size32(child_items_);
}


QVariant TreeItem::data(int col) const
{
	TWIST_CHECK_INVARIANT
    return item_data_list_.at(col);
}


TreeItem* TreeItem::parent_item()
{
	TWIST_CHECK_INVARIANT
    return parent_item_;
}


int TreeItem::row() const
{
	TWIST_CHECK_INVARIANT
	if (!parent_item_) return 0;

    return position_of_if(parent_item_->child_items_, [this](const auto& c) { 
		return c.get() == this; 
	});
}


#ifdef _DEBUG
void TreeItem::check_invariant() const noexcept
{
	assert(!item_data_list_.empty());
}
#endif

}

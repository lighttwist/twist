///  @file  TreeModelBase.hpp
///  TreeModelBase abstract class

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_QT_TREE_MODEL_BASE_HPP
#define TWIST_QT_TREE_MODEL_BASE_HPP

#include <QAbstractItemModel>
#include <QModelIndex>
#include <QVariant>

#include "twist/meta_ranges.hpp"

namespace twist::qt {

class TreeItem;

/// Abstract base for classes which implement the data model underlying a tree widget, in the 
/// model/view architecture.
class TreeModelBase : public QAbstractItemModel {
public:
	/// Destructor.
	virtual ~TreeModelBase() = 0;

	/// Append a child item to an item's list of children.
	///
	/// @param[in] parent  The parent item  
	/// @param[in] child_data  A set of data objects to be associated with the new child item, one for each 
	///					column in the tree widget
	/// @return  The new child item
	///
	TreeItem& add_child_item(TreeItem& parent, std::vector<QVariant>&& child_data_list);

    QVariant data(const QModelIndex& index, int role) const override;  // QAbstractItemModel override

    Qt::ItemFlags flags(const QModelIndex& index) const override;  // QAbstractItemModel override

    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;  // QAbstractItemModel override

    QModelIndex index(int row, int column, const QModelIndex& parent = QModelIndex()) const override;  // QAbstractItemModel override

    QModelIndex parent(const QModelIndex& index) const override;  // QAbstractItemModel override

    int rowCount(const QModelIndex& parent = QModelIndex()) const override;  // QAbstractItemModel override

    int columnCount(const QModelIndex& parent = QModelIndex()) const override;  // QAbstractItemModel override

protected:
	/// Constructor.
	///
	/// @tparam  Strings  Range type holding strings
	/// @param[in] headers  A range containing the headers which appear in the tree widget; the number of 
	///					headers defines how many columns there are in the widget
	/// @param[in] parent  The model's parent object; pass in nullptr if there is no parent  
	///
	template<class Strings, 
	         class = EnableIfRangeGives<Strings, std::wstring>>
    TreeModelBase(Strings&& headers, QObject* parent);

	/// Constructor.
	///
	/// @tparam  Strings  Range type holding strings
	/// @param[in] headers  A range containing the headers which appear in the tree widget; the number of 
	///					headers defines how many columns there are in the widget
	/// @param[in] parent  The model's parent object; pass in nullptr if there is no parent  
	///
	template<class String, 
	         class = EnableIfCreatable<std::wstring, String>>
    TreeModelBase(std::initializer_list<String> headers, QObject* parent);

	/// The tree's root item.
    const TreeItem& root_item() const;

	/// The tree's root item.
    TreeItem& root_item();

private:
	template<class Strings>
	void init(Strings&& headers);

    std::unique_ptr<TreeItem>  root_item_;
	int  tree_col_count_{};

	TWIST_CHECK_INVARIANT_DECL
    Q_OBJECT
};

}

#include "TreeModelBase.ipp"

#endif

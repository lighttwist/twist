/// @file SingleColumnCheckableTableModel.hpp
/// SingleColumnCheckableTableModel class, inherited from QAbstractTableModel

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_QT_SINGLE_COLUMN_CHECKABLE_TABLE_MODEL_HPP
#define TWIST_QT_SINGLE_COLUMN_CHECKABLE_TABLE_MODEL_HPP

#include "twist/qt/CheckableTableModel.hpp"

namespace twist::qt {

/*! Table "model" class (in accordance with Qt's model/view architecture) for table widgets which contain a single text 
    column and whose items are checkable via checkboxes displayed on each row.
	\note  The table model does not, at the moment, support headers and items can only be added and removed at the end 
	       of the table.
 */
class SingleColumnCheckableTableModel : public CheckableTableModel {
public:
	/*! Constructor. 
	    \param[in] parent  The model's parent object; pass in nullptr if there is no parent  
	 */
	SingleColumnCheckableTableModel(QObject* parent = nullptr);

	[[nodiscard]] auto nof_rows() const -> Ssize final; // CheckableTableModel override

	[[nodiscard]] auto nof_columns() const -> Ssize final; // CheckableTableModel override

	//! Add a new row to the end of the table, with the text \p text.
	auto add_row(std::wstring text) -> void;

	//! Get the text in the row with index \p row.
	[[nodiscard]] auto get_row_text(Ssize row) const -> std::wstring;

	//! Get the text in the rows with which are checked.
	[[nodiscard]] auto get_checked_row_texts() const -> std::vector<std::wstring>;

	//! Get the text in all rows.
	[[nodiscard]] auto get_all_row_texts() const -> std::vector<std::wstring>;

	//! Get the index of the row which contins the text \p text. Matching is case-sensitive.
	[[nodiscard]] auto get_row_index(std::wstring_view text) const -> Ssize;

	//! Set the text in the row with index \p row to \p text.
	auto set_row_text(Ssize row, std::wstring_view text) -> void;

	//! Delete the row with index \p row.
	auto delete_row(Ssize row) -> void;

	//! Delete all rows.
	auto delete_all_rows() -> void;

private:
	[[nodiscard]] auto get_cell_text(Ssize row, Ssize col) const -> std::wstring final; // CheckableTableModel override

	std::vector<std::wstring> rows_texts_;

	TWIST_CHECK_INVARIANT_DECL
	Q_OBJECT
};

#endif 

}

/// @file message_box_utils.cpp
/// Implementation file for "message_box_utils.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/qt/message_box_utils.hpp"

#pragma warning(disable: 4244) // Disable Qt warning
#pragma warning(disable: 4251) // Get rid of warnings inside QtCore/qvariant.h 

#include "twist/qt/qt_globals.hpp"

#include <QAbstractButton>
#include <QPushButton>

namespace twist::qt {

QMessageBox::StandardButton show_information_message_box(std::wstring_view text, std::wstring_view title, 
		QWidget* parent)
{
	return QMessageBox::information(parent, to_qstring(title), to_qstring(text));
}

QMessageBox::StandardButton show_critical_message_box(std::wstring_view text, std::wstring_view title, 
		QWidget* parent)
{
	return QMessageBox::critical(parent, to_qstring(title), to_qstring(text));
}

QMessageBox::StandardButton show_yesno_message_box(std::wstring_view text, std::wstring_view title,
		QWidget* parent)
{
	return show_question_message_box(
			text, title, {QMessageBox::StandardButton::Yes, QMessageBox::StandardButton::No}, parent);
}

QMessageBox::StandardButton show_question_message_box(std::wstring_view text, std::wstring_view title,
		const std::vector<QMessageBox::StandardButton>& buttons, QWidget* parent)
{
	QMessageBox msg_box{QMessageBox::Icon::Question, to_qstring(title), to_qstring(text), {}, parent};
	for (const auto& b : buttons) {
		msg_box.addButton(b);		
	}
	return QMessageBox::StandardButton(msg_box.exec());
}

QMessageBox::ButtonRole show_custom_message_box(std::wstring_view text, std::wstring_view title,
		const std::vector<CustomMessageBoxButton>& buttons, QWidget* parent)
{
	std::map<const QAbstractButton*, QMessageBox::ButtonRole>  button_widgets;

	QMessageBox msg_box{ {}, to_qstring(title), to_qstring(text), {}, parent };
	for (const auto& bi : buttons) {
		const auto* button = msg_box.addButton(to_qstring(bi.text), bi.role);		
		button_widgets.emplace(button, bi.role);
	}

	msg_box.exec();

	if (button_widgets.empty()) {
		return QMessageBox::ButtonRole(0);
	}

	return button_widgets.at(msg_box.clickedButton());
}

void display_except(const std::exception& ex, std::wstring_view title, QWidget* parent, bool print_func_name)
{
	if (title.empty()) {
		title = L"An exception has occurred.";
	}

	std::wstring text{ansi_to_string(ex.what())};

	if (print_func_name) {
		if (const auto* re = dynamic_cast<const RuntimeError*>(&ex)) {
			text += L"\n\nException thrown by function  " + re->function_name() + L"()";
		}
	}

	show_critical_message_box(text, title, parent);	
}

}

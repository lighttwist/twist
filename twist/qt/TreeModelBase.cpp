///  @file  TreeModelBase.hpp
///  Implementation file for "TreeModelBase.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/qt/TreeModelBase.hpp"

#include "twist/qt/qt_globals.hpp"
#include "twist/qt/TreeItem.hpp"

namespace twist::qt {

TreeModelBase::~TreeModelBase()
{
	TWIST_CHECK_INVARIANT
}


TreeItem& TreeModelBase::add_child_item(TreeItem& parent, std::vector<QVariant>&& child_data_list)
{
	if (size(child_data_list) != tree_col_count_) {
		TWIST_THROW(L"The number of data of data elements (%d) does not match the number of columns (%d).",
				size(child_data_list), tree_col_count_);
	}

	std::unique_ptr<TreeItem> child_item{ new TreeItem(move(child_data_list), &parent) };
	auto& child_item_ref = *child_item;
    
	parent.child_items_.push_back(move(child_item));

	return child_item_ref;
}


QVariant TreeModelBase::data(const QModelIndex& index, int role) const
{
	TWIST_CHECK_INVARIANT
    if (!index.isValid()) return QVariant{};
    if (role != Qt::DisplayRole) return QVariant{};

    auto* item = static_cast<TreeItem*>(index.internalPointer());

    return item->data(index.column());
}


Qt::ItemFlags TreeModelBase::flags(const QModelIndex& index) const
{
	TWIST_CHECK_INVARIANT
    if (!index.isValid()) {
		return {};
	}
    return QAbstractItemModel::flags(index);
}


QVariant TreeModelBase::headerData(int section, Qt::Orientation orientation, int role) const
{
	TWIST_CHECK_INVARIANT
    if (orientation != Qt::Horizontal || role != Qt::DisplayRole) return QVariant();
	return root_item_->data(section);
}


QModelIndex TreeModelBase::index(int row, int column, const QModelIndex& parent) const
{
	TWIST_CHECK_INVARIANT
    if (!hasIndex(row, column, parent)) return QModelIndex{};

    auto* parent_item = parent.isValid()
			? static_cast<TreeItem*>(parent.internalPointer())
			: root_item_.get();

    auto& child_item = parent_item->child(row);
    return createIndex(row, column, &child_item);
}


QModelIndex TreeModelBase::parent(const QModelIndex& index) const
{
	TWIST_CHECK_INVARIANT
    if (!index.isValid()) return QModelIndex();

    auto* parent_item = static_cast<TreeItem*>(index.internalPointer())->parent_item();
    if (parent_item == root_item_.get()) return QModelIndex();

    return createIndex(parent_item->row(), 0, parent_item);
}


int TreeModelBase::rowCount(const QModelIndex& parent) const
{
	TWIST_CHECK_INVARIANT
    if (parent.column() > 0) return 0;

    const auto* parent_item = parent.isValid() 
			? static_cast<TreeItem*>(parent.internalPointer()) 
			: root_item_.get();

    return parent_item->child_count();
}


int TreeModelBase::columnCount([[maybe_unused]] const QModelIndex& parent) const
{
	TWIST_CHECK_INVARIANT
    return tree_col_count_;
}


const TreeItem& TreeModelBase::root_item() const
{
	TWIST_CHECK_INVARIANT
	return *root_item_;
}


TreeItem& TreeModelBase::root_item()
{
	TWIST_CHECK_INVARIANT
	return *root_item_;
}


#ifdef _DEBUG
void TreeModelBase::check_invariant() const noexcept
{
	assert(root_item_);
	assert(tree_col_count_ > 0);
}
#endif

}

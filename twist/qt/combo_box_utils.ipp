/// @file combo_box_utils.ipp
/// Inline implementation file for "combo_box_utils.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/qt/qt_globals.hpp"

namespace twist::qt {

template<class Data, class> 
[[nodiscard]] auto add_item_with_data(gsl::not_null<QComboBox*> combo, std::wstring_view text, Data&& data) -> void
{
	combo->addItem(to_qstring(text), std::forward<Data>(data));
}

template<class Strings, class>
[[nodiscard]] auto add_items(gsl::not_null<QComboBox*> combo, const Strings& item_strings) -> void
{
	for (const auto& is : item_strings) {
		combo->addItem(to_qstring(is));
	}
}

template<class StringsAndData, class>
[[nodiscard]] auto add_items_with_data(gsl::not_null<QComboBox*> combo, const StringsAndData& item_strings_and_data) 
                    -> void
{
	for (const auto& sd : item_strings_and_data) {
		combo->addItem(to_qstring(sd.first), sd.second);
	}
}

template<class Enum>
requires std::is_enum_v<Enum> && 
         is_safe_numeric_cast<int, std::underlying_type_t<Enum>>
[[nodiscard]] auto current_data_as_enum(gsl::not_null<const QComboBox*> combo) -> Enum
{
	auto ok = true;
	const auto data = combo->currentData().toInt(&ok);
	if (!ok) {
		TWIST_THRO2(L"Combo box item data not convertible to int.");
	}
	return Enum{data};
}

}

///  @file  message_window_utils.cpp
///  Implementation file for "message_window_utils.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "message_window_utils.hpp"

#pragma warning(disable: 5054)  // Get rid of warnings inside QtGui/qpixelformat.h 

#include <QDialog>
#include <QGridLayout>
#include <QSizePolicy>
#include <QTextEdit>
#include <QWidget>

#include "qt_globals.hpp"
#include "text_edit_utils.hpp"

namespace twist::qt {


void show_message_window(std::wstring_view text, std::wstring_view title, QSize init_size)
{
	QDialog wnd;
	if (!title.empty()) {
		wnd.setWindowTitle(to_qstring(title));
	}

    auto tedt_message = new QTextEdit(&wnd);
	tedt_message->setReadOnly(true);
	set_text(tedt_message, text);

	auto layout = new QVBoxLayout(&wnd);
	layout->addWidget(tedt_message);
	wnd.setLayout(layout);

	if (init_size.width() > 0 && init_size.height() > 0) {
		wnd.resize(init_size.width(), init_size.height());
	}
	
	wnd.exec();
}

}

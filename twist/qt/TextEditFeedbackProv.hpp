///  @file  TextEditFeedbackProv.hpp
///  TextEditFeedbackProv class, inherits MessageFeedbackProv

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_QT_TEXT_EDIT_FEEDBACK_PROV_HPP
#define TWIST_QT_TEXT_EDIT_FEEDBACK_PROV_HPP
	
#include "twist/feedback_providers.hpp"

class QTextEdit;

namespace twist::qt {

/// Feedback provider which displays each feedback message as a new line at the bottom of a text edit widget.
class TextEditFeedbackProv : public virtual MessageFeedbackProv {
public:
	/// Constructor.
	///
	/// @param[in] widget  The widget
	///
	TextEditFeedbackProv(gsl::not_null<QTextEdit*> widget);

private:
	void do_set_prog_msg(std::wstring_view msg, bool try_replace_prev) override;  // MessageFeedbackProv override

	void do_set_fatal_error_msg(std::wstring_view msg) override;  // MessageFeedbackProv override

	gsl::not_null<QTextEdit*> widget_;

	TWIST_CHECK_INVARIANT_DECL
};

}

#endif

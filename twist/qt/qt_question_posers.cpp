/// @file qt_question_posers.cpp
/// Implementation file for "qt_question_posers.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/qt/qt_question_posers.hpp"

#include "twist/qt/qt_globals.hpp"
#include "twist/qt/message_box_utils.hpp"

namespace twist::qt {

MessageBoxYesNoQuestionPoser::MessageBoxYesNoQuestionPoser(std::wstring caption, QWidget& parent)
	: YesNoQuestionPoser{}
	, caption_{move(caption)}
	, parent_{parent}
{
	connect_signal(this, SIGNAL(send_question(QString)), 
			       this, SLOT(receive_question(QString)));
	TWIST_CHECK_INVARIANT
}

auto MessageBoxYesNoQuestionPoser::ask(std::wstring_view msg) -> bool
{
	TWIST_CHECK_INVARIANT
	answer_is_yes_.reset();

	send_question(to_qstring(msg));

	auto lock = std::unique_lock{mutex_};
	cond_var_.wait(lock, [this] { return answer_is_yes_.has_value(); });

	return *answer_is_yes_;
}

void MessageBoxYesNoQuestionPoser::receive_question(QString msg)
{
	TWIST_CHECK_INVARIANT
	const auto answer_is_yes = show_yesno_message_box(to_string(msg), caption_, std::addressof(parent_)) == 
			                           QMessageBox::StandardButton::Yes;
			
	auto lock = std::unique_lock{mutex_};
	assert(!answer_is_yes_.has_value());
	answer_is_yes_ = answer_is_yes;
	cond_var_.notify_all();
}

#ifdef _DEBUG
auto MessageBoxYesNoQuestionPoser::check_invariant() const noexcept -> void
{
}
#endif

}

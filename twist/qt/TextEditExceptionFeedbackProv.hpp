/// @file TextEditExceptionFeedbackProv.hpp
/// TextEditExceptionFeedbackProv class, inherits TextEditFeedbackProv, MessageExceptionFeedbackProv

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_QT_TEXT_EDIT_EXCEPTION_FEEDBACK_PROV_HPP
#define TWIST_QT_TEXT_EDIT_EXCEPTION_FEEDBACK_PROV_HPP
	
#include "../feedback_providers.hpp"

#include "TextEditFeedbackProv.hpp"

class QTextEdit;

namespace twist::qt {

#pragma warning(push)
#pragma warning(disable: 4250)  // Remove spurious warning in VS2017 (and apparently in many versions before) 

/// Feedback provider which displays each feedback message as a new line at the bottom of a text edit widget, 
/// and additionally provides a channel to report an uncaught exception.
class TextEditExceptionFeedbackProv 
		: public TextEditFeedbackProv
		, public MessageExceptionFeedbackProv {
public:
	using ExceptionListener = std::function<void(std::exception_ptr)>;

	/// Constructor.
	///
	/// @param[in] widget  The widget
	/// @param[in] on_uncaught_exception  Function object to be called when an uncaught exception is reported  
	///
	TextEditExceptionFeedbackProv(gsl::not_null<QTextEdit*> widget, ExceptionListener on_uncaught_exception); 

private:
	void do_report_uncaught_exception(std::exception_ptr exptr) override;  // ExceptionFeedbackProv override

	ExceptionListener  on_uncaught_exception_{};

	TWIST_CHECK_INVARIANT_DECL
};

#pragma warning(pop)

}

#endif

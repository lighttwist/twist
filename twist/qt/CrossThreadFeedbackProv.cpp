/// @file CrossThreadFeedbackProv.cpp
/// Implementation file for "CrossThreadFeedbackProv.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/qt/CrossThreadFeedbackProv.hpp"

#include "twist/qt/qt_globals.hpp"

namespace twist::qt {

CrossThreadFeedbackProv::CrossThreadFeedbackProv(
		std::unique_ptr<MessageExceptionFeedbackProv> feedback_receiver_prov)
	: QObject{nullptr/*parent*/}
	, feedback_receiver_prov_{*feedback_receiver_prov}
{
	owned_feedback_receiver_prov_ = move(feedback_receiver_prov);
	init();
	TWIST_CHECK_INVARIANT
}

CrossThreadFeedbackProv::CrossThreadFeedbackProv(MessageExceptionFeedbackProv& feedback_receiver_prov)
	: QObject{nullptr/*parent*/}
	, feedback_receiver_prov_{feedback_receiver_prov}
{
	init();
	TWIST_CHECK_INVARIANT
}

void CrossThreadFeedbackProv::receive_prog_msg(QString msg, bool try_replace_prev)
{
	TWIST_CHECK_INVARIANT
	feedback_receiver_prov_.set_prog_msg(to_string(msg), try_replace_prev);
}

void CrossThreadFeedbackProv::receive_set_prog_msg_indent(int indent_steps)
{
	TWIST_CHECK_INVARIANT
	feedback_receiver_prov_.set_prog_msg_indent(indent_steps);
}

void CrossThreadFeedbackProv::receive_fatal_error_msg(QString msg)
{
	TWIST_CHECK_INVARIANT
	feedback_receiver_prov_.set_fatal_error_msg(to_string(msg));
}

void CrossThreadFeedbackProv::receive_uncaught_exception(std::exception_ptr exptr)
{
	TWIST_CHECK_INVARIANT
	feedback_receiver_prov_.report_uncaught_exception(exptr);  
}

void CrossThreadFeedbackProv::init()
{
	connect_signal(&feedback_signal_prov_, SIGNAL(send_prog_msg(QString, bool)), 
			       this, SLOT(receive_prog_msg(QString, bool)));

	connect_signal(&feedback_signal_prov_, SIGNAL(send_set_prog_msg_indent(int)), 
			       this, SLOT(receive_set_prog_msg_indent(int)));

	connect_signal(&feedback_signal_prov_, SIGNAL(send_fatal_error_msg(QString)), 
			       this, SLOT(receive_fatal_error_msg(QString)));

	connect_signal(&feedback_signal_prov_, SIGNAL(send_uncaught_exception(std::exception_ptr)), 
			       this, SLOT(receive_uncaught_exception(std::exception_ptr)));
}

void CrossThreadFeedbackProv::do_set_prog_msg(std::wstring_view msg, bool try_replace_prev)
{
	TWIST_CHECK_INVARIANT
	auto lock = std::scoped_lock{mutex_};
	feedback_signal_prov_.set_prog_msg(msg, try_replace_prev);
}

void CrossThreadFeedbackProv::do_set_fatal_error_msg(std::wstring_view msg)
{
	TWIST_CHECK_INVARIANT
	auto lock = std::scoped_lock{mutex_};
	feedback_signal_prov_.set_fatal_error_msg(msg);
}

void CrossThreadFeedbackProv::do_report_uncaught_exception(std::exception_ptr exptr)
{
	TWIST_CHECK_INVARIANT
	auto lock = std::scoped_lock{mutex_};
	feedback_signal_prov_.report_uncaught_exception(exptr);  
}

#ifdef _DEBUG
void CrossThreadFeedbackProv::check_invariant() const noexcept
{
}
#endif

}


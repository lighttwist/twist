///  @file  item_view_utils.cpp
///  Implementation file for "item_view_utils.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "item_view_utils.hpp"

#pragma warning(disable: 4244) // Disable Qt warning

#include <QAbstractItemView>
#include <QMenu>

#include "PopupMenu.hpp"
#include "qt_globals.hpp"

using gsl::not_null;

namespace twist::qt {

void ItemViewCustomContextMenu::attach(not_null<QAbstractItemView*> view, GetMenu&& get_menu)
{
	new ItemViewCustomContextMenu(*view, view, std::forward<GetMenu>(get_menu));  // new pointer owned by the view
}


ItemViewCustomContextMenu::ItemViewCustomContextMenu(QAbstractItemView& view, QObject* parent, 
		GetMenu&& get_menu)
	: QObject{ parent }
	, view_{ view }
	, get_menu_{ move(get_menu) }
{
	view_.setContextMenuPolicy(Qt::CustomContextMenu);
	connect_signal(&view_, SIGNAL(customContextMenuRequested(QPoint)),
				   this, SLOT(on_context_menu(QPoint)));
}


void ItemViewCustomContextMenu::on_context_menu(QPoint pt)
{
	// Make sure that the menu was required (probably through a right-click) over an actual view item
	const auto view_idx = view_.indexAt(pt);
	if (!view_idx.isValid()) return;

	// Get the appropriate menu (if any) given the type of view item 
	auto menu = get_menu_(view_idx);
	if (menu.is_empty()) return;

	// Display menu (point coordinates relative to the viewport area for QAbstractScrollArea-derived classes)
	const auto global_pos = view_.viewport()->mapToGlobal(pt);
	menu.show(global_pos);
}

//
//  Free functions  
//

int get_first_selected_row(not_null<const QAbstractItemView*> view)
{
	assert(view->selectionBehavior() == QAbstractItemView::SelectRows);

	const auto selected_rows = view->selectionModel()->selectedRows();
	if (!selected_rows.empty()) {
		return selected_rows[0].row();
	}
	return -1;
}

}

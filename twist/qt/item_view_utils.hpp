///  @file  item_view_utils.hpp
///  Utilities for working with Qt "item view" classes

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_QT_ITEM__VIEW__UTILS_HPP
#define TWIST_QT_ITEM__VIEW__UTILS_HPP

#pragma warning(push)
#pragma warning(disable: 5054)  // Get rid of warnings inside QtGui/qpixelformat.h 

#include <QAbstractItemModel>
#include <QAbstractItemView>
#include <QObject>
#include <QPoint>

#include "twist/metaprogramming.hpp"

namespace twist::qt {

class PopupMenu;

/// Class which manages custom context (aka popup) menus accessible by right-clicking on an item in an "item 
/// view" (ie in a widget derived from QAbstractItemView).
class ItemViewCustomContextMenu : public QObject {
public:
	using GetMenu = std::function<PopupMenu(const QModelIndex&)>;

	/// Attach a context menu provider function to an "item view".
	///
	/// @param[in] view  The view
	/// @param[in] get_menu  Function object which returns the menu associated with specific item, as 
	///					identified by function argument
	///
	static void attach(gsl::not_null<QAbstractItemView*> view, GetMenu&& get_menu);
	
private slots:
	void on_context_menu(QPoint pt);

private:
	ItemViewCustomContextMenu(QAbstractItemView& view, QObject* parent, GetMenu&& get_menu);

	QAbstractItemView&  view_;
	GetMenu  get_menu_;

	Q_OBJECT
};

/// Set the model for an "item view" (ie in a widget derived from QAbstractItemView) such that the view 
/// owns it.
///
/// @tparam  Model  The model type
/// @tparam  Args  The types of the model constructor arguments (if any) excluding the Qt parent argument
/// @param[in] view  The view
/// @param[in] args  The model constructor arguments (if any) excluding the Qt parent argument
/// @return  The model
///
template<class Model, class... Args> 
auto set_owned_model(gsl::not_null<QAbstractItemView*> view, Args&&... args) -> 
		EnableIf<is_base_of<QAbstractItemModel, Model>, Model&>;

/// Get the index of the first selected row in an "item view" (ie in a widget derived from QAbstractItemView).
/// 
/// @param[in] view  The view; its selection behaviour is expected to be "select rows"
/// @return  The index of the first selected row, or -1 if there is no selection
///
int get_first_selected_row(gsl::not_null<const QAbstractItemView*> view);

}

#include "item_view_utils.ipp"

#pragma warning(pop)

#endif


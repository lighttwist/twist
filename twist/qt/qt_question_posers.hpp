/// @file qt_question_posers.hpp
/// Classes inheriting from abstract base classes defined in "question_posers.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_QT_QUESTION_POSERS_HPP
#define TWIST_QT_QUESTION_POSERS_HPP

#include "twist/question_posers.hpp"

#include <QObject>

#include <condition_variable>

class QWidget;

namespace twist::qt {

/*! Implements YesNoQuestionPoser using the standard Qt message box.
    The poser must be created in the GUI thread, but can be used from any thread.
 */
class MessageBoxYesNoQuestionPoser : public QObject, public YesNoQuestionPoser {
public:
	/*! Constructor.
	    \param[in] caption  Message box caption
	    \param[in] parent  Message box parent
	*/
	explicit MessageBoxYesNoQuestionPoser(std::wstring caption, QWidget& parent);

	[[nodiscard]] auto ask(std::wstring_view msg) -> bool final; // YesNoQuestionPoser override

signals:
	void send_question(QString msg);

private slots:
	void receive_question(QString msg);

private:
	std::wstring caption_;
	QWidget& parent_;

	std::condition_variable cond_var_;
	std::mutex mutex_;
	std::optional<bool> answer_is_yes_;

	TWIST_CHECK_INVARIANT_DECL
	Q_OBJECT
};

}

#endif 

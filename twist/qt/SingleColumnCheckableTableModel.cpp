/// @file SingleColumnCheckableTableModel.hpp
/// Implementation file for "SingleColumnCheckableTableModel.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/qt/SingleColumnCheckableTableModel.hpp"

namespace twist::qt {

SingleColumnCheckableTableModel::SingleColumnCheckableTableModel(QObject* parent)
	: CheckableTableModel{parent}
{
	TWIST_CHECK_INVARIANT
}

auto SingleColumnCheckableTableModel::add_row(std::wstring text) -> void
{
	TWIST_CHECK_INVARIANT
	begin_insert_row();
	rows_texts_.push_back(std::move(text));
	endInsertRows();
}

auto SingleColumnCheckableTableModel::get_row_text(Ssize row) const -> std::wstring
{
	TWIST_CHECK_INVARIANT
	return rows_texts_.at(row);	
}

auto SingleColumnCheckableTableModel::get_checked_row_texts() const -> std::vector<std::wstring>
{
	TWIST_CHECK_INVARIANT
	return checked_row_indexes() | vw::transform([this](auto row) { return rows_texts_[row]; }) 
	                             | rg::to<std::vector>();
}

auto SingleColumnCheckableTableModel::get_all_row_texts() const -> std::vector<std::wstring>
{
	TWIST_CHECK_INVARIANT
	return rows_texts_;
}

auto SingleColumnCheckableTableModel::get_row_index(std::wstring_view text) const -> Ssize
{
	TWIST_CHECK_INVARIANT
	return position_of(rows_texts_, text);
}

auto SingleColumnCheckableTableModel::set_row_text(Ssize row, std::wstring_view text) -> void
{
	TWIST_CHECK_INVARIANT
	rows_texts_.at(row) = text;		
}

auto SingleColumnCheckableTableModel::delete_row(Ssize row) -> void
{
	TWIST_CHECK_INVARIANT
	remove_rows(row, row);
	rows_texts_.erase(begin(rows_texts_) + row);
}

auto SingleColumnCheckableTableModel::delete_all_rows() -> void
{
	TWIST_CHECK_INVARIANT
	const auto nof_rows = ssize32(rows_texts_);
	if (nof_rows > 0) {
		remove_rows(0, nof_rows - 1);
		rows_texts_.clear();
	}
}

auto SingleColumnCheckableTableModel::nof_rows() const -> Ssize 
{
	TWIST_CHECK_INVARIANT
	return ssize(rows_texts_);
}

auto SingleColumnCheckableTableModel::nof_columns() const -> Ssize 
{
	TWIST_CHECK_INVARIANT
	return 1;
}

auto SingleColumnCheckableTableModel::get_cell_text(Ssize row, [[maybe_unused]] Ssize col) const -> std::wstring 
{
	TWIST_CHECK_INVARIANT
	assert(col == 0);
	return rows_texts_.at(row);
}

#ifdef _DEBUG
auto SingleColumnCheckableTableModel::check_invariant() const noexcept -> void
{
}
#endif 

}

///  @file  text_edit_utils.cpp
///  Implementation file for "text_edit_utils.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "text_edit_utils.hpp"

#include "qt_globals.hpp"

#pragma warning(disable: 5054)  // Get rid of warnings inside QtGui/qpixelformat.h 

#include <QLineEdit>
#include <QTextEdit>

using gsl::not_null;

namespace twist::qt {

std::wstring to_plain_text(not_null<const QTextEdit*> widget)
{
	return to_string(widget->toPlainText());
}

auto text(not_null<const QLineEdit*> widget) -> std::wstring
{
	return to_string(widget->text());
}

auto set_text(not_null<QTextEdit*> widget, std::wstring_view text) -> void
{
	widget->setText(to_qstring(text));
}

auto set_text(not_null<QLineEdit*> widget, std::wstring_view text) -> void
{
	widget->setText(to_qstring(text));
}

void set_text(not_null<QLineEdit*> widget, int value)
{
	widget->setText(QString::number(value));
}


void set_text(not_null<QLineEdit*> widget, double value, char format, int precision)
{
	widget->setText(QString::number(value, format, precision));
}

void append(not_null<QTextEdit*> widget, std::wstring_view text)
{
	widget->append(to_qstring(text));
}

auto to_double(not_null<const QLineEdit*> widget) -> double
{
	return widget->text().toDouble();
}

auto to_int(gsl::not_null<const QLineEdit*> widget) -> int
{
	return widget->text().toInt();
}

auto to_path(gsl::not_null<const QLineEdit*> widget) -> fs::path
{
	return to_path(widget->text());
}

void delete_last_line(not_null<QTextEdit*> widget)
{
	widget->setFocus();
	const auto cursor_pos = widget->textCursor();
	widget->moveCursor(QTextCursor::End, QTextCursor::MoveAnchor);
	widget->moveCursor(QTextCursor::StartOfLine, QTextCursor::MoveAnchor);
	widget->moveCursor(QTextCursor::End, QTextCursor::KeepAnchor);
	widget->textCursor().removeSelectedText();
	widget->textCursor().deletePreviousChar();
	widget->setTextCursor(cursor_pos);
}

}

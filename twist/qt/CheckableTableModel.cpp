/// @file CheckableTableModel.hpp
/// Implementation file for "CheckableTableModel.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/qt/CheckableTableModel.hpp"

#include "twist/qt/qt_globals.hpp"

namespace twist::qt {

CheckableTableModel::CheckableTableModel(QObject* parent)
	: QAbstractTableModel{parent}
{
	TWIST_CHECK_INVARIANT
}

auto CheckableTableModel::checked_row_indexes() const -> const std::set<twist::Ssize>&
{
	TWIST_CHECK_INVARIANT
	return checked_rows_;
}

auto CheckableTableModel::set_row_check(Ssize row, bool checked) -> void
{
	TWIST_CHECK_INVARIANT
	assert(row < nof_rows());

	const auto it = checked_rows_.find(row);
	if (checked && (it == end(checked_rows_))) {
		checked_rows_.insert(row);
	}
	else if (!checked && (it != end(checked_rows_))) {
		checked_rows_.erase(it);
	}
}

auto CheckableTableModel::begin_insert_row() -> void
{
	TWIST_CHECK_INVARIANT
	const auto old_nof_rows = nof_rows();
	beginInsertRows(QModelIndex{}, static_cast<int>(old_nof_rows), static_cast<int>(old_nof_rows));
}

auto CheckableTableModel::after_insert_rows() -> void
{
	TWIST_CHECK_INVARIANT
	endInsertRows();
}

auto CheckableTableModel::remove_rows(twist::Ssize first_row, twist::Ssize last_row) -> void

{
	TWIST_CHECK_INVARIANT
	assert(first_row <= last_row);

	beginRemoveRows(QModelIndex{}, static_cast<int>(first_row), static_cast<int>(last_row));
	removeRows(static_cast<int>(first_row), static_cast<int>(last_row - first_row + 1));
	endRemoveRows();

	for (auto i : IndexRange{first_row, last_row}) {
		checked_rows_.erase(checked_rows_.find(i));
	}
}

auto CheckableTableModel::flags(const QModelIndex& index) const -> Qt::ItemFlags
{
	TWIST_CHECK_INVARIANT
	assert(index.column() == 0);
	return QAbstractTableModel::flags(index) | Qt::ItemIsUserCheckable;
}

auto CheckableTableModel::rowCount(const QModelIndex& /*parent*/) const -> int
{
	TWIST_CHECK_INVARIANT
	return static_cast<int>(nof_rows());
}

auto CheckableTableModel::columnCount(const QModelIndex& /*parent*/) const -> int
{
	TWIST_CHECK_INVARIANT
	return static_cast<int>(nof_columns());
}

auto CheckableTableModel::headerData(int /*section*/, Qt::Orientation /*orientation*/, int /*role*/) const 
      -> QVariant
{
	TWIST_CHECK_INVARIANT
	return QVariant{};
}

auto CheckableTableModel::data(const QModelIndex& index, int role) const -> QVariant 
{
	TWIST_CHECK_INVARIANT
	assert(index.column() < nof_columns());
	assert(index.row() < nof_rows()); 

	switch (role) {
	case Qt::DisplayRole: 
		return to_qvariant(get_cell_text(index.row(), index.column()));
	case Qt::CheckStateRole:
		return checked_rows_.contains(index.row()) ? QVariant{Qt::Checked} : QVariant{Qt::Unchecked};
	default:
		return QVariant{};
	}
}

auto CheckableTableModel::setData(const QModelIndex &index, const QVariant &value, int role) -> bool
{
	assert(index.column() < nof_columns());
	assert(index.row() < nof_rows()); 

    if (role == Qt::CheckStateRole) {
		if (value.toInt() == Qt::Checked) {
			checked_rows_.insert(index.row());
		}
		else {
			checked_rows_.erase(index.row());
		}
        return true;
    }
    return QAbstractTableModel::setData(index,value,role);
}

#ifdef _DEBUG
auto CheckableTableModel::check_invariant() const noexcept -> void
{
}
#endif 

// --- Free functions ---

auto set_all_row_checks(CheckableTableModel& model, bool checked) -> Ssize
{
    for (auto i : IndexRange{model.nof_rows()}) {
        model.set_row_check(i, checked);
    }
    return model.nof_rows();
}

}

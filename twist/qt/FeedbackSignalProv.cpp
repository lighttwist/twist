/// @file FeedbackSignalProv.cpp
/// Implementation file for "FeedbackSignalProv.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/qt/FeedbackSignalProv.hpp"

#include "twist/qt/qt_globals.hpp"

namespace twist::qt {

FeedbackSignalProv::FeedbackSignalProv()
	: QObject{nullptr/*parent*/}
	, MessageExceptionFeedbackProv{}
{
	if (!metatypes_registered__) {
		metatypes_registered__ = true;
		// Register std::exception_ptr as a meta-type in Qt's meta-object system
		qRegisterMetaType<std::exception_ptr>();
	}
}

void FeedbackSignalProv::do_set_prog_msg(std::wstring_view msg, bool try_replace_prev)
{
	emit send_prog_msg(to_qstring(msg), try_replace_prev);
}

void FeedbackSignalProv::do_set_fatal_error_msg(std::wstring_view msg)
{
	emit send_fatal_error_msg(to_qstring(msg));
}

void FeedbackSignalProv::do_report_uncaught_exception(std::exception_ptr exptr)
{
	emit send_uncaught_exception(exptr);
}

}

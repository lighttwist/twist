///  @file  Application.hpp
///  Application class template definition

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_QT_APPLICATION_HPP
#define TWIST_QT_APPLICATION_HPP

#include <QApplication>
#include <QMainWindow>

namespace twist::qt {

/// Application class for a Qt application.
///
/// @tparam  MainWindow  The type of the main application window; must be derived from (or be) QWindow
///
template<class MainWindow>
class Application : public QApplication {
public:
	/// Constructor.
	///
	/// @param[in] argc  The number of command line arguments 
	/// @param[in] argv  Array of command line argument string  
	///
	Application(int& argc, char** argv);

	/// Specify the main window of the application. Can only be called once.
	///
	/// @param[in] main_wnd  The main window
	///
	void set_main_window(gsl::not_null<MainWindow*> main_wnd);

	/// Get the main window of the application. Throws an exception if the main window has not been set.
	///
	/// @return  The main window
	///
	gsl::not_null<MainWindow*> main_window();

private:
	MainWindow*  main_wnd_{};

	static_assert(std::is_base_of_v<QMainWindow, MainWindow>, 
			"The main window class must be derived from QMainWindow");
};

}

#include "Application.ipp"

#endif 

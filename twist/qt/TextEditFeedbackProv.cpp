///  @file  TextEditFeedbackProv.cpp
///  Implementation file for "TextEditFeedbackProv.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/qt/TextEditFeedbackProv.hpp"

#pragma warning(disable: 5054)  // Get rid of warnings inside QtGui/qpixelformat.h 

#include "twist/qt/text_edit_utils.hpp"
#include "twist/qt/qt_globals.hpp"

#include <QTextEdit>

using gsl::not_null;

namespace twist::qt {

TextEditFeedbackProv::TextEditFeedbackProv(not_null<QTextEdit*> widget)
	: MessageFeedbackProv{}
	, widget_{widget}
{
	TWIST_CHECK_INVARIANT
}

void TextEditFeedbackProv::do_set_prog_msg(std::wstring_view msg, bool try_replace_prev)
{
	TWIST_CHECK_INVARIANT
	if (try_replace_prev) {
		delete_last_line(widget_);
	}
	widget_->append(to_qstring(msg));
}

void TextEditFeedbackProv::do_set_fatal_error_msg(std::wstring_view msg)
{
	TWIST_CHECK_INVARIANT
	widget_->append("\nFATAL ERROR:  " + to_qstring(msg));
}

#ifdef _DEBUG
void TextEditFeedbackProv::check_invariant() const noexcept
{
}
#endif

}

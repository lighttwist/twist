/// @file qt_globals.cpp
/// Implementation file for "qt_globals.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 
#include "twist/twist_maker.hpp"

#include "qt_globals.hpp"

#include "twist/Date.hpp"
#include "twist/string_utils.hpp"

#include <QDate>
#include <QObject>

namespace twist::qt {

// --- Local functions ---

std::wstring object_name(const QObject* obj)
{
	if (obj == nullptr) return L"nullptr";
	return to_string(obj->objectName());
}

// --- Global functions ---

auto to_qstring(std::wstring_view str) -> QString
{
	assert(str.size() <= static_cast<int>(str.size()));
	return QString::fromUtf16(reinterpret_cast<const char16_t*>(str.data()), 
			                  static_cast<int>(str.size()));
}

std::wstring to_string(const QString& str)
{
	return reinterpret_cast<const wchar_t*>(str.utf16());
}

fs::path to_path(const QString& str)
{
	return reinterpret_cast<const wchar_t*>(str.utf16());
}

fs::path to_win_path(const QString& str)
{
	return replace_all_substr<wchar_t>(to_string(str), L"/", L"\\");
}


std::wstring_view view(const QString& str)
{
	return std::wstring_view{
			reinterpret_cast<const wchar_t*>(str.utf16()),
			static_cast<size_t>(str.length())};
}


bool equal(const QString& str1, std::wstring_view str2)
{
	if (str1.size() != ssize(str2)) {
		return false;
	}
	auto buf2 = reinterpret_cast<const wchar_t*>(str1.utf16());
	return std::equal(begin(str2), end(str2), buf2); 
}


bool equal(const std::wstring_view str1, const QString& str2)
{
	if (ssize(str1) != str2.size()) {
		return false;
	}
	auto buf2 = reinterpret_cast<const wchar_t*>(str2.utf16());
	return std::equal(begin(str1), end(str1), buf2); 
}


QString format_qstring(const wchar_t* format, ...)
{
	va_list args;
	va_start(args, format);
	const auto ret = to_qstring(format_str(format, args));
	va_end(args);

	return ret;
}


QVariant to_qvariant(std::wstring_view str)
{
	return to_qstring(str);
}


Date to_date(const QDate& date)
{
	return {date.day(), date.month(), date.year()};
}


QMetaObject::Connection connect_signal(const QObject* sender, const char* signal, const QObject* receiver, 
		const char* method, Qt::ConnectionType conn_type)
{
	assert(sender && signal && receiver && method);

	const auto conn = QObject::connect(sender, signal, receiver, method, conn_type);
	if (!conn) {
		TWIST_THROW(L"Qt connection failure for signal \"%s\" and slot \"%s::%s\".",  
				to_string(signal).c_str(), object_name(receiver).c_str(), to_string(method).c_str());
	}
	return conn;
}

auto get_static_qt_version() -> QVersionNumber
{
	const auto version_part_strings = tokenise<char>(QT_VERSION_STR, ".");
	if (ssize(version_part_strings) != 3) {
		TWIST_THROW(L"Invalid static Qt framework version string \"%s\".", ansi_to_string(QT_VERSION_STR).c_str());
	}
	return QVersionNumber{to_int_checked(version_part_strings[0]), 
	                      to_int_checked(version_part_strings[1]), 
						  to_int_checked(version_part_strings[2])};
}

}


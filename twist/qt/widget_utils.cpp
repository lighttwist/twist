///  @file  widget_utils.cpp
///  Implementation file for "widget_utils.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "widget_utils.hpp"

#pragma warning(disable: 5054)  // Get rid of warnings inside QtGui/qpixelformat.h 

#include <QWidget>

using gsl::not_null;

namespace twist::qt {

auto set_enabled(not_null<QWidget*> widget, bool enabled) -> void
{
	widget->setEnabled(enabled);
}

auto set_enabled(std::initializer_list<not_null<QWidget*>> widgets, bool enabled) -> void
{
	for (auto w : widgets) {
		w->setEnabled(enabled);
	}
}

auto set_visible(gsl::not_null<QWidget*> widget, bool visible) -> void
{
	widget->setVisible(visible);
}

auto set_visible(std::initializer_list<not_null<QWidget*>> widgets, bool visible) -> void
{
	for (auto w : widgets) {
		w->setVisible(visible);
	}
}

}

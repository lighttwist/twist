///  @file  Meta.hpp
///  Meta class template

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_QT_META_HPP
#define TWIST_QT_META_HPP

namespace twist::qt {

/// Qt metatype wrapper for objects to be used as data parameters in signal and slot functions.
/// Use this wrapper for objects which do not posses the necessary semantics (such as the default and the 
///   copy constructor).
/// Each templated metatype must be registered with the Qt meta-object system before being used in the signal 
///   slot mechanism (see Q_DECLARE_METATYPE() and qRegisterMetaType()).
///
/// @tparam  Data  The wrapped data object type
///
template<class Data>
class Meta {
public:
	/// Default constructor. Creates an empty wrapper.
	Meta() = default;

	/// Constructor. 
	///
	/// @param[in] data  The data object to wrap.  
	///
	Meta(std::shared_ptr<const Data> data);

	/// Constructor. 
	///
	/// @param[in] data  The data object to wrap.  
	///
	Meta(std::unique_ptr<const Data> data);

	/// Find out whether the wrapper is empty (no data object is wrapped).
	///
	/// @return  true if the wrapper is empty
	///
	bool is_empty() const;

	/// Get the data object contained by the wrapper. An exception is thrown if the wrapper is empty.
	///
	/// @return  The wrapped data object
	///
	const Data& data() const;

	/// Get the wrapped data object pointer.
	///
	/// @return  The data object pointer; can be empty
	///
	std::shared_ptr<const Data> data_ptr() const;

private:
	std::shared_ptr<const Data>  data_;
};

}

#include "Meta.ipp"

#endif  

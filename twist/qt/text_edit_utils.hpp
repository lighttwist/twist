///  @file  text_edit_utils.hpp
///  Utilities for working with the Qt text edit widget classes

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_QT_TEXT__EDIT_UTILS_HPP
#define TWIST_QT_TEXT__EDIT_UTILS_HPP

class QLineEdit;
class QTextEdit;

namespace twist::qt {

/// Get a text editor's contents as plain text.
///
/// @param[in] widget  The text editor widget
/// @return  The content as plain text
///
std::wstring to_plain_text(gsl::not_null<const QTextEdit*> widget);

/*! Get a line editor's text content.
    \param[in] widget  The text editor widget
    \return  The content
 */
[[nodiscard]] auto text(gsl::not_null<const QLineEdit*> widget) -> std::wstring;

/*! Set a text editor's text content.
    \param[in] widget  The text editor widget
    \param[in] text  The text; it can be plain text or HTML and the text edit will try to guess the right format
 */
auto set_text(gsl::not_null<QTextEdit*> widget, std::wstring_view text) -> void;

/*! Set a line editor's text content.
    \param[in] widget  The text editor widget
    \param[in] text  The text
 */
auto set_text(gsl::not_null<QLineEdit*> widget, std::wstring_view text) -> void;

/// Write a textual representation of an integer value in a text edit widget.
///
/// @param[in] widget  The widget
/// @param[in] value  The integer value
///
void set_text(gsl::not_null<QLineEdit*> widget, int value);

/// Write a textual representation of a floating-point value in a text edit widget.
///
/// @param[in] widget  The widget
/// @param[in] value  The floating-point value
/// @param[in] format  The text format; see QString::number() for the meaning 
/// @param[in] precision  The precision of the textual representation; see QString::number() for the meaning 
///
void set_text(gsl::not_null<QLineEdit*> widget, double value, char format = 'g', int precision = 6);

/// Append a new paragraph with text to the end of a text edit widget's existing text.
///
/// @param[in] widget  The text edit widget
/// @param[in] text  The text to append
///
void append(gsl::not_null<QTextEdit*> widget, std::wstring_view text);

/*! Convert the text in a line edit widget to a numeric value of type double.
    The rules of the conversion and the result value match those of QString::toDouble().
    \param[in] widget  The line edit widget
    \return  The result of the conversion
 */
[[nodiscard]] auto to_double(gsl::not_null<const QLineEdit*> widget) -> double;

/*! Convert the text in a line edit widget to a numeric value of type int.
    The rules of the conversion and the result value match those of QString::toInt().
    \param[in] widget  The line edit widget
    \return  The result of the conversion
 */
[[nodiscard]] auto to_int(gsl::not_null<const QLineEdit*> widget) -> int;

//! Convert the text in the line edit widget \p widget to a path.
[[nodiscard]] auto to_path(gsl::not_null<const QLineEdit*> widget) -> fs::path;

/// Delete the last line (if any) in a text edit widget. 
///
/// @param[in] widget  The text edit widget
///
void delete_last_line(gsl::not_null<QTextEdit*> widget);

}

#endif  


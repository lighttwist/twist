///  @file  qwt_histogram_utils.cpp
///  Implementation file for "qwt_histogram_utils.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "qwt_histogram_utils.hpp"

#pragma warning(disable: 4244) // Disable Qt warning
#pragma warning(disable: 4251)

#include <qwt_plot_layout.h>
#include <qwt_plot_canvas.h>
#include <qwt_plot_renderer.h>
#include <qwt_legend.h>
#include <qwt_legend_label.h>
#include <qwt_plot_grid.h>
#include <qwt_plot_histogram.h>
#include <qwt_column_symbol.h>
#include <qwt_series_data.h>

#include "twist/qt/qt_globals.hpp"

using namespace twist::math;
using gsl::not_null;

namespace twist::qt::qwt {

//
//  Local functions
//

std::tuple<double, double> get_value_range(not_null<const QwtSeriesData<QwtIntervalSample>*> data) 
{
	double min_val = std::numeric_limits<double>::max();
	double max_val = std::numeric_limits<double>::min();
	const auto size = data->size();
	for (int i = 0; i < size; ++i) {
		const auto val = data->sample(i).value;
		if (min_val > val) min_val = val;
		if (max_val < val) max_val = val;
	}
	return {min_val, max_val};
}


std::tuple<double, double> get_bin_range(not_null<const QwtSeriesData<QwtIntervalSample>*> data) 
{
	double min_val = std::numeric_limits<double>::max();
	double max_val = std::numeric_limits<double>::min();
	const auto size = data->size();
	for (int i = 0; i < size; ++i) {
		const auto& interval = data->sample(i).interval;
		if (min_val > interval.minValue()) min_val = interval.minValue();
		if (max_val < interval.maxValue()) max_val = interval.maxValue();
	}
	return {min_val, max_val};
}

//
//  HistogramPlotItem  class
//

const HistogramPlotItem::EqualBins HistogramPlotItem::equal_bins{};


void HistogramPlotItem::set_title(std::wstring_view title)
{
	setTitle(to_qstring(title));
}


void HistogramPlotItem::set_colour( const QColor &color )
{
    QColor c = color;
    c.setAlpha( 180 );
    setBrush( QBrush( c ) );
}


twist::math::RealInterval<double> HistogramPlotItem::value_range() const
{
	const auto [min_val, max_val] = get_value_range(not_null{data()});
	return {min_val, max_val};
}


twist::math::RealInterval<double> HistogramPlotItem::bin_range() const
{
	const auto [min_bin_bound, max_bin_bound] = get_bin_range(not_null{data()});
	return {min_bin_bound, max_bin_bound};
}

//
//void HistogramPlotItem::set_regular_values(const std::vector<double>& values, double first_bin_start, 
//		double bin_length)
//{
//	assert(bin_length > 0);
//
//	const auto value_count = size32(values);
//
//    QVector<QwtIntervalSample> samples(value_count);
//    
//	for (uint i = 0; i < value_count; ++i) {
//        QwtInterval interval{first_bin_start + bin_length * i, first_bin_start + bin_length * (i + 1)};
//        interval.setBorderFlags(QwtInterval::ExcludeMaximum);
//
//        samples[i] = QwtIntervalSample{values[i], interval};
//    }
//
//    setData(new QwtIntervalSeriesData{samples});
//}
//
//
//  HistogramPlotWindow  class
//

HistogramPlotWindow::HistogramPlotWindow(std::wstring_view title, QWidget* parent)
	: QwtPlot(parent)
{
    if (!title.empty()) setTitle(to_qstring(title));

    QwtPlotCanvas *canvas = new QwtPlotCanvas();  // This is the graph canvas, excluding the axes, labels, etc
    canvas->setPalette(Qt::white);
    // canvas->setBorderRadius( 5 );
    setCanvas(canvas);

    plotLayout()->setAlignCanvasToScales(true);

    QwtPlotGrid *grid = new QwtPlotGrid;
    grid->enableX( false );
    grid->enableY( true );
    grid->enableXMin( false );
    grid->enableYMin( false );
    grid->setMajorPen( Qt::black, 0, Qt::DotLine );
    grid->attach( this );

    setAutoReplot( true );
}


void HistogramPlotWindow::set_axis_titles(std::initializer_list<std::tuple<QwtPlot::Axis, std::wstring>> titles)
{
	for (const auto& [axis, title] : titles) {
		setAxisTitle(axis, to_qstring(title));
	}
}


void HistogramPlotWindow::insert_legend(QwtLegendData::Mode mode, LegendPosition pos)
{
	assert(!legend_);  // Multiple legends not yet supported

    legend_ = new QwtLegend;
    legend_->setDefaultItemMode(mode);
    insertLegend(legend_, pos);

	if (mode == QwtLegendData::Checkable) {
		connect(legend_, SIGNAL(checked(const QVariant&, bool, int)),
				SLOT(showItem(const QVariant&, bool)));
	}
	else {
		assert(false);
	}

	if (!plot_items_.empty()) set_up_legend();
}


void HistogramPlotWindow::add_plot_item(std::unique_ptr<HistogramPlotItem> item)
{
    item->attach(this);

	const auto& x_axis_range = axes_ranges_.add_axis_range(QwtPlot::xBottom, item->bin_range());
	setAxisScale(QwtPlot::xBottom, x_axis_range.lo(), x_axis_range.hi());

	const auto& y_axis_range = axes_ranges_.add_axis_range(QwtPlot::yLeft, item->value_range());
	setAxisScale(QwtPlot::yLeft, y_axis_range.lo(), y_axis_range.hi());

	plot_items_.push_back(not_null{ item.release() });
	if (legend_) set_up_legend();
}


void HistogramPlotWindow::set_up_legend()
{
	assert(legend_);
	assert(!plot_items_.empty());

    replot(); // creating the legend items

    QwtPlotItemList items = itemList(QwtPlotItem::Rtti_PlotHistogram);
    for (int i = 0; i < items.size(); ++i) {
        if (i == 0) {
            const auto item_info = itemToInfo(items[i]);

            auto legendLabel = qobject_cast<QwtLegendLabel*>(legend_->legendWidget(item_info));
            if (legendLabel) legendLabel->setChecked(true);
			
            items[i]->setVisible(true);
        }
        else {
            items[i]->setVisible(false);
        }
    }
}


void HistogramPlotWindow::export_plot()
{
    QwtPlotRenderer renderer;
    renderer.exportTo( this, "g:\\HistogramPlotWindow.pdf" );
}


void HistogramPlotWindow::set_mode( int mode )
{
    QwtPlotItemList items = itemList( QwtPlotItem::Rtti_PlotHistogram );

    for ( int i = 0; i < items.size(); i++ )
    {
        QwtPlotHistogram *histogram = static_cast<QwtPlotHistogram *>( items[i] );
        if ( mode < 3 )
        {
            histogram->setStyle( static_cast<QwtPlotHistogram::HistogramStyle>( mode ) );
            histogram->setSymbol( NULL );

            QPen pen( Qt::black, 0 );
            if ( mode == QwtPlotHistogram::Lines )
                pen.setBrush( histogram->brush() );

            histogram->setPen( pen );
        }
        else
        {
            histogram->setStyle( QwtPlotHistogram::Columns );

            QwtColumnSymbol *symbol = new QwtColumnSymbol( QwtColumnSymbol::Box );
            symbol->setFrameStyle( QwtColumnSymbol::Raised );
            symbol->setLineWidth( 2 );
            symbol->setPalette( QPalette( histogram->brush().color() ) );

            histogram->setSymbol( symbol );
        }
    }
}


void HistogramPlotWindow::showItem( const QVariant &itemInfo, bool on )
{
    QwtPlotItem *plotItem = infoToItem( itemInfo );
    if ( plotItem )
        plotItem->setVisible( on );
}

//
//  HistogramPlotWindow::AxesRanges  class
//

std::optional<HistogramPlotWindow::AxesRanges::Range> 
HistogramPlotWindow::AxesRanges::axis_range(QwtPlot::Axis axis) const
{
	const auto it = ranges_.find(axis);
	if (it == end(ranges_)) return {};
	return it->second;
}


HistogramPlotWindow::AxesRanges::Range 
HistogramPlotWindow::AxesRanges::add_axis_range(QwtPlot::Axis axis, HistogramPlotWindow::AxesRanges::Range new_range)
{
	const auto it = ranges_.find(axis);
	if (it == end(ranges_)) {
		ranges_.emplace(axis, new_range);
		return new_range;
	}

	const auto& old_range = it->second;
	const Range good_range{
			std::min(old_range.lo(), new_range.lo()),
			std::max(old_range.hi(), new_range.hi())};

	if (good_range != old_range) {
		ranges_[axis] = good_range;
		return good_range;
	}

	return old_range;
}

}

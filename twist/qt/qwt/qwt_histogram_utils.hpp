///  @file  qwt_histogram_utils.hpp
///  Utilities for working with Qtw histogram plots

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_QT_QTW_HISTOGRAM__UTILS_HPP
#define TWIST_QT_QTW_HISTOGRAM__UTILS_HPP

// Work in progress  Dan Ababei  7May'18

#pragma warning(push)
#pragma warning(disable: 4127)
#pragma warning(disable: 4505)
#pragma warning(disable: 4996)
#pragma warning(disable: 5054)  // Get rid of warnings inside QtGui/qpixelformat.h 

#include <qwt_plot.h>
#include <qwt_plot_histogram.h>

#include "twist/math/RealInterval.hpp"

class QwtLegend;

namespace twist::qt::qwt {

class HistogramPlotItem : public QwtPlotHistogram {
public:
	using EqualBins = std::integral_constant<int, 1>;

	static const EqualBins equal_bins;

    template<typename Range> 
	HistogramPlotItem(EqualBins, const Range& values, double first_bin_start, double bin_length);

    void set_colour(const QColor& colour);

    void set_title(std::wstring_view title);

	template<typename Range> 
	void set_regular_values();

	twist::math::RealInterval<double> value_range() const;

	twist::math::RealInterval<double> bin_range() const;
};


class HistogramPlotWindow : public QwtPlot {
public:
    HistogramPlotWindow(std::wstring_view title, QWidget* = NULL);

	void set_axis_titles(std::initializer_list<std::tuple<QwtPlot::Axis, std::wstring>> titles);

	void insert_legend(QwtLegendData::Mode mode = QwtLegendData::Checkable, LegendPosition pos = QwtPlot::RightLegend);

	void add_plot_item(std::unique_ptr<HistogramPlotItem> item);

    void set_mode( int );

    void export_plot();

private Q_SLOTS:
    void showItem( const QVariant &, bool on );

private:
	class AxesRanges {
	public:
		using Range = twist::math::RealInterval<double>;

		AxesRanges() = default;

		std::optional<Range> axis_range(QwtPlot::Axis axis) const;

		Range add_axis_range(QwtPlot::Axis axis, Range new_range);

		TWIST_NO_COPY_NO_MOVE(AxesRanges)

	private:
		std::map<QwtPlot::Axis, Range>  ranges_;
	};

	void set_up_legend();

	QwtLegend*  legend_{};
	std::vector<gsl::not_null<HistogramPlotItem*>>  plot_items_{}; 
	AxesRanges  axes_ranges_;

    Q_OBJECT
};

}

#include "qwt_histogram_utils.ipp"

#pragma warning(pop)

#endif  


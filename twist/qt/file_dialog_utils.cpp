/// @file file_dialog_utils.cpp
/// Implementation file for "file_dialog_utils.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/qt/file_dialog_utils.hpp"

#pragma warning(disable: 5054) // Get rid of warnings inside QtGui/qpixelformat.h 

#include "twist/qt/qt_globals.hpp"
#include "twist/qt/text_edit_utils.hpp"

#include <QFileDialog>
#include <QLineEdit>

namespace twist::qt {

auto do_select_file_dialog(QWidget* parent,
                           std::wstring_view caption,
                           std::wstring_view filter,
                           const fs::path& init_dir) -> fs::path
{
    const auto dir_path = QFileDialog::getOpenFileName(parent,
                                                       to_qstring(caption),
                                                       to_qstring(init_dir.wstring()),
                                                       to_qstring(filter),
                                                       nullptr/*selected_filter*/,
                                                       QFileDialog::DontResolveSymlinks);
	return to_path(dir_path).make_preferred();
}

auto do_select_dir_dialog(QWidget* parent, std::wstring_view caption, const fs::path& init_dir) -> fs::path
{
	const auto dir_path = QFileDialog::getExistingDirectory(parent, 
                                                            to_qstring(caption), 
                                                            to_qstring(init_dir.wstring()),
			                                                QFileDialog::ShowDirsOnly | 
                                                            QFileDialog::DontResolveSymlinks);
	return to_path(dir_path).make_preferred();
}

auto do_select_dir_dialog_to_widget(gsl::not_null<QLineEdit*> widget, QWidget* parent, std::wstring_view caption) 
      -> bool
{
	const auto init_dir_path = text(widget);
	const auto dir_path = do_select_dir_dialog(parent, caption, init_dir_path);
	if (dir_path.empty()) {
		return false;
	}
	set_text(widget, dir_path.c_str());
	return true;
}

}

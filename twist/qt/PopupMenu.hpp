///  @file  PopupMenu.hpp
///  PopupMenu class definition

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_QT_POPUP_MENU_HPP
#define TWIST_QT_POPUP_MENU_HPP

#include "twist/metaprogramming.hpp"

#pragma warning(push)
#pragma warning(disable: 5054)  // Get rid of warnings inside QtGui/qpixelformat.h 

#include <QMenu>

namespace twist::qt {

/// A popup menu, ie a menu containing several menu items each defined by a caption and an action.
class PopupMenu {
public:
	using ItemHandler = std::function<void()>; 

	/// Class storing information about a menu item: the item name, a handler for the "triggered" event and 
	/// a flag specifying whether the item (ie its underlying action) is enabled.
	class ItemInfo {
	public:
		ItemInfo(std::wstring name, ItemHandler handler, bool enabled = true);

		std::wstring name() const;
		
		ItemHandler handler() const;
		
		bool enabled() const;

	private:
		std::wstring  name_;
		ItemHandler  handler_;
		bool  enabled_;

		TWIST_CHECK_INVARIANT_DECL
	};

	/// Default constructor. Crates an empty popup menu.
	PopupMenu() = default;

	/// Constructor.
	///
	/// @param[in] items  The items which will appear in the popup menu, ordered as they are in this list
	///
	PopupMenu(std::initializer_list<ItemInfo> items_info);

	/// Whether the popup menu is empty, ie it contains no items.
	bool is_empty() const;

	/// Display (execute synchronously) the popup menu at a specific location on screen.
	///
	/// @param[in] global_pos  The position where the menu will be displayed, in global coordinates
	///
	void show(QPoint global_pos);

	/// Attach this menu to a specific "tool button". The button will then display a special arrow to indicate 
	/// that a menu is present. The menu is displayed when the arrow part of the button is pressed.
	///
	/// @param[in] button  The button
	///
	void set_menu_for(gsl::not_null<QToolButton*> button) const;

	/// Enable or disable a menu item (ie the action underlying one of the menu items), which is identified 
	/// by its name.
	///
	/// @param[in] name  The item (or action) name
	/// @param[in] enabled  Whether the item should be enabled (true) or disabled
	///
	void enable_item(std::wstring_view name, bool enabled);

	TWIST_NO_COPY_DEF_MOVE(PopupMenu)

private:
	std::unique_ptr<QMenu>  menu_;

	TWIST_CHECK_INVARIANT_DECL
};

}

#pragma warning(pop)

#endif

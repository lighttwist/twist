///  @file  ProgressProv.hpp
///  ProgressProv abstract class, inherits ProgFeedbackProv

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_PROGRESS_PROV_HPP
#define TWIST_PROGRESS_PROV_HPP

#include "Chronometer.hpp"
#include "ProgFeedbackProv.hpp"

namespace twist {

///
///  Abstract base class for visual progress providers that facilitate the indication of progress using a 
///    progress bar and/or progress messages. 
///  For the progress messages, two modes are available, "message mode" (a single progress message is 
///    displayed at any one time) and "history mode" (progress messages are listed in the order they are 
///    added, but it is also possible to overwrite the last message). Additionally, progress text can be 
///    output to a log file.
///
class ProgressProv : public ProgFeedbackProv {
public:
	/// The progress provider mode.
	enum Mode {
		k_message = 1, ///< Message mode: a single progress message is displayed		    
		k_history = 2  ///< History mode: all progress messages are listed in the order they are added    
	};

	/// Destructor.
	///
	virtual ~ProgressProv();

	/// Initialise the progress provider in "message" mode. The visual devices are guaranteed to be visible 
	/// after calling this method.
	///
	virtual void init_message_mode() = 0;
	
	/// Initialise the progress provider in "history" mode. The visual devices are guaranteed to be visible 
	/// after calling this method.
	///
	/// @param[in] expected_num_lines  The number of history lines you expect to be displayed in total. Some 
	///					implementations may resize the area showing the progress history lines accordingly. 
	///					Zero means you don't know.
	///
	virtual void init_hist_mode(long expected_num_lines = 0) = 0;

	/// Set the progress message.
	///
	/// @param[in] msg  The message.
	/// @param[in] force_update  Whether this method should try and force the UI to update
	///
	virtual void set_prog_msg(std::wstring_view msg, bool force_update = true) = 0;

	/// Add a new line to the progress history.
	///
	/// @param[in] hist_line  The history line.
	///
	virtual void add_new_hist_line(const std::wstring& hist_line) = 0;

	/// Set the "present" history line (i.e. the line that was last added to the history).
	/// If there are no lines yet in the progress history, a new one is added.
	///
	/// @param[in] hist_line  The history line.
	///
	virtual void set_present_hist_line(const std::wstring& hist_line) = 0;

	/// The visual devices will be hidden by this method, but this is not necessarily possible for all 
	/// implementations.
	///
    virtual void hide() = 0;

	/// Set the details of the timer output - this will determine how stop_timed_hist_line() will update the 
	/// present history line.
	///
	/// @param[in] unit  The timer output unit (default is "seconds")
	/// @param[in] precision  The timer output precision (the default is 2)
	///
	void set_timer_output(Chronometer::Unit unit, unsigned int precision); 

	/// Add a new history line and start the timer.
	/// A call to this method must be paired with a subsequent call to stop_timed_hist_line() . 
	///
	/// @param[in] text  The history line text.
	/// 
	void start_timed_hist_line(const std::wstring& text);

	/// Stop the timer and update the present history line with the elapsed time.
	/// A call to this method must be paired with a previous call to start_timed_hist_line(). 
	///
	void stop_timed_hist_line(); 

protected:
	/// Constructor. This constructs a progress provider that does not use a log file.
	///
	ProgressProv() = default;

private:	
	std::unique_ptr<Chronometer>  timer_{};
	std::wstring  timer_text_{};
	Chronometer::Unit  timer_unit_{ Chronometer::Unit::seconds };
	unsigned int  timer_precision_{ 2 };

	TWIST_CHECK_INVARIANT_DECL
};

} 

#endif 

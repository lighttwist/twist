//
//   Globals for the "twist::unn" sublib
//
//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//   following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//        following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//        following disclaimer in the documentation and/or other materials provided with the distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software without 
//        specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//   INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//   DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//   STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//   EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#ifndef TWIST_UNN_UNN__GLOBALS_H
#define TWIST_UNN_UNN__GLOBALS_H

#include "twist/ExplicitLibType.hpp"

namespace twist::unn {

// Unique library identifier. Useful for things such as explicit library types.
const unsigned long  twist_unn_lib_id = 41564588;

const unsigned long  typeid_node_id = 100;
const unsigned long  typeid_node_type = 101;
const unsigned long  typeid_arc_id = 102;

// Extension of the Uninet model main file 
static const wchar_t* unn_model_file_ext = L".uninet";

// Dependence structure node ID; unique within the owner dependence structure
typedef ExplicitLibType<long, 0, twist_unn_lib_id, typeid_node_id>  NodeId;

// Invalid dependence structure node ID
static const NodeId  no_node_id(0);

// Dependence structure node type 
typedef ExplicitLibType<long, 0, twist_unn_lib_id, typeid_node_type>  NodeType;
	extern const NodeType no_node_type;
	extern const NodeType bn_prob_node_type;
	extern const NodeType bn_func_node_type;
	extern const NodeType std_tree_node_type;
	extern const NodeType first_vine_tree_node_type;
	extern const NodeType hi_vine_tree_node_type;

// Dependence structure node ID; unique within the owner dependence structure
typedef ExplicitLibType<long, 0, twist_unn_lib_id, typeid_arc_id>  ArcId;

// Invalid dependence structure arc ID
static const ArcId  no_arc_id(0);

}

#endif

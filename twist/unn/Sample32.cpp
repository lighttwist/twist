//
//   Implementation file for "Sample32.hpp"
//
//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.
//

#include "twist/unn/pch.hpp"

#include "twist/unn/Sample32.hpp"

#include "twist/com/safearray_utils.hpp"
#include "twist/unn/unn_internals.hpp"

using namespace twist::com;

namespace twist::unn {

Sample32::Sample32()
	: com_(std::make_unique<Sample32Com>())
{
	TWIST_CHECK_INVARIANT
}


Sample32::Sample32(std::unique_ptr<Sample32Com> com)
	: com_(move(com))
{
	TWIST_CHECK_INVARIANT
}


Sample32::~Sample32()
{
	TWIST_CHECK_INVARIANT
}


std::vector<float> Sample32::get_to_array() const
{
	TWIST_CHECK_INVARIANT
	try {
		const auto array_var = (*com_)->getTo1DArray();
		std::vector<float> array;
		copy_from_1dlike_safearray<FLOAT, float>(array_var, back_inserter(array));
		return array;
	}
	TWIST_COM_RETHROW
}


void Sample32::sort()
{
	TWIST_CHECK_INVARIANT
	try {
		(*com_)->sort();
	}
	TWIST_COM_RETHROW
}


bool Sample32::is_sorted() const
{
	TWIST_CHECK_INVARIANT
	try {
		const auto ret = (*com_)->isSorted();
		return varbool_to_bool(ret);
	}
	TWIST_COM_RETHROW
}


double Sample32::get_quantile(double prob)
{
	TWIST_CHECK_INVARIANT
	try {
		return (*com_)->getQuantile(prob);
	}
	TWIST_COM_RETHROW
}


#ifdef _DEBUG
void Sample32::check_invariant() const noexcept
{
	assert(com_);
}
#endif

}

//
//   Implementation file for "BnNet.hpp"
//
//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.
//

#include "twist/unn/pch.hpp"

#include "twist/unn/BnNet.hpp"

#include "twist/com/com_utils.hpp"

#include "twist/unn/BnCondState.hpp"
#include "twist/unn/RandomVariable.hpp"
#include "twist/unn/unn_internals.hpp"

using namespace twist::com;
using namespace twist::unn::internals;

namespace twist::unn {

BnNet::BnNet(std::unique_ptr<BnNetCom> com)
	: com_(move(com))
{	
	try {
		bn_cond_state_.reset(new BnCondState(
				std::make_unique<BnCondStateCom>((*com_)->getCondState())));
	}
	TWIST_COM_RETHROW
	TWIST_CHECK_INVARIANT
}


BnNet::~BnNet()
{
	TWIST_CHECK_INVARIANT
}


void BnNet::add_prob_node(NodeId node_id, const RandomVariable& rand_var, const gfx::WorldPoint& world_pos)
{
	TWIST_CHECK_INVARIANT
	try {
		IUNNNodeSubstratePtr substrate;
		substrate.CreateInstance(__uuidof(UNNNodeSubstrate));
		substrate->initAsRandVar(rand_var.com_->interf());

		auto node = (*com_)->addNode(node_id.value(), NodeType_Unn(bn_prob_node_type.value()), 
				string_to_bstr(rand_var.name()), substrate);
		node->setWorldPos_script(world_pos.x(), world_pos.y());
	}
	TWIST_COM_RETHROW
}


void BnNet::add_arc(ArcId arc_id, NodeId parent_node_id, NodeId child_node_id)
{
	TWIST_CHECK_INVARIANT
	try {
		(*com_)->addNewArc(arc_id.value(), parent_node_id.value(), child_node_id.value());
	}
	TWIST_COM_RETHROW
}


const BnCondState& BnNet::cond_state() const
{
	TWIST_CHECK_INVARIANT
	return *bn_cond_state_;
}


BnCondState& BnNet::cond_state()
{
	TWIST_CHECK_INVARIANT
	return *bn_cond_state_;
}


#ifdef _DEBUG
void BnNet::check_invariant() const noexcept
{
	assert(com_);
	assert(bn_cond_state_);
}
#endif

}

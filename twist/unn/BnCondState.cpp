//
//   Implementation file for "BnCondState.hpp"
//
//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.
//

#include "twist/unn/pch.hpp"

#include "twist/unn/BnCondState.hpp"

#include "twist/unn/unn_internals.hpp"

using namespace twist::com;
using namespace twist::unn::internals;

namespace twist::unn {

BnCondState::BnCondState(std::unique_ptr<BnCondStateCom> com)
	: com_{move(com)}
{
	TWIST_CHECK_INVARIANT
}


BnCondState::~BnCondState()
{
	TWIST_CHECK_INVARIANT
}


void BnCondState::set_node_point_cond(std::wstring_view node_name, double point)
{
	TWIST_CHECK_INVARIANT
	try {
		RandVarCondCom rand_var_cond;
		rand_var_cond->initPointCond(point);
		(*com_)->setNodeCondByName(string_to_bstr(node_name), rand_var_cond.interf());
	}
	TWIST_COM_RETHROW
}


void BnCondState::reset_all_node_cond()
{
	TWIST_CHECK_INVARIANT
	try {
		(*com_)->resetAllNodeCond();
	}
	TWIST_COM_RETHROW
}


#ifdef _DEBUG
void BnCondState::check_invariant() const noexcept
{
	assert(com_);
}
#endif

}

///  @file  test_unn.cpp
///  Implementation file for "test_unn.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/unn/pch.hpp"

#include "twist/unn/test/test_unn.hpp"

#include "twist/FileVersionInfo.hpp"
#include "twist/OutTxtFileStream.hpp"
#include "twist/db/DelimValueFileReader.hpp"
#include "twist/unn/DistributionUtils.hpp"
#include "twist/unn/Engine.hpp"
#include "twist/unn/Model.hpp"
#include "twist/unn/RandVarBag.hpp"

using namespace twist;
using namespace twist::db;

namespace twist::unn::test {

// --- Local functions ---

using OutStream = OutTxtFileStream<char>;

std::unique_ptr<Engine> create_uninet_engine()
{
	static const auto engine_key = L"key-obtained-from-lighttwist";
	static const FileVersionInfo expected_version{3, 4, 5, 0};
	return create_engine(engine_key, expected_version);
}


std::vector<fs::path> break_combined_distr_file(const fs::path& in_file_path, 
		std::wstring_view out_file_prefix)
{
	DelimValueFileReader<char, DelimValueFileFormat::wspace_sep> reader{ with_header, in_file_path };
	const auto in_table = reader.read_data_into_string_table();

	std::vector<std::string> var_names( begin(reader.column_names()) + 1, end(reader.column_names()) );

	const auto num_vars = var_names.size();
	const auto num_rows = in_table->nof_rows();

	std::vector<fs::path> single_var_dis_paths(num_vars);
	
	// Preapare an output stream for each variable

	for (auto v = 0u; v < num_vars; ++v) {
		
		auto var_name = std::string{var_names[v]};
		var_name = trim_trail_char(var_name, '_');
		
		auto dis_files_dir_path = in_file_path.parent_path() / "dis_files";
		create_directory(dis_files_dir_path);
		auto path = dis_files_dir_path / (out_file_prefix + to_string(var_name));
		path.replace_extension(L".dis");
		single_var_dis_paths[v] = path;
		
		// Write the variable quantiles to the output stream
		OutStream var_out_stream{ std::make_unique<FileStd>(path, FileOpenMode::create_write) };
		for (auto i = 1; i < num_rows; ++i) {
			const auto quant_prob = in_table->get_cell(i, 0);
			const auto* quant_val = in_table->get_cell(i, v + 1);
			if (atof(quant_val) < 0) quant_val = "0";  // Negative quantile values are zapped to zero
			var_out_stream << quant_prob << '\t' << quant_val << '\n';
		}				
	}

	return single_var_dis_paths;
}

//
//  Global functions
//

void test_create_unn_engine()
{
	auto engine = create_uninet_engine();
}


void test_excalibur_distr_import()
{
	const auto ew_combined_dis_path = LR"(E:\junk\exca\output_combined\ew.dis)";
	const auto pw_combined_dis_path = LR"(E:\junk\exca\output_combined\pw.dis)";
//	const auto bn_model_file_path = LR"(E:\junk\exca\output_combined\excal_model.uninet)";

	const auto ew_single_var_dis_paths = break_combined_distr_file(ew_combined_dis_path, L"EW_");
	const auto pw_single_var_dis_paths = break_combined_distr_file(pw_combined_dis_path, L"PW_");

	DistributionUtils distr_utils;

	auto create_sample_files = [&distr_utils](const auto& single_var_dis_paths) {

		const auto sample_dir_name = L"sample";
		const auto sample_size = 1000;

		for (const auto& p : single_var_dis_paths) {
			
			const auto sample = distr_utils.make_sample_from_dis_file(p, sample_size);
			const auto sample_array = sample->get_to_array();

			const auto sample_dir_path = p.parent_path().parent_path() / sample_dir_name;
			create_directory(sample_dir_path);

			auto sample_file_path = sample_dir_path / p.stem();
			sample_file_path.replace_extension(".txt");

			OutStream sam_out_stream{ std::make_unique<FileStd>(sample_file_path, FileOpenMode::create_write) };
			for (auto s : sample_array) {
				sam_out_stream << std::to_string(s) << '\n';
			}
		}
	};

	create_sample_files(ew_single_var_dis_paths);
	create_sample_files(pw_single_var_dis_paths);

	//auto engine = create_uninet_engine();

	//// Create the new Uninet model
	//auto model = engine->create_new_udrv_model(bn_model_file_path);
	//auto& rand_var_bag = model->rand_var_bag();	
	//rand_var_bag.
		
}

}

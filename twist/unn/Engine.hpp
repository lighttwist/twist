//
//   Engine  class
//
//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.
//

#ifndef TWIST_UNN_ENGINE_HPP
#define TWIST_UNN_ENGINE_HPP

namespace twist {
class FileVersionInfo;
}

namespace twist::unn {
class EngineCom;
class Model;
}

namespace twist::unn {

class Engine {
public:
	Engine(std::wstring_view engine_key);

	~Engine();

	FileVersionInfo dll_version() const;

	/// Create a new model based on user-defined random variables. The newly created model becomes the 
	/// current model. 
	/// 
	/// @param[in] model_path  The model file path
	/// @return  The new model
	///
	std::unique_ptr<Model> create_new_udrv_model(const fs::path& model_path);

	/// Create a new model based on data mining. A sample file is first mined, then a new model is created. 
	/// The (correlated) random variables mined from the file are available in the random variable bag. 
	/// The new model becomes the current model.
	///
	/// @param[in] model_path  The model file path
	/// @param[in] sample_path  The sample file to be mined; this method decides what format it uses
	/// @return  The new model
	///
	std::unique_ptr<Model> create_dm_model(const fs::path& model_path, const fs::path& sample_path);

	/// Load an existing model from a file; the newly loaded model becomes the current model.
	///
	/// @param  model_path  [in] The model file path
	/// @return  The loaded model
	/// 
	std::unique_ptr<Model> load_model(const fs::path& model_path);

	/// Save the current model to the model file.
	/// 
	void save_model();

	TWIST_NO_COPY_NO_MOVE(Engine)

private:
	friend class DataMiner;
	friend class ModelSampler;

	std::unique_ptr<EngineCom>  com_;

	TWIST_CHECK_INVARIANT_DECL
};

//
//  Free functions
//

/// Get the version numbers of the UninetEngine library as a string.
///
/// @param[in] engine_key  The key to start Uninet's engine, as provided by Lighttwist Software
/// @return  the string
///
FileVersionInfo get_engine_dll_version();

/// Check whether the version of the imported UninetEngine library matches the expected version.
///
/// @param[in] expected_version  The expected library version
/// @return   0 - true if the versions match
///           1 - the error message (if there is a mismatch) 
///
std::tuple<bool, std::wstring> check_engine_dll_version(const FileVersionInfo& expected_version);

/// Check whether the version of a UninetEngine engine object matches the expected UninetEngine library version.
///
/// @param[in] engine  The engine
/// @param[in] expected_version  The expected library version
/// @return   0 - true if the versions match
///           1 - the error message (if there is a mismatch) 
///
std::tuple<bool, std::wstring> check_engine_dll_version(const Engine& engine, const FileVersionInfo& expected_version);

/// Create an instance of the Uninet engine (the main entry point for the UninetEngine library).
///
/// @param[in] engine_key  The key to start the engine, as supplied by LightTwist Software
/// @param[in] expected_version  The expected library version; an exception is thrown if the current library 
///					version is different
/// @return  The new engine instance 
///
std::unique_ptr<Engine> create_engine(std::wstring_view engine_key, const FileVersionInfo& expected_version);  

} 

#endif 

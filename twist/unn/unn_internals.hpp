//
//   Internal utilities for the "twist::unn" sublib
//
//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.
//

#ifndef TWIST_UNN_UNN__INTERNALS_HPP
#define TWIST_UNN_UNN__INTERNALS_HPP

#include "twist/com/ComWrapper.hpp"

#pragma warning(disable: 4471)

#import <C:\Program Files\UninetEngineLib64Bit\UninetEngine64.dll>  rename_namespace("twist::unn::internals")

#define TWIST_UNN_COM_WRAPPER_I_DECL(Class, Interface)  \
			class Class : public twist::com::ComWrapper<internals::Interface> {  \
			public:  \
				Class(internals::Interface* interf);  \
			};

#define TWIST_UNN_COM_WRAPPER_D_DECL(Class, Interface)  \
			class Class : public twist::com::ComWrapper<internals::Interface> {  \
			public:  \
				Class();  \
			};

#define TWIST_UNN_COM_WRAPPER_DI_DECL(Class, Interface)  \
			class Class : public twist::com::ComWrapper<internals::Interface> {  \
			public:  \
				Class();  \
				Class(internals::Interface* interf);  \
			};

namespace twist::unn {

TWIST_UNN_COM_WRAPPER_I_DECL(BnCondMemSampleInfoCom, IUNNCondBbnMemSampleInfo)
TWIST_UNN_COM_WRAPPER_I_DECL(BnCondStateCom, IUNNBbnCondState)
TWIST_UNN_COM_WRAPPER_I_DECL(BnMemSampleInfoCom, IUNNBbnMemSampleInfo)
TWIST_UNN_COM_WRAPPER_I_DECL(BnNetCom, IUNNBbnNetwork)
TWIST_UNN_COM_WRAPPER_I_DECL(BnPureMemSamplerCom, IUNNBbnPureMemSampler)
TWIST_UNN_COM_WRAPPER_I_DECL(CorrMatrixCatalogueCom, IUNNCorrMatrixCatalogue)
TWIST_UNN_COM_WRAPPER_I_DECL(DataMinerCom, IUNNDataMiner) 
TWIST_UNN_COM_WRAPPER_DI_DECL(DistributionCom, IUNNDistribution) 
TWIST_UNN_COM_WRAPPER_D_DECL(DistributionUtilsCom, IUNNDistributionUtils)
TWIST_UNN_COM_WRAPPER_D_DECL(EngineCom, IUNNEngine) 
TWIST_UNN_COM_WRAPPER_I_DECL(MarginalMemSampleInfoCom, IUNNMarginalMemSampleInfo)
TWIST_UNN_COM_WRAPPER_I_DECL(ModelCom, IUNNModel)
TWIST_UNN_COM_WRAPPER_I_DECL(ModelSamplerCom, IUNNModelSampler) 
TWIST_UNN_COM_WRAPPER_I_DECL(RandomVariableCom, IUNNRandomVariable)
TWIST_UNN_COM_WRAPPER_I_DECL(RandVarBagCom, IUNNRandVarBag)
TWIST_UNN_COM_WRAPPER_D_DECL(RandVarCondCom, IUNNRandVarCond)
TWIST_UNN_COM_WRAPPER_DI_DECL(Sample32Com, IUNNSample32)
TWIST_UNN_COM_WRAPPER_D_DECL(UnivarDistrSampleExaminerCom, IUNNUnivarDistrSampleExaminer)

}

#endif

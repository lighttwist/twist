//
//   BnPureMemSampler  class
//
//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.
//

#ifndef TWIST_UNN_BN_PURE_MEM_SAMPLER_HPP
#define TWIST_UNN_BN_PURE_MEM_SAMPLER_HPP

namespace twist::unn {

class BnCondMemSampleInfo;
class BnMemSampleInfo;
class BnPureMemSamplerCom;

class BnPureMemSampler {
public:
	~BnPureMemSampler();

	/// Create an unconditional sample of the entire joint distribution described by the BBN network.
	/// If there are any nodes in the BBN currently conditionalised on a value, this is ignored and the sample is 
	/// created as if no nodes were conditionalised.
	/// The entire sample is held in memory.
	/// The BBN product moment correlation matrix must be currently calculated.
	/// If the sampler is in "analytical conditioning" mode, this sample can be used later as the basis for one (or 
	/// more) conditional samples.
	///
	/// @param sample_size  [in] The number of samples to generate
	/// @param rand_seed  [in] If the sampler should use a fixed random seed, pass in the (strictly positive) random
	///                        seed value; if not, pass in zero
	/// @param txt_file_path  [in] If this is non-empty, a text sample file following the comma-separated value 
	///				               format is created at this path
	/// @return  BnMemSampleInfo or BnCondMemSampleInfo object holding the Bayesnet memory sample  
	///
    [[nodiscard]] auto create_uncond_sample(Ssize sample_size, long rand_seed, const fs::path& txt_file_path) 
	                    -> std::unique_ptr<BnMemSampleInfo>;

	/// Create a conditional sample of the entire joint distribution described by the BBN network, based on a 
	/// sample which was created previously.
	///
	/// @param[in] bn_sample_info  Interface to a UNNCondBbnMemSampleInfo object holding the already 
	///					existing BBN memory sample; it will be updated with the new BBN sample
	/// @param[in] txt_file_path  If this is non-empty, a text sample file following the "comma-separated 
	///					values" format is created at the location indicated by the path; otherwise no file is 
	///					created
	///
	void conditionalise_existing_sample(BnCondMemSampleInfo& bn_sample_info, 
			const fs::path& txt_file_path);

	TWIST_NO_COPY_NO_MOVE(BnPureMemSampler)

private:
	BnPureMemSampler(std::unique_ptr<BnPureMemSamplerCom> com);

	std::unique_ptr<BnPureMemSamplerCom>  com_;

	friend class ModelSampler;

	TWIST_CHECK_INVARIANT_DECL
};

} 

#endif 


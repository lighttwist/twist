//
//   DistributionUtils  class
//
//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.
//

#ifndef TWIST_UNN_DISTRIBUTION_UTILS_HPP
#define TWIST_UNN_DISTRIBUTION_UTILS_HPP

#include "Sample32.hpp"

namespace twist::unn {

class DistributionUtilsCom;

class DistributionUtils {
public:
	DistributionUtils();		

	~DistributionUtils();		
	
	/// Create a sample of specific size based on the infomation read from a "dis" (Excalibur format) file.
	///
	/// @param[in] path  The DIS filename
	/// @param[in] sample_size  The size that the newly created sample is to have
	/// @return  The sample; guaranteed to be sorted
	///
	std::unique_ptr<Sample32> make_sample_from_dis_file(const fs::path& path, unsigned int sample_size);

	TWIST_NO_COPY_NO_MOVE(DistributionUtils)

private:
	std::unique_ptr<DistributionUtilsCom>  com_;

	TWIST_CHECK_INVARIANT_DECL
};

} 

#endif 


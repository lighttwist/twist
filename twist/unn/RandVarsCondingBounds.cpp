//
//   Implementation file for "RandVarsCondingBounds.hpp"
//
//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//   following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//        following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//        following disclaimer in the documentation and/or other materials provided with the distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software without 
//        specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//   INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//   DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//   STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//   EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#include "twist/unn/pch.hpp"

#include "twist/unn/RandVarsCondingBounds.hpp"

namespace twist::unn {

//
//  RandVarsCondingBounds  class
//

RandVarsCondingBounds::RandVarsCondingBounds()
	: info_list_()
{
	TWIST_CHECK_INVARIANT
}


RandVarsCondingBounds::~RandVarsCondingBounds()
{
	TWIST_CHECK_INVARIANT
}


void RandVarsCondingBounds::add_info(const std::wstring& rand_var_name, double min_val, double max_val, 
		double perc01, double perc99)
{
	TWIST_CHECK_INVARIANT
	const auto result = info_list_.insert(
			std::make_pair(rand_var_name, RandVarInfo(min_val, max_val, perc01, perc99)));

	if (!result.second) {
		TWIST_THROW(L"The bounds information is already stored for random variable \"%s\".", rand_var_name.c_str());
	}
	TWIST_CHECK_INVARIANT
}


const RandVarsCondingBounds::RandVarInfo& RandVarsCondingBounds::get_info(const std::wstring& rand_var_name) const
{
	TWIST_CHECK_INVARIANT
	const auto it = info_list_.find(rand_var_name);
	if (it == end(info_list_)) {
		TWIST_THROW(L"Bounds information is nnot stored for random variable \"%s\".", rand_var_name.c_str());
	}
	return it->second;
}


#ifdef _DEBUG
void RandVarsCondingBounds::check_invariant() const noexcept
{
}
#endif 

//
//  RandVarsCondingBounds::RandVarInfo class
//

RandVarsCondingBounds::RandVarInfo::RandVarInfo(double min_val, double max_val, double perc01, double perc99)
	: min_val_(min_val)
	, max_val_(max_val)
	, perc01_(perc01)
	, perc99_(perc99)
{
	TWIST_CHECK_INVARIANT
}


double RandVarsCondingBounds::RandVarInfo::get_min_val() const
{
	TWIST_CHECK_INVARIANT
	return min_val_;
}


double RandVarsCondingBounds::RandVarInfo::get_max_val() const
{
	TWIST_CHECK_INVARIANT
	return max_val_;
}


double RandVarsCondingBounds::RandVarInfo::get_percentile_01() const
{
	TWIST_CHECK_INVARIANT
	return perc01_;
}


double RandVarsCondingBounds::RandVarInfo::get_percentile_99() const
{
	TWIST_CHECK_INVARIANT
	return perc99_;
}


#ifdef _DEBUG
void RandVarsCondingBounds::RandVarInfo::check_invariant() const noexcept
{
	assert(min_val_ <= perc01_);
	assert(perc01_ <= perc99_);
	assert(perc99_ <= max_val_);
}
#endif 

} 

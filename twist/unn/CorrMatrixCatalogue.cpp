//
//   Implementation file for "CorrMatrixCatalogue.hpp"
//
//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.
//

#include "twist/unn/pch.hpp"

#include "twist/unn/CorrMatrixCatalogue.hpp"

#include "twist/unn/unn_internals.hpp"

using namespace twist::com;
using namespace twist::unn::internals;

namespace twist::unn {

CorrMatrixCatalogue::CorrMatrixCatalogue(std::unique_ptr<CorrMatrixCatalogueCom> com)
	: com_(move(com))
{
	TWIST_CHECK_INVARIANT
}


CorrMatrixCatalogue::~CorrMatrixCatalogue()
{
	TWIST_CHECK_INVARIANT
}


bool CorrMatrixCatalogue::is_calculated(CorrMatrixType type) const
{
	TWIST_CHECK_INVARIANT
	try {
		const auto ret = (*com_)->corrMatrixIsCalculated(static_cast<CorrMatrixType_Unn>(type));
		return varbool_to_bool(ret);
	}
	TWIST_COM_RETHROW
}


void CorrMatrixCatalogue::calculate_bn_prmom_matrix()
{
	TWIST_CHECK_INVARIANT
	try {
		(*com_)->calculateBbnPrmomMatrix();
	}
	TWIST_COM_RETHROW
}


void CorrMatrixCatalogue::calculate_full_emp_norm_prmom_matrix()
{
	TWIST_CHECK_INVARIANT
	try {
		(*com_)->calculateFullEmpNormPrmomMatrix();
	}
	TWIST_COM_RETHROW
}


void CorrMatrixCatalogue::calculate_emp_norm_prmom_matrix()
{
	TWIST_CHECK_INVARIANT
	try {
		(*com_)->calculateEmpNormPrmomMatrix();
	}
	TWIST_COM_RETHROW
}


void CorrMatrixCatalogue::calculate_full_emp_rank_matrix()
{
}


#ifdef _DEBUG
void CorrMatrixCatalogue::check_invariant() const noexcept
{
	assert(com_);
}
#endif

}

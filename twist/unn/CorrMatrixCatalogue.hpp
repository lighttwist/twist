//
//   CorrMatrixCatalogue  class
//
//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.
//

#ifndef TWIST_UNN_CORR_MATRIX_CATALOGUE_HPP
#define TWIST_UNN_CORR_MATRIX_CATALOGUE_HPP

namespace twist::unn {

class CorrMatrixCatalogueCom;

/// Correlation matrix types
enum class CorrMatrixType {
	/// Product moment correlation matrix, as computed from the BBN graph.
	/// The order of the elements matches the node order in the BBN.
    prmom_bn = 0,  
	/// The empirical normal product moment correlation matrix, as computed from a sample being data-mined.
	/// The matrix only contains elements for those random variables currently used in the BBN.
	/// The order of the elements matches the node order in the BBN.
    prmom_emp_norm = 1,  
    /// The empirical rank correlation matrix, as computed from a sample being data-mined. 
	/// The matrix only contains elements for those random variables currently used in the BBN.
	/// The order of the elements matches the node order in the BBN.
    rank_emp = 2,  
	/// The empirical normal product moment correlation matrix, as computed from a sample being data-mined.
	/// The matrix contains elements for all random variables in the data-mined sample.
	/// The order of the elements matches the order of the random variables in the random variable cellar.
    prmom_full_emp_norm = 3,  
    /// The empirical rank correlation matrix, as computed from a sample being data-mined. 
	/// The matrix contains elements for all random variables in the data-mined sample.
	/// The order of the elements matches the order of the random variables in the random variable cellar.
    rank_full_emp = 4
};


class CorrMatrixCatalogue {
public:
	~CorrMatrixCatalogue();

    /// Find out whether a model correlation matrix is currently calculated.
    ///
    /// @param  type  [in] The correlation matrix type
    /// @return  true if it does
    ///
	bool is_calculated(CorrMatrixType type) const;

    /// Calculate the BBN product moment correlation matrix. A COM error occurs if the correlation matrix is 
    /// already calculated.
    void calculate_bn_prmom_matrix();

    /// Calculate the empirical normal product moment correlation matrix of the sample formed by all random 
	/// variables in the random variable bag (data-mining mode). A COM error occurs if the correlation matrix 
	/// is already calculated.
	void calculate_full_emp_norm_prmom_matrix();
	
	void calculate_emp_norm_prmom_matrix();

	void calculate_full_emp_rank_matrix();

	TWIST_NO_COPY_NO_MOVE(CorrMatrixCatalogue)

private:
	friend class Model;

	CorrMatrixCatalogue(std::unique_ptr<CorrMatrixCatalogueCom> com);

	std::unique_ptr<CorrMatrixCatalogueCom>  com_;

	TWIST_CHECK_INVARIANT_DECL
};

} 

#endif 


//
//   Implementation file for "DistributionUtils.hpp"
//
//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.
//

#include "twist/unn/pch.hpp"

#include "twist/unn/DistributionUtils.hpp"

#include "twist/unn/unn_internals.hpp"

using namespace twist::com;

namespace twist::unn {

DistributionUtils::DistributionUtils()
	: com_{std::make_unique<DistributionUtilsCom>()}
{
	TWIST_CHECK_INVARIANT
}


DistributionUtils::~DistributionUtils()
{
	TWIST_CHECK_INVARIANT
}


std::unique_ptr<Sample32> DistributionUtils::make_sample_from_dis_file(const fs::path& path, 
		unsigned int sample_size)
{
	TWIST_CHECK_INVARIANT
	auto isample = (*com_)->makeSampleFromDisFile(path.c_str(), sample_size);
	auto com_sample = std::unique_ptr<Sample32Com>{new Sample32Com{isample}};
	return std::unique_ptr<Sample32>{new Sample32{move(com_sample)}};
}


#ifdef _DEBUG
void DistributionUtils::check_invariant() const noexcept
{
	assert(com_);
}
#endif

}

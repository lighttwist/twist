//
//   Implementation file for "BnPureMemSampler.hpp"
//
//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.
//

#include "twist/unn/pch.hpp"

#include "twist/unn/BnPureMemSampler.hpp"

#include "twist/unn/BnCondMemSampleInfo.hpp"
#include "twist/unn/BnMemSampleInfo.hpp"
#include "twist/unn/unn_internals.hpp"

using namespace twist::com;
using namespace twist::unn::internals;

namespace twist::unn {

BnPureMemSampler::BnPureMemSampler(std::unique_ptr<BnPureMemSamplerCom> com)
	: com_{move(com)}
{
	TWIST_CHECK_INVARIANT
}

BnPureMemSampler::~BnPureMemSampler()
{
	TWIST_CHECK_INVARIANT
}

auto BnPureMemSampler::create_uncond_sample(Ssize sample_size, long rand_seed, const fs::path& txt_file_path) 
      -> std::unique_ptr<BnMemSampleInfo>
{
	TWIST_CHECK_INVARIANT
	try {
		auto isample_info = (*com_)->createUncondSample(static_cast<long>(sample_size), 
		                                                rand_seed, 
		                                                path_to_bstr(txt_file_path));

		auto icond_sample_info = IUNNCondBbnMemSampleInfoPtr{};	
		isample_info->QueryInterface<IUNNCondBbnMemSampleInfo>(&icond_sample_info);

		if (icond_sample_info) {
			return std::unique_ptr<BnMemSampleInfo>{ 
					new BnCondMemSampleInfo{ std::make_unique<BnCondMemSampleInfoCom>(icond_sample_info) } };
		}
		return std::unique_ptr<BnMemSampleInfo>{ 
				new BnMemSampleInfo{std::make_unique<BnMemSampleInfoCom>(isample_info)} };
	}
	TWIST_COM_RETHROW
}

void BnPureMemSampler::conditionalise_existing_sample(BnCondMemSampleInfo& bn_sample_info, 
                                                      const fs::path& txt_file_path)
{
	TWIST_CHECK_INVARIANT
	try {
		(*com_)->conditionaliseExistingSample(bn_sample_info.com_->interf(), path_to_bstr(txt_file_path));
		//+TODO: does the above throw on error or do we need to check the returned HRESULT?
	}
	TWIST_COM_RETHROW
}

#ifdef _DEBUG
void BnPureMemSampler::check_invariant() const noexcept
{
	assert(com_);
}
#endif

}

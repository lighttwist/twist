//
//   Implementation file for "MarginalMemSampleInfo.hpp"
//
//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.
//

#include "twist/unn/pch.hpp"

#include "twist/unn/MarginalMemSampleInfo.hpp"

#include "twist/unn/Sample32.hpp"
#include "twist/unn/unn_internals.hpp"

using namespace twist::com;

namespace twist::unn {

MarginalMemSampleInfo::MarginalMemSampleInfo(std::unique_ptr<MarginalMemSampleInfoCom> com)
	: com_{move(com)}
{
	TWIST_CHECK_INVARIANT
}


MarginalMemSampleInfo::~MarginalMemSampleInfo()
{
	TWIST_CHECK_INVARIANT
}


bool MarginalMemSampleInfo::sample_exists() const
{
	TWIST_CHECK_INVARIANT
	try {
		const auto ret = (*com_)->sampleExists();
		return varbool_to_bool(ret);
	}
	TWIST_COM_RETHROW
}


Sample32 MarginalMemSampleInfo::retrieve_sample() const
{
	TWIST_CHECK_INVARIANT
	try {
		const auto interf = (*com_)->retrieveSample();
		return Sample32{std::make_unique<Sample32Com>(interf)};
	}
	TWIST_COM_RETHROW
}


#ifdef _DEBUG
void MarginalMemSampleInfo::check_invariant() const noexcept
{
	assert(com_);
}
#endif

}

//
//   BnCondState  class
//
//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.
//

#ifndef TWIST_UNN_BN_COND_STATE_HPP
#define TWIST_UNN_BN_COND_STATE_HPP

namespace twist::unn {

class BnCondStateCom;

class BnCondState {
public:
	virtual ~BnCondState();

	/// Set the conditioning of a Bayesnet node to a point; any previous conditioning for this node is reset.
	/// 
	/// @param[in] nodeName  The node name
	/// @param[in] point  The conditioning point value; it should fall within the support of the node's 
    ///					distribution, if the underlying distribution is continuous, or be equal to a state's 
	///					value, if the distribution is discrete
	///
	void set_node_point_cond(std::wstring_view node_name, double point);

	/// Reset the conditioning of all nodes which are currently conditionalised. 
	void reset_all_node_cond();

	TWIST_NO_COPY_NO_MOVE(BnCondState)

private:
	BnCondState(std::unique_ptr<BnCondStateCom> com);

	std::unique_ptr<BnCondStateCom>  com_;

	friend class BnNet;

	TWIST_CHECK_INVARIANT_DECL
};

} 

#endif 


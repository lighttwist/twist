//
//   Sample32  class
//
//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.
//

#ifndef TWIST_UNN_SAMPLE32_H
#define TWIST_UNN_SAMPLE32_H

namespace twist::unn {

class Sample32Com;

class Sample32 {
public:
	Sample32();

	~Sample32();

	std::vector<float> get_to_array() const;

    /// Sort the sample.
    void sort();
    
    /// Whether the sample is sorted.
    bool is_sorted() const;

	/// Calculate a specific quantile. The sample is expected to be sorted.
	///
	/// @param  prob  [in] Probability realised by the returned quantile
	/// @param  quant  [out,retval] The quantile
	///
	double get_quantile(double prob);

	TWIST_NO_COPY_NO_MOVE(Sample32)

private:
	explicit Sample32(std::unique_ptr<Sample32Com> com);

	std::unique_ptr<Sample32Com>  com_;

	friend class DistributionUtils;
	friend class MarginalMemSampleInfo;
	friend class RandVarBag;
	friend class UnivarDistrSampleExaminer;

	TWIST_CHECK_INVARIANT_DECL
};

} 

#endif 


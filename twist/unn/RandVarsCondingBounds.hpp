//
//   RandVarsCondingBounds  class
//
//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//   following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//        following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//        following disclaimer in the documentation and/or other materials provided with the distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software without 
//        specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//   INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//   DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//   STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//   EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#ifndef TWIST_UNN_RAND_VARS_CONDING_BOUNDS_H
#define TWIST_UNN_RAND_VARS_CONDING_BOUNDS_H

namespace twist::unn {

//+TODO: Add comments

///
///  Stores information about the acceptable conditioning bounds of a number of random variables
///
class RandVarsCondingBounds {
public:
	class RandVarInfo {
	public:
		/// Constructor.
		///
		RandVarInfo(double min_val, double max_val, double perc01, double perc99);

		double get_min_val() const;
		double get_max_val() const; 
		double get_percentile_01() const;
		double get_percentile_99() const;

	private:
		double  min_val_;
		double  max_val_; 
		double  perc01_;
		double  perc99_;

		TWIST_CHECK_INVARIANT_DECL
	};

	/// Constructor.
	///
	RandVarsCondingBounds();
	
	/// Destructor.
	///
	virtual ~RandVarsCondingBounds();

	///+ Throws an exception if the bounds info for this node is already set
	void add_info(const std::wstring& rand_var_name, double min_val, double max_val, 
			double perc01, double perc99);

	//+
	const RandVarInfo& get_info(const std::wstring& rand_var_name) const;

private:	
	std::map<std::wstring, RandVarInfo>  info_list_;

	TWIST_CHECK_INVARIANT_DECL
	TWIST_NO_COPY_NO_MOVE(RandVarsCondingBounds)
};

} 

#endif 

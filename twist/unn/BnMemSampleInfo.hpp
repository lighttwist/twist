//
//   BnMemSampleInfo  class
//
//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.
//

#ifndef TWIST_UNN_BN_MEM_SAMPLE_INFO_HPP
#define TWIST_UNN_BN_MEM_SAMPLE_INFO_HPP

namespace twist::unn {

class BnMemSampleInfoCom;
class MarginalMemSampleInfo;

class BnMemSampleInfo {
public:
	virtual ~BnMemSampleInfo();

	/// The memory sample size (ie the number of sample rows in the sample).
	int sample_size() const;

	/// Get info sample about a marginal random variable which has been sampled.
	///
	/// @param  randVarName  [in] The random variable name; an exception is thrown if no sample information is 
	///				stored for it; matching is case-insensitive
	/// @return  The sample info
	///
	MarginalMemSampleInfo get_rand_var_sample_info(std::wstring_view rand_var_name) const;

	TWIST_NO_COPY_NO_MOVE(BnMemSampleInfo)

private:
	BnMemSampleInfo(std::unique_ptr<BnMemSampleInfoCom> com);

	std::unique_ptr<BnMemSampleInfoCom>  com_;

	friend class BnCondMemSampleInfo;
	friend class BnPureMemSampler;

	TWIST_CHECK_INVARIANT_DECL
};

} 

#endif 


//
//   Implementation file for "unn_internals.hpp"
//
//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.
//

#include "twist/unn/pch.hpp"

#include "twist/unn/unn_internals.hpp"

using namespace twist::unn::internals;

namespace twist::unn {

BnCondMemSampleInfoCom::BnCondMemSampleInfoCom(IUNNCondBbnMemSampleInfo* interf) : ComWrapper{ interf } {}

BnCondStateCom::BnCondStateCom(IUNNBbnCondState* interf) : ComWrapper{ interf } {}

BnMemSampleInfoCom::BnMemSampleInfoCom(IUNNBbnMemSampleInfo* interf) : ComWrapper{ interf } {}

BnNetCom::BnNetCom(IUNNBbnNetwork* interf) : ComWrapper{ interf } {}

BnPureMemSamplerCom::BnPureMemSamplerCom(IUNNBbnPureMemSampler* interf) : ComWrapper{ interf } {}

CorrMatrixCatalogueCom::CorrMatrixCatalogueCom(IUNNCorrMatrixCatalogue* interf) : ComWrapper{ interf } {}

DataMinerCom::DataMinerCom(IUNNDataMiner* interf) : ComWrapper{ interf } {}

DistributionCom::DistributionCom(IUNNDistribution* interf) : ComWrapper{ interf } {}
DistributionCom::DistributionCom() : ComWrapper{ __uuidof(UNNDistribution) } {}

DistributionUtilsCom::DistributionUtilsCom() : ComWrapper{ __uuidof(UNNDistributionUtils) } {}

EngineCom::EngineCom() : ComWrapper{ __uuidof(UNNEngine) } {}

MarginalMemSampleInfoCom::MarginalMemSampleInfoCom(IUNNMarginalMemSampleInfo* interf) : ComWrapper{ interf } {}

ModelCom::ModelCom(IUNNModel* interf) : ComWrapper{ interf } {}

ModelSamplerCom::ModelSamplerCom(IUNNModelSampler* interf) : ComWrapper{ interf } {}

RandomVariableCom::RandomVariableCom(IUNNRandomVariable* interf) : ComWrapper{ interf } {}

RandVarBagCom::RandVarBagCom(IUNNRandVarBag* interf) : ComWrapper{ interf } {}

RandVarCondCom::RandVarCondCom() : ComWrapper{ __uuidof(UNNRandVarCond) } {}

Sample32Com::Sample32Com() : ComWrapper{ __uuidof(UNNSample32) } {}
Sample32Com::Sample32Com(IUNNSample32* interf) : ComWrapper{ interf } {}

UnivarDistrSampleExaminerCom::UnivarDistrSampleExaminerCom() 
	: ComWrapper{__uuidof(UNNUnivarDistrSampleExaminer)} {}



}


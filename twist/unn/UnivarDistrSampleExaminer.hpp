//
//   UnivarDistrSampleExaminer  class
//
//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//   following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//        following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//        following disclaimer in the documentation and/or other materials provided with the distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software without 
//        specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//   INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//   DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//   STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//   EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#ifndef TWIST_UNN_UNIVAR_DISTR_SAMPLE_EXAMINER_H
#define TWIST_UNN_UNIVAR_DISTR_SAMPLE_EXAMINER_H

namespace twist::unn {

class Distribution;
class Sample32;
class UnivarDistrSampleExaminerCom;

class UnivarDistrSampleExaminer {
public:
	UnivarDistrSampleExaminer(const Sample32& sample);

	~UnivarDistrSampleExaminer();

	/*! Check whether the sample comes from a discrete distribution.
	    \param[in] max_discrete_states  The maximum number of states which the discrete distribution could have; if the 
                                        sample contains more distinct values than this number, it is considered to have 
                                        come from a continuous distribution
	    \return  The discrete distribution from which this sample is drawn, or nullptr if the sample is considered to 
                 have come from a continuous distribution
	 */
    [[nodiscard]] auto check_for_discrete(int max_discrete_states) const -> std::unique_ptr<Distribution>;

	TWIST_NO_COPY_NO_MOVE(UnivarDistrSampleExaminer)

private:
	std::unique_ptr<UnivarDistrSampleExaminerCom>  com_;

	TWIST_CHECK_INVARIANT_DECL
};

} 

#endif 


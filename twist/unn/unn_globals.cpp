//
//   Implementation file for "unn_globals.hpp"
//
//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//   following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//        following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//        following disclaimer in the documentation and/or other materials provided with the distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software without 
//        specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//   INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//   DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//   STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//   EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#include "twist/unn/pch.hpp"

#include "twist/unn/unn_globals.hpp"

#include "twist/unn/unn_internals.hpp"

using namespace twist::unn::internals;

namespace twist::unn {

const NodeType no_node_type{UNN_NO_NODE_TYPE};
const NodeType bn_prob_node_type{UNN_BBN_PROB_NODE};
const NodeType bn_func_node_type{UNN_BBN_FUNC_NODE};
const NodeType std_tree_node_type{UNN_STD_TREE_NODE_TREE};
const NodeType first_vine_tree_node_type{UNN_VINE_FIRST_TREE_NODE};
const NodeType hi_vine_tree_node_type{UNN_VINE_HI_TREE_NODE};

}


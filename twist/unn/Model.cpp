//
//   Implementation file for "Model.hpp"
//
//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.
//

#include "twist/unn/pch.hpp"

#include "twist/unn/Model.hpp"

#include "twist/unn/BnNet.hpp"
#include "twist/unn/CorrMatrixCatalogue.hpp"
#include "twist/unn/RandVarBag.hpp"
#include "twist/unn/unn_internals.hpp"

using namespace twist::com;

namespace twist::unn {

Model::Model(std::unique_ptr<ModelCom> com)
	: com_(move(com))
{
	try {
		rand_var_bag_.reset(new RandVarBag(
				std::make_unique<RandVarBagCom>((*com_)->getRandVarBag())));

		bn_net_.reset(new BnNet(
				std::make_unique<BnNetCom>((*com_)->getBbnMotor()->getBbnNetwork())));

		corr_matrix_catalogue_.reset(new CorrMatrixCatalogue(
				std::make_unique<CorrMatrixCatalogueCom>((*com_)->getCorrMatrixCatalogue())));
	}
	TWIST_COM_RETHROW
	TWIST_CHECK_INVARIANT
}


Model::~Model()
{
	TWIST_CHECK_INVARIANT
}


const RandVarBag& Model::rand_var_bag() const
{
	TWIST_CHECK_INVARIANT
	return *rand_var_bag_;
}


RandVarBag& Model::rand_var_bag()
{
	TWIST_CHECK_INVARIANT
	return *rand_var_bag_;
}


const CorrMatrixCatalogue& Model::corr_matrix_catalogue() const
{
	TWIST_CHECK_INVARIANT	
	return *corr_matrix_catalogue_;
}


CorrMatrixCatalogue& Model::corr_matrix_catalogue() 
{
	TWIST_CHECK_INVARIANT	
	return *corr_matrix_catalogue_;
}


const BnNet& Model::bn_net() const
{
	TWIST_CHECK_INVARIANT
	return *bn_net_;
}


BnNet& Model::bn_net()
{
	TWIST_CHECK_INVARIANT
	return *bn_net_;
}


#ifdef _DEBUG
void Model::check_invariant() const noexcept
{
	assert(com_);
	assert(rand_var_bag_);
	assert(corr_matrix_catalogue_);
	assert(bn_net_);
}
#endif

}

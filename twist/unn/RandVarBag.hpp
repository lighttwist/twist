//
//   RandVarBag  class
//
//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//   following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//        following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//        following disclaimer in the documentation and/or other materials provided with the distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software without 
//        specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//   INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//   DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//   STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//   EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#ifndef TWIST_UNN_RAND_VAR_BAG_HPP
#define TWIST_UNN_RAND_VAR_BAG_HPP

namespace twist::unn {

class Distribution;
class RandomVariable;
class RandVarBagCom;
class Sample32;

class RandVarBag {
public:
	~RandVarBag();

	std::unique_ptr<RandomVariable> get_rand_var(std::wstring_view name) const;

	std::unique_ptr<Sample32> read_rand_var_sample(std::wstring_view name) const;

	void set_rand_var_parametric_distribution(std::wstring_view name, const Distribution& distr);

	TWIST_NO_COPY_NO_MOVE(RandVarBag)

private:
	friend class Model;

	RandVarBag(std::unique_ptr<RandVarBagCom> com);

	std::unique_ptr<RandVarBagCom>  com_;

	TWIST_CHECK_INVARIANT_DECL
};

} 

#endif 


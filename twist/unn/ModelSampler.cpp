//
//   Implementation file for "ModelSampler.hpp"
//
//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.
//

#include "twist/unn/pch.hpp"

#include "twist/unn/ModelSampler.hpp"

#include "twist/unn/BnPureMemSampler.hpp"
#include "twist/unn/Engine.hpp"
#include "twist/unn/Model.hpp"
#include "twist/unn/unn_internals.hpp"

using namespace twist::com;

namespace twist::unn {

ModelSampler::ModelSampler(const Model& model, const Engine& engine, 
		const fs::path& modular_sample_dir_path)
	: com_{}
	, model_{model}
{
	try {
		com_ = std::make_unique<ModelSamplerCom>(
				(*engine.com_)->getClassFactory()->createModelSampler(
						model.com_->interf(), path_to_bstr(modular_sample_dir_path)));	
	}
	TWIST_COM_RETHROW
	TWIST_CHECK_INVARIANT
}


ModelSampler::~ModelSampler()
{
	TWIST_CHECK_INVARIANT
}


 std::unique_ptr<BnPureMemSampler> ModelSampler::create_bn_pure_mem_sampler(bool anly_cond)
 {
	TWIST_CHECK_INVARIANT
	try {
		auto interf = (*com_)->createBbnPureMemSampler(to_varbool(anly_cond));	
		return std::unique_ptr<BnPureMemSampler>{
				new BnPureMemSampler{std::make_unique<BnPureMemSamplerCom>(interf)} };
	}
	TWIST_COM_RETHROW 
 }


#ifdef _DEBUG
void ModelSampler::check_invariant() const noexcept
{
	assert(com_);
}
#endif

}

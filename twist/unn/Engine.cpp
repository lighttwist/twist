//
//   Implementation file for "Engine.hpp"
//
//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.
//

#include "twist/unn/pch.hpp"

#include "twist/unn/Engine.hpp"

#include "twist/FileVersionInfo.hpp"
#include "twist/com/com_utils.hpp"

#include "twist/unn/Model.hpp"
#include "twist/unn/unn_internals.hpp"

using namespace twist::com;

namespace twist::unn {

Engine::Engine(std::wstring_view engine_key)
	: com_(std::make_unique<EngineCom>())
{
	try {
		(*com_)->start(string_to_bstr(engine_key), 0);
		TWIST_CHECK_INVARIANT
	}
	TWIST_COM_RETHROW
}


Engine::~Engine()
{
	TWIST_CHECK_INVARIANT
}


FileVersionInfo Engine::dll_version() const
{
	try {
		long major = 0;
		long minor = 0;
		long release = 0;
		long build = 0;
		(*com_)->getDllVersion(&major, &minor, &release, &build);
		return {major, minor, release, build};
	}
	TWIST_COM_RETHROW
}


std::unique_ptr<Model> Engine::create_new_udrv_model(const fs::path& model_path)
{
	TWIST_CHECK_INVARIANT
	try {
		auto imodel = (*com_)->createNewUdrvModel(path_to_bstr(model_path));
		return std::unique_ptr<Model>{
				new Model(std::make_unique<ModelCom>(imodel))};
	}
	TWIST_COM_RETHROW
}


std::unique_ptr<Model> Engine::create_dm_model(const fs::path& model_path, const fs::path& sample_path)
{
	TWIST_CHECK_INVARIANT
	try {
		auto imodel = (*com_)->createNewDmModel(
				path_to_bstr(model_path), path_to_bstr(sample_path));
		return std::unique_ptr<Model>{
				new Model(std::make_unique<ModelCom>(imodel))};
	}
	TWIST_COM_RETHROW
}


std::unique_ptr<Model> Engine::load_model(const fs::path& model_path)
{
	TWIST_CHECK_INVARIANT
	try {
		auto imodel = (*com_)->loadModel(path_to_bstr(model_path));
		return std::unique_ptr<Model>{
				new Model(std::make_unique<ModelCom>(imodel))};
	}
	TWIST_COM_RETHROW
}


void Engine::save_model()
{
	TWIST_CHECK_INVARIANT
	try {
		(*com_)->saveModel();
	}
	TWIST_COM_RETHROW
}


#ifdef _DEBUG
void Engine::check_invariant() const noexcept
{
	assert(com_);
}
#endif

//
//  Local functions
//

std::tuple<bool, std::wstring> compare_engine_dll_versions(const FileVersionInfo& dll_version, 
		const FileVersionInfo& expected_version)
{
	if (dll_version != expected_version) {
		const auto err = format_str(L"Wrong UninetEngine library version %s.\nExpected version is %s.", 
				to_str(dll_version).c_str(), to_str(expected_version).c_str()); 
		return {false, err};
	}
	return {true, {}};
}

//
//  Free functions
//

FileVersionInfo get_engine_dll_version()
{
	long major = 0;
	long minor = 0;
	long release = 0;
	long build = 0;
	EngineCom com;
	com->getDllVersion(&major, &minor, &release, &build);
	return {major, minor, release, build};
}


std::tuple<bool, std::wstring> check_engine_dll_version(const FileVersionInfo& expected_version)
{
	return compare_engine_dll_versions(get_engine_dll_version(), expected_version);
}


std::tuple<bool, std::wstring> check_engine_dll_version(const Engine& engine, const FileVersionInfo& expected_version)
{
	return compare_engine_dll_versions(engine.dll_version(), expected_version);
}


std::unique_ptr<Engine> create_engine(std::wstring_view engine_key, const FileVersionInfo& expected_version)
{
	try {
		auto engine = std::make_unique<Engine>(engine_key);
		if (auto [version_ok, err] = check_engine_dll_version(*engine, expected_version); !version_ok) {
			TWIST_THROW(err.c_str());
		}
		return engine;
	}
	TWIST_COM_RETHROW
}

}

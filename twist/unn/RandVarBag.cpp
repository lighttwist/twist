//
//   Implementation file for "RandVarBag.hpp"
//
//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//   following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//        following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//        following disclaimer in the documentation and/or other materials provided with the distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software without 
//        specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//   INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//   DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//   STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//   EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#include "twist/unn/pch.hpp"

#include "twist/unn/RandVarBag.hpp"

#include "twist/com/com_utils.hpp"

#include "twist/unn/Distribution.hpp"
#include "twist/unn/RandomVariable.hpp"
#include "twist/unn/Sample32.hpp"
#include "twist/unn/unn_internals.hpp"

using namespace twist::com;

namespace twist::unn {

RandVarBag::RandVarBag(std::unique_ptr<RandVarBagCom> com)
	: com_(move(com))
{
	TWIST_CHECK_INVARIANT
}


RandVarBag::~RandVarBag()
{
	TWIST_CHECK_INVARIANT
}


std::unique_ptr<RandomVariable> RandVarBag::get_rand_var(std::wstring_view name) const
{
	TWIST_CHECK_INVARIANT
	try {
		auto irand_var = (*com_)->getRandVar(string_to_bstr(name));
		return std::unique_ptr<RandomVariable>{
				new RandomVariable(std::make_unique<RandomVariableCom>(irand_var))};
	}
	TWIST_COM_RETHROW
}


std::unique_ptr<Sample32> RandVarBag::read_rand_var_sample(std::wstring_view name) const
{
	TWIST_CHECK_INVARIANT
	try {
		auto isample = (*com_)->readRandVarSample(string_to_bstr(name));
		return std::unique_ptr<Sample32>{
				new Sample32(std::make_unique<Sample32Com>(isample))};
	}
	TWIST_COM_RETHROW
}


void RandVarBag::set_rand_var_parametric_distribution(std::wstring_view name, const Distribution& distr)
{
	TWIST_CHECK_INVARIANT
	try {
		(*com_)->setRandVarDistribution(string_to_bstr(name), distr.com_->interf(), nullptr/*distr_sample*/);
	}
	TWIST_COM_RETHROW
}


#ifdef _DEBUG
void RandVarBag::check_invariant() const noexcept
{
	assert(com_);
}
#endif

}

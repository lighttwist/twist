/// @file cpp20_ranges_utils.hpp
/// Utilities for working with the C++20 std::ranges sublibrary

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_CPP20_RANGES_UTILS_HPP
#define TWIST_CPP20_RANGES_UTILS_HPP

#include "twist/math/IntegerInterval.hpp"

namespace twist {

// --- twist::slice() ---

namespace detail {

struct Slice {
    constexpr Slice() {}

	constexpr [[nodiscard]] auto operator()(Ssize start, Ssize count) const -> Slice
	{
		return Slice{start, count};
	}

    template<std::integral T>
	constexpr [[nodiscard]] auto operator()(twist::math::IntegerInterval<T> pos_interval) const -> Slice
	{
		return Slice{pos_interval.lo(), pos_interval.length() + 1};
	}

	template<rg::viewable_range Rng>
    friend constexpr [[nodiscard]] auto operator|(Rng&& range, const Slice& sl)
    {
		return std::forward<Rng>(range) | vw::drop(sl.start_) | vw::take(sl.count_);
    }
	
private:
    constexpr Slice(Ssize start, Ssize count) 
        : start_{start}
        , count_{count} 
    {    
		assert(start_ >= 0);
		assert(count_ >= 0);
    }

	const Ssize start_{}; 
	const Ssize count_{};
};

}

/*! Use r | twist::slice(start, count) as a shorthand for r | vw::drop(start) | vw::take(count).
    Equivalently, use r | twist::slice(twist::math::IntegerInterval{start, start + length - 1}).
 */
inline constexpr auto slice = detail::Slice{};

//! Shorthand for rg::to<std::vector>()
template<class ...Args>
[[nodiscard]] auto to_vec(Args&& ...args) 
{
	return rg::to<std::vector>(std::forward(args)...);
}

}

#endif 


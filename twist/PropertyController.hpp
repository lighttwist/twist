///  @file  PropertyController.hpp
///  PropertyConstraint abstract class
///  PropConstraintNumOptions class, inherits PropertyConstraint
///  PropConstraintIntNumBounds class, inherits PropertyConstraint
///  PropConstraintFloatNumBounds class, inherits PropertyConstraint
///  PropertyController class

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_PROPERTY_CONTROLLER_HPP
#define TWIST_PROPERTY_CONTROLLER_H

#include "ExplicitLibType.hpp"
#include "PropertySet.hpp"

#include "math/ClosedInterval.hpp"
#include "math/RealInterval.hpp"

namespace twist {

class NamedPropertySet;

// Property constraint type type
using PropertyConstraintType = ExplicitLibType<unsigned long, 0, k_twist_lib_id, k_typeid_prop_constr_type>;

const PropertyConstraintType  k_prop_constr_none(0);			 // Invalid property constraint
const PropertyConstraintType  k_prop_constr_num_options(1);		 // "Numeric options" property constraint
const PropertyConstraintType  k_prop_constr_int_num_bounds(2);	 // "Numeric bounds" constraint for an integer property 
const PropertyConstraintType  k_prop_constr_float_num_bounds(3); // "Numeric options" constraint for a floating-point number property 

///
///  Abstract base class for all classes specifying a property constraint, that is a rule or set of rules 
///  controlling the possible values that a specific property can take, and sometimes giving names to those 
///  values.
///
class PropertyConstraint {
public:
	/// Destructor.
	///
	virtual ~PropertyConstraint() = 0;

	/// Get the property type
	///
	/// @return  The type
	///
	PropertyConstraintType get_type() const;

protected:
	/// Constructor. 
	///
	/// @param[in] prop_id  The ID of the property to which the constraint applies.
	/// @param[in] prop_set  The set which owns the property to which the constraint applies.
	/// @param[in] type  The constraint type
	///
	PropertyConstraint(PropertyId prop_id, const NamedPropertySet& prop_set, PropertyConstraintType type);

	/// Get the ID of the property to which the constraint applies.
	///
	/// @return  The property ID
	/// 
	PropertyId get_prop_id() const;

	/// Get the set which owns the property to which the constraint applies.
	///
	/// @param[in] The property set
	///
	const NamedPropertySet& get_prop_set() const;

private:
	PropertyId  prop_id_;
	const NamedPropertySet*  prop_set_;
	PropertyConstraintType  type_;

	friend class PropertyController;

	TWIST_CHECK_INVARIANT_DECL
};

///
///  The "numeric options" property constraint: this constraint applies to a property of "integer" type and 
///  specifies a finite set of integer values which the property can take, as well as a name for each of these 
///  values; in other words it presents the underlying integer property as a multi-option property.
///
class PropConstraintNumOptions : public PropertyConstraint {
public:
	typedef std::map<int, std::wstring>  NumOptions;

	/// Constructor. 
	///
	/// @param[in] prop_id  The ID of the property to which the constraint applies; an exception is thrown if 
	///					the property type is not "integer"
	/// @param[in] prop_set  The set which owns the property to which the constraint applies
	/// @param[in] num_options  The allowed values and their names
	///
	PropConstraintNumOptions(PropertyId prop_id, const NamedPropertySet& prop_set, NumOptions num_options);

	/// Destructor.
	///
	virtual ~PropConstraintNumOptions();

	/// Validate a candidate property value against the constraint rules.
	///
	/// @param[in] value  The property value.
	/// @param[in] err  If the current property value does not satisfy the constraint, this is a message 
	///					explaining the error.
	/// @return  true if the current property value satisfies the constraint.
	///
	bool check_prop_val(int value, std::wstring& err) const;

	/// Get the option name corresponding to a specific option (property) value.
	///
	/// @param[in] option_value  The option (property) value. An exception is thrown if the integer value is 
	///					not allowed by the constraint.
	/// @param[in] The option name
	///
	std::wstring get_option_name(int option_value) const;

	/// Get a list of all the option names, in increasing order of their numeric values.
	///
	/// @return  The option names
	///
	std::vector<std::wstring> get_option_names() const;

	/// Get the option (property) value corresponding to a specific option name.
	///
	/// @param[in] option_name  The option name. An exception is thrown if no option with the name is found; matching is 
	///				case sensitive.
	/// @param[in] The integer value
	///
	int get_option_value(std::wstring option_name) const;

private:
	NumOptions  num_options_;

	TWIST_CHECK_INVARIANT_DECL
};

///
///  The "integer numeric bounds" property constraint: this constraint applies to a property of "integer" type 
///  and specifies the numeric interval within which the property can take values.
///
class PropConstraintIntNumBounds : public PropertyConstraint {
public:
	typedef twist::math::ClosedInterval<int>  NumBounds;

	/// Constructor.
	///
	/// @param[in] prop_id  The ID of the property to which the constraint applies. An exception is thrown if 
	///					the property type is not "integer".
	/// @param[in] prop_set  The set which owns the property to which the constraint applies
	/// @param[in] num_bounds  The closed interval within which the property can take values.
	///
	PropConstraintIntNumBounds(PropertyId prop_id, const NamedPropertySet& prop_set, NumBounds num_bounds);

	/// Destructor.
	///
	virtual ~PropConstraintIntNumBounds();

	/// Validate a candidate property value against the constraint rules.
	///
	/// @param[in] value  The property value.
	/// @param[in] err  If the current property value does not satisfy the constraint, this is a message 
	///					expaining the error.
	/// @return  true if the current property value satisfies the constraint.
	///
	bool check_prop_val(int value, std::wstring& err) const;

	/// Get the closed interval within which the property can take values.
	///
	/// @return  The interval
	///
	const NumBounds& get_num_bounds() const;

private:
	NumBounds  num_bounds_;

	TWIST_CHECK_INVARIANT_DECL
};


///
///  The "floating-point numeric bounds" property constraint: this constraint applies to a property of 
///  "floating-point" type and specifies the numeric interval within which the property can take values.
///
class PropConstraintFloatNumBounds : public PropertyConstraint {
public:
	typedef math::RealInterval<double>  NumBounds;

	/// Constructor.
	///
	/// @param[in] prop_id  The ID of the property to which the constraint applies. An exception is thrown if 
	///					the property type is not "floating-point".
	/// @param[in] prop_set  The set which owns the property to which the constraint applies
	/// @param[in] num_bounds  The closed interval within which the property can take values.
	///
	PropConstraintFloatNumBounds(PropertyId prop_id, const NamedPropertySet& prop_set, NumBounds num_bounds);

	/// Destructor.
	///
	virtual ~PropConstraintFloatNumBounds();

	/// Validate a candidate property value against the constraint rules.
	///
	/// @param[in] value  The property value.
	/// @param[in] err  If the current property value does not satisfy the constraint, this is a message 
	///					explaining the error.
	/// @return  true if the current property value satisfies the constraint.
	///
	bool check_prop_val(double value, std::wstring& err) const;

	/// Get the closed interval within which the property can take values.
	///
	/// @return  The interval
	///
	const NumBounds& get_num_bounds() const;

private:
	NumBounds  num_bounds_;

	TWIST_CHECK_INVARIANT_DECL
};

///
///  Class used for ordering and grouping together a number of properties, under a common name (the group name).
///
class PropertyGroup {
public:
	typedef std::vector<PropertyId>  PropIds;

	/// Copy constructor
	///
	PropertyGroup(const PropertyGroup& src);

	/// Assignment operator
	///
	PropertyGroup& operator=(const PropertyGroup& rhs);

	/// Get the group name
	///
	/// @return  The name
	///
	std::wstring get_name() const;

	/// Get the IDs of the properties in the group. Order is important.
	///
	/// @return  The property IDs
	///
	const PropIds& get_prop_ids() const;

	/// Add a property at the end of the group.
	///
	/// @param[in] prop_id  The property ID. An exception is thrown if the property ID already exists in the 
	///					group, or if the functor passed into the constructor decides that this ID is not valid 
	///					for this group.
	///
	void add_prop_id(PropertyId prop_id);

private:
	typedef std::function<void (PropertyId)>  CheckPropId;  // Throws an exception if the prop ID is not valid

	/// Constructor
	///
	/// @param[in] name  The group name
	/// @param[in] check_prop_id  A functor whose responsibility is to check whether a specific property ID 
	///					can be added to the group and, if not, to throw an exception. 
	///
	PropertyGroup(std::wstring name, CheckPropId check_prop_id);

	std::wstring  name_;
	PropIds  prop_ids_;
	CheckPropId  check_prop_id_;

	friend class PropertyController;

	TWIST_CHECK_INVARIANT_DECL
};

typedef std::vector<PropertyGroup>  PropertyGroups;

///
///  Class used for ordering and grouping together a number of properties, under a common name (the family name).
///
class PropertyFamily {
public:
	typedef std::vector<PropertyId>  PropIds;

	/// Copy constructor
	///
	PropertyFamily(const PropertyFamily& src);

	/// Assignment operator
	///
	PropertyFamily& operator=(const PropertyFamily& rhs);

	/// Get the family name
	///
	/// @return  The name
	///
	std::wstring get_name() const;

	/// Get the IDs of the properties in the family. Order is important.
	///
	/// @return  The property IDs
	///
	const PropIds& get_prop_ids() const;

	/// Add a property at the end of the property family.
	///
	/// @param[in] prop_id  The property ID. An exception is thrown if the property ID already exists in the 
	///					family, or if the functor passed into the constructor decides that this ID is not 
	///					valid for this family.
	///
	void add_prop_id(PropertyId prop_id);

private:
	typedef std::function<void (PropertyId)>  CheckPropId;  // Throws an exception if the prop ID is not valid

	/*! Constructor
	    \param[in] parent  The PropertyId of the parent 
	    \param[in] name  The property family name
	    \param[in] check_prop_id  A functor whose responsibility is to check whether a specific property ID can be 
	                              added to the family and, if not, to throw an exception
	 */
	PropertyFamily(PropertyId parent, const std::wstring& name, CheckPropId check_prop_id);

	std::wstring name_;
	PropertyId parent_;
	PropIds prop_ids_;
	CheckPropId check_prop_id_;

	friend class PropertyController;

	TWIST_CHECK_INVARIANT_DECL
};

typedef std::vector<PropertyFamily>  PropertyFamilies;

///
///  Class which specifies details about the grouping (and visibility) of the properties in a property set as 
///  well as constraints to be applied to those properties.
///
class PropertyController {
public:
	/// Constructor.
	///
	/// @param[in] prop_set  The controlled property set
	///
	PropertyController(const NamedPropertySet& prop_set);

	/// Copy constructor.
	///
	PropertyController(const PropertyController& src);

	/// Move constructor.
	///
	PropertyController(PropertyController&& src);

	/// Destructor.
	///
	virtual ~PropertyController();

	/// Copy-assignment operator.
	///
	PropertyController& operator=(const PropertyController& rhs);

	/// Move-assignment operator.
	///
	PropertyController& operator=(PropertyController&& rhs);

	/// Add a property constraint to the controller
	///
	/// @param[in] constraint  The property constraint. An exception is thrown the property to which the 
	///					constraint applies is not found within the controlled property set, or if that 
	///					property already has an associated constraint.
	///
	void add_prop_constraint(std::unique_ptr<PropertyConstraint> constraint);

	/// Get the type of the constraint (if any) associated with a specific property in the controlled 
	/// property set.
	///
	/// @param[in] prop_id  The property ID
	/// @return  The constraint type, or  k_prop_constr_none  if no constraint is associated with the property
	///
	PropertyConstraintType get_constraint_type(PropertyId prop_id) const;

	/// Get the "numeric options" constraint associated with a specific property in the controlled property set.
	///
	/// @param[in] prop_id  The property ID. An exception is thrown if the property is not constrained 
	///					through a "numeric options" constraint.
	/// @return  The constraint.
	/// 
	const PropConstraintNumOptions& get_num_options_constraint(PropertyId prop_id) const;

	/// Get the "integer numeric bounds" constraint associated with a specific property in the controlled 
	/// property set.
	///
	/// @param[in] prop_id  The property ID. An exception is thrown if the property is not constrained 
	///					through an "integer numeric bounds" constraint.
	/// @return  The constraint.
	/// 
	const PropConstraintIntNumBounds& get_int_num_bounds_constraint(PropertyId prop_id) const;

	/// Get the "floating-point numeric bounds" constraint associated with a specific property in the 
	/// controlled property set.
	///
	/// @param[in] prop_id  The property ID. An exception is thrown if the property is not constrained 
	///					through an "floating-point numeric bounds" constraint.
	/// @return  The constraint.
	/// 	
	const PropConstraintFloatNumBounds& get_float_num_bounds_constraint(PropertyId prop_id) const;

	/// Get the constraint (if any) associated with a specific property in the controlled property set.
	///
	/// @param[in] prop_id  The property ID
	/// @return  The constraint, or  nullptr  if there is no constraint associated with the property
	/// 
	const PropertyConstraint* find_prop_constraint(PropertyId prop_id) const;

	/// Get all the property groups (if any) defined for the controlled property set.
	/// Generally, if no groups are defined, all the properties in a set will be visible, in ascending order 
	/// of their property IDs. 
	///
	/// @return  The property groups
	///
	const PropertyGroups& get_prop_groups() const;

	/// Add a new property group to the controller.
	///
	/// @param[in] name  The new group name. An exception is thrown if a group with this name already exists.
	/// @return  The new group.
	///
	PropertyGroup& add_prop_group(std::wstring name);

	/// Add a new property family to the controller.
	///
	/// @param[in] parent_id  property ID of the parent for the family.
	/// @param[in] name  The new family name. An exception is thrown if a family with this name already exists.
	/// @return  The new group.
	///
	PropertyFamily& add_prop_family(PropertyId parent_id, const std::wstring& name);

	/// Checks if a family is associated with a certain property id. 
	///
	/// @param[in] prop_id  The property ID
	///	@return  True if a family is found. 
	///
	bool has_family(PropertyId prop_id) const;
	
	/// Get the family associated with a specific property in the property set. If there is no associated 
	/// family, throws an exception
	///
	/// @param[in] prop_id  The property ID. 
	///	@return  The property family.
	///
	const PropertyFamily& get_family_of_prop(PropertyId prop_id) const;

private:	
	/// Check whether a specific property ID can be added to a group and, if not, throws an exception. 
	/// The conditions for success are that the property ID comes from the controlled property set and that it 
	/// does not already appear in another group.
	///
	/// @param[in] prop_id  The property ID.
	///
	void check_new_group_prop_id(PropertyId prop_id) const;

	/// Check whether a specific property ID can be added to a family and, if not, throws an exception. 
	/// The conditions for success are that the property ID comes from the controlled property set and that it 
	/// does not already appear in another family.
	///
	/// @param[in] prop_id  The property ID.
	///
	void check_new_family_prop_id(PropertyId prop_id) const;

	std::map<PropertyId, std::unique_ptr<PropertyConstraint>>  prop_constraints_;

	PropertyGroups  prop_groups_;
	PropertyFamilies prop_families_;
	const NamedPropertySet*  prop_set_; 

	TWIST_CHECK_INVARIANT_DECL
};

} 

#endif 

/// @file rtf_utils.hpp
/// Utilities for working with RTF documents.

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_RTF__UTILS_HPP
#define TWIST_RTF__UTILS_HPP

#include "twist/Colour.hpp"
#include "twist/Font.hpp"

#include <variant>

namespace twist {

//! Horizontal alignment in RTF documents.
enum class RtfHAlign {
	left,
	centre
};

//! The index of a colour within an RTF document's colour table. Zero based.
enum class RtfColourTableIndex : Ssize { none = -1 };

//! The indexes of the (typically background) colours used for a set of columns in an RTF table, keyed by column index.
using RtfColumnColours = std::map<Ssize, RtfColourTableIndex>;

/*! Information about the (typically background) colours used for the cells in an RTF table row. 
    If std::monostate, the cells use no colour;
	If RtfColourTableIndex, then the colour with the given index is used for all cells;
	If RtfColumnColours, then the colours with the specifies indexes are used in the corresponding columns.
 */
using RtfRowColourInfo = std::variant<std::monostate, RtfColourTableIndex, RtfColumnColours>;

constexpr auto twips_per_inch = 1440; ///< Number of twips in an inch

/*! Abstract base for classes modelling the content of an RTF document "section".
    \note  See also type RtfDocSection.
 */ 
class RtfDocSectionContent : public twist::NonCopyable {
public:
	virtual ~RtfDocSectionContent();

protected:	
	RtfDocSectionContent();
	
	//! Get the source code of the section.
	virtual auto get_source_text() const -> std::wstring = 0;

private:	
	friend class RtfDocSection;

	TWIST_CHECK_INVARIANT_DECL
};

//! A table in an RTF document.
class RtfDocTable : public RtfDocSectionContent {
public:
	virtual ~RtfDocTable();

	//! The number of columns in the table.
	[[nodiscard]] auto nof_columns() -> Ssize;

	/*! Set column width.
	    \param[in] colId  Index of the column to change width in twips (1/20 of a point, where 72 points are an inch)
	    \param[in] width  New width of the column.
	 */
	auto set_column_wdith(Ssize colId, Ssize width) -> void;

	/*! Add new row to the table formatted with the default font, and, optionally, specific background colour(s).
	    \param[in] cell_strings  The strings for the cells of this row
		\param[in] bkg_colour_info  Information about the background colour of the cells in the new row
	 */
	auto add_row(const std::vector<std::wstring>& cell_strings, const RtfRowColourInfo& bkg_colour_info = {}) -> void;

	/*! Add new row to the table formatted with a specific font.
	    \param[in] cell_strings  The strings for the cells of this row
	    \param[in] font  Font for formatting cell text
	 */
	auto add_row(const std::vector<std::wstring>& cell_strings, const Font& font) -> void;
	
protected:
	[[nodiscard]] auto get_source_text() const -> std::wstring override; // RtfDocSectionContent override
		
private:	
	friend class RtfDocSection;

	/*! Constructor.
	    \param[in] nof_cols  Number of columns.
	    \param[in] defColWidth  Default column width (applied to all columns) in twips (1/20 of a point, where 72 
	                            points are an inch)
	    \param[in] defFont  Default text font
	    \param[in] table_alignment  The horizontal alignment of the table within the document
	    \param[in] cell_alignment  The horizontal alignment of cell's contents within the cell
	 */
	RtfDocTable(Ssize nof_cols, 
	            Ssize defColWidth, 
				Font defFont, 
				RtfHAlign table_alignment, 
	            RtfHAlign cell_alignment);

	[[nodiscard]] auto row_align_string() const -> std::wstring;

	[[nodiscard]] auto cell_align_string() const -> std::wstring;
		
	std::wstring m_tableText;
	
	const Font m_defaultFont;
	
	Ssize nof_cols_;
	Ssize nof_rows_{0};
	std::vector<Ssize> col_widths_;

	RtfHAlign table_alignment_;
	RtfHAlign cell_alignment_;
	
	TWIST_CHECK_INVARIANT_DECL
};

//! A paragraph in an RTF document.
class RtfDocParagraph : public RtfDocSectionContent {
public:
	/*! Add text to paragraph formatted with the default font.
	    \param[in] text  Text added to the paragraph.
	 */
	void addText(const std::wstring& text);

	/*! Add text to paragraph.
	    \param[in] text  Text added to the paragraph.
	    \param[in] font  Font for formatting the text.
	 */
	void addText(const std::wstring& text, const Font& font);
	
	//! End current line.
	void newLine();

	/*! Add tabs.
	    \param[in] numTabs  Number of tabs to add.
	 */
	void addTabs(Ssize numTabs);
	
	/*! Add list item.
	    \param[in] itemText  Text of the list item.
	    \param[in] listLevel  Level of the list (1 - top, 2 - sublevel of top, 3 -sublevel of sublevel 2, etc.)
	    \param[in] listHeadString  std::wstring with numbering of the list item.
	 */
	void addListItem(const std::wstring& itemText, Ssize listLevel, const std::wstring& listHeadString);
	
private:		
	friend class RtfDocSection;

	/*! Constructor.
	    \param[in] text  Content text of the paragraph.
	    \param[in] defFont  Default text font.
	 */
	RtfDocParagraph(const std::wstring& text, const Font& defFont);
		
	[[nodiscard]] auto get_source_text() const -> std::wstring override; // RtfDocSectionContent override
	
	const Font m_defaultFont;
	std::wstring m_parText;
	
	TWIST_CHECK_INVARIANT_DECL
};

//! A block of simple text in an RTF document (typically not part of a paragraph).
class RtfDocSimpleTextBlock : public RtfDocSectionContent {
public:
	/*! Constructor.
	    \param[in] text  The text
		\param[in] start_on_new_line  Whether the block of text starts with newline markup
	 */
	explicit RtfDocSimpleTextBlock(std::wstring text, bool start_on_new_line);

	[[nodiscard]] auto get_source_text() const -> std::wstring override; // RtfDocSectionContent override

private:
	std::wstring text_;
};

//! An item in a list in an RTF document.
class RtfDocListItem {
public:
	~RtfDocListItem();

	TWIST_NO_COPY_NO_MOVE(RtfDocListItem);

private:
	friend class RtfDocList;

	/*! Constructor.
	    \param[in] itemText  Text of the list item.
	 */
	RtfDocListItem(const std::wstring& itemText);

	/*! Constructor.
	    \param[in] itemText  Text of the list item.
	    \param[in] childList  Reference to the child list.  Item owns this list.
	 */
	RtfDocListItem(const std::wstring& itemText, RtfDocList* childList);
	
	std::wstring m_listText;
	RtfDocList* m_childList{nullptr};

	TWIST_CHECK_INVARIANT_DECL
};

//! A list in an RTF document.
class RtfDocList : public RtfDocSectionContent {
public:
	/*! Add list item.
	    \param[in] itemText  Text of the list item.
	 */
	void addListItem(const std::wstring& itemText);

	/*! Add list item with sublist.
	    \param[in] itemText  Text of the list item.
	    \return  reference to the sublist.
	 */
	RtfDocList& addListItemWithSubList(const std::wstring& itemText);
	
	virtual ~RtfDocList();
		
protected:
	[[nodiscard]] auto get_source_text() const -> std::wstring override; // RtfDocSectionContent override

private:		
	friend class RtfDocSection;

	/*! Constructor.
	    \param[in] level  Level of the list.
	    \param[in] defFont  Default text font.
	 */
	RtfDocList(Ssize level, const Font& defFont);
			
	//! Get label for an item for specific level.
	std::wstring getLabel(Ssize index) const;
	
	const Font m_defaultFont;
	std::wstring m_listText;
	std::vector<RtfDocListItem*> m_itemsList;
	Ssize m_level;
	
	TWIST_CHECK_INVARIANT_DECL
};

//! A section in an RTF document, ie a collection of any or all of the following: text, lists and tables. 
class RtfDocSection  {
public:
	virtual ~RtfDocSection();

	/*! Add new empty paragraph to the section and give read/write access to it.
	    \return  The new paragraph.
	 */
	[[nodiscard]] auto addParagraph() -> RtfDocParagraph&;

	/*! Add new paragraph with text to the section and give read/write access to it.
	    \param[in] text  Text of the paragraph
	    \return  The new paragraph.
	 */
	auto addParagraph(const std::wstring& text) -> RtfDocParagraph&;

	/*! Add a new "simple text block" to the section and give read/write access to it.
	    \param[in] text  The text
		\param[in] start_on_new_line  Whether the block of text starts with newline markup
	    \return  The new text block
	*/
	auto add_simple_text_block(std::wstring text, bool start_on_new_line = true) -> RtfDocSimpleTextBlock&;

	/*! Add new list to the section and give read/write access to it.
	    \return  The new list
	 */
	[[nodiscard]] auto addList() -> RtfDocList&;

	/*! Add new table to the section and give read/write access to it.
	    \param[in] numCols  Number of columns
	    \param[in] defColWidth  Default column width (applied to all columns) in twips (1/20 of a point, where 72 
	                            points are an inch)
	    \param[in] table_alignment  The horizontal alignment of the table within the document
	    \param[in] cell_alignment  The horizontal alignment of cell's contents within the cell
	    \return  The new table
	 */
	[[nodiscard]] auto addTable(Ssize numCols, Ssize defColWidth, RtfHAlign table_alignment, 
	                            RtfHAlign cell_alignment) -> RtfDocTable&;

	TWIST_NO_COPY_NO_MOVE(RtfDocSection);

protected:
	//! Get the source code of the section.	 
	[[nodiscard]] auto get_source_text() const -> std::wstring;

private:	
	friend class RtfDoc;

	/*! Constructor.
	    \param[in] sectionTitle  Title of the section.
	    \param[in] defFont  Default font.
	    \param[in] titleFont  Section title font.
	 */
	RtfDocSection(std::wstring sectionTitle, const Font& defFont, const Font& titleFont);
	
	std::vector<std::unique_ptr<RtfDocSectionContent>> m_contentItemsList;
	const Font m_defaultFont;
	const Font m_defaultTitleFont;
	std::wstring m_sectionTitle;
	std::wstring m_sectText;

	TWIST_CHECK_INVARIANT_DECL
};

//! RTF document class. Use it for creating rich format documents. It creates new sections.
class RtfDoc {
public:
	/*! Constructor.
	    \param[in] doc_title  Title of the document
	    \param[in] default_font  Default font
	    \param[in] default_title_font  Default font for all titles in the document
		\param[in] colour_table  Optional colour table to be used in the document; each colour will be associated a
		                         RtfColourTableIndex value equal to its position in this vector
	 */
	RtfDoc(std::wstring doc_title, 
	       Font default_font, 
		   Font default_title_font, 
		   std::vector<Colour> colour_table = {});

	virtual ~RtfDoc();
	
	/*! Add new section to the document.
	    \param[in] sectionTitle  Title of the section.
	    \return  Ref to the new section.
	 */
	RtfDocSection& addSection(std::wstring sectionTitle);

	//! Delete all the sections in the document.
	auto clear() -> void;

	//! Get the source code of the RTF document.
	[[nodiscard]] auto get_source_text() const -> std::wstring;

	//! Save the document to a file with path \p path.
	auto save_document(const fs::path& path) const -> void;

	TWIST_NO_COPY_DEF_MOVE(RtfDoc);
	
private:
	std::wstring m_docTitle;
	Font m_defaultFont;
	Font m_defaultTitleFont;
	std::vector<std::unique_ptr<RtfDocSection>> m_sectList;
	std::wstring m_docText;
	std::vector<Colour> colour_table_;
	
	TWIST_CHECK_INVARIANT_DECL
};

/*! Abstract base for classes which provide RTF-formatted feedback.
    All feedback goes to a section in an RTF document owned by this class.
 */
class RtfFeedbackProv {
public: 
	virtual ~RtfFeedbackProv() = 0;

	//! The feedback section.
	[[nodiscard]] auto section() -> RtfDocSection&;

	//! Flush the document source to the that is currently in the RTF section to the target provider.
	auto update() -> void;

	/*! Erase the document source to the that is currently in the RTF section, and flush the empty source to the target
	    provider.
	 */
	auto clear() -> void;

protected:
	explicit RtfFeedbackProv(RtfDoc doc);

	//! The RTF document which holds the feedback section.
	[[nodiscard]] auto rtf_doc() -> RtfDoc&;

private:
	//! Flush the document source \p rtf_source to the target provider.
	virtual auto do_update(const wchar_t* rtf_source) -> void = 0;

	RtfDoc doc_;
	RtfDocSection* section_;

	TWIST_CHECK_INVARIANT_DECL
};

// --- Free functions ---

/*! Format bookmark for rtf document.
    \param[in] bookName  Name of the bookmark.
    \param[in] bookCaption  Text displayed in document (can be empty).
    \param[in] font  Font for caption.
 */
auto rtfFormatBookmark(const std::wstring& bookName, const std::wstring& bookCaption, const Font& font)
      -> std::wstring;

/*! Format reference to a bookmark for rtf document.
    \param[in] bookName  Name of the refered bookmark.
    \param[in] linkCaption  Link text.
    \param[in] font  Font for caption.
 */
auto rtfFormatReference(const std::wstring& bookName, const std::wstring& linkCaption, const Font& font) 
      -> std::wstring;

/*! Add a "simple text block" containing a single line to an RTF feedback provider.
   \param[in] feedback_prov  The provider
   \param[in] text  The text
   \param[in] update  Whether the feedback provider should be updated
   \param[in] start_on_new_line  Whether the text added should start with newline markup 
*/
auto add_simple_text_line(RtfFeedbackProv& feedback_prov, 
                          std::wstring text, 
						  bool update = true, 
						  bool start_on_new_line = true) -> void;

//! Add "bold" font markup around the string \p str. 
[[nodiscard]] auto rtf_bold(std::wstring str) -> std::wstring;

//! Add "italic" font markup around the string \p str. 
[[nodiscard]] auto rtf_italic(std::wstring str) -> std::wstring;

/*! Given the RTF table \p table, set the width of each of its columns to the values stored in \p widths.
    \p widths must have the same number of elements as there are columns in the table. Each element gives the width of 
	the corresponding cloumn in inches.
 */ 
auto set_column_inch_widths(RtfDocTable& table, const std::vector<double>& widths) -> void;

}  

#endif  

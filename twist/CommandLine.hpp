/// @file CommandLine.hpp
/// CommandLine class definition

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_COMMAND_LINE_HPP
#define TWIST_COMMAND_LINE_HPP

namespace twist {

/*! Simple command line manager.
    It parses the command line arguments, as passed into main(), and finds options by name and retrieves one or several
	values following an option name.
*/
class CommandLine {
public:
	/*! Constructor. 
	    \note  Use the exact same parameters as those passed into main(); this means the first argument will always be
		       the path to the program being executed
		\param[in] argc  Number of command line arguments
		\param[in] argv  Command line arguments
	 */
	explicit CommandLine(int argc, char* argv[]);

	//! Path to the program being executed.
	[[nodiscard]] auto program_path() const -> fs::path;

	//! The number of command line arguments, excluding the first, which is the path of the program executed.
	[[nodiscard]] auto nof_proper_arguments() const -> Ssize;

	//! Whether the option name \p option_name matches at least one argument.
	[[nodiscard]] auto has_option(std::string_view option_name) const -> bool;

	/*! Get the argument following the first argument matching the option name \p option_name.
	    If there are no arguments after the first argument matching the option name, an exception is thrown.
	 */
	[[nodiscard]] auto get_option_value(std::string_view option_name) const -> std::string;

	/*! Get the \p nof_values arguments following the first argument matching the option name \p option_name.
	    If there are not enough arguments after the first argument matching the option name, an exception is thrown.
	 */
	[[nodiscard]] auto get_option_values(std::string_view option_name, Ssize nof_values) const 
	                    -> std::vector<std::string>;

	TWIST_NO_COPY_NO_MOVE(CommandLine)

private:
	fs::path program_path_;
	std::vector<std::string> args_;

	TWIST_CHECK_INVARIANT_DECL
};

} 

#endif 

///  @file  NamedPropertySet.hpp
///  NamedPropertySet class 

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_NAMED_PROPERTY_SET_HPP
#define TWIST_NAMED_PROPERTY_SET_HPP

#include "PropertySet.hpp"

namespace twist {

///
///  Class modelling a set of properties and their values.
///  A "property" is defined by both an ID and a name (both unique within the set) and a type. The name matching is case 
///    sensitive and blank names are not allowed.
///  A property value is a value associated with a property, which has the type associated with that property. 
///  A property's value is always guaranteed to be set.
///  Additionally, a description can be associated with a property.
///
class NamedPropertySet {
public:
	/// Constructor.
	///
	NamedPropertySet();

	/// Constructor.
	///+
	NamedPropertySet(std::unique_ptr<PropertySet> prop_set);

	/// Copy constructor.
	///
	NamedPropertySet(const NamedPropertySet& src);

	/// Move constructor.
	///
	NamedPropertySet(NamedPropertySet&& src);

	/// Destructor.
	///
	~NamedPropertySet();

	/// Copy-assignment operator.
	///
	NamedPropertySet& operator=(const NamedPropertySet& rhs);

	/// Move-assignment operator.
	///
	NamedPropertySet& operator=(NamedPropertySet&& rhs);

	/// Define a new property of "integer" type.
	///
	/// @param[in] prop_id  The property ID. An exception is thrown if a property with this ID already exists.
	/// @param[in] prop_name  The property name. An exception is thrown if a property with this name already exists.
	/// @param[in] val  The initial property value.
	/// @param[in] prop_desc  The property description.
	///
	void define_int_prop(PropertyId prop_id, std::wstring prop_name, int val, std::wstring prop_descr = L"");

	/// Define a new property of "floating-point number" type.
	///
	/// @param[in] prop_id  The property ID. An exception is thrown if a property with this ID already exists.
	/// @param[in] prop_name  The property name. An exception is thrown if a property with this name already exists.
	/// @param[in] val  The initial property value.
	/// @param[in] prop_desc  The property description.
	///
	void define_float_prop(PropertyId prop_id, std::wstring prop_name, double val, std::wstring prop_descr = L"");

	/// Define a new property of "string" type.
	///
	/// @param[in] prop_id  The property ID. An exception is thrown if a property with this ID already exists.
	/// @param[in] prop_name  The property name. An exception is thrown if a property with this name already exists.
	/// @param[in] val  The initial property value.
	/// @param[in] prop_desc  The property description.
	///
	void define_string_prop(PropertyId prop_id, std::wstring prop_name, std::wstring val, std::wstring prop_descr = L"");

	/// Define a new property of "boolean" type.
	///
	/// @param[in] prop_id  The property ID. An exception is thrown if a property with this ID already exists.
	/// @param[in] prop_name  The property name. An exception is thrown if a property with this name already exists.
	/// @param[in] val  The initial property value.
	/// @param[in] prop_desc  The property description.
	///
	void define_bool_prop(PropertyId prop_id, std::wstring prop_name, bool val, std::wstring prop_descr = L"");

	/// Get the value of a property of "integer" type.
	///
	/// @param[in] prop_name  The property name. An exception is thrown if a property with this name is not defined or is not 
	///				of "integer" type.
	/// @return  The value.
	///
	int get_int_value(std::wstring prop_name) const;

	/// Set the value of a property of "integer" type.
	///
	/// @param[in] prop_name  The property name. An exception is thrown if a property with this name is not defined or is not 
	///				of "integer" type.
	/// @param[in] val  The value.
	///
	void set_int_value(std::wstring prop_name, int val);

	/// Get the value of a property of "floating-point number" type.
	///
	/// @param[in] prop_name  The property name. An exception is thrown if a property with this name is not defined or is not 
	///				of "floating-point number" type.
	/// @return  The value.
	///
	double get_float_value(std::wstring prop_name) const;

	/// Set the value of a property of "floating-point number" type.
	///
	/// @param[in] prop_name  The property name. An exception is thrown if a property with this name is not defined or is not 
	///				of "foating-point number" type.
	/// @param[in] val  The value.
	///
	void set_float_value(std::wstring prop_name, double val);

	/// Get the value of a property of "string" type.
	///
	/// @param[in] prop_name  The property name. An exception is thrown if a property with this name is not defined or is not 
	///				of "string" type.
	/// @return  The value.
	///
	std::wstring get_string_value(std::wstring prop_name) const;

	/// Set the value of a property of "string" type.
	///
	/// @param[in] prop_name  The property name. An exception is thrown if a property with this name is not defined or is not 
	///				of "string" type.
	/// @param[in] val  The value.
	///
	void set_string_value(std::wstring prop_name, std::wstring val);

	/// Get the value of a property of "boolean" type.
	///
	/// @param[in] prop_name  The property name. An exception is thrown if a property with this name is not defined or is not 
	///				of "boolean" type.
	/// @return  The value.
	///
	bool get_bool_value(std::wstring prop_name) const;

	/// Set the value of a property of "boolean" type.
	///
	/// @param[in] prop_name  The property name. An exception is thrown if a property with this name is not defined or is not 
	///				of "boolean" type.
	/// @param[in] val  The value.
	///
	void set_bool_value(std::wstring prop_name, bool val);

	/// Get the value of a property of "colour" type.
	///
	/// @param[in] prop_name  The property name. An exception is thrown if a property with this name is not defined or is not 
	///				of "colour" type.
	/// @return  The value.
	///
	unsigned long get_colour_value(std::wstring prop_name) const;

	/// Set the value of a property of "colour" type.
	///
	/// @param[in] prop_name  The property name. An exception is thrown if a property with this name is not defined or is not 
	///				of "colour" type.
	/// @param[in] val  The value.
	///
	void set_colour_value(std::wstring prop_name, unsigned long val);

	/// Get the ID of a specific property.
	///
	/// @param[in] prop_name  The property name. An exception is thrown if a property with this name is not defined.
	/// @return  The property ID.
	///
	PropertyId get_prop_id(std::wstring prop_name) const;

	/// Get the type of a specific property.
	///
	/// @param[in] prop_name  The property name. An exception is thrown if a property with this name is not defined.
	/// @return  The property type.
	///
	PropertyType get_prop_type(std::wstring prop_name) const;

	/// Get the name of a specific property.
	///
	/// @param[in] prop_id  The property ID. An exception is thrown if a property with this ID is not defined.
	/// @return  The name
	///
	std::wstring get_prop_name(PropertyId prop_id) const;

	/// Get the description of a specific property.
	///
	/// @param[in] prop_id  The property ID. An exception is thrown if a property with this ID is not defined.
	/// @return  The description
	///
	std::wstring get_prop_descr(PropertyId prop_id) const;

	/// Get the name and description of a specific property.
	///
	/// @param[in] prop_id  The property ID. An exception is thrown if a property with this ID is not defined.
	/// @return  The name and description, in a pair (first element is the name).
	///
	std::pair<std::wstring, std::wstring> get_prop_text(PropertyId prop_id) const;

	/// Get the value of a property of "integer" type.
	///
	/// @param[in] prop_id  The property ID. An exception is thrown if a property with this ID is not defined or is not 
	///				of "integer" type.
	/// @return  The value.
	///
	int get_int_value(PropertyId prop_id) const;

	/// Set the value of a property of "integer" type.
	///
	/// @param[in] prop_id  The property ID. An exception is thrown if a property with this ID is not defined or is not 
	///				of "integer" type.
	/// @param[in] val  The value.
	///
	void set_int_value(PropertyId prop_id, int val);

	/// Get the value of a property of "floating-point number" type.
	///
	/// @param[in] prop_id  The property ID. An exception is thrown if a property with this ID is not defined or is not 
	///				of "floating-point number" type.
	/// @return  The value.
	///
	double get_float_value(PropertyId prop_id) const;

	/// Set the value of a property of "floating-point number" type.
	///
	/// @param[in] prop_id  The property ID. An exception is thrown if a property with this ID is not defined or is not 
	///				of "floating-point number" type.
	/// @param[in] val  The value.
	///
	void set_float_value(PropertyId prop_id, double val);

	/// Get the value of a property of "string" type.
	///
	/// @param[in] prop_id  The property ID. An exception is thrown if a property with this ID is not defined or is not 
	///				of "string" type.
	/// @return  The value.
	///
	std::wstring get_string_value(PropertyId prop_id) const;

	/// Set the value of a property of "string" type.
	///
	/// @param[in] prop_id  The property ID. An exception is thrown if a property with this ID is not defined or is not 
	///				of "string" type.
	/// @param[in] val  The value.
	///
	void set_string_value(PropertyId prop_id, std::wstring val);

	/// Get the value of a property of "boolean" type.
	///
	/// @param[in] prop_id  The property ID. An exception is thrown if a property with this ID is not defined or is not 
	///				of "boolean" type.
	/// @return  The value.
	///
	bool get_bool_value(PropertyId prop_id) const;

	/// Set the value of a property of "boolean" type.
	///
	/// @param[in] prop_id  The property ID. An exception is thrown if a property with this ID is not defined or is not 
	///				of "boolean" type.
	/// @param[in] val  The value.
	///
	void set_bool_value(PropertyId prop_id, bool val);

	/// Find out whether a property with a specific ID exists in the set.
	///
	/// @param[in] prop_id  The property ID.
	/// @return  true if the property exists.
	///
	bool prop_exists(PropertyId prop_id) const;

	/// Get the type of a specific property.
	///
	/// @param[in] prop_id  The property ID. An exception is thrown if a property with this ID is not defined.
	/// @return  The property type ID.
	///
	PropertyType get_prop_type(PropertyId prop_id) const;

	/// Get information about all properties in the set.
	///
	/// @return  A map containing the IDs of all properties and the associated property type IDs.
	///
	std::map<PropertyId, PropertyType> get_props() const;

protected:
	/// Get the underlying property set.
	///
	/// @return  The property set
	///
	const PropertySet& get_prop_set() const;

	/// Get the underlying property set.
	///
	/// @return  The property set
	///
	PropertySet& get_prop_set();

protected:
	/// Define the text (name and description) to be associated with a new property.
	/// Note that this method does not define the new property in the underlying property set.
	///
	/// @param[in] prop_id  The property ID. An exception is thrown if a property with this ID already exists.
	/// @param[in] prop_name  The property name. An exception is thrown if a property with this name already exists.
	/// @param[in] val  The initial property value.
	/// @param[in] prop_desc  The property description.
	///
	void define_prop_text(PropertyId prop_id, const std::wstring& prop_name, const std::wstring& prop_descr);

private:	
	/// Define a new property.
	///
	/// @tparam  ValueT  The property value type.
	/// @param[in] prop_id  The property ID. An exception is thrown if a property with this ID already exists.
	/// @param[in] prop_name  The property name. An exception is thrown if a property with this name already exists.
	/// @param[in] val  The initial property value.
	/// @param[in] prop_desc  The property description.
	///
	template<typename ValueT> void define_prop(PropertyId prop_id, const std::wstring& prop_name, ValueT val, 
			const std::wstring& prop_descr);

	std::unique_ptr<PropertySet>  prop_set_;

	std::map<PropertyId, std::pair<std::wstring, std::wstring>>  prop_ids_to_text_;
	std::map<std::wstring, PropertyId>  prop_names_to_ids_;

	TWIST_CHECK_INVARIANT_DECL
};

} 

#include "NamedPropertySet.ipp"

#endif 
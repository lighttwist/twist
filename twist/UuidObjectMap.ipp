///  @file  UuidObjectMap.ipp
///  Inline implementation file for "UuidObjectMap.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

namespace twist {

template<class Value>
UuidObjectMap<Value>::UuidObjectMap()
	: map_()
{
	TWIST_CHECK_INVARIANT
}


template<class Value>
UuidObjectMap<Value>::~UuidObjectMap()
{
	TWIST_CHECK_INVARIANT
}


template<class Value>
void UuidObjectMap<Value>::insert(Value value, Uuid key)
{
	TWIST_CHECK_INVARIANT
	const auto result = map_.insert(std::make_pair(key, std::forward<Value>(value)));
	if (!result.second) {
		TWIST_THROW(L"A value with the key \"%s\" already exists.", to_str(key).c_str());
	}
	TWIST_CHECK_INVARIANT
}


template<class Value>
bool UuidObjectMap<Value>::try_insert(Value value, Uuid key)
{
	TWIST_CHECK_INVARIANT
	const auto result = map_.insert(std::make_pair(key, std::forward<Value>(value)));
	TWIST_CHECK_INVARIANT
	return result.second;
}


template<class Value>
const Value& UuidObjectMap<Value>::get(Uuid key) const
{
	TWIST_CHECK_INVARIANT
	auto it = map_.find(key);
	if (it == std::end(map_)) {
		TWIST_THROW(L"A value with the key \"%s\" could not be found.", to_str(key).c_str());
	}
	return it->second;
}


template<class Value>
bool UuidObjectMap<Value>::has(Uuid key) const
{
	TWIST_CHECK_INVARIANT
	auto it = map_.find(key);
	return it != std::end(map_);
}


template<class Value>
void UuidObjectMap<Value>::erase(Uuid key)
{
	TWIST_CHECK_INVARIANT
	auto it = map_.find(key);
	if (it == std::end(map_)) {
		TWIST_THROW(L"A value with the key \"%s\" could not be found.", to_str(key).c_str());
	}
	map_.erase(key);
	TWIST_CHECK_INVARIANT
}


template<class Value>	
typename UuidObjectMap<Value>::const_iterator UuidObjectMap<Value>::begin() const
{
	TWIST_CHECK_INVARIANT
	return map_.begin();
}


template<class Value>	
typename UuidObjectMap<Value>::const_iterator UuidObjectMap<Value>::end() const
{
	TWIST_CHECK_INVARIANT
	return map_.end();
}


#ifdef _DEBUG
template<class Value>
void UuidObjectMap<Value>::check_invariant() const noexcept
{
}
#endif 

} 



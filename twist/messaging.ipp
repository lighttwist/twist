/// @file messaging.ipp
/// Inline implementation file for "messaging.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace twist {

// --- Messenger class ---

template<class MsgT, typename MemFuncT, class ObjT> 
bool Messenger::add_listener(MemFuncT mem_func, ObjT& obj)
{
	TWIST_CHECK_INVARIANT
	static_assert(std::is_invocable_r_v<void, MemFuncT, ObjT&, const MessageId&, const typename MsgT::Data&>,
			      "Listener has the wrong signature."); 
	return internal_add_listener<MsgT>(MsgT::Listener(mem_func, obj));
}

template<class MsgT, typename StaticFuncT> 
bool Messenger::add_listener(StaticFuncT static_func)
{
	TWIST_CHECK_INVARIANT
	static_assert(std::is_invocable_r_v<void, StaticFuncT, const MessageId&, const typename MsgT::Data&>,
			      "Listener has the wrong signature."); 
	return internal_add_listener<MsgT>(MsgT::Listener(static_func));
}

template<class MsgT, typename MemFuncT, class ObjT> 
bool Messenger::remove_listener(MemFuncT mem_func, ObjT& obj)
{
	TWIST_CHECK_INVARIANT
	return internal_remove_listener<MsgT>(MsgT::Listener(mem_func, obj));
}

template<class MsgT, typename StaticFuncT> 
bool Messenger::remove_listener(StaticFuncT static_func)
{
	TWIST_CHECK_INVARIANT
	return internal_remove_listener<MsgT>(MsgT::Listener(static_func));
}

template<class MsgT> 
void Messenger::broadcast_message(const typename MsgT::Data& msg_data)
{
	TWIST_CHECK_INVARIANT
	const MessageId* key = &MsgT::id();
	for (auto it = listeners_.lower_bound(key); it != listeners_.upper_bound(key); ++it) {
		it->second->call(MsgT::id(), msg_data);
	}
	TWIST_CHECK_INVARIANT
}

template<class MsgT> 
void Messenger::broadcast_message()
{
	TWIST_CHECK_INVARIANT
	const MessageId* key = &MsgT::id();
	for (auto it = listeners_.lower_bound(key); it != listeners_.upper_bound(key); ++it) {
		it->second->call(MsgT::id());
	}
	TWIST_CHECK_INVARIANT
}

template<class MsgT> 
auto Messenger::internal_add_listener(typename MsgT::Listener listener) -> bool
{
	TWIST_CHECK_INVARIANT
	bool ret = false;
	const MessageId* key = &MsgT::id();

	// Make sure that the listener isn't already registered for this message type
	if (find_listener<MsgT>(listener) == end(listeners_)) {
		std::unique_ptr<ListenerWrapperBase> listener(new ListenerWrapper<MsgT::Data>(listener));
		listeners_.insert(std::make_pair(key, move(listener)));
		ret = true;
	}

	TWIST_CHECK_INVARIANT
	return ret;
}

template<class MsgT> 
auto Messenger::internal_remove_listener(typename MsgT::Listener listener) -> bool
{
	TWIST_CHECK_INVARIANT
	bool ret = false;

	// Check whether the listener is registered for this message type
	auto it = find_listener<MsgT>(listener);
	if (it != end(listeners_)) {
		listeners_.erase(it);
		ret = true;
	}

	TWIST_CHECK_INVARIANT
	return ret;
}

template<class MsgT> 
auto Messenger::find_listener(typename MsgT::Listener listener) -> ListenerWrapperMap::iterator
{
	TWIST_CHECK_INVARIANT
	ListenerWrapperMap::iterator ret_it = end(listeners_);
	const MessageId* key = &MsgT::id();

	const ListenerWrapper<MsgT::Data> wrapper(listener);

	for (auto it = listeners_.lower_bound(key); it != listeners_.upper_bound(key); ++it) {
		ListenerWrapper<MsgT::Data>& cur_wrapper = static_cast<ListenerWrapper<MsgT::Data>&>(*it->second);  // Entirely safe, as the listeners are keyed by type_info
		if (wrapper.compare(cur_wrapper)) {
			ret_it = it;
			break;
		}
	}

	TWIST_CHECK_INVARIANT
	return ret_it;
}

#ifdef _DEBUG
inline void Messenger::check_invariant() const noexcept
{
}
#endif

// --- Messenger::ListenerWrapperBase class ---

Messenger::ListenerWrapperBase::ListenerWrapperBase()
{
} 

Messenger::ListenerWrapperBase::~ListenerWrapperBase()
{
} 

auto Messenger::ListenerWrapperBase::call(const MessageId& /*msg_id*/, const MessageDataBase& /*msg_data*/) -> void 
{
}

auto Messenger::ListenerWrapperBase::call(const MessageId& /*msg_id*/) -> void
{
}

// --- Messenger::ListenerWrapper primary class template ---

template<class MsgDataT>
Messenger::ListenerWrapper<MsgDataT>::ListenerWrapper(Listener func) 
	: ListenerWrapperBase{}
	, func_{func}
{
}

template<class MsgDataT>
Messenger::ListenerWrapper<MsgDataT>::~ListenerWrapper()
{
}

template<class MsgDataT>
auto Messenger::ListenerWrapper<MsgDataT>::call(const MessageId& msg_id, const MessageDataBase& msg_data) -> void
{
	func_(msg_id, static_cast<const MsgDataT&>(msg_data));  // Entirely safe, as the listeners are keyed by type_info
}

template<class MsgDataT>
auto Messenger::ListenerWrapper<MsgDataT>::compare(const ListenerWrapper& other) const -> bool
{
	return func_ == other.func_;
}

// --- Messenger::ListenerWrapper explicit class template specialisation ---

inline Messenger::ListenerWrapper<void>::ListenerWrapper(Listener func) 
	: ListenerWrapperBase()
	, func_(func)
{
}

inline void Messenger::ListenerWrapper<void>::call(const MessageId& msg_id) 
{
	func_(msg_id);
}

inline bool Messenger::ListenerWrapper<void>::compare(const ListenerWrapper& other) const
{
	return func_ == other.func_;
}

// --- ScopedMessageRegistrar class template ---

template<class ListenerObj>
ScopedMessageRegistrar<ListenerObj>::ScopedMessageRegistrar(ListenerObj& obj, Messenger& messenger)
	: obj_{ obj }
	, messenger_{ messenger }
{
}

template<class ListenerObj>
ScopedMessageRegistrar<ListenerObj>::~ScopedMessageRegistrar()
{
	// Unregister any listeners which were registered via this object
	for_each(listener_removers_, [](auto remover) {
		remover();
	});
}

template<class ListenerObj>
template<class Msg, typename MemFunc> 
auto ScopedMessageRegistrar<ListenerObj>::add(MemFunc mem_func) -> bool
{
	if (!messenger_.add_listener<Msg, MemFunc, ListenerObj>(mem_func, obj_)) {
		return false;
	}
	listener_removers_.push_back([this, mem_func] {
		messenger_.remove_listener<Msg, MemFunc, ListenerObj>(mem_func, obj_);
	});
	return true;
}

} 

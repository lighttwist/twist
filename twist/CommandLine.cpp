/// @file CommandLine.cpp
/// Implementation file for "CommandLine.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/CommandLine.hpp"

#include "twist/IndexRange.hpp"
#include "twist/std_vector_utils.hpp"
#include "twist/string_utils.hpp"

namespace twist {

CommandLine::CommandLine(int argc, char* argv[])
{
	if (argc < 1 || argv == nullptr) {
		TWIST_THRO2(L"Invalid command line.");
	}

	auto arg_begin = argv;
	const auto arg_end = argv + argc;
	if (std::any_of(arg_begin, arg_end, [](auto arg) { return arg == nullptr || is_whitespace(arg); })) {
		TWIST_THRO2(L"Invalid command line.");
	}

	program_path_ = *arg_begin;
	++arg_begin;
	std::copy(arg_begin, arg_end, back_inserter(args_));

	TWIST_CHECK_INVARIANT
}

auto CommandLine::program_path() const -> fs::path
{
	TWIST_CHECK_INVARIANT
	return program_path_;
}

auto CommandLine::nof_proper_arguments() const -> Ssize
{
	TWIST_CHECK_INVARIANT
	return ssize(args_);
}

auto CommandLine::has_option(std::string_view option_name) const -> bool
{
	TWIST_CHECK_INVARIANT
	return has(args_, option_name);
}

auto CommandLine::get_option_value(std::string_view option_name) const -> std::string
{
	TWIST_CHECK_INVARIANT
	auto it = rg::find(args_, option_name);
	if (it == end(args_)) {
		TWIST_THRO2(L"Option \"{}\" not found.", ansi_to_string(option_name));
	}
	if (++it == end(args_)) {
		TWIST_THRO2(L"Option \"{}\" is not followed by any more arguments.", ansi_to_string(option_name));
	}
	return *it;
}

auto CommandLine::get_option_values(std::string_view option_name, Ssize nof_values) const -> std::vector<std::string>
{
	TWIST_CHECK_INVARIANT
	assert(nof_values > 0);

	auto it = rg::find(args_, option_name);
	if (it == end(args_)) {
		TWIST_THRO2(L"Option \"{}\" not found.", ansi_to_string(option_name));
	}

	auto values = reserve_vector<std::string>(nof_values);
	for ([[maybe_unused]] auto i : IndexRange{nof_values}) { 
		if (++it == end(args_)) {
			TWIST_THRO2(L"Option \"{}\" is not followed by enough arguments.", ansi_to_string(option_name));
		}
		values.push_back(*it);
	}
	return values;
}

#ifdef _DEBUG
auto CommandLine::check_invariant() const noexcept -> void
{
	assert(!program_path_.empty());
}
#endif

}

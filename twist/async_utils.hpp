/// @file async_utils.hpp
/// Asychronous programming utilities

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_ASYNC__UTILS_HPP
#define TWIST_ASYNC__UTILS_HPP

#include <exception>
#include <future>

#include "twist/feedback_providers.hpp"
#include "twist/os_utils.hpp"
#include "twist/scope.hpp"

namespace twist {

/*! The action to be taken by an object of the Worker class upon destruction, if the associated background thread 
    exists at that point and is in a joinable state (ie is running).
 */
enum class WorkerDtorAction { 
	join,  ///< Join the background thread (the destructor will not return until the thread has finished)
	detach ///< Detach the Worker object from the background thread (the destructor returns immediately)
};

/*! A background worker thread.
    A Worker object runs an arbitrary callable object on a separate thread. If an exception occurs and is not caught 
	within the function, the Worker object keeps hold of the exception, which can be retrieved via thrown_exception() 
	and which will be rethrown if get_result() is called. If a suitable feedback provider is passed into the 
	constructor, the exception will be reported to the parent thread.
    \tparam Result  The return type of the callable object which is run by the class
 */
template<class Result>
class Worker {
public:
	/*! Constructor. Use this constructor if you wish for uncaught exceptions to be reported back to the parent thread 
	     via a cross-thread provider.	
	     \param[in] dtor_action  The action to be taken by the object upon destruction  
	     \param[in] except_feedback_prov  Exception feedback provider; it must be a cross-thread feedback provider
	 */	
	Worker(WorkerDtorAction dtor_action, ExceptionFeedbackProv& except_feedback_prov);  

	/*! Constructor.	
	    \param[in] dtor_action  The action to be taken by the object upon destruction  
	 */	
	Worker(WorkerDtorAction dtor_action);  

	~Worker();

	/*! Run a callable object on a new thread. If a thread is currently running, the function will wait for it to 
	    finish first (this object will join that thread).	
	    \tparam Fn  The callable type; if its return type must match class template parameter Result, a 
					compile error occurs
	    \tparam Args  The call argument types
	    \param[in] func  The callable object
	    \param[in] args  The arguments for the call
	 */	
	template<class Fn, class... Args> 
	auto run(Fn&& func, Args&&... args) -> void;

	//! Whether the background worker thread is currently running.
	[[nodiscard]] auto working() const -> bool;

	/*! Retrieve the result of the callable object being run on the background worker thread.
	    If there is no background thread running, or the result has already been retrieved, an exception is thrown.
	    If the background thread completed with an uncaught exception, the exception is rethrown by this function.
	    If the background thread is still running, this function blocks the calling thread and waits until it the 
	    background thread is ready.	
	    \return  The callable object result value, if any; nothing is returned if Result is void
	 */	
	[[nodiscard]] auto get_result() -> Result;

	/*! Waits for the background thread to be ready for up to a specific (timeout) duration.
	    If the background thread is not yet ready, this function blocks the calling thread and waits until it is ready 
		or until the duration has elapsed, whichever happens first.
	    If there is no background thread running, or the result has already been retrieved, an exception is thrown.	
	    \tparam Rep  Arithmetic type representing the number of ticks in the duration
	    \tparam Period   A std::ratio representing the tick period (ie the number of seconds per tick)
	    \param[in] timeout_duration  The timeout duration
	    \return A value indicating what caused the callable object being run on the background thread to return
	 */	
	template<class Rep, class Period> 
	[[nodiscard]] auto wait_for_result(const std::chrono::duration<Rep, Period>& timeout_duration) 
	                    -> std::future_status;

	/*! Information about the exception thrown by the last call to run(), if any; a "null" pointer if no exception was 
	    thrown in the last call to run().
	 */
	[[nodiscard]] auto thrown_exception() const -> std::exception_ptr;

	/*! Terminate the working thread "natively"; that is, use facilities native to the OS to terminate the thread.
	    This is a brutal, dangerous function that should only be used in extreme cases; resources, locks, etc, 
		associated with the thread will be left dangling.
	 */
	[[nodiscard]] auto native_terminate() -> void;

	/*! Find out whether native_terminate() has been successfully called on this worker.	
	    \return  true if native_terminate() has successfully been called 
	 */
	[[nodiscard]] auto was_native_terminated() const -> bool;

	TWIST_NO_COPY_DEF_MOVE(Worker)

private:
	WorkerDtorAction dtor_action_;
	ExceptionFeedbackProv* feedback_prov_;

	std::unique_ptr<std::future<Result>> future_;
	std::unique_ptr<std::thread> thread_;
	bool was_native_terminated_{false};

	std::exception_ptr thrown_exception_;

	TWIST_CHECK_INVARIANT_DECL
};

// --- Free functions ---

/*! Run a callable object on a separate thread.
    Unlike a call to std::async() with the default launch policy, this function gurarantees that the callable object is 
    run on a separate thread.
    \tparam Fn  The callable type; if its return type must match class template parameter Result, a compile error 
	            occurs
    \tparam Args  The call argument types
    \param[in] func  The callable object
    \param[in] args  The arguments for the call
    \return  The return value of the call (if any)
 */
template<typename Fn, typename... Args> 
auto really_async(Fn&& func, Args&&... args);

/*! Run an action multiple times, distributing the runs on a given number of threads.
    \tparam Action  Callable type which runs the action
    \param action  The action, which will be called the given number of times, with one argument, which is the run 
	               index
    \param nof_runs  The number of times that the action should be run
    \param nof_threads  The number of threads; if greater than the number of times the action should be run, then the
	                    number of threads will equal the number of runs
*/
template<std::invocable<Ssize> Action>
auto run_on_threads(Action action, Ssize nof_runs, Ssize nof_threads = std::thread::hardware_concurrency()) -> void;

}

#include "async_utils.ipp"

#endif

/// @file PropertySet.hpp
/// PropertySet class

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_PROPERTY_SET_HPP
#define TWIST_PROPERTY_SET_HPP

#include "twist/ExplicitLibType.hpp"

namespace twist {

typedef twist::ExplicitLibType<unsigned long, 0, k_twist_lib_id, k_typeid_prop_id>  PropertyId;  // Property ID

static const PropertyId  k_no_prop_id(0);  // invalid property ID

typedef twist::ExplicitLibType<unsigned long, 0, k_twist_lib_id, k_typeid_prop_type>  PropertyType;  // Property value type

static const PropertyType  k_prop_type_none(0);    // invalid property type
static const PropertyType  k_prop_type_int(1);     // 32-bit integer value
static const PropertyType  k_prop_type_float(3);   // 64-bit floating-point value
static const PropertyType  k_prop_type_string(4);  // string value
static const PropertyType  k_prop_type_bool(5);    // boolean value


///
///  Class modelling a set of properties and their values.
///  A "property" is defined by an ID (unique within the set) and a type.
///  A property value is a value associated with a property, which has the type associated with that property. 
///  A property's value is always guaranteed to be set.
///
class PropertySet {
public:
	/// Constructor.
	///
	PropertySet();

	/// Copy constructor.
	///
	PropertySet(const PropertySet& src);

	/// Move constructor.
	///
	PropertySet(PropertySet&& src);

	/// Destructor.
	///
	~PropertySet();

	/// Copy-assignment operator.
	///
	PropertySet& operator=(const PropertySet& rhs);

	/// Move-assignment operator.
	///
	PropertySet& operator=(PropertySet&& rhs);

	/// Define a new property of "integer" type.
	///
	/// @param[in] prop_id  The property ID. An exception is thrown if a property with this ID already exists.
	/// @param[in] val  The initial property value.
	///
	void define_int_prop(PropertyId prop_id, int val);

	/// Define a new property of "floating-point number" type.
	///
	/// @param[in] prop_id  The property ID. An exception is thrown if a property with this ID already exists.
	/// @param[in] val  The initial property value.
	///
	void define_float_prop(PropertyId prop_id, double val);

	/// Define a new property of "string" type.
	///
	/// @param[in] prop_id  The property ID. An exception is thrown if a property with this ID already exists.
	/// @param[in] val  The initial property value.
	///
	void define_string_prop(PropertyId prop_id, const std::wstring& val);

	/// Define a new property of "boolean" type.
	///
	/// @param[in] prop_id  The property ID. An exception is thrown if a property with this ID already exists.
	/// @param[in] val  The initial property value.
	///
	void define_bool_prop(PropertyId prop_id, bool val);

	/// Define a new property.
	///
	/// @tparam  ValueT  The property value type.
	/// @param[in] prop_id  The property ID. An exception is thrown if a property with this ID already exists.
	/// @param[in] val  The initial property value.
	///
	template<typename ValueT> void define_prop(PropertyId prop_id, const ValueT& val);

	/// Get the value of a property of "integer" type.
	///
	/// @param[in] prop_id  The property ID. An exception is thrown if a property with this ID is not defined or is not 
	///				of "integer" type.
	/// @return  The value.
	///
	virtual int get_int_value(PropertyId prop_id) const;

	/// Set the value of a property of "integer" type.
	///
	/// @param[in] prop_id  The property ID. An exception is thrown if a property with this ID is not defined or is not 
	///				of "integer" type.
	/// @param[in] val  The value.
	///
	virtual void set_int_value(PropertyId prop_id, int val);

	/// Get the value of a property of "floating-point number" type.
	///
	/// @param[in] prop_id  The property ID. An exception is thrown if a property with this ID is not defined or is not 
	///				of "floating-point number" type.
	/// @return  The value.
	///
	virtual double get_float_value(PropertyId prop_id) const;

	/// Set the value of a property of "floating-point number" type.
	///
	/// @param[in] prop_id  The property ID. An exception is thrown if a property with this ID is not defined or is not 
	///				of "floating-point number" type.
	/// @param[in] val  The value.
	///
	virtual void set_float_value(PropertyId prop_id, double val);

	/// Get the value of a property of "string" type.
	///
	/// @param[in] prop_id  The property ID. An exception is thrown if a property with this ID is not defined or is not 
	///				of "string" type.
	/// @return  The value.
	///
	virtual std::wstring get_string_value(PropertyId prop_id) const;

	/// Set the value of a property of "string" type.
	///
	/// @param[in] prop_id  The property ID. An exception is thrown if a property with this ID is not defined or is not 
	///				of "string" type.
	/// @param[in] val  The value.
	///
	virtual void set_string_value(PropertyId prop_id, std::wstring val);

	/// Get the value of a property of "boolean" type.
	///
	/// @param[in] prop_id  The property ID. An exception is thrown if a property with this ID is not defined or is not 
	///				of "boolean" type.
	/// @return  The value.
	///
	virtual bool get_bool_value(PropertyId prop_id) const;

	/// Set the value of a property of "boolean" type.
	///
	/// @param[in] prop_id  The property ID. An exception is thrown if a property with this ID is not defined or is not 
	///				of "boolean" type.
	/// @param[in] val  The value.
	///
	virtual void set_bool_value(PropertyId prop_id, bool val);

	/// Find out whether a property with a specific ID exists in the set.
	///
	/// @param[in] prop_id  The property ID.
	/// @return  true if the property exists.
	///
	bool prop_exists(PropertyId prop_id) const;

	/// Get the type of a specific property.
	///
	/// @param[in] prop_id  The property ID. An exception is thrown if a property with this ID is not defined.
	/// @return  The property type ID.
	///
	virtual PropertyType get_prop_type(PropertyId prop_id) const;

	/// Get information about all properties in the set.
	///
	/// @return  A map containing the IDs of all properties and the associated property type IDs.
	///
	virtual std::map<PropertyId, PropertyType> get_props() const;

private:	
	///
	///  Simple class storing a property value and its type.
	///  The value data members (one for each possible value type) are stored in a union, to minimise memory footprint (so 
	///    values of complex types are stored on the heap).
	///  The value accessors throw exceptions if called for the wrong value type.
	///
	class PropValue {
	public:
		PropValue(int val);
		PropValue(double val);
		PropValue(std::wstring val);
		PropValue(bool val);
		PropValue(const PropValue& src);
		PropValue(PropValue&& src);
		~PropValue();
		PropValue& operator=(const PropValue& rhs);
		PropValue& operator=(PropValue&& rhs);
		PropertyType type() const;
		int int_val() const;
		double float_val() const;
		std::wstring str_val() const;
		bool bool_val() const;
	private:
		PropertyType  type_;
		union {
			int  int_val_;
			double  float_val_;
			const std::wstring*  str_val_;
			bool  bool_val_;
		};	
		TWIST_CHECK_INVARIANT_DECL
	};
	
	/// Set the value of a property.
	///
	/// @tparam  ValueT  The value type.
	/// @param[in] prop_id  The property ID. An exception is thrown if a property with this ID is not defined or is not 
	///				of the correct type type.
	/// @param[in] prop_type  The property type ID.
	/// @param[in] val  The value.
	///
	template<typename ValueT> void set_value(PropertyId prop_id, PropertyType prop_type, const ValueT& val);

	/// Get the value object of a property.
	///
	/// @param[in] prop_id  The property ID. An exception is thrown if a property with this ID is not defined.
	/// @return  The value object.
	///
	const PropValue& get_value(PropertyId prop_id) const;
	
	std::map<PropertyId, PropValue>  prop_values_;

	TWIST_CHECK_INVARIANT_DECL
};

} 

#include "twist/PropertySet.ipp"

#endif 

///  @file  PrefsFile.hpp  
///	 PrefsFile class 

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_PREFS_FILE_HPP
#define TWIST_PREFS_FILE_HPP

namespace twist {

namespace db {
class XmlDoc;
class XmlElemNode;
}

///
///  Abstract base class for classes which manage (versioned) preference files using the XML format.
///
class PrefsFile : public NonCopyable {
public:
	/// Destructor.
	///
	virtual ~PrefsFile() = 0;

protected:
	/// Constructor.
	///
	/// @param[in] path  The preferences file path
	/// @param[in] root_xnode_name  The tag of the root node of the XML document
	/// @param[in] current_version  The currently supported version of the preferences file
	/// @param[in] must_exist  If true, the constructor will throw an exception if the file is not found; 
	///					otherwise the class will create the file if and when needed
	///	
	PrefsFile(const fs::path& path, const std::wstring& root_xnode_name, int current_version, 
			bool must_exist);

	/// The preferences file path.
	fs::path path() const;

	/// The XML document.
	const db::XmlDoc& get_xdoc() const;

	/// The XML document.
	db::XmlDoc& get_xdoc();

	/// The root node of the XML document.
	const db::XmlElemNode& get_root_xnode() const;

	/// Given a parent XML node, find its unique child node with a specific tag, and retrieve that child node's "text subnode" 
	/// value. If the parent has more than one child nodes with the specified tag, an exception is thrown. If it has none, the 
	/// method returns false.
	///
	/// @param[in] parent  The parent XML node.
	/// @param[in] child_tag  The child node tag
	/// @param[in] child_val  The value of the child node's text subnode.
	/// @return  true if the unique child node has been found and it has a text subnode.
	/// 
	bool get_unique_child_node_text(const db::XmlElemNode& parent, const std::wstring& child_tag, std::wstring& child_val) const;

	/// Given a parent XML node, find its unique child node with a specific tag, and set that child node's "text subnode" value.
	/// If the parent has more than one child nodes with the specified tag, an exception is thrown.
	/// If it has none, the method creates it.
	///
	/// @param[in] parent  The parent XML node.
	/// @param[in] child_tag  The child node tag
	/// @param[in] child_val  The value of the child node's text subnode.
	/// 
	void set_unique_child_node_text(const db::XmlElemNode& parent, const std::wstring& child_tag, const std::wstring& child_val);

	/// Given a parent XML node, find its unique child node with a specific tag, and retrieve that child node's "text subnode" 
	/// value and convert it to integer. If the parent has more than one child nodes with the specified tag, an exception is 
	/// thrown. If it has none, the method returns false.
	///
	/// @param[in] parent  The parent XML node.
	/// @param[in] child_tag  The child node tag
	/// @param[in] child_val  The value of the child node's text subnode, converted to integer.
	/// @return  true if the unique child node has been found and it has a text subnode.
	/// 
	bool get_unique_child_node_text(const db::XmlElemNode& parent, const std::wstring& child_tag, bool& child_val) const;

	/// Given a parent XML node, find its unique child node with a specific tag, and set that child node's "text subnode" value 
	/// so that it represents a boolean value. If the parent has more than one child nodes with the specified tag, an exception 
	/// is thrown. If it has none, the method creates it.
	///
	/// @param[in] parent  The parent XML node.
	/// @param[in] child_tag  The child node tag
	/// @param[in] child_val  The value of the child node's text subnode, as a boolean.
	/// 
	void set_unique_child_node_text(const db::XmlElemNode& parent, const std::wstring& child_tag, bool child_val);

	/// Given a parent XML node, find its unique child node with a specific tag, and retrieve that child node's "text subnode" 
	/// value and convert it to a boolean. If the parent has more than one child nodes with the specified tag, an exception is 
	/// thrown. If it has none, the method returns false.
	///
	/// @param[in] parent  The parent XML node.
	/// @param[in] child_tag  The child node tag
	/// @param[in] child_val  The value of the child node's text subnode, converted to a boolean.
	/// @return  true if the unique child node has been found and it has a text subnode.
	/// 
	bool get_unique_child_node_text(const db::XmlElemNode& parent, const std::wstring& child_tag, int& child_val) const;

	/// Given a parent XML node, find its unique child node with a specific tag, and set that child node's "text subnode" value 
	/// so that it represents an integer value. If the parent has more than one child nodes with the specified tag, an exception 
	/// is thrown. If it has none, the method creates it.
	///
	/// @param[in] parent  The parent XML node.
	/// @param[in] child_tag  The child node tag
	/// @param[in] child_val  The value of the child node's text subnode, as an integer.
	/// 
	void set_unique_child_node_text(const db::XmlElemNode& parent, const std::wstring& child_tag, int child_val);

	/// Search for an unique element node that is a child of a specific XML node, and has a specific tag
	/// If no such unique node exists, it is created.
	/// If multiple nodes with the specified tag exist as children of the parent XML node, then an exception is thrown.
	///
	/// @param[in] parent  The parent XML node.	
	/// @param[in] tag  The tag.
	/// @return  The child element node.
	///
	twist::db::XmlElemNode ensure_unique_child_with_tag(const db::XmlElemNode& parent, const std::wstring& tag);

private:
	const fs::path  path_;
	const int  current_version_;

	std::unique_ptr<db::XmlDoc>  xdoc_;
	std::unique_ptr<db::XmlElemNode>  root_xnode_;

	TWIST_CHECK_INVARIANT_DECL
};

} // namespace twist

#endif // TWIST_PREFS_FILE_H

///  @file  File.cpp
///  Implementation file for "File.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "File.hpp"

#include "file_io.hpp"

#if TWIST_OS_WIN
  #include <windows.h>
  #include "os_utils.hpp"
#endif

namespace twist {

// --- FileStd class ---

FileStd::FileStd(fs::path path, FileOpenMode open_mode) 
	: path_{std::move(path)}
	, open_mode_{open_mode}
{
	// Open file
	std::ios_base::openmode mode{};

	switch (open_mode_) {
	case FileOpenMode::read: 
		mode = std::ios_base::in | std::ios_base::binary;
		break;	
	case FileOpenMode::write: 
		mode = std::ios_base::out | std::ios_base::binary;
		break;	
	case FileOpenMode::read_write: 
		mode = std::ios_base::in | std::ios_base::out | std::ios_base::binary;
		break;	
	case FileOpenMode::create_write: 
		mode = std::ios_base::out | std::ios_base::trunc | std::ios_base::binary;
		break;	
	case FileOpenMode::create_read_write: 
		mode = std::ios_base::in | std::ios_base::out | std::ios_base::trunc | std::ios_base::binary;
		break;	
	default: 
		TWIST_THROW(L"Unrecognised file open mode value %d.", open_mode_);
	}

	stream_.open(path_.string().c_str(), mode);
	if (!stream_.is_open()) { 
		TWIST_THRO2(L"Could not open file \"{}\" - {}", path_.wstring(), get_strerror());
	}

	TWIST_CHECK_INVARIANT
}


FileStd::~FileStd()
{
	TWIST_CHECK_INVARIANT
}


fs::path FileStd::path() const
{
	TWIST_CHECK_INVARIANT
	return path_;
}

	
std::int64_t FileStd::get_file_size() const
{
	TWIST_CHECK_INVARIANT
	return file_size(path_);
}


std::int64_t FileStd::get_file_pos() const
{
	TWIST_CHECK_INVARIANT
	return stream_.tellg();  
}


void FileStd::seek(long long offset, FileSeekDir seek_dir) const
{
	TWIST_CHECK_INVARIANT
	std::ios_base::seekdir way{};

	switch (seek_dir) {
	case seek_begin: 
		way = std::ios_base::beg;
		break;
	case seek_current:
		way = std::ios_base::cur;
		break;
	case seek_end:
		way = std::ios_base::end;
		break;
	default: 
		TWIST_THROW(L"Invalid value %d for seek direction argument.", seek_dir);
	}

	if (!stream_.seekg(offset, way)) {
		TWIST_THROW(L"Error setting the file pointer for file \"%s\" - %s", 
				wfilename().c_str(), get_strerror().c_str());
	}
}


void FileStd::read_buf(char* buffer, Ssize count) const
{
	TWIST_CHECK_INVARIANT
	// Compute how many full blocks are needed to read the whole buffer
	const auto full_block_count = static_cast<Ssize>(count / opt_file_block_size);	
	// Work out the size of the last, incomplete block (if there is one)
	const auto last_block_bytes = static_cast<Ssize>(count - full_block_count * opt_file_block_size);

	int pos{};

	// Read full blocks
	for (auto i = 0; i < full_block_count; ++i) {	
		if (!stream_.read(buffer + pos, opt_file_block_size)) {
			TWIST_THRO2(L"Error reading from file \"{}\" - only {} bytes could be read: {}",
					    wfilename(), stream_.gcount(), get_strerror());
		}
		pos += opt_file_block_size;		
	}
	// Read last block
	if (last_block_bytes > 0) {
		if (!stream_.read(buffer + pos, last_block_bytes)) {
			TWIST_THRO2(L"Error reading from file \"{}\" - only {} bytes could be read: {}",
					    wfilename(), stream_.gcount(), get_strerror());
		}
	}		
}


void FileStd::write_buf(const char* buffer, Ssize count)
{
	TWIST_CHECK_INVARIANT
	// Compute how many full blocks are needed to write the whole buffer
	const auto full_block_count = static_cast<Ssize>(count / opt_file_block_size);	
	// Work out the size of the last, incomplete block (if there is one)
	const auto last_block_bytes = static_cast<Ssize>(count - full_block_count * opt_file_block_size);

	auto pos = 0;
	
	// Write full blocks
	for (auto i = 0; i < full_block_count; ++i) {		
		if (!stream_.write(buffer + pos, opt_file_block_size)) {
			TWIST_THROW(L"Error writing to file \"%s\" - %s", 
					wfilename().c_str(), get_strerror().c_str());
		}
		pos += opt_file_block_size;		
	}
	// Write last block
	if (last_block_bytes > 0) {
		if (!stream_.write(buffer + pos, last_block_bytes)) {
			TWIST_THROW(L"Error writing to file \"%s\" - %s", 
					wfilename().c_str(), get_strerror().c_str());
		}
	}		
}
	

std::wstring FileStd::wfilename() const
{
	TWIST_CHECK_INVARIANT
	return path_.filename().wstring();
}


#ifdef _DEBUG
void FileStd::check_invariant() const noexcept
{
	assert(!path_.empty());
	assert((open_mode_ >= FileOpenMode::read) && (open_mode_ <= FileOpenMode::create_read_write));
	assert(stream_.is_open());
}
#endif 

#if TWIST_OS_WIN

//
//  FileWin class
//

FileWin::FileWin(fs::path path, FileOpenMode open_mode) 
	: path_{ std::move(path) }
	, open_mode_{ open_mode }
{
	// Open file
	// const auto long_filename{ make_long_filename(path_.wstring()) };  +TODO: Do we need this?
	const auto long_filename = path_.wstring();
	HANDLE hFile{ INVALID_HANDLE_VALUE };

	switch (open_mode_) {
	case FileOpenMode::read: 
		hFile = ::CreateFileW(long_filename.c_str(), GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, 
				nullptr, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);
		break;	
	case FileOpenMode::write: 
		hFile = ::CreateFileW(long_filename.c_str(), GENERIC_WRITE, FILE_SHARE_READ, nullptr, 
				OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);
		break;	
	case FileOpenMode::read_write: 
		hFile = ::CreateFileW(long_filename.c_str(), GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ, 
				nullptr, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);
		break;	
	case FileOpenMode::create_write: 
		hFile = ::CreateFileW(long_filename.c_str(), GENERIC_WRITE, 0, nullptr, CREATE_ALWAYS, 
				FILE_ATTRIBUTE_NORMAL, nullptr);
		break;	
	case FileOpenMode::create_read_write: 
		hFile = ::CreateFileW(long_filename.c_str(), GENERIC_READ | GENERIC_WRITE, 0, nullptr, 
				CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, nullptr);
		break;	
	}
	if (hFile == INVALID_HANDLE_VALUE) {
		TWIST_THROW(L"Could not open file \"%s\" - %s", 
				wfilename().c_str(), get_last_os_err().c_str());
	}	
	
	handle_ = hFile;
	TWIST_CHECK_INVARIANT
}


FileWin::~FileWin()
{
	::CloseHandle(handle_);
	TWIST_CHECK_INVARIANT
}


fs::path FileWin::path() const
{
	TWIST_CHECK_INVARIANT
	return path_;
}

	
std::int64_t FileWin::get_file_size() const
{
	TWIST_CHECK_INVARIANT

	_LARGE_INTEGER fileSizeLarge;
	::ZeroMemory(&fileSizeLarge, sizeof(_LARGE_INTEGER));		
	if (::GetFileSizeEx(handle_, &fileSizeLarge) == FALSE) {
		TWIST_THROW(L"Could not read size of file \"%s\" - %s", 
				wfilename().c_str(), get_last_os_err().c_str());
	}

	return fileSizeLarge.QuadPart;	
}


std::int64_t FileWin::get_file_pos() const
{
	TWIST_CHECK_INVARIANT
	_LARGE_INTEGER offsetStruct;
	::ZeroMemory(&offsetStruct, sizeof(_LARGE_INTEGER));		

	_LARGE_INTEGER posStruct;
	::ZeroMemory(&posStruct, sizeof(_LARGE_INTEGER));		
	
	if (::SetFilePointerEx(handle_, offsetStruct, &posStruct, FILE_CURRENT) == FALSE) {
		TWIST_THROW(L"Error quering the file pointer position for file \"%s\" - %s", 
				wfilename().c_str(), get_last_os_err().c_str());
	}
	
	return posStruct.QuadPart;
}


void FileWin::seek(long long offset, FileSeekDir seek_dir) const
{
	TWIST_CHECK_INVARIANT
	// Move the file pointer to the desired position

	DWORD moveMethod = 0;
	if (seek_dir == seek_begin) {
		moveMethod = FILE_BEGIN;
	}
	else if (seek_dir == seek_current) {
		moveMethod = FILE_CURRENT;
	}
	else if (seek_dir == seek_end) {
		moveMethod = FILE_END;	
	}
	else {
		TWIST_THROW(L"Invalid value for seek direction argument.");
	}
	
	_LARGE_INTEGER offsetStruct;
	::ZeroMemory(&offsetStruct, sizeof(_LARGE_INTEGER));		
	offsetStruct.QuadPart = offset;
		
	if (::SetFilePointerEx(handle_, offsetStruct, nullptr, moveMethod) == FALSE) {
		TWIST_THROW(L"Error setting the file pointer for file \"%s\" - %s", 
				wfilename().c_str(), get_last_os_err().c_str());
	}
}


void FileWin::read_buf(char* buffer, Ssize count) const
{
	TWIST_CHECK_INVARIANT
	// Compute how many full blocks are needed to read the whole buffer.
	const auto full_block_count = static_cast<DWORD>(count / opt_file_block_size);	
	// Work out the size of the last, incomplete block (if there is one).
	const auto last_block_bytes = static_cast<DWORD>(count - full_block_count * opt_file_block_size);

	DWORD writePos = 0;
	DWORD bytesRead = 0;

	// Read full blocks.
	for (DWORD i = 0; i < full_block_count; ++i) {		
		if (::ReadFile(handle_, buffer + writePos, opt_file_block_size, &bytesRead, nullptr) == FALSE) {
			TWIST_THROW(L"Error reading from file \"%s\" - %s", 
					wfilename().c_str(), get_last_os_err().c_str());
		}
		writePos += opt_file_block_size;		
	}
	// Read last block.
	if (last_block_bytes > 0) {
		if (::ReadFile(handle_, buffer + writePos, last_block_bytes, &bytesRead, nullptr) == FALSE) {	
			TWIST_THROW(L"Error reading from file \"%s\" - %s", 
					wfilename().c_str(), get_last_os_err().c_str());
		}
	}		
}


void FileWin::write_buf(const char* buffer, Ssize count)
{
	TWIST_CHECK_INVARIANT
	// Compute how many full blocks are needed to write the whole buffer
	const auto full_block_count = static_cast<DWORD>(count / opt_file_block_size);	
	// Work out the size of the last, incomplete block (if there is one)
	const auto last_block_bytes = static_cast<DWORD>(count - full_block_count * opt_file_block_size);

	DWORD writePos = 0;
	DWORD bytes_written = 0;
	
	// Write full blocks
	for (DWORD i = 0; i < full_block_count; ++i) {		
		if (::WriteFile(handle_, buffer + writePos, opt_file_block_size, &bytes_written, nullptr) == FALSE) {
			TWIST_THROW(L"Error writing to file \"%s\" - %s", wfilename().c_str(), 
					get_last_os_err().c_str());
		}
		writePos += opt_file_block_size;		
	}
	// Write last block
	if (last_block_bytes > 0) {
		if (::WriteFile(handle_, buffer + writePos, last_block_bytes, &bytes_written, nullptr) == FALSE) {	
			TWIST_THROW(L"Error writing to file \"%s\" - %s", wfilename().c_str(), 
					get_last_os_err().c_str());
		}
	}		
}


std::wstring FileWin::wfilename() const
{
	return path_.filename().wstring();
}
	

#ifdef _DEBUG
void FileWin::check_invariant() const noexcept
{
	assert(!path_.empty());
	assert((open_mode_ >= FileOpenMode::read) && (open_mode_ <= FileOpenMode::create_read_write));
	assert(handle_ != nullptr);
}
#endif 

#endif 

} 

/// @file os_utils.hpp
/// Operating system-specific utilities

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_OS__UTILS_HPP
#define TWIST_OS__UTILS_HPP

#include <thread>

#include "twist/FileVersionInfo.hpp"

namespace twist {
class DateTime;
}

namespace twist {

//! Special directory IDs
enum class SpecialDir {
	common_app_data =  1, ///< The application data directory common to all users
	sys_temp        =  2  ///< The system's directory designated for temporary files
};

//! This class encapsulates a UUID (universally unique identifier) value.
class Uuid {
public:
	/// Constructor. Generates a new, valid UUID.
	///
	Uuid();

	/// Constructor. Creates an object based on the textual representation of a UUID.
	///
	/// @param[in] uuid_str  The a textual UUID.
	///
	explicit Uuid(const std::wstring& uuid_str);
	
	/// Copy constructor.
	///
	Uuid(const Uuid& src);

	/// Move constructor.
	///
	Uuid(Uuid&& src);

	/// Destructor.
	///
	~Uuid();

	/// Assignment operator.
	///
	Uuid& operator=(const Uuid& rhs);

	/// Move-assignment operator.
	///
	Uuid& operator=(Uuid&& rhs);

	/// "Equal to" operator
	///
	bool operator==(const Uuid& rhs) const;
	
	/// "Not equal to" operator.
	///
	bool operator!=(const Uuid& rhs) const;
	
	/// "Smaller than" operator.
	///
	bool operator<(const Uuid& rhs) const;

private:	
	friend std::wstring to_str(const Uuid&, bool);

	struct UuidImpl;

	std::unique_ptr<UuidImpl>  uuid_impl_;

	TWIST_CHECK_INVARIANT_DECL
};

/// Get the textual representation of a UUID.
///
/// @param[in] uuid  The UUID.
/// @param[in] brackets  Whether the guid string should be enclosed in curly brackets.
/// @return  The textual UUID.
///
std::wstring to_str(const Uuid& uuid, bool brackets = false);

/// Create an invalid UUID value.
///
/// @return  The invalid UUID value
///
Uuid no_uuid();

/// Use the operating system to terminate a running thread.
///
/// @param[in] thread  Object wrapping the thread to terminate; this function will first detach the running 
///					thread from the std::thread object and then terminate the thread
///
void native_terminate_thread(std::thread& thread);

/// Ends the calling process and all its threads.
///
/// @param[in] exit_code  The exit code for the process and all threads
///
void exit_process(unsigned int exit_code);

/// Retrieve the file version information from a specific file (if the file contains version information).
///
/// @param[in] path  The file path
/// @return  The file version info, if the file contains version information; nullopt otherwise
///
std::optional<FileVersionInfo> get_file_version_info(const fs::path& path);

/*! Get the path to the currently running module (e.g. a DLL or an EXE).
    \return  Path to the module file
 */
[[nodiscard]] auto get_running_module_path() -> fs::path;

/// Find out whether a file is read only.
///
/// @param[in] path  The file path; an exception is thrown if the path does not exist or if it is a 
///					directory path
/// @return  The file name.
///
bool file_is_read_only(const fs::path& path);

/// Make sure a file has the "hidden" attribute turned on, on a Windows file system.
/// There is no comparable action on *nix systems (eg one way to hide files there is to to prepend a dot to 
///  the filename, which is not what this function is about) so this function will have no effect and return 
///	 false if called on platforms other than Windows.
///
/// @param[in] path  The file path; an exception is thrown if the path does not exist or if it is a 
///					directory path
/// @return  true if successful
/// 
bool make_win_file_hidden(const fs::path& path);

/// Retrieve the date and time when a file was created, last accessed, and last modified.
///
/// @param[in] filename  The filename.
/// @param[in] creationTime  The date and time the file was created. This parameter can be nullptr.
/// @param[in] lastAccessTime  The date and time the file was last accessed. This parameter can be nullptr.
/// @param[in] lastWriteTime  The date and time the file was written to, truncated, or overwritten. This date 
///					and time is not updated when file attributes or security descriptors are changed. This 
///					parameter can be nullptr.
///
void file_get_time(const fs::path& filename, DateTime* creationTime, DateTime* lastAccessTime, 
		DateTime* lastWriteTime);

/*! Get the path to a "special" directory. An exception is thrown if the directory cannot be identified.
    \param[in] dir_id  The special directory ID
    \return  The directory path
 */
[[nodiscard]] auto get_special_dir(SpecialDir dir_id) -> fs::path;

/// Get the decimal separator character in the current operating system locale.
/// Bear in mind that this is not necessarily the same as the C runtime library locale.
///
/// @return  The decimal separator
///
wchar_t get_os_decimal_sep();

/// Get a description of the last error triggered by calling the operating system API.
///
/// @return  The description.
///
std::wstring get_last_os_err(); 

//! Convert a wide-char (Unicode) string to a multi-byte string.
[[nodiscard]] auto wchar_to_multibyte_string(const wchar_t* wstr) -> std::vector<char>;

/*! Output a message to the debug stream.
    \param[in] msg  The message
 */
auto out_debug_str(const std::wstring& msg) -> void; 

/*! Output a message to the debug stream.
    \param[in] msg  The message
 */
auto out_debug_str(const std::string& msg) -> void; 

/// Output a formatted message to the debug stream.
///
/// @param[in] format  The message format; the following arguments (if any) must match it, with the 
///					formatting rules being the same as those for printf()
///
void outf_debug_str(const wchar_t* format, ...);

/// Output a formatted message to the debug stream.
///
/// @param[in] format  The message format; the following arguments (if any) must match it, with the 
///					formatting rules being the same as those for printf()
///
void outf_debug_str(const char* format, ...);

} 

#endif 

/// @file Singleton.ipp
/// Inline implementation file for "Singleton.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace twist {

template<class T> 
std::unique_ptr<T> Singleton<T>::singleton__{};


template<class T>
template<typename... Args> 
auto Singleton<T>::init(Args... args) -> void
{
	set_singleton(std::unique_ptr<T>{ new T{ std::forward<Args>(args)... } });
}


template<class T>
void Singleton<T>::term()
{
	reset_singleton();
}


template<class T>
T& Singleton<T>::get() 
{
	if (!singleton__) {
		TWIST_THROW(L"The singleton object has not been set.");
	}
	return *singleton__;
}


template<class T>
bool Singleton<T>::exists() 
{
	return static_cast<bool>(singleton__);
}


template<class T>
Singleton<T>::Singleton() 
{
	if (singleton__) {
		TWIST_THROW(L"The singleton object has already been created.");
	}
}


template<class T>
Singleton<T>::~Singleton()
{
}


template<class T>
void Singleton<T>::set_singleton(std::unique_ptr<T> singleton) 
{
	if (singleton__) {
		TWIST_THROW(L"The singleton object has already been set.");
	}
	singleton__ = move(singleton);
}


template<class T>
void Singleton<T>::reset_singleton() 
{
	if (!singleton__) {
		TWIST_THROW(L"The singleton object has not been set.");
	}
	singleton__.reset();
}

} 

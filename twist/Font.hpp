/// @file Font.hpp
/// Font class

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_FONT_HPP
#define TWIST_FONT_HPP

namespace twist {

//! Font weight
enum class FontWeight {
	normal, ///< Normal
	bold, ///< Bold
};

//! Font style
enum class FontStyle {
	normal, ///< Normal
	italic, ///< Italic
};

//! A simple font class storing a font's main properties.
class Font {
public:
	/*! Constructor.
		\param[in] family  The font family (eg Arial)
		\param[in] point_size  The font point size
		\param[in] weight  The font weight
		\param[in] style  The font style
	 */
	Font(std::wstring family, int point_size, FontWeight weight, FontStyle style);

	/// The font family.
	[[nodiscard]] auto family() const -> std::wstring;

	//! The font point size.
	[[nodiscard]] auto point_size() const -> int;

	//! The font weight.
	[[nodiscard]] auto  weight() const -> FontWeight;
	
	//! The font style.
	[[nodiscard]] auto style() const -> FontStyle;
	
	//! Whether the font is underlined.
	[[nodiscard]] auto underlined() const -> bool;

	/*! Set whether the font is underlined.
	    \param[in]  underlined  true for underlined
	 */
	auto set_underlined(bool underlined) -> void;

private:	
	std::wstring family_;
	int point_size_;
	FontWeight weight_;
	FontStyle style_;
	bool underlined_;

	TWIST_CHECK_INVARIANT_DECL
};

} 

#endif

/// @file OptionSet.hpp
/// OptionSet class template

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_OPTION_SET_HPP
#define TWIST_OPTION_SET_HPP

#include "twist/concepts.hpp"

#include <ranges>

namespace twist {

/*! This class models a set of options. 
    \tparam  Opt  The type of an option value
 */
template<class Opt>
class OptionSet {
public:
	using OptionRange = rg::subrange<typename std::set<Opt>::const_iterator>;

	//! Constructor. Constructs an empty set.
	OptionSet();

	//! Constructor. Construct a set containing the single option \p opt.
	OptionSet(Opt opt);

	/*! Constructor. Construct a set containing mutliple options, read from the range \p options.
	    If \p options contains duplicate values, an exception is thrown.
	 */
	OptionSet(std::initializer_list<Opt> options);

	/*! Add an option to the set.
	    \param[in] option  The new option; if it already exists in the set, an exception is thrown
	 */
	auto add(Opt option) -> void;

	/*! Whether the set contains a specific option.
	    \param[in] option  The option
	    \return  true only if it does
	 */
	[[nodiscard]] auto has(Opt option) const -> bool;

	/*! Get the size of the set.
	    \return  The size
	 */
	[[nodiscard]] auto size() const -> Ssize;

	//! Whether the set is empty.
	[[nodiscard]] auto is_empty() const -> bool;

	//! A view of all the options in the set.
	[[nodiscard]] auto all() const -> OptionRange;

	/*! Whether all option values in the set satisfy the predicate \p pred. 
	    Returns true if the set is empty.
	 */
	template<InvocableR<bool, Opt> Pred>
	[[nodiscard]] auto test_all(Pred pred) const -> bool;

private:
	std::set<Opt> set_;
	
	TWIST_CHECK_INVARIANT_DECL
};

}

#include "twist/OptionSet.ipp"

#endif

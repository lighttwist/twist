///  @file  twist_maker.hpp
///  Common header file for the "twist" library, includes generally useful headers
///  If it is desired that "twist" should use a precompiled header file, then this file should be it

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_MAKER_HPP
#define TWIST_MAKER_HPP

#include "targetver.hpp"

// C and C++ standard library essentials
#include <algorithm>
#include <array>
#include <cassert>
#include <concepts>
#include <cstdint>
#include <filesystem>
#include <functional>
#include <future>
#include <map>
#include <memory>
#include <optional>
#include <ranges>
#include <set>
#include <sstream>
#include <string_view>
#include <tuple>
#include <unordered_set>
#include <vector>

// Throw a gsl::fail_fast exception when pre/post conditions on the GSL types are violated
#define GSL_THROW_ON_CONTRACT_VIOLATION 
// The entire GSL ("Guidelines Support Library")
#include <gsl/gsl>

// In case Qt is used, get rid of "Release" build warnings
#ifndef _DEBUG
	#undef  Q_ASSERT
	#define Q_ASSERT(x) __noop
	#undef  Q_ASSERT_X
	#define Q_ASSERT_X(cond, where, what) __noop
#endif

#define WIN32_LEAN_AND_MEAN  // Exclude rarely-used stuff from Windows headers
#define VC_EXTRALEAN  // Exclude rarely-used stuff from Windows headers

// Get rid of Microsoft's inane min/max macros
#define NOMINMAX

// "twist" library essentials
#include "twist/namespace_aliases.hpp"
#include "twist/globals.hpp"
#include "twist/cpp20_ranges_utils.hpp"
#include "twist/IndexRange.hpp"
#include "twist/metaprogramming.hpp"
#include "twist/meta_ranges.hpp"
#include "twist/NonCopyable.hpp"
#include "twist/RuntimeError.hpp"
#include "twist/std_vector_utils.hpp"
#include "twist/string_utils.hpp"
#include "twist/twist_stl.hpp"

#endif 


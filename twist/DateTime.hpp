/// @file DateTime.hpp
/// DateTime class definition

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_DATE_TIME_HPP
#define TWIST_DATE_TIME_HPP

#include "twist/Date.hpp"

namespace twist {

/*! Date-time class; extends the Date class with basic time functionality.
    The time consists of:
	   Seconds after the minute - [0, 59]
	   Minutes after the hour - [0, 59] 
	   Hours since midnight - [0, 23]
 */
class DateTime : public Date {
public:
	//! Constructor. Initialises the date-time to the 1st of January, year 1, 00:00:00.
	DateTime();

	/*! Constructor. Initialises the time to 00:00:00.
	    \param[in] day  The day-of-the-month; between 1 and 31
	    \param[in] month  The month; 1 = January, 2 = February, 12 = December
	    \param[in] year  The year
	 */
	DateTime(int day, int month, int year);

	/*! Constructor.
	    \param[in] day  The day-of-the-month; between 1 and 31
	    \param[in] month  The month; 1 = January, 2 = February, 12 = December
	    \param[in] year  The year
	    \param[in] hour  The hour; a value in the interval [0, 23]
	    \param[in] minute  The minute; a value in the interval [0, 59]
	    \param[in] second  The second; a value in the interval [0, 59]
	 */
	DateTime(int day, int month, int year, int hour, int minute, int second);

	/*! Constructor.
	    \param[in] src  The source date value; the date is copied and the time components are set to zero  
	 */
	explicit DateTime(const Date& src);

	/*! Constructor.
	    \param[in] date  The date values
	    \param[in] hour  The hour; a value in the interval [0, 23]
	    \param[in] minute  The minute; a value in the interval [0, 59]
	    \param[in] second  The second; a value in the interval [0, 59]
	 */
	DateTime(const Date& date, int hour, int minute, int second);

	DateTime(const DateTime& src);

	virtual ~DateTime();
	
	DateTime& operator=(const DateTime& rhs);
	
	/*! Assignment operator.
	    \param[in] rhs  The right-hand-side date value; the date is copied and the time components are set to zero  
	 */
	virtual DateTime& operator=(const Date& rhs) override;
	
	//! The hour; a value in the interval [0, 23]
	[[nodiscard]] int hour() const;	
	
	//! The minute; a value in the interval [0, 59]
	[[nodiscard]] int minute() const;
		
	//! The second; a value in the interval [0, 59]
	[[nodiscard]] int second() const;
	
private:	
	bool check_valid_time(bool throw_if_invalid = true) const;

	TinyInt hour_;
	TinyInt minute_;     
	TinyInt second_;     	

	TWIST_CHECK_INVARIANT_DECL
};

// --- Free functions ---

//! Time string formats
enum class TimeStrFormat {	
	hhcmm = 3, //< 24hr hour-minute clock, colon separator; eg 9:04/09:04 -> 9:04am, 21:04 -> 9:04pm   
	hhmm = 4, //< 24hr hour-minute clock, no separator, always 4 digits; eg 2104 -> 9:04pm, 03:45 -> 3:45am    
};

//! Date-time string formats
enum class DateTimeStrFormat { 	
	dd_mm_yyyy_hh_mm_ss = 1, //< eg 25[S1]02[S1]2007[S2]19[S3]45[S3]32 where [Sn] are customisable separator substrings  	    
	yyyy_mm_dd_hh_mm_ss = 2, //< eg 2007[S1]02[S1]25[S2]19[S3]45[S3]32 where [Sn] are customisable separator substrings  
};

//! Equality operator.
bool operator==(const DateTime& lhs, const DateTime& rhs);

//! Inequality operator.
bool operator!=(const DateTime& lhs, const DateTime& rhs);

/*! Smaller-than operator; returns true if the left-hand-side date-time value falls before the right-hand-side 
    date-time value.
 */
bool operator<(const DateTime& lhs, const DateTime& rhs);

/*! Smaller-or-equal operator; returns true if the left-hand-side date-time value falls before, or is the same 
    as, the right-hand-side date-time value.
 */
bool operator<=(const DateTime& lhs, const DateTime& rhs); 

//! Whether datetime values \p datetime1 and \p datetime2 specify the same dates.
[[nodiscard]] auto same_date(const DateTime& datetime1, const DateTime& datetime2) -> bool;

/*! Whether datetime value \p datetime is an "exact hour" (or exactly "on the hour"), ie the minute and second values 
    are zero.
 */
[[nodiscard]] auto is_exact_hour(const DateTime& datetime) -> bool;

//! Get the last datetime value which is an "exact hour" and falls before (or coincides with) \p datetime.
[[nodiscard]] auto floor_to_exact_hour(const DateTime& datetime) -> DateTime;

//! Get the first datetime value which is an "exact hour" and falls after (or coincides with) \p datetime.
[[nodiscard]] auto ceil_to_exact_hour(const DateTime& datetime) -> DateTime;

//! Get the system date-time.
[[nodiscard]] auto get_system_datetime() -> DateTime;

/*! Add or subtract a number of hours to or from a date-time value.
    \param[in] datetime  The date-time value  
    \param[in] hours  The number of hours; if positive, hours are being added to the date-time, otherwise hours are 
	                  being subtracted
    \return  The new date-time value
 */
[[nodiscard]] auto add_hours(const DateTime& datetime, int hours) -> DateTime;

/*! Add or subtract a number of minutes to or from a date-time value.
    \param[in] datetime  The date-time value  
    \param[in] minutes  The number of minutes; if positive, minutes are being added to the date-time, otherwise minutes 
	                    are being subtracted
    \return  The new date-time value
 */
[[nodiscard]] auto add_minutes(const DateTime& datetime, int minutes) -> DateTime;

/*! Add or subtract a number of seconds to or from a date-time value.
    \param[in] datetime  The date-time value  
    \param[in] seconds  The number of seconds; if positive, minutes are being added to the date-time, otherwise seconds 
	                    are being subtracted
    \return  The new date-time value
 */
[[nodiscard]] auto add_seconds(const DateTime& datetime, int64_t seconds) -> DateTime;

/*! Add or subtract a number of seconds to or from a date-time value.
    \param[in] datetime  The date-time value  
    \param[in] seconds  The number of seconds; if positive, minutes are being added to the date-time, otherwise seconds 
	                    are being subtracted
    \return  The new date-time value
 */
[[nodiscard]] auto add(const DateTime& datetime, std::chrono::seconds seconds) -> DateTime;

/*! Get the difference, in whole hours, between two date-time values.
    \param[in] datetime1  The first date-time value  
    \param[in] datetime2  The second date-time value  
    \return  The difference, in whole hours, between the date-time values; positive if the second value denotes a time 
	         later than the first   
 */
[[nodiscard]] auto diff_hours(const DateTime& datetime1, const DateTime& datetime2) -> int;

/*! Get the difference, in whole minutes, between two date-time values.
    \param[in] datetime1  The first date-time value  
    \param[in] datetime2  The second date-time value  
    \return  The difference, in whole minutes, between the datetime values; positive if the second value denotes a 
             time later than the first
 */
[[nodiscard]] int diff_minutes(const DateTime& datetime1, const DateTime& datetime2);

/*! Get the difference in seconds between two datetime values. 
    \param[in] datetime1  The first date-time value  
    \param[in] datetime2  The second date-time value  
    \return  The time difference, in seconds; positive if the second value denotes a time later than the first
 */
[[nodiscard]] auto diff_seconds(const DateTime& datetime1, const DateTime& datetime2) -> std::chrono::seconds::rep;

/*! Get the difference in seconds between a timepoint and datetime value (both UTC). 
    \param[in] start_timept  The start timepoint
    \param[in] end_time  The end datetime value
    \return  The time difference, in seconds; positive if the second value denotes a time later than the first
 */
[[nodiscard]] auto diff_seconds(std::chrono::system_clock::time_point start_timept, twist::DateTime end_time) 
					-> std::chrono::seconds::rep;

/*! Convert a UTC datetime value to a standard timepoint based on the (UTC) system clock. 
    \param[in] datetime  The datetime value (UTC)
    \return  The timepoint
 */
[[nodiscard]] auto datetime_to_utc_timepoint(const DateTime& datetime) -> std::chrono::system_clock::time_point;

/*! Convert a standard timepoint based on the (UTC) system clock to a date-time value. 
    \param[in] timept  The timepoint
    \return  The date-time value
 */
[[nodiscard]] DateTime utc_timepoint_to_datetime(const std::chrono::system_clock::time_point& timept);

/*! Convert a date-time value to a std::tm C structure.    
    \param[in] date_time  The date-time value; an exception is thrown if the date is before 1 Jan 1900
    \return  The std::tm structure
 */
[[nodiscard]] std::tm to_tm(const DateTime& date_time);

/*! Convert a std::tm C structure to a date-time value. 
    \param[in] timeinfo  The std::tm structure; an exception is thrown if it contains a negative year (ie it specifies 
	                     a date before 1 Jan 1900)
    \return  The date-time value
 */   
[[nodiscard]] DateTime from_tm(const std::tm& timeinfo);

/*! Get a textual representation of a time, in the format hour[sep]minute[sep]second, where [sep] is a 
    customisable separator.
    \param[in] date_time  The date-time object (the date info is ignored by this function)
    \param[in] pad  Whether the minute and second values should be given leading zero padding, if necessary
    \param[in] sep  The separator substring 
    \return  The text
 */
[[nodiscard]] std::wstring time_to_str(const DateTime& date_time, bool pad = true, 
		                               const std::wstring& sep = L":");

/*! Get a textual representation of a time, in the format hour[sep]minute, where [sep] is a 
    customisable separator.   
    \param[in] date_time  The date-time object (the date info is ignored by this function)
    \param[in] pad  Whether the minute value should be given leading zero padding, if necessary
    \param[in] sep  The separator substring 
    \return  The text
 */
[[nodiscard]] std::wstring hour_min_to_str(const DateTime& date_time, bool pad = true, 
		const std::wstring& sep = L":");

/*! Convert a string, which follows the format format hour[sep]min[sep]sec, into a time. 
    The separator string [sep] cannot be empty.   
    \param[in] str  The string; an exception is thrown if it is not a valid textual representation of a time
    \param[in] sep  The substring which separates the numeric time values in the string
    @return  The time; only the time is set on the date-time object
 */
[[nodiscard]] DateTime time_from_str(std::wstring_view str, std::wstring_view sep);

/*! Convert a string, which follows a format where numeric values are not separated, into a time. 
    The hour is expected to be in the interval [00, 23].   
    \param[in] str  The string; an exception is thrown if it is not a valid textual representation of a time
    \param[in] format  The string format; only the value 'hhmm' is currently allowed
    \return  The time; only the time is set on the date-time object
 */
[[nodiscard]] auto time_from_str(const std::wstring& str, TimeStrFormat format) -> DateTime;

/*! Get a textual representation of a date-time. The time values are always padded.
    \param[in] datetime  The date-time object
    \param[in] format  The string format
    \param[in] sep1  First separator substring; see the appropriate string format for the meaning of this separator
    \param[in] sep2  Second separator substring; see the appropriate string format for the meaning of this separator
    \param[in] sep3  Third separator substring; see the appropriate string format for the meaning of this separator
    \return  The text
 */
[[nodiscard]] auto date_time_to_str(const DateTime& datetime, 
                                    DateTimeStrFormat format, 
		                            const std::wstring& sep1 = L"-", 
									const std::wstring& sep2 = L" ", 
									const std::wstring& sep3 = L":") -> std::wstring;

/*! Convert a string (which follows a specific format) into a date-time. 
    \param[in] str  The string; an exception is thrown if it is not a valid textual representation of a date-time using 
	                the format specified 
    \param[in] format  The string format
    \param[in] sep1  The substring which separates the numeric date values in the string
    \param[in] sep2  The substring which separates the date substring from the time substring; it must be different 
	                 from the other two separators
    \param[in] sep3  The substring which separates the numeric time values in the string
    \return  The date-time value
 */ 
[[nodiscard]] auto datetime_from_str(std::wstring_view str, 
                                     DateTimeStrFormat format, 
									 std::wstring_view sep1, 
                                     std::wstring_view sep2, 
									 std::wstring_view sep3) -> DateTime;

/*! Transform a time value to the number of (whole) minutes elapsed since midnight.    
    \param[in] time  The time value; its date and second components are ignored
    \return  The number of minutes
 */
[[nodiscard]] auto minutes_since_midnight(const DateTime& time) -> int;

} 

#endif 

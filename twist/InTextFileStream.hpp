///  @file  InTextFileStream.hpp
///  InTextFileStream  class template, inherits InFileStream<>

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_IN_TEXT_FILE_STREAM_HPP
#define TWIST_IN_TEXT_FILE_STREAM_HPP

#include "InFileStream.hpp"

namespace twist {

///
///  An extension of the InFileStream class for streaming from a text file in an efficient manner.
///
///  @tparam   Chr  The character type in the input text file; only the char and wchar_t types are 
///					currenty supported
///
template<typename Chr>
class InTextFileStream : public InFileStream<Chr> {
public:
	/// Constructor.
	///
	/// @param[in] in_file  The input file. Must be open in read mode. The streaming will start from the 
	///					current position of its file pointer. 
	///	
	InTextFileStream(std::unique_ptr<FileStd> in_file);

	/// Destructor.
	///
	virtual ~InTextFileStream();

	/// Read a line of text from the file. This means a string starting from the current position and ending 
	/// when either the first newline character or the end of file is met.
	/// Note: This method is somewhat faster than the overload.
	///
	/// @param[in] line_buffer  Pointer to the beginning of a memory buffer where the contents of the line 
	///					will be stored. A null character is *not* appended at the end of the line by this 
	///					function.
	/// @param[in] buffer_len  The size of the buffer. If the line is longer than the buffer, then only a 
	///					part of it (as much as fits in the buffer) is read from the file.
	/// @param[in] line_len  The length of the line as read from the file.
	///
	void get_line(Chr* line_buffer, Ssize buffer_len, Ssize& line_len);

	/// Read a line of text from the file. This means a string starting from the current position and ending 
	/// when either the first newline character or the end of file is met.
	///
	/// @param[in] line  The line
	///
	void get_line(std::basic_string<Chr>& line);

private:	
	bool  last_was_carriage_ret_{};

	TWIST_CHECK_INVARIANT_DECL
};

} 

#include "InTextFileStream.ipp"

#endif 


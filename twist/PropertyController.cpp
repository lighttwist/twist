///  @file  PropertyController.cpp
///  Implementation file for "PropertyController.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "PropertyController.hpp"

#include "NamedPropertySet.hpp"

namespace twist {

//
//   PropertyConstraint  class
//

PropertyConstraint::PropertyConstraint(PropertyId prop_id, const NamedPropertySet& prop_set, PropertyConstraintType type)
	: prop_id_(prop_id)
	, prop_set_(&prop_set)
	, type_(type)
{
	TWIST_CHECK_INVARIANT
}


PropertyConstraint::~PropertyConstraint()
{
	TWIST_CHECK_INVARIANT
}


PropertyConstraintType PropertyConstraint::get_type() const
{
	TWIST_CHECK_INVARIANT
	return type_;
}


PropertyId PropertyConstraint::get_prop_id() const
{
	TWIST_CHECK_INVARIANT
	return prop_id_;
}


const NamedPropertySet& PropertyConstraint::get_prop_set() const
{
	TWIST_CHECK_INVARIANT
	return *prop_set_;
}


#ifdef _DEBUG
void PropertyConstraint::check_invariant() const noexcept
{
	assert(prop_id_ != k_no_prop_id);
	assert(prop_set_ != nullptr);
	assert(type_ != k_prop_constr_none);
}
#endif


//
//   PropConstraintNumOptions  class
//

PropConstraintNumOptions::PropConstraintNumOptions(PropertyId prop_id, const NamedPropertySet& prop_set, NumOptions num_options)
	: PropertyConstraint(prop_id, prop_set, k_prop_constr_num_options)
	, num_options_(num_options)
{
	if (prop_set.get_prop_type(prop_id) != k_prop_type_int) {
		TWIST_THROW(L"The \"numeric options\" constraint can only be applied to a property of integer type.");
	}
	TWIST_CHECK_INVARIANT
}


PropConstraintNumOptions::~PropConstraintNumOptions()
{
	TWIST_CHECK_INVARIANT
}


bool PropConstraintNumOptions::check_prop_val(int value, std::wstring& err) const
{
	TWIST_CHECK_INVARIANT
	bool ret = true;
	if (num_options_.count(value) == 0) {
		err = format_str(L"The value %d is not one of the allowed property options.", value);
		ret = false;
	}
	return ret;
}


std::wstring PropConstraintNumOptions::get_option_name(int option_value) const
{
	TWIST_CHECK_INVARIANT
	auto it = num_options_.find(option_value);
	if (it == end(num_options_)) {
		TWIST_THROW(L"Option value %d not found.", option_value);
	}
	return it->second;
}


std::vector<std::wstring> PropConstraintNumOptions::get_option_names() const
{
	TWIST_CHECK_INVARIANT
	return std::vector<std::wstring>(cbegin_second(num_options_), cend_second(num_options_));
}


int PropConstraintNumOptions::get_option_value(std::wstring option_name) const
{
	TWIST_CHECK_INVARIANT
	auto it = find_if(num_options_, [option_name](const std::pair<int, std::wstring>& el) {
		return el.second == option_name;
	});
	if (it == end(num_options_)) {
		TWIST_THROW(L"Option named \"%s\" not found.", option_name.c_str());
	}
	return it->first;
}


#ifdef _DEBUG
void PropConstraintNumOptions::check_invariant() const noexcept
{
	assert(!num_options_.empty());
}
#endif


//
//   PropConstraintIntNumBounds  class
//

PropConstraintIntNumBounds::PropConstraintIntNumBounds(PropertyId prop_id, const NamedPropertySet& prop_set, NumBounds num_bounds)
	: PropertyConstraint(prop_id, prop_set, k_prop_constr_int_num_bounds)
	, num_bounds_(num_bounds)
{
	if (prop_set.get_prop_type(prop_id) != k_prop_type_int) {
		TWIST_THROW(L"The \"integer numeric bounds\" constraint can only be applied to a property of integer type.");
	}
	TWIST_CHECK_INVARIANT
}


PropConstraintIntNumBounds::~PropConstraintIntNumBounds()
{
	TWIST_CHECK_INVARIANT
}


bool PropConstraintIntNumBounds::check_prop_val(int value, std::wstring& err) const
{
	TWIST_CHECK_INVARIANT
	bool ret = true;
	if (!contains(num_bounds_, value)) {
		err = format_str(
				L"The value %d does not fall within the allowed numeric bounds for the property.", value);
		ret = false;
	}
	return ret;
}


const PropConstraintIntNumBounds::NumBounds& PropConstraintIntNumBounds::get_num_bounds() const
{
	TWIST_CHECK_INVARIANT
	return num_bounds_;
}


#ifdef _DEBUG
void PropConstraintIntNumBounds::check_invariant() const noexcept
{
}
#endif


//
//   PropConstraintFloatNumBounds  class
//

PropConstraintFloatNumBounds::PropConstraintFloatNumBounds(PropertyId prop_id, const NamedPropertySet& prop_set, NumBounds num_bounds)
	: PropertyConstraint(prop_id, prop_set, k_prop_constr_float_num_bounds)
	, num_bounds_(num_bounds)
{
	if (prop_set.get_prop_type(prop_id) != k_prop_type_float) {
		TWIST_THROW(L"The \"floating-point numeric bounds\" constraint can only be applied to a property of floating-point type.");
	}
	TWIST_CHECK_INVARIANT
}


PropConstraintFloatNumBounds::~PropConstraintFloatNumBounds()
{
	TWIST_CHECK_INVARIANT
}


bool PropConstraintFloatNumBounds::check_prop_val(double value, std::wstring& err) const
{
	TWIST_CHECK_INVARIANT
	bool ret = true;
	if (!num_bounds_.contains(value)) {
		err = format_str(L"The value %f does not fall within the allowed numeric bounds for the property.", value);
		ret = false;
	}
	return ret;
}


const PropConstraintFloatNumBounds::NumBounds& PropConstraintFloatNumBounds::get_num_bounds() const
{
	TWIST_CHECK_INVARIANT
	return num_bounds_;
}


#ifdef _DEBUG
void PropConstraintFloatNumBounds::check_invariant() const noexcept
{
}
#endif


//
//  PropertyGroup  class
//

PropertyGroup::PropertyGroup(std::wstring name, CheckPropId check_prop_id)
	: name_(name)
	, prop_ids_()
	, check_prop_id_(check_prop_id)
{
	TWIST_CHECK_INVARIANT
}

		
PropertyGroup::PropertyGroup(const PropertyGroup& src)
	: name_(src.name_)
	, prop_ids_(src.prop_ids_)
	, check_prop_id_(src.check_prop_id_)
{
	TWIST_CHECK_INVARIANT
}


PropertyGroup& PropertyGroup::operator=(const PropertyGroup& rhs)
{
	TWIST_CHECK_INVARIANT
	if (&rhs != this) {
		name_ = rhs.name_;
		prop_ids_ = rhs.prop_ids_;
		check_prop_id_ = rhs.check_prop_id_;
	}
	TWIST_CHECK_INVARIANT
	return *this;
}


std::wstring PropertyGroup::get_name() const
{
	TWIST_CHECK_INVARIANT
	return name_;
}


const PropertyGroup::PropIds& PropertyGroup::get_prop_ids() const
{
	TWIST_CHECK_INVARIANT
	return prop_ids_;
}


void PropertyGroup::add_prop_id(PropertyId prop_id) 
{
	TWIST_CHECK_INVARIANT
	// First check that the new property ID is valid (an exception is thrown by the functor otherwise).
	check_prop_id_(prop_id);
	// Add the prop ID to the list.
	prop_ids_.push_back(prop_id);
}


#ifdef _DEBUG
void PropertyGroup::check_invariant() const noexcept
{
	assert(!name_.empty());
	assert(check_prop_id_);
}
#endif


//
//  PropertyFamily  class
//

PropertyFamily::PropertyFamily(PropertyId parent, const std::wstring& name, CheckPropId check_prop_id)
	: parent_(parent)
	, name_(name)
	, prop_ids_()
	, check_prop_id_(check_prop_id)
{
	TWIST_CHECK_INVARIANT
}

		
PropertyFamily::PropertyFamily(const PropertyFamily& src)
	: parent_(src.parent_)
	, name_(src.name_)
	, prop_ids_(src.prop_ids_)
	, check_prop_id_(src.check_prop_id_)
{
	TWIST_CHECK_INVARIANT
}


PropertyFamily& PropertyFamily::operator=(const PropertyFamily& rhs)
{
	TWIST_CHECK_INVARIANT
	if (&rhs != this) {
		parent_ = rhs.parent_;
		name_ = rhs.name_;
		prop_ids_ = rhs.prop_ids_;
		check_prop_id_ = rhs.check_prop_id_;
	}
	TWIST_CHECK_INVARIANT
	return *this;
}


std::wstring PropertyFamily::get_name() const
{
	TWIST_CHECK_INVARIANT
	return name_;
}


const PropertyFamily::PropIds& PropertyFamily::get_prop_ids() const
{
	TWIST_CHECK_INVARIANT
	return prop_ids_;
}


void PropertyFamily::add_prop_id(PropertyId prop_id) 
{
	TWIST_CHECK_INVARIANT
	// First check that the new property ID is valid (an exception is thrown by the functor otherwise).
	check_prop_id_(prop_id);
	// Add the prop ID to the list.
	prop_ids_.push_back(prop_id);
}


#ifdef _DEBUG
void PropertyFamily::check_invariant() const noexcept
{
	assert(!name_.empty());
	assert(check_prop_id_);
}
#endif

//
//   PropertyController  class
//

PropertyController::PropertyController(const NamedPropertySet& prop_set)
	: prop_groups_()
	, prop_families_()
	, prop_set_(&prop_set)
{
	TWIST_CHECK_INVARIANT
}


PropertyController::PropertyController(const PropertyController& src)
	: prop_groups_(src.prop_groups_)
	, prop_families_(src.prop_families_)
	, prop_set_(src.prop_set_)
{
	TWIST_CHECK_INVARIANT
}


PropertyController::PropertyController(PropertyController&& src)
	: prop_groups_(move(src.prop_groups_))
	, prop_families_(move(src.prop_families_))
	, prop_set_(src.prop_set_)
{
	TWIST_CHECK_INVARIANT
}


PropertyController::~PropertyController()
{
	TWIST_CHECK_INVARIANT
}


PropertyController& PropertyController::operator=(const PropertyController& rhs)
{
	TWIST_CHECK_INVARIANT
	if (&rhs != this) {
		prop_groups_ = rhs.prop_groups_;
		prop_families_ = rhs.prop_families_;
		prop_set_ = rhs.prop_set_;
	}
	TWIST_CHECK_INVARIANT
	return *this;
}


PropertyController& PropertyController::operator=(PropertyController&& rhs)
{
	TWIST_CHECK_INVARIANT
	prop_groups_ = move(rhs.prop_groups_);
	prop_families_ = move(rhs.prop_families_);
	prop_set_ = rhs.prop_set_;
	TWIST_CHECK_INVARIANT
	return *this;
}


void PropertyController::add_prop_constraint(std::unique_ptr<PropertyConstraint> constraint)
{
	TWIST_CHECK_INVARIANT
	if (constraint->prop_set_ != prop_set_) {
		TWIST_THROW(L"The constraint references the wrong property set.");
	}
	if (!prop_set_->prop_exists(constraint->prop_id_)) {
		TWIST_THROW(L"Constrained property not found in the property set controlled by this controller.");
	}
	if (prop_constraints_.count(constraint->prop_id_) != 0) {
		TWIST_THROW(L"A constraint already exists for property with ID %d.", constraint->prop_id_.value());
	}
	prop_constraints_.insert(std::make_pair(constraint->prop_id_, move(constraint)));
	TWIST_CHECK_INVARIANT
}


PropertyConstraintType PropertyController::get_constraint_type(PropertyId prop_id) const
{
	TWIST_CHECK_INVARIANT
	PropertyConstraintType constr_type = k_prop_constr_none;

	auto it = prop_constraints_.find(prop_id);
	if (it != end(prop_constraints_)) {
		constr_type = it->second->get_type();
	}
	return constr_type;
}

 
const PropConstraintNumOptions& PropertyController::get_num_options_constraint(PropertyId prop_id) const
{
	TWIST_CHECK_INVARIANT
	auto it = prop_constraints_.find(prop_id);
	if (it == end(prop_constraints_)) {
		TWIST_THROW(L"Property with ID \"%d\" is not constrained.", prop_id.value());
	}
	if (it->second->get_type() != k_prop_constr_num_options) {
		TWIST_THROW(L"Property with ID \"%d\" is not constrained through a \"numeric option\" constraint.", prop_id.value());
	}
	return dynamic_cast<const PropConstraintNumOptions&>(*it->second);
}


const PropConstraintIntNumBounds& PropertyController::get_int_num_bounds_constraint(PropertyId prop_id) const
{
	TWIST_CHECK_INVARIANT
	auto it = prop_constraints_.find(prop_id);
	if (it == end(prop_constraints_)) {
		TWIST_THROW(L"Property with ID \"%d\" is not constrained.", prop_id.value());
	}
	if (it->second->get_type() != k_prop_constr_int_num_bounds) {
		TWIST_THROW(L"Property with ID \"%d\" is not constrained through an \"integer numeric bounds\" constraint.", prop_id.value());
	}
	return dynamic_cast<const PropConstraintIntNumBounds&>(*it->second);
}


const PropConstraintFloatNumBounds& PropertyController::get_float_num_bounds_constraint(PropertyId prop_id) const
{
	TWIST_CHECK_INVARIANT
	auto it = prop_constraints_.find(prop_id);
	if (it == end(prop_constraints_)) {
		TWIST_THROW(L"Property with ID \"%d\" is not constrained.", prop_id.value());
	}
	if (it->second->get_type() != k_prop_constr_float_num_bounds) {
		TWIST_THROW(L"Property with ID \"%d\" is not constrained through a \"floating-point numeric bounds\" constraint.", prop_id.value());
	}
	return dynamic_cast<const PropConstraintFloatNumBounds&>(*it->second);
}


const PropertyGroups& PropertyController::get_prop_groups() const
{
	TWIST_CHECK_INVARIANT
	return prop_groups_;
}


PropertyGroup& PropertyController::add_prop_group(std::wstring name)
{
	TWIST_CHECK_INVARIANT
	using namespace std::placeholders;

	// First check that the name is not already in use by another group
	const bool name_exists = has_if(prop_groups_, [name](const PropertyGroup& el) {
		return el.get_name() == name;
	});
	if (name_exists) {
		TWIST_THROW(L"A property group named \"%s\" already exists.", name.c_str());
	}

	PropertyGroup group(name, std::bind(&PropertyController::check_new_group_prop_id, *this, _1));
	prop_groups_.push_back(group);

	TWIST_CHECK_INVARIANT
	return prop_groups_.back();
}

PropertyFamily& PropertyController::add_prop_family(PropertyId parent_id, const std::wstring& name)
{
	TWIST_CHECK_INVARIANT
	using namespace std::placeholders;

	// First check that the name is not already in use by another group
	const bool name_exists = has_if(prop_families_, [name](const PropertyFamily& el) {
		return el.get_name() == name;
	});
	if (name_exists) {
		TWIST_THROW(L"A property group named \"%s\" already exists.", name.c_str());
	}

	PropertyFamily family(parent_id, name, std::bind(&PropertyController::check_new_family_prop_id, *this, _1));
	prop_families_.push_back(family);

	TWIST_CHECK_INVARIANT
	return prop_families_.back();
}


bool PropertyController::has_family(PropertyId prop_id) const
{
	TWIST_CHECK_INVARIANT
	// Second, check that the property ID is not already used by any of the property families.
	auto it = find_if(prop_families_, [prop_id](const PropertyFamily& family) {
		return family.parent_ == prop_id;
	});
	return (it != end(prop_families_) && it->get_prop_ids().size() > 0);
}

const PropertyFamily& PropertyController::get_family_of_prop(PropertyId prop_id)const 
{
	// Second, check that the property ID is not already used by any of the property families.
	auto it = find_if(prop_families_, [prop_id](const PropertyFamily& family) {
		return family.parent_ == prop_id;
	});
	if (it == end(prop_families_)) {
		TWIST_THROW(L"No property family is associated with the property with ID %d .", prop_id.value());
	}
	return *it; 	
}


void PropertyController::check_new_group_prop_id(PropertyId prop_id) const
{
	TWIST_CHECK_INVARIANT
	// First, check that the new property ID exists in the property set.
	if (!prop_set_->prop_exists(prop_id)) {
		TWIST_THROW(L"A property with ID %d does not exist in the property set.", prop_id.value());
	}
	// Second, check that the property ID is not already used by any of the property groups.
	auto it = find_if(prop_groups_, [prop_id](const PropertyGroup& group) {
		return has(group.prop_ids_, prop_id);
	});
	if (it != end(prop_groups_)) {
		TWIST_THROW(L"The property with ID %d already belongs to group \"%s\".", prop_id.value(), it->get_name().c_str());
	}                                                                                                         
}


void PropertyController::check_new_family_prop_id(PropertyId prop_id) const
{
	TWIST_CHECK_INVARIANT
	// First, check that the new property ID exists in the property set.
	if (!prop_set_->prop_exists(prop_id)) {
		TWIST_THROW(L"A property with ID %d does not exist in the property set.", prop_id.value());
	}
	// Second, check that the property ID is not already used by any of the property families.
	auto it = find_if(prop_families_, [prop_id](const PropertyFamily& family) {
		return has(family.prop_ids_, prop_id);
	});
	if (it != end(prop_families_)) {
		TWIST_THROW(L"The property with ID %d already belongs to family \"%s\".", prop_id.value(), it->get_name().c_str());
	}                                                                                                         
}


#ifdef _DEBUG
void PropertyController::check_invariant() const noexcept
{
	assert(prop_set_ != nullptr);
}
#endif 

} 



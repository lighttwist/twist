/// @file PropertySet.ipp
/// Inline implementation file for "PropertySet.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace twist {

template<typename ValueT> 
void PropertySet::define_prop(PropertyId prop_id, const ValueT& val)
{
	TWIST_CHECK_INVARIANT
	if (prop_values_.count(prop_id) > 0) {
		TWIST_THROW(L"A property with ID %d is already defined.", prop_id.value());
	}
	prop_values_.insert(std::make_pair(prop_id, PropValue(val)));
	TWIST_CHECK_INVARIANT
}


template<typename ValueT> 
void PropertySet::set_value(PropertyId prop_id, twist::PropertyType prop_type, const ValueT& val)
{
	TWIST_CHECK_INVARIANT
	auto it = prop_values_.find(prop_id);
	if (it == end(prop_values_)) {
		TWIST_THROW(L"A property with ID %d is not defined.", prop_id.value());
	}
	if (it->second.type() != prop_type) {
		TWIST_THROW(L"Wrong property type for property with ID %d.", prop_id.value());
	}
	it->second = PropValue(val);
	TWIST_CHECK_INVARIANT
}

} 

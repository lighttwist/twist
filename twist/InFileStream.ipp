///  @file  InFileStream.cpp
///  Inline mplementation file for "InFileStream.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

namespace twist {

template<typename Val>
InFileStream<Val>::InFileStream(std::unique_ptr<FileStd> in_file) 
	: in_file_{ move(in_file) }
	, buffer_{ std::make_unique<Val[]>(k_buffer_len) }
{
	file_size_ = in_file_->get_file_size();
	if (file_size_ % sizeof(Val) != 0) {
		TWIST_THROW(L"The file size indicates that it cannot be made up of %d-byte values.", sizeof(Val));
	}
	file_pos_ = in_file_->get_file_pos();
	if (file_pos_ % sizeof(Val) != 0) {
		TWIST_THROW(L"The current file position is not compatible with a file made up of %d-byte values.", sizeof(Val));
	} 

	refill();
	TWIST_CHECK_INVARIANT
}


template<typename Val>
InFileStream<Val>::~InFileStream()
{
	TWIST_CHECK_INVARIANT
}


template<typename Val>
inline InFileStream<Val>& InFileStream<Val>::operator>>(Val& value)
{
	TWIST_CHECK_INVARIANT
	value = get();
	TWIST_CHECK_INVARIANT
	return *this;
}


template<typename Val> 
inline Val InFileStream<Val>::get()
{
	TWIST_CHECK_INVARIANT
	Val value;
	if (buffer_pos_ < k_buffer_len) {
		value = buffer_[static_cast<size_t>(buffer_pos_)];
		buffer_pos_++;	
	}
	else {
		// We have reached the end of the memory buffer. Refill it and move back to its beginning.
		refill();
		value = buffer_[static_cast<size_t>(buffer_pos_)];
		buffer_pos_++;	
	}

	return value;
}


template<typename Val> 
inline Val InFileStream<Val>::get_safe()
{
	TWIST_CHECK_INVARIANT
	if (is_eof()) {
		TWIST_THROW(L"The end of the file has already been reached.");
	}

	Val value;
	if (buffer_pos_ < k_buffer_len) {
		value = buffer_[buffer_pos_];
		buffer_pos_++;	
	}
	else {
		// We have reached the end of the memory buffer. Refill it and move back to its beginning.
		refill();
		value = buffer_[buffer_pos_];
		buffer_pos_++;	
	}

	return value;
}


template<typename Val>
inline bool InFileStream<Val>::is_eof() const
{
	TWIST_CHECK_INVARIANT
	return buffer_pos_ == buffer_end_file_pos_;
}


template<typename Val>
fs::path InFileStream<Val>::path() const
{
	TWIST_CHECK_INVARIANT
	return in_file_->path();
}


template<typename Val>
void InFileStream<Val>::refill()
{
	TWIST_CHECK_INVARIANT
	// Check whether there is enough data left in the file to fill the buffer.
	if (file_size_ - file_pos_ > k_buffer_len) {
		in_file_->read_buf(reinterpret_cast<char*>(buffer_.get()), k_buffer_len);	
		file_pos_ += k_buffer_len;
	}
	else {
		in_file_->read_buf(reinterpret_cast<char*>(buffer_.get()), 
				static_cast<Ssize>(file_size_ - file_pos_));	
		buffer_end_file_pos_ = (file_size_ - file_pos_) / sizeof(Val);
		file_pos_ = file_size_;
	}
	buffer_pos_ = 0; 
}


#ifdef _DEBUG
template<typename Val>
void InFileStream<Val>::check_invariant() const noexcept
{
	assert(in_file_);
	assert(buffer_);
	assert(buffer_pos_ <= k_buffer_len);
	assert(file_pos_ <= file_size_);
}
#endif

} 

/// @file file_io.hpp
/// File input/output utilities using standard C++

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_FILE__IO_HPP
#define TWIST_FILE__IO_HPP

#include <filesystem>
#include <fstream>

namespace twist {

/// Alternative directory separator which may be used in addition to the portable /. 
/// On Windows, this is the backslash character \. 
/// On POSIX, this is the same forward slash / as the portable separator. 
constexpr wchar_t k_path_sep = fs::path::preferred_separator;

/// The default "optimal" number of bytes in a file block -- 2^17 bytes (i.e. a block of memory that is read 
/// from/written to a binary file as one chunk). This value was entirely invented, as it seemed to give better 
/// prformance than others.
constexpr std::int64_t opt_file_block_size  = 131072;

/// Filename type. This can be 
///  (a) A full or relative path of a file plus the file name plus extension, or any part thereof
///  (b) A full or relative path of a directory
//+OBSOLETE: Use fs::path instead
using Filename = std::wstring;

//! Create a fs::path object base on the string \p path.
[[nodiscard]] auto to_path(std::wstring_view path) -> fs::path;

/*! If a path ends with the directory separator character, remove it.
    \param[in] path  The path
    \return  The path, without one trailing directory separator, or unchanged if it did not end in one
 */
[[nodiscard]] auto remove_trailing_separator(const fs::path& path) -> fs::path;

/// Find out whether a path matches a pattern (optionally) containig path wildcards.
///
/// @param[in] path  The path
/// @param[in] wcard_pattern  The (wildcarded) pattern
/// @param[in] case_sens  Whether the matching should be case-sensitive
/// @return  true if the path matches the pattern
///
bool is_wcard_match(const fs::path& path, std::wstring_view wcard_pattern, bool case_sens = false);

/// Find out whether a path matches a regex pattern.
///
/// @param[in] path  The path
/// @param[in] pattern  The regex pattern
/// @param[in] case_sens  Whether the matching should be case-sensitive
/// @return  true if the path matches the pattern
///
bool is_regex_match(const fs::path& path, std::wstring_view pattern, bool case_sens = false);

/*! Search a directory for files whose path matches a pattern (optionally) containig path wildcards (* and/or ?).
    \param[in] dir_path  The directory path; if the directory does not exist, an exception is thrown
    \param[in] pattern  The (wildcarded) pattern
    \param[in] recursive  If false, only the files directly inside the directory are examined; if false, the files in 
                          all descendant directories are examined
    \param[in] case_sens  Whether the matching should be case-sensitive
    \return  The file paths which match the pattern
 */
[[nodiscard]] auto find_wcard_matching_filenames_in_dir(const fs::path& dir_path,
		                                                std::wstring_view pattern, 
                                                        bool recursive, 
                                                        bool case_sens = false) -> std::vector<fs::path>;

/*! Search a directory for files whose path matches a regex pattern.
    \param[in] dir_path  The directory path; if the directory does not exist, an exception is thrown
    \param[in] pattern  The regex pattern
    \param[in] recursive  If false, only the files directly inside the directory are examined; if false, the files in 
                          all descendant directories are examined
    \param[in] case_sens  Whether the matching should be case-sensitive
    \return  The file paths which match the pattern
 */
[[nodiscard]] auto find_regex_matching_filenames_in_dir(const fs::path& dir_path,
		                                                std::wstring_view pattern, 
                                                        bool recursive, 
                                                        bool case_sens = false) -> std::vector<fs::path>;

/*! Search a directory for subdirectories whose path matches a regex pattern.
    \param[in] dir_path  The directory path; if the directory does not exist, an exception is thrown
    \param[in] pattern  The regex pattern
    \param[in] case_sens  Whether the matching should be case-sensitive
    \return  The paths of the subdirectories which match the pattern
 */
[[nodiscard]] auto find_regex_matching_subdirs_in_dir(const fs::path& dir_path,
		                                              std::wstring_view pattern,
                                                      bool case_sens = false) -> std::vector<fs::path>;


/*! Given the range \p range of file paths, find and return the first whose filename component matches \p filename.
    If no match is found, an empty path is returned. The matching is case-sensitive. 
*/
template<class Rng>
requires is_range_and_gives<Rng, fs::path>        
[[nodiscard]] auto find_first_path_with_filename(const Rng& range, const fs::path& filename) -> fs::path;

/*! Get the names of all directories inside a specific directory.
    \param[in] dir_path  The directory path
    \param[in] recursive  If false, only the names of the directories directly inside the directory are examined; 
                          otherwise the names of all descendant directories are returned
    \return  The subdirectory names
 */
[[nodiscard]] auto get_subdirectories(const fs::path& dir_path, bool recursive) -> std::vector<fs::path>;

/// If two paths, in their absolute form, share a common directory, find that directory's path.
///
/// @param[in] path1  The first path
/// @param[in] path2  The second path
/// @return  The common directory path; or an empty path if there is no common directory
///
fs::path get_common_dir(const fs::path& path1, const fs::path& path2);

/*! Recursively delete all contents of the directory with path \p dir_path, including its subdirectories and their 
    contents. The directory itself is not deleted. If the directory with path \p dir_path does not exist, then if 
    \p throw_if_no_dir is true, an exception is thrown and if \p throw_if_no_dir is false nothing happens.
 */
auto delete_dir_contents(const fs::path& dir_path, bool throw_if_no_dir = true) -> void;

/*! Get the directory depth of a specific path. The drive (if any) and the drive root directory are included, so for 
    example "D:\" has a depth of 2. 
    \param[in] path  The path  
    \return  The directory depth
 */
[[nodiscard]] auto get_depth(const fs::path& path) -> int;

/// Given a directory path and another path (to a file or directory) get a relative path which, when appended 
/// the former, will resolve to same path as the latter. An exception is thrown if this is impossible (the 
/// paths have different root names).
///
/// @param[in] dir_path_from  The starting point, directory path 
/// @param[in] path_to  The destination path
/// @return  The relative path
///
[[nodiscard]] auto make_relative(const fs::path& dir_path_from, const fs::path& path_to) -> fs::path;

//! Whether the path \p path is made up entirely of whitespace, or is empty.
[[nodiscard]] auto is_wspace_path(const fs::path& path) -> bool;

/*! Given the file path \p path, replace its stem part by calling \p change with the old file stem as an argument and 
    using the returned value as the new file stem.
 */
template<class Fn>
requires std::is_invocable_r_v<fs::path, Fn, std::wstring>
[[nodiscard]] auto change_stem(const fs::path& path, Fn&& change) -> fs::path;

/*! Append an extension to a file path, even if the path already contains an extension.
    \param[in] path  The file path
    \param[in] ext  The extension to append (if it does not start with a dot, one will be added)
    \return  The new file path
 */
[[nodiscard]] auto append_extension(const fs::path& path, const fs::path& ext) -> fs::path;

/*! Change the extension in a filename to a new extension.
    If the filename currently has no extension, the new extension is simply appended.
    \param[in] path  The file path to be modified
    \param[in] ext  The new extension (must include the preceding dot)
    \return  The new file path
 */
[[nodiscard]] auto change_extension(const fs::path& path, const fs::path& ext) -> fs::path;

/*! Given an initial path, check that the object it points to exists. If not, the path is returned.
    Otherwise, similar paths are created (in the same parent directory) until one is found which is unused, ie it does 
    not point to an existing object. 
    The paths are created by appending -N to the initial path's stem, where N is a counter which starts from two and is 
    incremented until an unused path is found.   
    \param[in] init_path  The initial path
    \return  The unused path
 */   
[[nodiscard]] auto get_unused_path(const fs::path& init_path) -> fs::path;

/* Get the size (bytes) of file with path \p path as (signed) 64-bit integer. 
   If the file size exceeds the maximum value of a 64-bit integer (not that likely), an exception is thrown.
 */
[[nodiscard]] auto file_ssize(const fs::path& path) -> std::int64_t;

/*! Find out whether the file or directory with path \p path falls, or would fall inside the directory with path 
    \p dir_path. The paths must either be both absolute or both relative. Neither path needs to refer to an existing 
	file or directory.
 */
[[nodiscard]] auto is_inside_dir(const fs::path& dir_path, const fs::path& path) -> bool;

/*! Open a text file for writing.
    \tparam  The character type to be used by the stream which will be attached to the file
	\param[in] path  The text file path
	\param[in] overwrite  What to do if the file already exists: if true, then the file is overwritten; if false then
	                      an exception is thrown
	\return  Stream attached to the successufully opened file
 */
template<class Chr = wchar_t>
[[nodiscard]] auto open_text_file_for_writing(const fs::path& path, bool overwrite) -> std::basic_ofstream<Chr>;

/*! Read the contents of a text file into a string.
    \tparam  The character type to be used by the stream which will be attached to the file
	\param[in] path  The text file path
    \return  String containing all the text in the file 
*/
template<class Chr = wchar_t>
[[nodiscard]] auto read_text_file(const fs::path& path) -> std::basic_string<Chr>;

/*! Find all occurences of each of a set of strings inside a text file and replace them with the corresponding string 
    from another set of strings.
    \tparam  The character type to be used by the stream which will be attached to the file
	\param[in] path  The text file path
    \param[in] replacements  List of pairs of strings; all occurrences of the first string in each pair will be 
                             replaced with the second string in the pair
 */
template<class Chr = wchar_t>
auto replace_strings_in_text_file(const fs::path& path, 
                                  const Pairs<std::basic_string<Chr>, std::basic_string<Chr>>& replacements) -> void;

}

#include "twist/file_io.ipp"

#endif

/// @file RuntimeError.hpp
/// RuntimeError class definition and declarations of related free functions

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_RUNTIME_ERROR_HPP
#define TWIST_RUNTIME_ERROR_HPP

#include "globals.hpp"

#include <stdexcept>
#include <string>

namespace twist {

//! The main exception class in the "twist" library.
class RuntimeError : public std::runtime_error {
public:
	/*! Constructor. 
	    \param[in] err_msg  The error message
	    \param[in] func_name  The name of the function where the exception was originally thrown
	 */
	explicit RuntimeError(std::wstring err_msg, std::wstring func_name);

	auto what() const noexcept -> const char* override; // std::exception override

	//! The error message.
	auto error_message() const noexcept -> std::wstring;

	//! Prepend the text \p text to the error message.	
	auto prepend(std::wstring_view text) noexcept -> void;

	//! Append the text \p text to the error message.	
	auto append(std::wstring_view text) noexcept -> void;

	//! The name of the function where the exception was originally thrown.
	auto function_name() const noexcept -> std::wstring;

private:
	std::wstring err_msg_;
	std::wstring func_name_;
	mutable std::string ansi_err_msg_;

	TWIST_CHECK_INVARIANT_DECL
};

// --- Free functions ---

/*! Get the error message from an STL (or derived) exception object.
    If the exception type is RuntimeError, its proper widechar error message is retrieved.
    \param[in] excep  The exception
    \return  The message
 */
auto error_message(const std::exception& excep) noexcept -> std::wstring;

/*! Get the name of the function where an STL (or derived) exception object was originally thrown, if available.
    \param[in] excep  The exception
    \return  The function name, if available (ie if the exception type is RuntimeError); or an empty string otherwise 
 */
auto function_name(const std::exception& excep) noexcept -> std::wstring;

} 

#endif 

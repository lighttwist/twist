///  @file  OutFileStream.ipp
///  Inline implementation file for "OutFileStream.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "OutFileStream.hpp"

namespace twist {

template<typename TValue>
OutFileStream<TValue>::OutFileStream(std::unique_ptr<FileStd> out_file) 
	: out_file_{ move(out_file) }
	, buffer_{ std::make_unique<TValue[]>(k_buffer_len) }
	, buffer_pos_{ 0 }
{
	TWIST_CHECK_INVARIANT
}


template<typename TValue>
OutFileStream<TValue>::~OutFileStream()
{
	TWIST_CHECK_INVARIANT
	flush();
}


template<typename TValue>
void OutFileStream<TValue>::stream(const TValue& value)
{
	TWIST_CHECK_INVARIANT
	if (buffer_pos_ < k_buffer_len - 1) {
		buffer_[buffer_pos_] = value;
		buffer_pos_++;	
	}
	else {
		// We have reached the end of the memory buffer. Write it to the file and move back to its beginning.
		flush();
		buffer_[buffer_pos_] = value;
		buffer_pos_++;	
	}
	TWIST_CHECK_INVARIANT
}


template<typename TValue>
void OutFileStream<TValue>::flush()
{
	TWIST_CHECK_INVARIANT
	out_file_->write_buf(reinterpret_cast<const char*>(buffer_.get()), buffer_pos_ * sizeof(TValue));
	buffer_pos_ = 0; 
	TWIST_CHECK_INVARIANT
}


#ifdef _DEBUG
template<typename TValue>
void OutFileStream<TValue>::check_invariant() const noexcept
{
	assert(out_file_);
	assert(buffer_);
	assert(buffer_pos_ < k_buffer_len);
}
#endif 

} 



/// @file std_variant_utils.hpp
/// Utilities for working with std::variant

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST__STD__VARIANT__UTILS_HPP
#define TWIST__STD__VARIANT__UTILS_HPP

#include <variant>

namespace twist {

/*! Get the index of the first variant alternative which has a specific type.
    \tparam  Variant  The variant type
    \tparam  Ty  The variant alternative type to search for
    \return  The index of the first matching variant alternative type; or std::variant_npos if there is no match
 */ 
template<typename Variant, typename Ty>
[[nodiscard]] constexpr auto variant_index() -> std::size_t;

// --- Very simple syntactic sugar functions ---

template<class... Types>
[[nodiscard]] constexpr uint16_t get_uint16(const std::variant<Types...>& var);

template<class... Types>
[[nodiscard]] constexpr double get_double(const std::variant<Types...>& var);

template<class... Types>
[[nodiscard]] constexpr const std::string& get_string(const std::variant<Types...>& var);

template<class... Types>
[[nodiscard]] constexpr const std::wstring& get_wstring(const std::variant<Types...>& var);

template<class Elem, class... Types>
[[nodiscard]] constexpr const std::vector<Elem>& get_vector(const std::variant<Types...>& var);

template<class Elem, class... Types>
[[nodiscard]] constexpr std::vector<Elem>& get_vector(std::variant<Types...>& var);

template<class... Types>
[[nodiscard]] constexpr bool holds_uint16(const std::variant<Types...>& var) noexcept;

template<class... Types>
[[nodiscard]] constexpr bool holds_double(const std::variant<Types...>& var) noexcept;

template<class... Types>
[[nodiscard]] constexpr bool holds_float(const std::variant<Types...>& var) noexcept;

template<class... Types>
[[nodiscard]] constexpr bool holds_string(const std::variant<Types...>& var) noexcept;

template<class... Types>
[[nodiscard]] constexpr bool holds_wstring(const std::variant<Types...>& var) noexcept;

template<class Elem, class... Types>
[[nodiscard]] constexpr bool holds_vector(const std::variant<Types...>& var) noexcept;

template<class... Types>
[[nodiscard]] constexpr bool holds_monostate(const std::variant<Types...>& var) noexcept;

}

#include "twist/std_variant_utils.ipp"

#endif

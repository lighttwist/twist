/// @file commands.cpp
/// Implementation file for "commands.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/commands.hpp"

namespace twist {

//
//   Command  class
//

Command::Command()
	: state_(k_not_done)
{
	TWIST_CHECK_INVARIANT
}


Command::~Command()
{
	TWIST_CHECK_INVARIANT
}


void Command::do_it()
{
	TWIST_CHECK_INVARIANT
	if (state_ != k_not_done) {
		TWIST_THROW(L"The command has already been done.");
	}
	state_ = k_doing;
	do_command();
	state_ = k_done;
	TWIST_CHECK_INVARIANT
}


void Command::undo_it()
{
	TWIST_CHECK_INVARIANT
	if (state_ != k_done) {
		TWIST_THROW(L"The command has not been done.");
	}
	state_ = k_undoing;	
	undo_command();
	state_ = k_not_done;
	TWIST_CHECK_INVARIANT
}


Command::State Command::get_state() const
{
	TWIST_CHECK_INVARIANT
	return state_;
}


#ifdef _DEBUG
void Command::check_invariant() const noexcept
{
	assert(state_ >= k_not_done && state_ <= k_undoing);
}
#endif


//
//   CommandMgr  class
//

CommandMgr::CommandMgr()
	: commands_done_()
{
	TWIST_CHECK_INVARIANT
}


CommandMgr::~CommandMgr()
{
	TWIST_CHECK_INVARIANT
}


void CommandMgr::do_and_store_command(std::unique_ptr<Command> command)
{
	TWIST_CHECK_INVARIANT
	command->do_it();
	commands_done_.push(move(command));
	TWIST_CHECK_INVARIANT
}


void CommandMgr::undo_and_remove_last_command()
{
	TWIST_CHECK_INVARIANT
	if (commands_done_.empty()) {
		TWIST_THROW(L"Command stack is empty.");
	}
	commands_done_.top()->undo_it();
	commands_done_.pop();
	TWIST_CHECK_INVARIANT
}


const Command& CommandMgr::get_last_command() const
{
	TWIST_CHECK_INVARIANT
	if (commands_done_.empty()) {
		TWIST_THROW(L"Command stack is empty.");
	}
	return *commands_done_.top();
}


#ifdef _DEBUG
void CommandMgr::check_invariant() const noexcept
{
}
#endif

} // namespace twist

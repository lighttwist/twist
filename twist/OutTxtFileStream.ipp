///  @file  OutTxtFileStream.ipp
///  Implementation file for "OutTxtFileStream.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "OutTxtFileStream.hpp"

namespace twist {

template<typename Chr>
OutTxtFileStream<Chr>::OutTxtFileStream(std::unique_ptr<FileStd> out_txt_file) 
	: OutFileStream<Chr>(move(out_txt_file))
{
	TWIST_CHECK_INVARIANT
}


template<typename Chr>
OutTxtFileStream<Chr>::~OutTxtFileStream()
{
	TWIST_CHECK_INVARIANT
}
	

template<typename Chr>
OutTxtFileStream<Chr>& OutTxtFileStream<Chr>::operator<<(Chr value)
{
	TWIST_CHECK_INVARIANT
	this->stream(value);
	return *this;
}

	
template<typename Chr>
OutTxtFileStream<Chr>& OutTxtFileStream<Chr>::operator<<(const Chr* str)
{
	TWIST_CHECK_INVARIANT
	for (; *str != 0; ++str) {  //+cj  is that comparison fine for all char types?
		this->stream(*str);  
	}
	return *this;
}


template<typename Chr>
OutTxtFileStream<Chr>& OutTxtFileStream<Chr>::operator<<(std::basic_string_view<Chr> str)
{
	TWIST_CHECK_INVARIANT
	for (auto c : str) {
		this->stream(c); 
	}
	return *this;
}


#ifdef _DEBUG
template<typename Chr>
void OutTxtFileStream<Chr>::check_invariant() const noexcept
{
}
#endif 

}



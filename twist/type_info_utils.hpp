/// @file type_info_utils.hpp
/// Utilities for working with the C++ type_info RTTI type

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_TYPE__INFO__UTILS_HPP
#define TWIST_TYPE__INFO__UTILS_HPP

#include <map>
#include <typeinfo>
#include <unordered_map>
#include <unordered_set>

namespace twist {

//! A reference wrapper enabling const std::type_info& elements to be stored in containers.
using TypeInfoRef = std::reference_wrapper<const std::type_info>;

//! "Less than" functor for TypeInfoRef.
struct TypeInfoLess {
    bool operator()(TypeInfoRef lhs, TypeInfoRef rhs) const 
	{ 
		return lhs.get().before(rhs); 
	}
};
 
//! "Hasher" functor for TypeInfoRef.
struct TypeInfoHasher {
    std::size_t operator()(TypeInfoRef code) const
    {
        return code.get().hash_code();
    }
};
 
 //! "Equal to" functor for TypeInfoRef.
struct TypeInfoEqualTo {
    bool operator()(TypeInfoRef lhs, TypeInfoRef rhs) const
    {
        return lhs.get() == rhs.get();
    }
};

//! A std::set with TypeInfoRef elements.
using TypeInfoSet = std::set<TypeInfoRef, TypeInfoLess>;

/*! A std::map with TypeInfoRef keys.
    \tparam Val  The map value type
 */
template<class Val>
using TypeInfoMap = std::map<TypeInfoRef, Val, TypeInfoLess>;

//! A std::unordered_set with TypeInfoRef elements.
using TypeInfoHashSet = std::unordered_set<TypeInfoRef, TypeInfoHasher, TypeInfoEqualTo>;
 
/*! A std::unordered_map with TypeInfoRef keys.
    \tparam Val  The map value type
 */
template<class Val>
using TypeInfoHashMap = std::unordered_map<TypeInfoRef, Val, TypeInfoHasher, TypeInfoEqualTo>;

/*! Get (at compile-time) the name of a type, without any leading or trailing qualifiers such as "struct" or "class" 
    (namespaces are kept).  
    \tparam T  The type
    \return A std::string_view value holding the type name
 */
template<class T> 
[[nodiscard]] constexpr auto type_name();

/*! Get the name of a type, without any leading or trailing qualifiers such as "struct" or "class" (namespaces 
    are kept). 
    \tparam T  The type
    \return  The type name
 */
template<class T> 
[[nodiscard]] auto type_wname() -> std::wstring;

//! The size, in bytes, of type \p T, as a signed integer.
template<class T>
constexpr auto ssizeof = static_cast<Ssize>(sizeof(T));

/*! Get a string that may identify a specific type.
    The particular string representation is implementation-defined, and may or may not be different for 
    different types.
    \param[in] ti  Type informtion 
    \return  The type name representation
 */
[[nodiscard]] auto get_name(const std::type_info& ti) -> std::wstring;

/*! Compile-time debug tool for displaying the type of a template or auto variable (or expression).
    Usage examples 
      ShowType<decltype(x)> x_type;
      ShowType<std::decay_t<decltype(x)>> x_type;
      ShowType<T> t_type;
    The type is displayed as part of the error message generated by the compiler because of the missing template 
    definition.
 */
template<class T> 
class ShowType; 

} 

#include "twist/type_info_utils.ipp"

#endif 

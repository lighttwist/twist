/// @file file_io.ipp
/// Inline implementation file for "file_io.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace twist {

template<class Rng>
requires is_range_and_gives<Rng, fs::path>
[[nodiscard]] auto find_first_path_with_filename(const Rng& range, const fs::path& filename) -> fs::path
{
	using std::end;

	const auto it = rg::find_if(range, [&filename](const auto& path) { return path.filename() == filename; });
	if (it == end(range)) {
		return fs::path{};
	}
	return *it;
}

template<class Fn>
requires std::is_invocable_r_v<fs::path, Fn, std::wstring>
[[nodiscard]] auto change_stem(const fs::path& path, Fn&& change) -> fs::path
{
    return path.parent_path() / (std::invoke(std::move(change), path.stem().wstring()) + path.extension().wstring());
}

template<class Chr>
[[nodiscard]] auto open_text_file_for_writing(const fs::path& path, bool overwrite) -> std::basic_ofstream<Chr>
{
	if (!overwrite && exists(path)) {
		TWIST_THRO2(L"File \"{}\" already exists.", path.c_str());
	}
	
	auto stream = std::basic_ofstream<Chr>{path.c_str(), std::ios::out};
	if (!stream) {
		TWIST_THRO2(L"Error opening file \"{}\" for writing.", path.c_str());	
	}

	return stream;
}

template<class Chr>
[[nodiscard]] auto read_text_file(const fs::path& path) -> std::basic_string<Chr>
{
	auto stream = std::basic_ifstream<Chr>{path};
	if (!stream) {
		TWIST_THRO2(L"Error opening file \"%s\" for reading: {}", 
					path.c_str(),
					ansi_to_string(std::strerror(errno)));	
	}

	auto text = std::basic_string<Chr>{};
	stream.seekg(0, std::ios::end);
	text.resize(stream.tellg());
	stream.seekg(0, std::ios::beg);
	stream.read(&text[0], text.size());

	return text;
}

template<class Chr>
auto replace_strings_in_text_file(const fs::path& path, 
                                  const Pairs<std::basic_string<Chr>, std::basic_string<Chr>>& replacements) -> void
{
	auto text = read_text_file<Chr>(path);

	for (const auto& [old_str, new_str] : replacements) {
		assert(!old_str.empty());
		replace_all_substr_in_place<Chr>(text, old_str, new_str);
	}

	auto ostream = open_text_file_for_writing(path, true/*overwrite*/);
	ostream << text;
}

}

/// @file DateInterval.hpp
/// DateInterval class definition

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_DATE_INTERVAL_HPP
#define TWIST_DATE_INTERVAL_HPP

#include "twist/Date.hpp"

namespace twist {

//! A time interval, consisting of whole days, between two specific dates (which can be the same).
class DateInterval {
public:
	//! Constructor. Initialises the both date interval bounds to the 1st of January, year 1.
	DateInterval();

	/*! Constructor. 
	    \param[in] lo  The lower bound of the interval
	    \param[in] hi  The upper bound of the interval; must be equal or after the lower bound
	 */
	DateInterval(const Date& lo, const Date& hi);

	//! The lower bound of the interval.
	[[nodiscard]] auto lo() const -> Date;
	
	//! The upper bound of the interval.
	[[nodiscard]] auto hi() const -> Date;

private:	
	auto check() const -> void;

	const Date lo_;
	const Date hi_;

	TWIST_CHECK_INVARIANT_DECL
};

// --- Non-member functions ---

/*! Get the length, in days, of the date interval \p date_interval.
    If the bounds of the interval are the same date, the length is one. 
 */
[[nodiscard]] auto length_days(const DateInterval& date_interval) -> int;

} 

#endif 

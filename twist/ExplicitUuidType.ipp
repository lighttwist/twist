///  @file  ExplicitUuidType.ipp
///  Implementation file for "ExplicitUuidType.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

namespace twist {

template<unsigned long t_lib_id, unsigned long t_type_id>
ExplicitUuidType<t_lib_id, t_type_id>::ExplicitUuidType() 
	: uuid_()
{
}


template<unsigned long t_lib_id, unsigned long t_type_id>
ExplicitUuidType<t_lib_id, t_type_id>::ExplicitUuidType(const std::wstring& uuid_str) 
	: uuid_(uuid_str)
{
}


template<unsigned long t_lib_id, unsigned long t_type_id>
ExplicitUuidType<t_lib_id, t_type_id>::ExplicitUuidType(const twist::Uuid& uuid) 
	: uuid_(uuid)
{
}


template<unsigned long t_lib_id, unsigned long t_type_id>
template<unsigned long t_other_lib_id, unsigned long t_other_type_id> 
ExplicitUuidType<t_lib_id, t_type_id>::ExplicitUuidType(const ExplicitUuidType<t_other_lib_id, t_other_type_id>& uuid)
	: uuid_(uuid.uuid_)
{
}


template<unsigned long t_lib_id, unsigned long t_type_id>
ExplicitUuidType<t_lib_id, t_type_id>::ExplicitUuidType(const ExplicitUuidType& src)
	: uuid_(src.uuid_)	
{
}


template<unsigned long t_lib_id, unsigned long t_type_id>
ExplicitUuidType<t_lib_id, t_type_id>::ExplicitUuidType(ExplicitUuidType&& src)
	: uuid_(std::move(src.uuid_))
{
}


template<unsigned long t_lib_id, unsigned long t_type_id>
ExplicitUuidType<t_lib_id, t_type_id>& ExplicitUuidType<t_lib_id, t_type_id>::operator=(const ExplicitUuidType& rhs)
{
	if (&rhs != this) {
		uuid_ = rhs.uuid_;
	}
	return *this;
}


template<unsigned long t_lib_id, unsigned long t_type_id>
ExplicitUuidType<t_lib_id, t_type_id>& ExplicitUuidType<t_lib_id, t_type_id>::operator=(ExplicitUuidType&& rhs)
{
	uuid_ = std::move(rhs.uuid_);
	return *this;
}


template<unsigned long t_lib_id, unsigned long t_type_id>
bool ExplicitUuidType<t_lib_id, t_type_id>::operator==(const ExplicitUuidType& rhs) const
{
	return uuid_ == rhs.uuid_;
}


template<unsigned long t_lib_id, unsigned long t_type_id>
bool ExplicitUuidType<t_lib_id, t_type_id>::operator!=(const ExplicitUuidType& rhs) const
{
	return uuid_ != rhs.uuid_;
}


template<unsigned long t_lib_id, unsigned long t_type_id>
bool ExplicitUuidType<t_lib_id, t_type_id>::operator<(const ExplicitUuidType& rhs) const
{
	return uuid_ < rhs.uuid_;
}


template<unsigned long t_lib_id, unsigned long t_type_id>
Uuid ExplicitUuidType<t_lib_id, t_type_id>::value() const
{
	return uuid_;
}


//
//  Free functions
//

template<unsigned long t_lib_id, unsigned long t_type_id>
std::wstring to_str(const ExplicitUuidType<t_lib_id, t_type_id>& uuid, bool brackets) 
{
	return to_str(uuid.uuid_, brackets);
}

} // namespace twist

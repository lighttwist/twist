///  @file  ExplicitMemberType.hpp
///  ExplicitMemberType  class template 

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_EXPLICIT_MEMBER_TYPE_HPP
#define TWIST_EXPLICIT_MEMBER_TYPE_HPP

#include "string_utils.hpp"

namespace twist {

///
///  Class template that makes a basic integer type, which is declared as part of a class (nested in a class), into an 
///  explicit type. The template arguments are
///		_Class   The "owner" class.
///		_Num    The basic type. Must be an integer numeric type. 
///		_type_id  ID of the type. Must be unique within the owner class.
///     _def_value  The default value, that is, the value to which an instance of the class is intialised by the 
///					default constructor.
///
template<class _Class, typename _Num, unsigned long _type_id, _Num _def_value>
class ExplicitMemberType {
public:
	/// The default value of this type.
	static const _Num  k_def_value = _def_value;

	/// Constructor. Initialises the object with the default value.
	///
	ExplicitMemberType() : value_(_def_value) 
	{
	}

	/// Constructor.
	///
	/// @param[in] value  The value.
	///
	explicit ExplicitMemberType(_Num value) : value_(value) 
	{
	}

	/// Copy constructor.
	///
	ExplicitMemberType(const ExplicitMemberType& src) : value_(src.value_) 
	{		
	}

	/// Assignent operator.
	///
	ExplicitMemberType& operator=(const ExplicitMemberType& rhs) 
	{
		if (this != &rhs) {
			value_ = rhs.value_;
		}
		return *this;
	}

	/// Equality operator.
	///
	bool operator==(const ExplicitMemberType& rhs) const 
	{
		return value_ == rhs.value_;
	}

	/// Inquality operator.
	///
	bool operator!=(const ExplicitMemberType& rhs) const 
	{
		return value_ != rhs.value_;
	}

	/// < operator
	///
	bool operator<(const ExplicitMemberType& rhs) const 
	{
		return value_ < rhs.value_;
	}

	/// > operator
	///
	bool operator>(const ExplicitMemberType& rhs) const 
	{
		return value_ > rhs.value_;
	}

	/// <= operator
	///
	bool operator<=(const ExplicitMemberType& rhs) const 
	{
		return value_ <= rhs.value_;
	}

	/// >= operator
	///
	bool operator>=(const ExplicitMemberType& rhs) const 
	{
		return value_ >= rhs.value_;
	}

	/// Pre-increment operator
	///
	ExplicitMemberType& operator++() 
	{
		++value_; 
		return *this;	
	}

	/// Post-increment operator
	///
 	ExplicitMemberType operator++(int) 
	{
		ExplicitMemberType temp(*this);
		++(*this);
		return temp;	
	}

	/// Get the value.
	///
	/// @return  The value.
	///
	_Num value() const 
	{
		return value_;
	}

private:	
	_Num  value_;
};


/// Get a textual represtentation of an explicitly typed number object.
///
/// @param[in] obj  The explicit type object
/// @return  The text
///
template<class _Class, typename _Num, unsigned long _type_id, _Num _def_value>
std::wstring to_str(const ExplicitMemberType<_Class, _Num, _type_id, _def_value>& obj)
{
	return to_str(obj.value());
}

} 

#endif 

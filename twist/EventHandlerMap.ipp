///  @file  EventHandlerMap.ipp
///  Implementation file for "EventHandlerMap.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

namespace twist {

template<typename TEvent, typename TTrigger, typename TData>
EventHandlerMap<TEvent, TTrigger, TData>::EventHandlerMap()
{
	TWIST_CHECK_INVARIANT
}


template<typename TEvent, typename TTrigger, typename TData>
EventHandlerMap<TEvent, TTrigger, TData>::~EventHandlerMap()
{
	TWIST_CHECK_INVARIANT
}


template<typename TEvent, typename TTrigger, typename TData>
template<typename TMemFunc, class TObj> 
typename EventHandlerMap<TEvent, TTrigger, TData>::Handler EventHandlerMap<TEvent, TTrigger, TData>::add(Event event, 
		Trigger trigger, TMemFunc mem_func, TObj& obj)
{
	TWIST_CHECK_INVARIANT
	Handler old_handler;
	Handler new_handler = std::bind(mem_func, &obj, _1, _2, _3);
	
	const auto it = handlers_.find(Key(event, trigger));
	if (it != end(handlers_)) {
		old_handler = it->second;
		handlers_.erase(it);
	}
	
	handlers_.insert( std::make_pair(std::make_pair(event, trigger), new_handler) );

	TWIST_CHECK_INVARIANT
	return old_handler;
}


template<typename TEvent, typename TTrigger, typename TData>
typename EventHandlerMap<TEvent, TTrigger, TData>::Handler EventHandlerMap<TEvent, TTrigger, TData>::remove(Event event, Trigger trigger)
{
	TWIST_CHECK_INVARIANT
	Handler ret;
	const auto it = handlers_.find(Key(event, trigger));
	if (it != end(handlers_)) {
		ret = it->second;
		handlers_.erase(it);
	}
	TWIST_CHECK_INVARIANT
	return ret;
}


template<typename TEvent, typename TTrigger, typename TData>
bool EventHandlerMap<TEvent, TTrigger, TData>::call(Event event, Trigger trigger, const Data& data) const
{
	TWIST_CHECK_INVARIANT
	bool ret = false;
	const auto it = handlers_.find(Key(event, trigger));
	if (it != end(handlers_)) {
		it->second(event, trigger, data);
		ret = true;
	}
	return ret;
}


#ifdef _DEBUG
template<typename TEvent, typename TTrigger, typename TData>
void EventHandlerMap<TEvent, TTrigger, TData>::check_invariant() const noexcept
{
}
#endif 

//
//  EventHandlerMap<>::KeyCompare  class
//

template<typename TEvent, typename TTrigger, typename TData>
bool EventHandlerMap<TEvent, TTrigger, TData>::KeyCompare::operator()(const Key& k1, const Key& k2) const
{
	if (k1.first < k2.first) {
		return true;
	} 
	else if (k1.first == k2.first) {
		return k1.second < k2.second;
	}
	return false;
}

} 

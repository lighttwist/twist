/// @file string_utils.ipp
/// Inline implementation file for "string_utils.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/RuntimeError.hpp"
#include "twist/math/numeric_utils.hpp"

#include <charconv>
#include <cstdarg>
#include <sstream>
#include "string_utils.hpp"

namespace std {

template<typename Chr> 
auto operator+(const std::basic_string<Chr>& a, std::basic_string_view<Chr> b) -> std::basic_string<Chr>
{
	std::basic_string<Chr> ret;
	ret.reserve(a.size() + b.size());
	ret = a;
	ret += b;
	return ret;
}


template<typename Chr>
auto operator+(std::basic_string_view<Chr> a, const std::basic_string<Chr>& b) -> std::basic_string<Chr>
{
	std::basic_string<Chr> ret;
	ret.reserve(a.size() + b.size());
	ret = a;
	ret += b;
	return ret;
}

}

namespace twist {

template<class DestChr, class SrcChr>
auto convert_string(const std::basic_string<SrcChr>& src) -> std::basic_string<DestChr>
{
	static_assert(is_one_of<SrcChr, char, wchar_t>, "Unsupported source character type.");	
	static_assert(is_one_of<DestChr, char, wchar_t>, "Unsupported destination character type.");	

	if constexpr (std::is_same_v<SrcChr, DestChr>) {
		return src;
	}
	else if constexpr (std::is_same_v<SrcChr, char>) {
		return to_string(src);
	}
	else {
		return string_to_ansi(src);
	}
}

template<class Chr> 
std::basic_string_view<Chr> make_string_view(const Chr* first, const Chr* last)
{
	return {first, static_cast<size_t>(std::distance(first, last))};
}

template<class Chr> 
[[nodiscard]] auto to_int(const std::basic_string<Chr>& str) -> int
{
	static_assert(is_one_of<Chr, char, wchar_t>, "Unsupported character type.");	

	return std::stoi(str);
}

template<class Chr> 
[[nodiscard]] auto to_int_checked(const std::basic_string<Chr>& str) -> int
{
	static_assert(is_one_of<Chr, char, wchar_t>, "Unsupported character type.");	

	auto idx = size_t{0};
	const auto ret = std::stoi(str, &idx);
	if (idx == 0 || idx != str.size()) {
		TWIST_THRO2(L"String \"{}\" contains invalid characters for an int value.", to_string(str).c_str());
	}
	return ret;
}

template<class Chr>
auto to_uint32_checked(const std::basic_string<Chr>& str) -> uint32_t
{
	static_assert(is_one_of<Chr, char, wchar_t>, "Unsupported character type.");	

	auto idx = size_t{0};
	const auto ret = std::stoul(str, &idx);
	if (idx == 0 || idx != str.size()) {
		TWIST_THRO2(L"String \"{}\" contains invalid characters for a uint32 value.", to_string(str));
	}
    if (ret < std::numeric_limits<uint32_t>::lowest() or std::numeric_limits<uint32_t>::max() < ret) {
		TWIST_THRO2(L"String \"{}\" cannot be converted to a uint32 value.", to_string(str));
    }
	return static_cast<uint32_t>(ret);
}

template<class Chr>
[[nodiscard]] auto to_short_checked(const std::basic_string<Chr>& str) -> short
{
	using namespace twist::math;

	static_assert(is_one_of<Chr, char, wchar_t>, "Unsupported character type.");	

	const auto ret = to_int_checked(str);
	if (ret < short_lowest || short_max < ret) {
		TWIST_THROW(L"String \"%s\" cannot be converted to a short value.", to_string(str).c_str());
	}
	return static_cast<short>(ret);
}

template<class Chr>
[[nodiscard]] auto to_uchar_checked(const std::basic_string<Chr>& str) -> unsigned char
{
	using namespace twist::math;

	static_assert(is_one_of<Chr, char, wchar_t>, "Unsupported character type.");	

	const auto ret = to_int_checked(str);
	if (ret < uchar_lowest || uchar_max < ret) {
		TWIST_THROW(L"String \"%s\" cannot be converted to an unsigned char value.", to_string(str).c_str());
	}
	return static_cast<unsigned char>(ret);
}

template<class Chr>
[[nodiscard]] auto to_longlong_checked(const std::basic_string<Chr>& str) -> long long
{
	static_assert(is_one_of<Chr, char, wchar_t>, "Unsupported character type.");	

	auto idx = size_t{0};
	const auto ret = std::stoll(str, &idx);
	if (idx == 0 || idx != str.size()) {
		TWIST_THROW(L"String \"%s\" contains invalid characters for an int value.", to_string(str).c_str());
	}
	return ret;
}

template<class Chr>
[[nodiscard]] auto to_ssize_checked(const std::basic_string<Chr>& str) -> Ssize
{
#ifdef _WIN64
    return to_longlong_checked(str);
#else
    return to_int_checked(str);
#endif
}

template<class Chr> 
[[nodiscard]] auto to_double(const std::basic_string<Chr>& str) -> double
{
	static_assert(is_one_of<Chr, char, wchar_t>, "Unsupported character type.");	

	try {
		return std::stod(str);
	}
	catch (const std::exception& ex) {
		TWIST_THRO2(L"Error converting string \"{}\" to a double-precision floating-point value: {}", 
		            to_string(str), error_message(ex));
	}
}

template<class Chr> 
[[nodiscard]] auto to_double_checked(const std::basic_string<Chr>& str) -> double
{
	static_assert(is_one_of<Chr, char, wchar_t>, "Unsupported character type.");	

	auto idx = size_t{0};
	auto ret = 0.0;
	try {
		ret = std::stod(str, &idx);
	}
	catch (const std::exception& ex) {
		TWIST_THRO2(L"Error converting string \"{}\" to a double-precision floating-point value: {}", 
		            to_string(str), error_message(ex));
	}
	if (idx == 0 || idx != str.size()) {
		TWIST_THRO2(L"String \"%s\" contains invalid characters for a double-precision floating-point value.", 
				    to_string(str));
	}
	return ret;
}

template<class Chr> 
[[nodiscard]] auto to_float(const std::basic_string<Chr>& str) -> float
{
	static_assert(is_one_of<Chr, char, wchar_t>, "Unsupported character type.");	

	try {
		return std::stof(str);
	}
	catch (const std::exception& ex) {
		TWIST_THRO2(L"Error converting string \"{}\" to a single-precision floating-point value: {}", 
		            to_string(str), error_message(ex));
	}
}

template<class Chr> 
[[nodiscard]] auto to_float_checked(const std::basic_string<Chr>& str) -> float
{
	static_assert(is_one_of<Chr, char, wchar_t>, "Unsupported character type.");	

	auto idx = size_t{0};
	auto ret = 0.f;
	try {
		ret = std::stof(str, &idx);
	}
	catch (const std::exception& ex) {
		TWIST_THRO2(L"Error converting string \"{}\" to a single-precision floating-point value: {}", 
		            to_string(str), error_message(ex));
	}
	if (idx == 0 || idx != str.size()) {
		TWIST_THRO2(L"String \"{}\" contains invalid characters for a single-precision floating-point value.", 
				    to_string(str));
	}
	return ret;
}

template<class Num, class Chr>
requires is_number<Num>
[[nodiscard]] auto to_number_checked(const std::basic_string<Chr>& str) -> Num
{
	static_assert(is_one_of<Chr, char, wchar_t>, "Unsupported character type.");	
	
	if constexpr (std::is_same_v<Num, long long>) {
		return to_longlong_checked(str);
	}
	else if constexpr (std::is_same_v<Num, Ssize>) {
		return to_ssize_checked(str);
	}
	else if constexpr (std::is_same_v<Num, short>) {
		return to_short_checked(str);
	}	
	else if constexpr (std::is_same_v<Num, uint32_t>) {
		return to_uint32_checked(str);
	}	
	else if constexpr (std::is_same_v<Num, unsigned char>) {
		return to_uchar_checked(str);
	}	
	else if constexpr (std::is_same_v<Num, double>) {
		return to_double_checked(str);
	}
	else if constexpr (std::is_same_v<Num, float>) {
		return to_float_checked(str);
	}
	else {
		static_assert(always_false<Num>, "Number type not supported yet.");
	}
}

template<class Chr>
[[nodiscard]] auto equal_no_case(std::basic_string_view<Chr> str1, std::basic_string_view<Chr> str2,
		                         const std::locale& locale) -> bool
{	
	auto pred = CompStringsNoCase<Chr>{locale};
	return pred(str1, str2);
}

template<class Chr>
std::basic_string_view<Chr> trim_whitespace_to_view(std::basic_string_view<Chr> str_view)
{
	auto bptr = str_view.data();
	auto eptr = str_view.data() + str_view.size();
	if (bptr == eptr) return str_view;

	while (is_whitespace_char(*bptr)) ++bptr;
	
	--eptr;
	while (is_whitespace_char(*eptr)) --eptr;
	
	return make_string_view(bptr, eptr + 1);
}

template<class Chr>
[[nodiscard]] auto replace_all_substr(std::basic_string_view<Chr> str, 
									  std::basic_string_view<Chr> old_substr, 
									  std::basic_string_view<Chr> new_substr) -> std::basic_string<Chr>
{
	std::basic_string<Chr> new_str{ str };

	size_t pos = 0;
	const auto old_substr_len = old_substr.size();	
	const auto new_substr_len = new_substr.size();

	while ((pos = new_str.find(old_substr, pos)) != std::basic_string<Chr>::npos) {
		new_str.replace(pos, old_substr_len, new_substr);
		pos += new_substr_len;
	}
	
	return new_str;
}

template<class Chr>
auto replace_all_substr_in_place(std::basic_string<Chr>& str, 
								 std::basic_string_view<Chr> old_substr, 
								 std::basic_string_view<Chr> new_substr) -> void
{
	auto pos = size_t{0};
	const auto old_substr_len = old_substr.size();	
	const auto new_substr_len = new_substr.size();

	while ((pos = str.find(old_substr, pos)) != std::basic_string<Chr>::npos) {
		str.replace(pos, old_substr_len, new_substr);
		pos += new_substr_len;
	}
}

template<class... Args>
auto formatv(std::wstring_view format, const Args&... args) -> std::wstring
{
	auto format_args = std::make_format_args<std::wformat_context>(args...);
	return std::vformat(format, std::move(format_args));
}

template<std::output_iterator<std::wstring> OutIter>
auto extract_delimited_fields(const std::wstring& str, const std::wstring& del, OutIter dest, bool trim) -> Ssize
{
	const auto count = count_delimited_fields(str, del);
	for (int i = 0; i < count; ++i) {
		std::wstring field;
		get_delimited_field(str, del, i, field); //+ Implementation should be more efficient, as  get_delimited_field()  re-parses the string every time
		if (trim) {
			field = trim_whitespace(field);
		}
		*dest++ = field;
	}
	return count;
}

template<std::input_iterator Iter>
[[nodiscard]] auto assemble_delimited_fields(Iter first, Iter last, const std::wstring& del, bool trim) -> std::wstring
{
	std::wstringstream str;
	size_t count = std::distance(first, last);
	if (count > 0) {
		for (; first != last; ++first, --count) {
			std::wstring field(*first);
			if (trim) {
				field = trim_whitespace(field);
			}
			str << field;
			if (count > 1) {
				str << del;
			}
		}
	}
	return str.str();
}

template<class Chr, rg::input_range Rng> 
[[nodiscard]] auto concat(const Rng& range, 
                          std::basic_string_view<Chr> separator, 
                          std::basic_string_view<Chr> quote, 
                          std::basic_string_view<Chr> and_str) -> std::basic_string<Chr>
{
	return concat(range, std::identity{}, separator, quote, and_str);
}

template<class Chr, 
         rg::input_range Rng,
         std::invocable<rg::range_const_reference_t<Rng>> Transform> 
[[nodiscard]] auto concat(const Rng& range, 
                          Transform transform,
                          std::basic_string_view<Chr> separator, 
                          std::basic_string_view<Chr> quote, 
                          std::basic_string_view<Chr> and_str) -> std::basic_string<Chr>
{
	auto ret = std::basic_string<Chr>{};
	const auto end_it = end(range);
	for (auto it = begin(range); it != end_it; ++it) {
		ret.append(quote).append(std::invoke(transform, *it)).append(quote);
		const auto next_it = std::next(it);
		if (next_it != end_it) {
			// This is not last element in the container
			if (std::next(next_it) == end_it && !and_str.empty()) {
				// This is the element before last and we are to use the "and" string
				ret.append(and_str);
				continue;
			}
			ret.append(separator);
		}
	}
	return ret;
}

template<rg::input_range Rng> 
[[nodiscard]] auto wconcat(const Rng& range, 
                           std::wstring_view separator, 
                           std::wstring_view quote, 
                           std::wstring_view and_str) -> std::wstring
{
    return concat<wchar_t>(range, separator, quote, and_str);
}

template<rg::input_range Rng,
         std::invocable<rg::range_const_reference_t<Rng>> Transform> 
[[nodiscard]] auto wconcat(const Rng& range, 
                           Transform transform,
                           std::wstring_view separator, 
                           std::wstring_view quote, 
                           std::wstring_view and_str) -> std::wstring
{
    return concat<wchar_t>(range, std::move(transform), separator, quote, and_str);
}

template<class Chr>
[[nodiscard]] auto split_with_quotes(const std::basic_string_view<Chr> str, Chr delim_char, Chr quote_char) 
                    -> std::vector<std::basic_string<Chr>>
{
	static_assert(is_one_of<Chr, char, wchar_t>, "Unsupported character type.");	

	std::vector<std::basic_string<Chr>> fields;
    std::basic_string<Chr> field;

	const auto trimmed_str = trim_whitespace(str); 
	std::basic_stringstream<Chr> stream{ trimmed_str };

	// Look for the next beginning quote (or the end) 
    while (std::getline(stream, field, quote_char)) {
		// Get the string before the next beginning quote (or before the end) and split it into fields by delimiter
        std::basic_stringstream<Chr> stream_outside_quotes{ trim_whitespace(field) };
        while (std::getline(stream_outside_quotes, field, delim_char)) {
            fields.push_back(trim_whitespace(field));
        }
		// Get the string between the quotes, which is one field 
        if (std::getline(stream, field, quote_char)) {
            fields.push_back(field);
			// After the end quote, keep going until after the next delimiter (which should be after it) or the end
			std::getline(stream, field, delim_char);
        }
    }
	// If the string ends in a delimiter, we count that as one more blank element at the end
 	if (!trimmed_str.empty() && trimmed_str.back() == delim_char) {
 		fields.push_back({});
 	}

	return fields;
}

template<typename Chr>
auto to_upper(std::basic_string<Chr>& str, const std::locale& loc) -> void
{
	for (auto p = str.begin(); p != str.end(); ++p) {
		*p = std::use_facet<std::ctype<Chr>>(loc).toupper(*p);
	}
}

template<typename Chr>  
auto to_lower(std::basic_string<Chr>& str, const std::locale& loc) -> void
{
	for (auto p = str.begin(); p != str.end(); ++p) {
		*p = std::use_facet<std::ctype<Chr>>(loc).tolower(*p);
	}
}

template<class Chr>
auto reset(std::basic_stringstream<Chr>& sstream) -> void
{
	sstream.str(std::basic_string<Chr>{});
	sstream.clear();
}

template<class... Args>
auto formatv(std::string_view format, const Args&... args)
{
	auto format_args = std::make_format_args<std::format_context>(args...);
	return std::vformat(format, std::move(format_args));
}

template<class Chr>
std::basic_string<Chr> to_string_of_type(std::string_view ansi_str)
{
	return { ansi_str.data(), ansi_str.data() + ansi_str.size() };
}


template<class T, class>  
T to_number(std::wstring_view str)  
{
	return to_number<T>( string_to_ansi(str) );
}


template<class T, class>  
T to_number(const std::wstring& str)  
{
	if constexpr (std::is_same_v<T, int32_t>) { 
		return to_int(str);
	}
	else if constexpr (std::is_same_v<T, float>) { 
		return to_float(str);
	}
	else if constexpr (std::is_same_v<T, double>) { 
		return to_double(str);
	}
	else { 
		return to_number<T>(string_to_ansi(str));
	}
}


template<class T, size_t len, class>  
T to_number(const wchar_t (&str)[len])  
{
	return to_number<T>( string_to_ansi(std::wstring_view{str, len}) );
}

template<class T, class>  
T to_number(std::string_view str)  
{
	T num{};
	std::from_chars(str.data(), str.data() + str.length(), num);
	return num;
}

template<class T, class>  
T to_number_checked(std::string_view str)  
{
	const auto end_str = str.data() + str.length();

	T num{};
	const auto [ptr, err] = std::from_chars(str.data(), end_str, num);

    if (err == std::errc::invalid_argument) {
        TWIST_THROW(L"\"Invalid argument\" error ocurred while converting string \"%s\" to a number.", 
				ansi_to_string(str).c_str());
    }
    else if (err == std::errc::result_out_of_range) {
        TWIST_THROW(L"\"Result out of range\" error ocurred while converting string \"%s\" to a number.", 
				ansi_to_string(str).c_str());
    }
    else if (err != std::errc{}) {
        TWIST_THROW(L"Unknown error ocurred while converting string \"%s\" to a number.", 
				ansi_to_string(str).c_str());
    }
	else if (ptr != end_str) {
        TWIST_THROW(L"Not all characters of string \"%s\" could be converted to a number.", 
				ansi_to_string(str).c_str());			
	}

	return num;
}

constexpr auto is_newline(wchar_t chr) -> bool
{
	return chr == L'\n';
}

template<class Chr> 
constexpr auto is_carriage_return(Chr chr) -> bool
{
	static_assert(is_one_of<Chr, char, wchar_t>, "Unsupported character type.");	

	if constexpr (std::is_same_v<Chr, char>) {
		return chr == '\r';
	}
	else {
		return chr == L'\r';
	}
}

constexpr auto is_comma(wchar_t chr) -> bool
{
	return chr == L',';
}

constexpr auto is_double_quote(wchar_t chr) -> bool
{
	return chr == L'\"';
}

template<class Chr>
auto is_whitespace_char(Chr chr) -> bool
{
	static_assert(is_one_of<Chr, char, wchar_t>, "Unsupported character type.");	

	if constexpr (std::is_same_v<Chr, char>) {
		return isspace(chr);
	}
	else {
		return iswspace(chr);
	}
}

template<class Chr, class TokenFunc> 
auto tokenise(std::basic_string_view<Chr> str, gsl::not_null<const Chr*> delims, TokenFunc token_func) -> void
{
	static_assert(is_one_of<Chr, char, wchar_t>, "Unsupported character type.");	

	constexpr bool using_char = std::is_same_v<Chr, char>;
	constexpr bool using_wchar = std::is_same_v<Chr, wchar_t>;

	if constexpr (using_char) assert(strlen(delims) > 0);
	if constexpr (using_wchar) assert(wcslen(delims) > 0);		

	std::basic_string<Chr> buffer{str};
	Chr* context = nullptr;

	auto tokenise_raw = [delims, &context](auto s) {
		if constexpr (using_char) return strtok_s(s, delims, &context);
		if constexpr (using_wchar) return wcstok_s(s, delims, &context);
	};

	// Get the first token, if any
	auto token = tokenise_raw(buffer.data());

	// While there are any tokens left in the buffer...
	while (token != nullptr) {
		// Report the current token
		token_func(token);
		// Get the next token, if any
		token = tokenise_raw(nullptr);
	}
}	

template<class Chr> 
auto tokenise(std::basic_string_view<Chr> str, gsl::not_null<const Chr*> delimiters) 
      -> std::vector<std::basic_string<Chr>>
{
	std::vector<std::basic_string<Chr>> tokens;
	tokenise(str, delimiters, [&tokens](auto tok){ tokens.push_back(tok); });
	return tokens;
}

template<class Chr>
constexpr auto newline_char() -> Chr
{
	static_assert(is_one_of<Chr, char, wchar_t>, "Unsupported character type.");	

	if constexpr (std::is_same_v<Chr, char>) {
		return '\n';
	}
	else {
		return L'\n';
	}
}

template<class Chr>
constexpr auto comma_char() -> Chr
{
	static_assert(is_one_of<Chr, char, wchar_t>, "Unsupported character type.");	

	if constexpr (std::is_same_v<Chr, char>) {
		return ',';
	}
	else {
		return L',';
	}
}

template<class Chr>
constexpr auto comma_str() -> const Chr*
{
	static_assert(is_one_of<Chr, char, wchar_t>, "Unsupported character type.");	

	if constexpr (std::is_same_v<Chr, char>) {
		return ",";
	}
	else {
		return L",";
	}
}

template<class Chr>
constexpr auto underscore_char() -> Chr
{
	static_assert(is_one_of<Chr, char, wchar_t>, "Unsupported character type.");	

	if constexpr (std::is_same_v<Chr, char>) {
		return '_';
	}
	else {
		return L'_';
	}
}

} 

///  @file  ElemSubset.hpp
///  ElemSubset class template

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_ELEM_SUBSET_HPP
#define TWIST_ELEM_SUBSET_HPP

namespace twist {

// Element subset types
enum class ElemSubsetType {
	k_no_elems       = 1,  // A subset containing no elements: the void subset
	k_specific_elems = 2,  // A subset containing one or more specific elements of its superset
	k_all_elems      = 3   // A subset containing all possible elements from its superset
};

///
///  Class template representing a subset of a set of elements of a given type; the subset can be void (empty), can contain 
///    specific elements or can contain all possible elements in its superset (which may or may not be finite). 
///  Template parameter:
///    TElem  The type of the set elements
///
template<typename TElem> 
class ElemSubset {
public:
	typedef typename TElem  Elem;

	/// Constructor. Creates a subset containing no elements.
	///
	ElemSubset();
	
	/// Copy constructor.
	///
	ElemSubset(const ElemSubset& src);

	/// Move constructor.
	///
	ElemSubset(ElemSubset&& src);

	/// Copy-assignment operator.
	///
	ElemSubset& operator=(const ElemSubset& rhs);

	/// Move-assignment operator.
	///
	ElemSubset& operator=(ElemSubset&& rhs);

	/// Get the subset type.
	///
	/// @return  The type
	///
	ElemSubsetType get_type() const;

	/// Set the subset type to "no elements".
	///
	void set_no_elems();

	/// Get the specific elements contained in the subset. An exception is throws if the subset type is other than 
	/// "specific elements".
	///
	/// @return  The elements
	///
	const std::set<Elem>& get_specific_elems() const;

	/// Set the subset type to "specific elements".
	///
	/// @param[in] elems  The specific elements; should not be empty
	///
	void set_specific_elems(const std::set<Elem>& elems);

	/// Set the subset type to "all elements".
	///
	void set_all_elems();
	
private:	
	ElemSubsetType  type_;
	std::set<Elem>  elems_;

	TWIST_CHECK_INVARIANT_DECL
};


//
//  Free functions
//

/// Create a subset whose type is "all elements" (contains all the elements in its superset).
///
/// @tparam  TElem  The element type
/// @return  The subset
/// 
template<typename TElem> ElemSubset<TElem> make_subset_wih_all_elems();

/// Create a subset whose type is "specific elements" (contains specific elements of its superset).
///
/// @tparam  TElem  The element type
/// @param[in] elems  The elements which will be in the subset
/// @return  The subset
/// 
template<typename TElem> ElemSubset<TElem> make_subset_wih_specific_elems(const std::set<TElem>& elems);

/// Find out whether an element subset contains a specific element.
///
/// @param[in] subset  The subset
/// @param[in] elem  The element
/// @return  true if it does
///
template<typename TElem> bool has(const ElemSubset<TElem>& subset, const TElem& elem);

} 

#include "ElemSubset.ipp"

#endif 

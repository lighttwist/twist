/// @file graphics.ipp
/// Inline implementation file for "graphics.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include <cmath>

#include "twist/math/numeric_utils.hpp"

namespace twist::gfx {

//
//  GVector class
//

template <typename Coord>
inline GVector<Coord>::GVector() : 
	x_(0), y_(0) 
{
}


template <typename Coord>
inline GVector<Coord>::GVector(double x, double y) : 
	x_(x), y_(y) 
{
}
	
	
template <typename Coord>
inline GVector<Coord>::GVector(const GVector& orig) : 
	x_(orig.x_), y_(orig.y_) 
{
}

	
template <typename Coord>
inline void GVector<Coord>::operator=(const GVector& rhs) 
{
	x_ = rhs.x_;
	y_ = rhs.y_;
}


template <typename Coord>
inline GVector<Coord> GVector<Coord>::operator-() const 
{
	return GVector(-x_, -y_);
}

	
template <typename Coord>
inline Coord GVector<Coord>::x() const 
{
	return x_;
}
	
	
template <typename Coord>
inline Coord GVector<Coord>::get_x() const 
{
	return x_;
}
	
	
template <typename Coord>
inline void GVector<Coord>::set_x(double x) 
{
	x_ = x;
}

	
template <typename Coord>
inline Coord GVector<Coord>::y() const 
{
	return y_;
}
	
	
template <typename Coord>
inline Coord GVector<Coord>::get_y() const 
{
	return y_;
}


template <typename Coord>
inline void GVector<Coord>::set_y(double y) 
{
	y_ = y;
}

	
template <typename Coord>
inline void GVector<Coord>::set(double x, double y) 
{
	x_ = x;
	y_ = y;
}


template <typename Coord>
inline double GVector<Coord>::length() const 
{
	return sqrt(x_ * x_ + y_ * y_);
}
	

template <typename Coord>
inline double GVector<Coord>::get_length() const 
{
	return length();
}
	
	
template <typename Coord>
inline void GVector<Coord>::operator+=(const GVector& rhs) 
{
	x_ += rhs.x_;
	y_ += rhs.y_;
}


template <typename Coord>
inline GVector<Coord> GVector<Coord>::operator+(const GVector<Coord>& rhs) const 
{
	return GVector<Coord>(x_ + rhs.x_, y_ + rhs.y_);
}


template <typename Coord>
inline void GVector<Coord>::operator*=(double rhs) 
{
	x_ *= rhs;
	y_ *= rhs;
}


template <typename Coord>
inline GVector<Coord> GVector<Coord>::operator*(double rhs) const 
{
	return GVector<Coord>(x_ * rhs, y_ * rhs);
}


//
//   Point  class
//

template<class Coord, class CompPolicy>
inline Point<Coord, CompPolicy>::Point() : 
	x_(0), y_(0) 
{
}


template<class Coord, class CompPolicy>
inline Point<Coord, CompPolicy>::Point(Coord x, Coord y) : 
	x_(x), y_(y) 
{
}


template<class Coord, class CompPolicy>
inline void Point<Coord, CompPolicy>::operator=(const Point& rhs)
{
	x_ = rhs.x_;
	y_ = rhs.y_; 
}


template<class Coord, class CompPolicy>
inline bool Point<Coord, CompPolicy>::operator==(const Point& rhs) const 
{
	return equal(x_, rhs.x_) && equal(y_, rhs.y_);
}

template<class Coord, class CompPolicy>
inline bool Point<Coord, CompPolicy>::operator!=(const Point& rhs) const 
{
	return not operator==(rhs);
}

template<class Coord, class CompPolicy>
inline Point<Coord, CompPolicy> Point<Coord, CompPolicy>::operator+(const GVector<Coord>& displacement) const
{
	return Point(x_ + displacement.get_x(), y_ + displacement.get_y());	
}


template<class Coord, class CompPolicy>
inline GVector<Coord> Point<Coord, CompPolicy>::operator-(const Point& rhs) const 
{
	return GVector<Coord>(x_ - rhs.x_, y_ - rhs.y_);
}


template<class Coord, class CompPolicy>
inline Coord Point<Coord, CompPolicy>::x() const 
{
	return x_;
}


template<class Coord, class CompPolicy>
inline Coord Point<Coord, CompPolicy>::get_x() const 
{
	return x_;
}


template<class Coord, class CompPolicy>
inline void Point<Coord, CompPolicy>::set_x(Coord x) 
{
	x_ = x;
}


template<class Coord, class CompPolicy>
inline Coord Point<Coord, CompPolicy>::y() const 
{
	return y_;
}


template<class Coord, class CompPolicy>
inline Coord Point<Coord, CompPolicy>::get_y() const 
{
	return y_;
}


template<class Coord, class CompPolicy>
inline void Point<Coord, CompPolicy>::set_y(Coord y) 
{
	y_ = y;
}


template <typename Coord, typename CompPolicy>
inline Point<Coord, CompPolicy> Point<Coord, CompPolicy>::get_location() const
{
	return Point(x_, y_);	
}

	
template <typename Coord, typename CompPolicy>
inline void Point<Coord, CompPolicy>::set_location(Coord x, Coord y)
{
	x_ = x;
	y_ = y;
}
	

template <typename Coord, typename CompPolicy>
inline void Point<Coord, CompPolicy>::set_location(const Point& loc)
{
	x_ = loc.get_x();
	y_ = loc.get_y();
}


template<class Coord, class CompPolicy>
inline void Point<Coord, CompPolicy>::set(Coord x, Coord y) 
{
	x_ = x;
	y_ = y;
}


template<class Coord, class CompPolicy>
inline void Point<Coord, CompPolicy>::move(const GVector<Coord>& displacement) 
{
	x_ += displacement.get_x();
	y_ += displacement.get_y();
}


//
//   Segment  class
//

template<class Coord, class CompPolicy>
inline Segment<Coord, CompPolicy>::Segment() :
	p1_(), p2_()
{
}
	
	
template<class Coord, class CompPolicy>
inline Segment<Coord, CompPolicy>::Segment(const PointType& p1, const PointType& p2) :
	p1_(p1), p2_(p2)
{
}
	
	
template<class Coord, class CompPolicy>
inline Segment<Coord, CompPolicy>::Segment(const Segment& orig) :
	p1_(orig.p1_), p2_(orig.p2_)
{
}
	
	
template<class Coord, class CompPolicy>
inline void Segment<Coord, CompPolicy>::operator=(const Segment& rhs)
{
	p1_ = rhs.p1_;
	p2_ = rhs.p2_;
}


template<class Coord, class CompPolicy>
inline bool Segment<Coord, CompPolicy>::operator==(const Segment& rhs) const
{
	return equal(p1_, rhs.p1_) && equal(p2_, rhs.p2_);
}


template<class Coord, class CompPolicy>
inline const typename Segment<Coord, CompPolicy>::PointType& Segment<Coord, CompPolicy>::get_p1() const
{
	return p1_;
}
	
	
template<class Coord, class CompPolicy>
inline void Segment<Coord, CompPolicy>::set_p1(const PointType& p1)
{
	p1_ = p1;
}


template<class Coord, class CompPolicy>
inline const typename Segment<Coord, CompPolicy>::PointType& Segment<Coord, CompPolicy>::get_p2() const
{
	return p2_;
}


template<class Coord, class CompPolicy>
inline void Segment<Coord, CompPolicy>::set_p2(const PointType& p2)
{
	p2_ = p2;
}


template<class Coord, class CompPolicy>
inline double Segment<Coord, CompPolicy>::get_length() const
{
	return distance(p1_, p2_);
}


template<class Coord, class CompPolicy>
inline bool Segment<Coord, CompPolicy>::contains(const PointType& point) const
{
	return equal(distance(point, p1_) + distance(point, p2_), get_length());
}


template<class Coord, class CompPolicy>
inline void Segment<Coord, CompPolicy>::get_line_equation(double& m, double& n) const
{
    if (equal(p1_.get_x(), p2_.get_x())) {
    	TWIST_THROW(L"A vertical line s equation cannot be written as  y = mx + n .");
    }

	m = (p2_.get_y() - p1_.get_y()) / (p2_.get_x() - p1_.get_x());
	n = (p2_.get_x() * p1_.get_y() - p1_.get_x() * p2_.get_y()) / (p2_.get_x() - p1_.get_x());
}


template<class Coord, class CompPolicy>
double Segment<Coord, CompPolicy>::dist_to_point(const PointType& pt, PointType* segment_pt)
{
    //  We are implementing the following algorithm - considering  P = point,  P1 = p1,  P2 = p2,
    //  v,  w  vectors with  w·v  their dot product,  b,  c1,  c2  scalars and  Pb  the base of the perpendicular
    //  from  P  to the segment.
    //
	//  v = P2 - P1
    //  w = P  - P1
    //
    //  if ((c1 = w·v) <= 0)
    //    return dist(P, P1)
    //
    //  if ((c2 = v·v) <= c1)
    //    return dist(P, P2)
	//
    //  b  = c1 / c2
    //  Pb = P1 + bv
    //  return dist(P, Pb)
    
    double dist = 0;
    PointType sgmPt;
    bool found = false;

    double vx = p2_.get_x() - p1_.get_x();
    double vy = p2_.get_y() - p1_.get_y();
    double wx = pt.get_x() - p1_.get_x();
    double wy = pt.get_y() - p1_.get_y();

    const double c1 = vx * wx + vy * wy;
	double c2 = 0;

	if (greater_or_equal(0, c1)) {
    	sgmPt = p1_;
      	dist = distance(pt, p1_);
      	found = true;
    }
    
    if (!found) {
		c2 = vx * vx + vy * vy;
		if (greater_or_equal(c1, c2)) {
    		sgmPt = p2_;
    		dist = distance(pt, p2_);
    		found = true;
		}
	}
		
	if (!found) {
		const double b = c1 / c2;
		sgmPt.set(p1_.get_x() + b * vx, p1_.get_y() + b * vy);		
		dist = distance(pt, sgmPt);
	}
		
	if (segment_pt != nullptr) {
		*segment_pt = sgmPt;
	}
	return dist;
}


template<class Coord, class CompPolicy>
typename Segment<Coord, CompPolicy>::PointType Segment<Coord, CompPolicy>::get_point_along(double dist_ratio) const
{
	if (dist_ratio < 0 || dist_ratio > 1) {
		TWIST_THROW(L"Invalid distance ratio %f. Ratio must be in the interval [0, 1].", dist_ratio);
	}
	const Coord x = dist_ratio * (p2_.get_x() - p1_.get_x()) + p1_.get_x();
	const Coord y = dist_ratio * (p2_.get_y() - p1_.get_y()) + p1_.get_y();
	return PointType(x, y);
}


template<class Coord, class CompPolicy>
bool Segment<Coord, CompPolicy>::intersects(const Segment& other, PointType* intr)
{
	//  Fail if either line segment is zero-length.
	if ((p1_ == p2_) || (other.p1_ == other.p2_)) {
		return false;
	}

	// Denote this segment AB and the other CD

	double ax = p1_.get_x();
	double ay = p1_.get_y();
	double bx = p2_.get_x();
	double by = p2_.get_y();
	double cx = other.p1_.get_x();
	double cy = other.p1_.get_y();
	double dx = other.p2_.get_x();
	double dy = other.p2_.get_y();
	
	double distAB = 0;
	double theCos = 0;
	double theSin = 0;
	double newX = 0;
	double posAB = 0;

	// Check whether the segments share an end-point.
	if ((ax == cx && ay == cy) || (bx == cx && by == cy) || (ax == dx && ay == dy) || (bx == dx && by == dy)) {
		//+ intersection
		return true; 
	}

	// (1) Translate the system so that point A is on the origin.
	bx -= ax; 
	by -= ay;
	cx -= ax; 
	cy -= ay;
	dx -= ax; 
	dy -= ay;

	// Discover the length of segment AB.
	distAB = sqrt(bx * bx + by * by);

	// (2) Rotate the system so that point B is on the positive X axis.
	theCos = bx / distAB;
	theSin = by / distAB;
	newX   = cx * theCos + cy * theSin;
	cy     = cy * theCos - cx * theSin; 
	cx     = newX;
	newX   = dx * theCos + dy * theSin;
	dy     = dy * theCos - dx * theSin; 
	dx     = newX;

	// Check whether the segments are colinear.
	if (cy == 0 && dy == 0) {
		if (cx >= 0 && cx <= distAB) {
			if (intr != nullptr) {
				intr->set_x(cx + posAB * theCos);
				intr->set_y(cy + posAB * theSin);
			}
			return true;
		}
		else if (dx >= 0 && dx <= distAB) {
			if (intr != nullptr) {
				intr->set_x(dx + posAB * theCos);
				intr->set_y(dy + posAB * theSin);
			}
			return true;
		}
		else {
			return false;
		}
	}

	// Fail if segment CD doesn t cross line AB.
	if (cy < 0 && dy < 0 || cy >= 0 && dy >= 0) {
		return false;
	}

	// (3) Discover the position of the intersection point along line AB.
	posAB = dx + (cx - dx) * dy / (dy - cy);

	//  Fail if segment CD crosses line AB outside of segment AB.
	if (posAB < 0 || posAB > distAB) {
		return false;
	}

	// (4) Apply the discovered position to line AB in the original coordinate system.
	if (intr != nullptr) {
		intr->set_x(ax + posAB * theCos);
		intr->set_y(ay + posAB * theSin);
	}

	// Success.
	return true;
}

template<class Coord, class CompPolicy>
void Segment<Coord, CompPolicy>::move(const GVector<Coord>& displacement)
{
	p1_.move(displacement);
	p2_.move(displacement);
}

// --- Dimensions class ---

template<class Coord, class CompPolicy>
inline Dimensions<Coord, CompPolicy>::Dimensions() : 
	width_(0), height_(0) 
{
}

template<class Coord, class CompPolicy>
inline Dimensions<Coord, CompPolicy>::Dimensions(Coord width, Coord height) : 
	width_(width), height_(height) 
{
}

template<class Coord, class CompPolicy>
inline Dimensions<Coord, CompPolicy>::Dimensions(const Dimensions& orig) : 
	width_(orig.width_), height_(orig.height_) 
{
}

template<class Coord, class CompPolicy>
inline void Dimensions<Coord, CompPolicy>::operator=(const Dimensions& rhs) 
{
	set(rhs.width_, rhs.height_);
} 

template<class Coord, class CompPolicy>
inline auto Dimensions<Coord, CompPolicy>::operator==(const Dimensions& rhs) const -> bool
{
	return equal(width_, rhs.width_) && equal(height_, rhs.height_);
} 

template<class Coord, class CompPolicy>
inline auto Dimensions<Coord, CompPolicy>::operator!=(const Dimensions& rhs) const -> bool
{
	return not operator==(rhs);
} 

template<class Coord, class CompPolicy>
inline auto Dimensions<Coord, CompPolicy>::width() const -> Coord
{
	return width_;
}

template<class Coord, class CompPolicy>
inline Coord Dimensions<Coord, CompPolicy>::get_width() const 
{
	return width_;
}

template<class Coord, class CompPolicy>
inline void Dimensions<Coord, CompPolicy>::set_width(Coord width) 
{
	width_ = width;
}

template<class Coord, class CompPolicy>
inline auto Dimensions<Coord, CompPolicy>::height() const -> Coord
{
	return height_;
}

template<class Coord, class CompPolicy>
inline Coord Dimensions<Coord, CompPolicy>::get_height() const 
{
	return height_;
}

template<class Coord, class CompPolicy>
inline void Dimensions<Coord, CompPolicy>::set_height(Coord height) 
{
	height_ = height;
}

template<class Coord, class CompPolicy>
inline void Dimensions<Coord, CompPolicy>::set(Coord width, Coord height) 
{
	width_ = width;
	height_ = height;
}

// --- Rect class ---

template<class Coord, class CompPolicy>
inline Rect<Coord, CompPolicy>::Rect() 
    : x_{0}
    , y_{0}
    , width_{0}
    , height_{0}
{
}

template<class Coord, class CompPolicy>
inline Rect<Coord, CompPolicy>::Rect(Coord x, Coord y, Coord width, Coord height) 
    : x_{x}
    , y_{y}
    , width_{width}
    , height_{height}
{
}

template<class Coord, class CompPolicy>
inline Rect<Coord, CompPolicy>::Rect(PointType pt1, PointType pt2)
    : x_{std::min(pt1.x(), pt2.x())}
    , y_{std::min(pt1.y(), pt2.y())}
    , width_{std::abs(pt1.x() - pt2.x())}
    , height_{std::abs(pt1.y() - pt2.y())}
{
}

template<class Coord, class CompPolicy>
inline Rect<Coord, CompPolicy>::Rect(const Rect& orig) 
    : x_{orig.x_}
    , y_{orig.y_}
    , width_{orig.width_}
    , height_{orig.height_} 
{
}

template<class Coord, class CompPolicy>
inline void Rect<Coord, CompPolicy>::operator=(const Rect& rhs) 
{
	set(rhs.x_, rhs.y_, rhs.width_, rhs.height_);
} 


template<class Coord, class CompPolicy>
inline bool Rect<Coord, CompPolicy>::operator==(const Rect& rhs) const 
{
	return equal(x_, rhs.x_) && equal(y_, rhs.y_) && equal(width_, rhs.width_) && equal(height_, rhs.height_);
} 


template<class Coord, class CompPolicy>
inline bool Rect<Coord, CompPolicy>::operator!=(const Rect& rhs) const 
{
	return !equal(x_, rhs.x_) || !equal(y_, rhs.y_) || !equal(width_, rhs.width_) || !equal(height_, rhs.height_);
} 

template<class Coord, class CompPolicy>
inline auto Rect<Coord, CompPolicy>::x() const -> Coord
{
	return x_;
}

template<class Coord, class CompPolicy>
inline Coord Rect<Coord, CompPolicy>::get_x() const 
{
	return x_;
}

template<class Coord, class CompPolicy>
inline void Rect<Coord, CompPolicy>::set_x(Coord x) 
{
	x_ = x;
}

template<class Coord, class CompPolicy>
inline auto Rect<Coord, CompPolicy>::y() const -> Coord
{
	return y_;
}

template<class Coord, class CompPolicy>
inline Coord Rect<Coord, CompPolicy>::get_y() const 
{
	return y_;
}


template<class Coord, class CompPolicy>
inline void Rect<Coord, CompPolicy>::set_y(Coord y) 
{
	y_ = y;
}


template <typename Coord, typename CompPolicy>
inline typename Rect<Coord, CompPolicy>::PointType Rect<Coord, CompPolicy>::get_location() const
{
	return PointType(x_, y_);	
}

	
template <typename Coord, typename CompPolicy>
inline void Rect<Coord, CompPolicy>::set_location(Coord x, Coord y)
{
	x_ = x;
	y_ = y;
}
	
	
template <typename Coord, typename CompPolicy>
inline void Rect<Coord, CompPolicy>::set_location(const PointType& loc)
{
	x_ = loc.get_x();
	y_ = loc.get_y();
}


template<class Coord, class CompPolicy>
inline void Rect<Coord, CompPolicy>::move(const GVector<Coord>& displacement) 
{
	x_ += displacement.get_x();
	y_ += displacement.get_y();
}

template<class Coord, class CompPolicy>
inline auto Rect<Coord, CompPolicy>::width() const -> Coord 
{
	return width_;
}

template<class Coord, class CompPolicy>
inline Coord Rect<Coord, CompPolicy>::get_width() const 
{
	return width_;
}

template<class Coord, class CompPolicy>
inline void Rect<Coord, CompPolicy>::set_width(Coord width) {
	width_ = width;
}

template<class Coord, class CompPolicy>
inline Coord Rect<Coord, CompPolicy>::get_height() const 
{
	return height_;
}

template<class Coord, class CompPolicy>
inline auto Rect<Coord, CompPolicy>::height() const -> Coord
{
	return height_;
}

template<class Coord, class CompPolicy>
inline void Rect<Coord, CompPolicy>::set_height(Coord height) 
{
	height_ = height;
}


template<class Coord, class CompPolicy>
inline void Rect<Coord, CompPolicy>::set(Coord x, Coord y, Coord width, Coord height) 
{
	x_ = x;
	y_ = y;
	width_ = width;
	height_ = height;
}


template<class Coord, class CompPolicy>
inline Coord Rect<Coord, CompPolicy>::get_centre_x() const 
{
	return x_ + width_ / 2;
}


template<class Coord, class CompPolicy>
inline Coord Rect<Coord, CompPolicy>::get_centre_y() const 
{
	return y_ + height_ / 2;
}


template<class Coord, class CompPolicy>
inline typename Rect<Coord, CompPolicy>::PointType Rect<Coord, CompPolicy>::get_centre() const 
{
	return PointType(get_centre_x(), get_centre_y());
}


template<class Coord, class CompPolicy>
inline void Rect<Coord, CompPolicy>::get_bounding_rect(Rect& rect) const 
{
	rect = *this;
}


template<class Coord, class CompPolicy>
inline bool Rect<Coord, CompPolicy>::contains(const PointType& point) const 
{
	return (point.get_x() >= x_) && (x_ + width_ >= point.get_x()) &&
		   (point.get_y() >= y_) && (y_ + height_ >= point.get_y());
}


template<class Coord, class CompPolicy> 
void Rect<Coord, CompPolicy>::add(const PointType& point) 
{
	if (x_ > point.get_x()) {
		width_ += x_ - point.get_x();
		x_ = point.get_x();
	}
	else if (x_ + width_ < point.get_x()) {
		width_ = point.get_x() - x_;
	}
	if (y_ > point.get_y()) {
		height_ += y_ - point.get_y();
		y_ = point.get_y();
	}
	else if (y_ + height_ < point.get_y()) {
		height_ = point.get_y() - y_;
	}
}


template<class Coord, class CompPolicy>
void Rect<Coord, CompPolicy>::add(const Rect& rect) 
{
	Coord left   = std::min(x_, rect.x_);
	Coord top    = std::min(y_, rect.y_);
	Coord right  = std::max(x_ + width_, rect.x_ + rect.width_);
	Coord bottom = std::max(y_ + height_, rect.y_ + rect.height_);
	set(left, top, right - left, bottom - top);
}

template<class Coord, class CompPolicy> 
void Rect<Coord, CompPolicy>::inflate(Coord dist)
{
	if (dist < 0 && (abs(dist) > width_ / 2 || abs(dist) > height_ / 2)) {
		TWIST_THRO2(L"Invalid deflate value {}: the rectangle is too small.", dist);		
	}

	x_ -= dist;
	y_ -= dist;
	width_  += dist * 2;
	height_ += dist * 2;
}

template<class Coord, class CompPolicy> 
bool Rect<Coord, CompPolicy>::contains(const Rect& rect) const
{
	return greater_or_equal(rect.get_x(), x_) && 
		   greater_or_equal(rect.get_y(), y_) &&
		   greater_or_equal(x_ + width_, rect.x_ + rect.width_) &&
		   greater_or_equal(y_ + height_, rect.y_ + rect.height_);
}


template<class Coord, class CompPolicy> 
bool Rect<Coord, CompPolicy>::intersects(const SegmentType& segm, std::vector<PointType>* points) const
{
	bool retVal = 0;

	// Use the Liang-Barsky line clipping algorithm.
	
	// See http://www.skytopia.com/project/articles/compsci/clipping.html

	struct CheckEdge {
		// Returns  true  if we should keep on going,  false  if there is no intersection.
		static bool apply(double p, double q, double& t1, double& t2) {
			bool retVal = true;
			if (p < 0) {
				const double r = q / p;
				if (r > t2) {
					retVal = false;
				}
				else if (r > t1) {
					t1 = r;
				}
			}
			else if (p > 0) {
				const double r = q / p;
				if (r < t1) {
					retVal = false;
				}
				else if (r < t2) {
					t2 = r;
				}
			}
			else if (q < 0) {
				retVal = false;
			}
			return retVal;
		}
	};

	const double x1 = segm.get_p1().get_x();
	const double y1 = segm.get_p1().get_y();
	const double x2 = segm.get_p2().get_x();
	const double y2 = segm.get_p2().get_y();
	const double dx = x2 - x1;
	double t1 = 0;
	double t2 = 1;	

	// Check left edge
	if (CheckEdge::apply(-dx, x1 - x_, t1, t2)) {

		// Check right edge
		if (CheckEdge::apply(dx, x_ + width_ - x1, t1, t2)) {
		
			// Check bottom edge
			const double dy = y2 - y1;
			if (CheckEdge::apply(-dy, y1 - y_, t1, t2)) {
			
				// Check top edge
				if (CheckEdge::apply(dy, y_ + height_ - y1, t1, t2)) {
					
					// The segment intersects this rectangle.
					if (points != nullptr) {
						points->clear();
						if (t1 > 0) {
							points->push_back(PointType(x1 + t1 * dx, y1 + t1 * dy));
						}
						else {
							//  t1  is zero. This means that P1 is either inside this rectangle, or on the edge.
							//  We only consider it an intersection point in the latter case.
							if ((x1 == x_) || (x1 == x_ + width_) || (y1 == y_) || (y1 == y_ + height_)) {
								points->push_back(PointType(x1, y1));
							}
						}
						if (t2 < 1) {
							points->push_back(PointType(x1 + t2 * dx, y1 + t2 * dy));
						}
						else {
							//  t2  is one. This means that P2 is either inside this rectangle, or on the edge.
							//  We only consider it an intersection point in the latter case.
							if ((x2 == x_) || (x2 == x_ + width_) || (y2 == y_) || (y2 == y_ + height_)) {
								points->push_back(PointType(x2, y2));
							}
						}
					}
					retVal = true;
				}
			}
		}
	}

	return retVal;
}


template<class Coord, class CompPolicy> 
bool Rect<Coord, CompPolicy>::intersects(const Rect& rect) const
{
	return greater(rect.x_ + rect.width_, x_) &&
		   greater(x_ + width_, rect.x_) &&
           greater(rect.y_ + rect.height_, y_) &&
           greater(y_ + height_, rect.y_);
}


template<class Coord, class CompPolicy> 
typename Rect<Coord, CompPolicy>::PointType 
Rect<Coord, CompPolicy>::intersect_centre_line(PointType line_point) const
{
	PointType intr;

	const PointType centre(get_centre());
	if (centre.get_x() != line_point.get_x()) {
        // The two points do not have the same X, so we can compute the line slope
        const double m = (line_point.get_y() - centre.get_y()) / (line_point.get_x() - centre.get_x());    	
    	PointType intr_left;
    	PointType intr_right;
        intersect_centre_line(m, intr_left, intr_right);

        if (greater(centre.get_x(), line_point.get_x())) {
        	intr = intr_left;
        }
        else {
         	intr = intr_right;
        }
   	}
    else {
    	// The two points have the same X. Calculating the intersection point is trivial.
        intr.set_x(centre.get_x());

        if (greater(centre.get_y(), line_point.get_y())) {
            intr.set_y(centre.get_y() - height_ / 2);
        }
        else {
            intr.set_y(centre.get_y() + height_ / 2);
        }
    }
    
    return intr;
}


template<class Coord, class CompPolicy> 
void Rect<Coord, CompPolicy>::intersect_centre_line(double m, PointType& intr_left, PointType& intr_right) const
{
	if ((width_ != 0) && (height_ != 0)) {
		if (abs(m) > static_cast<double>(height_) / width_) {
			// The line intersects the top and bottom sides of the rectangle.
			const Coord centreX = get_centre_x();
			const double dx = height_ / (m * 2);
			intr_left.set(centreX + dx, y_ + height_);
			intr_right.set(centreX - dx, y_);
		}
		else {
			// The line intersects the left and right sides of the rectangle.
			const Coord centreY = get_centre_y();
			const double dy = m * width_ / 2;
			intr_left.set(x_, centreY - dy);
			intr_right.set(x_ + width_, centreY + dy);
		}

		if (greater(intr_left.get_x(), intr_right.get_x())) {
			// The "left" point is actually to the right. Swap points.
			std::swap(intr_left, intr_right);
		}
	}
	else {
		intr_left = get_centre();
		intr_right = intr_left;
	}
}

// --- Ellipse class ---

template<class Coord, class CompPolicy> 
inline Ellipse<Coord, CompPolicy>::Ellipse() : 
	x_(0), y_(0), width_(0), height_(0) 
{
}

	
template<class Coord, class CompPolicy> 
inline Ellipse<Coord, CompPolicy>::Ellipse(Coord x, Coord y, Coord width, Coord height) : 
	x_(x), y_(y), width_(width), height_(height) 
{
}

	
template<class Coord, class CompPolicy> 
inline Ellipse<Coord, CompPolicy>::Ellipse(const Ellipse& orig) : 
		x_(orig.x_), y_(orig.y_), width_(orig.width_), height_(orig.height_) 
{
}

	
template<class Coord, class CompPolicy> 
inline void Ellipse<Coord, CompPolicy>::operator=(const Ellipse& rhs) 
{
	set(rhs.x_, rhs.y_, rhs.width_, rhs.height_);
} 

	
template<class Coord, class CompPolicy> 
inline bool Ellipse<Coord, CompPolicy>::operator==(const Ellipse& rhs) const
{
	return equal(x_, rhs.x_) && equal(y_, rhs.y_) && equal(width_, rhs.width_) && equal(height_, rhs.height_);
} 

	
template<class Coord, class CompPolicy> 
inline bool Ellipse<Coord, CompPolicy>::operator!=(const Ellipse& rhs) const 
{
	return !equal(x_, rhs.x_) || !equal(y_, rhs.y_) || !equal(width_, rhs.width_) || !equal(height_, rhs.height_);
} 
	

template<class Coord, class CompPolicy> 
inline Coord Ellipse<Coord, CompPolicy>::get_x() const 
{
	return x_;
}

	
template<class Coord, class CompPolicy> 
inline void Ellipse<Coord, CompPolicy>::set_x(Coord x) 
{
	x_ = x;
}

	
template<class Coord, class CompPolicy> 
inline Coord Ellipse<Coord, CompPolicy>::get_y() const 
{
	return y_;
}

	
template<class Coord, class CompPolicy> 
inline void Ellipse<Coord, CompPolicy>::set_y(Coord y) 
{
	y_ = y;
}


template <typename Coord, typename CompPolicy>
inline typename Ellipse<Coord, CompPolicy>::PointType Ellipse<Coord, CompPolicy>::get_location() const
{
	return PointType(x_, y_);	
}

	
template <typename Coord, typename CompPolicy>
inline void Ellipse<Coord, CompPolicy>::set_location(Coord x, Coord y)
{
	x_ = x;
	y_ = y;
}
	
	
template <typename Coord, typename CompPolicy>
inline void Ellipse<Coord, CompPolicy>::set_location(const PointType& loc)
{
	x_ = loc.get_x();
	y_ = loc.get_y();
}
	

template<class Coord, class CompPolicy> 
inline void Ellipse<Coord, CompPolicy>::move(const GVector<Coord>& displacement) 
{
	x_ += displacement.get_x();
	y_ += displacement.get_y();
}


template<class Coord, class CompPolicy> 
inline auto Ellipse<Coord, CompPolicy>::width() const -> Coord 
{
	return width_;
}

template<class Coord, class CompPolicy> 
inline Coord Ellipse<Coord, CompPolicy>::get_width() const 
{
	return width_;
}
	
template<class Coord, class CompPolicy> 
inline void Ellipse<Coord, CompPolicy>::set_width(Coord width) 
{
	width_ = width;
}
	
template<class Coord, class CompPolicy> 
inline auto Ellipse<Coord, CompPolicy>::height() const -> Coord 
{
	return height_;
}
	
template<class Coord, class CompPolicy> 
inline Coord Ellipse<Coord, CompPolicy>::get_height() const 
{
	return height_;
}
	
template<class Coord, class CompPolicy> 
inline void Ellipse<Coord, CompPolicy>::set_height(Coord height) 
{
	height_ = height;
}

	
template<class Coord, class CompPolicy> 
inline void Ellipse<Coord, CompPolicy>::set(Coord x, Coord y, Coord width, Coord height) 
{
	x_ = x;
	y_ = y;
	width_ = width;
	height_ = height;
}
	
	
template<class Coord, class CompPolicy> 
inline Coord Ellipse<Coord, CompPolicy>::get_centre_x() const 
{
	return x_ + get_horz_radius();
}

	
template<class Coord, class CompPolicy> 
inline Coord Ellipse<Coord, CompPolicy>::get_centre_y() const 
{
	return y_ + get_vert_radius();
}
	
template<class Coord, class CompPolicy> 
inline typename Ellipse<Coord, CompPolicy>::PointType Ellipse<Coord, CompPolicy>::get_centre() const 
{
	return PointType(get_centre_x(), get_centre_y());
}
	
template<class Coord, class CompPolicy> 
inline void Ellipse<Coord, CompPolicy>::get_bounding_rect(RectType& rect) const 
{
	rect.set(x_, y_, width_, height_);
}	
	

template<class Coord, class CompPolicy> 
inline Coord Ellipse<Coord, CompPolicy>::get_horz_radius() const 
{
	return width_ / 2;
}


template<class Coord, class CompPolicy> 
inline Coord Ellipse<Coord, CompPolicy>::get_vert_radius() const 
{
	return height_ / 2;
}


template<class Coord, class CompPolicy> 
bool Ellipse<Coord, CompPolicy>::contains(const PointType& point) const 
{
	//  We are checking the point coordinates against
	//
	//  x^2   y^2
	//  --- + --- <= 1   where  a, b  are the ellipse radii
	//  a^2   b^2

	// Translate the point-ellipse system to the centre of the axes (so that we can use the equation above)
	const double x = point.get_x() - get_centre_x();
	const double y = point.get_y() - get_centre_y();

	const double a = get_horz_radius();
	const double b = get_vert_radius();

	return (x * x) / (a * a) + (y * y) / (b * b) <= 1;	
}


template<class Coord, class CompPolicy> 
long Ellipse<Coord, CompPolicy>::intersects_line(const PointType& p1, const PointType& p2, PointType* intr1, 
		PointType* intr2) const
{
	using twist::math::sqr;

	long intrCount = 0;
	
	// We are checking the intersection points of the ellipse (centered on origin) described by
	//
	//  x^2   y^2
	//  --- + --- = 1   where  a, b  are the ellipse radii
	//  a^2   b^2
	//
	// and the line equation.
	
	// Translate the line-ellipse system to the centre of the axes (so that we can use the equation above)
	const double a = get_horz_radius();
	const double b = get_vert_radius();

	const GVector<Coord> diff = PointType(0, 0) - get_centre();

	PointType transP1(p1);
	transP1.move(diff);	
	PointType transP2(p2);
	transP2.move(diff);
	
	if (!equal(transP1.get_x(), transP2.get_x())) {
		// Line is not vertical.
	
		// We are checking the intersection points of the ellipse described by
		//
		//  x^2   y^2
		//  --- + --- = 1   where  a, b  are the ellipse radii
		//  a^2   b^2
		//
		// and the segment support line, described by  y = mx + n	
		
		double m = 0;
		double n = 0;
		get_line_equation(transP1, transP2, m, n);
				
		double d = sqr(a * b) * (sqr(b) - sqr(n) + sqr(a * m));
		if (d >= 0) {
			d = sqrt(d);
			const double p = -a * a * m *n;
			const double q = sqr(b) + sqr(a * m);
			if (d == 0) {
				// Line is tangent.
				intrCount = 1;
				if (intr1 != nullptr) {
					intr1->set_x(p / q);
					intr1->set_y(m * intr1->get_x() + n);
					intr1->move(-diff);  // Transform back.
				}
			}
			else {
				intrCount = 2;
				if (intr1 != nullptr) {
					intr1->set_x((p - d) / q);
					intr1->set_y(m * intr1->get_x() + n);
					intr1->move(-diff);  // Transform back.
				}
				if (intr2 != nullptr) {
					intr2->set_x((p + d) / q);
					intr2->set_y(m * intr2->get_x() + n);
					intr2->move(-diff);  // Transform back.
				}
			}		
		}		
	}
	else {
		// Line is vertical.
		
		// We are checking the intersection points of the ellipse described by
		//
		//  x^2   y^2
		//  --- + --- = 1   where  a, b  are the ellipse radii
		//  a^2   b^2
		//
		// and the segment support line, described by  x = k (a constant)
		
		const Coord k = transP1.get_x();
		
		double term = sqr(b) - sqr(b / a) * sqr(k);
		if (term >= 0) {		
			term = sqrt(term);
			if (term == 0) {
				// Line is tangent.
				intrCount = 1;
				if (intr1 != nullptr) {
					intr1->set(k, term);
					intr1->move(-diff);  // Transform back.
				}
			}
			else {
				intrCount = 2;
				if (intr1 != nullptr) {
					intr1->set(k, -term);
					intr1->move(-diff);  // Transform back.
				}
				if (intr2 != nullptr) {
					intr2->set(k, term);
					intr2->move(-diff);  // Transform back.
				}
			}
		}
	}
	return intrCount;
}


template<class Coord, class CompPolicy> 
bool Ellipse<Coord, CompPolicy>::intersects(const SegmentType& segm, std::vector<PointType>* points) const
{
	bool retVal = false;

	// Get the intersection of the ellipse and the segment supporting line.
	PointType lineIntr1;
	PointType lineIntr2;	
	const long lineIntrCount = intersects_line(segm.get_p1(), segm.get_p2(), &lineIntr1, &lineIntr2);
	if (lineIntrCount == 1) {
		// Support line is tangent. Check whether the intersection point is on the segment.
		if (segm.contains(lineIntr1)) {
			if (points != nullptr) {
				points->clear();
				points->push_back(lineIntr1);
			}
			retVal = true;
		}
	}
	else if (lineIntrCount == 2) {
		// Support line intersects the ellipse at two points. 				
		const SegmentType lineIntrSegm(lineIntr1, lineIntr2);
		const bool p1Inside = lineIntrSegm.contains(segm.get_p1());
		const bool p2Inside = lineIntrSegm.contains(segm.get_p2());
		if (p1Inside && p2Inside) {
			// SegmentType is inside the ellipse.
			if (points != nullptr) {
				// Check whether any of the segment ends coincides with the line intersection points (i.e. is on the edge 
				// of the ellipse). 
				points->clear();
				if ((segm.get_p1() == lineIntr1) || (segm.get_p1() == lineIntr2)) {
					points->push_back(segm.get_p1());
				}
				if ((segm.get_p2() == lineIntr1) || (segm.get_p2() == lineIntr2)) {
					points->push_back(segm.get_p2());
				}
			}
			retVal = true;
		}
		else if ((p1Inside && !p2Inside) || (!p1Inside && p2Inside)) {
			// One of the line intersection points is on the segment.
			if (points != nullptr) {
				points->clear();
				if (segm.contains(lineIntrSegm.get_p1())) {
					points->push_back(lineIntrSegm.get_p1());				
				}
				else if (segm.contains(lineIntrSegm.get_p2())) {
					points->push_back(lineIntrSegm.get_p2());				
				}
			}
			retVal = true;
		}
		else {
			// The segment either intersects the ellipse at two points (line intersection points are contained in the the 
			// segment) or none.
			if (segm.contains(lineIntrSegm.get_p1())) {
				if (points != nullptr) {
					points->clear();
					points->push_back(lineIntrSegm.get_p1());				
					points->push_back(lineIntrSegm.get_p2());				
				}
				retVal = true;
			}
		}
	}
	return retVal;
}


template<class Coord, class CompPolicy> 
bool Ellipse<Coord, CompPolicy>::intersects(const RectType& rect) const
{
	bool retVal = true;
	
	// Check whether any of the rectangle vertices are inside the ellipse.
	const PointType rectTL(rect.get_x(), rect.get_y());
	if (!contains(rectTL)) {
		const PointType rectTR(rect.get_x() + rect.get_width(), rect.get_y());
		if (!contains(rectTR)) {
			const PointType rectBR(rectTR.get_x(), rect.get_y() + rect.get_height());
			if (!contains(rectBR)) {
				const PointType rectBL(rect.get_x(), rectBR.get_y());
				if (!contains(rectBR)) {
					// None of the vertices are inside the ellipse.
					// The intersection is empty if none of the sides of the rect crosses the corresponding ellipse axis.
					// If both sides cross the corresponding axis, the intersection is non-empty.
					const Coord centreX = get_centre_x();
					const Coord centreY = get_centre_y();
					const bool crossHorzAxis = 
							greater_or_equal(centreX, rectTL.get_x()) && greater_or_equal(rectTR.get_x(), centreX);
					const bool crossVertAxis = 
							greater_or_equal(centreY, rectTL.get_y()) && greater_or_equal(rectBL.get_y(), centreY);
					if (crossHorzAxis && !crossVertAxis) {
						// The rectangle crosses the ellipse horizontal axis. The intersection is non-empty if and only if
						// the rectangle reaches between the top and bottom bounds of the ellipse.
						retVal = greater_or_equal(rect.get_y(), y_ - rect.get_height()) &&
								 greater_or_equal(y_ + height_, rect.get_y());
					}
					else if (!crossHorzAxis && crossVertAxis) {
						// The rectangle crosses the ellipse vertical axis. The intersection is non-empty if and only if
						// the rectangle reaches between the left and right bounds of the ellipse.
						retVal = greater_or_equal(rect.get_x(), x_ - rect.get_width()) &&
								 greater_or_equal(x_ + width_, rect.get_x());
					}
					else if (!crossHorzAxis && !crossVertAxis) {
						retVal = false;
					}
				}
			}			
		}		
	}
	
	return retVal;
}


template<class Coord, class CompPolicy> 
typename Ellipse<Coord, CompPolicy>::PointType 
Ellipse<Coord, CompPolicy>::intersect_centre_line(PointType line_point) const
{
	PointType intersection;
	
	const double a = get_horz_radius();
	const double b = get_vert_radius();
	const WorldPoint centre(get_centre_x(), get_centre_y());

	if (centre != line_point) {
		//  We are solving the system of equations below, and choosing the appropriate solution
		//
		//  x^2   y^2
		//  --- + --- = 1   and   y = mx   where  a, b  are the ellipse radii and  m  is the line slope.
		//  a^2   b^2
		
		const double dx = line_point.get_x() - centre.get_x();
		if (dx != 0) {		
			const long sgn = twist::math::sign(dx);
			const double m = (line_point.get_y() - centre.get_y()) / dx;
			const double x = (a * b) / sqrt(a * a * m * m + b * b);
			const double y = m * x;
			intersection.set(
					static_cast<Coord>(sgn * x + centre.get_x()), 
					static_cast<Coord>(sgn * y + centre.get_y()));
		}
		else {
			// The line is vertical. Intersection is trivial.
			if (line_point.get_y() < centre.get_y()) {
				intersection.set(line_point.get_x(), y_); 
			}
			else {
				intersection.set(line_point.get_x(), y_ + height_); 
			}
		}
	}
	
	return intersection;
}

// --- Global functions ---

template<class Pt> 
[[nodiscard]] auto distance(Pt p1, Pt p2) -> double
{
	const auto dx = p2.get_x() - p1.get_x();
	const auto dy = p2.get_y() - p1.get_y();
	return std::hypot(dx, dy);
}

template<class ObjWithBoundingRect>
[[nodiscard]] auto dims(const ObjWithBoundingRect& obj) -> Dimensions<typename ObjWithBoundingRect::CoordType,
                                                                      typename ObjWithBoundingRect::CompPolicyType>
{
    return Dimensions<typename ObjWithBoundingRect::CoordType, 
                      typename ObjWithBoundingRect::CompPolicyType>{obj.width(), obj.height()};
}

template <typename _Point> 
void get_line_equation(const _Point& p1, const _Point& p2, double& m, double& n)
{
    if (_Point::CompPolicyType::equal(p1.get_x(), p2.get_x())) {
    	TWIST_THROW(L"A vertical line's equation cannot be written as  y = mx + n ");
    }

	m = (p2.get_y() - p1.get_y()) / (p2.get_x() - p1.get_x());
	n = (p2.get_x() * p1.get_y() - p1.get_x() * p2.get_y()) / (p2.get_x() - p1.get_x());
}

template<class Coord, class CompPolicy, rg::input_range Rng>
[[nodiscard]] auto calc_bounding_rect(const Rng& points) -> Rect<Coord, CompPolicy>
{
    const auto end_it = rg::end(points);
    auto it = rg::begin(points);
    if (it == end_it) {
        TWIST_THRO2(L"At least two points are needed.");
    }
    const auto pt1 = *it++;
    if (it == end_it) {
        TWIST_THRO2(L"At least two points are needed.");
    }
    const auto pt2 = *it++;
    auto rect = Rect<Coord, CompPolicy>{pt1, pt2};
    while (it != end_it) {
        rect.add(*it++); 
    }    
    return rect;
}

template<rg::input_range Rng>
requires std::same_as<rg::range_value_t<Rng>, WorldPoint>
[[nodiscard]] auto calc_world_bounding_rect(const Rng& points) -> WorldRect
{
    return calc_bounding_rect<WorldPoint::CoordType, WorldPoint::CompPolicyType>(points);
}

}

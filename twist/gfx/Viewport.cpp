///  @file  Viewport.cpp
///  Implementation file for "Viewport.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "Viewport.hpp"

#include "../math/numeric_utils.hpp"

using namespace twist::math;

namespace twist::gfx {

Viewport::Viewport(const ScreenRect& vwport_screen_rect) 
	: vwport_screen_rect_(vwport_screen_rect)
    , vwport_world_rect_(screen_to_world_rect(vwport_screen_rect))   
    , zoom_ratio_(1)
    , std_zoom_ratio_(1)
{
	TWIST_CHECK_INVARIANT
}


Viewport::~Viewport()
{
	TWIST_CHECK_INVARIANT
}


double Viewport::get_zoom_ratio() const
{
	TWIST_CHECK_INVARIANT
	return zoom_ratio_;
}


void Viewport::set_std_zoom()
{
	TWIST_CHECK_INVARIANT
	std_zoom_ratio_ = zoom_ratio_;
	TWIST_CHECK_INVARIANT
}


double Viewport::get_zoom_ratio_rel_to_std() const
{
	TWIST_CHECK_INVARIANT
    return zoom_ratio_ / std_zoom_ratio_;	
}


const ScreenRect& Viewport::get_vwport_screen_rect() const
{
	TWIST_CHECK_INVARIANT
	return vwport_screen_rect_;
}


const WorldRect& Viewport::get_vwport_world_rect() const
{
	TWIST_CHECK_INVARIANT
	return vwport_world_rect_;
}


void Viewport::set_vwport_world_rect(const WorldPoint& vwport_world_origin, double vwport_world_width)
{
	TWIST_CHECK_INVARIANT
	const double newVwportWorldHeight = vwport_world_width * 
			vwport_world_rect_.get_height() / vwport_world_rect_.get_width();
			
	zoom_ratio_ = vwport_world_rect_.get_width() / vwport_world_width * zoom_ratio_;

    vwport_world_rect_.set(
			vwport_world_origin.get_x(), 
			vwport_world_origin.get_y(),
			vwport_world_width,
			newVwportWorldHeight);
}


void Viewport::resize_vwport(long screen_width, long screen_height)
{
	TWIST_CHECK_INVARIANT
	vwport_screen_rect_.set_width(screen_width);
	vwport_screen_rect_.set_height(screen_height);
    vwport_world_rect_.set_width(vwport_dist_to_world(screen_width));
    vwport_world_rect_.set_height(vwport_dist_to_world(screen_height));	
	TWIST_CHECK_INVARIANT
}


bool Viewport::zoom(double recommended_zoom_ratio, const WorldRect& max_world_rect) 
{
	TWIST_CHECK_INVARIANT
	bool retVal = true;
	bool done = false;

    WorldRect normMaxWorldRect = normalise(max_world_rect);

	// First of all, we cannot zoom out if the maximum world rectangle is already COMPLETELY CONTAINED within the
    // current viewport rectangle.
    if (recommended_zoom_ratio < zoom_ratio_) {
    	if (vwport_world_rect_.contains(normMaxWorldRect)) {
        	done = true;
        	retVal = false;
        }
        else if (WorldComp::greater_or_equal(vwport_world_rect_.get_width(), normMaxWorldRect.get_width())) {
        	// The maximum world rectangle is smaller than the current viewport rectangle, but is not completely
            // contained within. We should not zoom out but rather shift the viewport centre so that the max world
            // rectangle becomes contained within.

            WorldPoint newVwportOrigin(vwport_world_rect_.get_x(), vwport_world_rect_.get_y());
            const double newVwportWidth = vwport_world_rect_.get_width();
   			const double newVwportHeight = vwport_world_rect_.get_height();

    		if (WorldComp::greater(newVwportOrigin.get_x(), normMaxWorldRect.get_x())) {
    			newVwportOrigin.set_x(normMaxWorldRect.get_x());
    		}
    		else if (WorldComp::greater(
    					normMaxWorldRect.get_x() + normMaxWorldRect.get_width(), 
    					newVwportOrigin.get_x() + newVwportWidth)) {

    			newVwportOrigin.set_x(normMaxWorldRect.get_x() + normMaxWorldRect.get_width() - newVwportWidth);
            }

    		if (WorldComp::greater(newVwportOrigin.get_y(), normMaxWorldRect.get_y())) {
    			newVwportOrigin.set_y(normMaxWorldRect.get_y());
    		}
    		else if (WorldComp::greater(
    					normMaxWorldRect.get_y() + normMaxWorldRect.get_height(), 
    					newVwportOrigin.get_y() + newVwportHeight)) {
    					
            	newVwportOrigin.set_y(normMaxWorldRect.get_y() + normMaxWorldRect.get_height() - newVwportHeight);
            }

            // All is good. Shift viewport.
			set_vwport_world_rect(newVwportOrigin, newVwportWidth);
            done = true;
        }
    }

    if (!done) {
    	const double vwportRatio = vwport_world_rect_.get_height() / vwport_world_rect_.get_width();

    	// Calculate the new viewport rectangle so that the centre point remains the same and the zoom ratio changes to
   		// the recommended one.
   		const double curCentreWorldX = vwport_world_rect_.get_centre_x();
   		const double curCentreWorldY = vwport_world_rect_.get_centre_y();

   		double newVwportWidth = vwport_world_rect_.get_width() * zoom_ratio_ / recommended_zoom_ratio;
   		double newVwportHeight = newVwportWidth * vwportRatio;
   		WorldPoint newVwportOrigin(curCentreWorldX - newVwportWidth / 2.0, curCentreWorldY - newVwportHeight / 2.0);

   		// If the new viewport width is greater than the width of the maximum world rectangle, shrink the new viewport
   		// rectangle to be the same as the maximum world rectangle.
   		if (WorldComp::greater(newVwportWidth, normMaxWorldRect.get_width())) {
   			newVwportWidth = normMaxWorldRect.get_width();
       		newVwportHeight = newVwportWidth * vwportRatio;
   		}

    	// Check that the new viewport rectangle is COMPLETELY CONTAINED inside the maximum world rectangle (if not,
        // move it so that it is)
    	if (WorldComp::greater(normMaxWorldRect.get_x(), newVwportOrigin.get_x())) {
    		newVwportOrigin.set_x(normMaxWorldRect.get_x());
    	}
    	else if (WorldComp::greater(
    				newVwportOrigin.get_x() + newVwportWidth, 
    				normMaxWorldRect.get_x() + normMaxWorldRect.get_width())) {
    			
    		newVwportOrigin.set_x(normMaxWorldRect.get_x() + normMaxWorldRect.get_width() - newVwportWidth);
    	}

    	if (WorldComp::greater(normMaxWorldRect.get_y(), newVwportOrigin.get_y())) {
    		newVwportOrigin.set_y(normMaxWorldRect.get_y());
    	}
    	else if (WorldComp::greater(
    				newVwportOrigin.get_y() + newVwportHeight, 
    				normMaxWorldRect.get_y() + normMaxWorldRect.get_height())) {
    			
			newVwportOrigin.set_y(normMaxWorldRect.get_y() + normMaxWorldRect.get_height() - newVwportHeight);
    	}

   		// All is good. Zoom.
		set_vwport_world_rect(newVwportOrigin, newVwportWidth);
    }
    
	TWIST_CHECK_INVARIANT
    return retVal;
}


WorldRect Viewport::normalise(const WorldRect& worldRect) const
{
	TWIST_CHECK_INVARIANT
	WorldRect normRect(worldRect);

	// Adjust the rectangle passed in to have the same ratio as the viewport rectangle, but keep its centre
    // point the same!
    double rectWidth  = normRect.get_width();
    double rectHeight = normRect.get_height();

    if ((rectWidth != 0) && (rectHeight != 0)) {

        const double vwportRatio = vwport_world_rect_.get_height() / vwport_world_rect_.get_width();

        const WorldPoint rectCentre = normRect.get_centre();        

        if (WorldComp::greater_or_equal(rectWidth * vwportRatio, rectHeight)) {
            rectHeight = rectWidth * vwportRatio;
        }
        else {
            rectWidth = rectHeight / vwportRatio;
        }

        normRect.set(
				rectCentre.get_x() - rectWidth / 2.0, rectCentre.get_y() - rectHeight / 2.0,
				rectWidth, rectHeight);
    }
    else {
        // The rectangle passed in a special rectangle, with all coordinates zero.
        // This means that there is NOTHING in the graphical world. Return the default rectangle, with origin in zero
        // and same dimensions as the current viewport.
        normRect.set(0, 0, vwport_world_rect_.get_width(), vwport_world_rect_.get_height()); 
    }
        
    return normRect;
}


WorldPoint Viewport::vwport_to_world(const ScreenPoint& vwport_point) const
{
	TWIST_CHECK_INVARIANT
	return WorldPoint(
			vwport_world_rect_.get_x() + vwport_dist_to_world(vwport_point.get_x()),
			vwport_world_rect_.get_y() + vwport_dist_to_world(vwport_point.get_y()));
}


ScreenPoint Viewport::world_to_vwport(const WorldPoint& worldPoint) const
{
	TWIST_CHECK_INVARIANT
    return ScreenPoint(
			round_to_long((worldPoint.get_x() - vwport_world_rect_.get_x()) * zoom_ratio_),
			round_to_long((worldPoint.get_y() - vwport_world_rect_.get_y()) * zoom_ratio_));
}


double Viewport::vwport_dist_to_world(double vwport_dist) const
{
	TWIST_CHECK_INVARIANT
	return vwport_dist / zoom_ratio_;
}


double Viewport::world_dist_to_vwport(double world_dist) const
{
	TWIST_CHECK_INVARIANT
	return world_dist * zoom_ratio_;
}


long Viewport::world_x_to_vwport(double world_x) const
{
	TWIST_CHECK_INVARIANT
    return round_to_long((world_x - vwport_world_rect_.get_x()) * zoom_ratio_);	
}


long Viewport::world_y_to_vwport(double world_y) const
{
	TWIST_CHECK_INVARIANT
    return round_to_long((world_y - vwport_world_rect_.get_y()) * zoom_ratio_);	
}


WorldRect Viewport::vwport_to_world(const ScreenRect& vwport_rect) const
{
	TWIST_CHECK_INVARIANT
	return WorldRect(
			vwport_world_rect_.get_x() + vwport_dist_to_world(vwport_rect.get_x()),
			vwport_world_rect_.get_y() + vwport_dist_to_world(vwport_rect.get_y()),
			vwport_dist_to_world(vwport_rect.get_width()),
			vwport_dist_to_world(vwport_rect.get_height()));
}


ScreenRect Viewport::world_to_vwport(const WorldRect& worldRect) const 
{
	TWIST_CHECK_INVARIANT
    return ScreenRect(
			world_x_to_vwport(worldRect.get_x()), 
			world_y_to_vwport(worldRect.get_y()),
			round_to_long(world_dist_to_vwport(worldRect.get_width())), 
			round_to_long(world_dist_to_vwport(worldRect.get_height())));
}


ScreenEllipse Viewport::world_to_vwport(const WorldEllipse& world_ellipse) const
{
	TWIST_CHECK_INVARIANT
	return ScreenEllipse(	
			world_x_to_vwport(world_ellipse.get_x()), 
			world_y_to_vwport(world_ellipse.get_y()),
			round_to_long(world_dist_to_vwport(world_ellipse.get_width())), 
			round_to_long(world_dist_to_vwport(world_ellipse.get_height())));
}


#ifdef _DEBUG
void Viewport::check_invariant() const noexcept
{
	assert(zoom_ratio_ > 0);
    assert(std_zoom_ratio_ > 0);
}
#endif  

}

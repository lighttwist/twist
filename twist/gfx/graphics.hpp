/// @file graphics.hpp
/// Two-dimensional graphics basics.
///		We refer to two distinct entities: a graphic world, with an unchanging coordinate system, and any number of 
///     screen projections of that world. 
///     //+TODO: How does the Y coordinate work?!
///  ObjWithLocation concept
///  ObjWithBoundingRect concept, refinement of ObjWithLocation 
///	 GVector class
///  Point class, implements the ObjWithLocation concept
///  Segment class
///  Dimensions class
///  Rect class, implements the ObjWithBoundingRect concept
///  Ellipse class, implements the ObjWithBoundingRect concept

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_MATH_GRAPHICS_HPP
#define TWIST_MATH_GRAPHICS_HPP

#include "policies.hpp"

namespace twist::gfx {


// --- Concepts ---

/*

// A graphic object which has a location in 2D space (an X and an Y coordinate).
// +TODO: How does the Y coordinate work?
template<typename Coord, typename CompPolicy>
concept ObjWithLocation { 

	typedef CoordType;
	typedef PointType;

	void  operator= (const ObjWithLocation& rhs);
	bool  operator==(const ObjWithLocation& rhs) const;	
	bool  operator!=(const ObjWithLocation& rhs) const;	

	Coord	get_x() const;
	void	set_x(double x);
	Coord	get_y() const;
	void	set_y(double y);
	Point   get_location() const;
	void    set_location(Coord x, Coord y);
	void    set_location(const Point& loc);

	/// Move the object.
	///
	/// @param[in] displacement  The displacement vector describing the move.
	///
	void move(const GVector<Coord>& displacement);
}

// A graphic object which has a point that is considered the location, and a bounding rectangle.
// Location is always the top left corner of that rectangle.
template<typename Coord, typename CompPolicy>
concept ObjWithBoundingRect : ObjWithLocation { 

	typedef CompPolicy;
	typedef Point;
	typedef Segment;
	typedef Rect;

    [[nodiscard]] auto width() const -> Coord;
	Coord  get_width() const; //+DEPRECATED
	void   set_width(Coord width);
    [[nodiscard]] auto height() const -> Coord;
	Coord  get_height() const; //+DEPRECATED
	void   set_height(Coord height);
	void   set(Coord x, Coord y, Coord width, Coord height);
	Coord  get_centre_x() const;
	Coord  get_centre_y() const;
	Point  get_centre() const;
	void   get_bounding_rect(Rect& rect) const;

	// Check whether a point is inside the object.
	//
	// @param[in] point  The point.
	// @return  true only if the point is inside the object (including the object edge).
	//
	bool contains(const Point& point) const;
	
	// Determine if a line segment intersects the object and if so finds the point(s) of intersection.
	//
	// @param[in] segm  The segment.
	// @param[in] intr1  The first point of intersection (if any). Pass in  nullptr  if not interested.
	// @param[in] intr2  The second point of intersection (if any). Pass in  nullptr  if not interested.
	// @return  The number of intersection points (0, 1, or 2).
	//
	bool intersects(const Segment& segm, std::vector<Points>* points = nullptr) const;

	// Find out if the object intersects a given rectangle.
	//
	// @param[in] rect  The rectangle.
	// @return  true only if they intersect at all (even if they only have one point in common).
	//
	bool intersects(const Rect& rect) const;

    // Get the point (or one of the points) where a line that passes through the centre of the object intersects the 
    // edge of the object. The line is defined by the centre of the object and another point, and the point returned is 
    // the intersection with the object edge that is nearest to the other point.
    //
    // @param[in] line_point  The other point that defines the line
    // @return  The intersection point
    //
    Point intersect_centre_line(const Point& line_point) const;   
}
*/

//! Generic graphic vector class. The template argument is the type of the coordinates.
template<typename Coord>
class GVector {
public:
	using CoordType = Coord;

	GVector();	

	GVector(double x, double y);

	GVector(const GVector& orig);

	void operator=(const GVector& rhs);

	void operator+=(const GVector& rhs);

	GVector operator-() const;
	
	GVector operator+(const GVector& rhs) const;

	void operator*=(double rhs);
	
	GVector operator*(double rhs) const;
	
	Coord x() const;

	Coord get_x() const; //+DEPRECATED

	void set_x(double x);

	Coord y() const; 

	Coord get_y() const; //+DEPRECATED

	void set_y(double y);
	
	void set(double x, double y);

	double length() const; 

	double get_length() const; //+DEPRECATED

private:
	Coord  x_;
	Coord  y_;
};

/*! Generic point class. Implements the ObjWithLocation concept.
    \targ Coord  Coordinate type
    \targ CompPolicy  Coordinate comparison policy
 */
template<
	typename Coord, 
	typename CompPolicy
>
class Point : private CompPolicy {
	using CompPolicy::equal;
public:
	using CoordType = Coord;
	using CompPolicyType = CompPolicy;

	Point();
	
	Point(Coord x, Coord y);
		
	GVector<Coord> operator-(const Point& rhs) const;

	Point operator+(const GVector<Coord>& displacement) const;

	//  ObjWithBoundingRect concept operators
	
	void operator= (const Point& rhs);

	bool operator==(const Point& rhs) const;	

	bool operator!=(const Point& rhs) const;	

	//  ObjWithBoundingRect concept methods
	
	Coord x() const;

	Coord get_x() const; //+DEPRECATED 

	void set_x(Coord x);

	Coord y() const; 

	Coord get_y() const; //+DEPRECATED 

	void set_y(Coord y);

	void set(Coord x, Coord y);

	Point get_location() const;

	void set_location(Coord x, Coord y);

	void set_location(const Point& loc);	

	void move(const GVector<Coord>& displacement);

private:
	Coord  x_;
	Coord  y_;
};

//! Generic line segment.
template<class Coord, class CompPolicy>
class Segment : private CompPolicy {
	using CompPolicy::equal;
	using CompPolicy::greater;
	using CompPolicy::greater_or_equal;

public:
	using CoordType = Coord;
	using PointType = Point<Coord, CompPolicy>;
	using CompPolicyType = CompPolicy;

	Segment();
	
	Segment(const PointType& p1, const PointType& p2);
	
	Segment(const Segment& orig);

	void operator=(const Segment& rhs);

	bool operator==(const Segment& rhs) const;	

	bool operator!=(const Segment& rhs) const;	

	const PointType& get_p1() const;
	
	void set_p1(const PointType& p1);

	const PointType& get_p2() const;
	
	void set_p2(const PointType& p2);
	
	double get_length() const;
	
	bool contains(const PointType& point) const;
		
	/// Find out if this segment intersects another, and where.
	///
	/// @param[in] other  The other segment.
	/// @param[in] intr  The intersection point, if it exists. If the intersection consits of more than one 
	///					point (the segments are colinear and overlap) an arbitrary point from the intersection 
	///					is returned. Pass in nullopt if not interested.
	/// @return   true  only if they intersect.
	///
	bool intersects(const Segment& other, PointType* intr = {});

	/// Move both ends of the segment.
	///
	/// @param[in] displacement  The displacement vector describing the move.
	///
	void move(const GVector<Coord>& displacement);	
	
	/// Get the parameters for the equation of the support line. The line equation considered is  y = mx + n.
	/// If the two segment ends have the same x coordinate, an exception is thrown, as a vertical line s equation cannot 
	/// be written as  y = mx + n
	///
	/// @param[in] m  The  m  parameter of the line equation (the slope).
	/// @param[in] n  The  n  parameter of the line equation (the intercept).
	///
	void get_line_equation(double& m, double& n) const;
	
	/// Get the shortest distance between a point and this segment (it will be either the distance from the point to the 
	/// line supporting the segment, or the distance from the point to one of the ends of the segment).
	///
	/// @param[in] pt  The point.
	/// @param[in] segment_pt  The corresponding point on the segment (either the base of the perpendicular from the point to the 
	///				line supporting the segment, or one of the ends of the segment). Pass in  nullptr  if you do not need this value.
	/// @return  The distance.
	///
	double dist_to_point(const PointType& pt, PointType* segment_pt = nullptr);
	
	/// Get a point along the segment (between the ends).
	///
	/// @param[in] dist_ratio  The ratio, of the total segnemt length, represented by the distance from the first segmend 
	///				end to the desired point. Must be in the interval [0, 1] (0 it the first endpoint, 1 is the secnod).
	///
	PointType get_point_along(double dist_ratio) const;
	
private:
	PointType  p1_;
	PointType  p2_;
};

/*! The dimensions of a 2D graphic object. Specifies width and height. 
    \targ Coord  Coordinate type
    \targ CompPolicy  Coordinate comparison policy
 */
template<class Coord, class CompPolicy>
class Dimensions : private CompPolicy {
	using CompPolicy::equal;

public:
	using CoordType = Coord;

	Dimensions();
	
	Dimensions(Coord width, Coord height);
	
	Dimensions(const Dimensions& orig);
	
	auto operator=(const Dimensions& rhs) -> void;

	auto operator==(const Dimensions& rhs) const -> bool;	

	auto operator!=(const Dimensions& rhs) const -> bool;	

    [[nodiscard]] auto width() const -> Coord;

	Coord get_width() const;
	
	void set_width(Coord width);
	
    [[nodiscard]] auto height() const -> Coord;

	Coord get_height() const;
	
	void set_height(Coord height);
	
	void set(Coord width, Coord height);

private:
	Coord width_;
	Coord height_;
};

/*! Generic rectangle class. The template argument is the type of the coordinates.
    Implements the ObjWithBoundingRect concept.
    Zero-size rectangles are allowed, as are "inside-out" rectangles (the width or height can be negative). 
 */
template<
	typename Coord,
	typename CompPolicy
>
class Rect : private CompPolicy {
	using CompPolicy::equal;
	using CompPolicy::greater;
	using CompPolicy::greater_or_equal;

public:
	using CoordType = Coord;
	using PointType = Point<Coord, CompPolicy>;
	using SegmentType = Segment<Coord, CompPolicy>;
	using CompPolicyType = CompPolicy;
	
	Rect();
	
	Rect(Coord x, Coord y, Coord width, Coord height);

	Rect(PointType pt1, PointType pt2);
	
	Rect(const Rect& orig);

	//  ObjWithBoundingRect  concept operators
	//
	void  operator=(const Rect& rhs);
	bool  operator==(const Rect& rhs) const;	
	bool  operator!=(const Rect& rhs) const;	
	
	// --- ObjWithBoundingRect concept methods ---

    [[nodiscard]] auto x() const -> Coord;
	Coord  get_x() const;
	void    set_x(Coord x);
    [[nodiscard]] auto y() const -> Coord;
	Coord  get_y() const;
	void    set_y(Coord y);
	PointType   get_location() const;
	void    set_location(Coord x, Coord y);
	void    set_location(const PointType& loc);	
	void    move(const GVector<Coord>& displacement);

	// --- ObjWithBoundingRect concept methods ---

    [[nodiscard]] auto width() const -> Coord;
	Coord  get_width() const;
	void    set_width(Coord width);
    [[nodiscard]] auto height() const -> Coord;
	Coord  get_height() const;
	void    set_height(Coord height);
	void    set(Coord x, Coord y, Coord width, Coord height);
	Coord  get_centre_x() const;
	Coord  get_centre_y() const;
	PointType   get_centre() const;
	void    get_bounding_rect(Rect& rect) const;
	bool    contains(const PointType& point) const;
	bool	intersects(const SegmentType& segm, std::vector<PointType>* points = nullptr) const;
	bool	intersects(const Rect& rect) const;                 
    PointType   intersect_centre_line(PointType line_point) const; 
    
	/// For a line passing through the centre of the rectangle, finds the two intersection points of the line with the
	/// rectangle.
	///
	/// @param[in] m  The line s slope.
	/// @param[in] intr_left  The first of the two intersection points. This will always be the point with the smaller (or 
	///				equal) X coordinate.
	/// @param[in] intr_right  The first of the two intersection points. This will always be the point with the bigger (or 
	///				equal) X coordinate.
	///
	void intersect_centre_line(double m, PointType& intr_left, PointType& intr_right) const;

	/// Check whether another rectangle is contained inside this rectangle.
	///
	/// @param[in] rect  The other rectangle.
	/// @return   true  only if the other rectangle is inside this rectangle.
	///
	bool contains(const Rect& rect) const;

	/// Add a point to this rectangle. The resulting rectangle is the smallest rectangle that contains both the original 
	/// rectangle and the specified point. 
	///
	/// @param[in] point  The point.
	///
	void add(const PointType& point);

	/// Add another rectangle to this rectangle. The resulting rectangle is the union of the two rectangle objects, i.e. the
	/// smallest rectangle that contains both. 
	///
	/// @param[in] rect  The other rectangle.
	///
	void add(const Rect& rect);

	/// "Inflate" the rectangle using a given distance. The sides of the rectangle move out from the centre by the distance.
	///
	/// @param[in] dist  The distance.
	///
	void inflate(Coord dist);
	
private:
	Coord x_;
	Coord y_;
	Coord width_;
	Coord height_;
};

/*! Generic ellipse class. The template argument is the type of the coordinates.
    Implements the ObjWithBoundingRect concept.
 */
template<
	typename Coord,
	typename CompPolicy
>
class Ellipse : private CompPolicy {
	using CompPolicy::equal;
	using CompPolicy::greater;
	using CompPolicy::greater_or_equal;

public:
	using CoordType = Coord;
	using PointType = Point<Coord, CompPolicy>;
	using SegmentType = Segment<Coord, CompPolicy>;
	using RectType = Rect<Coord, CompPolicy>;
	using CompPolicyType = CompPolicy;

	Ellipse();
	
	Ellipse(Coord x, Coord y, Coord width, Coord height);
	
	Ellipse(const Ellipse& orig);

	//  ObjWithLocation  concept operators
	//
	void  operator= (const Ellipse& rhs);
	bool  operator==(const Ellipse& rhs) const;	
	bool  operator!=(const Ellipse& rhs) const;	

	// --- ObjWithLocation concept methods ---

	Coord  get_x() const;
	void    set_x(Coord x);
	Coord  get_y() const;
	void    set_y(Coord y);
	PointType   get_location() const;
	void    set_location(Coord x, Coord y);
	void    set_location(const PointType& loc);	
	void    move(const GVector<Coord>& displacement);

	// --- ObjWithBoundingRect concept methods ---

    [[nodiscard]] auto width() const -> Coord;
	Coord  get_width() const;
	void    set_width(Coord width);
    [[nodiscard]] auto height() const -> Coord;
	Coord  get_height() const;
	void    set_height(Coord height);
	void    set(Coord x, Coord y, Coord width, Coord height);
	Coord  get_centre_x() const;
	Coord  get_centre_y() const;
	PointType   get_centre() const;
	void    get_bounding_rect(RectType& rect) const;
	bool    contains(const PointType& point) const;
	bool    intersects(const SegmentType& segm, std::vector<PointType>* points = nullptr) const;
	bool	intersects(const RectType& rect) const;
	PointType   intersect_centre_line(PointType line_point) const;
	
	/// Find the intersection between this ellipse edge and a line.
	///
	/// @param[in] p1, p2  Points defining the line.
	/// @param[in] intr1  The first intersection point (if it exists). Pass in  nullptr  if not interested.
	/// @param[in] intr2  The second intersection point (if it exists). Pass in  nullptr  if not interested.
	/// @return  The number of intersection points (0, 1 or 2).
	///
	long intersects_line(const PointType& p1, const PointType& p2, PointType* intr1 = nullptr, 
			PointType* intr2 = nullptr) const;

	/// Get the horizontal radius of the ellipse.
	///
	/// @return  The radius.
	///
	Coord get_horz_radius() const;

	/// Get the vertical radius of the ellipse.
	///
	/// @return  The radius.
	///
	Coord get_vert_radius() const;

private:
	Coord  x_;
	Coord  y_;
	Coord  width_;
	Coord  height_;
};

// --- Aliases ---

// Tolerance policy for the comparison policy class to be used for the graphics classes.
struct WorldTolerancePolicy {
	static double get_tolerance() {
		return 0.00001;
	}
};

// A vector in the graphic world. 
typedef GVector<double>  WorldGVector;
// A vector on the screen. 
typedef GVector<long>  ScreenGVector;

// A point in the graphic world (and related types). 
typedef Point<double, CompPolicyTolerance<double, WorldTolerancePolicy>>  WorldPoint;
typedef std::vector<WorldPoint>  WorldPointList;

// A point on the screen. 
typedef Point<long, CompPolicyExact<long>>  ScreenPoint;

// A segment in the graphic world. 
typedef Segment<double, CompPolicyTolerance<double, WorldTolerancePolicy>>  WorldSegment;
// A segment on the screen. 
typedef Segment<long, CompPolicyExact<long>>  ScreenSegment;

// Graphic object dimensions in the graphic world. 
using WorldDimensions = Dimensions<double, CompPolicyTolerance<double, WorldTolerancePolicy>>;
// Graphic object dimensions on the screen. 
using ScreenDimensions = Dimensions<long, CompPolicyExact<long>>;

// A rectangle in the graphic world.
typedef Rect<double, CompPolicyTolerance<double, WorldTolerancePolicy>>  WorldRect;
// A rectangle on the screen.
typedef Rect<long, CompPolicyExact<long>> ScreenRect;

// An ellipse in the graphic world.
typedef Ellipse<double, CompPolicyTolerance<double, WorldTolerancePolicy>>  WorldEllipse;
// An ellipse on the screen.
typedef Ellipse<long, CompPolicyExact<long>>  ScreenEllipse;

typedef CompPolicyTolerance<double, WorldTolerancePolicy>  WorldComp;

// --- Global functions ---

//! Get the euclidian distance between points \p p1 and \p p2.
template<class Pt> 
[[nodiscard]] auto distance(Pt p1, Pt p2) -> double;

//! Get the dimensions of graphic object \p obj.
template<class ObjWithBoundingRect>
[[nodiscard]] auto dims(const ObjWithBoundingRect& obj) -> Dimensions<typename ObjWithBoundingRect::CoordType,
                                                                      typename ObjWithBoundingRect::CompPolicyType>;

/// Get the parameters for the equation of a line. The line equation considered is  y = mx + n.
/// If the two points passed in have the same X coordinate, an exception is thrown, as a vertical line s equation cannot 
/// be written as  y = mx + n
///
/// @param[in] p1, p2  Points defining the line.
/// @param[in] m  The  m  parameter of the line equation (the slope).
/// @param[in] n  The  n  parameter of the line equation (the intercept).
///
template <typename _Point> 
void get_line_equation(const _Point& p1, const _Point& p2, double& m, double& n);

/// Cast a screen rectangle to a world rectangle.
///
/// @param[in] screen_rect  The screen rectangle.
/// @return  The world rectangle.
///
WorldRect screen_to_world_rect(const ScreenRect& screen_rect);

/// Cast a world rectangle to a screen rectangle.
///
/// @param[in] world_rect  The world rectangle.
/// @return  The screen rectangle.
///
ScreenRect world_to_screen_rect(const WorldRect& world_rect);

/*! Calculate the bounding rectangle of the set of points in \p points.
    At lest two points are needed.
 */ 
template<class Coord, class CompPolicy, rg::input_range Rng>
[[nodiscard]] auto calc_bounding_rect(const Rng& points) -> Rect<Coord, CompPolicy>;

/*! Calculate the bounding rectangle of the set of points in \p points.
    At lest two points are needed.
 */ 
template<rg::input_range Rng>
requires std::same_as<rg::range_value_t<Rng>, WorldPoint>
[[nodiscard]] auto calc_world_bounding_rect(const Rng& points) -> WorldRect;

/* Get the angle which the line given by \p line_start and \p line_end makes with the positive X axis, in radians.
   The result is in [0, 2*pi] and follows the conventions of the trigonometric circle.
*/
[[nodiscard]] auto get_line_direction(WorldPoint line_start, WorldPoint line_end) -> double;

/*! Find the point at distance \p dist from the given point \p origin, in the direction given by \p angle (radians).
    \p angle follows the conventions of the trigonometric circle.
 */
[[nodiscard]] auto get_point_at_distance(WorldPoint origin, double dist, double angle) -> WorldPoint;

}

#include "twist/gfx/graphics.ipp"

#endif 

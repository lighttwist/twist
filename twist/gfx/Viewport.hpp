///  @file  Viewport.hpp
///  Viewport  class

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_MATH_VIEWPORT_HPP
#define TWIST_MATH_VIEWPORT_HPP

#include "graphics.hpp"

namespace twist::gfx {

///  This class represents the viewport, i.e. the portion of the world currently visible; a rectangular window
///    into the graphical world.
///  It connects the graphical world to its representation on screen.
///  It handles zooming, resizing the viewport, moving the viewport, projecting from screen to world
///     and vice-versa, etc.
class Viewport : public NonCopyable {
public:
    /// Constructor.
    ///
    /// @param[in] vwport_screen_rect  The initial viewport rectangle (in screen coordinates).
    ///
    Viewport(const ScreenRect& vwport_screen_rect);	

	/// Destructor.
	///
	virtual ~Viewport();

    /// Get the current zoom ratio. "1" means "no zoom".
    ///
    /// @return  The zoom ratio.
    ///
    double get_zoom_ratio() const;

    /// Set the "standard" zoom ratio to be equal to the current zoom ratio. The standard zoom ratio is the zoom
    /// ratio which which is assumed to mean 100% zoom by the users of this class).
    ///
    void set_std_zoom();

    /// Get the current zoom ratio *relative* to the "standard" zoom ratio (i.e. the zoom ratio which is assumed to
    /// mean 100% zoom by the users of this class).
    ///
    /// @return  Relative zoom ratio.
    ///
    double get_zoom_ratio_rel_to_std() const;

    /// Get the viewport rectangle in screen coordinates.
    ///
    /// @return  The screen rectangle.
    ///
    const ScreenRect& get_vwport_screen_rect() const;

    /// Get the viewport rectangle in world coordinates.
    ///
    /// @return  The world rectangle.
    ///
    const WorldRect& get_vwport_world_rect() const;

    /// Set the viewport position, relative to the graphical world.
    ///
    /// @param[in] vwport_world_origin  The world point where the viewport origin will be (top left)
    /// @param[in] vwport_world_width  The width (in world coordinates of course) that the viewport will have.
    ///				Note that we do not need to pass in the viewport height, as that can be computed from the
    ///				current viewport width-height ratio, which stays the same.
    ///
    void set_vwport_world_rect(const WorldPoint& vwport_world_origin, double vwport_world_width);

    /// Resize the viewport. The viewport origin remains unchanged. No zoom is taking place.
    ///
    /// @param[in] screen_width  The new viewport width (in screen coordinates).
    /// @param[in] screen_height  The new viewport height (in screen coordinates).
    ///
    void resize_vwport(long screen_width, long screen_height);

    /// Zoom into/out of the world.
    ///
    /// Generally, the new zoom ratio will be the one passed in (the recommended zoom ratio), and the world point
    /// that is currently in the centre of the viewport will stay in the centre. However, there are cases where this
    /// will not be true: we cannot zoom out beyond the maximum world rectangle. Also, the viewport rectangle should
    /// be kept COMPLETELY INSIDE the maximum world rectangle. These constraints will sometimes make this method
    /// choose a different zoom ratio (as close to the recommended one as possible) and/or shift the centre of the
    /// viewport.
    ///
    /// @param[in] recommended_zoom_ratio  The recommended zoom ratio. If it is a smaller value than the current zoom
    /// 	  		ratio, we are zooming out. Otherwise, we are zooming in.
    /// @param[in] max_world_rect  The maximum world rectangle.
    /// @return  true only if a zoom actually takes place.
    ///
    bool zoom(double recommended_zoom_ratio, const WorldRect& max_world_rect);

    /// Normalises a world rectangle, i.e. returns a rectangle that includes the rectangle passed in and has the
    /// same height-to-width ratio as the viewport rectangle, while keeping its centre point unchanged.
    ///
    WorldRect normalise(const WorldRect& world_rect) const;

    /***  Projection methods  ***/

    /// Project a point in the current viewport coordinates to a point in absolute world coordinates.
    ///
    /// @param[in] vwport_point  The viewport point.
    /// @return  The world point.
    ///
    WorldPoint vwport_to_world(const ScreenPoint& vwport_point) const;
    
    /// Project a viewport rectangle into the world.
    ///
    /// @param[in] vwport_rect  The viewport rectangle.
    /// @return  The projected rectangle, in world coordinates.
    ///
    WorldRect vwport_to_world(const ScreenRect& vwport_rect) const; 
    
    /// Project a distance (between two points) in the current viewport coordinates to a distance in absolute world
    /// coordinates.
    ///
    /// @param[in] vwport_dist  The viewport distance.
    /// @return  The world distance.
    ///
    double vwport_dist_to_world(double vwport_dist) const;

    /// Project a point in absolute world coordinates to a point in the current viewport coordinates.
    ///
    /// @param[in] worldPoint  The world point.
    /// @return  The viewport point.
    ///
    ScreenPoint world_to_vwport(const WorldPoint& worldPoint) const;

    /// Project a world rectangle onto the viewport.
    ///
    /// @param[in] world_rect  The world rectangle.
    /// @return  The projected rectangle, in viewport coordinates.
    ///
    ScreenRect world_to_vwport(const WorldRect& world_rect) const; 
    
    /// Project a world ellipse onto the viewport.
    ///
    /// @param[in] world_ellipse  The world ellipse.
    /// @return  The projected ellipse, in viewport coordinates.
    ///
    ScreenEllipse world_to_vwport(const WorldEllipse& world_ellipse) const;

    /// Project a distance (between two points) in absolute world coordinates to a distance in the current viewport
    /// coordinates.
    ///
    /// @param[in] world_dist  The world distance.
    /// @return  The viewport distance.
    ///
    double world_dist_to_vwport(double world_dist) const;

    /// Project a X world coordinate to the viewport.
    ///
    /// @param[in] world_x  The world coordinate.
    /// @return  The projected X coordinate, in viewport coordinates.
    ///
    long world_x_to_vwport(double world_x) const;

    /// Project a Y world coordinate to the viewport.
    ///
    /// @param[in] worldY  The world coordinate.
    /// @return  The projected Y coordinate, in viewport coordinates.
    ///
    long world_y_to_vwport(double worldY) const;

private:	
    // The viewport rectangle in screen coordinates.
    ScreenRect vwport_screen_rect_;

    // The viewport rectangle in world coordinates.
    WorldRect vwport_world_rect_;

    // The current zoom ratio. "1" is the ratio at which the first drawing takes place (before any zooming).
    double zoom_ratio_;

    // The "standard" zoom ratio. This is the zoom ratio which is considered standard by the users of this 
	// class, ie the zoom will be assumed to be 100% when the zoom ratio has this value. The "standard" zoom 
	// ratio has no relevance for the inner workings of this class, it is simply used by the graphic world to 
	// obtain the value of the zoom ratio *relative* to the standard zoom.
    double std_zoom_ratio_;
	
	TWIST_CHECK_INVARIANT_DECL
};

}

#endif  

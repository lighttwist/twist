/// @file graphics.cpp
/// Implementation file for "graphics.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/gfx/graphics.hpp"

#include <numbers>

namespace twist::gfx {

WorldRect screen_to_world_rect(const ScreenRect& screen_rect) 
{
	return WorldRect(screen_rect.get_x(), screen_rect.get_y(), screen_rect.get_width(), screen_rect.get_height());
}

//+TODO: why is this not rounding?
ScreenRect world_to_screen_rect(const WorldRect& world_rect) 
{
	return ScreenRect(
			static_cast<long>(world_rect.get_x()), static_cast<long>(world_rect.get_y()), 
			static_cast<long>(world_rect.get_width()), static_cast<long>(world_rect.get_height()));
}

[[nodiscard]] auto get_line_direction(WorldPoint line_start, WorldPoint line_end) -> double 
{
    using std::numbers::pi;

	if (line_start == line_end) {
        TWIST_THRO2(L"The two points cannot be the same");
    }

    return atan2(line_start.y() - line_end.y(), line_start.x() - line_end.x()) + pi;
}

[[nodiscard]] auto get_point_at_distance(WorldPoint origin, double dist, double angle) -> WorldPoint
{
    return WorldPoint{origin.x() + dist * std::cos(angle), origin.y() + dist * std::sin(angle)};
}

}


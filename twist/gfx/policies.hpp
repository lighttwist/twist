///  @file  policies.hpp
///  Generic policy classes for the "twist::gfx" namespace
///  CompPolicyExact  class		
///  CompPolicyTolerance  class

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_MATH_POLICIES_HPP
#define TWIST_MATH_POLICIES_HPP

namespace twist::gfx {

///
///  Policy for comparing two numbers. This policy implements exact comparison.
///	 Template parameters
///	     _Number  The number type
///
template <typename _Number>
struct CompPolicyExact {

	static bool equal(_Number x, _Number y) {
		return x == y;
	}
	static bool greater(_Number x, _Number y) {
		return x > y;
	}
	static bool greater_or_equal(_Number x, _Number y) {
		return x >= y;
	}
};

///
///  Policy for comparing two numbers. This policy implements comparison using a tolerance.
///	 Template parameters
///	     _Number  The number type.
///		 _TolerancePolicy  Tolerance policy class. Used for retrieving the tolerance value. Must implement one 
///		     method named  get_tolerance()  taking no parameters and returning the tolerance value.
///
template <typename _Number, typename _TolerancePolicy>
class CompPolicyTolerance : public _TolerancePolicy {

	using _TolerancePolicy::get_tolerance;
public:
	static bool equal(_Number x, _Number y) {
		return abs(x - y) <= get_tolerance();
	}
	
	static bool greater(_Number x, _Number y) {
		return x - y > get_tolerance();
	}

	static bool greater_or_equal(_Number x, _Number y) {
		return greater(x, y) || equal(x, y);
	}
};

}

#endif 

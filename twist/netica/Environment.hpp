/// @file Environment.hpp
/// Environment class definition

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#pragma once

namespace netica_c_api {
class environ_ns;
}
namespace twist::netica {
class Net;
}

namespace twist::netica {

/*! A Netica environment.
    Most applications will have only one global environment, which should be passed to any function that requires it.
 */
class Environment {
public:
	/*! Constructor. Use this to construct an environment based on a licence, with full access to Netica's 
	    functionality.
	    \param[in] netica_licence  The Netica licence
	 */
	Environment(const std::wstring& netica_licence);

	/*! Constructor. Use this to construct an environment without a licence, with restricted access to Netica's 
	    functionality.
	 */
	Environment();

	~Environment();

	//! Reads a net from the file with path \p path.
	[[nodiscard]] auto read_net(const fs::path& path) -> Net;

	TWIST_NO_COPY_NO_MOVE(Environment)

private:
	auto init(const char* netica_licence) -> void;

	netica_c_api::environ_ns* env_{nullptr};

	TWIST_CHECK_INVARIANT_DECL
};

}

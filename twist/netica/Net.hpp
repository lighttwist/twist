/// @file Net.hpp
/// Net class definition

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#pragma once

namespace netica_c_api {
class environ_ns;
class net_bn;
class stream_ns;
}
namespace twist::netica {
class BnNode;
}

namespace twist::netica {

//! A Netica Bayesnet.
class Net {
public:
	Net(const Net& src) = delete;  

	Net(Net&& src);
	
	~Net();

	auto operator=(const Net& rhs) -> Net& = delete; 

	auto operator=(Net&& rhs) -> Net&;

	/*! Compiles net for fast belief updating (i.e., junction tree propagation).
	    If the net is an auto-update net then belief updating will be done immediately afterwards, but if it isn't, 
		then updating won't be done until you request a belief. 
	*/
	auto compile() -> void;

	/*! Retracts all findings (i.e., the current case) from all the nodes in net, except "constant" nodes.
	    This includes positive findings (state and real value), negative findings, and likelihood findings. 
	 */
	auto retract_net_findings() -> void;

	//! Returns the node named \p name (matching is case sensitive). If there is no such node, an exception is thrown. 
	[[nodiscard]] auto get_node_named(std::wstring_view name) const -> BnNode;

private:
	Net(netica_c_api::net_bn* net, netica_c_api::stream_ns* stream, netica_c_api::environ_ns* env);

	netica_c_api::net_bn* net_;
	netica_c_api::stream_ns* stream_;
	netica_c_api::environ_ns* env_;

	friend class Environment;

	TWIST_CHECK_INVARIANT_DECL
};

}

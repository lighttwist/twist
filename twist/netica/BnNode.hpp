/// @file BnNode.hpp
/// BnNode class definition

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#pragma once

namespace netica_c_api {
class environ_ns;
class node_bn;
}

namespace twist::netica {

//! The index of a discrete node's state.
enum class BnStateIndex : int {};

//! A node in a Netica Bayesnet.
class BnNode {
public:
	~BnNode();

	//! Get the node name.
	[[nodiscard]] auto get_name() const -> std::wstring;

	/*! Returns the number of states that node can take, or zero if the node is a continuous node that hasn't been 
	    discretised.
	 */
	[[nodiscard]] auto get_number_states() const -> int;

	/*! Returns the index of the state whose name is \p name (matching is case-sensitive), or std::nullopt if there 
	    isn't one. 
	 */
	[[nodiscard]] auto get_state_named(std::wstring_view name) const -> std::optional<BnStateIndex>;

	//! Enter a real number finding with value \p value for the node (which is normally a continuous variable node). 
	auto enter_value(double value) -> void;

	/*! Enters the discrete finding \p state for the node. This means that in the case currently being analysed, the 
	    node is known with certainty to have value \p state.
		\p state must be between 0 and n - 1 inclusive, where n is the node's number of states.
	    If the node could already have a finding that you wish to override with this new finding, retract_findings() 
		should be called first, otherwise an "inconsistent findings" error could result.
	 */
	auto enter_finding(BnStateIndex state) -> void;

	/*! Retracts all findings from the node.
	    This includes positive findings (state and real value), negative findings, and likelihood findings. It removes 
		them from any kind of node, including "constant" nodes. 
	 */
	auto retract_findings() -> void;

	/*! Returns a belief vector indicating the current probability for each state of the node.
	    The node should be a discrete or discretized nature node. 
        The vector will be indexed by states, with one probability for each state (if required, the state indexes can 
		be found from their names using get_state_named()). It will be normalized, so that the sum of its entries is 1.
		This provides the current beliefs (i.e., posterior probabilities) that the variable represented by node is in 
		each of its states, given the net model and all findings entered into all nodes of the net (positive findings, 
		negative findings and likelihood findings).
        The net containing node must have been compiled before calling this, or an error will be generated. 
		If the net has been modified it must be recompiled, but just entering findings does not require a recompile.
	 */
	[[nodiscard]] auto get_beliefs() const -> std::vector<float>;

	TWIST_NO_COPY_NO_MOVE(BnNode)

private:
	BnNode(netica_c_api::node_bn* node, netica_c_api::environ_ns* env);

	netica_c_api::node_bn* node_;
	netica_c_api::environ_ns* env_;

	friend class Net;

	TWIST_CHECK_INVARIANT_DECL
};

// --- Free functions ---

/*! Enters the discrete finding \p state_name for the node. This means that in the case currently being analysed, the 
	node is known with certainty to have the value of the state with name \p state_name.
	\p state_name must match the name of one of the node's states.
	If the node could already have a finding that you wish to override with this new finding, retract_findings() 
	should be called first, otherwise an "inconsistent findings" error could result.
 */
auto enter_finding(BnNode& node, std::wstring_view state_name) -> void;

}

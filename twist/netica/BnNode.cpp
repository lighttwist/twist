/// @file BnNode.cpp
/// Implementation file for "BnNode.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/netica/pch.hpp"

#include "twist/netica/BnNode.hpp"

#include "twist/netica/internal_utils.hpp"
#include "twist/netica/netica_c_api.hpp"

using namespace netica_c_api;

namespace twist::netica {

BnNode::BnNode(netica_c_api::node_bn* node, netica_c_api::environ_ns* env)
	: node_{node}
	, env_{env}
{
	TWIST_CHECK_INVARIANT
}

BnNode::~BnNode() = default;

auto BnNode::get_name() const -> std::wstring
{
	TWIST_CHECK_INVARIANT
	return ansi_to_string(GetNodeName_bn(node_));
}

auto BnNode::get_number_states() const -> int
{
	TWIST_CHECK_INVARIANT
	return GetNodeNumberStates_bn(node_);
}

auto BnNode::get_state_named(std::wstring_view name) const -> std::optional<BnStateIndex>
{
	auto state_idx = GetStateNamed_bn(string_to_ansi(name).c_str(), node_);
	return state_idx != UNDEF_STATE ? std::optional{BnStateIndex{state_idx}} : std::nullopt;
}

auto BnNode::enter_value(double value) -> void
{
	TWIST_CHECK_INVARIANT
	EnterNodeValue_bn(node_, value);
	check_last_error(env_);
}

auto BnNode::enter_finding(BnStateIndex state) -> void
{
	TWIST_CHECK_INVARIANT
	EnterFinding_bn(node_, static_cast<int>(state));
	check_last_error(env_);
}

auto BnNode::retract_findings() -> void
{
	TWIST_CHECK_INVARIANT
	RetractNodeFindings_bn(node_);
}

auto BnNode::get_beliefs() const -> std::vector<float>
{
	TWIST_CHECK_INVARIANT	
	static_assert(std::is_same_v<float, prob_bn>);
	const auto* prob_buffer = GetNodeBeliefs_bn(node_);

	const auto nof_states = get_number_states();
	auto ret = std::vector<float>(nof_states);
	std::copy(prob_buffer, prob_buffer + nof_states, ret.data());
	
	return ret;
}

#ifdef _DEBUG
auto BnNode::check_invariant() const noexcept -> void
{
	assert(node_);
	assert(env_);
}
#endif

// --- Free functions ---

auto enter_finding(BnNode& node, std::wstring_view state_name) -> void
{
	const auto state_idx = node.get_state_named(state_name);
	if (!state_idx) {
		TWIST_THRO2(L"Node \"{}\" does not contain a state named \"{}\".", node.get_name(), std::wstring{state_name});
	}
	node.enter_finding(*state_idx);
}

}

/// @file Environment.cpp
/// Implementation file for "Environment.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/netica/pch.hpp"

#include "twist/netica/Environment.hpp"

#include "twist/netica/internal_utils.hpp"
#include "twist/netica/Net.hpp"
#include "twist/netica/netica_c_api.hpp"

using namespace netica_c_api;

namespace twist::netica {

Environment::Environment(const std::wstring& netica_licence)
{
	init(string_to_ansi(netica_licence).c_str());
	TWIST_CHECK_INVARIANT
}

Environment::Environment()
{
	init(nullptr);
	TWIST_CHECK_INVARIANT
}

auto Environment::read_net(const fs::path& path) -> Net
{
	TWIST_CHECK_INVARIANT
	auto* stream = NewFileStream_ns(path.string().c_str(), env_, nullptr);
	auto* net = ReadNet_bn(stream, NO_VISUAL_INFO); 
	check_last_error(env_);

	CompileNet_bn(net);
	SetNetAutoUpdate_bn(net, 0);

	return Net{net, stream, env_};
}

Environment::~Environment()
{
	TWIST_CHECK_INVARIANT
	auto message = std::vector<char>(MESG_LEN_ns);
	[[maybe_unused]] auto result = CloseNetica_bn(env_, message.data());
	assert(result >= 0);
}

auto Environment::init(const char* netica_licence) -> void
{
	env_ = NewNeticaEnviron_ns(netica_licence, nullptr, nullptr);
	auto message = std::vector<char>(MESG_LEN_ns);
	if (InitNetica2_bn(env_, message.data()) < 0)  {
		TWIST_THROW(L"Unable to create Netica environment: %s", ansi_to_string(message.data()).c_str());
	}
}

#ifdef _DEBUG
auto Environment::check_invariant() const noexcept -> void
{
	assert(env_);
}
#endif

}

/// @file Net.cpp
/// Implementation file for "Net.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/netica/pch.hpp"

#include "twist/netica/Net.hpp"

#include "twist/netica/BnNode.hpp"
#include "twist/netica/internal_utils.hpp"
#include "twist/netica/netica_c_api.hpp"

using namespace netica_c_api;

namespace twist::netica {

Net::Net(netica_c_api::net_bn* net, netica_c_api::stream_ns* stream, netica_c_api::environ_ns* env)
	: net_{net}
	, stream_{stream}
	, env_{env}
{
	TWIST_CHECK_INVARIANT
}

Net::Net(Net&& src)
	: net_{src.net_}
	, stream_{src.stream_}
	, env_{src.env_}
{
	src.net_ = nullptr;
	src.stream_ = nullptr;
	src.env_ = nullptr;	
	TWIST_CHECK_INVARIANT
}

Net::~Net()
{
	if (net_) {
		DeleteNet_bn(net_);
		assert(stream_);
		DeleteStream_ns(stream_);
	}
}

auto Net::operator=(Net&& rhs) -> Net& 
{
	TWIST_CHECK_INVARIANT
	net_ = rhs.net_;
	stream_ = rhs.stream_;
	env_ = rhs.env_;
	rhs.net_ = nullptr;
	rhs.stream_ = nullptr;
	rhs.env_ = nullptr;	
	return *this;
}

auto Net::compile() -> void
{
	TWIST_CHECK_INVARIANT
	CompileNet_bn(net_);
}

auto Net::retract_net_findings() -> void
{
	TWIST_CHECK_INVARIANT
	RetractNetFindings_bn(net_);
}

auto Net::get_node_named(std::wstring_view name) const -> BnNode
{
	TWIST_CHECK_INVARIANT
	if (auto* node = GetNodeNamed_bn(string_to_ansi(name).c_str(), net_)) {
		return BnNode{node, env_};
	}
	TWIST_THROW(L"Node with name \"%s\" not found in the Bayesnet.", std::wstring{name}.c_str());
}

#ifdef _DEBUG
auto Net::check_invariant() const noexcept -> void
{
	assert(net_);
	assert(stream_);
	assert(env_);
}
#endif

}

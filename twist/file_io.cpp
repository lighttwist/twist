/// @file file_io.cpp
/// Implementation file for "file_io.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "file_io.hpp"

#include <fstream>
#include <regex>

#if TWIST_OS_WIN
  #include <windows.h>
  #include "os_utils.hpp"
#elif TWIST_OS_LINUX
  #include <stdio.h>
  #include <stdlib.h>
  #include <unistd.h>
  #include <linux/limits.h>
  #include <sys/types.h>
  #include <sys/stat.h>
#endif

namespace twist {

//
//  Local functions
//

void append_to_path(fs::path& p, fs::path::iterator first, fs::path::iterator last)
{ 		
    for (; first != last ; ++first) {
        p /= *first;
	}
}


template<class Pred>
std::vector<fs::path> find_matching_filenames_in_dir_impl(const fs::path& dir_path, 
		std::wstring_view pattern, Pred is_match, bool recursive, bool case_sens)
{
	auto matching_paths = std::vector<fs::path>{};

	auto last = fs::directory_iterator{}; // Default ctor yields past-the-end
	for (auto it = fs::directory_iterator{dir_path}; it != last; ++it) {
		if (recursive && is_directory(*it)) {				
			const auto matching_paths_in_subdir = find_matching_filenames_in_dir_impl(
					*it, pattern, is_match, true/*recursive*/, case_sens);
			matching_paths.insert(
					end(matching_paths), begin(matching_paths_in_subdir), end(matching_paths_in_subdir));
		}
		// Skip if not a file
		if (!fs::is_regular_file(it->status())) {
			continue;
		}
		// Skip if no match
		if (!is_match(it->path().filename(), pattern, case_sens)) {
			continue;
		}
		// File matches, store its path
		matching_paths.push_back(it->path());
	}

	return matching_paths;
}

// --- Global functions ---

auto to_path(std::wstring_view path) -> fs::path
{
	return fs::path{path};
}

auto remove_trailing_separator(const fs::path& path) -> fs::path
{
	if (!path.empty()) { 
		auto path_str = path.wstring();
		if (path_str.back() == k_path_sep) {
			return path_str.substr(0, path_str.size() - 1);
		}
	}
	return path;
}

bool is_wcard_match(const fs::path& path, std::wstring_view pattern, bool case_sens)
{
    // Escape all regex special chars
    auto prepared_wcard_pattern = escape_regex(std::wstring{pattern});

    // Convert chars '*?' back to their regex equivalents
    prepared_wcard_pattern = replace_all_substr<wchar_t>(prepared_wcard_pattern, L"\\?", L".");
    prepared_wcard_pattern = replace_all_substr<wchar_t>(prepared_wcard_pattern, L"\\*", L".*");

    auto regex_pattern = case_sens 
			? std::wregex(prepared_wcard_pattern) 
			: std::wregex(prepared_wcard_pattern, std::regex::icase);

    return std::regex_match(path.wstring(), regex_pattern);
}

bool is_regex_match(const fs::path& path, std::wstring_view pattern, bool case_sens)
{
    auto regex_pattern = case_sens ? std::wregex{begin(pattern), end(pattern)} 
			                       : std::wregex{begin(pattern), end(pattern), std::regex::icase};
    return std::regex_match(path.wstring(), regex_pattern);
}

auto find_wcard_matching_filenames_in_dir(const fs::path& dir_path, 
                                          std::wstring_view pattern, 
										  bool recursive, 
                                          bool case_sens) -> std::vector<fs::path>
{
	return find_matching_filenames_in_dir_impl(dir_path, pattern, is_wcard_match, recursive, case_sens);
}

auto find_regex_matching_filenames_in_dir(const fs::path& dir_path, 
                                          std::wstring_view pattern, 
										  bool recursive, 
                                          bool case_sens) -> std::vector<fs::path>
{
	return find_matching_filenames_in_dir_impl(dir_path, pattern, is_regex_match, recursive, case_sens);
}

auto get_subdirectories(const fs::path& dir_path, bool recursive) -> std::vector<fs::path>
{
	std::vector<fs::path> subdirs;

	fs::directory_iterator last;  // Default ctor yields past-the-end
	for (fs::directory_iterator it{dir_path}; it != last; ++it) {
		if (recursive && is_directory(*it)) {
			const auto dirs_in_subdir = get_subdirectories(*it, true/*recursive*/);
			subdirs.insert(
					end(subdirs), begin(dirs_in_subdir), end(dirs_in_subdir));
		}
		if (is_directory(*it)) {
			subdirs.push_back(it->path());
		}
	}

	return subdirs;
}

auto find_regex_matching_subdirs_in_dir(const fs::path& dir_path, std::wstring_view pattern, bool case_sens) 
      -> std::vector<fs::path>
{
	const auto dir_entries = rg::subrange{fs::directory_iterator{dir_path}, fs::directory_iterator{}};

	return dir_entries | vw::filter([pattern, case_sens](const auto& entry) { 
									       return is_directory(entry) && is_regex_match(entry.path().filename(), 
										                                                pattern, 
																				        case_sens); }) 
	                   | vw::transform([dir_path](const auto& entry) { return dir_path / entry.path(); })
	                   | rg::to<std::vector>(); 
}

fs::path get_common_dir(const fs::path& path1, const fs::path& path2)
{
   fs::path path_a(fs::absolute(path1));
   fs::path path_b(fs::absolute(path2));

   // Parse both paths into vectors of tokens. I call them "dir" because they'll be the common directories 
   // unless both paths are the exact same file. I also remove the "." and ".." paths as part of the loops

   std::vector<fs::path> dirs_a;
   std::vector<fs::path> dirs_b;
   for (auto it = path_a.begin(); it != path_a.end(); ++it) {
       const auto token = it->string();
       if (token.compare("..") == 0) {  // Go up 1 level => Pop vector
          dirs_a.pop_back();
       }
       else if (token.compare(".") != 0) {  // "." means "this dir" => ignore it
          dirs_a.push_back(*it);
       }
   }
   for (auto it = path_b.begin(); it != path_b.end(); ++it) {
       const auto token = it->string();
       if (token.compare("..") == 0) {  // Go up 1 level => Pop vector
          dirs_b.pop_back();
       }
       else if (token.compare(".") != 0) {  // "." means "this dir" => ignore it
          dirs_b.push_back(*it);
       }
   }

   // Determine how far to check in each directory set
   const auto common_depth = std::min(dirs_a.size(), dirs_b.size());
   if (!common_depth) {
       // They don't even share a common root- no way from A to B
       return {};
   }

   // Match entries in the 2 vectors until we see a divergence
   std::vector<fs::path> common_dirs_in_order;
   for (size_t i = 0; i< common_depth; ++i) {
      if (dirs_a[i].string().compare(dirs_b[i].string()) != 0) {  // Diverged
         break;
      }
      common_dirs_in_order.push_back(dirs_a[i]);  // I could use dirs_b too.
   }

   fs::path common_dir;
   for_each(common_dirs_in_order, [&](const auto& p) {
		common_dir /= p;
   });

   return common_dir;
}

auto delete_dir_contents(const fs::path& dir_path, bool throw_if_no_dir) -> void 
{
	if (!throw_if_no_dir && !exists(dir_path)) {
		return;
	}
    for (const auto& subdir_path : fs::directory_iterator{dir_path}) {
        remove_all(subdir_path);
    }
}

auto get_depth(const fs::path& path) -> int
{
	return static_cast<unsigned int>(std::distance(path.begin(), path.end()));
}

auto make_relative(const fs::path& dir_path_from, const fs::path& path_to) -> fs::path
{
	const auto abs_dir_path_from = fs::absolute(remove_trailing_separator(dir_path_from)); 
	const auto abs_path_to = fs::absolute(path_to);

	if (abs_dir_path_from.has_root_name() != abs_path_to.has_root_name() ||
			abs_dir_path_from.root_name() != abs_path_to.root_name()) {
		TWIST_THROW(L"The paths have different root names \"%s\" and \"%s\", which makes relativity impossible.",
				abs_dir_path_from.root_name().c_str(), abs_path_to.root_name().c_str());
	}

    fs::path ret;
    fs::path::const_iterator iter_from{abs_dir_path_from.begin()};
	fs::path::const_iterator iter_to{abs_path_to.begin()};

    // Find common base
    for (auto to_end{abs_path_to.end()}, from_end{abs_dir_path_from.end()}; 
			iter_from != from_end && iter_to != to_end && *iter_from == *iter_to; ++iter_from, ++iter_to);

    // Navigate backwards in directory to reach previously found base
    for (auto from_end{abs_dir_path_from.end()}; iter_from != from_end; ++iter_from) {
        if (*iter_from != ".") {
            ret /= "..";
		}
    }

    // Now navigate down the directory branch
    append_to_path(ret, iter_to, abs_path_to.end());

    return ret;
}

auto is_wspace_path(const fs::path& path) -> bool
{
	return is_whitespace(path.wstring());
}

auto append_extension(const fs::path& path, const fs::path& ext) -> fs::path
{
    const auto ext_str = ext.wstring();
	auto ext_sz = ext_str.c_str();
    if (L'.' == *ext_sz) {
		++ext_sz;
	}
    return path.wstring() + L"." + ext_sz;
}

auto change_extension(const fs::path& path, const fs::path& ext) -> fs::path 
{
	if (const auto old_ext = path.extension(); !old_ext.empty()) {
		auto path_str = path.wstring();
		return path_str.substr(0, path_str.size() - old_ext.wstring().size()) + ext.wstring();
	}
	return (path.wstring() + ext.wstring());  //+TODO: how does this work on linux?
}

auto get_unused_path(const fs::path& init_path) -> fs::path
{
	if (!exists(init_path)) {
		return init_path;
	}

	const auto parent_dir_path = init_path.parent_path();
	const auto stem = init_path.stem().wstring();
	const auto ext = init_path.extension().wstring();
	auto counter = 2;
	auto path = fs::path{};
	do {
		path = parent_dir_path / (stem + L"-" + std::to_wstring(counter++) + ext); 
	}
	while (exists(path));

	return path;
}

auto file_ssize(const fs::path& path) -> std::int64_t
{
	const auto size = file_size(path);
	if (size > static_cast<std::uintmax_t>(std::numeric_limits<std::int64_t>::max())) {
		TWIST_THRO2(L"Wow, the file \"{}\" exceeds {:.3e} bytes. This function's return value would be wrong.",
		            path.filename().c_str(), static_cast<double>(std::numeric_limits<std::int64_t>::max()));
	}
	return static_cast<std::int64_t>(size);
}

auto is_inside_dir(const fs::path& dir_path, const fs::path& path) -> bool
{
	if (dir_path.is_absolute() != path.is_absolute()) {
		TWIST_THRO2(L"The paths must either be both absolute or both relative: \"{}\" and \"{}\"",
		            dir_path.c_str(), path.c_str());
	}

	const auto dir_path_norm = dir_path.lexically_normal();
	const auto path_norm = path.lexically_normal();

	const auto [mismatch_start, _] = rg::mismatch(dir_path_norm, path_norm);	
	if (mismatch_start == dir_path_norm.end()) {
		return true;
	}
	if (next(mismatch_start) == dir_path_norm.end() && mismatch_start->empty()) {
		// If the last character in a path is the directory separator, the last element while iterating the path 
		// is empty
		return true;
	} 
	return false;
}

}

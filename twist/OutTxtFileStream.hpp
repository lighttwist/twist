///  @file  OutTxtFileStream.hpp
///  OutTxtFileStream class template, inherits OutFileStream<> 
	
//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_OUT_TXT_FILE_STREAM_HPP
#define TWIST_OUT_TXT_FILE_STREAM_HPP

#include "OutFileStream.hpp"

namespace twist {

///  An extension of the OutFileStream class for streaming to a text file in an efficient manner.
///
/// @tparam   Chr  The character type in the output text file; only the char and wchar_t types are currenty 
///				supported
///
template<typename Chr>
class OutTxtFileStream : public OutFileStream<Chr> {
public:
	/// Constructor.
	///
	/// @param[in] out_txt_file  The output text file. Must be open in write mode. The streaming will start from the 
	///				current position of its file pointer.  
	///			
	OutTxtFileStream(std::unique_ptr<FileStd> out_txt_file);

	/// Destructor.
	///
	virtual ~OutTxtFileStream();

	/// Streaming operator.
	///
	/// @param[in] chr  A single ANSI character value to be streamed into the file.
	/// @return  A reference to this object (for chaining operator calls).
	///
	OutTxtFileStream& operator<<(Chr chr);
		
	/// Streaming operator.
	///
	/// @param[in] str  ANSI string to be streamed into the file.
	/// @return  A reference to this object (for chaining operator calls).
	///
	OutTxtFileStream& operator<<(const Chr* str);
	
	/// Streaming operator.
	///
	/// @param[in] str  String to be streamed into the file. 
	/// @return  A reference to this object (for chaining operator calls).
	///
	OutTxtFileStream& operator<<(std::basic_string_view<Chr> str);

	TWIST_NO_COPY_NO_MOVE(OutTxtFileStream<Chr>)

private:	
	TWIST_CHECK_INVARIANT_DECL
};

} 

#include "OutTxtFileStream.ipp"

#endif 


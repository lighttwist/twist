///  @file  InFileStream.hpp
///  InFileStream class template definition

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_IN_FILE_STREAM_HPP
#define TWIST_IN_FILE_STREAM_HPP

#include "File.hpp"

namespace twist {

///  Class to be used for streaming values in from a file in an efficient way.
///  
///	 @tparam  Val  The type of the values being streamed in from the file
///
template<typename Val>
class InFileStream {
public:
	/// Constructor.
	///
	/// @param[in] in_file  The input file. Must be open in read mode. The streaming will start from the 
	///					current position of its file pointer.
	///			
	InFileStream(std::unique_ptr<FileStd> in_file);

	/// Destructor.
	///
	virtual ~InFileStream();

	/// Streaming operator. Extract a value from the file stream. An undefined value is returned if the 
	/// end-of-file has already been reached.
	///
	/// @param[in] value  Value streamed in (extracted) from the file.
	/// @return  A reference to this object (for chaining operator calls).
	///
	InFileStream& operator>>(Val& value);

	/// Extract a value from the file stream. An undefined value is returned if the end-of-file has already 
	/// been reached.
	///
	/// @return  Value streamed in (extracted) from the file
	///
	Val get();
	
	/// Extract a value from the file stream. An exception is thrown if the end-of-file has already been 
	/// reached. A safe, but slightly slower version of the get() method.
	///
	/// @return  Value streamed in (extracted) from the file
	///
	Val get_safe();

	/// Whether the end of file has been reached.
	///
	/// @return  true only if so.
	///
	bool is_eof() const;

	/// Get the input file path.
	///
	/// @return  The path
	///
	fs::path path() const;

	TWIST_NO_COPY_NO_MOVE(InFileStream<Val>)

private:
	static const Ssize  k_chunk_size  = 5242880;  //5MB 
	static const Ssize  k_buffer_len  = k_chunk_size / sizeof(Val);

	/// Fill the buffer with fresh data from the file. 
	/// Write the contents of the buffer (up to the current writing position) to the file and move back to 
	/// its beginning.
	void refill();
	
	// The input file
	std::unique_ptr<FileStd>  in_file_;
	
	// The memory buffer where values are streamed into, and which is periodically written to the file
	std::unique_ptr<Val[]>  buffer_; 
	
	// The current writing position into the buffer
	std::int64_t  buffer_pos_{};
	
	// The size of the input file
	std::int64_t  file_size_{};
	
	// The current position of the input file pointer
	std::int64_t  file_pos_{};
	
	// If there isn't enough data in the file to fill the buffer, this is the position, within the buffer, 
	// where the data read from the file stops (as the end-of-file has been reached); otherwise, the value 
	// is k_no_pos
	std::int64_t  buffer_end_file_pos_{ k_no_pos };
	
	TWIST_CHECK_INVARIANT_DECL
};

} 

#include "InFileStream.ipp"

#endif  

/// @file IndexRange.hpp
/// IndexRange class

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_INDEX_RANGE_HPP
#define TWIST_INDEX_RANGE_HPP

namespace twist {

/*! A range of integer numbers. The lower bound (lo) in considered inside this range, the upper bound (hi) outside.
    Provides range semantics to allow iteration across the numbers in the range, from lo to hi - 1.
    A range with equal bounds is allowed and is considered empty.
 */
template<std::integral T>
class IndexRange {
public:
	//! Iterates across the numbers in the range.
	class Iterator {
	public:
		using iterator_category = std::bidirectional_iterator_tag;
		using difference_type = Ssize;
		using value_type = T;
		using pointer = const value_type*;
		using reference = T;	

		Iterator(); ///< Constructs the "singular" iterator

		Iterator(T value);

		[[nodiscard]] auto operator*() const -> reference;

		[[nodiscard]] auto operator->() const -> pointer;

		auto operator++() -> Iterator&;

 		auto operator++(int) -> Iterator;

		auto operator--() -> Iterator&;

 		auto operator--(int) -> Iterator;

		friend auto operator==(const Iterator& it1, const Iterator& it2) -> bool
		{
			return it1.value_ == it2.value_; 
		}

		friend auto operator!=(const Iterator& it1, const Iterator& it2) -> bool
		{
			return !(it1 == it2);
		}

	private:
		T value_;
	};

	/*! Constructor. Use it to iterate over the range [lo, hi).
	    \param[in] lo  The low bound of the range
	    \param[in] hi  The high bound of the range; must be greater or equal to the low bound
	 */
	explicit IndexRange(T lo, T hi);
	
	/*! Constructor. Use it to iterate over the range [0, hi).
	    \param[in] hi  The high bound of the range; must be greater or equal to zero
	 */
	explicit IndexRange(T hi);

	//! Iterator addressing the first number in the range, ie the lower bound.
	[[nodiscard]] auto begin() const -> Iterator;

	//! Iterator addressing one past the final number in the range, ie the upper bound.
	[[nodiscard]] auto end() const -> Iterator;
		
	//! The lower bound of the range.
	[[nodiscard]] auto lo() const -> T;

	//! The upper bound of the range.
	[[nodiscard]] auto hi() const -> T;
	
private:
	auto check_bounds() const -> void;

	T lo_;
	T hi_;

	TWIST_CHECK_INVARIANT_DECL
};

// --- Non-member functions ---

//! The size of the range \p range (the difference between its upper and lower bound).
template<std::integral T>
[[nodiscard]] auto size(IndexRange<T> range) -> Ssize;

//! Whether the range \p range1 includes the range \p range2. A range includes itself.
template<std::integral T>
[[nodiscard]] auto includes(IndexRange<T> range1, IndexRange<T> range2) -> bool;

/*! Whether the range \p range contains the value \p point. The lower bound is considered inside the range, the upper 
	bound is not.	
 */
template<std::integral T>
[[nodiscard]] auto contains(IndexRange<T> range, T point) -> bool;

}

#include "twist/IndexRange.ipp"

#endif 

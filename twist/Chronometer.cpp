/// @file Chronometer.cpp
/// Implementation file for "Chronometer.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/Chronometer.hpp"

namespace twist {

const double Chronometer::invalid_time = -1.0;

Chronometer::Chronometer(Unit unit) 
	: unit_{unit}
	, last_rec_time_{invalid_time}
{
	TWIST_CHECK_INVARIANT
}

Chronometer::Chronometer(Chronometer&& src) noexcept
    : unit_{src.unit_}
    , start_time_{src.start_time_}
    , last_rec_time_{src.last_rec_time_}
{
	src.start_time_ = 0;
	src.last_rec_time_ = invalid_time;
	TWIST_CHECK_INVARIANT
}

auto Chronometer::unit() const -> Unit
{
	TWIST_CHECK_INVARIANT
	return unit_;
}

auto Chronometer::start() -> void
{
	TWIST_CHECK_INVARIANT
    if (start_time_ != 0) {
        TWIST_THRO2(L"The chronometer has already been started.");
    }
    last_rec_time_ = invalid_time;
    start_time_ = clock();
}

auto Chronometer::stop() -> double
{
	TWIST_CHECK_INVARIANT
    if (start_time_ == 0) {
        TWIST_THRO2(L"The chronometer has not been started.");
    }
    // Get the calculation time (in seconds)
    const double secs_elapsed = static_cast<double>(clock() - start_time_) / CLOCKS_PER_SEC;
    // Covert it in the desired unit and store it
    if (unit_ == Unit::millisecs) {
		last_rec_time_ = secs_elapsed * 1000;
	}
	else if (unit_ == Unit::seconds) {
		last_rec_time_ = secs_elapsed;	
	}
	else if (unit_ == Unit::hours) {
		last_rec_time_ = secs_elapsed / 3600;
	}

    start_time_ = 0;

	return last_rec_time_;
}

auto Chronometer::last_recorded_time() const -> double
{
	TWIST_CHECK_INVARIANT
	if (last_rec_time_ == invalid_time) {
        TWIST_THRO2(L"This method should only be called after the chronometer has been started and then stopped.");
	}
	return last_rec_time_;
}

auto Chronometer::get_unit_symbol(bool singular) const -> std::wstring
{
	TWIST_CHECK_INVARIANT
	if (unit_ == Unit::millisecs) {
		return L"ms";
	}
	if (unit_ == Unit::seconds) {
		return L"s";
	}
	return singular ? L"hr" : L"hrs";
}

#ifdef _DEBUG
auto Chronometer::check_invariant() const noexcept -> void
{
	assert(Unit::millisecs <= unit_ && unit_ <= Unit::hours);
	assert(start_time_ >= 0);
	assert((last_rec_time_ == invalid_time) || (last_rec_time_ >= 0));
}
#endif 

// --- Free functions ---

auto start_millisec_chronometer() -> Chronometer
{
	auto chron = Chronometer{Chronometer::Unit::millisecs};
	chron.start();
	return chron;
}

auto start_second_chronometer() -> Chronometer
{
	auto chron = Chronometer{Chronometer::Unit::seconds};
	chron.start();
	return chron;
}

}

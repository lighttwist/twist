/// @file feedback_providers.ipp
/// Inline implementation file for feedback_providers.hpp

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace twist {

// --- StreamMessageFeedbackProv class ---

template<class Stream>
const wchar_t* StreamMessageFeedbackProv<Stream>::fatal_err_str = L"FATAL ERROR:  ";

template<class Stream>
StreamMessageFeedbackProv<Stream>::StreamMessageFeedbackProv(Stream& stream)
	: stream_{ stream }
{
	TWIST_CHECK_INVARIANT
}

template<class Stream>
void StreamMessageFeedbackProv<Stream>::do_set_prog_msg(std::wstring_view msg, bool)
{
	TWIST_CHECK_INVARIANT
	stream_ << msg << L"\n";
}

template<class Stream>
void StreamMessageFeedbackProv<Stream>::do_set_fatal_error_msg(std::wstring_view msg)
{
	TWIST_CHECK_INVARIANT
	do_set_prog_msg(std::wstring{fatal_err_str} + msg, false);
}

#ifdef _DEBUG
template<class Stream>
void StreamMessageFeedbackProv<Stream>::check_invariant() const noexcept
{
}
#endif

}

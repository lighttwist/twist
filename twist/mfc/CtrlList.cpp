///  @file  CtrlList.cpp
///  Implementation file for "CtrlList.hpp"

//   Copyright (c) 2007-2019 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist_mfc_maker.hpp"

#include "CtrlList.hpp"

#include "list_ctrl_utils.hpp"

namespace twist::mfc {


IMPLEMENT_DYNAMIC(CtrlList, CListCtrl)

BEGIN_MESSAGE_MAP(CtrlList, CListCtrl)
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONUP()
	ON_NOTIFY_REFLECT(LVN_BEGINDRAG, &CtrlList::OnLvnBegindrag)
END_MESSAGE_MAP()


CtrlList::CtrlList() 
	: CListCtrl()
	, m_dragDropReorder(false)
	, m_dragging(false)
	, m_dragImg(nullptr)
	, m_dragItemIdx(k_no_item)
	, m_dropItemIdx(k_no_item)
{
	TWIST_CHECK_INVARIANT
}
	

CtrlList::~CtrlList()
{
	TWIST_CHECK_INVARIANT
}
	
	
void CtrlList::SetDragDropReorder(bool on)
{
	TWIST_CHECK_INVARIANT
	m_dragDropReorder = on;
	TWIST_CHECK_INVARIANT
}


void CtrlList::OnLvnBegindrag(NMHDR *pNMHDR, LRESULT *pResult)
{
	TWIST_CHECK_INVARIANT
	if (m_dragDropReorder) {
		LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);

		// Store index of dragged item.
		m_dragItemIdx = pNMLV->iItem;

		// Create the drag image and begin dragging.
		m_dragImg = CreateDragImage(m_dragItemIdx);
		m_dragImg->BeginDrag(0, CPoint(-8, -12));
		m_dragImg->DragEnter(GetDesktopWindow(), pNMLV->ptAction);

		// Set mouse capture to this window.
		SetCapture();

		m_dragging = true;
	}
	*pResult = 0;
	TWIST_CHECK_INVARIANT
}


void CtrlList::OnMouseMove(UINT nFlags, CPoint point)
{
	TWIST_CHECK_INVARIANT
    if (m_dragDropReorder && m_dragging) {    
		// Move the drag image
		CPoint pt(point);	
		ClientToScreen(&pt); 
		m_dragImg->DragMove(pt); 
		
		// Unlock window updates (this allows the dragging image to be shown smoothly).
		m_dragImg->DragShowNolock(false);
		
		if (m_dropItemIdx != k_no_item) {
			// Turn off highlight for previous drop target.
			SetItemState(m_dropItemIdx, 0, LVIS_DROPHILITED);
			// Redraw previous item.
			RedrawItems(m_dropItemIdx, m_dropItemIdx);
			UpdateWindow();
		}

		// Get the current drop target.
		UINT uFlags = 0;
		m_dropItemIdx = HitTest(point, &uFlags);
		if (m_dropItemIdx != k_no_item) {
			// Highlight it.
			SetItemState(m_dropItemIdx, LVIS_DROPHILITED, LVIS_DROPHILITED);
			// Redraw item.
			RedrawItems(m_dropItemIdx, m_dropItemIdx);
			UpdateWindow();
		}

		m_dragImg->DragShowNolock(true);
	}

	CListCtrl::OnMouseMove(nFlags, point);
	TWIST_CHECK_INVARIANT
}


void CtrlList::OnLButtonUp(UINT nFlags, CPoint point)
{
	TWIST_CHECK_INVARIANT
    if (m_dragDropReorder && m_dragging) {    
       
        // Release mouse capture.
        ReleaseCapture();
               
		// Drop the item at the new position.
        DropItem();
        
		// Kill drag image list.
		m_dragImg->DragLeave(GetDesktopWindow());
		m_dragImg->EndDrag();
        TWIST_DELETE(m_dragImg);

		m_dragItemIdx = k_no_item;
		m_dropItemIdx = k_no_item;
		m_dragging = false;
    }

	CListCtrl::OnLButtonUp(nFlags, point);
	TWIST_CHECK_INVARIANT
}


void CtrlList::DropItem()
{
	TWIST_CHECK_INVARIANT
	if ((m_dragItemIdx != k_no_item) && (m_dropItemIdx != k_no_item) && (m_dropItemIdx != m_dragItemIdx)) {	
		// Remove highlight from the drop target.
		SetItemState(m_dropItemIdx, 0, LVIS_DROPHILITED);		
		// Move the dragged item to the drop position.
		move_item(*this, m_dragItemIdx, m_dropItemIdx);
		// Select the newly dropped item.
		SetItemState(m_dropItemIdx, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);
	}
	TWIST_CHECK_INVARIANT
}


CImageList* CtrlList::CreateDragImage(long itemIdx)
{
	// Get the rectangle where the list item is displayed.
	CRect itemRect;
	GetItemRect(itemIdx, &itemRect, LVIR_LABEL);
	itemRect.bottom -= 1;

	// Get the device context of the list control, and copy the contents of the item ractangle into a memory bitmap (using a 
	// memory device context for the operation).	
	
	CDC* listCtrlDC = GetDC();

	CBitmap bitmap;
	bitmap.CreateCompatibleBitmap(listCtrlDC, itemRect.Width(), itemRect.Height());

	CDC memDC;
	memDC.CreateCompatibleDC(listCtrlDC);
		
	CBitmap* oldBitmap = memDC.SelectObject(&bitmap);
	memDC.BitBlt(0, 0, itemRect.Width(), itemRect.Height(), listCtrlDC, itemRect.left, itemRect.top, SRCCOPY);
	memDC.SelectObject(oldBitmap);

	ReleaseDC(listCtrlDC);

	// Set the memory bitmap as the sole image in the image list, and return the image list. 
	// Simply use white for the transparent colour. This should be improved sometime.
	CImageList* dragImg = new CImageList();
	dragImg->Create(itemRect.Width(), itemRect.Height(), ILC_COLOR24 | ILC_MASK, 0, 1);
	dragImg->Add(&bitmap, RGB(255, 255, 255));
	return dragImg;
}


#ifdef _DEBUG
void CtrlList::check_invariant() const noexcept
{
	if (m_dragging) {
		assert(m_dragDropReorder);
		assert(m_dragItemIdx != k_no_item);
		assert(m_dragImg != nullptr);
	}
}
#endif // _DEBUG

}


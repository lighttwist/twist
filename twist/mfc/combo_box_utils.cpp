///  @file  mfc/combo_box_utils.cpp
///  Implementation file for "combo_box_utils.hpp"

//   Copyright (c) 2010-2019 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist_mfc_maker.hpp"

#include "combo_box_utils.hpp"

namespace twist::mfc {

int add_item(CComboBox& ctrl, const std::wstring& text)
{
	return ctrl.AddString(text.c_str());
}


int count_items(const CComboBox& ctrl)
{
	return ctrl.GetCount();
}


std::wstring get_item_text(const CComboBox& ctrl, int item_idx)
{
	CString str;
	ctrl.GetLBText(item_idx, str);
	return str.GetString();
}


bool select_item(CComboBox& ctrl, int item_idx)
{
	return ctrl.SetCurSel(item_idx) != CB_ERR || item_idx == CB_ERR;
}


bool select_item_with_text(CComboBox& ctrl, const std::wstring& str)
{
	return ctrl.SelectString(-1, str.c_str()) != CB_ERR;
}


int get_sel_item(const CComboBox& ctrl)
{
	return ctrl.GetCurSel();
}


std::wstring get_sel_item_text(const CComboBox& ctrl)
{	
	CString text;
	const int item_idx = ctrl.GetCurSel();
	if (item_idx != CB_ERR) {
		ctrl.GetLBText(item_idx, text);
	}
	return text.GetString();
}


DWORD_PTR get_sel_item_data(const CComboBox& ctrl)
{
	return get_sel_item_data<DWORD_PTR>(ctrl);
}

}


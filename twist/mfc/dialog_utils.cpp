/// @file  dialog_utils.cpp
/// Implementation file for "dialog_utils.hpp"

//  Copyright (c) 2007-2022, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist_mfc_maker.hpp"

#include "dialog_utils.hpp"

#include <afxdlgs.h>

#include "wnd_utils.hpp"

namespace twist::mfc {

//
//  Local functions
//

std::wstring make_file_dlg_filter(const std::wstring& file_title, const std::vector<std::wstring>& file_extensions)
{
	std::wstringstream filter;
	filter << file_title << L" (";
	
	for (size_t i = 0; i < file_extensions.size(); ++i) {
		filter << L"*." << file_extensions[i];
		if (i < file_extensions.size() - 1) {
			filter << L";";
		}
	}
	
	filter << L")|";
	
	for (size_t i = 0; i < file_extensions.size(); ++i) {
		filter << L"*." << file_extensions[i];
		if (i < file_extensions.size() - 1) {
			filter << L";";
		}
	}

	filter << L"||";

	return filter.str();
}


//
//  Global functions
//

bool do_modal(CDialog& dlg)
{
	return dlg.DoModal() == IDOK;
}


bool do_load_file_dialog(const std::wstring& file_title, const std::vector<std::wstring>& file_extensions, fs::path& filename,
		const std::wstring& dlg_caption, const fs::path& init_dir)
{
	const auto filter = make_file_dlg_filter(file_title, file_extensions);

	CFileDialog dlg(TRUE/*openFileDialog*/, nullptr/*defExt*/, nullptr/*fileName*/, OFN_FILEMUSTEXIST, filter.c_str());

	OPENFILENAME& dlg_struct = dlg.GetOFN();
	
	if (!dlg_caption.empty()) {
		dlg_struct.lpstrTitle = dlg_caption.c_str();
	}
	if (!init_dir.empty()) {
		dlg_struct.lpstrInitialDir = init_dir.c_str();
	}
	
	// Give the buffer that will receive the selected filename a decent (if totally arbitrary) size.
	// The default is a measly 260.
	const size_t fileBufferSize = 2048; 
	wchar_t fileBuffer[fileBufferSize];
	memset(fileBuffer, 0, sizeof(wchar_t) * fileBufferSize);
	dlg_struct.lpstrFile = fileBuffer;
	dlg_struct.nMaxFile = fileBufferSize - 1;
	
	if (do_modal(dlg))	{
		filename = dlg.GetPathName().GetString();
		return true;
	}
	
	return false;
}		


bool do_load_files_dialog(const std::wstring& file_title, const std::vector<std::wstring>& file_extensions, 
		std::vector<fs::path>& filenames, const std::wstring& dlg_caption, const fs::path& init_dir)
{
	filenames.clear();

	const auto filter = make_file_dlg_filter(file_title, file_extensions);
	
	CFileDialog dlg(TRUE/*openFileDialog*/, nullptr/*defExt*/, nullptr/*fileName*/, OFN_FILEMUSTEXIST | OFN_ALLOWMULTISELECT, 
			filter.c_str());
	OPENFILENAME& dlgStruct = dlg.GetOFN();
	
	if (!dlg_caption.empty()) {
		dlgStruct.lpstrTitle = dlg_caption.c_str();
	}
	if (!init_dir.empty()) {
		dlgStruct.lpstrInitialDir = init_dir.c_str();
	}
	
	// Give the buffer that will receive the selected filenames a decent (if totally arbitrary) size.
	// The default is a measly 260.
	const size_t fileBufferSize = 128 * 1024;
	auto file_buffer = std::make_unique<wchar_t[]>(fileBufferSize);
	memset(file_buffer.get(), 0, sizeof(wchar_t) * fileBufferSize);
	dlgStruct.lpstrFile = file_buffer.get();
	dlgStruct.nMaxFile = fileBufferSize - 1;
	
	if (do_modal(dlg) == IDOK)	{
		POSITION pos =  dlg.GetStartPosition();
		while (pos != nullptr) {
			filenames.push_back(dlg.GetNextPathName(pos).GetString());		
		}
		return true;
	}
		
	return false;
}


bool do_save_file_dialog(const std::wstring& file_title, const std::vector<std::wstring>& file_extensions, fs::path& filename,
		const std::wstring& dlg_caption, const fs::path& init_dir)
{
	const auto filter = make_file_dlg_filter(file_title, file_extensions);
	
	const wchar_t* def_ext = file_extensions.empty() ? nullptr : file_extensions[0].data();

	CFileDialog dlg(FALSE/*openFileDialog*/, def_ext, nullptr/*fileName*/, OFN_OVERWRITEPROMPT, filter.c_str());
	OPENFILENAME& dlgStruct = dlg.GetOFN();
	
	if (!dlg_caption.empty()) {
		dlgStruct.lpstrTitle = dlg_caption.c_str();
	}
	if (!init_dir.empty()) {
		dlgStruct.lpstrInitialDir = init_dir.c_str();
	}
	
	// Give the buffer that will receive the selected filename a decent (if totally arbitrary) size.
	// The default is a measly 260.
	const size_t fileBufferSize = 2048; 
	wchar_t fileBuffer[fileBufferSize];
	memset(fileBuffer, 0, sizeof(wchar_t) * fileBufferSize);
	dlgStruct.lpstrFile = fileBuffer;
	dlgStruct.nMaxFile = fileBufferSize - 1;
	
	if (do_modal(dlg) == IDOK)	{
		filename = dlg.GetPathName().GetString();
		return true;
	}
	
	return false;
}


void show_error_box(const std::wstring& message, const std::wstring& caption)
{
	HWND parent_hwnd = nullptr;
	if (CWnd* main_wnd = ::AfxGetMainWnd()) {
		parent_hwnd = main_wnd->GetSafeHwnd();
	}
	::MessageBox(parent_hwnd, message.c_str(), caption.empty() ? L"An Error Has Occurred" : caption.c_str(), MB_ICONERROR);
}


void show_error_box(const CWnd& parent, const std::wstring& message, const std::wstring& caption)
{
	::MessageBox(parent.GetSafeHwnd(), message.c_str(), caption.empty() ? L"An Error Has Occurred" : caption.c_str(), 
			MB_ICONERROR);
}


void show_error_box(const CWnd& parent, const std::exception& ex, const std::wstring& caption)
{
	::MessageBox(parent.GetSafeHwnd(), ansi_to_string(ex.what()).c_str(), 
			caption.empty() ? L"An Error Has Occurred" : caption.c_str(), MB_ICONERROR);
}


void show_info_box(const std::wstring& message, const std::wstring& caption)
{
	HWND parent_hwnd = nullptr;
	if (CWnd* main_wnd = ::AfxGetMainWnd()) {
		parent_hwnd = main_wnd->GetSafeHwnd();
	}
	::MessageBox(parent_hwnd, message.c_str(), caption.empty() ? L"Information" : caption.c_str(), MB_ICONINFORMATION);
}


void show_info_box(const CWnd& parent, const std::wstring& message, const std::wstring& caption)
{
	::MessageBox(parent.GetSafeHwnd(), message.c_str(), caption.empty() ? L"Information" : caption.c_str(), 
			MB_ICONINFORMATION);
}

auto show_question_box(const CWnd& parent, const std::wstring& message, const std::wstring& caption, 
		QuestionBoxType type) -> QuestionBoxAnswer
{
	auto win_type = MB_YESNO;
	if (type == QuestionBoxType::yes_no_cancel) {
		win_type = MB_YESNOCANCEL;
	}

	const auto result = ::MessageBox(
			parent.GetSafeHwnd(), message.c_str(), caption.c_str(), MB_ICONQUESTION | win_type);
	
	auto ret = QuestionBoxAnswer::yes;
	if (result == IDNO) {
		ret = QuestionBoxAnswer::no;
	}
	else if (result == IDCANCEL) {
		ret = QuestionBoxAnswer::cancel;
	}
	
	return ret;
}

auto show_warning_box(const CWnd& parent, const std::wstring& message, const std::wstring& caption) -> void
{
	::MessageBox(hwnd(parent), message.c_str(), caption.c_str(), MB_ICONWARNING);
}

void on_init_menu_popup(CDialog& dlg, CMenu& popup_menu)
{
    // Check the enabled state of various menu items.
    CCmdUI state;
    state.m_pMenu = &popup_menu;
    assert(state.m_pOther == nullptr);
    assert(state.m_pParentMenu == nullptr);

    // Determine if menu is popup in top-level menu and set m_pOther to
    // it if so (m_pParentMenu == nullptr indicates that it is secondary popup).
    HMENU hParentMenu = nullptr;
    if (::AfxGetThreadState()->m_hTrackingMenu == popup_menu.m_hMenu) {
        state.m_pParentMenu = &popup_menu;    // Parent == child for tracking popup.
	}
    else if ((hParentMenu = ::GetMenu(dlg.GetSafeHwnd())) != nullptr) {
		CWnd* pParent = &dlg;
		// Child windows don't have menus--need to go to the top!
		if (pParent != nullptr && (hParentMenu = ::GetMenu(pParent->m_hWnd)) != nullptr) {
			int nIndexMax = ::GetMenuItemCount(hParentMenu);
			for (int nIndex = 0; nIndex < nIndexMax; ++nIndex) {
				if (::GetSubMenu(hParentMenu, nIndex) == popup_menu.m_hMenu) {
					// When popup is found, m_pParentMenu is containing menu.
					state.m_pParentMenu = CMenu::FromHandle(hParentMenu);
					break;
				}
			}
		}
	}

    state.m_nIndexMax = popup_menu.GetMenuItemCount();
    for (state.m_nIndex = 0; state.m_nIndex < state.m_nIndexMax; ++state.m_nIndex) {
        state.m_nID = popup_menu.GetMenuItemID(state.m_nIndex);
        if (state.m_nID == 0) {
           continue; // Menu separator or invalid cmd - ignore it.
		}
        assert(state.m_pOther == nullptr);
        assert(state.m_pMenu != nullptr);
        if (state.m_nID == -1) {
			// Possibly a popup menu, route to first item of that popup.
			state.m_pSubMenu = popup_menu.GetSubMenu(state.m_nIndex);
			if (state.m_pSubMenu == nullptr || (state.m_nID = state.m_pSubMenu->GetMenuItemID(0)) == 0 || state.m_nID == -1) {
				continue;  // First item of popup can't be routed to.
			}
			state.DoUpdate(&dlg, TRUE);   // Popups are never auto disabled.
        }
        else {
           // Normal menu item.
           // Auto enable/disable if frame window has m_bAutoMenuEnable
           // set and command is _not_ a system command.
           state.m_pSubMenu = nullptr;
           state.DoUpdate(&dlg, FALSE);
        }

        // Adjust for menu deletions and additions.
        const UINT nCount = popup_menu.GetMenuItemCount();
        if (nCount < state.m_nIndexMax) {
			state.m_nIndex -= (state.m_nIndexMax - nCount);
			while (state.m_nIndex < nCount && popup_menu.GetMenuItemID(state.m_nIndex) == state.m_nID) {
				++state.m_nIndex;
			}
        }
        state.m_nIndexMax = nCount;
    }
}


void hide_dlg_ctrl(CDialog& dlg, UINT ctrl_id)
{
	auto ctrl = dlg.GetDlgItem(ctrl_id);
	assert(ctrl);
	if (ctrl) {
		ctrl->ShowWindow(SW_HIDE);
	}
}


void move_dlg_ctrl(HWND dlg_hwnd, UINT ctrl_id, int dx, int dy, bool repaint)
{
	const auto ctrl_hwnd = ::GetDlgItem(dlg_hwnd, ctrl_id);
	assert(::IsWindow(ctrl_hwnd));
	
	move_child_wnd(dlg_hwnd, ctrl_hwnd, dx, dy, repaint);
}

}


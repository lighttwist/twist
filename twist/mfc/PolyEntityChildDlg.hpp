///  @file  PolyEntityChildDlg.hpp
///  PolyEntityChildDlg  abstract class template, inherits CDialog 

//   Copyright (c) 2010-2019 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_MFC_POLY_ENTITY_CHILD_DLG_HPP
#define TWIST_MFC_POLY_ENTITY_CHILD_DLG_HPP

namespace twist::mfc {

/// Abstract base class for dialogs for editing and validating a "polymorphic entity" (an entity which can 
/// have different types, and whose set of properties changes with the type, eg a mathematical distribution).
///
/// @tparam  Enty  The entity class type
/// @tparam  EntyTyp  The data type for the entity type
///
template<class Enty, class EntyTyp>
class PolyEntityChildDlg : public CDialog {
public:
	/// Event handler called when a property of the entity is modified within the child dialog
	using ChangedListener = std::function<void ()>;

	/// Event handler called when the child dialog requests that the properties of the entity be validated
	using ValidateListener = std::function<bool ()>;

	/// Constructor. Notice that the dialog's parent should be set later (upon creating the window).
	///
	/// @param[in]  res_id  The child dialog resource template ID.
	///
	PolyEntityChildDlg(UINT res_id);

	/// Initialise the child dialog.
	///
	/// @param[in]  changed_listener  "Enty changed" event listener
	/// @param[in]  validate_listener  "Validate entity" event listener
	///
	virtual void init(ChangedListener changed_listener, ValidateListener validate_listener);

	/// Get the edited entity. This entity will have the properties as entered in the child dialog when it was 
	/// last validated.
	///
	/// @return  The entity
	///
	virtual const Enty& getEntity() const = 0;

	/// Set the (properties of the) entity being edited in the child dialog. The dialog controls are updated 
	/// with the new property values. If this function is not called, the default entity properties will be 
	/// displayed in the dialog.
	///
	/// @param[in]  entity  The entity
	///
	virtual void setEntity(const Enty& entity) = 0;
	
	/// Validate the entity properties as currently entered in the child dialog controls.
	///
	/// @param[out]  err  The error message, if the properties are invalid
	/// @return  Whether the parameters are valid
	///
	virtual bool validate(std::wstring& err) = 0;

protected:
	/// Event handler called when a entity property value is modified within the child dialog.
	///
	void onChanged();

	/// Event handler called when the child dialog requests to be validated.
	///
	void onValidate();

private:
	ChangedListener  changed_listener_;
	ValidateListener  validate_listener_;

	TWIST_CHECK_INVARIANT_DECL
};

}

#include "PolyEntityChildDlg.ipp"

#endif 

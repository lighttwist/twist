///  @file  CtrlStaticGraphic.cpp
///  Implementation file for "CtrlGraph.hpp"

//   Copyright (c) 2007-2019 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist_mfc_maker.hpp"

#include "CtrlStaticGraphic.hpp"

namespace twist::mfc {

// Window class names
static const wchar_t*  k_static_ctrl_class_name =  L"Static";  
static const wchar_t*  k_owner_drawn_static_ctrl_class_name =  L"TwistMfc_OwnerDrawnStatic";  


IMPLEMENT_DYNAMIC(CtrlStaticGraphic, CStatic)

BEGIN_MESSAGE_MAP(CtrlStaticGraphic, CStatic)
	ON_WM_ERASEBKGND()
	ON_WM_SYSCOLORCHANGE()
END_MESSAGE_MAP()


// ATOM CtrlStaticGraphic::s_atom = NULL;


CtrlStaticGraphic::CtrlStaticGraphic(COLORREF clrText, COLORREF clrBorder)
	: CStatic()
	, m_clrText(clrText)
	, m_clrBorder(clrBorder)
	, m_bSysText(false)
	, m_bSysBorder(false)
{
	SetTextColor(clrText);
	SetBorderColor(clrBorder);
}


CtrlStaticGraphic::~CtrlStaticGraphic()
{
}


// void CtrlStaticGraphic::Register(void)
// {
// 	WNDCLASSEX wc = {0};
// 	WNDCLASSEX wcStatic = {0};
// 	wc.cbSize = sizeof(wc);
// 	wcStatic.cbSize = sizeof(wcStatic);
// 
// 	GetClassInfoEx(NULL, k_static_ctrl_class_name, &wcStatic);
// 
// 	wc.style = wcStatic.style | CS_HREDRAW | CS_VREDRAW;
// 	wc.lpfnWndProc = wcStatic.lpfnWndProc;
// 	wc.cbClsExtra = wcStatic.cbClsExtra;
// 	wc.cbWndExtra = wcStatic.cbWndExtra;
// 	wc.hInstance = wcStatic.hInstance;
// 	wc.hIcon = wcStatic.hIcon;
// 	wc.hCursor = wcStatic.hCursor;
// 	wc.hbrBackground = wcStatic.hbrBackground;
// 	wc.lpszMenuName = wcStatic.lpszMenuName;
// 	wc.lpszClassName = k_owner_drawn_static_ctrl_class_name;
// 	wc.hIconSm = wcStatic.hIconSm;
// 
// 	s_atom = RegisterClassEx(&wc);
// }
 

// void CtrlStaticGraphic::Unregister(void)
// {
// 	UnregisterClass((PCTSTR)s_atom, AfxGetInstanceHandle());
// }



COLORREF CtrlStaticGraphic::GetTextColor(void) const
{
	return m_clrText;
}


void CtrlStaticGraphic::SetTextColor(const COLORREF clrText)
{
	if(clrText == CLR_DEFAULT) {
		m_clrText = GetSysColor(COLOR_3DHIGHLIGHT);
		m_bSysText = true;
	}
	else {
		m_clrText = clrText;
		m_bSysText = false;
	}
	SafeInvalidate(FALSE);
}


COLORREF CtrlStaticGraphic::GetBorderColor(void) const
{
	return m_clrBorder;
}


void CtrlStaticGraphic::SetBorderColor(const COLORREF clrBorder)
{
	if (clrBorder == CLR_DEFAULT) {
		m_clrBorder = GetSysColor(COLOR_3DSHADOW);
		m_bSysBorder = true;
	}
	else {
		m_clrBorder = clrBorder;
		m_bSysBorder = false;
	}
	SafeInvalidate(FALSE);
}


void CtrlStaticGraphic::SetWindowText(const std::wstring& text)
{
	SetRedraw(FALSE);
	CStatic::SetWindowText(text.c_str());
	SetRedraw(TRUE);
	SafeInvalidate(FALSE);
}


void CtrlStaticGraphic::SafeInvalidate(bool erase)
{
	if (m_hWnd != nullptr && ::IsWindow(m_hWnd)) {
		Invalidate(erase ? TRUE : FALSE);
	}
}


BOOL CtrlStaticGraphic::PreCreateWindow(CREATESTRUCT &cs)
{
	// _ASSERTE(s_atom != NULL);

	if(!CStatic::PreCreateWindow(cs))
		return FALSE;

	ModifyStyle(SS_TYPEMASK, SS_OWNERDRAW);

	return TRUE;
}


void CtrlStaticGraphic::PreSubclassWindow()
{
	// _ASSERTE(s_atom != NULL);

	CStatic::PreSubclassWindow();
	ModifyStyle(SS_TYPEMASK, SS_OWNERDRAW);
}


void CtrlStaticGraphic::DrawItem(LPDRAWITEMSTRUCT pdis)
{
	CDC* pDC = CDC::FromHandle(pdis->hDC);

	CRect rect = pdis->rcItem;

	pDC->Draw3dRect(rect, m_clrBorder, m_clrBorder);
	rect.DeflateRect(1, 1);

	DrawGraphic(*pDC, rect);

/*
	// Gradient fill the background
	TRIVERTEX tvx[2];
	GRADIENT_RECT gr;
	tvx[0].x = rect.left;
	tvx[0].y = rect.top;
	tvx[0].Red = GetRValue(m_clrFirst) << 8;
	tvx[0].Green = GetGValue(m_clrFirst) << 8;
	tvx[0].Blue = GetBValue(m_clrFirst) << 8;
	tvx[0].Alpha = 0;

	tvx[1].x = rect.right;
	tvx[1].y = rect.bottom; 
	tvx[1].Red = GetRValue(m_clrLast) << 8;
	tvx[1].Green = GetGValue(m_clrLast) << 8;
	tvx[1].Blue = GetBValue(m_clrLast) << 8;
	tvx[1].Alpha = 0;

	gr.UpperLeft = 0;
	gr.LowerRight = 1;
	pDC->GradientFill(tvx, 2, &gr, 1, GRADIENT_FILL_RECT_H);

	// Add the text
	CFont *pFont = GetFont();
	CFont *pFontPrev = pDC->SelectObject(pFont);

	pDC->SetTextColor(m_clrText);
	pDC->SetBkMode(TRANSPARENT);

	CString strText;
	GetWindowText(strText);

	rect.left += m_dwTextSpacing;
	pDC->DrawText(strText, &rect, DT_SINGLELINE | DT_VCENTER);

	if(pFontPrev)
		pDC->SelectObject(pFontPrev);
*/
}


BOOL CtrlStaticGraphic::OnEraseBkgnd(CDC* /*pDC*/)
{
	return TRUE;
}


void CtrlStaticGraphic::OnSysColorChange()
{
	CStatic::OnSysColorChange();

	if(m_bSysText)
		m_clrText = GetSysColor(COLOR_3DHIGHLIGHT);
	if(m_bSysBorder)
		m_clrBorder = GetSysColor(COLOR_3DSHADOW);

	if (m_bSysBorder || m_bSysText) {
		SafeInvalidate(FALSE);
	}
}


#ifdef _DEBUG
void CtrlStaticGraphic::check_invariant() const noexcept
{
}
#endif // _DEBUG

}

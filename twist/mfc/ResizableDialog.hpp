///  @file  ResizableDialog.hpp
///  ResizableDialog  class, inherits CDialogEx

//   Copyright (c) 2010-2019 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_MFC_RESIZABLE_DIALOG_HPP
#define TWIST_MFC_RESIZABLE_DIALOG_HPP

#include <afxdialogex.h>

namespace twist::mfc {

class CtrlLayoutMgr;

///
///  Base class for resizable dialogues.
///  The class provides, for resizing, a layout manager and handles relevant Windows messages.
///
class ResizableDialog : public CDialogEx {
public:
	/// Destructor
	///
	virtual ~ResizableDialog() = 0;

	/// Set the dialogue caption. 
	///
	/// @param[in]  caption  The caption
	///
	void set_caption(const std::wstring& caption);

protected:
	/// Constructor
	///
	ResizableDialog();

	/// Constructor
	///
	/// @param[in]  template_id  The dialog template resource ID
	/// @param[in]  parent  The dialog parent window; pass in nullptr for the main application window
	///
	ResizableDialog(UINT template_id, CWnd* parent = nullptr);
	
	/// Constructor
	///
	/// @param[in]  template_name  The name of the dialog template resource
	/// @param[in]  parent  The dialog parent window; pass in nullptr for the main application window
	///
	ResizableDialog(const wchar_t* template_name, CWnd* parent = nullptr);   

	/// Get the dialog layout manager.
	///
	/// @return  The manager
	///
	CtrlLayoutMgr& GetLayoutMgr();

	virtual void DoDataExchange(CDataExchange* pDX) override;    
	
	virtual BOOL OnInitDialog() override;
	
	afx_msg void OnSize(UINT nType, int cx, int cy);

	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);

	DECLARE_DYNAMIC(ResizableDialog)

	DECLARE_MESSAGE_MAP()

private:	
	std::unique_ptr<CtrlLayoutMgr>  layout_mgr_;  // Layout manager for the dialog controls
	std::wstring  caption_;

	TWIST_CHECK_INVARIANT_DECL
};

}

#endif

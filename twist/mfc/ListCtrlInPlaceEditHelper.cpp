///  @file  ListCtrlInPlaceEditHelper.cpp
///  Implementation file for "ListCtrlInPlaceEditHelper.hpp"

//   Copyright (c) 2007-2019 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist_mfc_maker.hpp"

#include "ListCtrlInPlaceEditHelper.hpp"

using namespace std::placeholders;

namespace twist::mfc {

ListCtrlInPlaceEditHelper::ListCtrlInPlaceEditHelper(CListCtrl& lst_ctrl, int item_creation_col)
	: lst_ctrl_(lst_ctrl)
	, edt_ctrl_(std::make_unique<CEdit>(), lst_ctrl, std::bind(&ListCtrlInPlaceEditHelper::on_edt_ctrl_exit, this, _1))
	, item_creation_col_(item_creation_col)
	, on_adding_item_()
	, on_item_added_()
	, on_editing_item_()
	, on_item_edited_()
	, on_deleting_item_()
{
	TWIST_CHECK_INVARIANT
}


void ListCtrlInPlaceEditHelper::set_on_adding_item(OnAddingItem on_adding_item)
{
	TWIST_CHECK_INVARIANT
	assert(!on_adding_item_);
	on_adding_item_ = on_adding_item;
	TWIST_CHECK_INVARIANT
}


void ListCtrlInPlaceEditHelper::set_on_item_added(OnItemAdded on_item_added)
{
	TWIST_CHECK_INVARIANT
	assert(!on_item_added_);
	on_item_added_ = on_item_added;
	TWIST_CHECK_INVARIANT
}


void ListCtrlInPlaceEditHelper::set_on_editing_item(OnEditingItem on_editing_item)
{
	TWIST_CHECK_INVARIANT
	assert(!on_editing_item_);
	on_editing_item_ = on_editing_item;
	TWIST_CHECK_INVARIANT
}


void ListCtrlInPlaceEditHelper::set_on_item_edited(OnItemEdited on_item_edited)
{
	TWIST_CHECK_INVARIANT
	assert(!on_item_edited_);
	on_item_edited_ = on_item_edited;
	TWIST_CHECK_INVARIANT
}


void ListCtrlInPlaceEditHelper::set_on_deleting_item(OnDeletingItem on_deleting_item)
{
	TWIST_CHECK_INVARIANT
	assert(!on_deleting_item_);
	on_deleting_item_ = on_deleting_item;
	TWIST_CHECK_INVARIANT
}


void ListCtrlInPlaceEditHelper::on_lst_ctrl_hdn_begintrack(NMHDR* /*pNMHDR*/, LRESULT* pResult)
{
	TWIST_CHECK_INVARIANT
	edt_ctrl_.hide();
	*pResult = FALSE;
	TWIST_CHECK_INVARIANT
}


void ListCtrlInPlaceEditHelper::on_lst_ctrl_dblclk(NMHDR* pNMHDR, LRESULT* pResult)
{
	TWIST_CHECK_INVARIANT
	const auto* pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	if (pNMItemActivate->iItem != -1) {
		bool in_place_edit = true;
		if (pNMItemActivate->iItem == count_items(lst_ctrl_) - 1) {
			// The placeholder list item has been clicked; only allow editing of the log family name
			in_place_edit = pNMItemActivate->iSubItem == item_creation_col_;
		}
		if (in_place_edit) {
			edt_ctrl_.show_over_lst_ctrl_cell(lst_ctrl_, pNMItemActivate->iItem, pNMItemActivate->iSubItem);
		}
	}
	*pResult = 0;
	TWIST_CHECK_INVARIANT
}


void ListCtrlInPlaceEditHelper::on_lst_ctrl_keydown(NMHDR* pNMHDR, LRESULT* pResult)
{
	TWIST_CHECK_INVARIANT
	LPNMLVKEYDOWN pLVKeyDow = reinterpret_cast<LPNMLVKEYDOWN>(pNMHDR);
	if (pLVKeyDow->wVKey == VK_DELETE) {
		const int item = get_first_sel_item(lst_ctrl_);
		const int placeholder_item = count_items(lst_ctrl_) - 1;
		if (item >= 0 && item < placeholder_item) {
			if (!on_deleting_item_ || on_deleting_item_(item)) {
				lst_ctrl_.DeleteItem(item);
				if (item < count_items(lst_ctrl_) - 1) {
					select_and_focus_item(lst_ctrl_, item, true);
				}
				else if (item > 0) {
					select_and_focus_item(lst_ctrl_, item - 1, true);
				}
			}
		}
	}
	*pResult = 0;
	TWIST_CHECK_INVARIANT
}


void ListCtrlInPlaceEditHelper::on_edt_ctrl_exit(const std::wstring& text)
{
	TWIST_CHECK_INVARIANT
	const int item = edt_ctrl_.get_lst_ctrl_item_idx();
	if (item != -1) {  
		const auto trimmed_text = trim_whitespace(text);

		if (item == count_items(lst_ctrl_) - 1) {
			// The placeholder item has just been edited, which means that the user is trying to add new item and that the 
			// editing is taking place in the "item creation" column.  					
			if (!trimmed_text.empty()) {  // Do not allow a blank new item 

				DWORD_PTR data = 0;
				if (!on_adding_item_ || on_adding_item_(trimmed_text, data)) {

					set_item_text(lst_ctrl_, item, item_creation_col_, trimmed_text);

					if (data != 0) {
						set_item_data(lst_ctrl_, item, data);
					}
									
					// Add a new list control placeholder item 
					if (item == count_items(lst_ctrl_) - 1) {
						const int placeholder_item = add_item(lst_ctrl_, L"");
						lst_ctrl_.EnsureVisible(placeholder_item, FALSE/*partial_ok*/);
					}

					if (on_item_added_) {
						on_item_added_(trimmed_text);
					}
				}
			}
		}
		else {				
			const int col = edt_ctrl_.get_lst_ctrl_col_idx();	
			// An existing item has just been edited
			if (col == item_creation_col_ && trimmed_text.empty()) {
				return;  // Blank string is not allowed if the column being edited is the "item creation" column
			}
			if (!on_editing_item_ || on_editing_item_(item, col, trimmed_text)) {				
				set_item_text(lst_ctrl_, item, col, trimmed_text);						
				if (on_item_edited_) {
					on_item_edited_(item, col, trimmed_text);
				}
			}
		}	
	}
	TWIST_CHECK_INVARIANT
}


#ifdef _DEBUG
void ListCtrlInPlaceEditHelper::check_invariant() const noexcept
{
}
#endif 

}

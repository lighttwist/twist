///  @file  combo_box_utils.ipp
///  Inline implementation file for "combo_box_utils.hpp"

//   Copyright (c) 2010-2019 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "ctrl_utils.hpp"

namespace twist::mfc {

template<typename TData> 
int add_item(CComboBox& ctrl, const std::wstring& text, TData data)
{
	const int item_idx = ctrl.AddString(text.c_str());
	if (item_idx >= 0) {
		ctrl.SetItemData(item_idx, detail::user_to_internal_data<TData>(data, std::is_pointer<TData>()));
	}
	return item_idx;
}


template<typename TData> 
int insert_item(CComboBox& ctrl, const std::wstring& text, TData data, int index)
{
	const int item_idx = ctrl.InsertString(index, text.c_str());
	if (item_idx >= 0) {
		ctrl.SetItemData(item_idx, detail::user_to_internal_data<TData>(data, std::is_pointer<TData>()));
	}
	return item_idx;
}


template<typename TData> 
int find_item_with_data(const CComboBox& ctrl, TData data)
{
	int item_idx = CB_ERR;
	const DWORD_PTR internal_data = detail::user_to_internal_data<TData>(data, std::is_pointer<TData>());
	for (int i = 0; i < ctrl.GetCount(); ++i) {
		if (ctrl.GetItemData(i) == internal_data) {
			item_idx = i;
		}
	}
	return item_idx;
}


template<typename TData> 
TData get_sel_item_data(const CComboBox& ctrl)
{
	const int item_idx = ctrl.GetCurSel();
	if (item_idx != CB_ERR) {
		return detail::internal_to_user_data<TData>(ctrl.GetItemData(item_idx), std::is_pointer<TData>());
	}
	return detail::internal_to_user_data<TData>(0, std::is_pointer<TData>());
}


template<typename TData> 
int select_item_with_data(CComboBox& ctrl, TData data)
{
	const int item_idx = find_item_with_data<TData>(ctrl, data);
	if (item_idx != CB_ERR) {
		ctrl.SetCurSel(item_idx);
	}
	return item_idx;
}


template<typename TData> 
bool delete_item_with_data(CComboBox& ctrl, TData data)
{
	const int item_idx = find_item_with_data(ctrl, data);
	if (item_idx != CB_ERR) {
		return ctrl.DeleteString(item_idx) != CB_ERR;
	}
	return false;
}

}


///  @file  tree_ctrl_utils.cpp
///  Implementation file for "tree_ctrl_utils.hpp"

//   Copyright (c) 2010-2019 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/mfc/twist_mfc_maker.hpp"

#include "twist/mfc/tree_ctrl_utils.hpp"

namespace twist::mfc {

using namespace std;

bool select_item(CTreeCtrl& ctrl, HTREEITEM item)
{
	return ctrl.SelectItem(item) == TRUE;
}


bool item_has_children(const CTreeCtrl& ctrl, HTREEITEM item)
{
	return ctrl.ItemHasChildren(item) == TRUE;
}


bool ensure_visible(CTreeCtrl& ctrl, HTREEITEM item)
{
	return ctrl.EnsureVisible(item) == TRUE;
}


void expand_subtree(CTreeCtrl& ctrl, HTREEITEM top_item)
{
	// Expand the top item
	ctrl.Expand(top_item, TVE_EXPAND);

	// Call this function recursively for each child item
	HTREEITEM next_item = nullptr;
	HTREEITEM child_item = ctrl.GetChildItem(top_item);

	while (child_item != nullptr) {
		next_item = ctrl.GetNextItem(child_item, TVGN_NEXT);
		if (item_has_children(ctrl, child_item)) {
			expand_subtree(ctrl, child_item);
		}
		child_item = next_item;
	}
}


void explore_subtree(const CTreeCtrl& ctrl, HTREEITEM top_item, TreeItemFunc item_func)
{
	// Recursive functor
	function<void (HTREEITEM, unsigned int)> recursive_explore = [&](auto item, auto depth) {

		item_func(item, depth);

		HTREEITEM child_item = ctrl.GetChildItem(item);
		++depth;

		while (child_item != nullptr) {			
			recursive_explore(child_item, depth);
			child_item = ctrl.GetNextItem(child_item, TVGN_NEXT);
		}
	};

	recursive_explore(top_item, 0U);
}


void explore_subtree_leaves(const CTreeCtrl& ctrl, HTREEITEM top_item, TreeItemFunc item_func)
{
	// Recursive functor
	function<void (HTREEITEM, unsigned int)> recursive_explore = [&](auto item, auto depth) {

		if (item_has_children(ctrl, top_item)) {

			HTREEITEM child_item = ctrl.GetChildItem(item);
			++depth;

			while (child_item != nullptr) {
				if (item_has_children(ctrl, child_item)) {
					recursive_explore(child_item, depth);
				}
				else {
					item_func(child_item, depth);
				}
				child_item = ctrl.GetNextItem(child_item, TVGN_NEXT);
			}
		}
		else {
			item_func(item, depth);
		}
	};

	recursive_explore(top_item, 0U);
}


unsigned int get_subtree_depth(const CTreeCtrl& ctrl, HTREEITEM top_item)
{
	unsigned int max_depth = 0;
	explore_subtree(ctrl, top_item, [&](HTREEITEM /*item*/, unsigned int depth) {
		if (depth > max_depth) {
			max_depth = depth;
		}
	});
	return max_depth + 1;  // Add one, as count the top item as a subtree level
}

auto subtree_to_matrix(const CTreeCtrl& ctrl, HTREEITEM top_item) -> twist::math::RowwiseMatrix<HTREEITEM>
{
	const auto max_depth = get_subtree_depth(ctrl, top_item);
	auto mat = twist::math::RowwiseMatrix<HTREEITEM>{};

	explore_subtree_leaves(ctrl, top_item, [&mat, &ctrl, max_depth](auto item, auto depth) {

		const auto row = mat.add_row(std::vector<HTREEITEM>(max_depth, nullptr));
		HTREEITEM col_item = item;
		
		for (auto col = depth; col >= 0; --col) {
			mat(row, col) = col_item;	
			col_item = ctrl.GetParentItem(col_item);
		}
	});

	return mat;
}

}


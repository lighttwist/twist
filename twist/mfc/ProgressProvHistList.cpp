///  @file  ProgressProvHistList.cpp
///  Implementation file for "ProgressProvHistList.hpp"

//   Copyright (c) 2010-2019 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist_mfc_maker.hpp"

#include "ProgressProvHistList.hpp"

#include "list_ctrl_utils.hpp"
#include "wnd_utils.hpp"

namespace twist::mfc {

ProgressProvHistList::ProgressProvHistList(CProgressCtrl& prog_ctrl, CListCtrl& list_ctrl, CWnd* parent_wnd)
	: ProgressProv()
	, prog_ctrl_(prog_ctrl)
	, list_ctrl_(list_ctrl)
	, parent_wnd_(parent_wnd)
{
	TWIST_CHECK_INVARIANT
}


void ProgressProvHistList::init_message_mode()
{
	TWIST_CHECK_INVARIANT
	TWIST_THROW(L"This progress provider only supports the \"history\" mode.");
}

	
void ProgressProvHistList::init_hist_mode(long /*expected_num_lines*/)
{
	TWIST_CHECK_INVARIANT
	//+
}


void ProgressProvHistList::set_prog_msg(std::wstring_view /*msg*/, bool /*force_update*/)
{
	TWIST_CHECK_INVARIANT
	// Do nothing
}


void ProgressProvHistList::add_new_hist_line(const std::wstring& hist_line)
{
	TWIST_CHECK_INVARIANT
	const int item_idx = add_item(list_ctrl_, hist_line);	
	list_ctrl_.EnsureVisible(item_idx, FALSE/*partial_ok*/);
	if (parent_wnd_ != nullptr) {
		pump_window_messages(parent_wnd_->GetSafeHwnd());
	}
	TWIST_CHECK_INVARIANT
}


void ProgressProvHistList::set_present_hist_line(const std::wstring& hist_line)
{
	TWIST_CHECK_INVARIANT
	set_item_text(list_ctrl_, count_items(list_ctrl_) - 1, 0, hist_line);
	if (parent_wnd_ != nullptr) {
		pump_window_messages(parent_wnd_->GetSafeHwnd());
	}
	TWIST_CHECK_INVARIANT
}


void ProgressProvHistList::get_prog_range(Ssize& min_pos, Ssize& max_pos) const
{
	TWIST_CHECK_INVARIANT
	int min = 0;
	int max = 0;
	prog_ctrl_.GetRange(min, max);
	min_pos = static_cast<Ssize>(min);
	max_pos = static_cast<Ssize>(max);
	TWIST_CHECK_INVARIANT
}


void ProgressProvHistList::set_prog_range(Ssize min_pos, Ssize max_pos)
{
	TWIST_CHECK_INVARIANT
	prog_ctrl_.SetRange(static_cast<short>(min_pos), static_cast<short>(max_pos)); //+ check conv to short
	TWIST_CHECK_INVARIANT
}


Ssize ProgressProvHistList::get_prog_pos() const
{
	TWIST_CHECK_INVARIANT
	return prog_ctrl_.GetPos();
}


void ProgressProvHistList::set_prog_pos(Ssize pos, bool force_update)
{
	TWIST_CHECK_INVARIANT
	prog_ctrl_.SetPos(static_cast<int>(pos));
	if (force_update && parent_wnd_ != nullptr) {
		pump_window_messages(parent_wnd_->GetSafeHwnd());
	}
	TWIST_CHECK_INVARIANT
}


void ProgressProvHistList::inc_prog_pos(bool force_update)
{
	TWIST_CHECK_INVARIANT
	prog_ctrl_.SetPos(prog_ctrl_.GetPos() + 1);
	if (force_update && parent_wnd_ != nullptr) {
		pump_window_messages(parent_wnd_->GetSafeHwnd());
	}
	TWIST_CHECK_INVARIANT
}


void ProgressProvHistList::hide()
{
	TWIST_CHECK_INVARIANT
	if (parent_wnd_ != nullptr) {
		::ShowWindow(parent_wnd_->GetSafeHwnd(), SW_HIDE);
	}
	TWIST_CHECK_INVARIANT
}


#ifdef _DEBUG
void ProgressProvHistList::check_invariant() const noexcept
{
}
#endif // _DEBUG

}



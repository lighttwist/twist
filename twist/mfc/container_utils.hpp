///  @file  container_utils.hpp
///  Utilities for working with MFC containers

//   Copyright (c) 2010-2019 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_MFC_CONTAINER__UTILS_HPP
#define TWIST_MFC_CONTAINER__UTILS_HPP

#include <afxtempl.h>

namespace twist::mfc {

/// Forward iterator for the CTypedPtrList MFC class template.
/// It addresses the pointer elements of such a list.
template<class ListBase, class Ty>
class TypedPtrListIterator {
public:
	using List = CTypedPtrList<ListBase, Ty>;
	using iterator_category = std::forward_iterator_tag;
	using difference_type = INT_PTR;
	using value_type = Ty;
	using pointer = Ty;
	using reference = Ty;	

	TypedPtrListIterator(const List& list, POSITION pos);

	reference operator*() const; 
	
	pointer operator->() const;  

	TypedPtrListIterator& operator++();

 	TypedPtrListIterator operator++(int); 

	bool operator==(const TypedPtrListIterator& rhs) const; 

	bool operator!=(const TypedPtrListIterator& rhs) const; 

private:
	static_assert(std::is_pointer_v<Ty>);

	const gsl::not_null<const List*>  list_;
	POSITION  pos_;
};

}

/// Get an iterator addressing the first element in a CTypedPtrList list object.
///
/// @param[in]  list  The list
/// @return  The iterator
///
template<class ListBase, class Ty>
twist::mfc::TypedPtrListIterator<ListBase, Ty> begin(const CTypedPtrList<ListBase, Ty>& list);

/// Get an iterator addressing one past the last element in a CTypedPtrList list object.
///
/// @param[in]  list  The list
/// @return  The iterator
///
template<class ListBase, class Ty>
twist::mfc::TypedPtrListIterator<ListBase, Ty> end(const CTypedPtrList<ListBase, Ty>& list);

/// Get a const iterator (represented by a const pointer) addressing the first element in a CArray array 
/// object.
///
/// @param  arr  [in] The array
/// @return  The const iterator
///
template<typename Ty, typename ArgTy>
const Ty* begin(const CArray<Ty, ArgTy>& arr);

/// Get an iterator (represented by a pointer) addressing the first element in a CArray array object.
///
/// @param  arr  [in] The array
/// @return  The iterator
///
template<typename Ty, typename ArgTy>
Ty* begin(CArray<Ty, ArgTy>& arr);

/// Get a const iterator (represented by a const pointer) addressing one past the final element in a CArray 
/// array object.
///
/// @param  arr  [in] The array
/// @return  The iterator
///
template<typename Ty, typename ArgTy>
const Ty* end(const CArray<Ty, ArgTy>& arr);

/// Get an iterator (represented by a pointer) addressing one past the final element in a CArray 
/// array object.
///
/// @param  arr  [in] The array
/// @return  The iterator
///
template<typename Ty, typename ArgTy>
Ty* end(CArray<Ty, ArgTy>& arr);

#include "container_utils.ipp"

#endif

//  Inline implementation file for "button_utils.hpp"
//
//  Copyright (c) 2007-2022, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/mfc/wnd_utils.hpp"

namespace twist::mfc {

template<class Option>
RadioButtonGroup<Option>::RadioButtonGroup(std::vector<std::tuple<int, Option>> ctrl_id_options, CWnd& parent_wnd)
	: ctrl_id_options_{move(ctrl_id_options)}
	, parent_wnd_{parent_wnd}
{
	if (ssize(ctrl_id_options_) < 2) {
		TWIST_THROW(L"The radio button group must contain at least two buttons.");
	}

	auto ctrl_ids = vw::elements<0>(ctrl_id_options_) | rg::to<std::vector>();
	if (has_duplicates(ctrl_ids)) {
		TWIST_THROW(L"A control ID cannot appear twice in the radio button group.");
	}

	auto options = vw::elements<1>(ctrl_id_options_) | rg::to<std::vector>();
	if (has_duplicates(options)) {
		TWIST_THROW(L"An option value cannot appear twice in the radio button group.");
	}

	auto has_ws_group = [this](auto ctrl_id) {
		auto ctrl_wnd = gsl::not_null{parent_wnd_.GetDlgItem(ctrl_id)};
		return has_group_window_style(hwnd(*ctrl_wnd));
	};

	if (!has_ws_group(get<0>(ctrl_id_options_.front()))) {
		TWIST_THROW(L"The first control in the list must have the \"group\" window style.");
	}
	for (auto i : IndexRange{1, ssize(ctrl_id_options_)}) {
		if (has_ws_group(get<0>(ctrl_id_options_[i]))) {
			TWIST_THROW(L"Only the first control in the list can have the \"group\" window style.");
		}	
	}
	TWIST_CHECK_INVARIANT
}

template<class Option>
auto RadioButtonGroup<Option>::get_checked_option() const -> std::optional<Option>
{
	TWIST_CHECK_INVARIANT
	const auto checked_id = parent_wnd_.GetCheckedRadioButton(get<0>(ctrl_id_options_.front()),  
	                                                          get<0>(ctrl_id_options_.back()));
    return checked_id == 0 ? std::optional<Option>{} : get_option(checked_id);
}

template<class Option>
auto RadioButtonGroup<Option>::check_option(Option option) -> void
{
	TWIST_CHECK_INVARIANT
	assert(twist::has(vw::elements<1>(ctrl_id_options_), option));

	parent_wnd_.CheckRadioButton(get<0>(ctrl_id_options_.front()), get<0>(ctrl_id_options_.back()), 
	                             get_ctrl_id(option));
}

template<class Option>
auto RadioButtonGroup<Option>::get_option(int ctrl_id) const -> Option
{
	TWIST_CHECK_INVARIANT
	if (auto it = rg::find_if(ctrl_id_options_, [ctrl_id](const auto& el) { return get<0>(el) == ctrl_id; }); 
	         it != end(ctrl_id_options_)) {
		return get<1>(*it);
	}
	TWIST_THROW(L"Control ID %d is not part of the radio button group.", ctrl_id);
}

template<class Option>
auto RadioButtonGroup<Option>::get_ctrl_id(Option option) const -> int
{
	TWIST_CHECK_INVARIANT
	if (auto it = rg::find_if(ctrl_id_options_, [option](const auto& el) { return get<1>(el) == option; }); 
	         it != end(ctrl_id_options_)) {
		return get<0>(*it);
	}
	TWIST_THROW(L"Option is not part of the radio button group.");
}

#ifdef _DEBUG
template<class Option>
auto RadioButtonGroup<Option>::check_invariant() const noexcept -> void
{
	assert(ssize(ctrl_id_options_) >= 2);
}
#endif

}

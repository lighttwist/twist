///  @file  ResizableDialog.cpp
///  Implementation file for "ResizableDialog.hpp"

//   Copyright (c) 2010-2019 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist_mfc_maker.hpp"

#include "ResizableDialog.hpp"

#include "CtrlLayoutMgr.hpp"
#include "wnd_utils.hpp"

namespace twist::mfc {

IMPLEMENT_DYNAMIC(ResizableDialog, CDialogEx)

BEGIN_MESSAGE_MAP(ResizableDialog, CDialogEx)
	ON_WM_SIZE()
	ON_WM_GETMINMAXINFO()
END_MESSAGE_MAP()


ResizableDialog::ResizableDialog() 
	: CDialogEx()
	, layout_mgr_()
	, caption_()
{
	TWIST_CHECK_INVARIANT
}


ResizableDialog::ResizableDialog(UINT template_id, CWnd* parent) 
	: CDialogEx(template_id, parent)
	, layout_mgr_()
	, caption_()
{
	TWIST_CHECK_INVARIANT
}


ResizableDialog::ResizableDialog(const wchar_t* template_name, CWnd* parent)
	: CDialogEx(template_name, parent)
	, layout_mgr_()
	, caption_()
{
	TWIST_CHECK_INVARIANT
}


ResizableDialog::~ResizableDialog()
{
	TWIST_CHECK_INVARIANT
}


void ResizableDialog::set_caption(const std::wstring& caption)
{
	TWIST_CHECK_INVARIANT
	caption_ = caption;
	TWIST_CHECK_INVARIANT
}


void ResizableDialog::DoDataExchange(CDataExchange* pDX)
{
	TWIST_CHECK_INVARIANT
	CDialogEx::DoDataExchange(pDX);
	TWIST_CHECK_INVARIANT
}


BOOL ResizableDialog::OnInitDialog()
{
	TWIST_CHECK_INVARIANT
	CDialogEx::OnInitDialog();

	layout_mgr_ = std::make_unique<CtrlLayoutMgr>(GetSafeHwnd());

	if (!caption_.empty()) {
		set_wnd_text(*this, caption_);
	}

	TWIST_CHECK_INVARIANT
	return TRUE;  
}


CtrlLayoutMgr& ResizableDialog::GetLayoutMgr()
{
	TWIST_CHECK_INVARIANT
	if (!layout_mgr_) {
		TWIST_THROW(L"The layout manager has not been intialised.");
	}
	TWIST_CHECK_INVARIANT
	return *layout_mgr_;
}


void ResizableDialog::OnSize(UINT nType, int cx, int cy)
{
	TWIST_CHECK_INVARIANT
	CDialogEx::OnSize(nType, cx, cy);
	if (layout_mgr_) {
		layout_mgr_->arrange_ctrls();
	}
	TWIST_CHECK_INVARIANT
}


void ResizableDialog::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	TWIST_CHECK_INVARIANT
	if (layout_mgr_) {
		layout_mgr_->on_parent_min_max(lpMMI);
	}
	CDialogEx::OnGetMinMaxInfo(lpMMI);
	TWIST_CHECK_INVARIANT
}


#ifdef _DEBUG
void ResizableDialog::check_invariant() const noexcept
{
}
#endif // _DEBUG

}


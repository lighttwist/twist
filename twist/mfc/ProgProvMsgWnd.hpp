///  @file  ProgProvMsgWnd.hpp
///  ProgProvMsgWnd  class, inherits ProgressProv

//   Copyright (c) 2010-2019 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_MFC_PROG_PROV_MSG_WND_HPP
#define TWIST_MFC_PROG_PROV_MSG_WND_HPP

#include "../ProgressProv.hpp"

namespace twist::mfc {

///
///  Implements a progress provider which uses a "message window" (eg a label or an edit control) and a progress bar control.
///
class ProgProvMsgWnd : public ProgressProv {
public:
	/// Constructor.
	///
	/// @param[in]  prog_wnd  The progress control window
	/// @param[in]  msg_wnd  The "message window"
	/// @param[in]  parent_wnd  The parent parent_wnd of the two controls passed in (if any); used for pumping messages for 
	///				updating the controls; if it is nullptr, no messages are pumped
	///	
	ProgProvMsgWnd(CProgressCtrl& prog_wnd, CWnd& msg_wnd, CWnd* parent_wnd);

	/// Destructor.
	///
	virtual ~ProgProvMsgWnd();

	virtual void init_message_mode() override;  //  ProgressProv  override
	
	virtual void init_hist_mode(long expected_num_lines) override;  //  ProgressProv  override

	virtual void set_prog_msg(std::wstring_view msg, bool force_update) override;  //  ProgressProv  override

	virtual void add_new_hist_line(const std::wstring& hist_line) override;  //  ProgressProv  override

	virtual void set_present_hist_line(const std::wstring& hist_line) override;  //  ProgressProv  override

    virtual void hide() override;  //  ProgressProv  override
	
	virtual void get_prog_range(twist::Ssize& min_pos, twist::Ssize& max_pos) const override;  //  ProgProvBase  override
	
	virtual void set_prog_range(twist::Ssize min_pos, twist::Ssize max_pos) override;  //  ProgProvBase  override
	
	virtual twist::Ssize get_prog_pos() const override;  //  ProgProvBase  override
	
	virtual void set_prog_pos(twist::Ssize pos, bool force_update) override;  //  ProgProvBase  override
	
	virtual void inc_prog_pos(bool force_update) override;  //  ProgProvBase  override

private:	
	CProgressCtrl&  prog_wnd_;
	CWnd&  msg_wnd_;
	CWnd*  parent_wnd_;

	TWIST_CHECK_INVARIANT_DECL
};

}

#endif 


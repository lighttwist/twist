///  @file  list_ctrl_utils.hpp
///  Utilities for working with the classic MFC control class  CListCtrl 

//   Copyright (c) 2007-2019 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_MFC_LIST__CTRL__UTILS_HPP
#define TWIST_MFC_LIST__CTRL__UTILS_HPP

#include "wnd_utils.hpp"

namespace twist::mfc {

///
///  A class which can read, store and restore the state of a list control, including the number of items and 
///  columns, the text in each subitem of each item, and each item's data.
///
class ListCtrlState {
public:
	///
	///  The state of a list item, including the the text in each subitem and the item's data.
	///
	class ItemState {
	public:
		const std::vector<std::wstring>& subitem_strings() const;
		
		DWORD_PTR item_data() const;
	
	private:
		ItemState(const std::vector<std::wstring>& subitem_strings, DWORD_PTR item_data);

		std::vector<std::wstring>  subitem_strings_;
		DWORD_PTR  item_data_;

		friend class ListCtrlState;
	};

	/// Read the state of a list control.
	///
	/// @param[in]  ctrl  The list control
	/// @return  The state
	///
	static std::unique_ptr<ListCtrlState> read_from_ctrl(const CListCtrl& ctrl);

	/// Restore a list control's state to match this object (or transfer it to another, compatible list control). 
	///
	/// @param[in]  ctrl  The list control; an exception is throw if it is not compatible with teh stored state
	///
	void write_to_ctrl(CListCtrl& ctrl) const;

	/// Get a list of each item's state.
	///
	/// @return  The item states
	///
	std::vector<const ItemState*> get_item_states() const;

	/// Get the strings which appear in a specific column, in the order of the items in the list.
	///
	/// @param[in]  col_idx  The column index
	/// @return  The strings
	///
	std::vector<std::wstring> get_column_strings(int col_idx) const;

private:
	ListCtrlState(int num_cols);

	const int num_cols_;
	std::vector<ItemState>  item_states_;

	TWIST_CHECK_INVARIANT_DECL
};

/// Initialise a list control with default styles.
///
/// @param[in]  ctrl  The list control
///
void init(CListCtrl& ctrl);  
	
/// Initialise a list control with specific, or'ed styles.
///
/// @param[in]  ctrl  The list control
/// @param[in]  styles  One or more list view styles, or'ed together. See help for "Extended List-View Styles" 
///					for the values.
///
void init(CListCtrl& ctrl, DWORD styles);  

/// Set whether only one item at a time can be selected in a list control.
///
/// @param[in]  ctrl  The list control
/// @param[in]  on  true for on. Default is off.
///
void set_single_sel(CListCtrl& ctrl, bool on);

/// Set whether a list control should display gridlines.
///
/// @param[in]  ctrl  The list control
/// @param[in]  on  true for on. Default is on.
///
void set_grid_lines(CListCtrl& ctrl, bool on);

/// Set whether a list control should use full row selection. This means that when an item is selected, the 
/// item and all its subitems are highlighted. This style applies only if the list has the "report" view mode.
///
/// @param[in]  ctrl  The list control
/// @param[in]  on  true for on. Default is on.
///
void set_full_row_select(CListCtrl& ctrl, bool on); 
	
/// Set whether a list control should display infotips (when the mouse hovers over cells where the text is 
/// incomplete). This also enables custom handling of the  LVN_GETINFOTIP  message.
///
/// @param[in]  ctrl  The list control
/// @param[in]  on  true for on. Default is on.
///
void set_info_tip(CListCtrl& ctrl, bool on); 
	
/// Set whether a list control should contain checkboxes for each item.
///
/// @param[in]  ctrl  The list control
/// @param[in]  on  true for on. Default is off.
///
void set_check_boxes(CListCtrl& ctrl, bool on);

//! Whether a list control displays checkboxes for each item.
auto has_check_boxes(const CListCtrl& ctrl) -> bool;

/// The "header control" embedded in a list control is traditionally assigned a dialogue control ID of zero.
/// This can be a problem if multiple list controls exist in the same dialogue and list header notification 
/// messages are listened to. This function will assign the ID of the list control to its embedded header 
/// control.
///
/// @param[in]  ctrl  The list control
///
void assign_list_id_to_header(CListCtrl& ctrl);

/// Get the number of items a list control.
///
/// @param[in]  ctrl  The list control
/// @return  The item count
///
int count_items(const CListCtrl& ctrl);

/// Add a new item to a list control. The item will generally be added at the end of the list.
///
/// @param[in]  ctrl  The list control
/// @param[in]  text  The item text
/// @return  The index of the new item if successful or  k_no_item  otherwise.
///
int add_item(CListCtrl& ctrl, const std::wstring& text);

/// Add a new item to a list control, and set its data value. The item will generally be added at the end of 
/// the list.
///
/// @tparam  TData  The type of the data value; must be convertible to a numeric type
/// @param[in]  ctrl  The list control
/// @param[in]  text  The item text
/// @param[in]  data  The item data
/// @return  The index of the new item if successful;  k_no_item  otherwise
///
template<typename TData> 
int add_item(CListCtrl& ctrl, const std::wstring& text, TData data);

/// Add a new item to a list control and set the text for the first two subitems, as well as the item's data 
/// value. The item will generally be added at the end of the list.
///
/// @tparam  TData  The type of the data value; must be convertible to a numeric type
/// @param[in]  ctrl  The list control
/// @param[in]  text0  The text for the first subitem, which is equivalent to the item text
/// @param[in]  text1  The text for the second subitem
/// @param[in]  data  The item data
/// @return  The index of the new item if successful;  k_no_item  otherwise
///
template<typename TData> 
int add_item(CListCtrl& ctrl, const std::wstring& text0, const std::wstring& text1, TData data);

/// Get the text of a list control item or subitem.
///
/// @param[in]  ctrl  The list control
/// @param[in]  item_idx  The item index
/// @param[in]  subitem_idx  Index of the subitem, or zero to get the item text 
/// @return  The text
///
std::wstring get_item_text(const CListCtrl& ctrl, int item_idx, int subitem_idx);

/// Change the text of a list control item or subitem.
///
/// @param[in]  ctrl  The list control
/// @param[in]  item_idx  The item index
/// @param[in]  subitem_idx  Index of the subitem, or zero to set the item text 
/// @param[in]  text  The text
///
void set_item_text(CListCtrl& ctrl, int item_idx, int subitem_idx, const std::wstring& text);

/// Add a column to a list control.
///
/// @param[in]  ctrl  The list control
/// @param[in]  name  The column name
/// @param[in]  width_perc  The percentage from the total width available for columns (i.e. the client width 
///					of the list, leaving space for a vertical scrollbar)
/// @return  The index of the new column
///
int add_column(CListCtrl& ctrl, const std::wstring& name, double width_perc);

/// Get the number of columns in a list control.
///
/// @param[in]  ctrl  The list control
/// @return  The column count
///
int count_columns(const CListCtrl& ctrl);

/// Move a list control item to a different position.
///
/// @param[in]  ctrl  The list control
/// @param[in]  old_item_idx  The old item index
/// @param[in]  new_item_idx  The new item index
/// @return  true only on success
///
bool move_item(CListCtrl& ctrl, int old_item_idx, int new_item_idx);

/// Get the data value associated with a specific list control item.
///
/// @tparam  TData  The type of the data value; must be convertible to a numeric type
/// @param[in]  ctrl  The list control
/// @param[in]  item_idx  The item index
/// @return  The data value, or the zero value if the item index is invalid, or item has no associated data
/// 
template<typename TData> 
TData get_item_data(const CListCtrl& ctrl, int item_idx);

/// Set the data value associated with a specific list control item.
///
/// @tparam  TData  The type of the data value; must be convertible to a numeric type
/// @param[in]  ctrl  The list control
/// @param[in]  item_idx  The item index
/// @param[in]  data  The data value
/// 
template<typename TData> 
void set_item_data(CListCtrl& ctrl, int item_idx, TData data);

/// Find the first item in a list control with specific text (starting from a given item).
///
/// @param[in]  ctrl  The list control
/// @param[in]  text  The item text (that is, the string in the first column)
/// @param[in]  start_idx  The index of the item where the search should start (the search will start *after* 
///					this item); pass in k_no_item if you want the search to start from the beginning
/// @return  The index of the first matching item or  k_no_item  if no match is found
///
int find_item_with_text(const CListCtrl& ctrl, const std::wstring& text, int start_idx = k_no_item);

/// Find the first item in a list control with specific data (starting from a given item).
///
/// @tparam  TData  The type of the data value; must be convertible to a numeric type	
/// @param[in]  ctrl  The list control
/// @param[in]  data  The data
/// @param[in]  start_idx  The index of the item where the search should start (the search will start *after* 
///					this item). Pass in  k_no_item  if you want the search to start from the beginning.
/// @return  The index of the first matching item or  k_no_item  if no match is found.
///
template<typename TData> 
int find_item_with_data(const CListCtrl& ctrl, TData data, int start_idx = k_no_item);

/// Delete a specific item from a list control.
///
/// @param[in]  ctrl  The list control
/// @param[in]  item_idx  The item 
/// @return  true if the item was deleted successfully
///
bool delete_item(CListCtrl& ctrl, int item_idx);

/// Look for the first item in a list control with specific data and, if found, delete it.
///
/// @tparam  TData  The type of the data value; must be convertible to a numeric type
/// @param[in]  ctrl  The list control
/// @param[in]  data  The data
/// @return  true if the item was found and deleted
///
template<typename TData> 
bool delete_item_with_data(CListCtrl& ctrl, TData data);

/// Get the index of the first item that is selected in a list control.
///
/// @param[in]  ctrl  The list control
/// @return  The item index, if any is selected,  k_no_item  otherwise
///
int get_first_sel_item(const CListCtrl& ctrl);

/// Get the indexes of all items that are selected in a list control.
///
/// @param[in]  ctrl  The list control
/// @return  The set of indexes
///	
std::set<int> get_sel_items(const CListCtrl& ctrl);

/// Get the data associated with the currently selected item (if any) in a list control.
///
/// @param[in]  ctrl  The list control
/// @return  The data, if an item is currently selected, otherwise the zero value
///
DWORD_PTR get_sel_item_data(const CListCtrl& ctrl);

/// Get the data associated with the currently selected item (if any) in a list control.
///
/// @tparam  TData  The type of the data value; must be convertible to a numeric type
/// @param[in]  ctrl  The list control
/// @return  The data, if an item is currently selected, otherwise the zero value
///
template<typename TData> 
TData get_sel_item_data(const CListCtrl& ctrl);

/// Get the text associated with the currently selected item (if any) or subitem in a list control.
///
/// @param[in]  ctrl  The list control
/// @param[in]  subitem_idx  Index of the subitem, or zero to get the item text 
/// @return  The text, if an item is currently selected, otherwise a blank string
///
std::wstring get_sel_item_text(const CListCtrl& ctrl, int subitem_idx = 0);
	
/*! Select/deselect a specific item in a list control.
    \param[in] ctrl  The list control
	\param[in] item_idx  The item index
	\param[in] select  Whether to select or deselect the item
	\return  Whether the item was (de)selected successfully
 */
auto select_item(CListCtrl& ctrl, int item_idx, bool select) -> bool;
	
/*! Select/deselect a specific set of items in a list control.
    \tparam Rng  Type of range holding the item indexes
    \param[in] ctrl  The list control
	\param[in] item_indexes  The indexes of the items to (de)select
	\param[in] select  Whether to select or deselect the items
	\return  Whether all items were (de)selected successfully
 */
template<class Rng>
requires std::is_convertible_v<rg::range_value_t<Rng>, int>
auto select_items(CListCtrl& ctrl, const Rng& item_indexes, bool select) -> bool;

/// Select and focus, or deselect and unfocus a specific item in a list control.
///
/// @param[in]  ctrl  The list control
/// @param[in]  item_idx  The item index
/// @param[in]  select_and_focus  Whether to select and focus, or deselect and unfocus the item
/// @return  true only on success
///
bool select_and_focus_item(CListCtrl& ctrl, long item_idx, bool select_and_focus);

/// Unselect all selected items (if any) in a list control.
///
/// @param[in]  ctrl  The list control
///
void unselect_all(CListCtrl& ctrl);

/// Count all items that are checked in a list control.
///
/// @param[in]  ctrl  The list control
/// @return  The number of checked items
///	
size_t count_checked_items(const CListCtrl& ctrl);

/// Get the indexes of all items that have a specific check state in a list control.
///
/// @param[in]  ctrl  The list control
/// @param[in]  checked  Whether to look for items which are checked (true) or unchecked
/// @return  The set of indexes
///	
std::set<int> get_items_by_check(const CListCtrl& ctrl, bool checked);

/// Get the check state of a specific item in a list control with checkboxes.
///
/// @param[in]  ctrl  The list control
/// @param[in]  item_idx  The item index
/// @return  true if the item is checked, or false if the item is unchecked, or the item index is invalid
///
bool get_item_check(const CListCtrl& ctrl, int item_idx);

/// Set the check state of a specific item in a list control with checkboxes.
///
/// @param[in]  ctrl  The list control
/// @param[in]  item_idx  The item index
/// @param[in]  checked  Whether the item should become checked (true) or unchecked
///
void set_item_check(CListCtrl& ctrl, int item_idx, bool checked);

/// Set the check state of all items in a list control with checkboxes.
///
/// @param[in]  ctrl  The list control
/// @param[in]  checked  Whether the items should become checked (true) or un-checked
///
void set_all_items_check(CListCtrl& ctrl, bool checked);

/*! Whether the location \p location in client coordinates (typically an item event location) falls over an item's 
    checkbox in the list control \p ctrl.
 */
auto is_location_over_item_checkbox(const CListCtrl& ctrl, CPoint location) -> bool;

/// Determines, in a list control, the minimum column width necessary to display the strings for all the items currently in 
///   that column.
/// The returned width takes into account the control's current font and column margins, but not the width of a small icon.
/// This function has only been tested with the "report" list style.
///
/// @param[in]  ctrl  The list control
/// @param[in]  col_idx  The column index
/// @return  The maximum width (zero if there are no strings in the column)
///
int get_max_text_width(const CListCtrl& ctrl, int col_idx);

/// Get the bounding rectangle of the text area of a list control subitem.
/// NOTE: This function has only been tested for a list using the "report" view and without groups.
///
/// @param[in]  ctrl  The list control
/// @param[in]  item_idx  The item index
/// @param[in]  col_idx  Index of the column (zero to set the item text) 
/// @return  The visible part of the bounding rectangle of the text area
///
CRect get_subitem_rect(const CListCtrl& ctrl, int item_idx, int col_idx);

/// Determine which list control subitem, if any, is at a specified position.
///
/// @param[in]  ctrl  The list control
/// @param[in]  pt  The position, relative to the list control's client area
/// @param[out]  item_idx  The item index, or -1 if the position is not a hit
/// @param[in]  subitem_idx  Index of the subitem, or -1 if the position is not a hit
///
void hit_test(const CListCtrl& ctrl, const POINT& pt, int& item_idx, int& subitem_idx);

/// Enables or disables whether the items in a list view control display as a group.
///
/// @param[in]  ctrl  The list control
/// @param[in]  enable  Whether the group view should become enabled (true) or disabled
/// 
void enable_group_view(CListCtrl& ctrl, bool enable);

/// Insert a group into the list control.
///
/// @param[in]  ctrl  The list control
/// @param[in]  group_id  The ID to be assigned to the new group
/// @param[in]  header  The group header
/// @param[in]  header_align  The alignment of the group header
/// @return  The index of the item that the group was added to, or  k_no_item  if the operation failed
///
int insert_group(CListCtrl& ctrl, int group_id, const std::wstring& header, int header_align = LVGA_HEADER_LEFT);

/// Add a number of list control items to an existing list group.
///
/// @param[in]  ctrl  The list control
/// @param[in]  group_id  The group ID
/// @param[in]  item_indexes  The indexes of the items to be added to the group
///
void add_items_to_group(CListCtrl& ctrl, int group_id, const std::set<int>& item_indexes);

/* Sort the items in a list control using a callable which compares the data values associated with two items.
   \tparam CompareData  Type of data value comparison callable; must return a negative value if the item with the first 
                        data value should precede the item with the second data value, a positive value if in the 
						reverse case, or zero if the two items are equivalent 
   \param[in] ctrl  The list control
   \param[in] compare_items  The data value comparison callable 
   \return  true if successful
*/
template<class CompareData>
requires std::is_invocable_r_v<int, CompareData, LPARAM, LPARAM> 
auto sort_items(CListCtrl& ctrl, CompareData compare_data) -> bool;

/* Sort the items in a list control whose all items' data are valid pointers to values of the same type, using a 
   callable which compares two data objects.
   \tparam ItemData  The item data object type
   \tparam CompareData  Type of data object comparison callable; must return a negative value if the item with the 
                        first data object should precede the item with the second data object, a positive value if in the 
						reverse case, or zero if the two items are equivalent 
   \param[in] ctrl  The list control
   \param[in] compare_data  The data value comparison callable 
   \return  true if successful
*/
template<class ItemData, class CompareData>
requires std::is_invocable_r_v<int, CompareData, const ItemData&, const ItemData&> 
auto sort_items_with_pointer_data(CListCtrl& ctrl, CompareData compare_data) -> bool;

/*! Check whether a list view/control notification message was triggered by a selection change event, is whether the 
    selection state of one (or more) of the list's items has changed.
    \param[in] msg_data  The data associated with the notification message
    \return  true if the message was triggered by a selection change event
 */
auto is_item_selection_event(const NMLISTVIEW* msg_data) -> bool;

/*! Check whether a list view/control notification message was triggered by a check-box state change event, is whether 
    the "checked" state of one of the list's items has changed.
    \param[in] msg_data  The data associated with the notification message
    \return  true if the message was triggered by a check-box state change event
 */
auto is_item_check_event(const NMLISTVIEW* msg_data) -> bool;

/*! For a list view/control notification message with associated data \p msg_data, get the location at which the 
    trigger event occurred, in client coordinates. The returned value is undefined for notification codes that do not 
	use it.
*/
auto location(const NMITEMACTIVATE* msg_data) -> CPoint;

}

#include "list_ctrl_utils.ipp"

#endif 

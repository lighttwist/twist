///  @file  PolyEntityChildDlgList.ipp
///  Inline implementation file for "PolyEntityChildDlgList.hpp"

//   Copyright (c) 2010-2019 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

namespace twist::mfc {

template<class Entity, typename EntityType>
PolyEntityChildDlgList<Entity, EntityType>::PolyEntityChildDlgList(CWnd& parent) 
	: m_dlgList(parent)
{
	TWIST_CHECK_INVARIANT
}


template<class Entity, typename EntityType>
PolyEntityChildDlgList<Entity, EntityType>::~PolyEntityChildDlgList()
{
	TWIST_CHECK_INVARIANT
}


template<class Entity, typename EntityType>
void PolyEntityChildDlgList<Entity, EntityType>::setEntity(const Entity& entity, EntityType entityType)
{
	TWIST_CHECK_INVARIANT
	ChildDlg& dlg = dynamic_cast<ChildDlg&>(m_dlgList.get(entityType));
	dlg.setEntity(entity);
	TWIST_CHECK_INVARIANT
}


template<class Entity, typename EntityType>
void PolyEntityChildDlgList<Entity, EntityType>::moveAll(const CRect& dlgRect)
{
	TWIST_CHECK_INVARIANT
	m_dlgList.moveAll(dlgRect);	
	TWIST_CHECK_INVARIANT
}


template<class Entity, typename EntityType>
typename PolyEntityChildDlgList<Entity, EntityType>::ChildDlg& 
PolyEntityChildDlgList<Entity, EntityType>::getActive()
{
	TWIST_CHECK_INVARIANT
	return dynamic_cast<ChildDlg&>(m_dlgList.getActive());
}


template<class Entity, typename EntityType>
void PolyEntityChildDlgList<Entity, EntityType>::setActive(EntityType entityType)
{
	TWIST_CHECK_INVARIANT
	m_dlgList.setActive(entityType);
	TWIST_CHECK_INVARIANT
}


template<class Entity, class EntityType> template<class ChildDlgClass, class> 
ChildDlgClass& PolyEntityChildDlgList<Entity, EntityType>::add(EntityType entityType, long resId, 
		typename ChildDlg::ChangedListener changedListener, typename ChildDlg::ValidateListener validateListener)
{
	TWIST_CHECK_INVARIANT
	ChildDlg& dlg = m_dlgList.add<ChildDlgClass>(entityType, resId);	
	dlg.init(changedListener, validateListener);

	TWIST_CHECK_INVARIANT
	return static_cast<ChildDlgClass&>(dlg);
}


#ifdef _DEBUG
template<class Entity, typename EntityType>
void PolyEntityChildDlgList<Entity, EntityType>::check_invariant() const noexcept
{
}
#endif 

}





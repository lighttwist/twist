//  Utilities for working with button controls, including checkboxes and radio buttons
//
//  Copyright (c) 2007-2022, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_MFC_BUTTON__UTILS_HPP
#define TWIST_MFC_BUTTON__UTILS_HPP

namespace twist::mfc {

/*! Manager for a group of radio buttons, which associates a distinct value of a given option type (eg an enum type) to 
    each radio button.
	\tparam Option  The option type
 */
template<class Option>
class RadioButtonGroup {
public:
	/*! Constructor. 
	    \note  Only call the constructor once the button windows have been created.
		\param[in] ctrl_id_options  The controls IDs of all buttons in the radio buton group, paired with their 
		                            corresponding option values. The control IDs should be ordered as they are in the 
									group (matching the "tab order"). The first control in the group must have the 
									"group" window style. No control ID and no option value can appear twice in the 
									list.
		\param[in] parent_wnd  The button controls parent window (typically a dialog window)
	 */
	explicit RadioButtonGroup(std::vector<std::tuple<int, Option>> ctrl_id_options, CWnd& parent_wnd);

	//! Get the option corresponding to the currently checked radio button, or std::nullopt if no button is checked.
	[[nodiscard]] auto get_checked_option() const -> std::optional<Option>;

	//! Check the radio button corresponding to the option value \p option.
	auto check_option(Option option) -> void;

private:
	[[nodiscard]] auto get_option(int ctrl_id) const -> Option;

	[[nodiscard]] auto get_ctrl_id(Option option) const -> int;

	std::vector<std::tuple<int, Option>> ctrl_id_options_;
	CWnd& parent_wnd_;

	TWIST_CHECK_INVARIANT_DECL
};

// --- Free functions ---

/*! Find out whether a check box control, or a radio button control (both of which are types of button) is 
    checked, that is, whether it has the BST_CHECKED state.
    \param[in] button  The check box/radio button control
    \return  true if it is checked
 */
[[nodiscard]] auto is_checked(const CButton& button) -> bool;

/*! Set the "checked" state of a check box control, or a radio button control (both of which are types of button) to 
    either BST_CHECKED or BST_UNCHECKED.
    \param[in] button  The check box/radio button control
    \param[in] checked  Whether the state should be set to BST_CHECKED (true) or BST_UNCHECKED
 */
auto set_checked(CButton& button, bool checked) -> void;

} 

#include "button_utils.ipp"

#endif 

///  @file  CtrlLayoutMgr.hpp
///  CtrlLayoutMgr class defintion and declarations of the free functions which are part of its interface

//   Copyright (c) 2007-2019 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_MFC_CTRL_LAYOUT_MGR_HPP
#define TWIST_MFC_CTRL_LAYOUT_MGR_HPP

#include "twist/OptionSet.hpp"

namespace twist::mfc {

///
///  Manager for the layout of control windows on a parent window.
///  The class deals mainly with re-arranging the controls when the parent window is resized, but also 
///    provides other related services.
///
class CtrlLayoutMgr : public NonCopyable {
public:
	/// Control anchor types
	enum class Anchor {
		k_left    = 1, ///< Control is anchored to the left side of the parent window
		k_top     = 2, ///< Control is anchored to the top side of the parent window
		k_right   = 3, ///< Control is anchored to the right side of the parent window
		k_bottom  = 4  ///< Control is anchored to the bottom side of the parent window
	};

	typedef OptionSet<Anchor>  Anchors;
	
	/// Constructor.
	/// It should be called once the parent window has been properly created (eg from InitInstance(), if the 
	/// parent window is a dialog).
	///
	/// @param[in]  parent_hwnd  Handle to the parent window.
	///
	CtrlLayoutMgr(HWND parent_hwnd);
	
	/// Destructor.
	///
	virtual ~CtrlLayoutMgr();

	/// Register a control with the manager, with its anchors.
	///
	/// @param[in]  ctrl_id  The control ID.
	/// @param[in]  anchors  The set of anchors for that control.
	///
	void add_ctrl(int ctrl_id, const Anchors& anchors);

	/// Re-arrange the controls depending of the parent window's current size.
	/// Call this from the parent window's WM_SIZE handler, after relaying the message to the parent.
	///
	void arrange_ctrls();

	/// Set the minimum size of the parent window. If you call this function, you should then call 
	/// OnParentMinMax() at the correct time (see the comment for that method).
	///
	/// @param[in]  min_width  The minimum width of the parent window.
	/// @param[in]  min_height  The minimum height of the parent window.
	///
	void set_min_size(int min_width, int min_height);
	
	/// Set the maximum size of the parent window. If you call this function, you should then call 
	/// OnParentMinMax() at the correct time (see the comment for that method).
	///
	/// @param[in]  max_width  The maximum width of the parent window.
	/// @param[in]  max_height  The maximum height of the parent window.
	///
	void set_max_size(int max_width, int max_height);

	/// If you have previously set the minimum and/or maximum size of the parent window, call this method from 
	/// the parent window's WM_GETMINMAXINFO handler, before relaying the message to the parent.
	/// 
	/// @param[in]  min_max_info  [in/out] The associated Windows message structure.
	///
	void on_parent_min_max(MINMAXINFO* min_max_info) const;

	/// Set a flag specifying whether the parent window should be entirely invalidated at the end of each call
	/// to ArrangeCtrls(). This is usually not necessary, and can produce flickering but in some cases it must
	/// be done. Default is off.
	///  
	/// @param[in]  erase  The flag value.
	///
	void set_erase_parent(bool erase);

private:	
	typedef std::map<int, Anchors>  CtrlAnchorsMap;
	typedef std::map<long, CRect>  CtrlRectMap;

	const HWND  parent_hwnd_;
	int  min_width_;   // The parent window's minimum width. Zero if not set.
	int  min_height_;  // The parent window's minimum height. Zero if not set.
	int  max_width_;   // The parent window's maximum width. Zero if not set. 
	int  max_height_;  // The parent window's maximum height. Zero if not set.
	
	// Map storing the set of anchors for each control registered with the manager.
	CtrlAnchorsMap  ctrl_anchors_;
	
	// The size that the parent window had when this object was constructed.
	CSize  orig_parent_size_;

	// Map storing the size that all controls belonging to the parent window had when this manager object 
	// was created
	CtrlRectMap  orig_ctrl_rects_;

	bool  erase_parent_;

	TWIST_CHECK_INVARIANT_DECL
};

//
//  Free functions
//

/// Register a control with a layout manager, with top and left anchors.
///
/// @param[in]  layout_mgr  The layout manager 
/// @param[in]  ctrl_id  The control ID
///
void add_ctrl_tl(CtrlLayoutMgr& layout_mgr, int ctrl_id);

/// Register a number of controls with a layout manager, with top and left anchors.
///
/// @param[in]  layout_mgr  The layout manager 
/// @param[in]  ctrln_id  The n-th control ID.
///
void add_ctrls_tl(CtrlLayoutMgr& layout_mgr, int ctrl1_id, int ctrl2_id);

/// Register a number of controls with a layout manager, with top and left anchors.
///
/// @param[in]  layout_mgr  The layout manager 
/// @param[in]  ctrln_id  The n-th control ID.
///
void add_ctrls_tl(CtrlLayoutMgr& layout_mgr, int ctrl1_id, int ctrl2_id, int ctrl3_id);
	
/// Register a control with a layout manager, with top and right anchors.
///
/// @param[in]  layout_mgr  The layout manager 
/// @param[in]  ctrl_id  The control ID.
///
void add_ctrl_tr(CtrlLayoutMgr& layout_mgr, int ctrl_id);

/// Register a number of controls with a layout manager, with top and right anchors.
///
/// @param[in]  layout_mgr  The layout manager 
/// @param[in]  ctrln_id  The n-th control ID.
///
void add_ctrls_tr(CtrlLayoutMgr& layout_mgr, int ctrl1_id, int ctrl2_id);

/// Register a number of controls with a layout manager, with top and right anchors.
///
/// @param[in]  layout_mgr  The layout manager 
/// @param[in]  ctrln_id  The n-th control ID.
///
void add_ctrls_tr(CtrlLayoutMgr& layout_mgr, int ctrl1_id, int ctrl2_id, int ctrl3_id);

/// Register a number of controls with a layout manager, with top and right anchors.
///
/// @param[in]  layout_mgr  The layout manager 
/// @param[in]  ctrln_id  The n-th control ID.
///
void add_ctrls_tr(CtrlLayoutMgr& layout_mgr, int ctrl1_id, int ctrl2_id, int ctrl3_id, int ctrl4_id);

/// Register a number of controls with a layout manager, with top and right anchors.
///
/// @param[in]  layout_mgr  The layout manager 
/// @param[in]  ctrln_id  The n-th control ID.
///
void add_ctrls_tr(CtrlLayoutMgr& layout_mgr, int ctrl1_id, int ctrl2_id, int ctrl3_id, int ctrl4_id, 
		int ctrl5_id);

/// Register a number of controls with a layout manager, with top and right anchors.
///
/// @param[in]  layout_mgr  The layout manager 
/// @param[in]  ctrln_id  The n-th control ID.
///
void add_ctrls_tr(CtrlLayoutMgr& layout_mgr, int ctrl1_id, int ctrl2_id, int ctrl3_id, int ctrl4_id, 
		int ctrl5_id, int ctrl6_id);

/// Register a number of controls with a layout manager, with top and right anchors.
///
/// @param[in]  layout_mgr  The layout manager 
/// @param[in]  ctrln_id  The n-th control ID.
///
void add_ctrls_tr(CtrlLayoutMgr& layout_mgr, int ctrl1_id, int ctrl2_id, int ctrl3_id, int ctrl4_id, 
		int ctrl5_id, int ctrl6_id, int ctrl7_id);

/// Register a control with a layout manager, with top, left and right anchors.
///
/// @param[in]  layout_mgr  The layout manager 
/// @param[in]  ctrl_id  The control ID.
///
void add_ctrl_tlr(CtrlLayoutMgr& layout_mgr, int ctrl_id);

/// Register a number of controls with a layout manager, with top, left and right anchors.
///
/// @param[in]  layout_mgr  The layout manager 
/// @param[in]  ctrln_id  The n-th control ID.
///
void add_ctrls_tlr(CtrlLayoutMgr& layout_mgr, int ctrl1_id, int ctrl2_id);

/// Register a number of controls with a layout manager, with top, left and right anchors.
///
/// @param[in]  layout_mgr  The layout manager 
/// @param[in]  ctrln_id  The n-th control ID.
///
void add_ctrls_tlr(CtrlLayoutMgr& layout_mgr, int ctrl1_id, int ctrl2_id, int ctrl3_id);

/// Register a control with a layout manager, with left, bottom and right anchors.
///
/// @param[in]  layout_mgr  The layout manager 
/// @param[in]  ctrl_id  The control ID.
///
void add_ctrl_lbr(CtrlLayoutMgr& layout_mgr, int ctrl_id);

/// Register a number of controls with a layout manager, with left, bottom and right anchors.
///
/// @param[in]  layout_mgr  The layout manager 
/// @param[in]  ctrln_id  The n-th control ID.
///
void add_ctrls_lbr(CtrlLayoutMgr& layout_mgr, int ctrlId1, int ctrlId2);

/// Register a number of controls with a layout manager, with left, bottom and right anchors.
///
/// @param[in]  layout_mgr  The layout manager 
/// @param[in]  ctrln_id  The n-th control ID.
///
void add_ctrls_lbr(CtrlLayoutMgr& layout_mgr, int ctrlId1, int ctrlId2, int ctrlId3);

/// Register a control with a layout manager, with bottom and left anchors.
///
/// @param[in]  layout_mgr  The layout manager 
/// @param[in]  ctrl_id  The control ID.
///
void add_ctrl_bl(CtrlLayoutMgr& layout_mgr, int ctrl_id);

/// Register a number of controls with a layout manager, with bottom and left anchors.
///
/// @param[in]  layout_mgr  The layout manager 
/// @param[in]  ctrln_id  The n-th control ID.
///
void add_ctrls_bl(CtrlLayoutMgr& layout_mgr, int ctrl1_id, int ctrl2_id);

/// Register a number of controls with a layout manager, with bottom and left anchors.
///
/// @param[in]  layout_mgr  The layout manager 
/// @param[in]  ctrln_id  The n-th control ID.
///
void add_ctrls_bl(CtrlLayoutMgr& layout_mgr, int ctrl1_id, int ctrl2_id, int ctrl3_id);

/// Register a number of controls with a layout manager, with bottom and left anchors.
///
/// @param[in]  layout_mgr  The layout manager 
/// @param[in]  ctrln_id  The n-th control ID.
///
void add_ctrls_bl(CtrlLayoutMgr& layout_mgr, int ctrl1_id, int ctrl2_id, int ctrl3_id, int ctrl4_id);

/// Register a number of controls with a layout manager, with bottom and left anchors.
///
/// @param[in]  layout_mgr  The layout manager 
/// @param[in]  ctrln_id  The n-th control ID.
///
void add_ctrls_bl(CtrlLayoutMgr& layout_mgr, int ctrl1_id, int ctrl2_id, int ctrl3_id, int ctrl4_id, 
		int ctrl5_id);

/// Register a number of controls with a layout manager, with bottom and left anchors.
///
/// @param[in]  layout_mgr  The layout manager 
/// @param[in]  ctrln_id  The n-th control ID.
///
void add_ctrls_bl(CtrlLayoutMgr& layout_mgr, int ctrl1_id, int ctrl2_id, int ctrl3_id, int ctrl4_id, 
		int ctrl5_id, int ctrl6_id);

/// Register a number of controls with a layout manager, with bottom and left anchors.
///
/// @param[in]  layout_mgr  The layout manager 
/// @param[in]  ctrln_id  The n-th control ID.
///
void add_ctrls_bl(CtrlLayoutMgr& layout_mgr, int ctrl1_id, int ctrl2_id, int ctrl3_id, int ctrl4_id, 
		int ctrl5_id, int ctrl6_id, int ctrl7_id);

/// Register a control with a layout manager, with bottom and right anchors.
///
/// @param[in]  layout_mgr  The layout manager 
/// @param[in]  ctrl_id  The control ID.
///
void add_ctrl_br(CtrlLayoutMgr& layout_mgr, int ctrl_id);

/// Register a number of controls with a layout manager, with bottom and right anchors.
///
/// @param[in]  layout_mgr  The layout manager 
/// @param[in]  ctrln_id  The n-th control ID.
///
void add_ctrls_br(CtrlLayoutMgr& layout_mgr, int ctrl1_id, int ctrl2_id);

/// Register a number of controls with a layout manager, with bottom and right anchors.
///
/// @param[in]  layout_mgr  The layout manager 
/// @param[in]  ctrln_id  The n-th control ID.
///
void add_ctrls_br(CtrlLayoutMgr& layout_mgr, int ctrl1_id, int ctrl2_id, int ctrl3_id);

/// Register a number of controls with a layout manager, with bottom and right anchors.
///
/// @param[in]  layout_mgr  The layout manager 
/// @param[in]  ctrln_id  The n-th control ID.
///
void add_ctrls_br(CtrlLayoutMgr& layout_mgr, int ctrl1_id, int ctrl2_id, int ctrl3_id, int ctrl4_id);

/// Register a number of controls with a layout manager, with bottom and right anchors.
///
/// @param[in]  layout_mgr  The layout manager 
/// @param[in]  ctrln_id  The n-th control ID.
///
void add_ctrls_br(CtrlLayoutMgr& layout_mgr, int ctrl1_id, int ctrl2_id, int ctrl3_id, int ctrl4_id, 
		int ctrl5_id);

/// Register a number of controls with a layout manager, with bottom and right anchors.
///
/// @param[in]  layout_mgr  The layout manager 
/// @param[in]  ctrln_id  The n-th control ID.
///
void add_ctrls_br(CtrlLayoutMgr& layout_mgr, int ctrl1_id, int ctrl2_id, int ctrl3_id, int ctrl4_id, 
		int ctrl5_id, int ctrl6_id);

/// Register a control with a layout manager, with top, left and bottom anchors.
///
/// @param[in]  layout_mgr  The layout manager 
/// @param[in]  ctrl_id  The control ID.
///
void add_ctrl_tlb(CtrlLayoutMgr& layout_mgr, int ctrl_id);

/// Register a number of controls with a layout manager, with top, left and bottom anchors.
///
/// @param[in]  layout_mgr  The layout manager 
/// @param[in]  ctrln_id  The n-th control ID.
///
void add_ctrls_tlb(CtrlLayoutMgr& layout_mgr, int ctrl1_id, int ctrl2_id);

/// Register a control with a layout manager, with top, bottom and right anchors.
///
/// @param[in]  layout_mgr  The layout manager 
/// @param[in]  ctrl_id  The control ID.
///
void add_ctrl_tbr(CtrlLayoutMgr& layout_mgr, int ctrl_id);

/// Register a number of controls with a layout manager, with top, bottom and right anchors.
///
/// @param[in]  layout_mgr  The layout manager 
/// @param[in]  ctrln_id  The n-th control ID.
///
void add_ctrls_tbr(CtrlLayoutMgr& layout_mgr, int ctrl1_id, int ctrl2_id);

/// Register a control with a layout manager, with top, left, bottom and right anchors.
///
/// @param[in]  layout_mgr  The layout manager 
/// @param[in]  ctrl_id  The control ID.
///
void add_ctrl_tlbr(CtrlLayoutMgr& layout_mgr, int ctrl_id);

/// Register a number of controls with a layout manager, with top, left, bottom and right anchors.
///
/// @param[in]  layout_mgr  The layout manager 
/// @param[in]  ctrln_id  The n-th control ID.
///
void add_ctrls_tlbr(CtrlLayoutMgr& layout_mgr, int ctrl1_id, int ctrl2_id);

/// Register a number of controls with a layout manager, with top, left, bottom and right anchors.
///
/// @param[in]  layout_mgr  The layout manager 
/// @param[in]  ctrln_id  The n-th control ID.
///
void add_ctrls_tlbr(CtrlLayoutMgr& layout_mgr, int ctrl1_id, int ctrl2_id, int ctrl3_id);

}

#endif

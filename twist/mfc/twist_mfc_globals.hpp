///  @file  twist_mfc_globals.hpp
///  Globals for the "twist::mfc" library

//   Copyright (c) 2010-2019 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_MFC_TWIST__MFC__GLOBALS_HPP
#define TWIST_MFC_TWIST__MFC__GLOBALS_HPP

namespace twist::mfc {

// Invalid control item index
const int  k_no_item  = -1;

/// Convert an MFC string to a raw C-string.
///
/// @param  str  [in] The MFC string object
/// @return  The C-string
///
const wchar_t* to_cstr(const CString& str);

/// Convert a CString object to a std::wstring object.
///
/// @param[in]  str  The input string object
/// @return  The converted string object
///
std::wstring to_string(const CString& str);

/// Convert an MFC string to a filesystem path.
///
/// @param  str  [in] The MFC string object
/// @return  The filesystem path
///
fs::path to_path(const CString& str);

/// Convert a standard string to an MFC string.
///
/// @param  str  [in] The standard string object
/// @return  The MFC string object
///
CString to_mfc(const std::wstring& str);

/// Convert a standard string view to an MFC string.
///
/// @param  str  [in] The standard string object
/// @return  The MFC string object
///
CString to_mfc(std::wstring_view str);

}

#endif 


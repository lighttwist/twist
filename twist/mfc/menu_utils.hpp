///  @file  menu_utils.hpp
///  Miscellaneous utilities for working with Windows menus.

//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_MFC_MENU__UTILS_HPP
#define TWIST_MFC_MENU__UTILS_HPP

namespace twist::mfc {

/// Display a popup (aka context) menu.
///
/// @param[in]  res_id  The resource ID of the menu to be displayed; it must have been registered with the 
///					context menu manager 
/// @param[in]  point  The point where the menu will be displayed, in screen coordinates 
/// @param[in]  parent  The menu's parent window
/// @param[in]  own_msg  Flag which indicates how messages are routed: if false, standard MFC routing is used; 
///					otherwise, the parent window receives the messages
/// @param[in]  right_align  Whether the menu items should be right-aligned
///
void show_popup_menu(unsigned int res_id, const CPoint& point, CWnd& parent, bool own_msg = true, 
		bool right_align = false);

/// Enable or disable the user-interface item (usually, a menu item) which generates a command.
///
/// @param[in]  cmd_ui  Object representing the menu item or toolbar button or other user-interface object 
///					that generates the command
/// @param[in]  enable  Whether the UI item should be enabled (true) or disabled
///
void enable_cmd_ui(CCmdUI* cmd_ui, bool enable);

/// Enable or disable the user-interface item (usually, a menu item) which generates a command.
///
/// @param[in]  cmd_ui  Object representing the menu item or toolbar button or other user-interface object 
///					that generates the command
/// @param[in]  enable  Whether the UI item should be enabled (true) or disabled
///
void enable_cmd_ui(CCmdUI* cmd_ui, bool enable);

/// Find the menu item with a specific string within a menu. The function goes through all items in the menu, 
/// and for each item which displays a string (ie whose type is MFT_STRING) it checks whether the associate 
/// string matches the string passed in. The matching is case sensitive.  
///
/// @param[in]  menu  The menu handle
/// @param[in]  item_string  The item string to search for
/// @return  the position of the first matching item, or  k_no_item  if no match is found
///
int find_menu_item_pos(HMENU menu, const std::wstring& item_string);  //+ Does not use MFC

/// Get a handle to the submenu activated by the menu item with a specific string within a menu.
///
/// @param[in]  menu  The menu handle
/// @param[in]  item_string  The item string to search for
/// @return  the handle of the first matching item, or  nullptr  if no match is found (or the item with a 
///					matching string does not activate a submenu)
///
HMENU get_submenu(HMENU menu, const std::wstring& item_string);  //+ Does not use MFC

/// Find out whether the menu item at a specific position in a menu is a separator.
///
/// @param[in]  menu  The menu handle
/// @param[in]  item_pos  The item position
/// @return  true if the item is a separator
///
bool is_menu_item_separator(HMENU menu, UINT item_pos);  //+ Does not use MFC

/// Delete a specific menu item from a menu. The function goes through all items in the menu, and for each 
/// item which displays a string (ie whose type is MFT_STRING) it checks whether the associate string matches 
/// the string passed in. The matching is case sensitive. If the menu item has an associated pop-up menu, the 
/// handle to the pop-up menu is destroyed and the memory used by the pop-up menu is freed. 
///
/// @param[in]  hmenu  The menu handle
/// @param[in]  item_cmd_id  The item command ID
/// @return  true if the item was deleted successfully
///
bool delete_menu_item_by_cmd_id(HMENU hmenu, UINT item_cmd_id);  //+ Does not use MFC

/// Delete a specific menu item from a menu. The menu item is identified by its position within the menu. If 
/// the menu item has an associated pop-up menu, the handle to the pop-up menu is destroyed and the memory 
/// used by the pop-up menu is freed. 
///
/// @param[in]  hmenu  The menu handle
/// @param[in]  item_pos  The item position
/// @return  true if the item was deleted successfully
///
bool delete_menu_item_by_pos(HMENU hmenu, UINT item_pos);  //+ Does not use MFC

/// Delete a specific menu item from a menu. The function goes through all items in the menu, and for each 
/// item which displays a string (ie whose type is MFT_STRING) it checks whether the associate string matches 
/// the string passed in. The matching is case sensitive. The first matching item (if any) is deleted. If the 
/// deleted menu item has an associated pop-up menu, the handle to the pop-up menu is destroyed and the memory 
/// used by the pop-up menu is freed. 
///
/// @param[in]  hmenu  The menu handle
/// @param[in]  item_string  The item string to search for
/// @return  true if the item was deleted successfully
///
bool delete_menu_item_by_string(HMENU hmenu, const std::wstring& item_string);  //+ Does not use MFC

/// Display a dynamically created popup menu containing only items of type "string".
///
/// @param[in]  menu_items  List containing the item command ID and item text for each item in the menu; the 
///					items will appear in the menu in the same order they appear in the list
/// @param[in]  parent_wnd  Handle to the window which will be the parent of the popup menu; this window will 
///					receive the menu item command messages WM_COMMAND and CN_UPDATE_COMMAND_UI
/// @param[in]  location  The location where the popup menu will be displayed (screen coordinates)
///
void show_popup_menu(const std::vector<std::pair<UINT, std::wstring>>& menu_items, HWND parent_wnd, 
		const POINT& location);  //+ Does not use MFC

/// Given a list control with checkboxes, show a context menu given the user the following options, and commit 
/// the selected action, if any:
///  *  Check all selected items
///  *  Un-check all selected items
///  *  Check all items
///  *  Un-check all items
/// Only the viable options are added to the menu.
/// 
/// @param[in]  ctrl  The list control
/// @param[in]  point  The point where the context menu will be shown (control client coordinates)
///
void show_multi_check_context_menu(CListCtrl& ctrl, CPoint point);

}

#endif

///  @file  wnd_utils.hpp
///  Miscellaneous utilities for working with generic windows

//   Copyright (c) 2010-2019 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_MFC_WND__UTILS_HPP
#define TWIST_MFC_WND__UTILS_HPP

namespace twist::mfc {

/*! Class which prevents changes to a window's contents to be redrawn, and optionally changes the mouse cursor to the 
    "wait cursor", for the lifetime of an instance.
 */
class RedrawFreezer : NonCopyable {
public:
	/// Constructor.
	///
	/// @param[in]  wnd  The window
	/// @param[in]  do_wait_cursor  Whether the mouse cursor should changed to the "wait cursor"
	/// 
	RedrawFreezer(CWnd& wnd, bool do_wait_cursor = true);

	/// Destructor.
	///
	~RedrawFreezer();

private:
	CWnd&  wnd_;
	std::unique_ptr<CWaitCursor>  wait_cursor_;
};

/// Get the window handle for a window. 
///
/// @param[in]  wnd  The window object
/// @return  The window handle; or nullptr if the window objct is not attached to a window 
///
HWND hwnd(const CWnd& wnd);

/// Enable/disable a specific window.
///
/// @param[in]  wnd  The window
/// @param[in]  enable  Whether the window should be enabled (true) or disabled
///
void enable_wnd(CWnd& wnd, bool enable);

/// Find out whether a specific window is enabled for mouse and keyboard input.
///
/// @param[in]  wnd  The window
/// @return  true if it is
///
bool is_wnd_enabled(const CWnd& wnd);

/// Get the bounding rectangle of the specified window. 
///
/// @param[in]  wnd  The window
/// @return  The window's bounding rectangle (screen coordinates)
///
CRect get_wnd_rect(const CWnd& wnd);

/// Get a specific window's client rectangle.
///
/// @param[in]  wnd  The window
/// @return  The client rectangle
///
CRect get_client_rect(const CWnd& wnd);

/// Convert a point from screen coordinates to the client coordinates of a specific window.
///
/// @param[in]  point  The point
/// @param[in]  wnd  The window
/// @return  The converted point
///
CPoint screen_to_client(const CPoint& point, const CWnd& wnd);

/// Convert the screen coordinates of a specified rectangle on the screen to client-area coordinates. 
///
/// @param[in]  rect  [in,out] The rectangle.
/// @param[in]  wnd  The window
///
CRect screen_to_client(const CRect& rect, const CWnd& wnd);

/// Convert the screen coordinates of a specified rectangle on the screen to client-area coordinates. 
///
/// @param[in]  rect  [in,out] The rectangle
/// @param[in]  hwnd  The window handle
///
CRect screen_to_client(const CRect& rect, HWND hwnd);

/// Convert a point from the client coordinates of a specific window to screen coordinates.
///
/// @param[in]  point  The point
/// @param[in]  wnd  The window
/// @return  The converted point
///
CPoint client_to_screen(const CPoint& point, const CWnd& wnd);

/// Convert a rectangle from the client coordinates of a specific window to screen coordinates.
///
/// @param[in]  rect  The point
/// @param[in]  wnd  The window
/// @return  The converted rectangle
///
CRect client_to_screen(const CRect& rect, const CWnd& wnd);

/// Hide a window (make it invisible).
///
/// @param[in]  wnd  The window
/// @return  true if the window was previously visible
///
bool hide_wnd(CWnd& wnd);

/// Change the position (and dimensions) of a window.
///
/// @param[in]  wnd  The window
/// @param[in]  rect  The new window rectangle (in screen coordinates for a top-level window, or client coordinates of the 
///				parent for a child window)
/// @param[in]  repaint  Whether the window is to be repainted 
///
void move_wnd(CWnd& wnd, const CRect& rect, bool repaint = true);

/// Retrieve the mouse cursor position for the last message retrieved by the  GetMessage()  Win API function, relative to the 
/// client area of a specific window.
///
/// @param[in]  wnd  The window
/// @return  The mouse position
///
CPoint get_last_message_mouse_pos(const CWnd& wnd); 

/// Retrieve the mouse cursor position for the last message retrieved by the GetMessage() Win API function, in 
/// screen coordinates.
///
/// @return  The mouse position
///
CPoint get_last_message_mouse_pos(); 

/// Process all pending Windows messages for a window.
///
/// @param[in]  hwnd  Handle to the window whose messages are to be retrieved. The window must belong to the 
///					current thread. 
///
void pump_window_messages(HWND hwnd);

/// Get a window's size (width and height).
///
/// @param[in]  hwnd  Handle to the window.
/// @return  The window size.
///
CSize get_wnd_size(HWND hwnd);

/// Get the text of a window's title bar (if it has one). If the window is a control, the text of the control is
/// obtained. However, this function cannot retrieve the text of a control in another application.
///
/// @param[in]  wnd  The window.
/// @return  The window text.
///
std::wstring get_wnd_text(const CWnd& wnd);

/// Get the text of a window's title bar (if it has one). If the window is a control, the text of the control is
/// obtained. However, this function cannot retrieve the text of a control in another application.
///
/// @param[in]  hwnd  The window handle.
/// @return  The window text.
///
std::wstring get_wnd_text(HWND hwnd);

/// Set a window's caption title to a specific text.
///
/// @param[in]  wnd  The window
/// @param[in]  text  The text
///
void set_wnd_text(CWnd& wnd, const std::wstring& text);

/// Find out whether a specific window is visible, i.e. it has the WS_VISIBLE style bit set, and its parent window is visible.
///
/// @param[in]  wnd  The window
/// @return  true if it is
///
bool is_wnd_visible(const CWnd& wnd);

/// Get the first child window of a given window which is represented by a specific CWnd-derived class.
///
/// @tparam  TChildWnd  The CWnd-derived class representing the child window
/// @param[in]  parent  The parent window
/// @return  The first matching window object, if any; or nullptr if no match is found
///
template<class TChildWnd,
         class = EnableIfBaseOf<CWnd, TChildWnd>> 
TChildWnd* get_child_wnd(const CWnd& parent);

/// Move a specific child window.
/// 
/// @param[in]  parent_hwnd  The window handle of the parent window
/// @param[in]  child_hwnd  The window handle of the child window
/// @param[in]  dx  The horizontal distance that the child window should move by (pixels, a positive value 
///					means move to the right, a negative value to the left) 
/// @param[in]  dy  The vertical distance that the child window should move by (pixels, a positive value means  
///					move down, a negative value move up) 
/// @param[in]  repaint  Whether the chlid window (and any part of parent window that was uncovered) should be 
///					repainted after the move  
///
void move_child_wnd(HWND parent_hwnd, HWND child_hwnd, int dx, int dy, bool repaint = true);

/// Resize the rectangle of a specific window.
/// 
/// @param[in]  hwnd  The window handle of the window to resize
/// @param[in]  parent_hwnd  The window handle of the parent window; pass in nullptr if the window to resize 
///					is a top-level window
/// @param[in]  left  The horizontal distance that the left edge of the window should move by (pixels, 
///					a positive value means move to the right, a negative value to the left) 
/// @param[in]  top  The vertical distance that the top edge of the window should move by (pixels, a positive 
///					value means move down, a negative value move up) 
/// @param[in]  right  The horizontal distance that the right edge of the window should move by (pixels, 
///					a positive value means move to the right, a negative value to the left) 
/// @param[in]  bottom  The vertical distance that the bottom edge of the window should move by (pixels, a 
///					positive value means move down, a negative value move up) 
/// @param[in]  repaint  Whether the window (and any part of parent window, if any, which was uncovered) 
///					should be repainted after the move  
///
void resize_wnd(HWND hwnd, HWND parent_hwnd, int left, int top, int right, int bottom, bool repaint = true);

/// Get the bounding rectangle of a specific window. 
///
/// @param[in]  wnd  The window
/// @return  The bounding rectangle (screen coordinates)
///
CRect get_window_rect(const CWnd& wnd);

/// Get the Windows desktop window.
///
/// @return  The desktop window; the object is temporary and should not be stored for later use
///
CWnd& get_desktop_window();

/// Allow or prevent changes to a window to be redrawn, ie set or clear the window's redraw flag. While the redraw flag is 
/// cleared, the contents of the window will not be updated after each change and will not be repainted until the redraw flag 
/// is set. 
///
/// @param[in]  wnd  The window
/// @param[in]  redraw  The redraw flag value
///
void set_redraw(CWnd& wnd, bool redraw);

//! Whether the window with handle \p hwnd has the "group" window style (WS_GROUP).
auto has_group_window_style(HWND hwnd) -> bool;

}

#include "wnd_utils.ipp"

#endif 

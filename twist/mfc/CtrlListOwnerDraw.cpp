///  @file  CtrlListOwnerDraw.cpp
///  Implementation file for "CtrlListOwnerDraw.hpp"

//   Copyright (c) 2007-2019 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist_mfc_maker.hpp"

#include "CtrlListOwnerDraw.hpp"

#include "dc_utils.hpp"

namespace twist::mfc {

IMPLEMENT_DYNAMIC(CtrlListOwnerDraw, CtrlListOwnerDrawBase)

BEGIN_MESSAGE_MAP(CtrlListOwnerDraw, CtrlListOwnerDrawBase)
	ON_WM_MEASUREITEM_REFLECT()
END_MESSAGE_MAP()


CtrlListOwnerDraw::CtrlListOwnerDraw() 
	: CtrlListOwnerDrawBase()
{
	TWIST_CHECK_INVARIANT
}


CtrlListOwnerDraw::~CtrlListOwnerDraw()
{
	TWIST_CHECK_INVARIANT
}


void CtrlListOwnerDraw::DrawSubitems(int rowIdx, const CRect& itemRect, HDC dc)
{
	TWIST_CHECK_INVARIANT
	const int numCols = CountColumns();
	for (int i = 0; i < numCols; ++i) {		
		const CRect rect = GetSubitemRect(itemRect, i);
		DrawSubitem(rowIdx, i, rect, dc);		
		UpdateMaxColContentWidth(i, GetSubitemContentWidth(rowIdx, i, dc));
	}
	TWIST_CHECK_INVARIANT
}


#ifdef _DEBUG
void CtrlListOwnerDraw::check_invariant() const noexcept
{
}
#endif // _DEBUG

}




///  @file  ProgressDlgBase.cpp
///  Implementation file for "ProgressDlgBase.hpp"

//   Copyright (c) 2010-2019 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist_mfc_maker.hpp"

#include "ProgressDlgBase.hpp"


namespace twist::mfc {

#define WM_MYMESSAGE (WM_USER + 100)


IMPLEMENT_DYNAMIC(ProgressDlgBase, CDialogEx)

BEGIN_MESSAGE_MAP(ProgressDlgBase, CDialogEx)
    ON_MESSAGE(WM_MYMESSAGE, OnMyMessage)		
	ON_WM_WINDOWPOSCHANGED()
END_MESSAGE_MAP()


ProgressDlgBase::ProgressDlgBase(unsigned int res_id, CWnd* parent)
	: CDialogEx(res_id, parent)
	, caption_()
	, function_()
	, shown_(false)
	, exception_thrown_(false) 
	, exception_message_()
{
	// No state testing: constructed via an external function
}


bool ProgressDlgBase::was_exception_thrown() const
{
	TWIST_CHECK_INVARIANT
	return exception_thrown_;
}
	

std::wstring ProgressDlgBase::get_exception_message() const
{
	TWIST_CHECK_INVARIANT
	return exception_message_;
}


BOOL ProgressDlgBase::OnInitDialog()
{
	TWIST_CHECK_INVARIANT
	CDialogEx::OnInitDialog();

	SetWindowText(caption_.c_str());

	TWIST_CHECK_INVARIANT
	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}


void ProgressDlgBase::OnWindowPosChanged(WINDOWPOS* lpwndpos)
{
	TWIST_CHECK_INVARIANT
	CDialogEx::OnWindowPosChanged(lpwndpos);

	if ((lpwndpos->flags & SWP_SHOWWINDOW) != 0 && !shown_) {
		shown_ = true;
      	this->PostMessage(WM_MYMESSAGE, 0, 0);	
    }
	TWIST_CHECK_INVARIANT
}


LRESULT ProgressDlgBase::OnMyMessage(WPARAM /*wParam*/, LPARAM /*lParam*/)
{
	TWIST_CHECK_INVARIANT
	ProgFeedbackProv& prog_prov = get_prog_prov();

	try {
		function_(prog_prov);
	}
	catch (std::exception& ex) {
		exception_thrown_ = true;
		exception_message_ = error_message(ex);
	}

	this->PostMessage(WM_CLOSE, 0, 0);	

	TWIST_CHECK_INVARIANT
	return 0;
}


#ifdef _DEBUG
void ProgressDlgBase::check_invariant() const noexcept
{
	assert(function_);
}
#endif //_DEBUG


//
//  Free function
//

void show_prov_prog_dlg(ProgressDlgBase& dlg, const std::wstring& caption, std::function<void (ProgFeedbackProv&)> func)
{
	// Initialise dialogue
	dlg.caption_ = caption;
	dlg.function_ = func;

	// Execute the function
	dlg.DoModal();

	// Check whether an exception was thrown during the execution of the function
	if (dlg.was_exception_thrown()) {
		TWIST_THROW(dlg.get_exception_message().c_str());
	}

	dlg.DestroyWindow();
}

}

///  @file  ChildDlgList.ipp
///  Inline implementation file for "ChildDlgList.hpp"

//   Copyright (c) 2010-2019 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

namespace twist::mfc {

template<typename TDlgId>
ChildDlgList<TDlgId>::ChildDlgList(CWnd& parent) : 
	m_parent(parent),
	m_childDlgMap(),
	m_activeDlgId()
{
	TWIST_CHECK_INVARIANT
}


template<typename TDlgId>
ChildDlgList<TDlgId>::~ChildDlgList()
{
	TWIST_CHECK_INVARIANT
}


template<typename TDlgId> template<class ChildDlgClass, class>
ChildDlgClass& ChildDlgList<TDlgId>::add(TDlgId dlgId, long resourceId)
{
	TWIST_CHECK_INVARIANT
	ChildDlgClass* dlg = new ChildDlgClass();
	if (dlg == nullptr) {
		TWIST_THROW(L"Error creating child dialog with resource ID %d.", resourceId);
	}	

	DialogPtr dlgPtr(dlg);
	if (dlgPtr->Create(resourceId, &m_parent) != TRUE) {
		TWIST_THROW(L"Error creating child dialog with resource ID %d.", resourceId);
	}	
	
	auto ret = m_childDlgMap.insert(DlgMap::value_type(dlgId, dlgPtr));
	if (!ret.second) {
		TWIST_THROW(L"A child dialog with resource ID %d already exists in the list.", resourceId);
	}

	TWIST_CHECK_INVARIANT
	return *dlg;
}


template<typename TDlgId> 
void ChildDlgList<TDlgId>::erase(TDlgId dlgId)
{
	TWIST_CHECK_INVARIANT
	auto it = m_childDlgMap.find(dlgId);
	if (it == m_childDlgMap.end()) {
		TWIST_THROW(L"Child dialog with ID %d not found in the list.", dlgId);
	}
	m_childDlgMap.erase(it);
	TWIST_CHECK_INVARIANT
}


template<typename TDlgId> 
void ChildDlgList<TDlgId>::move(TDlgId dlgId, const CRect& dlgRect)
{
	TWIST_CHECK_INVARIANT
	auto it = m_childDlgMap.find(dlgId);
	if (it == m_childDlgMap.end()) {
		TWIST_THROW(L"Child dialog with ID %d not found in the list.", dlgId);
	}
	CDialog* dlg = it->second.get();
	dlg->SetWindowPos(&CWnd::wndTop, dlgRect.left, dlgRect.top, dlgRect.Width(), dlgRect.Height(), SWP_NOACTIVATE);
	TWIST_CHECK_INVARIANT
}


template<typename TDlgId> 
void ChildDlgList<TDlgId>::moveAll(const CRect& dlgRect)
{
	TWIST_CHECK_INVARIANT
	for (auto it = m_childDlgMap.begin(); it != m_childDlgMap.end(); ++it) {
		CDialog* dlg = it->second.get();
		dlg->SetWindowPos(&CWnd::wndTop, dlgRect.left, dlgRect.top, dlgRect.Width(), dlgRect.Height(), SWP_NOACTIVATE);
	}
	TWIST_CHECK_INVARIANT
}


template<typename TDlgId> 
void ChildDlgList<TDlgId>::setActive(TDlgId dlgId)
{
	TWIST_CHECK_INVARIANT
	for (auto it = m_childDlgMap.begin(); it != m_childDlgMap.end(); ++it) {
		CDialog* dlg = it->second.get();
		if (it->first == dlgId) {
			dlg->ShowWindow(SW_SHOW);
		}
		else {
			dlg->ShowWindow(SW_HIDE);
		}
	}
	m_activeDlgId.reset(new TDlgId(dlgId));
	TWIST_CHECK_INVARIANT
}	


template<typename TDlgId> 
CDialog& ChildDlgList<TDlgId>::getActive()
{
	TWIST_CHECK_INVARIANT
	if (m_activeDlgId.get() == nullptr) {
		TWIST_THROW(L"No child dialog is currently active.");
	}
	CDialog& activeDlg = get(*m_activeDlgId);
	TWIST_CHECK_INVARIANT
	return activeDlg;
}


template<typename TDlgId> 
CDialog& ChildDlgList<TDlgId>::get(TDlgId dlgId)
{
	TWIST_CHECK_INVARIANT
	auto it = m_childDlgMap.find(dlgId);
	if (it == m_childDlgMap.end()) {
		TWIST_THROW(L"Child dialog with ID %d not found in the list.", dlgId);
	}
	
	TWIST_CHECK_INVARIANT
	return *it->second;
}


#ifdef _DEBUG
template<typename TDlgId> 
void ChildDlgList<TDlgId>::check_invariant() const noexcept
{
	if (m_activeDlgId.get() != nullptr) {
		assert(m_childDlgMap.size() != 0);
	}
}
#endif // _DEBUG

}


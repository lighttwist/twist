///  @file  ModelessDialogsMgr.ipp
///  Inline implementation file for "ModelessDialogsMgr.hpp"

//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

namespace twist::mfc {

template<typename TKey>
ModelessDialogsMgr<TKey>::ModelessDialogsMgr(CWnd& dialogs_parent_wnd, CreateDlg create_dlg)
	: dialogs_parent_wnd_(dialogs_parent_wnd)
	, create_dlg_(create_dlg)
	, dialogs_()
{
	TWIST_CHECK_INVARIANT
}


template<typename TKey>
ModelessDialogsMgr<TKey>::~ModelessDialogsMgr()
{
	TWIST_CHECK_INVARIANT
}


template<typename TKey>
void ModelessDialogsMgr<TKey>::show_dlg(const TKey& key)
{
	TWIST_CHECK_INVARIANT
	bool create_dlg = true;

	// Check whether the dialogue is already displayed
	const auto it = dialogs_.find(key);
	if (it != end(dialogs_)) {
			
		auto* existing_dlg = it->second.get();
		if (::IsWindow(existing_dlg->m_hWnd) && ::IsWindowVisible(existing_dlg->m_hWnd)) {
				
			::SetFocus(existing_dlg->m_hWnd);
			create_dlg = false;
		}
		else {
			dialogs_.erase(it);
		}
	} 
		
	if (create_dlg) {
		unsigned int res_id = 0; 
		std::wstring caption;
		auto dlg = create_dlg_(key, res_id, caption);

		dlg->Create(res_id, &dialogs_parent_wnd_);
		dlg->ShowWindow(SW_SHOW);

		set_wnd_text(*dlg, caption);

		dialogs_.insert( std::make_pair(key, move(dlg)) );		
	}
	
	TWIST_CHECK_INVARIANT
}


#ifdef _DEBUG
template<typename TKey>
void ModelessDialogsMgr<TKey>::check_invariant() const noexcept
{
	assert(create_dlg_);
}
#endif // _DEBUG

}

///  @file  CtrlEditFloatHelper.hpp
///  CtrlEditFloatHelper  class template

//   Copyright (c) 2007-2019 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_MFC_CTRL_EDIT_FLOAT_HELPER_HPP
#define TWIST_MFC_CTRL_EDIT_FLOAT_HELPER_HPP

#include "../string_utils.hpp"

namespace twist::mfc {

///
///  Helper class for specialised edit control classes which allow only input of valid floating point numbers.
///  The template parameter is the class type of the edit control.
///
template<class TEditCtrl>
class CtrlEditFloatHelper {
public:
	/// Constructor.
	///
	/// @param[in]  ctrl  The edit control
	/// @param[in]  precision  The precision to be used while displaying the floating-point number (this means different 
	///				things depending on the format). Pass in zero for the default precision.
	/// @param[in]  format  The format in which the floating-point number should be displayed.
	/// @param[in]  decimal_sep  The decimal separator character 
	///  
	CtrlEditFloatHelper(TEditCtrl& ctrl, unsigned int precision, FloatStrFormat format, wchar_t decimal_sep);

	/// Destructor.
	///
	virtual ~CtrlEditFloatHelper();

	/// Get the text currently entered in the control.
	///
	/// @return  The text.
	///
	CString GetText() const;
	
	/// Get the floating-point number currently entered in the control. Returns zero if the text is an invalid 
	/// representation of a floating-point number (e.g. if it is blank).
	///
	/// @return  The number.
	///
	double GetFloat() const;
	
	/// Set the floating-point number currently displayed in the control.
	///
	/// @param[in]  value  The number.
	///
	void SetFloat(double value);
	
	/// Find out whether the control is currently empty (i.e. it contains no text).
	///
	/// @return  true if it is.
	///
	bool IsEmpty() const;

	/// Get the decimal separator character for this instance.
	///
	/// @return  The decimal separator
	///
	wchar_t GetDecimalSep() const;
	
	/// Set the "special text" for the control.
	/// This text (which can contain forbidden characters) can then be displayed by calling 'DisplaySpecialText'.
	/// Once displayed, the control has specific behaviour on subsequent input (see 'DisplaySpecialText').
	///
	/// @param[in]  special_text  The special text. Pass in an empty string to reset any previous special text.
	///
	void SetSpecialText(CString special_text);
	
	/// Display the "special text" in the control (as set by a previous call to 'DisplaySpecialText').
	/// Once the special text is displayed:
	///  * The special text will be fully selected in the control every time it receives focus.
	///  * As soon as a valid character is typed in the control, the special text will disappear.
	///
	void DisplaySpecialText();

	/// Call before the edit control attempts to handle the WM_CHAR message.
	///
	/// @param[in]  chr_code  The character code
	/// @return  Whether the charcter is valid; if true, the edit control should call the base class's WM_CHAR handler 
	///
	bool BeforeChar(UINT chr_code);  
	
	/// Call before the edit control has handled the WM_SETFOCUS message.
	///
	void AfterSetFocus();						 
	
	/// Call before the edit control has handled the WM_KILLFOCUS message.
	///
	void AfterKillFocus();					  

	/// Call before the edit control has handled the WM_LBUTTONDOWN message.
	///	
	void AfterLButtonDown();        

private:
	TEditCtrl&  ctrl_;

	unsigned int  precision_;
	FloatStrFormat  format_;
	wchar_t  decimal_sep_;

	wchar_t  c_runtime_decimal_sep_;  // The decimal separator used by the text-to-number-to-text conversion routines
	
	CString  special_text_;  // Blank if not active
	bool  has_focus_;

	std::set<wchar_t>  allowed_chars_;
	
	TWIST_CHECK_INVARIANT_DECL
};

}

#include "CtrlEditFloatHelper.ipp"

#endif

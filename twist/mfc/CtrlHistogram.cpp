///  @file  CtrlHistogram.cpp
///  Implementation file for "CtrlHistogram.hpp"

//   Copyright (c) 2010-2019 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist_mfc_maker.hpp"

#include "CtrlHistogram.hpp"

#include "../math/numeric_utils.hpp"

namespace twist::mfc {

// Margins between the inner, histogram rectangle an the outer, image control client rectangle (pixels).
// Fiddle with these four numbers to fit the inner histogram rectangle within the image control.
static const int  k_hist_rect_margin_left   = 40;
static const int  k_hist_rect_margin_top    = 10;
static const int  k_hist_rect_margin_right  = 18;
static const int  k_hist_rect_margin_bottom = 25;

static const COLORREF  k_bin_colour  = 0xAA0A0A;
static const COLORREF  k_axis_colour = 0x808080;

static const int  k_axis_protrusion     = 2;
static const int  k_dist_left_from_hist = 2;
static const int  k_dist_down_from_hist = 5;


BEGIN_MESSAGE_MAP(CtrlHistogram, CtrlStaticGraphic)
END_MESSAGE_MAP()


CtrlHistogram::CtrlHistogram(const HistData& histData, HistType histType)
	: CtrlStaticGraphic()
	, m_histData(histData)
	, m_histType(histType)
{
	TWIST_CHECK_INVARIANT
}


CtrlHistogram::~CtrlHistogram()
{
	TWIST_CHECK_INVARIANT
}


void CtrlHistogram::SetHistData(const HistData& histData, bool invalidate)
{
	TWIST_CHECK_INVARIANT
	m_histData = histData;
	if (invalidate) {
		SafeInvalidate();
	}
	TWIST_CHECK_INVARIANT
}


void CtrlHistogram::SetHistType(HistType histType, bool invalidate)
{
	TWIST_CHECK_INVARIANT
	m_histType = histType;
	if (invalidate) {
		SafeInvalidate();
	}
	TWIST_CHECK_INVARIANT
}


void CtrlHistogram::DrawGraphic(CDC& dc, const CRect& rect)
{
	TWIST_CHECK_INVARIANT
	// Erase background
    dc.BitBlt(rect.left, rect.top, rect.Width(), rect.Height(), 0, 0, 0, WHITENESS);

	const CRect hist_rect(rect.left + k_hist_rect_margin_left, rect.top + k_hist_rect_margin_top, 
			rect.right - k_hist_rect_margin_right, rect.bottom - k_hist_rect_margin_bottom);

    TWIST_ASSERT(m_histData.num_bins() <= static_cast<size_t>(hist_rect.Width()));  // A bin cannot be thinner than a pixel

    const double binWidth = static_cast<double>(hist_rect.Width()) / m_histData.num_bins();
	const size_t max_freq = m_histType == HistType::k_pdf ? m_histData.max_freq() : m_histData.sample_size();
    const double vertRatio = static_cast<double>(hist_rect.Height() - 2) / max_freq;

	DrawHistogram(dc, hist_rect, binWidth, vertRatio);
	DrawCoords(dc, hist_rect);

	TWIST_CHECK_INVARIANT
}


void CtrlHistogram::DrawHistogram(CDC& dc, const CRect& rect, double binWidth, double vertRatio)
{
	TWIST_CHECK_INVARIANT
	CBrush brush;
	brush.CreateSolidBrush(k_bin_colour);

	CBrush* old_brush = dc.SelectObject(&brush);
	
	double freq = 0;
	for (size_t i = 0; i < m_histData.num_bins(); ++i) {
		
		freq = m_histType == HistType::k_pdf ? m_histData.frequencies().at(i) : freq + m_histData.frequencies().at(i);

		dc.Rectangle(
				rect.left + twist::math::floor_to_int(binWidth * i), 
				rect.bottom, 
				rect.left + twist::math::floor_to_int(binWidth * (i + 1)),
				rect.bottom - twist::math::floor_to_int(freq * vertRatio));
	}
	
	dc.SelectObject(old_brush);
	TWIST_CHECK_INVARIANT
}


void CtrlHistogram::DrawCoords(CDC& dc, const CRect& rect)
{
	TWIST_CHECK_INVARIANT
	static const wchar_t* k_prob_mask = L"%.1f";
	static const wchar_t* k_bounds_mask = L"%.2f";

	CPen pen;
	pen.CreatePen(PS_SOLID, 1, k_axis_colour);
	auto* old_pen = dc.SelectObject(&pen);
	const auto old_colour = dc.SetTextColor(k_axis_colour);
	const auto old_bkg_mode = dc.SetBkMode(TRANSPARENT);

	// Draw the axes
	dc.MoveTo(rect.left - k_axis_protrusion, rect.bottom);
	dc.LineTo(rect.right + 2 * k_axis_protrusion, rect.bottom);
	dc.MoveTo(rect.left, rect.bottom + k_axis_protrusion);
	dc.LineTo(rect.left, rect.top - k_axis_protrusion);

	// Draw coordinate markers on the axes
	dc.MoveTo(rect.right - 1, rect.bottom);
	dc.LineTo(rect.right - 1, rect.bottom + k_axis_protrusion + 1);
	dc.MoveTo(rect.left - k_axis_protrusion - 1, rect.top + 2);
	dc.LineTo(rect.left, rect.top + 2);

    // Write the low bound of the first bin
	auto text = format_str(k_bounds_mask, m_histData.sample_min());
	auto text_size = dc.GetTextExtent(text.c_str());
	dc.TextOut(rect.left - text_size.cx / 2, rect.bottom + k_dist_down_from_hist, text.c_str());

	// Write the high bound of the last bin
	text = format_str(k_bounds_mask, m_histData.sample_max());
	text_size = dc.GetTextExtent(text.c_str());
	dc.TextOut(rect.right - text_size.cx / 2, rect.bottom + k_dist_down_from_hist, text.c_str());

	// Write the highest probability
	text = m_histType == HistType::k_pdf ? 
		format_str(k_prob_mask, static_cast<double>(m_histData.max_freq()) / m_histData.sample_size() * 100) + L"%" : L"1     ";
	text_size = dc.GetTextExtent(text.c_str());
	dc.TextOut(rect.left - k_dist_left_from_hist - text_size.cx, rect.top - text_size.cy / 2 + 2, text.c_str());

	// Write zero probability. Left-align it with the highest probability.
	text = m_histType == HistType::k_pdf ? L"0%" : L"0";
	dc.TextOut(rect.left - k_dist_left_from_hist - text_size.cx, rect.bottom - text_size.cy / 2, text.c_str());

	dc.SetBkMode(old_bkg_mode);
	dc.SetTextColor(old_colour);
	dc.SelectObject(old_pen);
	TWIST_CHECK_INVARIANT
}


#ifdef _DEBUG
void CtrlHistogram::check_invariant() const noexcept
{
	assert(m_histType >= HistType::k_pdf && m_histType <= HistType::k_cdf);
}
#endif // _DEBUG

}


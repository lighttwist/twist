///  @file  CtrlEditInt.cpp
///  Implementation file for "CtrlEditInt.hpp"

//   Copyright (c) 2010-2019 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist_mfc_maker.hpp"

#include "CtrlEditInt.hpp"

#include "wnd_utils.hpp"
#include "../string_utils.hpp"

namespace twist::mfc {

IMPLEMENT_DYNAMIC(CtrlEditInt, CEdit)

BEGIN_MESSAGE_MAP(CtrlEditInt, CEdit)
	ON_WM_CHAR()
	ON_WM_LBUTTONDOWN()
	ON_WM_KILLFOCUS()
	ON_WM_SETFOCUS()
END_MESSAGE_MAP()


const wchar_t CtrlEditInt::s_allowedCharList[] = 
		{L'0', L'1', L'2', L'3', L'4', L'5', L'6', L'7', L'8', L'9', L'0', L'-', VK_BACK};

const size_t CtrlEditInt::s_allowedCharCount = 
		sizeof(s_allowedCharList) / sizeof(wchar_t);
		

CtrlEditInt::CtrlEditInt() :
	CEdit{},
	m_rangeMin{ 0 },
	m_rangeMax{ 0 },
	m_rangeSet{ false },
	m_specialText{},
	m_hasFocus{ false }
{
	TWIST_CHECK_INVARIANT
}


CString CtrlEditInt::GetText() const
{
	TWIST_CHECK_INVARIANT
	CString text;
	GetWindowText(text);	
	return text;
}


int CtrlEditInt::GetInt() const
{
	TWIST_CHECK_INVARIANT
	const auto text = get_wnd_text(GetSafeHwnd());	
	assert(!text.empty());
	if (!text.empty()) {
		return to_int(text);
	}
	return 0;
}


void CtrlEditInt::SetInt(int value)
{
	TWIST_CHECK_INVARIANT
	SetWindowText(std::to_wstring(value).c_str());
	TWIST_CHECK_INVARIANT
}


bool CtrlEditInt::IsEmpty() const
{
	TWIST_CHECK_INVARIANT
	CString text;
	GetWindowText(text);	
	return text.GetLength() == 0;
}


void CtrlEditInt::SetRange(int min, int max)
{
	TWIST_CHECK_INVARIANT
	if (min < max) {
		m_rangeSet = true;
		m_rangeMin = min;
		m_rangeMax = max;
	}
	TWIST_CHECK_INVARIANT
}


void CtrlEditInt::SetSpecialText(CString specialText)
{
	TWIST_CHECK_INVARIANT
	m_specialText = specialText;	
	TWIST_CHECK_INVARIANT
}


void CtrlEditInt::DisplaySpecialText()
{
	TWIST_CHECK_INVARIANT
	ASSERT(!m_specialText.IsEmpty());
	if (!m_specialText.IsEmpty()) {
		SetWindowText(m_specialText);
	}
	TWIST_CHECK_INVARIANT
}


void CtrlEditInt::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	TWIST_CHECK_INVARIANT

	const wchar_t chr = static_cast<wchar_t>(nChar);

	bool inputOk = (std::find(
			s_allowedCharList, 
			s_allowedCharList + s_allowedCharCount, chr) != 
			s_allowedCharList + s_allowedCharCount);

	if (inputOk) {
		int selStart = 0;
		int selEnd = 0; 
		GetSel(selStart, selEnd);

		CString text;
		GetWindowText(text);

		// If the control currently dispays the "special text", zap it.
		if (!m_specialText.IsEmpty()) {
			if (text.Compare(m_specialText) == 0) {
				SetWindowText(L"");
				text = L"";
			}
		}

        // Anything goes if all the text is selected.
        if ((selEnd - selStart) != text.GetLength()) {
		
			if (chr == L'-') {
				// Only allow the minus sign if it is the first character in the control.
				inputOk = (selStart == 0);
			}			
			if (inputOk) {
				// Do not allow input before a leading minus sign
				if (selStart <= text.Find(L'-')) {
					inputOk = false;
				}
            }            
            if (inputOk) {
                // Only allow one decimal point.
                if ((chr == m_decimalSep) && (text.Find(m_decimalSep) != -1)) {
					inputOk = false;
                }
            }
            if (inputOk) {
                // Only allow one, leading minus sign.
                if ((chr == L'-') && ((selStart > 0) || (text.Find(L'-') != -1))) {
                    inputOk = false;
                }
            }           
		}

		if (inputOk) {
			CEdit::OnChar(nChar, nRepCnt, nFlags);
		}
	}
}


void CtrlEditInt::OnLButtonDown(UINT nFlags, CPoint point)
{	
	TWIST_CHECK_INVARIANT
	// Check the "has focus" flag before calling 'CEdit::OnLButtonDown', as that call can trigger a "set focus" 
	// event itself.
	if (!m_hasFocus) {
		CEdit::OnLButtonDown(nFlags, point);
		if (!m_specialText.IsEmpty()) {
			CString text;
			GetWindowText(text);
			if (text.Compare(m_specialText) == 0) {
				// The "special text" is currently displayed in the control, and the control has just received focus 
				// by clicking. Select the whole text.
				SetSel(0, -1);
			}
		}
	}
	else {
		CEdit::OnLButtonDown(nFlags, point);
	}
	TWIST_CHECK_INVARIANT
}


void CtrlEditInt::OnSetFocus(CWnd* pOldWnd)
{
	TWIST_CHECK_INVARIANT
	CEdit::OnSetFocus(pOldWnd);
	if (!m_specialText.IsEmpty()) {
		CString text;
		GetWindowText(text);
		if (text.Compare(m_specialText) == 0) {
			// The "special text" is currently displayed in the control, and the control has just received focus. 
			// Select the whole text.
			SetSel(0, -1);
		}
	}
	m_hasFocus = true;
	TWIST_CHECK_INVARIANT
}


void CtrlEditInt::OnKillFocus(CWnd* pNewWnd)
{
	TWIST_CHECK_INVARIANT
	const int value = GetInt();
	if (m_rangeSet) {
		if (value < m_rangeMin) {
			SetInt(m_rangeMin);
		}
		else if (value > m_rangeMax) {
			SetInt(m_rangeMax);
		}
	}
	
	CEdit::OnKillFocus(pNewWnd);
	m_hasFocus = false;
	TWIST_CHECK_INVARIANT
}


#ifdef _DEBUG
void CtrlEditInt::check_invariant() const noexcept
{
}
#endif // _DEBUG

}

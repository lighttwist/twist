///  @file  CtrlInPlaceEdit.hpp
///  CtrlInPlaceEdit  class template

//   Copyright (c) 2007-2019, Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_MFC_CTRL_IN_PLACE_EDIT_HPP
#define TWIST_MFC_CTRL_IN_PLACE_EDIT_HPP

#include "../string_utils.hpp"

namespace twist::mfc {

///
///  This class represents an "in-place" edit control which can be displayed on any window, at any position. 
///  When the edit box loses focus it is hidden and a user-supplied handler for the "exit" event is called.
///  Special facilities are provided for the case where the list box is shown over a list control cell.
///  A filter for the characters entered into the edit box can be supplied.
///  The template parameter is the type of the edit control - it should have CEdit-like semantics.
///
template<class TEditCtrl>
class CtrlInPlaceEdit : public CWnd {
public:
	/// Handler for the "exit" event for the edit box; the handler is not called if the edit box was exited by 
	/// pressing ESCAPE.
	///
	/// @param[in]  text  The text entered into the edit box at the moment it was exited
	///
	typedef std::function<void(const std::wstring& text)>  OnExit;

	/// Filter for the characters entered into the edit box.
	///
	/// @param[in]  chr  The character entered
	/// @return  true to accept the character, false to reject it
	///
	typedef std::function<bool(wchar_t chr)>  CharFilter;

	/// Constructor.
	///
	/// @param[in]  ctrl  A handler for the "exit" event of the underlying edit control
	/// @param[in]  parent  The parent window (eg a dialogue window)
	/// @param[in]  on_exit  Handler for the "exit" event of the underlying edit control called when the edit 
	///					box loses focus
	///
	CtrlInPlaceEdit(std::unique_ptr<TEditCtrl> ctrl, CWnd& parent, OnExit on_exit);

	/// Destructor.
	///
	virtual ~CtrlInPlaceEdit();

    /// Show the edit control.
    ///
    /// @param[in]  ctrl_rect  The control's bounding rectangle; in coordinates relative to the parent's 
	///					client area
    /// @param[in]  text  The text that will be initially displayed in the control
    ///
    void show(const CRect& ctrl_rect, const std::wstring& text);

    /// Show the edit control so that it precisely covers a cell in a list control.
	/// The text in the control will be the text in the cell.
    ///
    /// @param[in]  lst_ctrl  The list control
    /// @param[in]  item_idx  The list item index 
    /// @param[in]  col_idx  The list column index
    ///
    void show_over_lst_ctrl_cell(const CListCtrl& lst_ctrl, int item_idx, int col_idx);

	/// If the edit control has been shown over a list control cell, and it is currently visible, get the item 
	/// index for the cell.
	///
	/// @return  The cell item index; or -1 if the edit control is not displayed over a list control cell  
	/// 
	int get_lst_ctrl_item_idx() const; 

	/// If the edit control has been shown over a list control cell, and it is currently visible, get the 
	/// column index for the cell.
	///
	/// @return  The cell column index; or -1 if the edit control is not displayed over a list control cell  
	/// 
	int get_lst_ctrl_col_idx() const; 

	/// Set a filter for the characters entered into the edit box.
	///
	/// @param[in]  char_filter  The filter
	///
	void set_char_filter(CharFilter char_filter);

    /// Hide the control (if visible). If the control was visible, hiding it will trigger an "exit" event.
    /// 
    void hide();

private:	
	virtual BOOL PreTranslateMessage(MSG* pMsg) override;

	bool on_edt_char(unsigned int chr);  // WM_CHAR

	afx_msg void on_edt_killfocus();  // EN_KILLFOCUS

	std::unique_ptr<TEditCtrl>  ctrl_;

	OnExit  on_exit_;

	int  lst_ctrl_item_idx_;
	int  lst_ctrl_col_idx_;

	CharFilter  char_filter_;

	bool  esc_pressed_;
	bool  ignore_killfocus_;

	DECLARE_MESSAGE_MAP()
	
	TWIST_CHECK_INVARIANT_DECL
};

}

#include "CtrlInPlaceEdit.ipp"

#endif

///  @file  CtrlEditInt.hpp
///  CtrlEditInt  class, inherits CEdit

//   Copyright (c) 2007-2019 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_MFC_CTRL_EDIT_INT_HPP
#define TWIST_MFC_CTRL_EDIT_INT_HPP

namespace twist::mfc {

///
///  Specialised edit control, allowing only input of valid integer numbers.
///
class CtrlEditInt : public CEdit {
public:
	/// Defaukt constructor.
	///
	CtrlEditInt();

	/// Get the text currently entered in the control.
	///
	/// @return  The text.
	///
	CString GetText() const;
	
	/// Get the integer number currently entered in the control. Returns zero if the text is an invalid 
	/// representation of a integer number (e.g. if it is blank).
	///
	/// @return  The number.
	///
	int GetInt() const;
	
	/// Set the integer number currently displayed in the control.
	///
	/// @param[in]  value  The number.
	///
	void SetInt(int value);
	
	/// Find out whether the control is currently empty (i.e. it contains no text).
	///
	/// @return  true only if it is.
	///
	bool IsEmpty() const;
	
	/// Set the valid range for the value entered in the control.
	/// If the range is set, then upon losing the focus, the control will adjust itself automatically, that is if 
	/// the entered value is smaller than the minimim, the value becomes the minimimum, and if it is greater than 
	/// the maximim, the value becomes the maximum.
	///
	/// @param[in]  min  The smallest valid value.
	/// @param[in]  max  The greatest valid value.
	///
	void SetRange(int min, int max);

	/// Set the "special text" for the control.
	/// This text (which can contain forbidden characters) can then be displayed by calling 'DisplaySpecialText'.
	/// Once displayed, the control has specific behaviour on subsequent input (see 'DisplaySpecialText').
	///
	/// @param[in]  specialText  The special text. Pass in an empty string to reset any previous special text.
	///
	void SetSpecialText(CString specialText);
	
	/// Display the "special text" in the control (as set by a previous call to 'DisplaySpecialText').
	/// Once the special text is displayed:
	///  * The special text will be fully selected in the control every time it receives focus.
	///  * As soon as a valid character is typed in the control, the special text will disapper.
	///
	void DisplaySpecialText();

protected:
	// Control event handlers 
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);  // WM_CHAR

	afx_msg void OnSetFocus(CWnd* pOldWnd);  // WM_SETFOCUS
	
	afx_msg void OnKillFocus(CWnd* pNewWnd);  // WM_KILLFOCUS
	
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);  // WM_LBUTTONDOWN

	DECLARE_DYNAMIC(CtrlEditInt)

	DECLARE_MESSAGE_MAP()

private:
	// List of all allowed characters.
	static const wchar_t s_allowedCharList[];
	
	// The number of allowed characters.
	static const size_t s_allowedCharCount;
	
	// The decimal separator character to be used in this control.
	wchar_t m_decimalSep;
		
	// Whether a valid range has been set for the value entered in the control.
	bool m_rangeSet;
	
	// The smallest valid value (only meaningful if 'm_rangeSet' is true).
	int m_rangeMin;
	
	// The greatest valid value (only meaningful if 'm_rangeSet' is true).
	int m_rangeMax;

	// The "special text" (see 'DisplaySpecialText'). Blank if not active.
	CString m_specialText;

	// Whether the control currently has focus.
	bool m_hasFocus;
	
	TWIST_CHECK_INVARIANT_DECL
};

}

#endif

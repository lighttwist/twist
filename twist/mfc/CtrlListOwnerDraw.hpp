///  @file  CtrlListOwnerDraw.hpp
///  CtrlListOwnerDraw  class, inherits CtrlListOwnerDrawBase

//   Copyright (c) 2007-2019 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_MFC_CTRL_LIST_OWNER_DRAW_HPP
#define TWIST_MFC_CTRL_LIST_OWNER_DRAW_HPP

#include "CtrlListOwnerDrawBase.hpp"

namespace twist::mfc {

///
///  Abstract base class for owner-draw list controls.
///  Specialises  CtrlListOwnerDrawBase  for drawing using the Windows API.
///
class CtrlListOwnerDraw : public CtrlListOwnerDrawBase {
public:
	CtrlListOwnerDraw();

	virtual ~CtrlListOwnerDraw();

protected:
	DECLARE_DYNAMIC(CtrlListOwnerDraw)

	DECLARE_MESSAGE_MAP()

private:
	virtual void DrawSubitems(int rowIdx, const CRect& itemRect, HDC dc) override;  //  CtrlListOwnerDrawBase  override

	/// Draw a particular subitem (cell) of the list.
	/// 
	/// @param[in]  rowIdx  The subitem row index.
	/// @param[in]  colIdx  The subitem column index.
	/// @param[in]  rect  The subitem bounding rectangle.
	/// @param[in]  dc  Handle to the device context.
	///
	virtual void DrawSubitem(int rowIdx, int colIdx, const CRect& rect, HDC dc) = 0;

	TWIST_CHECK_INVARIANT_DECL
};

}

#endif


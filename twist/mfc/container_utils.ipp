///  @file  container_utils.ipp
///  Inline implementation file for "container_utils.hpp"

//   Copyright (c) 2010-2019 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

namespace twist::mfc {

template<class ListBase, class Ty>
TypedPtrListIterator<ListBase, Ty>::TypedPtrListIterator(const List& list, POSITION pos)
	: list_{ &list }
	, pos_{ pos }
{
}


template<class ListBase, class Ty>
typename TypedPtrListIterator<ListBase, Ty>::reference TypedPtrListIterator<ListBase, Ty>::operator*() const 
{
	return list_->GetAt(pos_); 
}
	

template<class ListBase, class Ty>
typename TypedPtrListIterator<ListBase, Ty>::pointer TypedPtrListIterator<ListBase, Ty>::operator->() const  
{ 
	return list_->GetAt(pos_); 
} 


template<class ListBase, class Ty>
TypedPtrListIterator<ListBase, Ty>& TypedPtrListIterator<ListBase, Ty>::operator++() 
{
	list_->GetNext(pos_);	
	return *this;
}


template<class ListBase, class Ty>
TypedPtrListIterator<ListBase, Ty> TypedPtrListIterator<ListBase, Ty>::operator++(int) 
{
	auto temp = *this;
	++(*this);
	return temp;	
}


template<class ListBase, class Ty>
bool TypedPtrListIterator<ListBase, Ty>::operator==(const TypedPtrListIterator& rhs) const 
{ 
	assert(list_ == rhs.list_); 
	return pos_ == rhs.pos_;
}


template<class ListBase, class Ty>
bool TypedPtrListIterator<ListBase, Ty>::operator!=(const TypedPtrListIterator& rhs) const 
{ 
	assert(list_ == rhs.list_); 
	return pos_ != rhs.pos_;
}

}

template<class ListBase, class Ty>
twist::mfc::TypedPtrListIterator<ListBase, Ty> begin(const CTypedPtrList<ListBase, Ty>& list)
{
	return twist::mfc::TypedPtrListIterator<ListBase, Ty>{ list, list.GetHeadPosition() };
}


template<class ListBase, class Ty>
twist::mfc::TypedPtrListIterator<ListBase, Ty> end(const CTypedPtrList<ListBase, Ty>& list)
{
	return twist::mfc::TypedPtrListIterator<ListBase, Ty>{ list, nullptr };
}


template<typename Ty, typename ArgTy>
const Ty* begin(const CArray<Ty, ArgTy>& arr)
{
	return arr.GetData();	
}


template<typename Ty, typename ArgTy>
Ty* begin(CArray<Ty, ArgTy>& arr)
{
	return arr.GetData();	
}


template<typename Ty, typename ArgTy>
const Ty* end(const CArray<Ty, ArgTy>& arr)
{
	return arr.GetData() + arr.GetSize();
}


template<typename Ty, typename ArgTy>
Ty* end(CArray<Ty, ArgTy>& arr)
{
	return arr.GetData() + arr.GetSize();
}



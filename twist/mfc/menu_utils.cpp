///  @file  menu_utils.cpp
///  Implementation file for "menu_utils.hpp"

//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist_mfc_maker.hpp"

#include <afxcontextmenumanager.h>
#include <afxwinappex.h>

#include "menu_utils.hpp"

#include "list_ctrl_utils.hpp"

namespace twist::mfc {

void show_popup_menu(unsigned int res_id, const CPoint& point, CWnd& parent, bool own_msg, bool right_align)
{
	auto* application = dynamic_cast<CWinAppEx*>(::AfxGetApp());
	assert(application != nullptr);

	application->GetContextMenuManager()->ShowPopupMenu(res_id, point.x, point.y, &parent, own_msg, right_align);
}


void enable_cmd_ui(CCmdUI* cmd_ui, bool enable)
{
	assert(cmd_ui != nullptr);
	cmd_ui->Enable(enable ? TRUE : FALSE);
}


bool delete_menu_item_by_cmd_id(HMENU hmenu, UINT item_cmd_id)
{
	return ::DeleteMenu(hmenu, item_cmd_id, MF_BYCOMMAND) != FALSE;
}


bool delete_menu_item_by_pos(HMENU hmenu, UINT item_pos)
{
	return ::DeleteMenu(hmenu, item_pos, MF_BYPOSITION) != FALSE;
}


bool delete_menu_item_by_string(HMENU hmenu, const std::wstring& item_string)
{
	const int item_pos = find_menu_item_pos(hmenu, item_string);
	if (item_pos != k_no_item) {
		return delete_menu_item_by_pos(hmenu, item_pos);
	}
	return false;
}


void show_popup_menu(const std::vector<std::pair<UINT, std::wstring>>& menu_items, HWND parent_wnd, const POINT& location)
{
	assert(menu_items.size() != 0);

	const HMENU popup_menu = ::CreatePopupMenu();
	int pos = 0;
	for (const auto& el : menu_items) {
		TWIST_ASSERT( ::InsertMenu(popup_menu, pos++, MF_BYPOSITION | MF_STRING, el.first, el.second.c_str()) );
	}

	TWIST_ASSERT( ::TrackPopupMenu(popup_menu, TPM_LEFTALIGN | TPM_RIGHTBUTTON, location.x, location.y, 0, parent_wnd, nullptr) );
}


int find_menu_item_pos(HMENU menu, const std::wstring& item_string)
{
	MENUITEMINFO menu_item_info;
	::ZeroMemory(&menu_item_info, sizeof(MENUITEMINFO));
	menu_item_info.cbSize = sizeof(MENUITEMINFO);
	menu_item_info.fMask = MIIM_TYPE;

	const int cnt = ::GetMenuItemCount(menu);
	for (int i = 0; i < cnt; ++i) {

		menu_item_info.dwTypeData = nullptr;

		TWIST_ASSERT( ::GetMenuItemInfo(menu, i, TRUE/*byPosition*/, &menu_item_info) );

		if (menu_item_info.fType == MFT_STRING) {

			assert(menu_item_info.cch > 0);

			std::vector<wchar_t> buffer(menu_item_info.cch + 1);		
			menu_item_info.dwTypeData = buffer.data();
			menu_item_info.cch = size32(buffer);

			TWIST_ASSERT( ::GetMenuItemInfo(menu, i, TRUE/*byPosition*/, &menu_item_info) );

			if (item_string == menu_item_info.dwTypeData) {
				return i;
			}
		}
	}

	return k_no_item;
}


HMENU get_submenu(HMENU menu, const std::wstring& item_string)
{
	const int submenu_pos = find_menu_item_pos(menu, item_string);
	if (submenu_pos != k_no_item) {		
		return ::GetSubMenu(menu, submenu_pos);
	}
	return nullptr;
}


bool is_menu_item_separator(HMENU menu, UINT item_pos)
{
	MENUITEMINFO menu_item_info;
	::ZeroMemory(&menu_item_info, sizeof(MENUITEMINFO));
	menu_item_info.cbSize = sizeof(MENUITEMINFO);
	menu_item_info.fMask = MIIM_FTYPE;

	TWIST_ASSERT( ::GetMenuItemInfo(menu, item_pos, TRUE/*byPosition*/, &menu_item_info) );

	return menu_item_info.fType == MFT_SEPARATOR;
}


void show_multi_check_context_menu(CListCtrl& ctrl, CPoint point)
{
	// Menu item IDs
	static const int k_mnu_id_check_sel   = 1;
	static const int k_mnu_id_uncheck_sel = 2;
	static const int k_mnu_id_check_all   = 3;
	static const int k_mnu_id_uncheck_all = 4;

	const size_t num_items = ctrl.GetItemCount();
	if (num_items > 0) {

		// Create the context menu
		CMenu context_menu; 
		context_menu.CreatePopupMenu();

		const auto sel_item_indexes = get_sel_items(ctrl);
		const size_t num_sel_items = sel_item_indexes.size();
		if (num_sel_items > 0) {
		
			const size_t num_sel_checked_items = twist::count_if(sel_item_indexes, [&](int el) {
				return ctrl.GetCheck(el) == TRUE;
			});
			const size_t num_sel_unchecked_items = num_sel_items - num_sel_checked_items;

			if (num_sel_unchecked_items > 0) {
				context_menu.AppendMenu(MF_STRING, k_mnu_id_check_sel, L"Check all selected");
			}
			if (num_sel_checked_items > 0) {
				context_menu.AppendMenu(MF_STRING, k_mnu_id_uncheck_sel, L"Un-check all selected");
			}
		}

		const size_t num_checked_items = get_items_by_check(ctrl, true).size();	
		const size_t num_unchecked_items = num_items - num_checked_items;

		if (num_checked_items < num_items) {
			context_menu.AppendMenu(MF_STRING, k_mnu_id_check_all, L"Check all");
		}
		if (num_unchecked_items < num_items) {
			context_menu.AppendMenu(MF_STRING, k_mnu_id_uncheck_all, L"Un-check all");
		}

		// Show the context menu
		ctrl.ClientToScreen(&point);
		const int sel_menu = context_menu.TrackPopupMenu(TPM_LEFTALIGN | TPM_LEFTBUTTON | TPM_RIGHTBUTTON | TPM_RETURNCMD, 
				point.x, point.y, &ctrl);
			
		switch (sel_menu) {
			case k_mnu_id_check_sel : {
				twist::for_each(sel_item_indexes, [&](int el) {
					ctrl.SetCheck(el, TRUE);
				});
				break;
			}
			case k_mnu_id_uncheck_sel : {
				twist::for_each(sel_item_indexes, [&](int el) {
					ctrl.SetCheck(el, FALSE);
				});
				break;
			}
			case k_mnu_id_check_all : {
				set_all_items_check(ctrl, true);
				break;
			}
			case k_mnu_id_uncheck_all : {
				set_all_items_check(ctrl, false);
				break;
			}
		}
	}
}

}

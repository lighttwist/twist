///  @file  dc_utils.hpp
///  Miscellaneous utilities for working with Windows device contexts.

//   Copyright (c) 2007-2019 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_MFC_DC__UTILS_HPP
#define TWIST_MFC_DC__UTILS_HPP

namespace twist::mfc {

/// Draw some text.
///
/// @param[in]  text  The text
/// @param[in]  rect  The bounding rectangle for the text
/// @param[in]  dc  The device context
/// @param[in]  format  The format; see the API function  DrawTextEx  for values
///
void draw_text(const std::wstring& text, const CRect& rect, HDC dc, UINT format = DT_CENTER | DT_VCENTER | DT_SINGLELINE);

/// Get the width of drawn text.
///
/// @param[in]  text  The text
/// @param[in]  hdc  The device context on which the text is drawn
/// @return  The text width pixels, or -1 if it could not be computed
///
int get_text_width(const std::wstring& text, HDC hdc);

/// Lighten a colour by a factor.
///
/// @param[in]  colour  The colour.
/// @param[in]  factor  The factor by which the colour should be made lighter. Must be a number in the range (0,1]. 
/// @return  The lighter colour.
///
COLORREF lighten_colour(COLORREF colour, double factor);

}

#endif 

/// @file  test_serialisation_utils.cpp
/// Implementation file for "test_serialisation_utils.hpp"

//  Copyright (c) 2007-2022, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/mfc/twist_mfc_maker.hpp"

#include "twist/mfc/test/test_serialisation_utils.hpp"

#include "twist/mfc/serialisation_utils.hpp"

namespace twist::mfc::test {

// --- MemArchive class ---

//! Ties writable and readable CArchive instances to a memory buffer, for serialisation testing.
class MemArchive {
public:
	MemArchive()
	{
		writable_mem_file_ = std::make_unique<CMemFile>();
		writable_archive_ = std::make_unique<CArchive>(writable_mem_file_.get(), CArchive::store);
	}

	auto get_writable_archive() -> CArchive&
	{
		if (!writable_archive_) {
			TWIST_THROW(L"There is no writable archive. Call this method before calling flush().");
		}	
		return *writable_archive_;
	}

	auto flush() -> void
	{
		if (!writable_archive_) {
			TWIST_THROW(L"There is no writable archive. You cannot call this this method twice.");
		}	
		writable_archive_->Flush();
		data_size_ = static_cast<unsigned int>(writable_mem_file_->GetLength());
		data_ = writable_mem_file_->Detach();

		writable_archive_.reset();
		writable_mem_file_.reset();

		readable_mem_file_ = std::make_unique<CMemFile>(data_, data_size_);
		readable_archive_ = std::make_unique<CArchive>(readable_mem_file_.get(), CArchive::load);
	}

	auto get_readable_archive() -> CArchive&
	{
		if (!readable_archive_) {
			TWIST_THROW(L"There is no readable archive. Call this method after calling flush().");
		}
		return *readable_archive_;
	}
	
	TWIST_NO_COPY_NO_MOVE(MemArchive)

private:
	std::unique_ptr<CMemFile> writable_mem_file_;
	std::unique_ptr<CArchive> writable_archive_;
	std::unique_ptr<CMemFile> readable_mem_file_;
	std::unique_ptr<CArchive> readable_archive_;
	unsigned int data_size_{0};
	unsigned char* data_{nullptr};
};

// --- Local functions ---

template<class Cont>
auto test_stl_container(const Cont& in_cont)
{
	auto mem_archive = MemArchive{};
	auto& writable_ar = mem_archive.get_writable_archive();

	writable_ar << in_cont; 

	mem_archive.flush();

	auto& readable_ar = mem_archive.get_readable_archive();
	
	auto out_cont = Cont{};
	readable_ar >> out_cont;

	assert(in_cont == out_cont);
}

// --- Global functions ---

auto test_stl_container_archiving() -> void
{
	test_stl_container(std::vector<int>{1, 5, 8});

	test_stl_container(std::vector<std::wstring>{L"one", L"five", L"eight"});

	test_stl_container(std::map<double, std::wstring>{{1.0, L"one"}, {5.0, L"five"}, {8.0, L"eight"}});
}

}

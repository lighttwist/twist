///  @file  CtrlEditFloatHelper.ipp
///  Inline implementation file for "CtrlEditFloatHelper.hpp"

//   Copyright (c) 2007-2019 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

namespace twist::mfc {	

template<class TEditCtrl>
CtrlEditFloatHelper<TEditCtrl>::CtrlEditFloatHelper(TEditCtrl& ctrl, unsigned int precision, FloatStrFormat format, wchar_t decimal_sep) 
	: ctrl_(ctrl)
	, special_text_()
	, has_focus_(false)
	, precision_(precision)
	, format_(format)
	, decimal_sep_(decimal_sep)
	, c_runtime_decimal_sep_(get_c_runtime_decimal_sep())
{
	// We do not include the decimal separator in the list of default allowable characters, as that is settable per 
	// class instance 
	static const wchar_t k_def_allowed_chars[] = {L'0', L'1', L'2', L'3', L'4', L'5', L'6', L'7', L'8', L'9', L'0', L'-', VK_BACK};
	
	allowed_chars_.insert(std::begin(k_def_allowed_chars), std::end(k_def_allowed_chars));
	allowed_chars_.insert(decimal_sep_);

	TWIST_CHECK_INVARIANT
}


template<class TEditCtrl>
CtrlEditFloatHelper<TEditCtrl>::~CtrlEditFloatHelper()
{
	TWIST_CHECK_INVARIANT
}


template<class TEditCtrl>
CString CtrlEditFloatHelper<TEditCtrl>::GetText() const
{
	TWIST_CHECK_INVARIANT
	CString text;
	ctrl_.GetWindowText(text);	
	return text;
}


template<class TEditCtrl>
double CtrlEditFloatHelper<TEditCtrl>::GetFloat() const
{
	TWIST_CHECK_INVARIANT
	double ret = 0;

	auto text = get_wnd_text(ctrl_);
	assert(!text.empty());
	if (!text.empty()) {
		// For the transformation from text to number, revert the text to the default decimal separator (if necessary) 
		if (decimal_sep_ != c_runtime_decimal_sep_) {
			auto it = find(text, decimal_sep_);
			if (it != end(text)) {
				*it = c_runtime_decimal_sep_;
			}
		}
		ret = _wtof(text.c_str());
	}
	
	return ret;
}


template<class TEditCtrl>
void CtrlEditFloatHelper<TEditCtrl>::SetFloat(double value)
{
	TWIST_CHECK_INVARIANT
	set_wnd_text(ctrl_, to_str(value, precision_, format_, decimal_sep_));
	TWIST_CHECK_INVARIANT
}


template<class TEditCtrl>
bool CtrlEditFloatHelper<TEditCtrl>::IsEmpty() const
{
	TWIST_CHECK_INVARIANT
	return get_wnd_text(ctrl_).empty();
}


template<class TEditCtrl>
wchar_t CtrlEditFloatHelper<TEditCtrl>::GetDecimalSep() const
{
	TWIST_CHECK_INVARIANT
	return decimal_sep_;
}


template<class TEditCtrl>
void CtrlEditFloatHelper<TEditCtrl>::SetSpecialText(CString special_text)
{
	TWIST_CHECK_INVARIANT
	special_text_ = special_text;	
	TWIST_CHECK_INVARIANT
}


template<class TEditCtrl>
void CtrlEditFloatHelper<TEditCtrl>::DisplaySpecialText()
{
	TWIST_CHECK_INVARIANT
	assert(!special_text_.IsEmpty());
	if (!special_text_.IsEmpty()) {
		ctrl_.SetWindowText(special_text_);
	}
	TWIST_CHECK_INVARIANT
}


template<class TEditCtrl>
bool CtrlEditFloatHelper<TEditCtrl>::BeforeChar(UINT chr_code)
{
	TWIST_CHECK_INVARIANT

	const wchar_t chr = static_cast<wchar_t>(chr_code);

	bool ret = allowed_chars_.count(chr) != 0;

	if (ret) {
		int selStart = 0;
		int selEnd = 0; 
		ctrl_.GetSel(selStart, selEnd);

		CString text;
		ctrl_.GetWindowText(text);

		// If the control currently displays the "special text", zap it.
		if (!special_text_.IsEmpty()) {
			if (text.Compare(special_text_) == 0) {
				ctrl_.SetWindowText(L"");
				text = L"";
			}
		}

        // Anything goes if all the text is selected.
        if ((selEnd - selStart) != text.GetLength()) {
		
			if (chr == L'-') {
				// Only allow the minus sign if it is the first character in the control.
				ret = (selStart == 0);
			}			
			if (ret) {
				// Do not allow input before a leading minus sign
				if (selStart <= text.Find(L'-')) {
					ret = false;
				}
            }            
            if (ret) {
                // Only allow one decimal point.
                if ((chr == decimal_sep_) && (text.Find(decimal_sep_) != -1)) {
					ret = false;
                }
            }
            if (ret) {
                // Only allow one, leading minus sign.
                if ((chr == L'-') && ((selStart > 0) || (text.Find(L'-') != -1))) {
                    ret = false;
                }
            }           
		}
	}
	return ret;
}


template<class TEditCtrl>
void CtrlEditFloatHelper<TEditCtrl>::AfterLButtonDown()
{	
	TWIST_CHECK_INVARIANT
	// Check the "has focus" flag before calling 'CEdit::OnLButtonDown', as that call can trigger a "set focus" 
	// event itself.
	if (!has_focus_) {
		if (!special_text_.IsEmpty()) {
			CString text;
			ctrl_.GetWindowText(text);
			if (text.Compare(special_text_) == 0) {
				// The "special text" is currently displayed in the control, and the control has just received focus 
				// by clicking. Select the whole text.
				ctrl_.SetSel(0, -1);
			}
		}
	}
	TWIST_CHECK_INVARIANT
}


template<class TEditCtrl>
void CtrlEditFloatHelper<TEditCtrl>::AfterSetFocus()
{
	TWIST_CHECK_INVARIANT
	if (!special_text_.IsEmpty()) {
		CString text;
		ctrl_.GetWindowText(text);
		if (text.Compare(special_text_) == 0) {
			// The "special text" is currently displayed in the control, and the control has just received focus. 
			// Select the whole text.
			ctrl_.SetSel(0, -1);
		}
	}
	has_focus_ = true;
	TWIST_CHECK_INVARIANT
}


template<class TEditCtrl>
void CtrlEditFloatHelper<TEditCtrl>::AfterKillFocus()
{
	TWIST_CHECK_INVARIANT
	has_focus_ = false;
	TWIST_CHECK_INVARIANT
}


#ifdef _DEBUG
template<class TEditCtrl>
void CtrlEditFloatHelper<TEditCtrl>::check_invariant() const noexcept
{
	assert(decimal_sep_ > 0);
	assert(!allowed_chars_.empty());
}
#endif // _DEBUG

}

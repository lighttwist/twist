/// @file  serialisation_utils.ipp
/// Inline implementation file for "serialisation_utils.hpp"

//  Copyright (c) 2007-2022, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace twist::mfc {

template<class Elem, class Allocator>
auto operator<<(CArchive& ar, const std::vector<Elem, Allocator>& obj) -> CArchive&
{
	const auto size = std::int64_t{ssize(obj)};
	ar << size;
	for (const auto& elem : obj) {
		ar << elem;
	}
	return ar;
}

template<class Elem, class Allocator>
auto operator>>(CArchive& ar, std::vector<Elem, Allocator>& obj) -> CArchive&
{
	auto size = std::int64_t{0};
	ar >> size;

	obj.clear();
	obj.reserve(static_cast<std::vector<Elem, Allocator>::size_type>(size));
	
	for ([[maybe_unused]] auto i : IndexRange{size}) {
		auto elem = Elem{};
		ar >> elem;
		obj.push_back(std::move(elem));
	}
	return ar;
}

template<class Key, class Value, class Compare, class Allocator>
auto operator<<(CArchive& ar, const std::map<Key, Value, Compare, Allocator>& obj) -> CArchive&
{
	const auto size = std::int64_t{ssize(obj)};
	ar << size;
	for (const auto& [key, value] : obj) {
		ar << key << value;
	}
	return ar;
}

template<class Key, class Value, class Compare, class Allocator>
auto operator>>(CArchive& ar, std::map<Key, Value, Compare, Allocator>& obj) -> CArchive&
{
	auto size = std::int64_t{0};
	ar >> size;

	obj.clear();
	
	for ([[maybe_unused]] auto i : twist::IndexRange{size}) {
		auto key = Key{};
		auto value = Value{};
		ar >> key;
		ar >> value;
		obj.emplace(std::move(key), std::move(value));
	}
	return ar;
}

} 

///  @file  CtrlList.hpp
///  CtrlList  class, inherits CListCtrl

//   Copyright (c) 2007-2019 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_MFC_CTRL_LIST_HPP
#define TWIST_MFC_CTRL_LIST_HPP

namespace twist::mfc {

///
///  Control extending the functionality of the classic CListCtrl (eg with drag-and-drop abilities).
///  Note: This list has only been tested for lists that have the "report" view type.
///
class CtrlList : public CListCtrl {
public:
	CtrlList();

	virtual ~CtrlList();

	/// Set whether the list should be reorderable by drag-dropping its items.
	///
	/// @param[in]  on  true for on. Default is off.
	///
	void SetDragDropReorder(bool on); 

protected:
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	
	afx_msg void OnLvnBegindrag(NMHDR* pNMHDR, LRESULT* pResult);

	DECLARE_DYNAMIC(CtrlList)

	DECLARE_MESSAGE_MAP()

private:
	/// Drop the list item being dragged back onto the list.
	/// If the dragged item is hovering over another list item, the dragged item will take that position, otherwise nothing
	/// happens, except the drag-drop operation being concluded.
	/// 
	void DropItem();	

	/// Create a drag image list for a specific list item. 
	/// This function's responsibility is the same as that of  CListCtrl::CreateDragImage() . The trouble is that the latter
	/// function does not always work properly (e.g. perhaps when the application is a "dll" rather than an "exe") so we 
	/// needed a local implementation. Additionally of course a local implementation can be refined further.
	///
	/// @param[in]  itemIdx  The list item index.
	/// @return  The image list. Caller takes ownership.
	///
	CImageList* CreateDragImage(long itemIdx);

	// Whether the list should be reorderable by drag-dropping its items.
	bool  m_dragDropReorder;
	
	// Whether a drag-drop operation is in progress.
	bool  m_dragging;
	
	// The drag image list for the list item being dragged, while a drag-drop operation is in progress;  nullptr  otherwise
	CImageList*  m_dragImg;
	
	// The index of the list item being dragged, while a drag-drop operation is in progress;  k_no_item  otherwise
	int  m_dragItemIdx;

	// The index of the list item over which the drag item is currently hovering (the "drop target") if any;
	// k_no_item  the rest of the time.
	int  m_dropItemIdx;
	
	TWIST_CHECK_INVARIANT_DECL
};

}

#endif 

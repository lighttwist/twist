///  @file  CtrlListOwnerDrawBase.hpp
///  CtrlListOwnerDrawBase  class, inherits CListCtrl

//   Copyright (c) 2007-2019 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_MFC_CTRL_LIST_OWNER_DRAW_BASE_HPP
#define TWIST_MFC_CTRL_LIST_OWNER_DRAW_BASE_HPP

namespace twist::mfc {

///
///  Abstract base class for owner-draw list controls.
///  Adds some useful functionality to the classic  CListCtrl  for the special case where it is owner-drawn.
///  The associated resource must have the "owner draw fixed" flag set to true and the "report" view type.
///
class CtrlListOwnerDrawBase : public CListCtrl {
public:
	CtrlListOwnerDrawBase();

	virtual ~CtrlListOwnerDrawBase();

	/// Initialise the list control. Always call this method before populating the list for the first time.
	/// Note that sometimes you need to add the macro  ON_WM_MEASUREITEM_REFLECT()  in the derived class's message map for a
	/// non-zero item height to work.
	///
	/// @param[in]  showSel  Whether the current selection (and focusing) should be visually indicated in the list.
	/// @param[in]  itemHeight  The desired item height (pixels). Pass in zero for the default.
	///
	void Init(bool showSel = true, int itemHeight = 0);

	/// Get the number of columns in the list control.
	///
	/// @return  The column count.
	///
	int CountColumns() const;

	/// Find out how wide a specific list column should be so that the content of all the cells (subitems) in the column 
	///   can be fully drawn.
	/// Note that by default this method returns zero. Override  GetSubitemContentWidth()  in derived classes if you need 
	///   this method to work. Additionally, this method will only return a proper result after the list control has been 
	///   fully drawn at least once.
	/// 
	/// @param[in]  colIdx  The column index.
	/// @return  The width.
	///
	int GetMaxColContentWidth(int colIdx) const;

	/// Clears all stored maximum column content widths.
	/// Call this when the list contents have changed, and you wish the next redraw action to recompute the max wTIdhs 
	/// from scratch.
	///
	void ZapMaxColsContentWidth();

protected:
	/// Get the bounding rectangle for a subitem (cell) of the list.
	///
	/// @param[in]  itemRect  The bounding rectangle of the whole item (row).
	/// @param[in]  colIdx  The subitem column index.
	/// @return  The subitem bounding rectangle.
	///
	CRect GetSubitemRect(const CRect& itemRect, int colIdx) const;

	/// Update the maximum content width for a give column, if necessary.
	/// This method is called when the content width of a subitem in that column becomes known.
	///
	/// @param[in]  colIdx  The column index.
	/// @param[in]  width  The subitem content width.
	///
	void UpdateMaxColContentWidth(int colIdx, int width);

	/// Get the width that a subitem should be so that its content can be fully drawn.
	/// Note that this implementation returns zero. Override in derived classes if you need  GetSubitemContentWidth()  
	/// to work.
	///
	/// @param[in]  rowIdx  The subitem row index.
	/// @param[in]  colIdx  The subitem column index.
	/// @param[in]  dc  Handle to the device context.
	/// @return  The width.
	///
	virtual int GetSubitemContentWidth(int rowIdx, int colIdx, HDC dc) const;

	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct) override;

	afx_msg void MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct);

	DECLARE_DYNAMIC(CtrlListOwnerDrawBase)

	DECLARE_MESSAGE_MAP()

private:
	typedef std::map<int, int>  ColumnWidthMap;

	/// Draw a particular item (row) of the list (that is, draw all its subitems).
	/// 
	/// @param[in]  rowIdx  The row index.
	/// @param[in]  itemRect  The item bounding rectangle.
	/// @param[in]  dc  Handle to the device context.
	///
	virtual void DrawSubitems(int rowIdx, const CRect& itemRect, HDC dc) = 0;

	bool  show_sel_;  // Whether the current selection (and focusing) should be visually indicated in the list
	int  item_height_;  // The item height (pixels); zero means default
	ColumnWidthMap  max_col_content_widths_;  // Map used to keep track of the maximum content width for all columns

	HBRUSH  white_hbrush_;
	CBrush  selected_brush_;
	CBrush  focused_brush_;

	TWIST_CHECK_INVARIANT_DECL
};

}

#endif 

///  @file  CtrlEditFloat.cpp
///  Implementation file for "CtrlEditFloat.hpp"
//
//   Copyright (c) 2007-2019 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist_mfc_maker.hpp"

#include "CtrlEditFloat.hpp"

#include "wnd_utils.hpp"

namespace twist::mfc {

IMPLEMENT_DYNAMIC(CtrlEditFloat, CEdit)

BEGIN_MESSAGE_MAP(CtrlEditFloat, CEdit)
	ON_WM_CHAR()
	ON_WM_LBUTTONDOWN()
	ON_WM_KILLFOCUS()
	ON_WM_SETFOCUS()
END_MESSAGE_MAP()


CtrlEditFloat::CtrlEditFloat(unsigned int precision, FloatStrFormat format, wchar_t decimal_sep) 
	: CEdit()
	, helper_()
{
	helper_ = std::make_unique<CtrlEditFloatHelper<CtrlEditFloat>>(*this, precision, format, decimal_sep);
	TWIST_CHECK_INVARIANT
}


CtrlEditFloat::~CtrlEditFloat()
{
	TWIST_CHECK_INVARIANT
}


CString CtrlEditFloat::GetText() const
{
	TWIST_CHECK_INVARIANT
	return helper_->GetText();
}


double CtrlEditFloat::GetFloat() const
{
	TWIST_CHECK_INVARIANT
	return helper_->GetFloat();
}


void CtrlEditFloat::SetFloat(double value)
{
	TWIST_CHECK_INVARIANT
	helper_->SetFloat(value);
	TWIST_CHECK_INVARIANT
}


bool CtrlEditFloat::IsEmpty() const
{
	TWIST_CHECK_INVARIANT
	return helper_->IsEmpty();
}


wchar_t CtrlEditFloat::GetDecimalSep() const
{
	TWIST_CHECK_INVARIANT
	return helper_->GetDecimalSep();
}


void CtrlEditFloat::SetSpecialText(CString special_text)
{
	TWIST_CHECK_INVARIANT
	helper_->SetSpecialText(special_text);	
	TWIST_CHECK_INVARIANT
}


void CtrlEditFloat::DisplaySpecialText()
{
	TWIST_CHECK_INVARIANT
	helper_->DisplaySpecialText();
	TWIST_CHECK_INVARIANT
}


void CtrlEditFloat::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	TWIST_CHECK_INVARIANT
	if (helper_->BeforeChar(nChar)) {
		CEdit::OnChar(nChar, nRepCnt, nFlags);
	}
	TWIST_CHECK_INVARIANT
}


void CtrlEditFloat::OnLButtonDown(UINT nFlags, CPoint point)
{	
	TWIST_CHECK_INVARIANT
	CEdit::OnLButtonDown(nFlags, point);
	helper_->AfterLButtonDown();
	TWIST_CHECK_INVARIANT
}


void CtrlEditFloat::OnSetFocus(CWnd* pOldWnd)
{
	TWIST_CHECK_INVARIANT
	CEdit::OnSetFocus(pOldWnd);
	helper_->AfterSetFocus();
	TWIST_CHECK_INVARIANT
}


void CtrlEditFloat::OnKillFocus(CWnd* pNewWnd)
{
	TWIST_CHECK_INVARIANT
	CEdit::OnKillFocus(pNewWnd);
	helper_->AfterKillFocus();
	TWIST_CHECK_INVARIANT
}


#ifdef _DEBUG
void CtrlEditFloat::check_invariant() const noexcept
{
	assert(helper_);
}
#endif // _DEBUG

}

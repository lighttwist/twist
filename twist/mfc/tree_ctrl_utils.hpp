///  @file  tree_ctrl_utils.hpp
///  Utilities for working with the MFC CTreeCtrl class

//   Copyright (c) 2010-2019 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_MFC_TREE__CTRL__UTILS_HPP
#define TWIST_MFC_TREE__CTRL__UTILS_HPP

#include "twist/math/RowwiseMatrix.hpp"

namespace twist::mfc {

// Function called by some of the tree (or subtree) exploration functions for a tree item.
//
// @param[in]  item  The item handle
// @param[in]  depth  The number of levels from either the root item (if the whole tree is explored) or the top item of the 
//				subtree (if a subtree is explored) to the current item; zero means root/top item
//
typedef std::function<void (HTREEITEM item, unsigned int depth)>  TreeItemFunc;

/// Select a tree control item.
///
/// @param[in]  ctrl  The tree control
/// @param[in]  item  The item handle
/// @return  true if the item is selected successfully
///
bool select_item(CTreeCtrl& ctrl, HTREEITEM item);

/// Find out whether a tree control item has any child items.
///
/// @param[in]  ctrl  The tree control
/// @param[in]  item  The item handle
/// @return  true if it does
///
bool item_has_children(const CTreeCtrl& ctrl, HTREEITEM item);  

/// Ensure that a tree view item is visible. If necessary, the function expands the parent item or scrolls the tree view control 
/// so that the item is visible.
///
/// @param[in]  ctrl  The tree control
/// @param[in]  item  The item handle
/// @return  true if the items in the tree-view control are scrolled to ensure that the specified item is visible
///
bool ensure_visible(CTreeCtrl& ctrl, HTREEITEM item);  
	
/// Expand a tree control item and all its descendant items (the item's subtree).
///
/// @param[in]  ctrl  The tree control
/// @param[in]  top_item  Handle of the item whose subtree is to be expanded
///
void expand_subtree(CTreeCtrl& ctrl, HTREEITEM top_item);

///+ gets called for all items in the subtree, including the top item
///
void explore_subtree(const CTreeCtrl& ctrl, HTREEITEM top_item, TreeItemFunc item_func);

///+ Only gets called for leaf items in the subtree
void explore_subtree_leaves(const CTreeCtrl& ctrl, HTREEITEM top_item, TreeItemFunc item_func);

/// Given a top item, explore all the leaf items in its subtree and calculate the longest distance (or depth) from any such leaf
/// to the top item. The top item counts as a level of depth too, so if the subtree is made up entirely of the top node (ie if 
/// the top node is a leaf), the subtree depth will be one.
///
/// @param[in]  ctrl  The tree control
/// @param[in]  top_item  Handle of the top item in the subtree
/// @return  The subtree depth
///
unsigned int get_subtree_depth(const CTreeCtrl& ctrl, HTREEITEM top_item);

///+TODO: Comment
[[nodiscard]] auto subtree_to_matrix(const CTreeCtrl& ctrl, HTREEITEM top_item) 
                    -> twist::math::RowwiseMatrix<HTREEITEM>;

} 

#endif 


///  @file  ModelessDialogsMgr.hpp
///  ModelessDialogsMgr  class template

//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_MFC_MODELESS_DIALOGS_MGR_HPP
#define TWIST_MFC_MODELESS_DIALOGS_MGR_HPP

namespace twist::mfc {

///
///  Class template which manages a collection of modeless dialogues, each associated with a unique key and all having a common 
///  parent window. This class is responsible for showing and keeping track of the modeless dialogues.
///
template<typename TKey>
class ModelessDialogsMgr : public NonCopyable {
public:
	/// Functor type for creating new modeless dialogue instances.
	///
	/// @param[in]  key  The key corresponding to the dialogue to be created
	/// @param[out]  res_id  The dialogue resource ID
	/// @param[out]  caption  The dialogue caption
	/// @return  The new dialogue instance
	/// 
	typedef std::function<std::unique_ptr<CDialog> (const TKey& key, unsigned int& res_id, std::wstring& caption)>  CreateDlg;
	 
	/// Constructor.
	///	
	/// @param[in]  dialogs_parent_wnd  The parent window for all the dialogues managed by this class
	/// @param[in]  create_dlg  Functor for creating new dialogue instances
	///
	ModelessDialogsMgr(CWnd& dialogs_parent_wnd, CreateDlg create_dlg);

	/// Destructor.
	///
	virtual ~ModelessDialogsMgr();

	/// Show a specific modeless dialogue. If the dialogue is already visible, it receives focus.
	///
	/// @param[in]  key  The unique key corresponding to the dialogue
	///
	void show_dlg(const TKey& key);

private:
	CWnd&  dialogs_parent_wnd_;
	CreateDlg  create_dlg_;

	std::map<TKey, std::unique_ptr<CDialog>>  dialogs_;

	TWIST_CHECK_INVARIANT_DECL
};

}

#include "ModelessDialogsMgr.ipp"

#endif 

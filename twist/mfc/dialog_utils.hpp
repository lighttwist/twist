/// @file  dialog_utils.hpp
/// Utilities for working with CDialog and derived classes

//  Copyright (c) 2007-2022, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_MFC_DIALOG__UTILS_HPP
#define TWIST_MFC_DIALOG__UTILS_HPP

class CDialog;
class CMenu;
class CWnd;

namespace twist::mfc {

// Question box types
enum class QuestionBoxType {
	yes_no         = 1,
	yes_no_cancel  = 2
};

// Question box answer types
enum class QuestionBoxAnswer {
	yes     = 1,
	no      = 2,
	cancel  = 3
};

/*! Display a dialog modally. The function returns when the dialog is closed.
    \return  true if the dialog was closed by pressing OK (or ENTER)
 */
bool do_modal(CDialog& dlg);

/*! Create a dialog object and display the associated window modally. The function returns when the dialog is closed.
    \tparam Dlg  The dialog class. Must inherit CDialog.
	\tparam Args  The types of the dialog class contructor arguments.
	\param args  The dialog class constructor arguments.
    \return  true if the dialog was closed by pressing OK (or ENTER)
 */
template<class Dlg, 
         class... Args>
auto do_modal(Args&&... args) -> EnableIfBaseOf<CDialog, Dlg, bool>;

/// Display an "open file" modal dialog. The user is allowed to select a single file.
///
/// @param[in]  file_title  The title of the file type (as displayed next to the allowed file extensions)
/// @param[in]  file_extensions  The allowed file extensions (one or more)
/// @param[out]  filename  The chosen filename, if the user chooses a file successfully
/// @param[in]  dlg_caption  The dialog caption. Pass in an empty string for the default caption
/// @param[in]  init_dir  The directory which will be displayed initially by the dialog; pass in an empty string for the 
///					default directory
/// @return  true if the user chooses a file successfully
///
bool do_load_file_dialog(const std::wstring& file_title, const std::vector<std::wstring>& file_extensions, 
		fs::path& filename, const std::wstring& dlg_caption = L"", const fs::path& init_dir = L"");

/// Display an "open files" modal dialog. The user is allowed to select one or more file.
///
/// @param[in]  file_title  The title of the file type (as displayed next to the allowed file extensions)
/// @param[in]  file_extensions  The allowed file extensions (one or more)
/// @param[out]  filenames  The chosen filenames, if the user chooses file(s) successfully
/// @param[in]  dlg_caption  The dialog caption; pass in an empty string for the default caption
/// @param[in]  init_dir  The directory which will be displayed initially by the dialog. Pass in an empty string for the 
///					default directory
/// @return  true if the user chooses file(s) successfully
///
bool do_load_files_dialog(const std::wstring& file_title, const std::vector<std::wstring>& file_extensions, 
		std::vector<fs::path>& filenames, const std::wstring& dlg_caption = L"", const fs::path& init_dir = L"");

/// Display a "save file" modal dialog. The user is allowed to select a single file.
///
/// @param[in]  file_title  The title of the file type (as displayed next to the allowed file extensions).
/// @param[in]  file_extensions  The allowed file extensions (one or more).
/// @param[out]  filename  The chosen filename, if the user chooses a file successfully.
/// @param[in]  dlg_caption  The dialog caption. Pass in an empty string for the default caption.
/// @param[in]  init_dir  The directory which will be displayed initially by the dialog. Pass in an empty string for the 
///					default directory.
/// @return  true if the user chooses a file successfully.
///
bool do_save_file_dialog(const std::wstring& file_title, const std::vector<std::wstring>& file_extensions, fs::path& filename,
		const std::wstring& dlg_caption = L"", const fs::path& init_dir = L"");

/// Show a modal dialogue displaying an error message (a standard error message box).
/// The message box parent will be the application main window, if it exists.
///
/// @param[in]  message  The message
/// @param[in]  caption  The dialogue caption; pass in a blank string for a default caption
///
void show_error_box(const std::wstring& message, const std::wstring& caption = L"");

/// Show a modal dialogue displaying an error message (a standard error message box).
///
/// @param[in]  parent  The parent window of the message box
/// @param[in]  message  The message
/// @param[in]  caption  The dialogue caption; pass in a blank string for a default caption
///
void show_error_box(const CWnd& parent, const std::wstring& message, const std::wstring& caption = L"");

/// Show a modal dialogue displaying the error message contained in an exception object (as a standard error message box).
///
/// @param[in]  parent  The parent window of the message box
/// @param[in]  ex  The exception
/// @param[in]  caption  The dialogue caption; pass in a blank string for a default caption
///
void show_error_box(const CWnd& parent, const std::exception& ex, const std::wstring& caption = L"");

/// Show a modal dialogue displaying an information message (a standard information message box).
/// The message box parent will be the application main window, if it exists.
///
/// @param[in]  message  The message
/// @param[in]  caption  The dialogue caption; pass in a blank string for a default caption
///
void show_info_box(const std::wstring& message, const std::wstring& caption = L"");

/// Show a modal dialogue displaying an information message (a standard information message box).
///
/// @param[in]  parent  The parent window of the message box
/// @param[in]  message  The message
/// @param[in]  caption  The dialogue caption; pass in a blank string for a default caption
///
void show_info_box(const CWnd& parent, const std::wstring& message, const std::wstring& caption = L"");

/*! Show a modal dialog displaying a question and offering two possible answers "yes" and "no" (a standard 
    question message box with two buttons).
     \param[in] parent  The parent window of the message box
     \param[in] message  The message
     \param[in] caption  The dialog caption
     \param[in] type  The box type
     \return  The answer provided by the user
 */
auto show_question_box(const CWnd& parent, const std::wstring& message, const std::wstring& caption = L"", 
        QuestionBoxType type = QuestionBoxType::yes_no) -> QuestionBoxAnswer;

/*! Show a modal dialog displaying a message and an exclamation-point icon.
     \param[in] parent  The parent window of the message box
     \param[in] message  The message
     \param[in] caption  The dialog caption
 */
auto show_warning_box(const CWnd& parent, const std::wstring& message, const std::wstring& caption = L"") -> void;

/// Changing the state (enable/disable, check/uncheck, change text) of a menu item from its CN_UPDATE_COMMAND_UI command handler 
///   does not work correctly if the menu is attached to a dialogue box. See http://support.microsoft.com/kb/q242577
/// In order to fix this problem, a handler for WM_INITMENUPOPUP needs to be added to the dialogue, and the handler needs to call 
///   this function.
///
/// @param[in]  dlg  The dialogue
/// @param[in]  popup_menu  The popup menu
///
void on_init_menu_popup(CDialog& dlg, CMenu& popup_menu);

/// Hide a specific control in a dialog box.
/// 
/// @param  dlg  [in] The dialog 
/// @param  item_id  [in] The identifier of the control (eg the resource ID)
///
void hide_dlg_ctrl(CDialog& dlg, UINT ctrl_id);

/// Move a specific control in a dialog box.
/// 
/// @param[in]  dlg_hwnd  The dialog window handle
/// @param[in]  item_id  The identifier of the control (eg the resource ID)
/// @param[in]  dx  The horizontal distance that the control should move by (pixels, a positive value means 
///					move to the right, a negative value to the left) 
/// @param[in]  dy  The vertical distance that the control should move by (pixels, a positive value means  
///					move down, a negative value move up) 
/// @param[in]  repaint  Whether the control (and any part of dialog that was uncovered) should be repainted 
///					after the move  
///
void move_dlg_ctrl(HWND dlg_hwnd, UINT ctrl_id, int dx, int dy, bool repaint = true);

}

#include "dialog_utils.ipp"

#endif 

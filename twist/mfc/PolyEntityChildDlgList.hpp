///  @file  PolyEntityChildDlgList.hpp
///  PolyEntityChildDlgList  class template

//   Copyright (c) 2010-2019 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_MFC_POLY_ENTITY_CHILD_DLG_LIST_HPP
#define TWIST_MFC_POLY_ENTITY_CHILD_DLG_LIST_HPP

#include "ChildDlgList.hpp"
#include "PolyEntityChildDlg.hpp"

namespace twist::mfc {

///
///  Maintains a list of instances of classes derived from PolyEntityChildDlg, which are embedded as children 
///    into a parent window and provides some common functionality for the children. 
///  They are keyed on entity type.
///	 One (or zero) child dialog is active and visible at any one time.
///
template<class Enty, class EntyTyp>
class PolyEntityChildDlgList : public twist::NonCopyable {
public:
	typedef PolyEntityChildDlg<Enty, EntyTyp>  ChildDlg;

	/// Constructor.
	///
	/// @param[in]  parent  The parent window for all dialogs in the list.
	///
	PolyEntityChildDlgList(CWnd& parent);

	/// Destructor.
	///
	virtual ~PolyEntityChildDlgList();

	/// Add a new child dialog to the list.
	///
	/// @tparam  ChildDlgClass  The class type of the child dialog to be added
	/// @param[in]  entityType  The entity type.
	/// @param[in]  resId  The child dialog resource ID.
	/// @param[in]  changedListener  "Enty changed" event listener.
	/// @param[in]  validateListener  "Validate entity" event listener.
	/// @return  The new child dialog.
	///
	template<class ChildDlgClass,
			 class = EnableIfBaseOf<ChildDlg, ChildDlgClass>>
	ChildDlgClass& add(EntyTyp entityType, long resId, 
			typename ChildDlg::ChangedListener changedListener, 
			typename ChildDlg::ValidateListener validateListener);

	/// Set the (properties of the) entity being edited in a specific child dialog. The dialog controls are 
	/// updated with the new values.
	///
	/// @param[in]  entity  The entity. 
	/// @param[in]  entityType  The entity type.
	///
	void setEntity(const Enty& entity, EntyTyp entityType);

	/// Move all child dialogs to the same position.
	///
	/// @param[in]  dlgRect  The new dialog position (relative to the parent window).
	///
	void moveAll(const CRect& dlgRect);

	/// Get the currently active child dialog. An exception is thrown if there is no dialog is ative.
	///
	/// @return  The active child dialog.
	///
	ChildDlg& getActive();

	/// Set a specific child dialog.
	///
	/// @param[in]  entityType  The entity type.
	///
	void setActive(EntyTyp entityType);

private:	
	ChildDlgList<EntyTyp>  m_dlgList;

	TWIST_CHECK_INVARIANT_DECL
};

}

#include "PolyEntityChildDlgList.ipp"

#endif 

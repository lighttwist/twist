///  @file  wnd_utils.cpp
///  Implementation file for "wnd_utils.hpp"

//   Copyright (c) 2010-2019 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist_mfc_maker.hpp"

#include "wnd_utils.hpp"

namespace twist::mfc {

// --- RedrawFreezer class ---

RedrawFreezer::RedrawFreezer(CWnd& wnd, bool do_wait_cursor)
	: wnd_(wnd)
{
	wnd_.SetRedraw(FALSE);
	if (do_wait_cursor) {
		wait_cursor_ = std::make_unique<CWaitCursor>();
	}
}

RedrawFreezer::~RedrawFreezer()
{
	wnd_.SetRedraw(TRUE);
}

// --- Global functions ---

HWND hwnd(const CWnd& wnd)
{
	return wnd.m_hWnd;
}

void enable_wnd(CWnd& wnd, bool enable)
{
	wnd.EnableWindow(enable ? TRUE : FALSE);
}


bool is_wnd_enabled(const CWnd& wnd)
{
	return wnd.IsWindowEnabled() == TRUE;
}


CRect get_wnd_rect(const CWnd& wnd)
{
	CRect rect;
	wnd.GetWindowRect(rect);
	return rect;
}


CRect get_client_rect(const CWnd& wnd)
{
	CRect rect;
	wnd.GetClientRect(rect);
	return rect;
}


CRect screen_to_client(const CRect& rect, const CWnd& wnd)
{
	CRect ret(rect);
	wnd.ScreenToClient(&ret);
	return ret;
}


CRect screen_to_client(const CRect& rect, HWND hwnd)
{
	CPoint topLeft(rect.TopLeft());		
	CPoint bottomRight(rect.BottomRight());

	BOOL ret = ::ScreenToClient(hwnd, &topLeft);
	assert(ret != FALSE);

	ret = ::ScreenToClient(hwnd, &bottomRight);
	assert(ret != FALSE);
	
	return CRect(topLeft, bottomRight);	
}


CPoint screen_to_client(const CPoint& point, const CWnd& wnd)
{
	CPoint ret(point);
	wnd.ScreenToClient(&ret);
	return ret;
}


CPoint client_to_screen(const CPoint& point, const CWnd& wnd)
{
	CPoint ret(point);
	wnd.ClientToScreen(&ret);
	return ret;
}


CRect client_to_screen(const CRect& rect, const CWnd& wnd)
{
	CRect ret(rect);
	wnd.ClientToScreen(&ret);
	return ret;
}


bool hide_wnd(CWnd& wnd)
{
	return wnd.ShowWindow(SW_HIDE) == TRUE;
}


void move_wnd(CWnd& wnd, const CRect& rect, bool repaint)
{
	wnd.MoveWindow(&rect, repaint ? TRUE : FALSE);
}


CSize get_wnd_size(HWND hwnd)
{
	CRect wndRect;
	::GetWindowRect(hwnd, &wndRect);
	return CSize(wndRect.Width(), wndRect.Height());
}


std::wstring get_wnd_text(const CWnd& wnd)
{
	CString text;
	wnd.GetWindowText(text);
	return text.GetString();
}


std::wstring get_wnd_text(HWND hwnd)
{
	const size_t k_buffer_len = 1024;
	wchar_t buffer[k_buffer_len];

	const int ret = ::GetWindowText(hwnd, buffer, k_buffer_len);
	if (ret == 0) {
		return L"";
	}
	return buffer;
}


void set_wnd_text(CWnd& wnd, const std::wstring& text)
{
	wnd.SetWindowText(text.c_str());
}


bool is_wnd_visible(const CWnd& wnd)
{
	return wnd.IsWindowVisible() == TRUE;
}


void move_child_wnd(HWND parent_hwnd, HWND child_hwnd, int dx, int dy, bool repaint)
{
	CRect child_rect;
	::GetWindowRect(child_hwnd, &child_rect);
	child_rect = screen_to_client(child_rect, parent_hwnd);		

	child_rect.left += dx;
	child_rect.right += dx;

	child_rect.top += dy;
	child_rect.bottom += dy;

	::MoveWindow(child_hwnd, child_rect.left, child_rect.top, 
			child_rect.Width(), child_rect.Height(), repaint);
}


void resize_wnd(HWND hwnd, HWND parent_hwnd, int left, int top, int right, int bottom, bool repaint)
{
	CRect wnd_rect;
	::GetWindowRect(hwnd, &wnd_rect);
	if (parent_hwnd) {
		wnd_rect = screen_to_client(wnd_rect, parent_hwnd);		
	}

	wnd_rect.left += left;
	wnd_rect.right += right;

	wnd_rect.top += top;
	wnd_rect.bottom += bottom;

	::MoveWindow(hwnd, wnd_rect.left, wnd_rect.top, wnd_rect.Width(), wnd_rect.Height(), repaint);
}


void set_redraw(CWnd& wnd, bool redraw)
{
	wnd.SetRedraw(redraw ? TRUE : FALSE);
}


CPoint get_last_message_mouse_pos()
{
	const DWORD mouse_pos = GetMessagePos(); 
    return CPoint(GET_X_LPARAM(mouse_pos), GET_Y_LPARAM(mouse_pos));
} 


CPoint get_last_message_mouse_pos(const CWnd& wnd)
{
    CPoint mouse_point = get_last_message_mouse_pos();
	wnd.ScreenToClient(&mouse_point);
	return mouse_point;
} 


void pump_window_messages(HWND hwnd)
{
	assert(::IsWindow(hwnd));
	if (::IsWindow(hwnd)) {

		MSG msg;
		while (::PeekMessage(&msg, hwnd,  0, 0, PM_REMOVE)) { 
			::TranslateMessage(&msg); 
			::DispatchMessage(&msg); 
		}
	}
}


CRect get_window_rect(const CWnd& wnd)
{
    CRect rect;
    GetWindowRect(wnd, rect);
    return rect;
}


CWnd& get_desktop_window()
{
    CWnd* dtop_wnd = CWnd::GetDesktopWindow();
    assert(dtop_wnd != nullptr);
    return *dtop_wnd;
}

auto has_group_window_style(HWND hwnd) -> bool
{
	auto wnd_style = static_cast<DWORD>(::GetWindowLong(hwnd, GWL_STYLE));
	return (wnd_style & WS_GROUP) != 0;
}

}

///  @file  CtrlLayoutMgr.cpp
///  Implementation file for "CtrlLayoutMgr.hpp"

//   Copyright (c) 2007-2019 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist_mfc_maker.hpp"

#include "CtrlLayoutMgr.hpp"

#include "wnd_utils.hpp"

namespace twist::mfc {

CtrlLayoutMgr::CtrlLayoutMgr(HWND parent_hwnd) 
	: parent_hwnd_(parent_hwnd)
	, min_width_(0)
	, min_height_(0)
	, max_width_(0) 
	, max_height_(0)
	, ctrl_anchors_()
	, orig_parent_size_()
	, orig_ctrl_rects_()
	, erase_parent_(false)
{
	// Sanity check.
	if (::IsWindow(parent_hwnd_) == FALSE) {
		TWIST_THROW(L"Invalid parent window handle.");
	}
	// Get the parent window size.
	orig_parent_size_ = get_wnd_size(parent_hwnd_);
}


CtrlLayoutMgr::~CtrlLayoutMgr()
{
	TWIST_CHECK_INVARIANT
}


void CtrlLayoutMgr::set_min_size(int min_width, int min_height)
{
	TWIST_CHECK_INVARIANT
	assert(min_width > 0);
	if (min_width > 0) {
		min_width_ = min_width;
	}
	assert(min_height > 0);
	if (min_height > 0) {
		min_height_ = min_height;
	}
	TWIST_CHECK_INVARIANT
}


void CtrlLayoutMgr::set_max_size(int max_width, int max_height)
{
	TWIST_CHECK_INVARIANT
	assert(max_width >= min_width_);
	if (max_width >= min_width_) {
		max_width_ = max_width;
	}
	assert(max_height >= min_height_);
	if (max_height >= min_height_) {
		max_height_ = max_height;
	}
}


void CtrlLayoutMgr::on_parent_min_max(MINMAXINFO* min_max_info) const
{
	TWIST_CHECK_INVARIANT
	if (min_width_ > 0) {
		min_max_info->ptMinTrackSize.x = min_width_;
	}
	if (min_height_ > 0) {
		min_max_info->ptMinTrackSize.y = min_height_;
	}
	if (max_width_ > 0) {
		min_max_info->ptMaxSize.x = max_width_;
		min_max_info->ptMaxTrackSize.x = max_width_;
	}
	if (max_height_ > 0) {
		min_max_info->ptMaxSize.y = max_height_;
		min_max_info->ptMaxTrackSize.y = max_height_;
	}
}


void CtrlLayoutMgr::add_ctrl(int ctrl_id, const Anchors& anchors)
{
	TWIST_CHECK_INVARIANT
	// Check that the control resource ID is valid.
	const HWND ctrlHwnd = ::GetDlgItem(parent_hwnd_, ctrl_id);
	assert(ctrlHwnd != nullptr);
	if (ctrlHwnd != nullptr) {
		// Store the control's initial rectangle in the map, keyed on the control's resource ID.
		CRect ctrlRect;
		::GetWindowRect(ctrlHwnd, &ctrlRect);
		// Will store the rectangle in coordinates relative to the parent window.
		ctrlRect = screen_to_client(ctrlRect, parent_hwnd_);
		orig_ctrl_rects_.insert(std::make_pair(ctrl_id, ctrlRect));
		// Store the control anchors
		ctrl_anchors_.insert(std::make_pair(ctrl_id, anchors));
	}
	TWIST_CHECK_INVARIANT
}


void CtrlLayoutMgr::arrange_ctrls()
{
	TWIST_CHECK_INVARIANT
	// Get the current parent size
	const CSize parentSize(get_wnd_size(parent_hwnd_));	
		
	const double horzRatio = static_cast<double>(parentSize.cx) / orig_parent_size_.cx;
	const double vertRatio = static_cast<double>(parentSize.cy) / orig_parent_size_.cy;

	for (const auto& el : ctrl_anchors_) {

		const int ctrl_id = el.first;
		const Anchors& anchors = el.second;
		const CRect& origCtrlRect = orig_ctrl_rects_.at(ctrl_id);
		
		// Get the current control rectangle
		const HWND ctrlHwnd = ::GetDlgItem(parent_hwnd_, ctrl_id);
		assert(::IsWindow(ctrlHwnd) != FALSE);
		CRect ctrlRect;
		::GetWindowRect(ctrlHwnd, &ctrlRect);
		ctrlRect = screen_to_client(ctrlRect, parent_hwnd_);		

		if (anchors.has(Anchor::k_left) && anchors.has(Anchor::k_right)) {
			ctrlRect.left = origCtrlRect.left;
			ctrlRect.right = parentSize.cx - orig_parent_size_.cx + origCtrlRect.right;
		}
		else if (anchors.has(Anchor::k_left)) {
			ctrlRect.left = origCtrlRect.left;
			ctrlRect.right = ctrlRect.left + origCtrlRect.Width();
		}
		else if (anchors.has(Anchor::k_right)) {
			ctrlRect.right = parentSize.cx - orig_parent_size_.cx + origCtrlRect.right;
			ctrlRect.left = ctrlRect.right - origCtrlRect.Width();
		}
		else {
			ctrlRect.left = static_cast<int>(origCtrlRect.left * horzRatio);
			ctrlRect.right = ctrlRect.left + origCtrlRect.Width();
		}
		
		if (anchors.has(Anchor::k_top) && anchors.has(Anchor::k_bottom)) {
			ctrlRect.top = origCtrlRect.top;
			ctrlRect.bottom = parentSize.cy - orig_parent_size_.cy + origCtrlRect.bottom;
		}
		else if (anchors.has(Anchor::k_top)) {
			ctrlRect.top = origCtrlRect.top;
			ctrlRect.bottom = ctrlRect.top + origCtrlRect.Height();
		}
		else if (anchors.has(Anchor::k_bottom)) {
			ctrlRect.bottom = parentSize.cy - orig_parent_size_.cy + origCtrlRect.bottom;
			ctrlRect.top = ctrlRect.bottom - origCtrlRect.Height();
		}
		else {
			ctrlRect.top = static_cast<int>(origCtrlRect.top * vertRatio);
			ctrlRect.bottom = ctrlRect.top + origCtrlRect.Height();
		}

		// Move control window
		::MoveWindow(ctrlHwnd, ctrlRect.left, ctrlRect.top, ctrlRect.Width(), ctrlRect.Height(), TRUE/*repaint*/);
		::InvalidateRect(ctrlHwnd, nullptr/*rect*/, FALSE/*erase*/);
	}

	if (erase_parent_) {
		::InvalidateRect(parent_hwnd_, nullptr/*rect*/, TRUE/*erase*/);
	}
	TWIST_CHECK_INVARIANT
}


void CtrlLayoutMgr::set_erase_parent(bool erase)
{
	TWIST_CHECK_INVARIANT
	erase_parent_ = erase;
	TWIST_CHECK_INVARIANT
}


#ifdef _DEBUG
void CtrlLayoutMgr::check_invariant() const noexcept
{
	assert(orig_parent_size_.cx > 0);
	assert(orig_parent_size_.cy > 0);
}
#endif // _DEBUG


//
//  Free functions
//

void add_ctrl_tl(CtrlLayoutMgr& layout_mgr, int ctrl_id)
{
	layout_mgr.add_ctrl(ctrl_id, { CtrlLayoutMgr::Anchor::k_top, CtrlLayoutMgr::Anchor::k_left });
}


void add_ctrls_tl(CtrlLayoutMgr& layout_mgr, int ctrl1_id, int ctrl2_id)
{
	add_ctrl_tl(layout_mgr, ctrl1_id);
	add_ctrl_tl(layout_mgr, ctrl2_id);
}


void add_ctrls_tl(CtrlLayoutMgr& layout_mgr, int ctrl1_id, int ctrl2_id, int ctrl3_id)
{
	add_ctrls_tl(layout_mgr, ctrl1_id, ctrl2_id);
	add_ctrl_tl(layout_mgr, ctrl3_id);
}


void add_ctrl_tr(CtrlLayoutMgr& layout_mgr, int ctrl_id)
{
	layout_mgr.add_ctrl(ctrl_id, { CtrlLayoutMgr::Anchor::k_top, CtrlLayoutMgr::Anchor::k_right });
}


void add_ctrls_tr(CtrlLayoutMgr& layout_mgr, int ctrl1_id, int ctrl2_id)
{
	add_ctrl_tr(layout_mgr, ctrl1_id);
	add_ctrl_tr(layout_mgr, ctrl2_id);
}


void add_ctrls_tr(CtrlLayoutMgr& layout_mgr, int ctrl1_id, int ctrl2_id, int ctrl3_id)
{
	add_ctrls_tr(layout_mgr, ctrl1_id, ctrl2_id);
	add_ctrl_tr(layout_mgr, ctrl3_id);
}


void add_ctrls_tr(CtrlLayoutMgr& layout_mgr, int ctrl1_id, int ctrl2_id, int ctrl3_id, int ctrl4_id)
{
	add_ctrls_tr(layout_mgr, ctrl1_id, ctrl2_id, ctrl3_id);
	add_ctrl_tr(layout_mgr, ctrl4_id);
}


void add_ctrls_tr(CtrlLayoutMgr& layout_mgr, int ctrl1_id, int ctrl2_id, int ctrl3_id, int ctrl4_id, int ctrl5_id)
{
	add_ctrls_tr(layout_mgr, ctrl1_id, ctrl2_id, ctrl3_id, ctrl4_id);
	add_ctrl_tr(layout_mgr, ctrl5_id);
}


void add_ctrls_tr(CtrlLayoutMgr& layout_mgr, int ctrl1_id, int ctrl2_id, int ctrl3_id, int ctrl4_id, int ctrl5_id, int ctrl6_id)
{
	add_ctrls_tr(layout_mgr, ctrl1_id, ctrl2_id, ctrl3_id, ctrl4_id, ctrl5_id);
	add_ctrl_tr(layout_mgr, ctrl6_id);
}


void add_ctrls_tr(CtrlLayoutMgr& layout_mgr, int ctrl1_id, int ctrl2_id, int ctrl3_id, int ctrl4_id, int ctrl5_id, int ctrl6_id, int ctrl7_id)
{
	add_ctrls_tr(layout_mgr, ctrl1_id, ctrl2_id, ctrl3_id, ctrl4_id, ctrl5_id, ctrl6_id);
	add_ctrl_tr(layout_mgr, ctrl7_id);
}


void add_ctrl_tlr(CtrlLayoutMgr& layout_mgr, int ctrl_id)
{
	layout_mgr.add_ctrl(
			ctrl_id, 
			{ CtrlLayoutMgr::Anchor::k_top, CtrlLayoutMgr::Anchor::k_left, CtrlLayoutMgr::Anchor::k_right });
}


void add_ctrls_tlr(CtrlLayoutMgr& layout_mgr, int ctrl1_id, int ctrl2_id)
{
	add_ctrl_tlr(layout_mgr, ctrl1_id);
	add_ctrl_tlr(layout_mgr, ctrl2_id);
}


void add_ctrls_tlr(CtrlLayoutMgr& layout_mgr, int ctrl1_id, int ctrl2_id, int ctrl3_id)
{
	add_ctrls_tlr(layout_mgr, ctrl1_id, ctrl2_id);
	add_ctrl_tlr(layout_mgr, ctrl3_id);
}


void add_ctrl_lbr(CtrlLayoutMgr& layout_mgr, int ctrl_id)
{
	layout_mgr.add_ctrl(
			ctrl_id, 
			{ CtrlLayoutMgr::Anchor::k_bottom, CtrlLayoutMgr::Anchor::k_left, 
			  CtrlLayoutMgr::Anchor::k_right });
}


void add_ctrls_lbr(CtrlLayoutMgr& layout_mgr, int ctrl1_id, int ctrl2_id)
{
	add_ctrl_lbr(layout_mgr, ctrl1_id);
	add_ctrl_lbr(layout_mgr, ctrl2_id);
}


void add_ctrls_lbr(CtrlLayoutMgr& layout_mgr, int ctrl1_id, int ctrl2_id, int ctrl3_id)
{
	add_ctrls_lbr(layout_mgr, ctrl1_id, ctrl2_id);
	add_ctrl_lbr(layout_mgr, ctrl3_id);
}


void add_ctrl_bl(CtrlLayoutMgr& layout_mgr, int ctrl_id)
{
	layout_mgr.add_ctrl(
			ctrl_id, 
			{ CtrlLayoutMgr::Anchor::k_bottom, CtrlLayoutMgr::Anchor::k_left });
}


void add_ctrls_bl(CtrlLayoutMgr& layout_mgr, int ctrl1_id, int ctrl2_id)
{
	add_ctrl_bl(layout_mgr, ctrl1_id);
	add_ctrl_bl(layout_mgr, ctrl2_id);
}


void add_ctrls_bl(CtrlLayoutMgr& layout_mgr, int ctrl1_id, int ctrl2_id, int ctrl3_id)
{
	add_ctrls_bl(layout_mgr, ctrl1_id, ctrl2_id);
	add_ctrl_bl(layout_mgr, ctrl3_id);
}


void add_ctrls_bl(CtrlLayoutMgr& layout_mgr, int ctrl1_id, int ctrl2_id, int ctrl3_id, int ctrl4_id)
{
	add_ctrls_bl(layout_mgr, ctrl1_id, ctrl2_id, ctrl3_id);
	add_ctrl_bl(layout_mgr, ctrl4_id);
}


void add_ctrls_bl(CtrlLayoutMgr& layout_mgr, int ctrl1_id, int ctrl2_id, int ctrl3_id, int ctrl4_id, int ctrl5_id)
{
	add_ctrls_bl(layout_mgr, ctrl1_id, ctrl2_id, ctrl3_id, ctrl4_id);
	add_ctrl_bl(layout_mgr, ctrl5_id);
}

void add_ctrls_bl(CtrlLayoutMgr& layout_mgr, int ctrl1_id, int ctrl2_id, int ctrl3_id, int ctrl4_id, int ctrl5_id, int ctrl6_id)
{
	add_ctrls_bl(layout_mgr, ctrl1_id, ctrl2_id, ctrl3_id, ctrl4_id, ctrl5_id);
	add_ctrl_bl(layout_mgr, ctrl6_id);
}


void add_ctrls_bl(CtrlLayoutMgr& layout_mgr, int ctrl1_id, int ctrl2_id, int ctrl3_id, int ctrl4_id, int ctrl5_id, int ctrl6_id, int ctrl7_id)
{
	add_ctrls_bl(layout_mgr, ctrl1_id, ctrl2_id, ctrl3_id, ctrl4_id, ctrl5_id, ctrl6_id);
	add_ctrl_bl(layout_mgr, ctrl7_id);
}


void add_ctrl_br(CtrlLayoutMgr& layout_mgr, int ctrl_id)
{
	layout_mgr.add_ctrl(
			ctrl_id, 
			{ CtrlLayoutMgr::Anchor::k_bottom, CtrlLayoutMgr::Anchor::k_right });
}


void add_ctrls_br(CtrlLayoutMgr& layout_mgr, int ctrl1_id, int ctrl2_id)
{
	add_ctrl_br(layout_mgr, ctrl1_id);
	add_ctrl_br(layout_mgr, ctrl2_id);
}


void add_ctrls_br(CtrlLayoutMgr& layout_mgr, int ctrl1_id, int ctrl2_id, int ctrl3_id)
{
	add_ctrls_br(layout_mgr, ctrl1_id, ctrl2_id);
	add_ctrl_br(layout_mgr, ctrl3_id);
}


void add_ctrls_br(CtrlLayoutMgr& layout_mgr, int ctrl1_id, int ctrl2_id, int ctrl3_id, int ctrl4_id)
{
	add_ctrls_br(layout_mgr, ctrl1_id, ctrl2_id, ctrl3_id);
	add_ctrl_br(layout_mgr, ctrl4_id);
}


void add_ctrls_br(CtrlLayoutMgr& layout_mgr, int ctrl1_id, int ctrl2_id, int ctrl3_id, int ctrl4_id, int ctrl5_id)
{
	add_ctrls_br(layout_mgr, ctrl1_id, ctrl2_id, ctrl3_id, ctrl4_id);
	add_ctrl_br(layout_mgr, ctrl5_id);
}


void add_ctrls_br(CtrlLayoutMgr& layout_mgr, int ctrl1_id, int ctrl2_id, int ctrl3_id, int ctrl4_id, int ctrl5_id, int ctrl6_id)
{
	add_ctrls_br(layout_mgr, ctrl1_id, ctrl2_id, ctrl3_id, ctrl4_id, ctrl5_id);
	add_ctrl_br(layout_mgr, ctrl6_id);
}


void add_ctrl_tlb(CtrlLayoutMgr& layout_mgr, int ctrl_id)
{
	layout_mgr.add_ctrl(
			ctrl_id, 
			{ CtrlLayoutMgr::Anchor::k_top, CtrlLayoutMgr::Anchor::k_left, CtrlLayoutMgr::Anchor::k_bottom });
}


void add_ctrls_tlb(CtrlLayoutMgr& layout_mgr, int ctrl1_id, int ctrl2_id)
{
	add_ctrl_tlb(layout_mgr, ctrl1_id);
	add_ctrl_tlb(layout_mgr, ctrl2_id);
}


void add_ctrl_tbr(CtrlLayoutMgr& layout_mgr, int ctrl_id)
{
	layout_mgr.add_ctrl(
			ctrl_id, 
			{ CtrlLayoutMgr::Anchor::k_top, CtrlLayoutMgr::Anchor::k_bottom, 
			  CtrlLayoutMgr::Anchor::k_right });
}


void add_ctrls_tbr(CtrlLayoutMgr& layout_mgr, int ctrl1_id, int ctrl2_id)
{
	add_ctrl_tbr(layout_mgr, ctrl1_id);
	add_ctrl_tbr(layout_mgr, ctrl2_id);
}


void add_ctrl_tlbr(CtrlLayoutMgr& layout_mgr, int ctrl_id)
{
	layout_mgr.add_ctrl(
			ctrl_id, 
			{ CtrlLayoutMgr::Anchor::k_top, CtrlLayoutMgr::Anchor::k_left, CtrlLayoutMgr::Anchor::k_bottom, 
			  CtrlLayoutMgr::Anchor::k_right });
}
	

void add_ctrls_tlbr(CtrlLayoutMgr& layout_mgr, int ctrl1_id, int ctrl2_id)
{
	add_ctrl_tlbr(layout_mgr, ctrl1_id);
	add_ctrl_tlbr(layout_mgr, ctrl2_id);
}


void add_ctrls_tlbr(CtrlLayoutMgr& layout_mgr, int ctrl1_id, int ctrl2_id, int ctrl3_id)
{
	add_ctrl_tlbr(layout_mgr, ctrl1_id);
	add_ctrl_tlbr(layout_mgr, ctrl2_id);
	add_ctrl_tlbr(layout_mgr, ctrl3_id);
}

}

///  @file  ProgProvMsgWnd.cpp
///  Implementation file for "ProgProvMsgWnd.hpp"

//   Copyright (c) 2010-2019 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist_mfc_maker.hpp"

#include "ProgProvMsgWnd.hpp"

#include "wnd_utils.hpp"

namespace twist::mfc {

ProgProvMsgWnd::ProgProvMsgWnd(CProgressCtrl& prog_wnd, CWnd& msg_wnd, CWnd* parent_wnd)
	: ProgressProv()
	, prog_wnd_(prog_wnd)
	, msg_wnd_(msg_wnd)
	, parent_wnd_(parent_wnd)
{
	TWIST_CHECK_INVARIANT
}


ProgProvMsgWnd::~ProgProvMsgWnd()
{
	TWIST_CHECK_INVARIANT
}


void ProgProvMsgWnd::init_message_mode()
{
	TWIST_CHECK_INVARIANT
}

	
void ProgProvMsgWnd::init_hist_mode(long /*expected_num_lines*/)
{
	TWIST_CHECK_INVARIANT
	TWIST_THROW(L"This progress provider only supports the \"message\" mode.");
}


void ProgProvMsgWnd::get_prog_range(twist::Ssize& min_pos, twist::Ssize& max_pos) const
{
	TWIST_CHECK_INVARIANT
	int min = 0;
	int max = 0;
	prog_wnd_.GetRange(min, max);
	min_pos = static_cast<twist::Ssize>(min);
	max_pos = static_cast<twist::Ssize>(max);
	TWIST_CHECK_INVARIANT
}


void ProgProvMsgWnd::set_prog_range(twist::Ssize min_pos, twist::Ssize max_pos)
{
	TWIST_CHECK_INVARIANT
	prog_wnd_.SetRange(static_cast<short>(min_pos), static_cast<short>(max_pos)); //+ check conv to short
	TWIST_CHECK_INVARIANT
}


twist::Ssize ProgProvMsgWnd::get_prog_pos() const
{
	TWIST_CHECK_INVARIANT
	return prog_wnd_.GetPos();
}


void ProgProvMsgWnd::set_prog_pos(twist::Ssize pos, bool force_update)
{
	TWIST_CHECK_INVARIANT
	prog_wnd_.SetPos(static_cast<int>(pos));
	if (force_update && parent_wnd_ != nullptr) {
		pump_window_messages(parent_wnd_->GetSafeHwnd());
	}
	TWIST_CHECK_INVARIANT
}


void ProgProvMsgWnd::inc_prog_pos(bool force_update)
{
	TWIST_CHECK_INVARIANT
	prog_wnd_.SetPos(prog_wnd_.GetPos() + 1);
	if (force_update && parent_wnd_ != nullptr) {
		pump_window_messages(parent_wnd_->GetSafeHwnd());
	}
	TWIST_CHECK_INVARIANT
}


void ProgProvMsgWnd::set_prog_msg(std::wstring_view msg, bool force_update)
{
	TWIST_CHECK_INVARIANT
	msg_wnd_.SetWindowText(std::wstring{msg}.c_str());
	if (force_update && parent_wnd_ != nullptr) {
		pump_window_messages(parent_wnd_->GetSafeHwnd());
	}
	TWIST_CHECK_INVARIANT
}


void ProgProvMsgWnd::add_new_hist_line(const std::wstring& hist_line)
{
	TWIST_CHECK_INVARIANT
	set_prog_msg(hist_line, true/*force_update*/);
	TWIST_CHECK_INVARIANT
}


void ProgProvMsgWnd::set_present_hist_line(const std::wstring& hist_line)
{
	TWIST_CHECK_INVARIANT
	set_prog_msg(hist_line, true/*force_update*/);
	TWIST_CHECK_INVARIANT
}


void ProgProvMsgWnd::hide()
{
	TWIST_CHECK_INVARIANT
	assert(false);
}


#ifdef _DEBUG
void ProgProvMsgWnd::check_invariant() const noexcept
{
}
#endif // _DEBUG

}



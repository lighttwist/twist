///  @file  ListCtrlInPlaceEditHelper.hpp
///  ListCtrlInPlaceEditHelper class definition 

//   Copyright (c) 2007-2019 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_MFC_LIST_CTRL_IN_PLACE_EDIT_HELPER_HPP
#define TWIST_MFC_LIST_CTRL_IN_PLACE_EDIT_HELPER_HPP

#include "CtrlInPlaceEdit.hpp" 

namespace twist::mfc {

///  Helper class for displaying an "in-place" edit control over a list control (sub)item, and the related 
///    functionality of editing any subitem in the list control by double-clicking on it, as well as creating 
///    new items by double-clicking on the blank item immediately below the last item in the list (referred to 
///    as the "placeholder" item) in a particular column, referred to as the "item creation" column.
///  When an item is edited, blank string is not allowed if the column being edited is the "item creation" 
///    column, but it is allowed in other columns.
///  Items can be deleted from the list by pressing the "Delete" key while the item is selected.
class ListCtrlInPlaceEditHelper {
public:
	/// Event handler type for the "on adding item" event. The event is emitted when the user has finished 
	/// entering text in the "item creation" subitem of the "placeholder" item.
	///
	/// @param[in]  text  The text just entered in the "item creation" column
	/// @param[out]  data  Data value that should be associated with the new item  
	/// @return  true to accept the new item (it will be added to the list), false to reject it (it will not 
	///					be added) 
	///
	typedef std::function<bool(const std::wstring& text, DWORD_PTR& data)>  OnAddingItem;

	/// Event handler type for the "on item added" event. The event is emitted after a new item has just been 
	/// added to the list by entering text in the "item creation" subitem of the "placeholder" item.
	///
	/// @param[in]  text  The text just entered in the "item creation" column
	///
	typedef std::function<void(const std::wstring& text)>  OnItemAdded;

	/// Event handler type for the "on editing item" event. The event is emitted when the user has finished 
	/// editing the text in a subitem.
	///
	/// @param[in]  item  The item index
	/// @param[in]  col  The column index
	/// @param[in]  text  The text just entered in the "item creation" column
	/// @return  true to accept the new text (the subitem text will be modified in the list), false to reject 
	///					it (it will not be modified) 
	///
	typedef std::function<bool(int item, int col, const std::wstring& text)>  OnEditingItem;

	/// Event handler type for the "on item edited" event. The event is emitted after a subitem's text has 
	/// been modified in the list by the user.
	///
	/// @param[in]  item  The item index
	/// @param[in]  col  The column index
	/// @param[in]  text  The text just entered in the subitem
	///
	typedef std::function<void(int item, int col, const std::wstring& text)>  OnItemEdited;

	/// Event handler type for the "on deleting item" event. The event is emitted when the user has pressed 
	/// the "Delete" key while the item was selected in the list.
	///
	/// @param[in]  item  The item index
	/// @return  true to accept the deletion (the item will be deleted from the list), false to reject it (it 
	///					will not be deleted) 
	///	
	typedef std::function<bool(int item)>  OnDeletingItem;

	/// Constructor.
	///
	/// @param[in]  lst_ctrl  The list control
	/// @param[in]  item_creation_col  The "item creation" column index
	///
	ListCtrlInPlaceEditHelper(CListCtrl& lst_ctrl, int item_creation_col = 0);

	/// Register a handler of the the "on adding item" event with this object.
	///
	/// @param[in]  on_adding_item  The handler
	///
	void set_on_adding_item(OnAddingItem on_adding_item);

	/// Register a handler of the the "on item added" event with this object.
	///
	/// @param[in]  on_item_added  The handler
	///
	void set_on_item_added(OnItemAdded on_item_added);

	/// Register a handler of the the "on editing item" event with this object.
	///
	/// @param[in]  on_editing_item  The handler
	///
	void set_on_editing_item(OnEditingItem on_editing_item);

	/// Register a handler of the the "on item edited" event with this object.
	///
	/// @param[in]  on_item_edited  The handler
	///
	void set_on_item_edited(OnItemEdited on_item_edited);

	/// Register a handler of the the "on deleting item" event with this object.
	///
	/// @param[in]  on_deleting_item  The handler
	///
	void set_on_deleting_item(OnDeletingItem on_deleting_item);

	/// Call this method when the HDN_BEGINTRACK is received by the list control, with the standard arguments 
	/// of the handler for that event.
	/// 
	void on_lst_ctrl_hdn_begintrack(NMHDR* pNMHDR, LRESULT* pResult);

	/// Call this method when the NM_DBLCLK is received by the list control, with the standard arguments of 
	/// the handler for that event.
	/// 
	void on_lst_ctrl_dblclk(NMHDR* pNMHDR, LRESULT* pResult); 

	/// Call this method when the LVN_KEYDOWN is received by the list control, with the standard arguments of 
	/// the handler for that event.
	/// 
	void on_lst_ctrl_keydown(NMHDR* pNMHDR, LRESULT* pResult);

private:
	void on_edt_ctrl_exit(const std::wstring& text);

	CListCtrl&  lst_ctrl_;
	CtrlInPlaceEdit<CEdit>  edt_ctrl_;
	int  item_creation_col_;

	OnAddingItem  on_adding_item_;
	OnItemAdded  on_item_added_;
	OnEditingItem  on_editing_item_;
	OnItemEdited  on_item_edited_;
	OnDeletingItem  on_deleting_item_;

	TWIST_CHECK_INVARIANT_DECL
};

}

#endif 

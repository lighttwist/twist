///  @file  ctrl_utils.hpp
///  Miscellaneous utilities for working with Windows controls.

//   Copyright (c) 2007-2019 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_MFC_CTRL__UTILS_HPP
#define TWIST_MFC_CTRL__UTILS_HPP

namespace twist::mfc {

/// Select all the text currently contained in an edit box.
///
/// @param[in]  edit_box  The edit box
/// @param[in]  focus  Whether the box should also receive the focus
///
void select_all(CEdit& edit_box, bool focus);

namespace detail { 

/// Convert a data value of a type supplied by the user to the internal data type of some MFC controls (eg the 
/// list and combo control), when the user data type is not a pointer type. 
///
/// @tparam  Data  The type of the data value; must be convertible to a numeric type 	
/// @param[in]  data  The data value
/// @param[in]  is_ptr  Whether the user data type is a pointer type; MPL parameter for overloading resolution
/// @return  The converted data value
///
template<class Data> 
DWORD_PTR user_to_internal_data(Data data, std::false_type is_ptr);

/// Overload for user data types which are pointer types.
template<class Data> 
DWORD_PTR user_to_internal_data(Data data, std::true_type is_ptr);

/// Convert a data value of the internal data type of some MFC controls (eg the list and combo control) to a 
/// type supplied by the user, when the user data type is not a pointer type. 
///
/// @tparam  Data  The type of the data value; must be convertible to a numeric type 	
/// @param[in]  data  The data value
/// @param[in]  is_ptr  Whether the user data type is a pointer type; MPL parameter for overloading resolution
/// @return  The converted data value
///
template<class Data> 
Data internal_to_user_data(DWORD_PTR data, std::false_type is_ptr);

/// Overload for user data types which are pointer types.
template<class Data> 
Data internal_to_user_data(DWORD_PTR data, std::true_type is_ptr);

} 
}

#include "ctrl_utils.ipp"

#endif 

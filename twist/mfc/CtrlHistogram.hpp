///  @file  CtrlHistogram.hpp
///  CtrlHistogram  class, inherits CtrlGraph

//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_MFC_CTRL_HISTOGRAM_HPP
#define TWIST_MFC_CTRL_HISTOGRAM_HPP

//+  Work in progress  Dan A.  18DEC2013

#include "../math/sample_utils.hpp"
#include "../math/UnivarDistrSample.hpp"

#include "CtrlStaticGraphic.hpp"

namespace twist::mfc {

///
///  An graphical static control base class specialised for drawing histograms.
///
class CtrlHistogram : public CtrlStaticGraphic {
public:
	typedef twist::math::HistogramData  HistData;

	enum class HistType {
		k_pdf  = 1,
		k_cdf  = 2
	};

	CtrlHistogram(const HistData& histData, HistType histType = HistType::k_pdf);

	virtual ~CtrlHistogram();

	void SetHistData(const HistData& histData, bool invalidate = false);

	void SetHistType(HistType histType, bool invalidate = false);

protected:
	DECLARE_MESSAGE_MAP()

private:
	virtual void DrawGraphic(CDC& dc, const CRect& rect) override;  //  CtrlStaticGraphic  override

	void DrawHistogram(CDC& dc, const CRect& rect, double binWidth, double vertRatio);

	void DrawCoords(CDC& dc, const CRect& rect);

	HistData  m_histData;
	HistType  m_histType;

	TWIST_CHECK_INVARIANT_DECL
};

}

#endif 

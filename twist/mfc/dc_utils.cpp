///  @file  dc_utils.cpp
///  Implementation file for "dc_utils.hpp"

//   Copyright (c) 2007-2019 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist_mfc_maker.hpp"

#include "dc_utils.hpp"

namespace twist::mfc {

void draw_text(const std::wstring& text, const CRect& rect, HDC dc, UINT format)
{
	RECT rct = {rect.left, rect.top, rect.right, rect.bottom};
	::DrawTextEx(dc, const_cast<wchar_t*>(text.c_str()), -1, &rct, format, nullptr);
}


int get_text_width(const std::wstring& text, HDC hdc)
{
	CSize size;
	if (::GetTextExtentPoint32(hdc, text.c_str(), static_cast<int>(text.size()), &size) != FALSE) {
		return size.cx;
	}
	return -1;
}


COLORREF lighten_colour(COLORREF colour, double factor)
{
	if (factor > 0 && factor <= 1) {

		BYTE red   = GetRValue(colour);
		BYTE green = GetGValue(colour);
		BYTE blue  = GetBValue(colour);
		red   = static_cast<BYTE>((factor * (255 - red )) + red);
		green = static_cast<BYTE>((factor * (255 - green)) + green);
		blue  = static_cast<BYTE>((factor * (255 - blue)) + blue);
		colour = RGB(red, green, blue);
	}

	return colour;
}

}



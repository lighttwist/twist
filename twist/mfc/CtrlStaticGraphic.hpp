///  @file  CtrlStaticGraphic.hpp
///  CtrlStaticGraphic  class, inherits CStatic

//   Copyright (c) 2007-2019 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_MFC_CTRL_STATIC_GRAPH_HPP
#define TWIST_MFC_CTRL_STATIC_GRAPH_HPP

//+  Work in progress  Dan A.  18DEC2013

namespace twist::mfc {

///
///  An abstract static control base class specialised for drawing graphics.
///
class CtrlStaticGraphic : public CStatic {
public:
	CtrlStaticGraphic(COLORREF clrText = CLR_DEFAULT, COLORREF clrBorder = CLR_DEFAULT);

	virtual ~CtrlStaticGraphic();

	// static void Register(void);    Does not seem necessary

	// static void Unregister(void);  Does not seem necessary

	COLORREF GetTextColor() const;

	void SetTextColor(COLORREF clrText);

	COLORREF GetBorderColor() const;

	void SetBorderColor(COLORREF clrBorder);

	virtual void SetWindowText(const std::wstring& text);

	void SafeInvalidate(bool erase = true);

protected:
	// static ATOM s_atom;  Does not seem necessary

	virtual BOOL PreCreateWindow(CREATESTRUCT& cs) override;

	virtual void PreSubclassWindow() override;
	
	virtual void DrawItem(PDRAWITEMSTRUCT pdis) override;
	
	afx_msg BOOL OnEraseBkgnd(CDC *pDC);
	
	afx_msg void OnSysColorChange();

	DECLARE_DYNAMIC(CtrlStaticGraphic)

	DECLARE_MESSAGE_MAP()

private:
	virtual void DrawGraphic(CDC& dc, const CRect& rect) = 0;

	COLORREF  m_clrText;
	COLORREF  m_clrBorder;

	bool  m_bSysText;
	bool  m_bSysBorder;

	TWIST_CHECK_INVARIANT_DECL
};

}

#endif 

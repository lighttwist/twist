///  @file  CtrlListOwnerDrawBase.cpp
///  Implementation file for "CtrlListOwnerDrawBase.hpp"

//   Copyright (c) 2010-2019 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist_mfc_maker.hpp"

#include "CtrlListOwnerDrawBase.hpp"

#include "dc_utils.hpp"

namespace twist::mfc {

IMPLEMENT_DYNAMIC(CtrlListOwnerDrawBase, CListCtrl)

BEGIN_MESSAGE_MAP(CtrlListOwnerDrawBase, CListCtrl)
	ON_WM_MEASUREITEM_REFLECT()
END_MESSAGE_MAP()


CtrlListOwnerDrawBase::CtrlListOwnerDrawBase() 
	: CListCtrl()
	, show_sel_(false)
	, item_height_(0)
	, max_col_content_widths_()
	, white_hbrush_(0)
	, selected_brush_()
	, focused_brush_()
{
	white_hbrush_ = static_cast<HBRUSH>(::GetStockObject(WHITE_BRUSH));
	selected_brush_.CreateSolidBrush(lighten_colour(::GetSysColor(COLOR_HIGHLIGHT), 0.75));
	focused_brush_ .CreateSolidBrush(lighten_colour(::GetSysColor(COLOR_HIGHLIGHT), 0.50));
	TWIST_CHECK_INVARIANT
}


CtrlListOwnerDrawBase::~CtrlListOwnerDrawBase()
{
	TWIST_CHECK_INVARIANT
}


void CtrlListOwnerDrawBase::Init(bool showSel, int itemHeight)
{
	TWIST_CHECK_INVARIANT
	show_sel_ = showSel;
	item_height_ = itemHeight;

	// Force the WM_MEASUREITEM message to be reflected to this control.	
	CRect lstRect;
	GetWindowRect(&lstRect);
	
	WINDOWPOS wpos;
	wpos.hwnd = GetSafeHwnd();
	wpos.hwndInsertAfter = nullptr;
	wpos.x = lstRect.left;
	wpos.y = lstRect.top;
	wpos.cx = lstRect.Width();
	wpos.x = lstRect.Height();
	wpos.flags = SWP_NOMOVE | SWP_NOOWNERZORDER;
	SendMessage(WM_WINDOWPOSCHANGED, 0, reinterpret_cast<LPARAM>(&wpos));

	const DWORD style = GetStyle();
	assert((style & LVS_OWNERDRAWFIXED) != 0);  // The list should have the LVS_OWNERDRAWFIXED style
	TWIST_CHECK_INVARIANT
}


int CtrlListOwnerDrawBase::CountColumns() const
{
	TWIST_CHECK_INVARIANT
	return const_cast<CtrlListOwnerDrawBase*>(this)->GetHeaderCtrl()->GetItemCount();
}


int CtrlListOwnerDrawBase::GetMaxColContentWidth(int colIdx) const
{
	TWIST_CHECK_INVARIANT
	const auto it = max_col_content_widths_.find(colIdx);
	if (it == end(max_col_content_widths_)) {
		return it->second;
	}
	return 0;
}


void CtrlListOwnerDrawBase::ZapMaxColsContentWidth()
{
	TWIST_CHECK_INVARIANT
	max_col_content_widths_.clear();
	TWIST_CHECK_INVARIANT
}


CRect CtrlListOwnerDrawBase::GetSubitemRect(const CRect& itemRect, int colIdx) const
{
	TWIST_CHECK_INVARIANT
	int left = itemRect.left;
	for (int i = 0; i < colIdx; ++i) {
		left += GetColumnWidth(i);
	}

	return CRect(left, itemRect.top, left + GetColumnWidth(colIdx), itemRect.bottom);
}


void CtrlListOwnerDrawBase::UpdateMaxColContentWidth(int colIdx, int width)
{
	TWIST_CHECK_INVARIANT
	if (width > 0) {
		const auto it = max_col_content_widths_.find(colIdx);
		if (it == end(max_col_content_widths_)) {
			max_col_content_widths_.insert(std::make_pair(colIdx, width));
			return;
		}
		if (width > it->second) {
			it->second = width;
		}
	}
	TWIST_CHECK_INVARIANT
}


int CtrlListOwnerDrawBase::GetSubitemContentWidth(int /*rowIdx*/, int /*colIdx*/, HDC /*dc*/) const
{
	TWIST_CHECK_INVARIANT
	return 0;  // Override in derived classes if you need  GetMaxColContentWidth()  to work.
}


void CtrlListOwnerDrawBase::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	TWIST_CHECK_INVARIANT
	const CRect itemRect(lpDrawItemStruct->rcItem);

	const bool itemSelected = (lpDrawItemStruct->itemState & ODS_SELECTED) != 0;
	const bool itemFocused  = (lpDrawItemStruct->itemState & ODS_FOCUS)    != 0;

	if (show_sel_ && itemSelected) {
		::FillRect(lpDrawItemStruct->hDC, &itemRect, selected_brush_);
	}
	else {
		::FillRect(lpDrawItemStruct->hDC, &itemRect, white_hbrush_);
	}
	if (show_sel_ && itemFocused) {
		::FrameRect(lpDrawItemStruct->hDC, &itemRect, focused_brush_);
	}

	DrawSubitems(lpDrawItemStruct->itemID, itemRect, lpDrawItemStruct->hDC);
	TWIST_CHECK_INVARIANT
}


void CtrlListOwnerDrawBase::MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{
	TWIST_CHECK_INVARIANT
	if (item_height_ > 0) {
		lpMeasureItemStruct->itemHeight = item_height_;
	}
	TWIST_CHECK_INVARIANT
}


#ifdef _DEBUG
void CtrlListOwnerDrawBase::check_invariant() const noexcept
{
}
#endif 

} 


///  @file  list_ctrl_utils.cpp
///  Implementation file for "list_ctrl_utils.hpp"

//   Copyright (c) 2007-2019 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist_mfc_maker.hpp"

#include "list_ctrl_utils.hpp"

using namespace std::placeholders;

namespace twist::mfc {

//
//  Local code
//

///
///  Contains information about a list control item.
///
struct ListCtrlItemInfo {
	ListCtrlItemInfo() 
		: item()
		, subitems()
		, itemData(0)
	{
		::ZeroMemory(&item, sizeof(LVITEM));
		item.mask = LVIF_IMAGE | LVIF_PARAM | LVIF_STATE | LVIF_TEXT;
		item.cchTextMax = 260;
		item.pszText = new wchar_t[260];
	}
		
	~ListCtrlItemInfo() 
	{
		TWIST_DELETE_ARRAY(item.pszText);
	}

	LVITEM  item;  // The list item structure
	std::vector<CString>  subitems;	 // Vector containing the text for each subitem
	DWORD_PTR  itemData;  // Item data
};


//
//  ListCtrlState::ItemState  class
//

ListCtrlState::ItemState::ItemState(const std::vector<std::wstring>& subitem_strings, DWORD_PTR item_data)
	: subitem_strings_(subitem_strings)
	, item_data_(item_data)
{
}


const std::vector<std::wstring>& ListCtrlState::ItemState::subitem_strings() const
{
	return subitem_strings_;
}		


DWORD_PTR ListCtrlState::ItemState::item_data() const
{
	return item_data_;
}


//
//  ListCtrlState  class
//

ListCtrlState::ListCtrlState(int num_cols)
	: num_cols_(num_cols)
{
	TWIST_CHECK_INVARIANT
}

std::unique_ptr<ListCtrlState> ListCtrlState::read_from_ctrl(const CListCtrl& ctrl)
{
	const int num_cols = count_columns(ctrl);
	if (num_cols == 0) {
		TWIST_THROW(L"The source list control must have at least one column.");
	}
	const int num_items = count_items(ctrl);

	std::unique_ptr<ListCtrlState> state(new ListCtrlState(num_cols));
	for (int i = 0; i < num_items; ++i) {
		std::vector<std::wstring> subitem_strings;
		for (int j = 0; j < state->num_cols_; ++j) {
			subitem_strings.push_back(get_item_text(ctrl, i, j));
		}
		state->item_states_.push_back(ItemState(subitem_strings, ctrl.GetItemData(i)));
	}
	return state;
}


void ListCtrlState::write_to_ctrl(CListCtrl& ctrl) const
{
	TWIST_CHECK_INVARIANT
	if (count_columns(ctrl) != num_cols_) {
		TWIST_THROW(L"The list control has the wrong number of columns.");
	}
	ctrl.DeleteAllItems();
	for (const auto& el : item_states_) {
		int item_idx = add_item(ctrl, el.subitem_strings().at(0), el.item_data());
		for (int j = 1; j < num_cols_; ++j) {
			set_item_text(ctrl, item_idx, j, el.subitem_strings().at(j));
		}
	}
}


std::vector<const ListCtrlState::ItemState*> ListCtrlState::get_item_states() const
{
	TWIST_CHECK_INVARIANT
	return twist::transform_to_vector(item_states_, [](const ListCtrlState::ItemState& el) {
		return &el;
	});
}


std::vector<std::wstring> ListCtrlState::get_column_strings(int col_idx) const
{
	TWIST_CHECK_INVARIANT
	if (col_idx >= num_cols_) {
		TWIST_THROW(L"Column index %d out of bounds.", col_idx);
	}
	return twist::transform_to_vector(item_states_, [=](const ListCtrlState::ItemState& el)->std::wstring {
		return el.subitem_strings()[col_idx];
	});
}


#ifdef _DEBUG
void ListCtrlState::check_invariant() const noexcept
{
	assert(num_cols_ > 0);
}
#endif // _DEBUG

//
//  Global functions
//

void init(CListCtrl& ctrl)
{
	const DWORD exStyle = ctrl.GetExtendedStyle() | LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_INFOTIP;
	ctrl.SetExtendedStyle(exStyle);
}


void init(CListCtrl& ctrl, DWORD styles)
{
	const DWORD exStyle = ctrl.GetExtendedStyle();
	ctrl.SetExtendedStyle(exStyle | styles);
}


void set_single_sel(CListCtrl& ctrl, bool on)
{
	if (on) {
		ctrl.ModifyStyle(0, LVS_SINGLESEL);
	}
	else {
		ctrl.ModifyStyle(LVS_SINGLESEL, 0);
	}
}


void set_grid_lines(CListCtrl& ctrl, bool on)
{
	const DWORD exStyle = ctrl.GetExtendedStyle() | (on ? LVS_EX_GRIDLINES : ~LVS_EX_GRIDLINES);
	ctrl.SetExtendedStyle(exStyle);
}

	
void set_full_row_select(CListCtrl& ctrl, bool on)
{
	const DWORD exStyle = ctrl.GetExtendedStyle() | (on ? LVS_EX_FULLROWSELECT : ~LVS_EX_FULLROWSELECT);
	ctrl.SetExtendedStyle(exStyle);
}

	
void set_info_tip(CListCtrl& ctrl, bool on)
{
	const DWORD exStyle = ctrl.GetExtendedStyle() | (on ? LVS_EX_INFOTIP : ~LVS_EX_INFOTIP);
	ctrl.SetExtendedStyle(exStyle);
}

	
void set_check_boxes(CListCtrl& ctrl, bool on)
{
	const DWORD exStyle = ctrl.GetExtendedStyle() | (on ? LVS_EX_CHECKBOXES : ~LVS_EX_CHECKBOXES);
	ctrl.SetExtendedStyle(exStyle);
}

auto has_check_boxes(const CListCtrl& ctrl) -> bool
{
	return (ctrl.GetExtendedStyle() & LVS_EX_CHECKBOXES) == LVS_EX_CHECKBOXES;
}

void assign_list_id_to_header(CListCtrl& ctrl)
{
	ctrl.GetHeaderCtrl()->SetDlgCtrlID(ctrl.GetDlgCtrlID());
}


int count_items(const CListCtrl& ctrl)
{
	return ctrl.GetItemCount();
}


int add_item(CListCtrl& ctrl, const std::wstring& text)
{
	return ctrl.InsertItem(ctrl.GetItemCount(), text.c_str());
}

	
std::wstring get_item_text(const CListCtrl& ctrl, int item_idx, int subitem_idx)
{
	return ctrl.GetItemText(item_idx, subitem_idx).GetString();
}


void set_item_text(CListCtrl& ctrl, int item_idx, int subitem_idx, const std::wstring& text)
{
	ctrl.SetItemText(item_idx, subitem_idx, text.c_str());
}


int add_column(CListCtrl& ctrl, const std::wstring& name, double width_perc)
{
	if (width_perc <= 0 || width_perc > 1) {
		TWIST_THROW(L"Invalid column width.");
	}

	const CRect client_rect = get_client_rect(ctrl);
	const long width = static_cast<long>((client_rect.Width() - ::GetSystemMetrics(SM_CXVSCROLL)) * width_perc);

	return ctrl.InsertColumn(count_columns(ctrl), name.c_str(), LVCFMT_LEFT, width);	
}


int count_columns(const CListCtrl& ctrl) 
{
	const auto* header_ctrl = ctrl.GetHeaderCtrl();
	if (header_ctrl != nullptr) {
		return header_ctrl->GetItemCount();
	}
	return 0;
}


bool move_item(CListCtrl& ctrl, int old_item_idx, int new_item_idx)
{
	const int item_count = count_items(ctrl);
	if (old_item_idx >= 0 && old_item_idx < item_count && new_item_idx >= 0 && new_item_idx < item_count && 
			old_item_idx != new_item_idx) {
		
		const int num_cols = count_columns(ctrl);
		
		// Store data about the item.
		ListCtrlItemInfo item_info;
		item_info.item.iItem = old_item_idx;
		ctrl.GetItem(&item_info.item);					
		for (long i = 1; i < num_cols; ++i) {
			const CString subitemText = ctrl.GetItemText(old_item_idx, i);
			item_info.subitems.push_back(static_cast<const wchar_t*>(subitemText));	
		}
		item_info.itemData = ctrl.GetItemData(old_item_idx);
		
		// Remove the item from the list.
		VERIFY(ctrl.DeleteItem(old_item_idx));
				
		// Insert the item at the drop location.
		item_info.item.iItem = new_item_idx;
		const long insert_idx = ctrl.InsertItem(&item_info.item);
		assert(insert_idx == new_item_idx);
		for (long i = 1; i < num_cols; ++i) {
			ctrl.SetItemText(insert_idx, i, item_info.subitems[i - 1]);
		}
		ctrl.SetItemData(insert_idx, item_info.itemData);
		
		return true;
	}

	return false;
}


int find_item_with_text(const CListCtrl& ctrl, const std::wstring& text, int start_idx)
{
	LVFINDINFO find_info;
	find_info.flags = LVFI_STRING;
	find_info.psz = text.c_str();
	return ctrl.FindItem(&find_info, start_idx);
}


bool delete_item(CListCtrl& ctrl, int item_idx)
{
	return ctrl.DeleteItem(item_idx) == TRUE;
}


int get_first_sel_item(const CListCtrl& ctrl)
{
	return ctrl.GetNextItem(k_no_item, LVNI_SELECTED);
}


std::set<int> get_sel_items(const CListCtrl& ctrl)
{
	std::set<int> item_indexes;
	int item_idx = k_no_item;
	while ((item_idx = ctrl.GetNextItem(item_idx, LVNI_SELECTED)) != k_no_item) {
		item_indexes.insert(end(item_indexes), item_idx);
	}
	return item_indexes;	
}


DWORD_PTR get_sel_item_data(const CListCtrl& ctrl)
{
	return get_sel_item_data<DWORD_PTR>(ctrl);
}


std::wstring get_sel_item_text(const CListCtrl& ctrl, int subitem_idx)
{
	const int item_idx = get_first_sel_item(ctrl);
	if (item_idx != k_no_item) {
		return get_item_text(ctrl, item_idx, subitem_idx);
	}
	return L"";
}


auto select_item(CListCtrl& ctrl, int item_idx, bool select) -> bool
{
	return ctrl.SetItemState(item_idx, select ? LVIS_SELECTED : 0, LVIS_SELECTED) == TRUE;	
}


bool select_and_focus_item(CListCtrl& ctrl, long item_idx, bool select_and_focus)
{
	return ctrl.SetItemState(item_idx, select_and_focus ? LVIS_SELECTED | LVIS_FOCUSED : 0, LVIS_SELECTED | LVIS_FOCUSED) == TRUE;
}


void unselect_all(CListCtrl& ctrl)
{
	const long num_items = ctrl.GetItemCount();
	for (long i = 0; i < num_items; ++i) {
		select_item(ctrl, i, false);
	}
}


size_t count_checked_items(const CListCtrl& ctrl)
{
	assert(has_check_boxes(ctrl));

	size_t ret = 0;
	int item_idx = k_no_item;
	while ((item_idx = ctrl.GetNextItem(item_idx, LVNI_ALL)) != k_no_item) {
		if (ctrl.GetCheck(item_idx) == TRUE) {
			++ret;
		}
	}
	return ret;
}


std::set<int> get_items_by_check(const CListCtrl& ctrl, bool checked)
{
	assert(has_check_boxes(ctrl));

	std::set<int> item_indexes;
	int item_idx = k_no_item;
	while ((item_idx = ctrl.GetNextItem(item_idx, LVNI_ALL)) != k_no_item) {
		if (checked == (ctrl.GetCheck(item_idx) == TRUE)) {
			item_indexes.insert(end(item_indexes), item_idx);
		}
	}
	return item_indexes;	
}


bool get_item_check(const CListCtrl& ctrl, int item_idx)
{
	assert(has_check_boxes(ctrl));
	return ctrl.GetCheck(item_idx) == TRUE;
}


void set_item_check(CListCtrl& ctrl, int item_idx, bool checked)
{
	assert(has_check_boxes(ctrl));
	ctrl.SetCheck(item_idx, checked ? TRUE : FALSE);
}


void set_all_items_check(CListCtrl& ctrl, bool checked)
{
	assert(has_check_boxes(ctrl));

	int item_idx = k_no_item;
	while ((item_idx = ctrl.GetNextItem(item_idx, LVNI_ALL)) != k_no_item) {
		ctrl.SetCheck(item_idx, checked ? TRUE : FALSE);
	}
}

auto is_location_over_item_checkbox(const CListCtrl& ctrl, CPoint location) -> bool
{
	assert(has_check_boxes(ctrl));

    auto hit_test_flags = UINT{0};
    ctrl.HitTest(location, &hit_test_flags);
    return (LVHT_ONITEMSTATEICON & hit_test_flags) == LVHT_ONITEMSTATEICON;
}

int get_max_text_width(const CListCtrl& ctrl, int col_idx)
{
	int max_text_width = 0;
	const int num_items = count_items(ctrl);
	for (long i = 0; i < num_items; ++i) {
		const CString text = ctrl.GetItemText(i, col_idx);
		max_text_width = std::max(ctrl.GetStringWidth(text), max_text_width);
	}
	return max_text_width;
}


CRect get_subitem_rect(const CListCtrl& ctrl, int item_idx, int col_idx)
{
	LVITEMINDEX lvItemIndex;
	lvItemIndex.iGroup = 0;
	lvItemIndex.iItem = item_idx;

	CRect rect;
	
	const BOOL result = ctrl.GetItemIndexRect(&lvItemIndex, col_idx, LVIR_LABEL, &rect);
	TWIST_ASSERT(result == TRUE);

	return rect;
}


void hit_test(const CListCtrl& ctrl, const POINT& pt, int& item_idx, int& subitem_idx)
{
	LVHITTESTINFO hit_test_info;
	hit_test_info.pt = pt;
	ctrl.HitTest(&hit_test_info);
	item_idx = hit_test_info.iItem;
	subitem_idx = hit_test_info.iSubItem;
}


void enable_group_view(CListCtrl& ctrl, bool enable)
{
	ctrl.EnableGroupView(enable ? TRUE : FALSE);
}


int insert_group(CListCtrl& ctrl, int group_id, const std::wstring& header, int header_align)
{
	LVGROUP lvgroup;
	ZeroMemory(&lvgroup, sizeof(LVGROUP));
	lvgroup.cbSize = sizeof(LVGROUP);
	lvgroup.mask = LVGF_HEADER | LVGF_GROUPID | LVGF_ALIGN;

	// Make a read-write copy of the caption
	std::vector<wchar_t> header_buffer(begin(header), end(header));
	header_buffer.push_back(0);	
	lvgroup.pszHeader = header_buffer.data();
	lvgroup.cchHeader = static_cast<int>(header_buffer.size());

	lvgroup.iGroupId = group_id;
	lvgroup.uAlign = header_align;

	return ctrl.InsertGroup(group_id, &lvgroup);
}


void add_items_to_group(CListCtrl& ctrl, int group_id, const std::set<int>& item_indexes)
{
	for (const int el : item_indexes) {
		LVITEM lvitem;
		::ZeroMemory(&lvitem, sizeof(LVITEM));
		lvitem.mask = LVIF_GROUPID;
		lvitem.iItem = el;
		lvitem.iGroupId = group_id; 			
		ctrl.SetItem(&lvitem);
	}
}

auto is_item_selection_event(const NMLISTVIEW* msg_data) -> bool
{
	assert(msg_data);
	return (msg_data->uOldState & LVIS_SELECTED) != 0 || (msg_data->uNewState & LVIS_SELECTED) != 0;
}

auto is_item_check_event(const NMLISTVIEW* msg_data) -> bool 
{
	assert(msg_data);
	
	// We signal a check event if the check (image) has been set or unset
	const int new_img_state = msg_data->uNewState & LVIS_STATEIMAGEMASK; 
	const int old_img_state = msg_data->uOldState & LVIS_STATEIMAGEMASK; 
	return ((old_img_state & 0x1000) != 0 && (new_img_state & 0x2000) != 0) ||
		   ((old_img_state & 0x2000) != 0 && (new_img_state & 0x1000) != 0);
}

auto location(const NMITEMACTIVATE* msg_data) -> CPoint
{
	assert(msg_data);
	return msg_data->ptAction;
}

}

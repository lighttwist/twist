///  @file  list_ctrl_utils.ipp
///  Inline implementation file for "list_ctrl_utils.hpp"

//   Copyright (c) 2007-2019 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "ctrl_utils.hpp"

namespace twist::mfc {

template<typename TData> 
int add_item(CListCtrl& ctrl, const std::wstring& text, TData data)
{
	const int item_pos = ctrl.InsertItem(ctrl.GetItemCount(), text.c_str());
	ctrl.SetItemData(item_pos, detail::user_to_internal_data<TData>(data, std::is_pointer<TData>()));
	return item_pos;
}

template<typename TData> 
int add_item(CListCtrl& ctrl, const std::wstring& text0, const std::wstring& text1, TData data)
{
	const int item_idx = add_item<TData>(ctrl, text0, data);
	ctrl.SetItemText(item_idx, 1, text1.c_str());
	return item_idx;
}

template<typename TData> 
TData get_item_data(const CListCtrl& ctrl, int item_idx)
{
	return detail::internal_to_user_data<TData>(ctrl.GetItemData(item_idx), std::is_pointer<TData>());
}

template<typename TData> 
void set_item_data(CListCtrl& ctrl, int item_idx, TData data)
{
	ctrl.SetItemData(item_idx, detail::user_to_internal_data(data, std::is_pointer<TData>()));
}

template<typename TData> 
int find_item_with_data(const CListCtrl& ctrl, TData data, int start_idx) 
{
	const int first_idx = (start_idx == k_no_item) ? 0 : start_idx;
	const int cnt = ctrl.GetItemCount();

	for (int i = first_idx; i < cnt; ++i) {
		if (get_item_data<TData>(ctrl, i) == data) {
			return i;
		}
	}

	return k_no_item;
}

template<typename TData> 
bool delete_item_with_data(CListCtrl& ctrl, TData data)
{
	const int item_idx = find_item_with_data<TData>(data);
	if (item_idx != k_no_item) {
		return ctrl.DeleteItem(item_idx) == TRUE;
	}
	return false;
}

template<typename TData> 
TData get_sel_item_data(const CListCtrl& ctrl)
{
	const int item_idx = get_first_sel_item(ctrl);
	if (item_idx != k_no_item) {
		return get_item_data<TData>(ctrl, item_idx);
	}
	return detail::internal_to_user_data<TData>(0, std::is_pointer<TData>());
}

template<class Rng>
requires std::is_convertible_v<rg::range_value_t<Rng>, int>
auto select_items(CListCtrl& ctrl, const Rng& item_indexes, bool select) -> bool
{
	auto ret = true;
	for (auto i : item_indexes) {
		ret = select_item(ctrl, i, select) && ret;
	}
	return ret;
}

template<class CompareData>
requires std::is_invocable_r_v<int, CompareData, LPARAM, LPARAM> 
auto sort_items(CListCtrl& ctrl, CompareData compare_data) -> bool
{
	struct Comparer {
		Comparer(CompareData func) : func_{func} {}
		auto call(LPARAM lparam1, LPARAM lparam2) -> int { return func_(lparam1, lparam2); }
		CompareData func_;
	};
	auto comparer = Comparer{compare_data};
	
	struct CompareFunc {
		static int CALLBACK call(LPARAM lparam1, LPARAM lparam2, LPARAM lparam_sort)
		{
			auto comparer = reinterpret_cast<Comparer*>(lparam_sort);
			assert(comparer);

			return comparer->call(lparam1, lparam2);
		}
	};

	return ctrl.SortItems(&CompareFunc::call, reinterpret_cast<LPARAM>(&comparer)) == TRUE;
}

template<class ItemData, class CompareData>
requires std::is_invocable_r_v<int, CompareData, const ItemData&, const ItemData&> 
auto sort_items_with_pointer_data(CListCtrl& ctrl, CompareData compare_data) -> bool
{
	return sort_items(ctrl, [compare_data](LPARAM lparam1, LPARAM lparam2) {
		auto data1 = reinterpret_cast<const ItemData*>(lparam1);
		assert(data1);
		auto data2 = reinterpret_cast<const ItemData*>(lparam2);
		assert(data2);
		return compare_data(*data1, *data2);
	});
}

}

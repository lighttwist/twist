///  @file  ProgressDlgBase.hpp
///  ProgressDlgBase  class, inherits CDialogEx

//   Copyright (c) 2010-2019 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_MFC_PROGRESS_DLG_BASE_HPP
#define TWIST_MFC_PROGRESS_DLG_BASE_HPP

#include "../ProgFeedbackProv.hpp"

#include <afxdialogex.h>

namespace twist::mfc {

///
///  Abstract base class for modal dialogue classes which display progress information. 
///
class ProgressDlgBase : public CDialogEx {
public:
	/// Find out whether an exception was thrown during the execution of the function.
	/// Call this method after  DoModal()  returns.
	///
	/// @return  true if so
	///
	bool was_exception_thrown() const;
	
	/// Get the message contained by the exception was thrown during the execution of the function (if one was thrown).
	///
	/// @return  The message, or a blank string if no exception was thrown
	///
	std::wstring get_exception_message() const;

protected:
	/// Constructor
	///
	/// @param[in]  res_id  The dialogue resource ID
	/// @param[in]  parent  The dialogue parent window; nullptr means the main application window
	///
	ProgressDlgBase(unsigned int res_id, CWnd* parent = nullptr);  
	
	virtual BOOL OnInitDialog() override;
	
	afx_msg void OnWindowPosChanged(WINDOWPOS* lpwndpos);
	
	afx_msg LRESULT OnMyMessage(WPARAM wParam, LPARAM lParam);

	DECLARE_DYNAMIC(ProgressDlgBase)
	
	DECLARE_MESSAGE_MAP()

private:
	/// Get the progress provider connected to the dialogue.
	///
	/// @return  The progress provider
	///
	virtual ProgFeedbackProv& get_prog_prov() = 0;

	std::wstring  caption_;  // The dialogue caption
	std::function<void (ProgFeedbackProv&)>  function_;  // The function whose progress is being displayed

	bool  shown_;

	bool  exception_thrown_;
	std::wstring  exception_message_;

	friend void show_prov_prog_dlg(ProgressDlgBase&, const std::wstring&, 
			std::function<void (ProgFeedbackProv&)>);

	TWIST_CHECK_INVARIANT_DECL
};

//
//  Free function
//

/// Display the modal progress provider dialogue, and execute the function whose progress is being displayed.
///
/// @param[in]  dlg  The dialogue object (before the window is displayed)
/// @param[in]  caption  The progress dialogue caption
/// @param[in]  func  The function
///
void show_prov_prog_dlg(ProgressDlgBase& dlg, const std::wstring& caption, 
		std::function<void (ProgFeedbackProv&)> func);

}

#endif

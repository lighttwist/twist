///  @file  CtrlInPlaceEdit.ipp
///  Inline implementation file for "CtrlInPlaceEdit.hpp"

//   Copyright (c) 2007-2019, Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "list_ctrl_utils.hpp"

namespace twist::mfc {	

static const unsigned int k_edt_parent_res_id = 998;
static const unsigned int k_edt_res_id = 999;

BEGIN_TEMPLATE_MESSAGE_MAP(CtrlInPlaceEdit, TEditCtrl, CCmdTarget)
	ON_EN_KILLFOCUS(k_edt_res_id, on_edt_killfocus)
END_MESSAGE_MAP()


template<class TEditCtrl>
CtrlInPlaceEdit<TEditCtrl>::CtrlInPlaceEdit(std::unique_ptr<TEditCtrl> ctrl, CWnd& parent, OnExit on_exit) 
	: CWnd()
	, ctrl_(move(ctrl))
	, on_exit_(on_exit)
	, lst_ctrl_item_idx_(-1)
	, lst_ctrl_col_idx_(-1)
	, char_filter_()
	, esc_pressed_(false)
	, ignore_killfocus_(false)
{
	Create(L"STATIC", L"", WS_CHILD, CRect(0, 0, 0, 0), &parent, k_edt_parent_res_id);
	ctrl_->Create(WS_CHILD | WS_TABSTOP | WS_BORDER | ES_AUTOHSCROLL, CRect(0, 0, 0, 0), this, k_edt_res_id);
	ctrl_->SetFont(parent.GetFont());
	TWIST_CHECK_INVARIANT
}


template<class TEditCtrl>
CtrlInPlaceEdit<TEditCtrl>::~CtrlInPlaceEdit()
{
	TWIST_CHECK_INVARIANT
	ctrl_->DestroyWindow();
	::DestroyWindow(m_hWnd);
	m_hWnd = nullptr;
}


template<class TEditCtrl>
void CtrlInPlaceEdit<TEditCtrl>::show(const CRect& ctrl_rect, const std::wstring& text)
{
	TWIST_CHECK_INVARIANT
	this->MoveWindow(&ctrl_rect, FALSE/*repaint*/);
	this->ShowWindow(SW_SHOW);

	ctrl_->MoveWindow(get_client_rect(*this), TRUE/*repaint*/);
	ctrl_->ShowWindow(SW_SHOW);

	set_wnd_text(*ctrl_, text);
	ctrl_->SetFocus();
	ctrl_->SetCapture();
	ctrl_->SetSel(0, -1); 
	TWIST_CHECK_INVARIANT
}


template<class TEditCtrl>
void CtrlInPlaceEdit<TEditCtrl>::show_over_lst_ctrl_cell(const CListCtrl& lst_ctrl, int item_idx, int col_idx)
{
	TWIST_CHECK_INVARIANT
	auto rect = get_subitem_rect(lst_ctrl, item_idx, col_idx);

	// The cell may not be all visible within the list control client area
	CRect visible_rect;
	visible_rect.IntersectRect(rect, get_client_rect(lst_ctrl));	

	// Make sure to transform the rectangle to coordinates relative to the parent window's client area
	auto& parent_wnd = *CWnd::GetParent();
	if (&lst_ctrl != &parent_wnd) {
		visible_rect = client_to_screen(visible_rect, lst_ctrl);
		visible_rect = screen_to_client(visible_rect, parent_wnd);
	} 
	if (visible_rect.Width() != 0 && visible_rect.Height() != 0) {
		lst_ctrl_item_idx_ = item_idx;
		lst_ctrl_col_idx_ = col_idx;
		show(visible_rect, get_item_text(lst_ctrl, item_idx, col_idx));
	}
	TWIST_CHECK_INVARIANT
}


template<class TEditCtrl>
int CtrlInPlaceEdit<TEditCtrl>::get_lst_ctrl_item_idx() const
{
	TWIST_CHECK_INVARIANT
	return lst_ctrl_item_idx_;
}


template<class TEditCtrl>
int CtrlInPlaceEdit<TEditCtrl>::get_lst_ctrl_col_idx() const
{
	TWIST_CHECK_INVARIANT
	return lst_ctrl_col_idx_;
}


template<class TEditCtrl>
void CtrlInPlaceEdit<TEditCtrl>::set_char_filter(CharFilter char_filter)
{
	TWIST_CHECK_INVARIANT
	char_filter_ = char_filter;
	TWIST_CHECK_INVARIANT
}


template<class TEditCtrl>
void CtrlInPlaceEdit<TEditCtrl>::hide()
{
	TWIST_CHECK_INVARIANT
	ctrl_->ShowWindow(SW_HIDE);
	this->ShowWindow(SW_HIDE);
	TWIST_CHECK_INVARIANT
}


template<class TEditCtrl>
BOOL CtrlInPlaceEdit<TEditCtrl>::PreTranslateMessage(MSG* pMsg)
{
	TWIST_CHECK_INVARIANT
	if (pMsg->hwnd == ctrl_->GetSafeHwnd()) {
		if (pMsg->message == WM_CHAR) {
			if (!on_edt_char(static_cast<unsigned int>(pMsg->wParam))) {
				return TRUE;
			}
		}
		else if (pMsg->message == WM_KEYDOWN) { 
			if (pMsg->wParam == VK_RETURN) {
				on_edt_killfocus();
				return TRUE;
			}
			else if (pMsg->wParam == VK_ESCAPE) {
				esc_pressed_ = true;
				on_edt_killfocus();
				return TRUE;
			}
		}
	}

	return CWnd::PreTranslateMessage(pMsg);
}


template<class TEditCtrl>
bool CtrlInPlaceEdit<TEditCtrl>::on_edt_char(unsigned int chr)
{
	TWIST_CHECK_INVARIANT
	if (char_filter_) {
		return char_filter_(static_cast<wchar_t>(chr));
	}
	return true;
}


template<class TEditCtrl>
void CtrlInPlaceEdit<TEditCtrl>::on_edt_killfocus()
{
	TWIST_CHECK_INVARIANT
	if (!ignore_killfocus_) {
		// Avoid re-entering the function
		ignore_killfocus_ = true;

		// Call the "exit" handler, but not if ESCAPE was pressed
		if (!esc_pressed_) {
			on_exit_(get_wnd_text(*ctrl_));
		}

		// Make sure the control is hidden
		hide();

		lst_ctrl_col_idx_ = -1;
		lst_ctrl_item_idx_ = -1;
		esc_pressed_ = false;
		ignore_killfocus_ = false;
	}
	TWIST_CHECK_INVARIANT
}


#ifdef _DEBUG
template<class TEditCtrl>
void CtrlInPlaceEdit<TEditCtrl>::check_invariant() const noexcept
{
	assert(ctrl_);
	assert(on_exit_);
}
#endif // _DEBUG

}

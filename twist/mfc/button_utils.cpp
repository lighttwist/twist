//  Implementation file for "button_utils.hpp"
//
//  Copyright (c) 2007-2022, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist_mfc_maker.hpp"

#include "button_utils.hpp"

namespace twist::mfc {

auto is_checked(const CButton& button) -> bool
{
	assert(((button.GetButtonStyle() & BS_CHECKBOX) != 0) || ((button.GetButtonStyle() & BS_RADIOBUTTON) != 0) || 
	       ((button.GetButtonStyle() & BS_AUTORADIOBUTTON) != 0));

	return button.GetCheck() == BST_CHECKED;
}


auto set_checked(CButton & button, bool checked) -> void
{
	assert(((button.GetButtonStyle() & BS_CHECKBOX) != 0) || ((button.GetButtonStyle() & BS_RADIOBUTTON) != 0) || 
	       ((button.GetButtonStyle() & BS_AUTORADIOBUTTON) != 0));

	button.SetCheck(checked ? BST_CHECKED : BST_UNCHECKED);
}

}

///  @file  mfc/combo_box_utils.hpp
///  Utilities for working with the MFC control class  CComboBox

//   Copyright (c) 2010-2019 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_MFC_COMBO__BOX__UTILS_HPP
#define TWIST_MFC_COMBO__BOX__UTILS_HPP

namespace twist::mfc {

/// Add a string item to the list box of a combo box.
///
/// @param[in]  ctrl  The combo box control
/// @param[in]  text  The string
/// @return  The index of the new item
///
int add_item(CComboBox& ctrl, const std::wstring& text); 

/// Add a string item to the list box of a combo box. This function will take into consideration whether the combo box has the
/// "sorted" style or not.
///
/// @tparam  TData  The type of the data value; must be convertible to a numeric type 
/// @param[in]  ctrl  The combo box control
/// @param[in]  text  The string
/// @param[in]  data  The item data
/// @return  The index of the new item
///
template<typename TData> int add_item(CComboBox& ctrl, const std::wstring& text, TData data); 

/// Insert an item at a specific position in the list box of a combo box. This function will not take into consideration whether 
/// the combo box has the "sorted" style or not.
///
/// @tparam  TData  The type of the data value; must be convertible to a numeric type 	
/// @param[in]  ctrl  The combo box control
/// @param[in]  text  The string.
/// @param[in]  data  The item data.
/// @param[in]  index  The position in the list box that will receive the new item. If this parameter is �1, the string 
///				is added to the end of the list.
/// @return  The index of the new item.
///
template<typename TData> int insert_item(CComboBox& ctrl, const std::wstring& text, TData data, int index);

/// Get the number of items in the list-box portion of a combo box.
///
/// @return  The item count
///
int count_items(const CComboBox& ctrl);

/// Get a string corresponding to an item in the list box of a combo box.
///
/// @param[in]  ctrl  The combo box control
/// @param[in]  item_idx  The item index
/// @return  The item string
///
std::wstring get_item_text(const CComboBox& ctrl, int item_idx);

/// Selects an item in the list box of a combo box. If necessary, the list box scrolls the string into view (if the list box is 
/// visible). The text in the edit control of the combo box is changed to reflect the new selection. Any previous selection in 
/// the list box is removed.
///
/// @param[in]  ctrl  The combo box control
/// @param[in]  item_idx  The index of the item to select; if CB_ERR, any current selection in the list box is removed and the 
///				edit control is cleared
/// @return  true if the item was successfully selected or if the item index passed in is CB_ERR; false otherwise
///
bool select_item(CComboBox& ctrl, int item_idx);

/// Search for a string in the list box of a combo box, and if the string is found, select the corresponding item in the list box 
/// and copy it to the edit control.
///
/// @param[in]  ctrl  The combo box control
/// @param[in]  str  The string
/// @return  true if the item was successfully selected
///
bool select_item_with_text(CComboBox& ctrl, const std::wstring& str);

/// Get the index of the currently selected item in a combo box.
///
/// @param[in]  ctrl  The combo box control
/// @return  The selected item index, or CB_ERR if no item is currently selected
///
int get_sel_item(const CComboBox& ctrl);

/// Find the first item in a combo box which has a specific associated data value.
/// 
/// @tparam  TData  The type of the data value; must be convertible to a numeric type 
/// @param[in]  ctrl  The combo box control
/// @param[in]  data  The data value associated with the item
/// @return  The index of the first item containing the data, if any, or CB_ERR if such an item is not found. 
///
template<typename TData> int find_item_with_data(const CComboBox& ctrl, TData data);

/// Get the text associated with the currently selected item in a combo box (if there is a selected item).
///
/// @param[in]  ctrl  The combo box control
/// @return  The text, if an item is currently selected, otherwise a blank string.
///
std::wstring get_sel_item_text(const CComboBox& ctrl);

/// Get the data associated with the currently selected item (if any) in a combo box control.
///
/// @param[in]  ctrl  The combo box control
/// @return  The data, if an item is currently selected, otherwise the zero value
///
DWORD_PTR get_sel_item_data(const CComboBox& ctrl);
	
/// Get the data associated with the currently selected item (if any) in a combo box control.
///
/// @tparam  TData  The type of the data value; must be convertible to a numeric type
/// @param[in]  ctrl  The combo box control
/// @return  The data, if an item is currently selected, otherwise the zero value
///
template<typename TData> TData get_sel_item_data(const CComboBox& ctrl);

/// Select the first item (if any) that has particular associated data in a combo box control.
///
/// @tparam  TData  The type of the data value; must be convertible to a numeric type 	
/// @param[in]  ctrl  The combo box control
/// @param[in]  data  The data value
/// @return  The index of the selected item, or  CB_ERR  if no item matched the data passed in
///
template<typename TData> int select_item_with_data(CComboBox& ctrl, TData data);

/// Delete a specific item from a combo box.
/// 
/// @tparam  TData  The type of the data value; must be convertible to a numeric type 	
/// @param[in]  ctrl  The combo box control
/// @param[in]  data  The data of the item to be deleted. The first item containing this data will be deleted.
/// @return  true if the item was found and deleted.
///
template<typename TData> bool delete_item_with_data(CComboBox& ctrl, TData data);

} 

#include "combo_box_utils.ipp"

#endif 

///  @file   ChildDlgList.hpp  
///  ChildDlgList  class template 

//   Copyright (c) 2010-2019 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_MFC_CHILD_DLG_LIST_HPP
#define TWIST_MFC_CHILD_DLG_LIST_HPP

#include "twist/metaprogramming.hpp"

namespace twist::mfc {

///
///  Maintains a list of  CDialog  (or derived) objects that are embedded as children into a parent window and 
///    provides some common functionality for the children.
///  One (or zero) child dialog is active and visible at any one time.
///  The template argument  TDlgId  is the type of a unique ID to be associated with each child dialog.
///
template<typename TDlgId>
class ChildDlgList : public twist::NonCopyable {
public:
	/// Constructor.
	///
	/// @param[in]  parent  The parent window for all dialogs in the list.
	///
	ChildDlgList(CWnd& parent);

	/// Destructor.
	///
	virtual ~ChildDlgList();

	/// Add a new child dialog to the list.
	///
	/// @tparam  ChildDlgClass  The type of the child dialog to be added
	/// @param[in]  dlgId  The child dialog ID.
	/// @param[in]  resourceId  The child dialog resource ID.
	/// @return  The new child dialog.
	///
	template<class ChildDlgClass,
	         class = EnableIfBaseOf<CDialog, ChildDlgClass>> 
	ChildDlgClass& add(TDlgId dlgId, long resourceId);
	
	/// Erase a child dialog to the list.
	///
	/// @param[in]  dlgId  The child dialog ID.
	///
	void erase(TDlgId dlgId);

	/// Move a child dialog.
	///
	/// @param[in]  dlgId  The child dialog ID.
	/// @param[in]  dlgRect  The new dialog position (relative to the parent window).
	///
	void move(TDlgId dlgId, const CRect& dlgRect);
	
	/// Move all child dialogs to the same position.
	///
	/// @param[in]  dlgRect  The new dialog position (relative to the parent window).
	///
	void moveAll(const CRect& dlgRect);

	/// Set the active child dialog.
	///
	/// @param[in]  dlgId  The child dialog ID.
	///
	void setActive(TDlgId dlgId);

	/// Get the currently active child dialog. An exception is thrown if there is no dialog is ative.
	///
	/// @return  The active child dialog.
	///
	CDialog& getActive();
	
	/// Get a child dialog.
	///
	/// @param[in]  dlgId  The child dialog ID.
	/// @return  The child dialog.
	///
	CDialog& get(TDlgId dlgId); 

private:
	typedef std::shared_ptr<CDialog>  DialogPtr;
	typedef std::map<TDlgId, DialogPtr>  DlgMap;

	CWnd&  m_parent;  // The parent window
	DlgMap  m_childDlgMap;  // The map of child dialog objects
	std::unique_ptr<TDlgId>  m_activeDlgId;  // The ID of the currently active child dialog, or nullptr if no dialog is active

	TWIST_CHECK_INVARIANT_DECL
};

}

#include "ChildDlgList.ipp"

#endif 

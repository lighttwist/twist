/// @file  serialisation_utils.hpp
/// Utilities related to serialisation with CArchive

//  Copyright (c) 2007-2022, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#pragma once

namespace twist::mfc {

/*! Stream an std::wstring object into an MFC serialisation archive.
    \param[in] ar  The archive
    \param[in] obj  The string object
    \return  The archive (for chaining)
 */
auto operator<<(CArchive& ar, const std::wstring& obj) -> CArchive&;

/*! Stream an std::wstring object out of an MFC serialisation archive.

    \param[in] ar  The archive
    \param[out] obj  The string object
    \return  The archive (for chaining)
 */
auto operator>>(CArchive& ar, std::wstring& obj) -> CArchive&;

/*! Stream an std::vector object into an MFC serialisation archive.
    \tparam Elem  The vector element type; operator<<(CArchive&, const Elem&) must exist
    \param[in] ar  The archive
    \param[in] obj  The vector object
    \return  The archive (for chaining)
 */
template<class Elem,
         class Allocator = std::allocator<Elem>>
auto operator<<(CArchive& ar, const std::vector<Elem, Allocator>& obj) -> CArchive&;

/*! Stream an std::vector object out of an MFC serialisation archive.
    \tparam Elem  The vector element type; operator>>(CArchive&, Elem&) must exist
    \param[in] ar  The archive
    \param[out] obj  The vector object
    \return  The archive (for chaining)
 */
template<class Elem, class Allocator>
auto operator>>(CArchive& ar, std::vector<Elem, Allocator>& obj) -> CArchive&;

/*! Stream an std::map object into an MFC serialisation archive.
    \tparam Key  Key type; operator<<(CArchive&, const Key&) must exist 
    \tparam Value  Value type; operator<<(CArchive&, const Value&) must exist 
    \tparam Compare  Comparison callable type for sorting the keys 
    \tparam Allocator  Allocator type for map elements 
    \param[in] ar  The archive
	\param[in] obj  The map object
    \return  The archive (for chaining)
 */
template<class Key, class Value, class Compare, class Allocator>
auto operator<<(CArchive& ar, const std::map<Key, Value, Compare, Allocator>& obj) -> CArchive&;

/*! Stream an std::map object out of an MFC serialisation archive.
    \tparam Key  Key type; operator>>(CArchive&, Key&) must exist
    \tparam Value  Value type; operator>>(CArchive&, Value&) must exist
    \tparam Compare  Comparison callable type for sorting the keys 
    \tparam Allocator  Allocator type for map elements 
    \param[in] ar  The archive
    \param[out] obj  The vector object
    \return  The archive (for chaining)
 */
template<class Key, class Value, class Compare, class Allocator>
auto operator>>(CArchive& ar, std::map<Key, Value, Compare, Allocator>& obj) -> CArchive&;

} 

#include "serialisation_utils.ipp"

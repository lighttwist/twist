///  @file  DualKeyedList.ipp
///  Inline implementation file for "DualKeyedList.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

namespace twist {

template<typename Obj, typename Id, typename Chr, bool case_sens, bool owns_objects>
DualKeyedList<Obj, Id, Chr, case_sens, owns_objects>::DualKeyedList() 
{
	TWIST_CHECK_INVARIANT
}


template<typename Obj, typename Id, typename Chr, bool case_sens, bool owns_objects>
DualKeyedList<Obj, Id, Chr, case_sens, owns_objects>::~DualKeyedList()
{
	TWIST_CHECK_INVARIANT_NO_SCOPE
	if constexpr (owns_objects) {
		for (auto& el : elem_list_) {
			delete el.obj();
		}
	}
}


template<typename Obj, typename Id, typename Chr, bool case_sens, bool owns_objects>
void DualKeyedList<Obj, Id, Chr, case_sens, owns_objects>::assign(const DualKeyedList& src)
{
	TWIST_CHECK_INVARIANT
	static_assert(!owns_objects, "Cannot copy elements between lists which own their elements.");
		
	elem_list_.assign(src.elem_list_.begin(), src.elem_list_.end());
	
	id_elem_map_.clear();
	id_elem_map_.insert(src.id_elem_map_.begin(), src.id_elem_map_.end());
	
	name_elem_map_.clear();
	name_elem_map_.insert(src.name_elem_map_.begin(), src.name_elem_map_.end());	
}


template<typename Obj, typename Id, typename Chr, bool case_sens, bool owns_objects>
const Obj& DualKeyedList<Obj, Id, Chr, case_sens, owns_objects>::at(Ssize pos) const
{
	TWIST_CHECK_INVARIANT
	const Elem& elem = elem_list_.at(pos);
	return *elem.obj();
}


template<typename Obj, typename Id, typename Chr, bool case_sens, bool owns_objects>
Obj& DualKeyedList<Obj, Id, Chr, case_sens, owns_objects>::at(Ssize pos)
{
	TWIST_CHECK_INVARIANT
	Elem& elem = elem_list_.at(pos);
	TWIST_CHECK_INVARIANT
	return *elem.obj();
}


template<typename Obj, typename Id, typename Chr, bool case_sens, bool owns_objects>
const Obj* DualKeyedList<Obj, Id, Chr, case_sens, owns_objects>::find(Id id) const
{
	TWIST_CHECK_INVARIANT
	const Obj* obj = nullptr;
	const auto it = id_elem_map_.find(id);
	if (it != id_elem_map_.end()) {
		obj = it->second.obj();
	}
	return obj;
}


template<typename Obj, typename Id, typename Chr, bool case_sens, bool owns_objects>
Obj* DualKeyedList<Obj, Id, Chr, case_sens, owns_objects>::find(Id id)
{
	TWIST_CHECK_INVARIANT
	Obj* obj = nullptr;
	const auto it = id_elem_map_.find(id);
	if (it != id_elem_map_.end()) {
		obj = it->second.obj();
	}
	TWIST_CHECK_INVARIANT
	return obj;
}


template<typename Obj, typename Id, typename Chr, bool case_sens, bool owns_objects>
const Obj* DualKeyedList<Obj, Id, Chr, case_sens, owns_objects>::find(const String& name) const
{
	TWIST_CHECK_INVARIANT
	const Obj* obj = nullptr;
	const auto it = name_elem_map_.find(name);
	if (it != name_elem_map_.end()) {
		obj = it->second.obj();
	}
	return obj;
}


template<typename Obj, typename Id, typename Chr, bool case_sens, bool owns_objects>
Obj* DualKeyedList<Obj, Id, Chr, case_sens, owns_objects>::find(const String& name)
{
	TWIST_CHECK_INVARIANT
	Obj* obj = nullptr;
	const auto it = name_elem_map_.find(name);
	if (it != name_elem_map_.end()) {
		obj = it->second.obj();
	}
	TWIST_CHECK_INVARIANT
	return obj;
}	


template<typename Obj, typename Id, typename Chr, bool case_sens, bool owns_objects>
const Obj& DualKeyedList<Obj, Id, Chr, case_sens, owns_objects>::get(Id id) const
{
	TWIST_CHECK_INVARIANT
	using std::to_wstring;

	const auto it = id_elem_map_.find(id);
	if (it == id_elem_map_.end()) {
		TWIST_THROW(L"The  ID \"%d\" is not a key in this list.", to_wstring(id).c_str());		
	}
	return *it->second.obj();
}


template<typename Obj, typename Id, typename Chr, bool case_sens, bool owns_objects>
Obj& DualKeyedList<Obj, Id, Chr, case_sens, owns_objects>::get(Id id)
{
	TWIST_CHECK_INVARIANT
	using std::to_wstring;

	const auto it = id_elem_map_.find(id);
	if (it == id_elem_map_.end()) {
		TWIST_THROW(L"The  ID \"%d\" is not a key in this list.", to_wstring(id).c_str());		
	}
	TWIST_CHECK_INVARIANT
	return *it->second.obj();
}
	

template<typename Obj, typename Id, typename Chr, bool case_sens, bool owns_objects>
const Obj& DualKeyedList<Obj, Id, Chr, case_sens, owns_objects>::get(const String& name) const
{
	TWIST_CHECK_INVARIANT
	const auto it = name_elem_map_.find(name);
	if (it == name_elem_map_.end()) {
		TWIST_THROW(L"The name \"%s\" is not a key in this list.", name.c_str());
	}
	return *it->second.obj();
}


template<typename Obj, typename Id, typename Chr, bool case_sens, bool owns_objects>
Obj& DualKeyedList<Obj, Id, Chr, case_sens, owns_objects>::get(const String& name)
{
	TWIST_CHECK_INVARIANT
	const auto it = name_elem_map_.find(name);
	if (it == name_elem_map_.end()) {
		TWIST_THROW(L"The name \"%s\" is not a key in this list.", name.c_str());
	}
	TWIST_CHECK_INVARIANT
	return *it->second.obj();
}


template<typename Obj, typename Id, typename Chr, bool case_sens, bool owns_objects>
Ssize DualKeyedList<Obj, Id, Chr, case_sens, owns_objects>::get_pos(Id id) const
{
	TWIST_CHECK_INVARIANT
	Ssize retVal = k_no_pos;
	
	auto it1 = id_elem_map_.find(id);
	if (it1 != id_elem_map_.end()) {
		auto it2 = std::find(elem_list_.begin(), elem_list_.end(), it1->second);
		if (it2 == elem_list_.end()) {
			TWIST_THROW(L"The list is corrupted.");
		}
		retVal = it2 - elem_list_.begin();
	}

	return retVal;	
}


template<typename Obj, typename Id, typename Chr, bool case_sens, bool owns_objects>
Ssize DualKeyedList<Obj, Id, Chr, case_sens, owns_objects>::get_pos(const String& name) const
{
	TWIST_CHECK_INVARIANT
	Ssize retVal = k_no_pos;

	const auto it1 = name_elem_map_.find(name);
	if (it1 != name_elem_map_.end()) {

		const auto it2 = std::find(elem_list_.begin(), elem_list_.end(), it1->second);
		if (it2 == elem_list_.end()) {
			TWIST_THROW(L"The list is corrupted.");
		}
		retVal = it2 - elem_list_.begin();
	}

	return retVal;	
}


template<typename Obj, typename Id, typename Chr, bool case_sens, bool owns_objects>
Id DualKeyedList<Obj, Id, Chr, case_sens, owns_objects>::get_id_for_name(const String& name) const
{
	TWIST_CHECK_INVARIANT
	const auto it = name_elem_map_.find(name);
	if (it == std::end(name_elem_map_)) {
		TWIST_THROW(L"The name \"%s\" is not a key in the list.", name.c_str());
	}
	return it->second.id();
}


template<typename Obj, typename Id, typename Chr, bool case_sens, bool owns_objects>
typename DualKeyedList<Obj, Id, Chr, case_sens, owns_objects>::const_iterator 
DualKeyedList<Obj, Id, Chr, case_sens, owns_objects>::begin() const
{
	TWIST_CHECK_INVARIANT
	return elem_list_.begin();
}


template<typename Obj, typename Id, typename Chr, bool case_sens, bool owns_objects>
typename DualKeyedList<Obj, Id, Chr, case_sens, owns_objects>::iterator 
DualKeyedList<Obj, Id, Chr, case_sens, owns_objects>::begin()
{
	TWIST_CHECK_INVARIANT
	return elem_list_.begin();
}


template<typename Obj, typename Id, typename Chr, bool case_sens, bool owns_objects>
typename DualKeyedList<Obj, Id, Chr, case_sens, owns_objects>::const_iterator 
DualKeyedList<Obj, Id, Chr, case_sens, owns_objects>::end() const
{
	TWIST_CHECK_INVARIANT
	return elem_list_.end();
}


template<typename Obj, typename Id, typename Chr, bool case_sens, bool owns_objects>
typename DualKeyedList<Obj, Id, Chr, case_sens, owns_objects>::iterator 
DualKeyedList<Obj, Id, Chr, case_sens, owns_objects>::end()
{
	TWIST_CHECK_INVARIANT
	return elem_list_.end();
}


template<typename Obj, typename Id, typename Chr, bool case_sens, bool owns_objects>
Ssize DualKeyedList<Obj, Id, Chr, case_sens, owns_objects>::size() const
{
	TWIST_CHECK_INVARIANT
	return size32(elem_list_);
}


template<typename Obj, typename Id, typename Chr, bool case_sens, bool owns_objects>
bool DualKeyedList<Obj, Id, Chr, case_sens, owns_objects>::empty() const
{
	TWIST_CHECK_INVARIANT
	return elem_list_.empty();
}


template<typename Obj, typename Id, typename Chr, bool case_sens, bool owns_objects>
Obj& DualKeyedList<Obj, Id, Chr, case_sens, owns_objects>::push_back(Obj* obj, Id id, const String& name)
{
	TWIST_CHECK_INVARIANT
	if (obj == nullptr) {
		TWIST_THROW(L"This list can only store valid pointers.");
	}

	Elem elem{obj, id, name};
	
	// First, check the the ID does not already exist in the list.
	const auto idMapRet = id_elem_map_.emplace(id, elem);
	if (!idMapRet.second) {
		TWIST_THROW(L"An object keyed on this ID already exists in the list.");
	}
	// Second, check the the name does not already exist in the list.
	const auto nameMapRet = name_elem_map_.emplace(name, elem);
	if (!nameMapRet.second) {
		id_elem_map_.erase(idMapRet.first);
		TWIST_THROW(L"An object keyed on this name already exists in the list.");
	}
	// Third, check the the object does not already exist in the list.
	if (std::find(elem_list_.begin(), elem_list_.end(), elem) != elem_list_.end()) {
		TWIST_THROW(L"List is corrupted.");
	}
	elem_list_.push_back(elem);

	TWIST_CHECK_INVARIANT
	return *obj;
}


template<typename Obj, typename Id, typename Chr, bool case_sens, bool owns_objects>
void DualKeyedList<Obj, Id, Chr, case_sens, owns_objects>::erase(Id id)
{
	TWIST_CHECK_INVARIANT
	Obj* obj = extract(id);
	if constexpr (owns_objects) {
		TWIST_DELETE(obj);
	}
	TWIST_CHECK_INVARIANT
}


template<typename Obj, typename Id, typename Chr, bool case_sens, bool owns_objects>
void DualKeyedList<Obj, Id, Chr, case_sens, owns_objects>::erase(const String& name)
{
	TWIST_CHECK_INVARIANT
	Obj* obj = extract(name);
	if constexpr (owns_objects) {
		TWIST_DELETE(obj);
	}
	TWIST_CHECK_INVARIANT
}


template<typename Obj, typename Id, typename Chr, bool case_sens, bool owns_objects>
Obj* DualKeyedList<Obj, Id, Chr, case_sens, owns_objects>::extract(Id id)
{
	TWIST_CHECK_INVARIANT
	using std::to_wstring;

	const auto idMapIt = id_elem_map_.find(id);
	if (idMapIt == id_elem_map_.end()) {
		TWIST_THROW(L"The ID \"%d\" is not a key in this list.", to_wstring(id).c_str());
	}
	auto elem = idMapIt->second;
	
	const auto nameMapIt = name_elem_map_.find(idMapIt->second.name());
	if (nameMapIt == name_elem_map_.end()) {
		TWIST_THROW(L"No name in the list for ID \"%d\". List is corrupted.", to_wstring(id).c_str());
	}
	
	const auto listIt = std::find(elem_list_.begin(), elem_list_.end(), elem);
	if (listIt == elem_list_.end()) {
		TWIST_THROW(L"No object in the list for ID \"%d\". List is corrupted.", to_wstring(id).c_str());
	}

	id_elem_map_.erase(idMapIt);
	name_elem_map_.erase(nameMapIt);
	elem_list_.erase(listIt);

	TWIST_CHECK_INVARIANT
	return elem.obj();
}


template<typename Obj, typename Id, typename Chr, bool case_sens, bool owns_objects>
Obj* DualKeyedList<Obj, Id, Chr, case_sens, owns_objects>::extract(const String& name)
{
	TWIST_CHECK_INVARIANT
	auto nameMapIt = name_elem_map_.find(name);
	if (nameMapIt == name_elem_map_.end()) {
		TWIST_THROW(L"Name \"%s\" is not a key in the list.", name.c_str());
	}
	Elem elem = nameMapIt->second;
	
	auto idMapIt = id_elem_map_.find(nameMapIt->second.id());
	if (idMapIt == id_elem_map_.end()) {
		TWIST_THROW(L"No ID in the list for name \"%s\". List is corrupted.", name.c_str());
	}
	
	auto listIt = std::find(elem_list_.begin(), elem_list_.end(), elem);
	if (listIt == elem_list_.end()) {
		TWIST_THROW(L"No object in the list for name \"%s\". List is corrupted.", name.c_str());
	}
	
	name_elem_map_.erase(nameMapIt);
	id_elem_map_.erase(idMapIt);
	elem_list_.erase(listIt);

	TWIST_CHECK_INVARIANT
	return elem.obj();
}


template<typename Obj, typename Id, typename Chr, bool case_sens, bool owns_objects>
void DualKeyedList<Obj, Id, Chr, case_sens, owns_objects>::clear()
{
	TWIST_CHECK_INVARIANT
	if constexpr (owns_objects) {
		for (auto it = elem_list_.begin(); it != elem_list_.end(); ++it) {
			delete it->obj();
		}
	}
	elem_list_.clear();
	id_elem_map_.clear();
	name_elem_map_.clear();
	TWIST_CHECK_INVARIANT
}


template<typename Obj, typename Id, typename Chr, bool case_sens, bool owns_objects>
void DualKeyedList<Obj, Id, Chr, case_sens, owns_objects>::sort_by_id()
{
	TWIST_CHECK_INVARIANT
	elem_list_.clear();
	for (auto it = id_elem_map_.begin(); it != id_elem_map_.end(); ++it) {
		elem_list_.push_back(it->second);	
	}
}


template<typename Obj, typename Id, typename Chr, bool case_sens, bool owns_objects>
void DualKeyedList<Obj, Id, Chr, case_sens, owns_objects>::sort_by_name()
{
	TWIST_CHECK_INVARIANT
	elem_list_.clear();
	for (auto it = name_elem_map_.begin(); it != name_elem_map_.end(); ++it) {
		elem_list_.push_back(it->second);	
	}
}


template<typename Obj, typename Id, typename Chr, bool case_sens, bool owns_objects>
Obj& DualKeyedList<Obj, Id, Chr, case_sens, owns_objects>::change_name(const String& old_name, const String& new_name)
{
	TWIST_CHECK_INVARIANT
	// First check whether the old name exists as a key.
	const auto it1 = name_elem_map_.find(old_name);
	if (it1 == std::end(name_elem_map_)) {
		TWIST_THROW(L"Name \"%s\" is not a key in the list.", old_name.c_str());
	}

	// Second make sure that the new name doesn't already exist as a key.
	if (name_elem_map_.find(new_name) != std::end(name_elem_map_)) {
		TWIST_THROW(L"The new name \"%s\" is already a key in the list.", new_name.c_str());
	}
	// All good. 
	// (a) Change the name in the name map (erase the old element and insert a new one).
	const Elem elem{it1->second.obj(), it1->second.id(), new_name};
	name_elem_map_.erase(it1);
	name_elem_map_.emplace(new_name, elem);
	
	// (b) Change the name in the ID map element.
	auto it2 = id_elem_map_.find(elem.id());
	if (it2 == id_elem_map_.end()) {
		TWIST_THROW(L"The old name \"%s\" cannot be found in the ID map. The dual list is corrupted.", old_name.c_str());
	}
	it2->second.name_ = new_name;

	// (c) Change the name in the element list
	auto it3 = std::find(std::begin(elem_list_), std::end(elem_list_), elem); 
	if (it3 == std::end(elem_list_)) {  // Sanity check
		TWIST_THROW(L"The dual list is corrupted.");
	}
	it3->name_ = new_name;

	TWIST_CHECK_INVARIANT
	return *it2->second.obj();
}


template<typename Obj, typename Id, typename Chr, bool case_sens, bool owns_objects>
Id DualKeyedList<Obj, Id, Chr, case_sens, owns_objects>::get_largest_id() const
{
	TWIST_CHECK_INVARIANT
	if (id_elem_map_.empty()) {
		TWIST_THROW(L"The list is empty.");
	}
	return id_elem_map_.rbegin()->first;
}


#ifdef _DEBUG
template<typename Obj, typename Id, typename Chr, bool case_sens, bool owns_objects>
void DualKeyedList<Obj, Id, Chr, case_sens, owns_objects>::check_invariant() const noexcept
{
	assert(elem_list_.size() == id_elem_map_.size());
	assert(elem_list_.size() == name_elem_map_.size());
}
#endif  

}  


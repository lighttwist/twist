///  @file  iterators.hpp 
///  Extensions to the STL: new iterators and related functions

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_ITERATORS_HPP
#define TWIST_ITERATORS_HPP

#include <iterator>

#include "meta_misc_traits.hpp"

namespace twist {

/// This iterator wraps an STL-style bidirectional (or better) iterator which addresses an  pair  element.
/// This iterator exposes the first member of the pair directly (and not the second member). 
///
/// @tparam  Iter  The wrapped iterator type
///
template<typename Iter>
class IteratorFirst {
public:
	using iterator_category = std::bidirectional_iterator_tag;
	using difference_type = typename std::iterator_traits<Iter>::difference_type;
	using value_type = typename std::iterator_traits<Iter>::value_type::first_type;
	using pointer = value_type*;
	using reference = value_type&;	

	IteratorFirst(Iter iter) : iter_(iter) {
	}
	reference operator*() const {
		return iter_->first; 
	} 
	pointer operator->() const  { 
		return &(iter_->first); 
	} 
	IteratorFirst& operator++() {
		++iter_; 
		return *this;	
	}
 	IteratorFirst operator++(int) {
		IteratorFirst temp(*this);
		++(*this);
		return temp;	
	}
	IteratorFirst& operator--() {
		--iter_;
		return *this;
	}
	IteratorFirst operator--(int) {
		IteratorFirst temp(*this);
		--(*this);
		return temp;
	}
	bool operator==(const IteratorFirst& rhs) const { 
		return iter_ == rhs.iter_; 
	}
	bool operator!=(const IteratorFirst& rhs) const { 
		return iter_ != rhs.iter_; 
	}
private:
	Iter iter_;
};

/// Convenience function for creating a IteratorFirst<> instance.
///
/// @tparam  Iter  Type of the iterator which the  IteratorFirst<>  object is to be based on
/// @param[in] it  The iterator which the  IteratorFirst<>  object is to be based on
/// @return  The  IteratorFirst<>  object 
///
template<typename Iter> 
IteratorFirst<Iter> iter_first(Iter it) 
{
	return IteratorFirst<Iter>(it);
}

/// Create a  IteratorFirst<>  object based on the iterator returned by a container's  begin()  method
///
/// @tparam  Cont   Container type
/// @param[in] cont  The container
/// @return  The iterator
///
template<class Cont> 
IteratorFirst<typename Cont::iterator> begin_first(Cont& cont)
{
	return iter_first(cont.begin());
} 

/// Create a  IteratorFirst<>  object based on the iterator returned by a container's  end()  method
///
/// @tparam  Cont   Container type
/// @param[in] cont  The container
/// @return  The iterator
///
template<class Cont> 
IteratorFirst<typename Cont::iterator> end_first(Cont& cont)
{
	return iter_first(cont.end());
} 

/// This iterator wraps an STL-style bidirectional (or better) iterator which addresses an std::pair<> element.
/// This iterator exposes the first member of the pair directly, as a const value. 
///
/// @tparam  Iter  The wrapped iterator type
///
template<typename Iter>
class CiteratorFirst {
public:
	using iterator_category = std::bidirectional_iterator_tag;
	using difference_type = typename std::iterator_traits<Iter>::difference_type;
	using value_type = typename std::iterator_traits<Iter>::value_type::first_type;
	using pointer = const value_type*;
	using reference = const value_type&;	

	CiteratorFirst(Iter iter) : iter_(iter) {
	}
	reference operator*() const {
		return iter_->first; 
	} 
	pointer operator->() const  { 
		return &(iter_->first); 
	} 
	CiteratorFirst& operator++() {
		++iter_; 
		return *this;	
	}
 	CiteratorFirst operator++(int) {
		CiteratorFirst temp(*this);
		++(*this);
		return temp;	
	}
	CiteratorFirst& operator--() {
		--iter_;
		return *this;
	}
	CiteratorFirst operator--(int) {
		CiteratorFirst temp(*this);
		--(*this);
		return temp;
	}
	bool operator==(const CiteratorFirst& rhs) const { 
		return iter_ == rhs.iter_; 
	}
	bool operator!=(const CiteratorFirst& rhs) const { 
		return iter_ != rhs.iter_; 
	}
private:
	Iter  iter_;
};

/// Create a CiteratorFirst<> object based on the iterator returned by a container's  cbegin()  method
///
/// @tparam  Cont   Container type
/// @param[in] cont  The container
/// @return  The iterator
///
template<class Cont> 
CiteratorFirst<typename Cont::const_iterator> cbegin_first(const Cont& cont)
{
	return CiteratorFirst{ cont.cbegin() };
} 

/// Create a CiteratorFirst<> object based on the iterator returned by a container's  cend()  method
///
/// @tparam  Cont   Container type
/// @param[in] cont  The container
/// @return  The iterator
///
template<class Cont> 
CiteratorFirst<typename Cont::const_iterator> cend_first(const Cont& cont)
{
	return CiteratorFirst{ cont.cend() };
} 

///  This iterator wraps an STL-style bidirectional (or better) iterator which addresses a std::pair<> 
///    element.
///  This iterator exposes the second member of the pair directly (and not the first member).
///
/// @tparam  Iter  The wrapped iterator type
///
template<typename Iter>
class IteratorSecond {
public:
	using iterator_category = std::bidirectional_iterator_tag;
	using difference_type = typename std::iterator_traits<Iter>::difference_type;
	using value_type = typename std::iterator_traits<Iter>::value_type::second_type;
	using pointer = value_type*;
	using reference = value_type&;

	IteratorSecond(Iter iter) : iter_(iter) {
	}
	reference operator*() const {
		return iter_->second; 
	} 
	pointer operator->() const  { 
		return &(iter_->second); 
	} 
	IteratorSecond& operator++() {
		++iter_; 
		return *this;	
	}
 	IteratorSecond operator++(int) {
		IteratorSecond temp(*this);
		++(*this);
		return temp;	
	}
	IteratorSecond& operator--() {
		--iter_;
		return *this;
	}
	IteratorSecond operator--(int) {
		IteratorSecond temp(*this);
		--(*this);
		return temp;
	}
	bool operator==(const IteratorSecond& rhs) const { 
		return iter_ == rhs.iter_; 
	}
	bool operator!=(const IteratorSecond& rhs) const { 
		return iter_ != rhs.iter_; 
	}
private:
	Iter  iter_;
};

/// Convenience function for creating a  IteratorSecond<>  instance.
///
/// @tparam  Iter  Type of the iterator which the  IteratorSecond<>  object is to be based on
/// @param[in] it  The iterator which the  IteratorSecond<>  object is to be based on
/// @return  The  IteratorSecond<>  object 
///
template<typename Iter> 
IteratorSecond<Iter> iter_second(Iter it) 
{
	return IteratorSecond<Iter>(it);
}

/// Create a  IteratorSecond<>  object based on the iterator returned by a container's  begin()  method
///
/// @tparam  Cont   Container type
/// @param[in] cont  The container
/// @return  The iterator
///
template<class Cont> 
IteratorSecond<typename Cont::iterator> begin_second(Cont& cont)
{
	return iter_second(cont.begin());
} 

/// Create a  IteratorSecond<>  object based on the iterator returned by a container's  end()  method
///
/// @tparam  Cont   Container type
/// @param[in] cont  The container
/// @return  The iterator
///
template<class Cont> 
IteratorSecond<typename Cont::iterator> end_second(Cont& cont)
{
	return iter_second(cont.end());
} 

/// @cond  Doxygen exclusion section
template<class Iter, class = std::void_t<>>
class CiteratorSecond {
	// Disable the primary template to emit a clean compile error when the template parameter type 
	// requirements are not met. See the CiteratorSecond specialisation for the "real" class definition.
	static_assert(IsAnyInputIterator<Iter>::value, 
			"The template argument does not satisfy the Iterator concept requirements.");
};
/// @endcond  

/// This iterator wraps an STL-style bidirectional (or better) iterator which addresses a std::pair<> element.
/// This iterator exposes the second member of the pair directly (and not the first member), as a const value.
///
/// @tparam  Iter  The wrapped iterator type; must satisfy the InputIterator requiremenents (as tested by the 
///				IsAnyInputIterator metafunction)
///
template<class Iter>
class CiteratorSecond<Iter, std::void_t<EnableIfAnyInputIterator<Iter>>> {
public:
	using value_type = typename std::iterator_traits<Iter>::value_type::second_type;
	using reference = const value_type&;
	using pointer = const value_type*;
	using iterator_category = std::bidirectional_iterator_tag;
	using difference_type = typename std::iterator_traits<Iter>::difference_type;

	CiteratorSecond(Iter iter) 
		: iter_{ iter } 
	{
	}

	reference operator*() const 
	{
		return iter_->second; 
	}
	
	pointer operator->() const  
	{ 
		return &(iter_->second); 
	} 
	
	CiteratorSecond& operator++() 
	{
		++iter_; 
		return *this;	
	}
 	
	CiteratorSecond operator++(int) 
	{
		CiteratorSecond temp(*this);
		++(*this);
		return temp;	
	}
	
	CiteratorSecond& operator--() 
	{
		--iter_;
		return *this;
	}
	
	CiteratorSecond operator--(int) 
	{
		CiteratorSecond temp(*this);
		--(*this);
		return temp;
	}
	
	bool operator==(const CiteratorSecond& rhs) const 
	{ 
		return iter_ == rhs.iter_; 
	}
	
	bool operator!=(const CiteratorSecond& rhs) const 
	{ 
		return iter_ != rhs.iter_; 
	}

private:
	Iter  iter_;
};

/// Create a  CiteratorSecond<>  object based on the iterator returned by a container's  cbegin()  method
///
/// @tparam  Cont   Container type
/// @param[in] cont  The container
/// @return  The iterator
///
template<class Cont> 
CiteratorSecond<typename Cont::const_iterator> cbegin_second(const Cont& cont)
{
	return CiteratorSecond<typename Cont::const_iterator>(cont.cbegin());
} 

/// Create a  CiteratorSecond<>  object based on the iterator returned by a container's  cend()  method
///
/// @tparam  Cont   Container type
/// @param[in] cont  The container
/// @return  The iterator
///
template<class Cont> 
CiteratorSecond<typename Cont::const_iterator> cend_second(const Cont& cont)
{
	return CiteratorSecond<typename Cont::const_iterator>(cont.cend());
} 

/*! Insert iterator for associative containers.
    \tparam Cont  The type of associative container to insert into
 */
template<class Cont>
class AssoInsertIterator {
public:
	using iterator_category = std::output_iterator_tag;
	using value_type = void;
	using difference_type = ptrdiff_t;
	using pointer = void;
	using reference = void;
    using container_type = Cont;

	/*! Constructor.
	    \param[in] cont  The associative container  
	    \param[in] throw_on_failed_insert  Whether the iterator should throw an exception if an insertion in the 
		                                   container fails (if the key already exists in the container)
	 */
	explicit AssoInsertIterator(Cont& container, bool throw_on_failed_insert) 
		: cont_{&container} 
		, throw_on_failed_insert_{throw_on_failed_insert}
	{
	}

	auto operator=(const typename Cont::value_type& rhs) -> AssoInsertIterator&
	{
		if (!throw_on_failed_insert_) {
			cont_->insert(rhs);		
		}
		else {
			if (auto [it, ok] = cont_->insert(rhs); !ok) {
				TWIST_THROW(L"Insertion in associative container failed.");
			}
		}
		return *this;
	}

	auto operator*() -> AssoInsertIterator& { return *this; } // no-op

	auto operator++() -> AssoInsertIterator& { return *this; } // no-op
	
	auto operator++(int) -> AssoInsertIterator { return *this; } // no-op

private:
	gsl::not_null<Cont*> cont_;
	bool throw_on_failed_insert_;
};

/*! Convenience function to create an AssoInsertIterator<> instance
    \tparam Cont  Type of the container in which insertion is to take place
    \param[in] cont  The container
    \param[in] throw_on_failed_insert  Whether the iterator should throw an exception if an insertion in the container 
	                                   fails (if the key already exists in the container)
    \return  The AssoInsertIterator<> object 
 */
template<class Cont>
[[nodiscard]] auto asso_inserter(Cont& cont, bool throw_on_failed_insert = true) -> AssoInsertIterator<Cont>
{
	return AssoInsertIterator<Cont>{cont, throw_on_failed_insert};
}

} 

#endif 


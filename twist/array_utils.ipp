/// @file array_utils.ipp
/// Inline implementation file for "array_utils.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace twist {

template<class T, std::size_t size>
constexpr int is_increasing(T (&arr)[size])
{
	for (auto i = 1u; i < size; ++i) {
		if (arr[i] < arr[i - 1]) {
			return false;
		}				
	}
	return true;
}


template<class T, std::size_t size>
constexpr int is_strictly_increasing(T (&arr)[size])
{
	for (auto i = 1u; i < size; ++i) {
		if (!(arr[i - 1] < arr[i])) {
			return false;
		}				
	}
	return true;
}


template<class T, std::size_t size>
constexpr int binary_search(T (&arr)[size], T elem)
{
	int left_pos = 0;
	int right_pos = static_cast<int>(size - 1);

	while (left_pos <= right_pos) {
		const auto pos = (left_pos + right_pos) / 2;
		if (arr[pos] < elem) {
			left_pos = pos + 1;
		}
		else if (elem < arr[pos]) {
			right_pos = pos - 1;			
		}
		else {
			return pos;
		}
	}
	return -1;
}


template<class D, class... Types>
constexpr twist::detail::ReturnType<D, Types...> make_array(Types&&... t) 
{
	return { std::forward<Types>(t)... };
}


template<class T, std::size_t size>
constexpr std::array<std::remove_cv_t<T>, size> to_array(T (&arr)[size])  
{
    return detail::to_array_impl(arr, std::make_index_sequence<size>{});
}
 
}

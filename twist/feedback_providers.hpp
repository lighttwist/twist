/// @file feedback_providers.hpp
/// "Feedback provider" classes.

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_FEEDBACK__PROVIDERS_HPP
#define TWIST_FEEDBACK__PROVIDERS_HPP

#include <fstream>
#include <iostream>

namespace twist {

//! Abstract base class for feedback providers which provide feedback using textual mesages. 
class MessageFeedbackProv {
public:
	virtual ~MessageFeedbackProv() = 0;

	/*! Set the progress message.
	    \param[in] msg  The message
	    \param[in] try_replace_prev  If the feedback is provided in an environment where a message can replace the 
		                             previous message, then if true is passed in and a previous message exits, this 
									 method will try to replace it
	 */
	auto set_prog_msg(std::wstring_view msg, bool try_replace_prev = false) -> void;

	/*! Set the progress message.
	    \param[in] msg  The message
	    \param[in] try_replace_prev  If the feedback is provided in an environment where a message can replace the 
		                             previous message, then if true is passed in and a previous message exits, this 
									 method will try to replace it
	 */
	auto set_prog_msg(std::string_view msg, bool try_replace_prev = false) -> void;

	//! The number of indentation steps (eg tabs) that the messages currently use.
	[[nodiscard]] auto prog_msg_indent() const -> int;

	/*! Set the indentation that messages will have from here on.
	    For multiline messages, all lines will be indented similarly.
	    \param[in] indent_steps  The number of indentation steps (eg tabs)
	 */
	auto set_prog_msg_indent(int indent_steps) -> void;

	/*! Call when a fatal error has occurred which means that the process whose progress is being reported must 
	    terminate.
	    \param[in] msg  The error message
	 */
	auto set_fatal_error_msg(std::wstring_view msg) -> void;

protected:	
	/*! Constructor.
	    \param[in] indent_str  String to be used as one indentation "unit" for the progress messages
	 */ 
	MessageFeedbackProv(std::wstring indent_str = L"    ");

private:
	/*! Set the progress message.
	    \param[in] msg  The message
	    \param[in] try_replace_prev  If the feedback is provided in an environment where a message can replace the 
		                             previous message, then if true is passed in and a previous message exits, this 
									 method will try to replace it
	 */
	virtual void do_set_prog_msg(std::wstring_view msg, bool try_replace_prev) = 0;

	/*! Call when a fatal error has occurred which means that the process whose progress is being reported must 
	    terminate.
	    \param[in] msg  The error message
	 */
	virtual void do_set_fatal_error_msg(std::wstring_view msg) = 0;

	std::wstring indent_str_;
	int indent_steps_{0};

	TWIST_CHECK_INVARIANT_DECL
};

/// Abstract base class for feedback providers which have a channel to report exceptions being thrown.
class ExceptionFeedbackProv {
public:
	/// Destructor.
	virtual ~ExceptionFeedbackProv() = 0;

	/*! Report that an exception has been thrown and was not caught within the main process whose progress is being 
	    reported, which means that the process has terminated.
	    \param[in] exptr  The exception
	 */
	void report_uncaught_exception(std::exception_ptr exptr);

protected:
	ExceptionFeedbackProv() = default;

private:
	/*! Report that an exception has been thrown and was not caught within the main process whose progress is being 
	    reported, which means that the process has terminated.
	    \param[in] exptr  The exception
	 */
	virtual void do_report_uncaught_exception(std::exception_ptr exptr) = 0;
};

/*! Abstract base class for feedback providers which provide feedback using textual mesages and additionaly have a 
    channel to report exceptions being thrown.
 */
class MessageExceptionFeedbackProv : public virtual MessageFeedbackProv
                                   , public virtual ExceptionFeedbackProv {
protected:
	MessageExceptionFeedbackProv() = default;
};

//! Message feedback provider which outputs the feedback messages to a stream.
template<class Stream> 
class StreamMessageFeedbackProv : public MessageFeedbackProv {
public:
	StreamMessageFeedbackProv(Stream& stream);

private:
	void do_set_prog_msg(std::wstring_view, bool) override; // MessageFeedbackProv override 

	void do_set_fatal_error_msg(std::wstring_view) override; // MessageFeedbackProv override

	static const wchar_t* indent_str;
	static const wchar_t* fatal_err_str;

	Stream& stream_;
	int indent_steps_{0};

	TWIST_CHECK_INVARIANT_DECL
};

//! Feedback provider which outputs progress messages to a text log file. 
class LogFileFeedbackProvider : public MessageFeedbackProv {
public:
	/*! Constructor. 
	    \param[in] log_path  The log file path; if a file with this filename exists, it will be deleted
		\param[in] use_timestamps  If true, each message will be prefixed with a timestamp
	 */
	LogFileFeedbackProvider(fs::path path, bool use_timestamps = true);

private:
	auto do_set_prog_msg(std::wstring_view msg, bool try_replace_prev) -> void override; // MessageFeedbackProv override 

	auto do_set_fatal_error_msg(std::wstring_view msg) -> void override; // MessageFeedbackProv override

	fs::path path_;
	bool use_timestamps_;
	std::wofstream file_stream_;

	TWIST_CHECK_INVARIANT_DECL
};

//! Feedback provider which outputs progress messages via multiple other providers. 
class MultipleFeedbackProvider : public MessageFeedbackProv {
public:
	using Providers = std::vector<std::unique_ptr<MessageFeedbackProv>>;

	/*! Constructor. 
	    \param[in] providers  The other providers
	 */
	MultipleFeedbackProvider(Providers providers);

private:
	auto do_set_prog_msg(std::wstring_view msg, bool try_replace_prev) -> void override; // MessageFeedbackProv override 

	auto do_set_fatal_error_msg(std::wstring_view msg) -> void override; // MessageFeedbackProv override

	Providers providers_;

	TWIST_CHECK_INVARIANT_DECL
};

//! Dummy message feedback provider: it does nothing.
class DummyMessageFeedbackProv : public MessageFeedbackProv {
private:
	auto do_set_prog_msg(std::wstring_view, bool) -> void override; // MessageFeedbackProv override 

	auto do_set_fatal_error_msg(std::wstring_view) -> void override; // MessageFeedbackProv override
};

/*! Class which increments the indentation used by a feedback provider to output progress messages in the constructor,
    and decrements it in the destructor.
 */
class [[nodiscard]] ScopedProgressMessageIndenter {
public:
	/*! Constructor.
        \param[in] feedback_prov  The feedback provider	 
	 */
	explicit ScopedProgressMessageIndenter(MessageFeedbackProv& feedback_prov);

	~ScopedProgressMessageIndenter();

	TWIST_NO_COPY_NO_MOVE(ScopedProgressMessageIndenter)

private:
	MessageFeedbackProv& feedback_prov_;
};

// --- Free functions ---

/*! Increment the indentation used by a feedback provider to output progress messages.
    \param[in] feedback_prov  The feedback provider
    \return The new number or indentation steps
 */
auto inc_prog_msg_indent(MessageFeedbackProv& feedback_prov) -> int;

/*! Decrement the indentation used by a feedback provider to output progress messages.
    \param[in] feedback_prov  The feedback provider
    \return  The new number or indentation steps
 */
auto dec_prog_msg_indent(MessageFeedbackProv& feedback_prov) -> int;

/*! Create a feedback provider instance which outputs the feedback messages to std::wcout.
    \return  The feedback provider
 */
[[nodiscard]] auto make_cout_feedback_prov() -> StreamMessageFeedbackProv<std::wostream>;

} 

#include "twist/feedback_providers.ipp"

#endif 

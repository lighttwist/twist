/// @file RuntimeError.cpp
/// Implementation file for "RuntimeError.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/RuntimeError.hpp"

#include <cstdarg>

namespace twist {

// ---  RuntimeError class ---

RuntimeError::RuntimeError(std::wstring err_msg, std::wstring func_name) 
	: std::runtime_error(string_to_ansi(err_msg.c_str()))
	, err_msg_{move(err_msg)}
	, func_name_{move(func_name)}
{
	TWIST_CHECK_INVARIANT
}

auto RuntimeError::what() const noexcept -> const char*
{
	TWIST_CHECK_INVARIANT
	if (ansi_err_msg_.empty()) {
		ansi_err_msg_ = string_to_ansi(err_msg_);
	}
	return ansi_err_msg_.c_str();
}

auto RuntimeError::error_message() const noexcept -> std::wstring 
{
	TWIST_CHECK_INVARIANT
	return err_msg_;
}

auto RuntimeError::prepend(std::wstring_view text) noexcept -> void
{
	TWIST_CHECK_INVARIANT
	err_msg_ = text + move(err_msg_);
	ansi_err_msg_.clear();
}

auto RuntimeError::append(std::wstring_view text) noexcept -> void
{
	TWIST_CHECK_INVARIANT
	err_msg_ += text;
	ansi_err_msg_.clear();
}

auto RuntimeError::function_name() const noexcept -> std::wstring
{
	TWIST_CHECK_INVARIANT
	return func_name_;
}

#ifdef _DEBUG
auto RuntimeError::check_invariant() const noexcept -> void
{
	assert(!err_msg_.empty());
	assert(!func_name_.empty());
}
#endif

// --- Free functions ---

auto error_message(const std::exception& excep) noexcept -> std::wstring
{
	return ansi_to_string(excep.what());
}

auto function_name(const std::exception& excep) noexcept -> std::wstring
{
	if (const auto* runtime_err = dynamic_cast<const RuntimeError*>(&excep)) {
		return runtime_err->function_name();
	}
	return {};
}

} 
 
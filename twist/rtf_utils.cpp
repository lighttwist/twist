/// @file rtf_utils.hpp.cpp
/// Implementation file for "rtf_utils.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/rtf_utils.hpp"

#include "twist/std_variant_utils.hpp"

#include <ctime>
#include <fstream>
#include <sstream>

namespace twist {

// --- Local functions ---

std::wstring rtfFormatText(const std::wstring& text, const Font& font)
{
	std::wstringstream stream;

	stream << L"{";
	stream << L"\\fs" << font.point_size() * 2 << L" "; // Set text size.
	if (font.weight() == FontWeight::bold) { // Set text weight.
		stream << L"\\b ";
	}
	if (font.style() == FontStyle::italic) { // Set text style.
		stream << L"\\i ";
	}
	
	stream << text << L"}";
	
	return stream.str();
}

/// Convert an integer value to its roman representation. The converted value must between 1 and 3999.
///
/// @param  value  [in] The value.
/// @return  The string. 
///
std::wstring iToRoman(Ssize value)
{
	static Ssize values[13] = { 1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1 };
	static const std::string numerals[13] = 
			{ "M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I" };

	std::wstringstream text;
	Ssize valCopy = value;
	// Loop through each of the values to diminish the number
	for (Ssize i = 0; i < 13; ++i) {
		// If the number being converted is less than the test value, append
		// the corresponding numeral or numeral pair to the resultant string
		while (valCopy >= values[i]) {
			valCopy -= values[i];
			text << numerals[i].c_str();
		}
	}

	return text.str();
}

/// Convert an integer value to letter. The converted value must between 1 and 26.
///
/// @param  value  [in] The value.
/// @return  The string. 
///
std::wstring iToLetter(Ssize value)
{
	std::wstringstream text;
	text << static_cast<char>((value%26) + 'a' - 1);
	return text.str();
}

// --- RtfDocSectionContent class ---

RtfDocSectionContent::RtfDocSectionContent()
{
	TWIST_CHECK_INVARIANT
}


RtfDocSectionContent::~RtfDocSectionContent()
{
	TWIST_CHECK_INVARIANT
}

#ifdef _DEBUG
void RtfDocSectionContent::check_invariant() const noexcept 
{
}
#endif  

// --- RtfDocTable class ---

RtfDocTable::RtfDocTable(Ssize nof_cols, 
                         Ssize defColWidth, 
						 Font defFont, 
						 RtfHAlign table_alignment, 
	                     RtfHAlign cell_alignment) 
	: nof_cols_{nof_cols}
	, m_defaultFont{std::move(defFont)}
	, table_alignment_{table_alignment}
	, cell_alignment_{cell_alignment}

{
	for (Ssize col = 0; col < nof_cols_; ++col) {
		col_widths_.push_back(defColWidth);
	}
	TWIST_CHECK_INVARIANT
}

RtfDocTable::~RtfDocTable()
{
	TWIST_CHECK_INVARIANT
}

std::wstring RtfDocTable::get_source_text() const
{
	TWIST_CHECK_INVARIANT
	return m_tableText + L"{\\pard \\fs8 \\par}\n"; // Set extra space after the table.
}

auto RtfDocTable::nof_columns() -> Ssize
{
	TWIST_CHECK_INVARIANT
	return nof_cols_;
}

auto RtfDocTable::set_column_wdith(Ssize colId, Ssize width) -> void
{
	TWIST_CHECK_INVARIANT
	if (colId < nof_cols_) {
		col_widths_.at(colId) = width;
	}
}

auto RtfDocTable::add_row(const std::vector<std::wstring>& cell_strings, const RtfRowColourInfo& bkg_colour_info) -> void
{
	TWIST_CHECK_INVARIANT
	auto text = std::wstringstream{};

	auto get_cell_colour_idx = std::function<RtfColourTableIndex(Ssize)>{};
	if (holds_monostate(bkg_colour_info)) {
		get_cell_colour_idx = [](auto) { return RtfColourTableIndex::none; };
	}
	else if (holds_alternative<RtfColourTableIndex>(bkg_colour_info)) {
		get_cell_colour_idx = [&bkg_colour_info](auto) { return get<RtfColourTableIndex>(bkg_colour_info); };
	}
	else {
		get_cell_colour_idx = [&bkg_colour_info](auto cell_idx) { 
			const auto& colours = get<RtfColumnColours>(bkg_colour_info); 
			if (auto it = colours.find(cell_idx); it != end(colours)) {
				return it->second;
			} 	
			return RtfColourTableIndex::none;
		};		
	}

	// Start new row
	text << L"{\\trowd " << row_align_string() << L" \\clvertalc\n"; 
		
	auto rightBoundary = Ssize{0};
	for (auto j : IndexRange{nof_cols_}) {
		auto bkg_colour_idx = get_cell_colour_idx(j);
		if (bkg_colour_idx != RtfColourTableIndex::none) {
			text << L"\\clcbpat" << static_cast<Ssize>(bkg_colour_idx) + 1;
		}
		rightBoundary += col_widths_.at(j);
		text << L"\\cellx" << rightBoundary << L"\n";
	}
	
	for (auto j : IndexRange{nof_cols_}) {
		// Add cell content
		text << L"\\pard\\intbl" << cell_align_string() << L" " 
		     << rtfFormatText(cell_strings.at(j), m_defaultFont) << L"\\cell\n"; 
	}
	
	text << L"\\row}\n"; // End row.
	
	m_tableText += text.str();

	++nof_rows_;
}

void RtfDocTable::add_row(const std::vector<std::wstring>& cell_strings, const Font& font)
{
	TWIST_CHECK_INVARIANT
	std::wstringstream text;

	// Start new row
	text << L"{\\trowd " << row_align_string() << L" \\clvertalc\n"; 
	
	Ssize rightBoundary = 0;
	for (Ssize cellLoop = 0; cellLoop < nof_cols_; ++cellLoop) {
		text << L"\\clbrdrt\\brdrs \\clbrdrl\\brdrs \\clbrdrb\\brdrs \\clbrdrr\\brdrs"; // Set single border on the cell
		rightBoundary += col_widths_.at(cellLoop);
		text << L"\\cellx" << rightBoundary << L"\n";
	}
	
	for (Ssize cellLoop = 0; cellLoop < nof_cols_; ++cellLoop) {
		// Add cell content
		text << L"\\pard\\intbl" << cell_align_string() << L" " 
		     << rtfFormatText(cell_strings.at(cellLoop), font) << L"\\cell\n"; 
	}
	
	text << L"\\row}\n"; // End row.
	
	m_tableText += text.str();
	TWIST_CHECK_INVARIANT
}

auto RtfDocTable::row_align_string() const -> std::wstring
{
	TWIST_CHECK_INVARIANT
	return table_alignment_ == RtfHAlign::left ? L"\\trql" : L"\\trqc";
}

auto RtfDocTable::cell_align_string() const -> std::wstring
{
	TWIST_CHECK_INVARIANT
	return cell_alignment_ == RtfHAlign::left ? L"\\ql" : L"\\qc";
}

#ifdef _DEBUG
void RtfDocTable::check_invariant() const noexcept
{
	assert(nof_cols_ > 0);
	assert(RtfHAlign::left <= table_alignment_ && table_alignment_ <= RtfHAlign::centre);
	assert(RtfHAlign::left <= cell_alignment_ && cell_alignment_ <= RtfHAlign::centre);
}
#endif 

// --- RtfDocParagraph class ---

RtfDocParagraph::RtfDocParagraph(const std::wstring& text, const Font& defFont) :
	m_parText(),
	m_defaultFont(defFont)
{
	m_parText = L"{\\pard \\sa90 \\ql \\li270 " + text;
	TWIST_CHECK_INVARIANT
}

void RtfDocParagraph::addText(const std::wstring& text)
{
	TWIST_CHECK_INVARIANT
	m_parText += rtfFormatText(text, m_defaultFont) + L" ";
	TWIST_CHECK_INVARIANT
}

void RtfDocParagraph::addText(const std::wstring& text, const Font& font)
{
	TWIST_CHECK_INVARIANT
	m_parText += rtfFormatText(text, font) + L" ";
	TWIST_CHECK_INVARIANT
}

void RtfDocParagraph::newLine()
{
	TWIST_CHECK_INVARIANT
	m_parText += L"\\line \n ";
	TWIST_CHECK_INVARIANT
}

void RtfDocParagraph::addTabs(Ssize numTabs)
{
	TWIST_CHECK_INVARIANT
	for (Ssize tabLoop = 0; tabLoop < numTabs; ++tabLoop) {
		m_parText += L"\\tab ";
	}
	TWIST_CHECK_INVARIANT
}

void RtfDocParagraph::addListItem(const std::wstring& itemText, Ssize listLevel, const std::wstring& listHeadString)
{
	TWIST_CHECK_INVARIANT
	const long lineIndent = static_cast<long>(listLevel)*720;
	m_parText += L"\\par \\li" + std::to_wstring(lineIndent) + L" \\fi-360 " + listHeadString 
				+ L"\\tab " + itemText;
	TWIST_CHECK_INVARIANT
}

std::wstring RtfDocParagraph::get_source_text() const
{
	TWIST_CHECK_INVARIANT
	return m_parText + L"\\par}\n"; // End paragraph.
}

#ifdef _DEBUG
void RtfDocParagraph::check_invariant() const noexcept
{
}
#endif 

// --- RtfDocSimpleTextBlock ---

RtfDocSimpleTextBlock::RtfDocSimpleTextBlock(std::wstring text, bool start_on_new_line)
	: RtfDocSectionContent{}
{
	if (start_on_new_line) {
		text_ += L"\\line ";
	}
	text_ += move(text);
}

auto RtfDocSimpleTextBlock::get_source_text() const -> std::wstring
{
	return text_;
}

// --- RtfDocListItem class ---

RtfDocListItem::RtfDocListItem(const std::wstring& itemText) 
	: m_listText{itemText}
{
	TWIST_CHECK_INVARIANT
}

RtfDocListItem::RtfDocListItem(const std::wstring& itemText, RtfDocList* childList) 
	: m_listText(itemText)
	, m_childList(childList)
{
	TWIST_CHECK_INVARIANT
}

RtfDocListItem::~RtfDocListItem()
{
	TWIST_CHECK_INVARIANT
	TWIST_DELETE(m_childList);
}

#ifdef _DEBUG
void RtfDocListItem::check_invariant() const noexcept
{
}
#endif

// --- RtfDocList class ---

RtfDocList::RtfDocList(Ssize level, const Font& defFont) 
	: m_listText()
	, m_defaultFont(defFont)
	, m_itemsList()
	, m_level(level)
{
	m_listText = L"{\\pard ";
	TWIST_CHECK_INVARIANT
}

RtfDocList::~RtfDocList()
{
	TWIST_CHECK_INVARIANT
	// Delete all items.
	for (auto iter = m_itemsList.begin(); iter != m_itemsList.end(); ++iter) {
		delete *iter;
	}
}

void RtfDocList::addListItem(const std::wstring& itemText)
{
	TWIST_CHECK_INVARIANT
	RtfDocListItem* item = new RtfDocListItem(itemText);
	m_itemsList.push_back(item);
	TWIST_CHECK_INVARIANT
}


RtfDocList& RtfDocList::addListItemWithSubList(const std::wstring& itemText)
{
	TWIST_CHECK_INVARIANT
	RtfDocList* childList = new RtfDocList(m_level + 1, m_defaultFont);
	RtfDocListItem* item = new RtfDocListItem(itemText, childList);
	m_itemsList.push_back(item);

	TWIST_CHECK_INVARIANT
	return *childList;
}


std::wstring RtfDocList::get_source_text() const
{
	TWIST_CHECK_INVARIANT
	auto text = std::wstring{};

	Ssize lineIndent = 360*(m_level + 1);
	std::vector<RtfDocListItem*>::const_iterator iter = m_itemsList.begin();
	long itemIndex = 0;
	for (; iter < m_itemsList.end(); ++iter) {
		const RtfDocListItem* item = *iter;
		text += L"\\li" + std::to_wstring(lineIndent) + L" \\fi-360 " + getLabel(++itemIndex) 
				    + L"  " + item->m_listText + L"\\par\n";
		if (item->m_childList != nullptr) {
			text += item->m_childList->get_source_text();
		}
		if (m_level == 1) {
			text += L"{\\pard \\fs8 \\par}\n";
		}
	}
	
	text += L"}\n"; // End list.

	TWIST_CHECK_INVARIANT
	return text;
}


std::wstring RtfDocList::getLabel(Ssize index) const
{
	TWIST_CHECK_INVARIANT
	std::wstringstream text;
	if (m_level == 1) {
		text << index << L". ";	// Arabic numerals.
	} 
	else if (m_level == 2) {
		text << iToLetter(index) << L") "; // Letters
	} 
	else {
		text << L"- ";
	}
	
	TWIST_CHECK_INVARIANT
	return text.str();
}

#ifdef _DEBUG
void RtfDocList::check_invariant() const noexcept
{
}
#endif

// --- RtfDocSection class ---

RtfDocSection::RtfDocSection(std::wstring sectionTitle, const Font& defFont, const Font& titleFont) 
	: m_sectionTitle{move(sectionTitle)}
	, m_defaultFont{defFont}
	, m_defaultTitleFont{titleFont}
{
	TWIST_CHECK_INVARIANT
}

RtfDocSection::~RtfDocSection()
{
	TWIST_CHECK_INVARIANT
}

auto RtfDocSection::addParagraph() -> RtfDocParagraph&
{
	TWIST_CHECK_INVARIANT
	return addParagraph(L"");
}

auto RtfDocSection::addParagraph(const std::wstring& text) -> RtfDocParagraph&
{
	TWIST_CHECK_INVARIANT
	auto par = std::unique_ptr<RtfDocParagraph>{new RtfDocParagraph{text, m_defaultFont}};
	auto& par_ref = *par;
	m_contentItemsList.push_back(move(par));
	return par_ref;
}

auto RtfDocSection::add_simple_text_block(std::wstring text, bool start_on_new_line) -> RtfDocSimpleTextBlock&
{
	TWIST_CHECK_INVARIANT
	auto block = std::unique_ptr<RtfDocSimpleTextBlock>{new RtfDocSimpleTextBlock{move(text), start_on_new_line}};
	auto& block_ref = *block;
	m_contentItemsList.push_back(move(block));
	return block_ref;
}

auto RtfDocSection::addList() -> RtfDocList&
{
	TWIST_CHECK_INVARIANT
	auto list = std::unique_ptr<RtfDocList>{new RtfDocList{1/*level*/, m_defaultFont}};
	auto& list_ref = *list;
	m_contentItemsList.push_back(move(list));
	return list_ref;
}

auto RtfDocSection::addTable(Ssize numCols, Ssize defColWidth, RtfHAlign table_alignment, RtfHAlign cell_alignment)
      -> RtfDocTable&
{
	TWIST_CHECK_INVARIANT
	auto table = std::unique_ptr<RtfDocTable>{new RtfDocTable{numCols, defColWidth, m_defaultFont, table_alignment, 
	                                                          cell_alignment}};
	auto& table_ref = *table;
	m_contentItemsList.push_back(move(table));
	return table_ref;
}

auto RtfDocSection::get_source_text() const -> std::wstring
{
	TWIST_CHECK_INVARIANT
	auto text = std::wstring{};
	for (auto& item : m_contentItemsList) {
		text += item->get_source_text();
	}
	return text;
}

#ifdef _DEBUG
void RtfDocSection::check_invariant() const noexcept
{
}
#endif 

// --- RtfDoc class ---

RtfDoc::RtfDoc(std::wstring doc_title, 
               Font default_font, 
			   Font default_title_font, 
               std::vector<Colour> colour_table) 
	: m_docTitle{move(doc_title)}
	, m_defaultFont{std::move(default_font)}
	, m_defaultTitleFont{std::move(default_title_font)}
	, colour_table_{move(colour_table)}
{
	TWIST_CHECK_INVARIANT
}

RtfDoc::~RtfDoc()
{
	TWIST_CHECK_INVARIANT
}

RtfDocSection& RtfDoc::addSection(std::wstring sectionTitle)
{
	TWIST_CHECK_INVARIANT
	auto section = std::unique_ptr<RtfDocSection>{new RtfDocSection{move(sectionTitle), 
	                                                                m_defaultFont, 
																	m_defaultTitleFont}};
	auto& section_ref = *section;
	m_sectList.push_back(move(section));
	return section_ref;
}

auto RtfDoc::clear() -> void
{
	TWIST_CHECK_INVARIANT
	m_sectList.clear();
	m_docText.clear();
}

auto RtfDoc::get_source_text() const -> std::wstring
{
	TWIST_CHECK_INVARIANT
	auto text = std::wstringstream{};

	text << L"{\\rtf1\\ansi\\deff0\n"; // General info.
	text << L"{\\info{\\title " << m_docTitle << L"}{\\author n/a}{\\company n/a}}\n";
	text << L"{\\fonttbl {\\f0 " << m_defaultFont.family() << L"}}\n";
	
	if (!colour_table_.empty()) {
		text << LR"({\colortbl;)";
		for (auto colour : colour_table_) {
			const auto [r, g, b] = colour.rgb();
			text << LR"(\red)" << r << LR"(\green)" << g << LR"(\blue)" << b << L";";			
		}
		text << L"}";
	}
	
	auto buffer = std::array<char, 32>{};
	auto now = time(0);
	auto localtm = tm{};
	localtime_s(&localtm, &now);	
	asctime_s(buffer.data(), buffer.size(), &localtm);
	
	text << L"{\\header \\pard\\ql\\f0\\fs14 File created: " << ansi_to_string(buffer.data()) << L"}\n"; // Header
	text << L"{\\footer \\pard\\qr\\f0\\fs14 Page \\chpgn}\n"; // Footer
	text << L"\\fs" << m_defaultFont.point_size() * 2 << L"\n";
	if (!is_whitespace(m_docTitle)) {
		text << L"{\\pard \\sa90 \\qc " << rtfFormatText(m_docTitle, m_defaultTitleFont) << L" \\par}\n\n"; // Report title
	}

	auto secNum = 1;
	for (const auto& section : m_sectList) {	
		// Start section.
		const auto sect_title = section->m_sectionTitle;
		if (!is_whitespace(sect_title)) {
			const auto sect_title_rtf = rtfFormatText(iToRoman(secNum++) + L".\\tab " + sect_title, 
			                                          section->m_defaultTitleFont);
			text << L"{\\pard \\sb180 \\sa90 \\ql " + sect_title_rtf + L"\\par}\n\n";
		}
		
		// Add content
		text << section->get_source_text();

		// End section; don't break page after section and end this section
		text << L"\\sbknone\\sect\n\n"; 
	}
	
	text << L"}\n";
	
	return text.str();
}

auto RtfDoc::save_document(const fs::path& path) const -> void
{
	TWIST_CHECK_INVARIANT	
	auto outFileStream = std::wofstream{path.c_str(), std::ios_base::out | std::ios_base::trunc};
	if (outFileStream.bad()) {
		TWIST_THROW(L"Error saving RTF document");	
	}
	outFileStream << get_source_text() << std::endl;
}

#ifdef _DEBUG
void RtfDoc::check_invariant() const noexcept
{
	{
		auto colour_table_clone = std::vector<Colour>{colour_table_};
		assert(!has_duplicates(colour_table_clone));
	}
}
#endif 

// --- RtfFeedbackProv class ---

RtfFeedbackProv::RtfFeedbackProv(RtfDoc doc)
	: doc_{std::move(doc)}
	, section_{&doc_.addSection(L"")}
{
	TWIST_CHECK_INVARIANT
}

RtfFeedbackProv::~RtfFeedbackProv()
{
	TWIST_CHECK_INVARIANT
}

auto RtfFeedbackProv::section() -> RtfDocSection&
{
	TWIST_CHECK_INVARIANT
	return *section_;
}

auto RtfFeedbackProv::rtf_doc() -> RtfDoc&
{
	TWIST_CHECK_INVARIANT
	return doc_;
}

auto RtfFeedbackProv::update() -> void
{
	TWIST_CHECK_INVARIANT
	do_update(doc_.get_source_text().c_str());
}

auto RtfFeedbackProv::clear() -> void
{
	TWIST_CHECK_INVARIANT
	section_ = nullptr;
	doc_.clear();
	section_ = &doc_.addSection(L"");
	update();
}

#ifdef _DEBUG
auto RtfFeedbackProv::check_invariant() const noexcept -> void
{
	assert(section_);
}
#endif

// --- Free functions ---

auto rtfFormatBookmark(const std::wstring& bookName, const std::wstring& bookCaption, const Font& font) 
      -> std::wstring
{
	std::wstringstream stream;
	stream << L"{\\*\\bkmkstart " + bookName + L"}"; // Start bookmark.
	stream << rtfFormatText(bookCaption, font); // Write bookmark caption.
	stream << L"{\\*\\bkmkend " + bookName + L"}"; // End bookmark.
	
	return stream.str();
}

auto rtfFormatReference(const std::wstring& bookName, const std::wstring& linkCaption, const Font& font) 
      -> std::wstring
{
	std::wstringstream stream;
	stream << L"{\\field{\\*\\fldinst {HYPERLINK  \\\\l " + bookName + L"}}{\\fldrslt{\\ul\\cf1 "; // Write bookmark name.
	stream << rtfFormatText(linkCaption, font) + L"}}}"; // Write link caption.
	
	return stream.str();
}

auto add_simple_text_line(RtfFeedbackProv& feedback_prov, std::wstring text, bool update, bool start_on_new_line) 
      -> void
{
	feedback_prov.section().add_simple_text_block(move(text), start_on_new_line);
	if (update) {
		feedback_prov.update();
	}
}

auto rtf_bold(std::wstring str) -> std::wstring
{
	return L"\\b " + str + L"\\b0";
}

auto rtf_italic(std::wstring str) -> std::wstring
{
	return L"\\i " + str + L"\\i0";
}

auto set_column_inch_widths(RtfDocTable& table, const std::vector<double>& widths) -> void
{
	const auto nof_cols = table.nof_columns();
	if (ssize(widths) != nof_cols) {
		TWIST_THRO2(L"The number of width values passed in {} does not match the number of table columns {}.",
		            ssize(widths), nof_cols);
	}
	for (auto col : IndexRange{nof_cols}) {
		table.set_column_wdith(col, static_cast<Ssize>(twips_per_inch * widths[col]));
	}
}

}

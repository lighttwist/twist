/// @file async_utils.ipp
/// Inline implementation file for "async_utils.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/IndexRange.hpp"
#include "twist/std_vector_utils.hpp"
#include "twist/math/numeric_utils.hpp"

namespace twist {

namespace detail {

template<class T>
using NoRef = std::remove_reference_t<T>;

}

// --- Worker class template ---

template<class Result>
Worker<Result>::Worker(WorkerDtorAction dtor_action, ExceptionFeedbackProv& feedback_prov) 
	: dtor_action_{dtor_action} 
	, feedback_prov_{&feedback_prov}
{
	TWIST_CHECK_INVARIANT
}

template<class Result>
Worker<Result>::Worker(WorkerDtorAction dtor_action) 
	: dtor_action_{dtor_action} 
{
	TWIST_CHECK_INVARIANT
}

template<class Result>
Worker<Result>::~Worker()
{
	TWIST_CHECK_INVARIANT_NO_SCOPE
	if (thread_ && thread_->joinable()) { 
		if (dtor_action_ == WorkerDtorAction::join) {
			thread_->join();
		} 
		else {
			thread_->detach();
		}
	}
}

template<class Result> 
template<class Fn, class... Args> 
auto Worker<Result>::run(Fn&& func, Args&&... args) -> void
{
	TWIST_CHECK_INVARIANT
	using twist::detail::NoRef;

	static_assert(std::is_same_v<Result, std::invoke_result_t<Fn, Args...>>, 
			"The return type of the callable object must match class template parameter Result");

	thrown_exception_ = {};

	// If a previous thread was fired up by this object, wait for it/clean it up 
	if (thread_ && thread_->joinable()) {
		thread_->join();
	}

	auto try_func = [this](NoRef<Fn>&& function, NoRef<Args>&&... arguments) {
		try {
			return std::invoke(std::forward<Fn>(function), std::forward<Args>(arguments)...);
		}
		catch (...) {
			if (feedback_prov_) {
				feedback_prov_->report_uncaught_exception(std::current_exception());
			}
			thrown_exception_ = std::current_exception();
			throw;		
		}
	};

	std::packaged_task<Result(NoRef<Fn>&&, NoRef<Args>&&...)> task{ try_func };
	
	future_ = std::make_unique<std::future<Result>>(task.get_future());

	thread_ = std::make_unique<std::thread>(
			move(task), 
			std::forward<NoRef<Fn>>(func), 
			std::forward<NoRef<Args>>(args)...);
}

template<class Result>
auto Worker<Result>::working() const -> bool
{
	TWIST_CHECK_INVARIANT
	return thread_ && thread_->joinable() && !thrown_exception_;  
}

template<class Result>
auto Worker<Result>::get_result() -> Result 
{
	TWIST_CHECK_INVARIANT
	if (!future_ || !future_->valid()) {
		TWIST_THROW(L"Nothing to get.");
	}

	scope (exit) {
		thread_->detach();
		thread_.reset();
		future_.reset();	
	};

	return future_->get();
}

template<class Result> 
template<class Rep, class Period>
auto Worker<Result>::wait_for_result(const std::chrono::duration<Rep, Period>& timeout_duration) -> std::future_status 
{
	TWIST_CHECK_INVARIANT
	if (!future_ || !future_->valid()) {
		TWIST_THROW(L"Nothing to get.");
	}
	return future_->wait_for(timeout_duration);
}

template<class Result>
auto Worker<Result>::thrown_exception() const -> std::exception_ptr 
{
	TWIST_CHECK_INVARIANT
	return thrown_exception_;
}

template<class Result>
auto Worker<Result>::native_terminate() -> void
{
	TWIST_CHECK_INVARIANT
	if (!working()) {
		TWIST_THROW(L"There is no worker thread running.");
	}
	native_terminate_thread(*thread_);
	thread_.reset();
	future_.reset();
	was_native_terminated_ = true;
}

template<class Result>
auto Worker<Result>::was_native_terminated() const -> bool
{
	TWIST_CHECK_INVARIANT	
	return was_native_terminated_;
}

#ifdef _DEBUG
template<class Result>
auto Worker<Result>::check_invariant() const noexcept -> void
{
	assert(dtor_action_ >= WorkerDtorAction::join && dtor_action_ <= WorkerDtorAction::detach);
	assert((thread_.get() == nullptr) == (future_.get() == nullptr));	
	if (was_native_terminated_) {
		assert(!thread_);		
	}
}
#endif

// --- Free functions ---

template<typename Fn, typename... Args>
auto really_async(Fn&& func, Args&&... args)
{
	return std::async(std::launch::async, std::forward<Fn>(func), std::forward<Args>(args)...);
}

template<std::invocable<Ssize> Action>
auto run_on_threads(Action action, Ssize nof_runs, Ssize nof_threads) -> void
{
	assert(nof_runs > 0);
	assert(nof_threads > 0);

	if (nof_threads >= nof_runs) {
		nof_threads = nof_runs;
	}
	const auto min_nof_runs_on_thread = nof_runs / nof_threads;
	const auto nof_threads_with_extra_run = nof_runs % nof_threads;

	auto threads = reserve_vector<std::jthread>(nof_threads);
	auto run_idx = Ssize{0};
	for (auto thread_idx : IndexRange{nof_threads}) {
		const auto nof_runs_on_cur_thread = thread_idx < nof_threads_with_extra_run ? min_nof_runs_on_thread + 1
												                                    : min_nof_runs_on_thread;		
		for ([[maybe_unused]] auto i : IndexRange{nof_runs_on_cur_thread}) {
			threads.emplace_back(action, run_idx++);
		}
	}

	assert(run_idx == nof_runs);
}

}

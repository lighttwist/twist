///  @file  MetaConstvalSequence.hpp
///  MetaConstvalSequence struct definition

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_META_CONSTVAL_SEQUENCE_HPP
#define TWIST_META_CONSTVAL_SEQUENCE_HPP

#include <utility>

namespace twist {

/// A compile-time sequence of compile-time constant values of the same type.
/// One way to use this class is as a generalisation of std::integer_sequence to also work types that can be 
/// cast to fundamental integer types.
///
/// @tparam  T  The constant value type
/// @tparam  elems...  The elements in the sequence
///
template<class T, T... elems>
struct MetaConstvalSequence {
	/// The constant value type.
	using Constval = T;

	/// Metafunction which creates a new constant value sequence type obtained by transforming the elements 
	///   of this sequence to the new element type via static_cast.
	/// A compilation error will occur if elements of this sequence cannot be transformed to the new element
	///   type via static_cast.
	///
	/// @tparam  U  The constant value type of the new sequence
	/// @return  The new sequence type
	///
	template<class U> 
	using To = MetaConstvalSequence<U, static_cast<U>(elems)...>;

	/// Metafunction which creates an instantiation of the std::integer_sequence template obtained by 
	///   transforming the elements of this sequence to the integer numeric type of the std::integer_sequence 
	///   elements via static_cast.
	/// A compilation error will occur if elements of this sequence cannot be transformed to the integer 
	///   numeric type via static_cast.
	/// Devnote: Doesn't work in the VS2017 compiler (VS version 15.9.16) if the elements of this sequence are 
	///   scoped enums: which must be a bug in the compiler.
	///   
	/// @tparam  U  The integer numeric type of the std::integer_sequence elements
	/// @return  The new sequence type
	///
	template<class U> 
	using ToIntegerSequence = std::integer_sequence<U, static_cast<U>(elems)...>;  

	/// Metafunction which creates an instantiation of the std::integer_sequence template with elements of 
	/// type std::size_t. Needed because of the compiler bug described for metafunction ToIntegerSequence.
	using ToIntegerSequenceOfSizeT = 
			std::integer_sequence<std::size_t, static_cast<std::size_t>(elems)...>;

	/// The number of elements in the sequence.
	static constexpr std::size_t size() 
	{ 
		return sizeof...(elems); 
	} 
};

/// Get the element as a specific position in a compile-time sequence of compile-time constant values.
///
/// @tparam  pos  The position; a compile error occurs if it is out of bounds
/// @tparam  T  The sequence element type
/// @tparam  elems...  The elements in the sequence
/// @param[in] seq  The sequence
/// @return  The element
///
template<std::size_t pos, class T, T... elems>
constexpr T get_element(MetaConstvalSequence<T, elems...> seq);

/// Get the position of a specific element in a compile-time sequence of compile-time constant values.
/// The search runs in linear time.
///
/// @tparam  T  The sequence element type
/// @tparam  elems...  The elements in the sequence
/// @param[in] seq  The sequence
/// @param[in] elem  The element to search for  
/// @return  The position of the first occurrence of the element within the sequence; or -1 if no match 
///				is found
///
template<class T, T... elems>
constexpr T get_pos(MetaConstvalSequence<T, elems...> seq, T elem);

/// Find out (at compile-time) whether the elements in a compile-time sequence of compile-time constant values 
/// are sorted in increasing order, ie no element compares as "smaller" than the element before it. 
///
/// @tparam  T  The sequence element type
/// @tparam  elems...  The elements in the sequence
/// @param[in] seq  The sequence
/// @return  true if the sequence is sorted in increasing order
///
template<class T, T... elems>
constexpr bool is_increasing(MetaConstvalSequence<T, elems...> seq);

/// Find out (at compile-time) whether the elements in a compile-time sequence of compile-time constant values 
/// are sorted in strictly increasing order, ie no element compares as "not smaller" that the element 
/// before it. 
///
/// @tparam  T  The sequence element type
/// @tparam  elems...  The elements in the sequence
/// @param[in] seq  The sequence
/// @return  true if the sequence is sorted in strictly increasing order
///
template<class T, T... elems>
constexpr bool is_strictly_increasing(MetaConstvalSequence<T, elems...> seq);

/// Find (at compile-time) an element in a sorted compile-time sequence of compile-time constant values, using 
/// a binary search. The average performance is O(log n).
///
/// @tparam  T  The sequence element type
/// @tparam  elems...  The elements in the sequence
/// @param[in] seq  The sequence
/// @param[in] elem  The element to search for  
/// @return  The position of the occurrence of the matching element (or one of the matching elements if there 
///				are multiple, contiguous matching elements) within the sequence; or -1 if no match is found
///
template<class T, T... elems>
constexpr int binary_search(MetaConstvalSequence<T, elems...> seq, T elem);

} 

#include "MetaConstvalSequence.ipp"

#endif 

/// @file HiResClock.hpp
/// HiResClock class

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_HI_RES_CLOCK_HPP
#define TWIST_HI_RES_CLOCK_HPP

#include <fstream>

namespace twist {

/*! High resolution clock; that is the clock with the shortest tick period. It may be the same as the standard 
    library's "system clock" or "steady clock". 
 */
class HiResClock {
public:
	using Milliseconds = std::chrono::milliseconds::rep;

	HiResClock();
	
	HiResClock(HiResClock&& src) noexcept;

	HiResClock(const HiResClock&) = delete;  

	HiResClock& operator=(const HiResClock&) = delete;  

	HiResClock& operator=(HiResClock&&) = delete;
	
	//! Start the clock. This method can only be called if it has never been called before, or after a call to stop().
	auto start() -> void;

	/*! Stop the clock. This method can only be called after a call to start().
	    \return  The period elapsed since the clock was started (milliseconds)
	 */
    auto stop() -> Milliseconds;

	/*! Get the period elapsed when the clock was last used, in milliseconds. This method can only be called after a 
        call to stop() and before a subsequent call to start().
	    \return  The period elapsed when the clock was last used (milliseconds)
	 */
    [[nodiscard]] auto milliseconds() const -> Milliseconds;

private:
	using TimePt = std::chrono::time_point<std::chrono::high_resolution_clock>;

	[[nodiscard]] auto get_milliseconds() const -> Milliseconds; // no checks

    [[nodiscard]] static auto is_zero(const TimePt& time_pt) -> bool;

	TimePt start_time_;
	TimePt stop_time_;

	TWIST_CHECK_INVARIANT_DECL
};

// --- Free functions ---

//! Create a high resolution clock, start and return it.
[[nodiscard]] auto start_hi_res_clock() -> HiResClock;

//! Stop the high resolution clock \p clock and return the period elapsed since the clock was started in seconds. 
[[nodiscard]] auto stop_and_get_seconds(HiResClock& clock) -> double;

} 

#endif 

/// @file HiResClock.hpp
/// Implementation file for "HiResClock.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/HiResClock.hpp"

#include <chrono>

namespace chrono = std::chrono;

namespace twist {

HiResClock::HiResClock() 
	: start_time_()
	, stop_time_()
{
	TWIST_CHECK_INVARIANT
}

HiResClock::HiResClock(HiResClock&& src) noexcept
	: start_time_(src.start_time_)
	, stop_time_(src.stop_time_)
{
	src.start_time_ = TimePt{};
	src.stop_time_ = TimePt{};
	TWIST_CHECK_INVARIANT
}

auto HiResClock::start() -> void
{
	TWIST_CHECK_INVARIANT
	if (!is_zero(start_time_) && is_zero(stop_time_)) TWIST_THROW(L"The clock is still ticking.");

	stop_time_ = TimePt{};
	start_time_ = chrono::high_resolution_clock::now();
}

auto HiResClock::stop() -> Milliseconds
{
	TWIST_CHECK_INVARIANT
	if (is_zero(start_time_)) TWIST_THROW(L"The clock has not been started.");
	if (!is_zero(stop_time_)) TWIST_THROW(L"The clock has already been stopped.");

	stop_time_ = chrono::high_resolution_clock::now();
	return get_milliseconds();
}

auto HiResClock::milliseconds() const -> Milliseconds
{
	TWIST_CHECK_INVARIANT
	if (is_zero(start_time_)) {
        TWIST_THRO2(L"The clock has not been started.");
    }
	if (is_zero(stop_time_)) {
        TWIST_THRO2(L"The clock is still ticking.");
    }
	return get_milliseconds();
}

auto HiResClock::get_milliseconds() const -> Milliseconds
{
	return chrono::duration_cast<chrono::milliseconds>(stop_time_ - start_time_).count();
}

auto HiResClock::is_zero(const TimePt& time_pt) -> bool
{
	return time_pt.time_since_epoch().count() == 0;
}

#ifdef _DEBUG
void HiResClock::check_invariant() const noexcept
{
	assert(is_zero(stop_time_) || stop_time_ > start_time_);
}
#endif 

// --- Free functions ---

auto start_hi_res_clock() -> HiResClock
{
	auto clock = HiResClock{};
	clock.start();
	return clock;
}

auto stop_and_get_seconds(HiResClock& clock) -> double
{
    return clock.stop() / 1000.0;
}

} 

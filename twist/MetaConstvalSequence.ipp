///  @file  MetaConstvalSequence.ipp
///  Inline implementation file for "MetaConstvalSequence.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "array_utils.hpp"

namespace twist {

template<std::size_t pos, class T, T... elems>
constexpr T get_element(MetaConstvalSequence<T, elems...>)
{
	constexpr T arr[] = { elems... };
	return arr[pos];	
}


template<class T, T... elems>
constexpr T get_pos(MetaConstvalSequence<T, elems...> seq, T elem)
{
	constexpr T arr[] = { elems... };
	return get_pos(arr, elem);	
}


template<class T, T... elems>
constexpr bool is_increasing(MetaConstvalSequence<T, elems...>)
{
	constexpr T arr[] = { elems... };
	return is_increasing(arr);	
}


template<class T, T... elems>
constexpr bool is_strictly_increasing(MetaConstvalSequence<T, elems...>)
{
	constexpr T arr[] = { elems... };
	return is_strictly_increasing(arr);	
}


template<class T, T... elems>
constexpr int binary_search(MetaConstvalSequence<T, elems...>, T elem)
{
	constexpr T arr[] = { elems... };
	return binary_search<const T>(arr, elem);	
}

} 


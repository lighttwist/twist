///  @file  normal_distr.cpp
///  Implementation file for "normal_distr.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "normal_distr.hpp"

#include <cmath>

#include "defs.hpp"
#include "numeric_utils.hpp"

namespace twist::math {

static const double  max_exp_arg = log(double_max);  // Biggest possible argument for exponent 

//
//  Local functions
//

// Distribution function for the standard normal.
// See comments for the declaration of  stdnormal_cdf()  for documentation.
//
// @param  x  [in] The argument.
// @return  The image.
//
double phi(double x)
{
	long double s = x, t = 0, b = x, q = x * x, i = 1;
	while (s != t) s = (t = s) + (b *= q / (i += 2));
	return static_cast<double>(.5 + s * exp(-.5 * q - .91893853320467274178L));
}

// Complementary distribution function for the standard normal. That is, cPhi(x) = 1- phi(x)
// See comments for the declaration of  stdnormal_cdf()  for documentation.
//
// @param  x  [in] The argument.
// @return  The image.
//
long double cPhi_R[9]= {
	1.25331413731550025L,  .421369229288054473L,  .236652382913560671L,
	.162377660896867462L,  .123131963257932296L,  .0990285964717319214L,
	.0827662865013691773L, .0710695805388521071L, .0622586659950261958L
};
double cPhi(double x)
{
	const int j = static_cast<int>(.5 * (fabs(x) + 1));
	long double pwr = 1; 
	long double a = cPhi_R[j];
	long double z = 2 * j; 
	long double b = a * z - 1; 
	long double h = fabs(x) - z; 
	long double s = a + h * b; 
	long double t = a; 
	long double q = h * h;

	for (int i = 2; s != t; i += 2) {
		a = (a + z * b) / i; 
		b = (b + z * a) / (i + 1); 
		pwr *= q; 
		s = (t = s) + pwr * (a + h * b);
	}
	
	s = s * exp(-.5 * x * x - .91893853320467274178L);
	
	if (x >= 0) {
		return static_cast<double>(s); 
	}
	return static_cast<double>(1. - s);
}

//
//  Global functions
//

double stdnormal_cdf(double x)
{
	if (x < -8) {
		return double_epsilon;
	}
	else if (x > 8) {
		return 1.0 - double_epsilon;
	}
	if ((x > -3) && (x < 3)) {
		return phi(x);
	}
	else {
		// Switch to the complementary function, which is more accurate in the tails.
		return 1. - cPhi(x);
	}
}

double stdnormal_invcdf(double p)
{
	// Define break-points
	static const double p_low  = 0.02425;
	static const double p_high = 1 - p_low;

	static const double a[6] = {
		-3.969683028665376e+01,
		2.209460984245205e+02,
		-2.759285104469687e+02,
		1.383577518672690e+02,
		-3.066479806614716e+01,
		2.506628277459239e+00 };
	static const double b[5] = {
		-5.447609879822406e+01,
		1.615858368580409e+02,
		-1.556989798598866e+02,
		6.680131188771972e+01,
		-1.328068155288572e+01 };
	static const double c[6] = {
		-7.784894002430293e-03,
		-3.223964580411365e-01,
		-2.400758277161838e+00,
		-2.549732539343734e+00,
		4.374664141464968e+00,
		2.938163982698783e+00 };
	static const double d[4] = {
		7.784695709041462e-03,
		3.224671290700398e-01,
		2.445134137142996e+00,
		3.754408661907416e+00
	};

    if ((p <= 0) || (p >= 1)) {
        TWIST_THRO2(L"Function argument p={:.8f} must be in the range (0, 1)", p);
    }

    if ((0 < p) && (p < p_low)) {
	    // Rational approximation for lower region
        const double q = sqrt(-2 * log(p));
        return (((((c[0] * q + c[1]) * q + c[2]) * q + c[3]) * q + c[4]) * q + c[5]) /
                ((((d[0] * q + d[1]) * q + d[2]) * q + d[3]) * q + 1);
    }    
    
	if ((p_low <= p) && (p <= p_high)) {
		// Rational approximation for central region
        const double q = p - 0.5 ;
        const double r = q * q ;
        return (((((a[0] * r + a[1]) * r + a[2]) * r + a[3]) * r + a[4]) * r + a[5]) * q /
               (((((b[0] * r + b[1]) * r + b[2]) * r + b[3]) * r + b[4]) * r + 1);
    }
    
	if ((p_high < p) && (p < 1)) {
        // Rational approximation for upper region
        const double q = sqrt(-2 * log(1 - p));
        return -(((((c[0] * q + c[1]) * q + c[2]) * q + c[3]) * q + c[4]) * q + c[5]) /
                 ((((d[0] * q + d[1]) * q + d[2]) * q + d[3]) * q + 1);
    }

	return 0;
}

double normal_pdf(double x, double mu, double sigma)
{
	if (sigma <= 0.) {
		TWIST_THROW(L"Parameter sigma must be greater than 0.");
	}

	// Make sure the argument for the exponent is not too high
	double tExpArg = -(pow(x - mu, 2) / (2 * pow(sigma, 2)));
	tExpArg = std::min(tExpArg, max_exp_arg);
	return 1. / (sigma * sqrt(2 * k_pi)) * exp(tExpArg);
}

double normal_invcdf(double p, double mu, double sigma)
{
	if (sigma <= 0.) {
		TWIST_THROW(L"Parameter sigma must be greater than 0.");
	} 
	else if ((p <= 0.) || (p >= 1.)) {
		TWIST_THROW(L"Percentile p must be in interval (0, 1).");
	}
	return stdnormal_invcdf(p) * sigma + mu;
}

double normal_cdf(double x, double mu, double sigma)
{
	if (sigma <= 0.) {
		TWIST_THROW(L"Parameter sigma must be greater than 0.");
	}
	return stdnormal_cdf((x - mu)/sigma);
}

}

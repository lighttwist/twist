///  @file  UnivarDistrSampleInfo.hpp
///  UnivarDistrSampleInfo class template

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_MATH_UNIVAR_DISTR_SAMPLE_INFO_HPP
#define TWIST_MATH_UNIVAR_DISTR_SAMPLE_INFO_HPP

namespace twist::math {

/// This class contains some information about a random variable's sample. So, this class describes marginal 
/// distribution samples for one random variable, never a joint distribution sample. 
///
/// @tparam  Sam  The type of one sample value
///
template<typename Sam>
class UnivarDistrSampleInfo {
public:
	// An ordered set of unique sample values
	typedef std::set<Sam>  SamSet;

	/// Create an instance of this class, describing a sample drawn from a continuous distribution.
	///
	/// @param[in] size  The sample size.
	/// @param[in] minVal  The minimum value in the sample.
	/// @param[in] maxVal  The maximum value in the sample.
	/// @param[in] mean  The sample mean.
	/// @param[in] stddev  The sample standard deviation.
	/// @return  The new class instance.
	///
    static std::unique_ptr<UnivarDistrSampleInfo> create_cont_sample_info(size_t size, Sam minVal, Sam maxVal, 
			double mean, double stddev); 

	/// Create an instance of this class, describing a sample drawn from a discrete distribution.
    ///
    /// @param[in] size  The sample size.
    /// @param[in] minVal  The minimum value in the sample.
    /// @param[in] maxVal  The maximum value in the sample.
	/// @param[in] mean  The sample mean.
	/// @param[in] stddev  The sample standard deviation.
	/// @param[in] distinct_discr_values  List containing all disting discrete values in the sample.
	/// @return The new class instance.
    ///
    static std::unique_ptr<UnivarDistrSampleInfo> create_discr_sample_info(size_t size, Sam minVal, Sam maxVal, 
			double mean, double stddev, const SamSet& distinct_discr_values);

	/// Copy constructor.
	///
	UnivarDistrSampleInfo(const UnivarDistrSampleInfo& src);

	/// Move constructor.
	///
	UnivarDistrSampleInfo(UnivarDistrSampleInfo&& src);

	/// Destructor.
	///
	virtual ~UnivarDistrSampleInfo();
	
	/// Copy-assignment operator.
	///	
	void operator=(const UnivarDistrSampleInfo& rhs);
	
	/// Move-assignment operator.
	///	
	void operator=(UnivarDistrSampleInfo&& rhs);
	
	/// Get the sample size.
	///
	/// @return  The number of sample values.
	///
    size_t size() const;

    /// Get the minimum value in the sample.
	///
	/// @return  The min value.
	///
    Sam min() const;

    /// Get the maximum value in the sample.
	///
	/// @return  The max value.
	///
    Sam max() const;

    /// Get the sample mean.
	///
	/// @return  The mean.
	///
    double mean() const;

    /// Get the sample standard deviation (this value is not always set).
	///
	/// @return  The standard deviation, or zero if it's not set.
	///
    double stddev() const;

	/// Find out whether the sample was drawn from a discrete distribution.
	///
	/// @return  true if it was, false if it was drawn from a continuous distribution.
	///
	bool is_discrete() const;

	/// Get an ordered set containing all distinct discrete values in the sample. If this object does not 
	/// describe a discrete sample, an exception is thrown.
	///
	/// @return  The set
	///
	const SamSet& distinct_discr_values() const;
    
private:	
	/// Constructor.
	///     
	/// @param[in] numSampleValues  The number of sample values in the sample.
	/// @param[in] minVal  The minimum value in the sample.
	/// @param[in] maxVal  The maximum value in the sample.
	/// @param[in] mean  The sample mean.
	/// @param[in] stddev  The sample standard deviation.
	///
	UnivarDistrSampleInfo(size_t size, Sam min_val, Sam max_val, double mean, double stddev);

    size_t  size_;
    Sam  min_val_;
    Sam  max_val_;
    double  mean_;
    double  stddev_;
    std::unique_ptr<SamSet>  distinct_discr_values_;  // This is empty if the sample is not discrete (ie the 
	                                                  // associated distribution is not discrete)
	TWIST_CHECK_INVARIANT_DECL
};

} 

#include "UnivarDistrSampleInfo.ipp"

#endif 


///  @file  QuantileSet.ipp
///  Inline implementation file for "QuantileSet.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

namespace twist::math {

template<class NumTol>
QuantileSet<NumTol>::QuantileSet(const NumericSet& quant_vals, const NumericSet& quant_probs)
	: quant_vals_(quant_vals) 
	, quant_probs_(quant_probs)
{
	// 1. Check that the two set sizes match
	if (quant_vals_.size() != quant_probs_.size()) {
		TWIST_THROW(
				L"The number of quantile values %d does not match the number of quantile probabilities %d.", 
				quant_vals_.size(), quant_probs_.size());
	}
	// 2. Check that the set contains at least two quantiles
	if (quant_vals_.size() < 2) {
		TWIST_THROW(L"A quantile set must contain at least two quantiles.");
	}		
	// 3. Check that all probabilities are in the [0, 1] interval
	for (const auto p : quant_probs_) { 
		if (p < 0 || p > 1) {
			TWIST_THROW(L"Invalid probability value %f. Probabilities must fall in the interval [0, 1].", p);
		}
	} 		
	TWIST_CHECK_INVARIANT
}


template<class NumTol>
size_t QuantileSet<NumTol>::size() const
{
	TWIST_CHECK_INVARIANT
	return quant_vals_.size();
}


template<class NumTol>
const typename QuantileSet<NumTol>::NumericSet& QuantileSet<NumTol>::vals() const
{
	TWIST_CHECK_INVARIANT
	return quant_vals_;
}


template<class NumTol>
const typename QuantileSet<NumTol>::NumericSet& QuantileSet<NumTol>::probs() const
{
	TWIST_CHECK_INVARIANT
	return quant_probs_;
}
	

template<class NumTol>
typename QuantileSet<NumTol>::T QuantileSet<NumTol>::lowest_val() const
{
	TWIST_CHECK_INVARIANT
	return *quant_vals_.begin();
}
	 

template<class NumTol>
typename QuantileSet<NumTol>::T QuantileSet<NumTol>::highest_val() const 
{
	TWIST_CHECK_INVARIANT
	return *quant_vals_.rbegin();
}	
	

template<class NumTol>
typename QuantileSet<NumTol>::T QuantileSet<NumTol>::lowest_prob() const 
{
	TWIST_CHECK_INVARIANT
	return *quant_probs_.begin();
}
	 

template<class NumTol>
typename QuantileSet<NumTol>::T QuantileSet<NumTol>::highest_prob() const
{
	TWIST_CHECK_INVARIANT
	return *quant_probs_.rbegin();
}	
	

template<class NumTol>
typename QuantileSet<NumTol>::T QuantileSet<NumTol>::get_prob(T quant_val, bool interpolate) const
{
	TWIST_CHECK_INVARIANT
	return get_other(quant_val, true/*is_quant*/, interpolate);
}
	

template<class NumTol>
typename QuantileSet<NumTol>::T QuantileSet<NumTol>::get_val(T quant_prob, bool interpolate) const
{
	TWIST_CHECK_INVARIANT
	return get_other(quant_prob, false/*is_quant*/, interpolate);
}
	

template<class NumTol>
typename QuantileSet<NumTol>::NumericMap QuantileSet<NumTol>::as_map() const
{
	TWIST_CHECK_INVARIANT
	NumericMap map;
	
	auto vit = quant_vals_.begin();
	auto pit = quant_probs_.begin();
	for (; vit != quant_vals_.end(); ++vit, ++pit) {
		map.insert(std::make_pair(*vit, *pit));
	}

	return map;
}


template<class NumTol>
typename QuantileSet<NumTol>::T QuantileSet<NumTol>::get_other(T val, bool is_quant, bool interpolate) const
{
	TWIST_CHECK_INVARIANT
		
	const auto& set = is_quant ? quant_vals_  : quant_probs_;
	const auto& other_set = is_quant ? quant_probs_ : quant_vals_;
	const std::wstring word{is_quant ? L"value"  : L"probability"};
	const std::wstring words{is_quant ? L"values" : L"probabilities"};
		
	auto lo_it = set.lower_bound(val);
	auto hi_it = set.upper_bound(val);
	
	if (hi_it == set.begin()) {
		TWIST_THROW(L"The quantile %s %f falls outside the range of %s [%f, %f].", 
				word.c_str(), val, words.c_str(), *set.begin(), *set.rbegin());
	}
	else if (hi_it == set.end()) {
		if (lo_it == set.end()) {
			TWIST_THROW(L"The quantile %s %f falls outside the range of %s [%f, %f].", 
					word.c_str(), val, words.c_str(), *set.begin(), *set.rbegin());
		}
		else {
			return highest_prob();  // exact match
		}
	} 
		
	// The value falls in the interval [ set[lo_it], set[hi_it] )	

	auto other_lo_it = other_set.begin();
	auto other_hi_it = other_set.begin();
	advance(other_lo_it, distance(set.begin(), lo_it)); 
	advance(other_hi_it, distance(set.begin(), hi_it));

	if (equal_tol(*lo_it, val, NumTol::tolerance)) {
		// exact match
		return *other_lo_it;  
	}
	else if (!interpolate) {
		TWIST_THROW(L"No exact match could be found for quantile %s %f.", word.c_str(), val);
	}
		
	--lo_it;
	--other_lo_it;
		
	/// The value falls in the interval ( set[lo_it], set[hi_it] ) and we can interpolate
	const T ratio = (*other_hi_it - *other_lo_it) / (*hi_it - *lo_it);
	return *other_lo_it + (val - *lo_it) * ratio;
}		


#ifdef _DEBUG
template<class NumTol>
void QuantileSet<NumTol>::check_invariant() const noexcept
{
	assert(quant_vals_.size() == quant_probs_.size());
	assert(quant_vals_.size() >= 2);
}
#endif 

} 

/// @file maps.hpp
/// Map types and related utilities for the "twist::math" library

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_MATH_MAPS_HPP
#define TWIST_MATH_MAPS_HPP

namespace twist::math {

/// Binary predicate: whether the first value is smaller than the second by at least a given tolerance (the 
/// tolerance is passed into the constructor).
template<typename T> 
class BPredLessTolerance {
public:
	BPredLessTolerance(double tol) 
		: tol_{ abs(tol) } 
	{
	}
	
	bool operator()(const T& val1, const T& val2) const 
	{
		return val1 < val2 - tol_;
	}

private:
	double  tol_;
};

/// Map type using a custom precision tolerance (of type double) for comparing keys.
///
/// @tparam  TKey  The map key type
/// @tparam  TVal  The map value type
///
template<typename TKey, typename TVal>
struct MapDoubleTol {
	typedef std::map<TKey, TVal, BPredLessTolerance<double>>  type;
};

/// Convenience function for creating a map object which uses a custom precision tolerance (of type double) 
/// for comparing keys.
///
/// @tparam  TKey  The map key type
/// @tparam  TVal  The map value type
/// @param  tol  [in] The tolerance value
/// @return  The map object
///
template<typename TKey, typename TVal>
typename MapDoubleTol<TKey, TVal>::type make_map_double_tol(double tol)
{
	return typename MapDoubleTol<TKey, TVal>::type{ BPredLessTolerance<double>(tol) };
}

}

#endif

/// @file UnsquareSpaceGrid.ipp
/// Inline implementation file for "UnsquareSpaceGrid.hpp"

//  Copyright (c) 2007-2024, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace twist::math {

// --- Local functions ---

namespace detail {

template<class Coord>
void ensure_valid_row_col(const UnsquareSpaceGrid<Coord>& grid, Ssize row, Ssize col)
{
	if (row < 0 || row >= grid.nof_rows() || col < 0 || col >= grid.nof_columns()) {
		TWIST_THRO2(L"Invalid grid cell position ({}, {}).", row, col);
	}
}

}

// --- UnsquareSpaceGrid class template ---

template<class Coord>
UnsquareSpaceGrid<Coord>::UnsquareSpaceGrid(Coord left, Coord bottom, Coord cell_width, Coord cell_height, 
                                            Ssize nof_rows, Ssize nof_cols) 
	: left_{left}
	, bottom_{bottom}
	, cell_width_{cell_width}
	, cell_height_{cell_height}
	, nof_rows_{nof_rows}
	, nof_cols_{nof_cols}
{
	TWIST_CHECK_INVARIANT
}

template<class Coord>
auto UnsquareSpaceGrid<Coord>::nof_rows() const -> Ssize
{
	TWIST_CHECK_INVARIANT
	return nof_rows_;
}

template<class Coord>
auto UnsquareSpaceGrid<Coord>::nof_columns() const -> Ssize
{
	TWIST_CHECK_INVARIANT
	return nof_cols_;
}

template<class Coord>
auto UnsquareSpaceGrid<Coord>::cell_width() const -> Coord
{
	TWIST_CHECK_INVARIANT
	return cell_width_;
}

template<class Coord>
auto UnsquareSpaceGrid<Coord>::cell_height() const -> Coord
{
	TWIST_CHECK_INVARIANT
	return cell_height_;
}

template<class Coord>
auto UnsquareSpaceGrid<Coord>::left() const -> Coord
{
	TWIST_CHECK_INVARIANT
	return left_;
}

template<class Coord>
auto UnsquareSpaceGrid<Coord>::bottom() const -> Coord
{
	TWIST_CHECK_INVARIANT
	return bottom_;
}

template<class Coord>	
auto UnsquareSpaceGrid<Coord>::right() const -> Coord
{
	TWIST_CHECK_INVARIANT
	return left_ + static_cast<Coord>(cell_width_ * nof_cols_);
}

template<class Coord>
auto UnsquareSpaceGrid<Coord>::top() const -> Coord
{
	TWIST_CHECK_INVARIANT
	return bottom_ + static_cast<Coord>(cell_height_ * nof_rows_);
}

#ifdef _DEBUG
template<class Coord>
auto UnsquareSpaceGrid<Coord>::check_invariant() const noexcept -> void 
{
	assert(cell_width_ > 0);
	assert(cell_height_ > 0);
	assert(nof_rows_ > 0);
	assert(nof_cols_ > 0);
}
#endif

// --- Non-member functions ---

template<class Coord>
auto perimeter_rect(const UnsquareSpaceGrid<Coord>& grid) -> Rectangle<Coord>
{
	return Rectangle<Coord>{grid.left(), grid.bottom(), grid.right(), grid.top()};
}

}

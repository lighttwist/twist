///  @file  RealInterval.ipp
///  Inline implementation file for "RealInterval.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include <iomanip>

namespace twist::math {

//
//   RealInterval  class 
//

template<typename T>
typename RealInterval<T>::Type RealInterval<T>::infer_type(bool left_open, bool right_open) 
{
	if (left_open && right_open) return open;
	if (!left_open && right_open) return semi_open_right;
	if (left_open && !right_open) return semi_open_left;
	return closed;
}


template<typename T>
RealInterval<T>::RealInterval()
	: lo_(-infinity)
	, hi_(infinity)
	, type_(Type::open) 
{
	TWIST_CHECK_INVARIANT
}


template<typename T>
RealInterval<T>::RealInterval(T lo, T hi, Type type) 
	: lo_(lo)
	, hi_(hi)
	, type_(type) 
{
	if (lo_ >= hi_) {
		TWIST_THROW(L"The interval lower bound %f must be strictly smaller than the upper bound %f.", lo, hi);
	}
	if (((lo_ == -infinity) && !left_open()) || ((hi_ == infinity) && !right_open())) {
		TWIST_THROW(L"An infinite bound cannot be closed.");
	} 
	TWIST_CHECK_INVARIANT
}
	

template<typename T>
bool RealInterval<T>::operator==(const RealInterval& rhs) const 
{
	TWIST_CHECK_INVARIANT
	return (lo_ == rhs.lo_) && (hi_ == rhs.hi_) && (type_ == rhs.type_);
}


template<typename T>
bool RealInterval<T>::operator!=(const RealInterval& rhs) const
{
	TWIST_CHECK_INVARIANT
	return !(*this == rhs);
}

	
template<typename T>
T RealInterval<T>::hi() const 
{
	TWIST_CHECK_INVARIANT
	return hi_;
} 
	

template<typename T>
T RealInterval<T>::lo() const 
{
	TWIST_CHECK_INVARIANT
	return lo_;
}
	

template<typename T>
typename RealInterval<T>::Type RealInterval<T>::type() const 
{
	TWIST_CHECK_INVARIANT
	return type_;
}


template<typename T>
T RealInterval<T>::length() const 
{
	TWIST_CHECK_INVARIANT
	return hi_ - lo_;
}
	

template<typename T>
bool RealInterval<T>::includes(const RealInterval& other) const 
{			
	TWIST_CHECK_INVARIANT
	return left_bound_ok(other) && right_bound_ok(other);
}


template<typename T>
bool RealInterval<T>::contains(T point) const 
{
	TWIST_CHECK_INVARIANT
	return left_bound_ok(point) && right_bound_ok(point);
}
		

template<typename T>
bool RealInterval<T>::left_open() const 
{
	TWIST_CHECK_INVARIANT
	return (type_ == open) || (type_ == semi_open_left);
}
	

template<typename T>
bool RealInterval<T>::right_open() const 
{
	TWIST_CHECK_INVARIANT
	return (type_ == open) || (type_ == semi_open_right);
}


template<typename T>
bool RealInterval<T>::left_bound_ok(T point) const 
{
	TWIST_CHECK_INVARIANT
	return left_open() ? point > lo_ : point >= lo_;
}


template<typename T>
bool RealInterval<T>::left_bound_ok(const RealInterval& other) const 
{
	TWIST_CHECK_INVARIANT
	if (left_open()) {
		return other.left_open() ? lo_ <= other.lo_ : lo_ < other.lo_;
	}
	return lo_ <= other.lo_;
}


template<typename T>
bool RealInterval<T>::right_bound_ok(T point) const 
{
	TWIST_CHECK_INVARIANT
	return right_open()? point < hi_ : point <= hi_;
}


template<typename T>
bool RealInterval<T>::right_bound_ok(const RealInterval& other) const 
{
	TWIST_CHECK_INVARIANT
	if (right_open()) {
		return other.right_open() ? hi_ >= other.hi_ : hi_ > other.hi_;
	}
	return hi_ >= other.hi_;
}


#ifdef _DEBUG
template<typename T>
void RealInterval<T>::check_invariant() const noexcept
{
	assert(lo_ < hi_);
	assert(closed <= type_ && type_ <= semi_open_right);
}
#endif 

//
//  Global functions
//

template<typename T>
std::wstring to_str(const RealInterval<T>& interval, size_t precision) 
{
	std::wstringstream text;
	text << std::fixed;
	if (precision > 0) {
		text << std::setprecision(static_cast<std::streamsize>(precision));
	}
	text << (interval.left_open() ? L"(" : L"["); 
	if (interval.lo() != -RealInterval<T>::infinity) { 
		text << interval.lo(); 
	} 
	else { 
		text << L"-INF"; 
	} 
	text << L", ";
	if (interval.hi() != RealInterval<T>::infinity) {
		text << interval.hi(); 
	}
	else {
		text << L"INF";
	}
	text << (interval.right_open() ? L")" : L"]");
	return text.str().c_str();
}


template<typename T> 
RealInterval<T> connect(const RealInterval<T>& interval1, const RealInterval<T>& interval2)
{
	const T lo = std::min(interval1.lo(), interval2.lo());
	const T hi = std::max(interval1.hi(), interval2.hi());
	return RealInterval<T>(lo, hi);
}


template<class T, class Iter, class OutIter> 
void partition(const RealInterval<T>& interval, Iter first_point, Iter last_point, OutIter out_it, 
		bool left_open)
{
	if (first_point == last_point) {
		*out_it = interval;
		return;
	}
	
	Iter curPoint = first_point;
	RealInterval<T> curInterval{ interval };

	const auto firstIntervalType = RealInterval<T>::infer_type(interval.left_open(), !left_open);
	const auto midIntervalType = left_open ? 
			RealInterval<T>::semi_open_left : RealInterval<T>::semi_open_right;
	const auto lastIntervalType  = RealInterval<T>::infer_type(left_open, interval.right_open());

	while (curPoint != last_point) {
		if (!curInterval.contains(*curPoint)) {
			UT_THROWF(L"Invalid value %f in point range (either the value is outside the interval, or the range is not sorted).", *first_point);
		}
	
		const auto intervalType = (curPoint != first_point) ? midIntervalType : firstIntervalType;

		*out_it++ = RealInterval<T>{ curInterval.lo(), *curPoint, intervalType };
		const T prevPoint = *curPoint++;

		if (curPoint != last_point) {
			curInterval = RealInterval<T>{ prevPoint, curInterval.hi(), midIntervalType };
		}
		else {
			// We have reached the last curPoint in the range. Output the last interval too, as this loop will 
			// not be entered again.
			*out_it = RealInterval<T>{ prevPoint, interval.hi(), lastIntervalType };
		}
	}	
}


template<typename T, typename Iter>
Iter find_real_interval(T point, Iter first_interval, Iter last_interval, Iter start_interval)
{
	return spiral_search(first_interval, last_interval, start_interval, [=](const auto& interval) { 
		return interval.contains(point); 
	});
}

} 




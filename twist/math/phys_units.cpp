/// @file phys_units.cpp
/// Implementation file for "phys_units.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/math/phys_units.hpp"

#include "twist/math/PolarVector.hpp"
#include "twist/math/trigonometry.hpp"

#include <numbers>

namespace twist::math {

auto calc_rel_humidity(double spec_hum, double psurf, double tk) -> double
{
    // psurf    : surface pressure (hPa)
    // rel_hum  : relative humidity (%)
    // spec_hum : specific humidity (kg/kg)
    // tk       : air temp (K)
    // es       : saturation vapour pressure
    // ws       : specific humidity at saturation

    double tsot = 373.16 / tk;
    double ewlog = -7.90298 * (tsot-1) + 5.02808 * log(tsot) / log(10.0);
    double ewlog2 = ewlog - 1.3816e-07 * (pow(10, 11.344 * (1 - (1/tsot))) - 1);
    double ewlog3 = ewlog2 + 0.0081328 * (pow(10, -3.49149 * (tsot-1)) - 1);
    double ewlog4 = ewlog3 + log(1013.246) / log(10.0);
    double es = pow(10.0, ewlog4);
    double ws = 0.622 * es / (psurf - es);
    double rel_hum = spec_hum * 100.0 / ws;

    return rel_hum;
}

auto wind_rose_degrees_to_reversed_unit_circle_radians(double deg) -> double
{
    return normalise_to_2pi(3 * std::numbers::pi / 2 - degrees_to_radians(deg));
}

auto wind_rose_vector_with_degrees_to_cartesian(double speed, double deg) -> std::tuple<double, double>
{
	const auto rad = wind_rose_degrees_to_reversed_unit_circle_radians(deg);
        
    auto wind_vec = PolarVector<double>{PolarVector<double>::from_polar, speed, rad};
    const auto eastward_wind = (deg != 0 && deg != 180 && deg != 360) ? wind_vec.x_component() : 0; 
    const auto northward_wind = (deg != 90 && deg != 270) ? wind_vec.y_component() : 0;

    return {eastward_wind, northward_wind};
}

}

/// @file UnivarDistrSample.ipp
/// Inline implementation file for "UnivarDistrSample.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/math/RandNumGenerator.hpp"

namespace twist::math {

template<class Sam>
UnivarDistrSample<Sam>::UnivarDistrSample(Ssize size) 
	: size_{ size }
	, sample_{ std::make_unique<Sam[]>(size_) }
{		
	if (size_ == 0) {
		TWIST_THROW(L"Cannot construct an empty sample.");
	}
}

template<class Sam>
template<class Iter, class> 
UnivarDistrSample<Sam>::UnivarDistrSample(Iter sample_begin, Iter sample_end) 
	: size_{ std::distance(sample_begin, sample_end) }
	, sample_{ std::make_unique<Sam[]>(size_) }
{		
	if (size_ == 0) {
		TWIST_THROW(L"Cannot construct an empty sample.");
	}
	std::copy(sample_begin, sample_end, begin());
}


template<class Sam>
UnivarDistrSample<Sam>::UnivarDistrSample(std::unique_ptr<Sam[]> buffer, Ssize buffer_size)
	: size_{ buffer_size }
	, sample_{ move(buffer) }
{
	if (size_ == 0) {
		TWIST_THROW(L"Cannot construct an empty sample.");
	}
} 


template<class Sam>
std::unique_ptr<UnivarDistrSample<Sam>> UnivarDistrSample<Sam>::get_clone() const
{
	return std::make_unique<UnivarDistrSample>(begin(), end());
}


template<class Sam>
const Sam& UnivarDistrSample<Sam>::operator[](Ssize idx) const 
{
	return sample_[idx];
}	


template<class Sam>
Sam& UnivarDistrSample<Sam>::operator[](Ssize idx) 
{
	return sample_[idx];
}	
	

template<class Sam>
const Sam& UnivarDistrSample<Sam>::at(Ssize idx) const 
{
	if (idx >= size_) {
		TWIST_THROW(L"Sample value index %d out of bounds (the sample size is %d).", idx, size_);
	}
	return sample_[idx];
}	


template<class Sam>
Sam& UnivarDistrSample<Sam>::at(Ssize idx) 
{
	if (idx >= size_) {
		TWIST_THROW(L"Sample value index %d out of bounds (the sample size is %d).", idx, size_);
	}
	return sample_[idx];
}	
	

template<class Sam>
Sam UnivarDistrSample<Sam>::first() const 
{
	return *begin();
}
	

template<class Sam>
Sam UnivarDistrSample<Sam>::last() const 
{
	return *(end() - 1);
}
	

template<class Sam>
typename UnivarDistrSample<Sam>::const_iterator UnivarDistrSample<Sam>::begin() const 
{
	return sample_.get();
}
	

template<class Sam>
typename UnivarDistrSample<Sam>::iterator UnivarDistrSample<Sam>::begin() 
{
	return sample_.get();
}


template<class Sam>
typename UnivarDistrSample<Sam>::const_iterator UnivarDistrSample<Sam>::end() const 
{
	return begin() + size_;
}


template<class Sam>
typename UnivarDistrSample<Sam>::iterator UnivarDistrSample<Sam>::end() 
{
	return begin() + size_;
}


template<class Sam>
Ssize UnivarDistrSample<Sam>::size() const 
{
	return size_;
}
	

template<class Sam>
const Sam* UnivarDistrSample<Sam>::buffer() const 
{
	return sample_.get();
}


template<class Sam>
Sam* UnivarDistrSample<Sam>::buffer() 
{
	return sample_.get();
}


template<class Sam>
const Sam* UnivarDistrSample<Sam>::data() const 
{
	return buffer();
}


template<class Sam>
void UnivarDistrSample<Sam>::sort() 
{
	std::sort(begin(), end());
}
	

template<class Sam>
bool UnivarDistrSample<Sam>::is_sorted() const 
{
	return std::is_sorted(begin(), end());
}


template<class Sam>
Sam UnivarDistrSample<Sam>::min_val() const 
{
	return *std::min_element(begin(), end());
}
	

template<class Sam>
Sam UnivarDistrSample<Sam>::max_val() const 
{
	return *std::max_element(begin(), end());
}
	

template<class Sam>
void UnivarDistrSample<Sam>::min_max_val(Sam& min, Sam& max) const 
{
	auto result = std::minmax_element(begin(), end());
	min = *result.first;
	max = *result.second;
}


template<class Sam>
std::tuple<Sam, Sam> UnivarDistrSample<Sam>::min_max_val() const 
{
	const auto result = std::minmax_element(begin(), end());
	return { *result.first, *result.second };
}


template<class Sam>
void UnivarDistrSample<Sam>::fill(Sam val) 
{
	std::fill(begin(), end(), val);
}


template<class Sam>
void UnivarDistrSample<Sam>::resize(Ssize new_size) 
{
	if (new_size == 0) {
		TWIST_THROW(L"Cannot construct an empty sample.");
	}
	if (new_size != size_) {
		size_ = new_size;
		sample_ = std::make_unique<Sam[]>(size_);
	}	
}
	

template<class Sam>
template<class Iter, class> 
void UnivarDistrSample<Sam>::subsample(Iter val_indexes_begin, Iter val_indexes_end, 
		UnivarDistrSample<Sam>& subsample) const 
{
	if (subsample.size() != std::distance(val_indexes_begin, val_indexes_end)) {
		TWIST_THROW(L"The subsample has the wrong size.");
	}
	Ssize subsample_idx = 0;
	for (auto it = val_indexes_begin; it != val_indexes_end; ++it, ++subsample_idx) {
		subsample.sample_[subsample_idx] = sample_[*it];
	}
}


template<class Sam>
template<class OutIter, class> 
void UnivarDistrSample<Sam>::get_pointers(OutIter out_it) const 
{
	std::transform(begin(), end(), out_it, [](const Sam& el) {
		return &el;
	});
}


template<class Sam>
template<class OutIter, class> 
void UnivarDistrSample<Sam>::get_pointers(OutIter out_it) 
{
	std::transform(begin(), end(), out_it, [](Sam& el) {
		return &el;
	});
}


template<class Sam>
void UnivarDistrSample<Sam>::resample(UnivarDistrSample& new_sample, unsigned long rand_seed) const 
{
	/// Generate random sample value indexes.
	std::unique_ptr<RandNumGenerator> randGen{  //devnote: we should use the new language facilities
			rand_seed == 0 ? new RandNumGenerator() : new RandNumGenerator(rand_seed) };

	const auto this_size = size();		
	for (iterator it = new_sample.begin(); it != new_sample.end(); ++it) {
		const auto rand_idx = static_cast<Ssize>(floor(randGen->get_sample() * this_size));
		*it = sample_[rand_idx];
	}
}

//
//  Free functions
//

template<class Sam>
typename UnivarDistrSample<Sam>::const_iterator begin(const UnivarDistrSample<Sam>& sample)  
{
	return sample.begin();
}
	

template<class Sam>
typename UnivarDistrSample<Sam>::iterator begin(UnivarDistrSample<Sam>& sample) 
{
	return sample.begin();
}


template<class Sam>
typename UnivarDistrSample<Sam>::const_iterator end(const UnivarDistrSample<Sam>& sample)  
{
	return sample.end();
}

template<class Sam>
typename UnivarDistrSample<Sam>::iterator end(UnivarDistrSample<Sam>& sample) 
{
	return sample.end();
}

}


///  @file  RandNumGenerator.hpp
///  RandNumGenerator class

//   Copyright (c) 1997-2019, Makoto Matsumoto, Takuji Nishimura, Daniel Lewandowski and Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_MATH_RAND_NUM_GENERATOR_HPP
#define TWIST_MATH_RAND_NUM_GENERATOR_HPP

#include <time.h>

namespace twist::math {

///
///  This class implements a Mersenne Twister random number generator.
///  This method has been developed by M. Matsumoto and T. Nishimura and introduced in their paper 
///    "Mersenne Twister: A 623-dimensionally equidistributed uniform pseudorandom number generator". 
///     http://www.math.sci.hiroshima-u.ac.jp/%7Em-mat/MT/ARTICLES/mt.pdf
///  The method's period is 2^19937-1.
///  This implementation is based on the code by Jasper Bedaux (see http://www.bedaux.net/mtrand).
///
class RandNumGenerator {
public:
	/// Constructor. Takes random seed based on time.
	///
	RandNumGenerator() 
	{ 
		set_seed((unsigned long)clock()); 
		init_ = true;
	}

	/// Constructor.
	///
	/// @param[in] s  Random seed value.
    ///
	RandNumGenerator(unsigned long s) 
	{ 
		set_seed(s); 
		init_ = true;
	}
	
	/// Destructor.
	///
	virtual ~RandNumGenerator() 
	{
	}

	/// Set random seed
	///
	void set_seed(unsigned long s);

	/// Set the random seed to a random number
	///
	void set_seed_random();

	/// Generate a new random number in the interval (0, 1).
	///
	/// @return  The number.
	///
	double get_sample();
  
	TWIST_NO_COPY_NO_MOVE(RandNumGenerator)

private:
	/// Generate new state
	///
	void gen_state();

	/// Generate 32 bit random integer
	///
	unsigned long rand_int32(); 

	/// Used by  gen_state()
	///
	unsigned long twiddle(unsigned long, unsigned long); 

	// Compile time constants
	static const int  k_n  = 624; 
	static const int  k_m  = 397; 

	// State vector array
	static unsigned long  state_[k_n]; 

	// Position in state array
	static int  p_; 

	// True if init function is called
	static bool  init_; 
};


inline unsigned long RandNumGenerator::twiddle(unsigned long u, unsigned long v) 
{
	return (((u & 0x80000000UL) | (v & 0x7FFFFFFFUL)) >> 1) ^ ((v & 1UL) ? 0x9908B0DFUL : 0x0UL);
}


inline unsigned long RandNumGenerator::rand_int32() 
{
	if (p_ == k_n) {
		gen_state(); // new state vector needed
	}
	unsigned long x = state_[p_++];
	x ^= (x >> 11);
	x ^= (x << 7) & 0x9D2C5680UL;
	x ^= (x << 15) & 0xEFC60000UL;
	return x ^ (x >> 18);
}

} 

#endif 


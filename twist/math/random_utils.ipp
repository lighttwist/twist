/// @file random_utils.ipp
/// Inline implementation file for "random_utils.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/IndexRange.hpp"

#include <random>

namespace twist::math {

namespace detail {

template<class T>
using UniformDistribution = std::conditional_t<std::is_integral_v<T>, std::uniform_int_distribution<T>,
	                                                                  std::uniform_real_distribution<T>>;

template<template<class U> class RandNumDistr, class T, class Generator> 
[[nodiscard]] auto generate_uniform_sample(Ssize size, T lo, T hi, Generator& gen) -> std::vector<T>
{
	auto sample = std::vector<T>(size);
    auto distr = RandNumDistr<T>{lo, hi};
	for (auto i : IndexRange{size}) {
		sample[i] = distr(gen);
	}
	return sample;
}

template<template<class U> class RandNumDistr, class T> 
[[nodiscard]] auto generate_uniform_sample(Ssize size, T lo, T hi, RandEngineType rand_engine_type) -> std::vector<T>
{
	if (rand_engine_type != RandEngineType::mt_19937) {
		TWIST_THROW(L"Unrecognised pseudo-random number generator engine type value %d.", rand_engine_type);
	}

	auto rd = std::random_device{};
    auto mt = std::mt19937{rd()};
	return generate_uniform_sample<RandNumDistr>(size, lo, hi, mt);
}

}

template<class T, class> 
[[nodiscard]] auto generate_uniform_real_sample(Ssize size, T lo, T hi, RandEngineType rand_engine_type) 
                    -> std::vector<T>
{
	return detail::generate_uniform_sample<std::uniform_real_distribution>(size, lo, hi, rand_engine_type);
}

template<class T, class Generator, class> 
[[nodiscard]] auto generate_uniform_real_sample(Ssize size, T lo, T hi, Generator& gen) -> std::vector<T>
{
	return detail::generate_uniform_sample<std::uniform_real_distribution>(size, lo, hi, gen);
}

template<class T, class> 
[[nodiscard]] auto generate_uniform_int_sample(Ssize size, T lo, T hi, RandEngineType rand_engine_type) 
                    -> std::vector<T>
{
	return detail::generate_uniform_sample<std::uniform_int_distribution>(size, lo, hi, rand_engine_type);
}

template<class T, class Generator, class> 
[[nodiscard]] auto generate_uniform_sample(Ssize size, T lo, T hi, Generator& gen) -> std::vector<T>
{
	return detail::generate_uniform_sample<detail::UniformDistribution>(size, lo, hi, gen);
}

template<class T, class Generator, class> 
[[nodiscard]] auto generate_uniform_int_sample(Ssize size, T lo, T hi, Generator& gen) -> std::vector<T>
{
	return detail::generate_uniform_sample<std::uniform_int_distribution>(size, lo, hi, gen);
}

template<class RandDistr, class Generator>
[[nodiscard]] auto generate_rand_value_greater_than_min(RandDistr& rand_distr, Generator& gen) 
                    -> typename RandDistr::result_type
{
	const auto min_val = rand_distr.min();
	auto val = min_val;
	while (val == min_val) {
		val = rand_distr(gen);
	}
	return val;
}

}

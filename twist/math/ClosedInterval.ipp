/// @file ClosedInterval.ipp
/// Inline implementation file for "ClosedInterval.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace twist::math {

template<class T>
ClosedInterval<T>::ClosedInterval(T lo, T hi) 
	: lo_{lo}
	, hi_{hi}
{
	if (lo_ > hi_) {
		TWIST_THROW(L"The bounds %f and %f of the interval are ill-specified.", lo_, hi_);
	}
	TWIST_CHECK_INVARIANT
}
		
template<class T>
auto ClosedInterval<T>::hi() const -> T
{
	TWIST_CHECK_INVARIANT
	return hi_;
} 
	
template<class T>
auto ClosedInterval<T>::lo() const -> T
{
	TWIST_CHECK_INVARIANT
	return lo_;
}

#ifdef _DEBUG
template<class T>
void ClosedInterval<T>::check_invariant() const noexcept
{
	assert(lo_ <= hi_);
}
#endif 

// --- Free functions ---

template<class T>
bool operator==(const ClosedInterval<T>& lhs, const ClosedInterval<T>& rhs) 
{
	return lhs.lo() == rhs.lo() && lhs.hi() == rhs.hi();
}

template<class T>
bool operator!=(const ClosedInterval<T>& lhs, const ClosedInterval<T>& rhs) 
{
	return lhs.lo() != rhs.lo() || lhs.hi() != rhs.hi();
}

template<class T>
[[nodiscard]] auto length(const ClosedInterval<T>& interv) -> T
{
	return interv.hi() - interv.lo();
}

template<class T>
[[nodiscard]] auto includes(const ClosedInterval<T>& interv1, const ClosedInterval<T>& interv2) -> bool
{
	return interv1.lo() <= interv2.lo() && interv2.hi() <= interv1.hi();
}

template<class T>
[[nodiscard]] auto contains(const ClosedInterval<T>& interv, T point) -> bool
{
	return interv.lo() <= point && point <= interv.hi();
}

template<class T, class>
[[nodiscard]] auto count_elements(const ClosedInterval<T>& interv) -> Ssize
{
	return interv.hi() - interv.lo() + 1;
}

template<class T, class> 
std::vector<T> enumerate_elements(const ClosedInterval<T>& interv)
{
	const auto count = count_elements(interv);
	std::vector<T> ret(static_cast<unsigned int>(count));
	for (auto i = 0; i < count; ++i) {
		ret[i] = interv.lo() + i;
	}
	return ret;
}

template<class T, class Fn, class, class> 
void for_each_element(const ClosedInterval<T>& interv, Fn&& func)
{
	for (const auto el : enumerate_elements(interv)) {
		std::forward<Fn>(func)(el);
	}
}

template<class T>
[[nodiscard]] auto intersect(const ClosedInterval<T>& interv1, const ClosedInterval<T>& interv2) 
                    -> std::optional<ClosedInterval<T>>
{
	if (interv1.hi() < interv2.lo() || interv1.lo() > interv2.hi()) {
		return std::nullopt;
	}

	return std::make_optional<ClosedInterval<T>>(std::max(interv1.lo(), interv2.lo()),
			                                     std::min(interv1.hi(), interv2.hi()));
}

template<class T>
[[nodiscard]] auto unite(const ClosedInterval<T>& interv1, const ClosedInterval<T>& interv2) 
                    -> std::optional<ClosedInterval<T>>
{
	if (interv1.hi() < interv2.lo() || interv1.lo() > interv2.hi()) return {};

	return cover(interv1, interv2);
}

template<class T>
[[nodiscard]] auto cover(const ClosedInterval<T>& interv1, 
		const ClosedInterval<T>& interv2) -> ClosedInterval<T>
{
	return { std::min(interv1.lo(), interv2.lo()),
			 std::max(interv1.hi(), interv2.hi()) };
}

template<class T>
T enclose(const ClosedInterval<T>& interv, T point)
{
	if (point < interv.lo()) {
		return interv.lo();
	}
	if (point > interv.hi()) {
		return interv.hi();
	}
	return point;
}

template<class T>
[[nodiscard]] auto to_index_range(const ClosedInterval<T>& interv) -> IndexRange<T>
{
	return IndexRange{interv.lo(), interv.hi()};
}

template<class T>
[[nodiscard]] auto as_string(const ClosedInterval<T>& interv) -> std::wstring
{
	return std::format(L"[{}, {}]", interv.lo(), interv.hi());
}

}

/// @file geometry.ipp
/// Inline implementation file for "geometry.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace twist::math {

// --- Point class template ---

template<class Coordinate>
Point<Coordinate>::Point()
	: x_{0}
	, y_{0}
{
}

template<class Coordinate>
Point<Coordinate>::Point(Coord x, Coord y)
	: x_{x}
	, y_{y}
{
}

template<class Coordinate>
auto Point<Coordinate>::x() const -> Coord
{
	return x_;
}

template<class Coordinate>
auto Point<Coordinate>::y() const -> Coord
{
	return y_;
}

template<class Coordinate>
auto Point<Coordinate>::move(Coord dx, Coord dy) const -> Point
{
	return Point(x_ + dx, y_ + dy);
}

template<class Coordinate> 
auto operator==(const Point<Coordinate>& lhs, const Point<Coordinate>& rhs) -> bool
{
	return lhs.x() == rhs.x() && lhs.y() == rhs.y();
}

// --- Vector class template ---

template<class Coord>
Vector<Coord>::Vector() 
	: x_(0)
	, y_(0) 
{
}

template<class Coord>
Vector<Coord>::Vector(double x, double y) 
	: x_(x)
	, y_(y) 
{
}
	
template<class Coord>
Vector<Coord>::Vector(const Vector& orig) 
	: x_(orig.x_)
	, y_(orig.y_) 
{
}

template<class Coord>
void Vector<Coord>::operator=(const Vector& rhs) 
{
	x_ = rhs.x_;
	y_ = rhs.y_;
}

template<class Coord>
Vector<Coord> Vector<Coord>::operator-() const 
{
	return Vector(-x_, -y_);
}

template<class Coord>
Coord Vector<Coord>::x() const 
{
	return x_;
}

template<class Coord>
void Vector<Coord>::set_x(double x) 
{
	x_ = x;
}

template<class Coord>
Coord Vector<Coord>::y() const 
{
	return y_;
}

template<class Coord>
void Vector<Coord>::set_y(double y) 
{
	y_ = y;
}

template<class Coord>
void Vector<Coord>::set(double x, double y) 
{
	x_ = x;
	y_ = y;
}

template<class Coord>
double Vector<Coord>::length() const 
{
	return sqrt(x_ * x_ + y_ * y_);
}

template<class Coord>
void Vector<Coord>::operator+=(const Vector& rhs) 
{
	x_ += rhs.x_;
	y_ += rhs.y_;
}

template<class Coord>
Vector<Coord> Vector<Coord>::operator+(const Vector<Coord>& rhs) const 
{
	return Vector<Coord>(x_ + rhs.x_, y_ + rhs.y_);
}

template<class Coord>
void Vector<Coord>::operator*=(double rhs) 
{
	x_ *= rhs;
	y_ *= rhs;
}

template<class Coord>
Vector<Coord> Vector<Coord>::operator*(double rhs) const 
{
	return Vector<Coord>(x_ * rhs, y_ * rhs);
}

//
//  Rectangle class template
//

template<class Coord>
Rectangle<Coord>::Rectangle(Coord x1, Coord y1, Coord x2, Coord y2)
{
	if (x1 <= x2) {
		left_  = x1;
		right_ = x2;
	}
	else {
		right_ = x1;
		left_  = x2;
	}
	if (y1 <= y2) {
		bottom_ = y1;
		top_    = y2;
	}
	else {
		top_    = y1;
		bottom_ = y2;
	}
	TWIST_CHECK_INVARIANT
}


template<class Coord>
Rectangle<Coord>::Rectangle(const Point<Coord>& pt1, const Point<Coord>& pt2)
{
	if (pt1.x() < pt2.x()) {
		left_  = pt1.x();
		right_ = pt2.x();
	}
	else {
		right_ = pt1.x();
		left_  = pt2.x();
	}
	if (pt1.y() < pt2.y()) {
		bottom_ = pt1.y();
		top_    = pt2.y();
	}
	else {
		top_    = pt1.y();
		bottom_ = pt2.y();
	}
	TWIST_CHECK_INVARIANT
}

template<class Coord>
auto Rectangle<Coord>::operator==(const Rectangle& rhs) const -> bool 
{
	TWIST_CHECK_INVARIANT
	return left_ == rhs.left_ && bottom_ == rhs.bottom_ && right_ == rhs.right_ && top_ == rhs.top_;
}

template<class Coord>
auto Rectangle<Coord>::operator!=(const Rectangle& rhs) const -> bool 
{
	TWIST_CHECK_INVARIANT
	return !operator==(rhs);
}

template<class Coord>
auto Rectangle<Coord>::left() const -> Coord 
{
	TWIST_CHECK_INVARIANT
	return left_;
}

template<class Coord>
auto Rectangle<Coord>::right() const -> Coord
{
	TWIST_CHECK_INVARIANT
	return right_;
}

template<class Coord>
auto Rectangle<Coord>::bottom() const -> Coord
{
	TWIST_CHECK_INVARIANT
	return bottom_;
}

template<class Coord>
auto Rectangle<Coord>::top() const -> Coord 
{
	TWIST_CHECK_INVARIANT
	return top_;
}

template<class Coord>
auto Rectangle<Coord>::width() const -> Coord 
{
	TWIST_CHECK_INVARIANT
	return right_ - left_;
}

template<class Coord>
auto Rectangle<Coord>::height() const -> Coord 
{
	TWIST_CHECK_INVARIANT
	return top_ - bottom_;
}

template<class Coord>
auto Rectangle<Coord>::bottom_left() const -> Point<Coord>
{
	TWIST_CHECK_INVARIANT
	return { left_, bottom_ };
}

template<class Coord>
auto Rectangle<Coord>::bottom_right() const -> Point<Coord>
{
	TWIST_CHECK_INVARIANT
	return { right_, bottom_ };
}

template<class Coord>
auto Rectangle<Coord>::top_left() const -> Point<Coord>
{
	TWIST_CHECK_INVARIANT
	return { left_, top_ };
}

template<class Coord>
auto Rectangle<Coord>::top_right() const -> Point<Coord>
{
	TWIST_CHECK_INVARIANT
	return { right_, top_ };
}

template<class Coord>
auto Rectangle<Coord>::centroid() const -> Point<Coord>
{
	TWIST_CHECK_INVARIANT
	return {left_ + width() / 2, bottom_ + height() / 2};
}

#ifdef _DEBUG
template<class Coord>
auto Rectangle<Coord>::check_invariant() const noexcept -> void
{
	assert(left_ <= right_);
	assert(bottom_ <= top_);
}
#endif

// --- Free functions ---

template<class Coord>
[[nodiscard]] auto distance(Point<Coord> pt1, Point<Coord> pt2) -> double
{
	return sqrt(sqr(pt1.x() - pt2.x()) + sqr(pt1.y() - pt2.y()));
}

template<class Coord>
[[nodiscard]] auto area(const Rectangle<Coord>& rect) -> Coord
{
	return rect.width() * rect.height();
}

template<class Coord>
[[nodiscard]] auto inflate(const Rectangle<Coord>& rect, Coord d) -> Rectangle<Coord>
{
	if (d < 0 && (abs(d) > rect.width() / 2 || abs(d) > rect.height() / 2)) {
		TWIST_THRO2(L"Invalid deflate value {}: the rectangle is too small.", d);		
	}
	return {rect.left() - d, rect.bottom() - d, rect.right() + d, rect.top() + d};
}

template<class Coord>
std::optional<Rectangle<Coord>> intersect(const Rectangle<Coord>& rect1, const Rectangle<Coord>& rect2)
{
	if (rect1.left() < rect2.right() && rect1.right() > rect2.left() && 
			rect1.bottom() < rect2.top() && rect1.top() > rect2.bottom()) {
		return Rectangle<Coord>(std::max(rect1.left(), rect2.left()), std::max(rect1.bottom(), rect2.bottom()), 
				std::min(rect1.right(), rect2.right()), std::min(rect1.top(), rect2.top()));	
	}
	return {};
}

template<class Coord>
[[nodiscard]] auto contains(const Rectangle<Coord>& rect, const Point<Coord>& pt) -> bool
{
	return pt.x() >= rect.left() && pt.x() <= rect.right() &&
		   pt.y() >= rect.bottom() && pt.y() <= rect.top();
}

template<class Coord>
std::optional<RectQuadrant> contains_in_quadrant(const Rectangle<Coord>& rect, const Point<Coord>& pt)
{
	if (!contains(rect, pt)) return {};
	
	const auto rect_centre = rect.centroid();
	if (pt.x() <= rect_centre.x()) {
		if (pt.y() <= rect_centre.y()) {
			return RectQuadrant::bottom_left;
		}
		return RectQuadrant::top_left;
	}
	if (pt.y() <= rect_centre.y()) {
		return RectQuadrant::bottom_right;
	}
	return RectQuadrant::top_right;
}


template<class Coord>
bool contains(const Rectangle<Coord>& rect1, const Rectangle<Coord>& rect2)
{
	return rect1.left() <= rect2.left() && rect1.right() >= rect2.right() && 
			rect1.bottom() <= rect2.bottom() && rect1.top() >= rect2.top();
}


template<class Coord>
bool contains_tol(const Rectangle<Coord>& rect1, const Rectangle<Coord>& rect2, Coord tol)
{
	return greater_or_equal_tol(rect2.left(), rect1.left(), tol) &&
	       greater_or_equal_tol(rect1.right(), rect2.right(), tol) &&
		   greater_or_equal_tol(rect2.bottom(), rect1.bottom(), tol) &&
		   greater_or_equal_tol(rect1.top(), rect2.top(), tol);
}

template<class Point2D>
[[nodiscard]] auto is_regular_point_grid_tol(const FlatMatrix<Point2D>& grid, 
                                             typename Point2D::Coord tol) 
                    -> std::tuple<bool, typename Point2D::Coord, typename Point2D::Coord>
{
	using Coord = typename Point2D::Coord;

	if (grid.nof_cells() == 0) {
		TWIST_THRO2(L"The input grid must have at least one cell.");
	}

	auto row_step = std::optional<Coord>{};
	auto col_step = std::optional<Coord>{};
	for (auto i : IndexRange{grid.nof_rows()}) {
		for (auto j : IndexRange{grid.nof_columns()}) {
			if (j > 0) {
				const auto cur_row_step = grid(i, j).x() - grid(i, j - 1).x();
				if (!row_step) {
					row_step = cur_row_step;
				}
				else {
					if (!equal_tol(cur_row_step, *row_step, tol)) {
						return {false, Coord{}, Coord{}};
					}
				}
			}
			if (i > 0) {
				const auto cur_col_step = grid(i, j).y() - grid(i - 1, j).y();
				if (!col_step) {
					col_step = cur_col_step;
				}
				else {
					if (!equal_tol(cur_col_step, *col_step, tol)) {
						return {false, Coord{}, Coord{}};
					}
				}
			}
		}	
	}

	return {true, 
	        row_step ? *row_step : Coord{}, 
			col_step ? *col_step : Coord{}};
}

} 

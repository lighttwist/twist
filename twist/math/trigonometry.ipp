/// @file trigonometry.ipp
/// Inline implementation file for "trigonometry.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include <numbers>

namespace twist::math {

template<std::floating_point T>
[[nodiscard]] auto normalise_to_2pi(T radians) -> T
{
	auto ret = std::fmod(radians, 2 * std::numbers::pi);
	if (ret < 0) {
		ret += 2 * std::numbers::pi;
	}
	return ret;
}

template<class T, class>
[[nodiscard]] auto normalise_to_360(T deg) -> T
{
	auto ret = std::fmod(deg, 360);
	if (ret < 0) {
		ret += 360;
	}
	return ret;
}

template<std::floating_point T>
[[nodiscard]] auto radians_to_degrees(T rad) -> T
{
	return static_cast<T>(rad * 180.0 / std::numbers::pi);		
}

template<std::floating_point T>
[[nodiscard]] auto radians_to_degrees_to_360(T rad) -> T 
{
	return normalise_to_360(radians_to_degrees(rad));		
}

template<std::floating_point T>
[[nodiscard]] auto degrees_to_radians(T deg) -> T
{
	return deg * std::numbers::pi / 180.0;
}

template<std::floating_point T>
[[nodiscard]] auto degrees_to_radians_to_2pi(T deg) -> T
{
	return normalise_to_2pi(degrees_to_radians(deg));
}

} 

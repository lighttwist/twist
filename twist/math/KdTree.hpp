/// @file KdTree.hpp
/// KdTree class template 
/// KdNodeBase class template 
/// KdNode class template 

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_MATH_KD_TREE_HPP
#define TWIST_MATH_KD_TREE_HPP

#include "twist/math/kd_geometry.hpp"

namespace twist::math {

template<int k, class Coord, class Data> class KdNode;

/*! A k-d tree (k-dimensional tree): a binary tree whose nodes are points in k-dimensional space.
    A k-d tree is a space-partitioning data structure for organizing the points represented by its nodes.
    K-d trees are a useful data structure for several applications, such as searches involving a multidimensional 
	search key (eg range searches and nearest neighbor searches). 
    K-d trees are a special case of binary space partitioning trees.
    After all the nodes are added to the tree, the tree must be built before any other methods are called.   
    \tparam k  The number of dimensions
    \tparam Coord  The point coordinate type
    \tparam Data  Type for an optional data value to be associated with each tree node; use void if no data is to be 
	              associated with the nodes
 */   
template<int k, class Coord, class Data = void>
class KdTree {
public:
	using Point = KdPoint<k, Coord>;
	using Node = KdNode<k, Coord, Data>;

	explicit KdTree() = default;

	//! Get a clone of this tree.
	[[nodiscard]] auto clone() const -> KdTree requires std::copyable<Data>;

	/*! Add a new node in the tree. There is no check for duplicate point positions.	   
	    \param[in] node  The new node
	 */   
	auto add_node(Node node) -> void;

	//! The number of nodes in the tree.
	[[nodiscard]] auto nof_nodes() const -> Ssize;

	//! Build the tree. Call this after all nodes were added to the tree.
	auto build_tree() -> void;

	//! Whether the tree has been built.
	[[nodiscard]] auto is_built() const -> bool;

	/*! Search among the points represented by the tree nodes for the nearest point (using Euclidian distance in k 
	    dimensions) to a given point (the "test point"). 
	    For low numbers of dimensions (two or three) this is very efficient even for very large sets of points.	
		\note  If the tree has not yet been built, an exception is thrown.
	    \param[in] test_point  The test point; if the point exactly matches one of the tree points, then the node 
		                       containing that point will be returned
	    \return  0. The node containing the nearest point
	   			 1. The square of the distance between the given and the nearest point
	 */   
	[[nodiscard]] auto find_nearest(const Point& test_point) const -> std::tuple<const Node&, Coord>;

	/*! Get the root node of the tree.
		\note  If the tree has not yet been built, an exception is thrown.
	 */
	[[nodiscard]] auto root() const -> const Node&;

	TWIST_NO_COPY_DEF_MOVE(KdTree)
	
private:
	[[nodiscard]] auto build_tree_impl(Ssize* node_indexes, Ssize nof_nodes, Ssize depth) -> Node*;

	static auto find_nearest_impl(const Node* cur_node, const Point& test_point, int dimension_idx, 
	                              const Node*& best_node, std::optional<Coord>& best_dist_squared) -> void;

	Node* root_{nullptr};
	std::vector<Node> nodes_;

	TWIST_CHECK_INVARIANT_DECL
};

/*! Abstract base for classes representing node in a k-d tree (k-dimensional tree).
    A k-d tree node contains a point in k-dimensional space.
    \tparam k  The number of dimensions
    \tparam Coord  The point coordinate type
    \tparam Data  Type for an optional data value to be associated with each tree node; use void if no data is to be 
	              associated with the nodes
    \tparam Node  The derived node class type
 */      
template<int k, class Coord, class Node> 
class KdNodeBase {
public:
	using Point = KdPoint<k, Coord>;

	virtual ~KdNodeBase() = 0;

	/*! Get the point coordinate in one of the k dimensions.
	    \param[in] dim  The dimension index (eg 0 for x, 1 for y, 2 for z, etc); passing in an out-of-bounds index 
		                results in undefined behaviour 
	    \return  The point coordinate
	 */
	[[nodiscard]] auto point() const -> const Point&;

	//! The child node on the left; or nullptr if there is no child on the left.	   
	const Node* left() const;

	//! The child node on the left; or nullptr if there is no subtree on the left.	   
	Node* left();

	//! Set the child node on the left to \p node; pass in nullptr if there is no subtree on the left.	   
	void set_left(Node* node);

	//! The child node on the right; or nullptr if there is no child on the left.	   
	const Node* right() const;

	//! The child node on the right; or nullptr if there is no child on the left.	   
	Node* right();

	//! Set the child node on the right to \p node; pass in nullptr if there is no subtree on the right.	   
	void set_right(Node* node);

	TWIST_NO_COPY_DEF_MOVE(KdNodeBase)

protected:
	/*! Constructor.
	    \param[in]  The point corresponding to the node
	 */
	explicit KdNodeBase(const Point& point);

private:
	Point point_; 
    Node* left_{nullptr};
	Node* right_{nullptr}; 

	TWIST_CHECK_INVARIANT_DECL
};

/*! A node in a k-d tree (k-dimensional tree).
    A k-d tree node contains a point in k-dimensional space.
 */
template<int k, class Coord, class Data> 
class KdNode : public KdNodeBase<k, Coord, KdNode<k, Coord, Data>> {
public:
	using Point = KdPoint<k, Coord>;
	typedef Data Data;

	/*! Constructor.
	    \param[in] data  The data value to be associated with the node
	    \param[in] coords  The k-dimensional point corresponding to the node
	 */
	explicit KdNode(Data data, const Point& point);

	//! The data value associated with the node.
	[[nodiscard]] auto data() const -> const Data&;

	TWIST_NO_COPY_DEF_MOVE(KdNode)

private:
	Data data_;

	TWIST_CHECK_INVARIANT_DECL
};

/*! KdNode class template specialisation for Data = void.
    This node type has no associated data value.
 */
template<int k, class Coord> 
class KdNode<k, Coord, void> : public KdNodeBase<k, Coord, KdNode<k, Coord, void>> {
public:
	using Point = KdPoint<k, Coord>;

	/*! Constructor.
	    \param[in] data  The data value to be associated with the node
	    \param[in] coords  The k-dimensional point corresponding to the node
	 */
	explicit KdNode(const Point& point);

	TWIST_NO_COPY_DEF_MOVE(KdNode)
};

// --- Free functions ---

/*! Create a k-d tree from a set of points in k dimensions. The points are added to the tree as nodes whose data is the
    index of the corresponding point in the range. The tree is not built.
    \tparam PointRange  Point range type; the elements are expected to be KdPoint<> objects
    \param[in] points  The range of points
	\return  The tree
 */
template<rg::input_range PointRange>
auto create_kd_tree(const PointRange& points) -> KdTree<rg::range_value_t<PointRange>::k, 
                                                        typename rg::range_value_t<PointRange>::Coord, 
														Ssize>;

/*! Create a k-d tree from a set of points in k dimensions, whose coordinates and (optionally) data values are read
    from a CSV file. The tree is not built.
    \tparam k  The number of dimensions
    \tparam Coord  The point coordinate type 
    \tparam Data  Tree node data type; use void for no data
    \tparam CreateNodeFromRow  Type of callable which creates a tree node from a set of strings
    \param[in] csv_path  Path to the CSV file
	\param[in] create_node_from_row  Callable which takes the strings on a row in the CSV file and creates a tree node
	\return  The tree
 */
template<int k, class Coord, class Data, class CreateNodeFromRow>
requires std::is_invocable_r_v<typename KdTree<k, Coord, Data>::Node, CreateNodeFromRow, std::vector<std::string>>
auto load_kd_tree_from_csv(fs::path csv_path, CreateNodeFromRow create_node_from_row) -> KdTree<k, Coord, Data>;

}

#include "KdTree.ipp"

#endif 

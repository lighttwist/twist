/// @file numeric_utils.hpp
/// Global utilities for working with numbers

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_MATH_NUMERIC__UTILS_HPP
#define TWIST_MATH_NUMERIC__UTILS_HPP

#include "twist/meta_misc_traits.hpp"

namespace twist::math {

//! Lowest finite value representable by the type unsigned char
static const auto uchar_lowest = std::numeric_limits<unsigned char>::lowest();  

//! Maximum finite value representable by the type unsigned char
static const auto uchar_max = std::numeric_limits<unsigned char>::max();  

//! Lowest finite value representable by the type short
static const auto short_lowest = std::numeric_limits<short>::lowest();  

//! Maximum finite value representable by the type short
static const auto short_max = std::numeric_limits<short>::max();  

//! Maximum finite double value 
static const auto double_max = std::numeric_limits<double>::max();  

//! Minimum positive normalised double value 
static const auto double_min = std::numeric_limits<double>::min();  

//! The machine epsilon, that is, the difference between 1.0 and the next value representable by the double type
static const auto double_epsilon = std::numeric_limits<double>::epsilon();  

//! The lowest finite double value, that is, a finite value x such that there is no other finite value y 
//! where y < x
static const auto double_lowest = std::numeric_limits<double>::lowest();

//! Maximum finite float value 
static const auto float_max = std::numeric_limits<float>::max();  

//! Minimum positive normalised float value 
static const auto float_min = std::numeric_limits<float>::min();  

//! The machine epsilon, that is, the difference between 1.0 and the next value representable by the float type
static const auto float_epsilon = std::numeric_limits<float>::epsilon();  

//! The lowest finite float value, that is, a finite value x such that there is no other finite value y 
//! where y < x
static const auto float_lowest = std::numeric_limits<float>::lowest();

//! The special value "positive infinity", as represented by the float type
static const auto float_inf = std::numeric_limits<float>::infinity();  

/*! Safely convert a number from one numeric type to another.
    \tparam Dest  Destination number type
    \tparam Src  Source number type
    \param[in] src  The source number
    \return  The result of the conversion
 */
template<typename Dest, typename Src> 
[[nodiscard]] auto convert_num(Src src) -> Dest;

/*! Round a 64-bit floating-point number to an int value.
    If the argument is such that the return type cannot represent the function value, the return value is undefined.
    \param x  [in] The floating point number
    \return  The rounded number
 */
[[nodiscard]] auto round_to_int(double x) -> int;

/*! The floor function, which maps a real number to the largest previous integer.
    If the argument is such that the return type cannot represent the function value, the return value is undefined.
    \param[in]  x  The argument. 
    \return  The function value.
 */
[[nodiscard]] auto floor_to_int(double x) -> int;

/// The ceiling function, which maps a real number to the smallest following integer.
/// If the argument is such that the return type cannot represent the function value, the return value is undefined.
///
/// @param[in] x  The argument.
/// @return  The function value.
///
int ceil_to_int(double x);

/*! Round a floating-point number to an integer value opf type long.
    If the argument is such that the return type cannot represent the function value, the return value is undefined.
    \param[in] x  The floating point number.
    \return  The rounded number.
 */
[[nodiscard]] auto round_to_long(double x) -> long;

/*! The floor function, which maps a real number to the largest previous long integer.
    If the argument is such that the return type cannot represent the function value, the return value is undefined.
    \param[in] x  The argument. 
    \return  The function value.
 */
[[nodiscard]] auto floor_to_long(double x) -> long;

/*! The floor function, which maps the real number \p x to the largest previous Ssize value.
    If the argument is such that the return type cannot represent the function value, the return value is undefined.
 */
[[nodiscard]] auto floor_to_ssize(double x) -> Ssize;

/*! The ceiling function, which maps a real number \p x to the smallest following long integer value.
    If the argument is such that the return type cannot represent the function value, the return value is undefined.
 */
[[nodiscard]] auto ceil_to_long(double x) -> long;

/*! The ceiling function, which maps a real number \p x to the smallest following Ssize value.
    If the argument is such that the return type cannot represent the function value, the return value is undefined.
 */
[[nodiscard]] auto ceil_to_ssize(double x) -> Ssize;

/*! Round a floating-point number to an integer value of type Ssize.
    If the argument is such that the return type cannot represent the function value, the return value is 
	implementation-defined and an error may or may not be raised.
    \param[in] x  The floating point number.
    \return  The rounded number.
 */
[[nodiscard]] auto round_to_ssize(double x) -> Ssize;

/// Truncate the decimal part of a floating point number to a specific number of decimals (digits afther the 
/// decimal point).
///
/// @tparam  T  The floating point number type
/// @param[in] x  The number to trucate
/// @param[in] decimal_count  The number of decimals to be "kept" 
/// @return  The truncation result
///
template<class T, class = EnableIfFloatingPoint<T>>
T trunc_decimals(T x, int decimal_count);

/// Round the decimal part of a floating point number to a specific number of decimals (digits afther the 
/// decimal point).
///
/// @tparam  T  The floating point number type
/// @param[in] x  The number to round
/// @param[in] decimal_count  The number of decimals to be "kept" 
/// @return  The rounding result
///
template<class T, class = EnableIfFloatingPoint<T>>
T round_decimals(T x, int decimal_count);

/*! Divide the number \p x by \p y. If the quotient is an integer (within the tolerance \p tolerance), then return the 
    quotient, otherwise return nullopt.
 */
template<class T>
requires is_number<T>
[[nodiscard]] auto divide_exactly(T x, T y, double tolerance) -> std::optional<Ssize>;

/*! Whether the integer number \p x is a perfect square, ie can be written as y * y, where y is an integer.
    \note  Zero and one are considered perfect squares.
 */
template<std::integral T>
[[nodiscard]] auto is_perfect_square(T x) -> bool;

/*! Given the integer number \p x which is a perfect square, get its square root.
    \note  If \p x is not a perfect square, an exception is thrown. Zero and one are considered perfect squares.
 */
template<std::integral T>
[[nodiscard]] auto perfect_square_root(T x) -> T;

/// Find out whether a (typically numeric) value falls inside a closed interval.
///
/// @tparam  T  The value type
/// @tparam  I  The interval bound type
/// @param[in] x  The value
/// @param[in] interv_lo  The lower bound of the interval 
/// @param[in] interv_hi  The upper bound of the interval 
/// @return  true if the value falls inside the closed interval 
///
template<class T, class I>
bool in_closed_interval(T x, I interv_lo, I interv_hi);  

/// The sign (or signum) function.
///
/// @tparam  T  The argument type
/// @param[in] x  The argument
/// @return  The function value (-1 for x < 0, 0 for x = 0, 1 for x > 0).
///
template<typename T> 
long sign(T x);

/// Compute the power of two of a given number.
///
/// @tparam  T  The number type
/// @param[in] x  The number value
/// @return  The squared number
///
template<class T,
         class = EnableIfNumber<T>>
[[nodiscard]] constexpr auto sqr(T x) -> T;

/// Generate the "not a number" value.
/// This function returns the "quiet", rather than the "signaling", NaN.
///
/// @return  The "not a number" value.
///
double get_nan();

/*! Find out whether a specific integer is a multiple of another (the reminder of its division by the other integer 
    is zero).
    \param[in] multiple  The number we are checking for being a multiple of the other
    \param[in] divisor  The number we are checking for being a divisor of the other
    \return  true if the first number is a multiple of the second
 */
[[nodiscard]] auto is_multiple(std::integral auto multiple, std::integral auto divisor) -> bool;

/*! Find out whether the floating-point number \p multiple is a multiple of \p divisor (ie the reminder of its division 
    by \p divisor is within the tolerance value \p tol of zero). 
 */
template<std::floating_point T>
[[nodiscard]] auto is_multiple_tol(T multiple, T divisor, T tol) -> bool;

/*! Find out whether a number is equal to another number, using a tolerance.
    \tparam T  The numeric type of the arguments
    \param[in] x  The first number x
    \param[in] y  The second number y
    \param[in] tol  The tolerance (should be positive)
    \return  true only if the two numbers are equal within the given tolerance
 */
template<typename T> 
[[nodiscard]] auto equal_tol(T x, T y, T tol) -> bool;
	
/*! Find out whether a number x is greater than another number y, using a tolerance.
    \tparam T  The numeric type of the arguments
    \param[in] x  The first number x
    \param[in] y  The second number y
    \param[in] tol  The tolerance (should be positive)
 */
template<typename T> 
[[nodiscard]] auto greater_tol(T x, T y, T tol) -> bool;

/*! Find out whether a number x is greater than or equal to another number y, using a tolerance.
    \tparam T  The numeric type of the arguments
    \param[in] x  The first number x
    \param[in] y  The second number y
    \param[in] tol  The tolerance (should be positive)
 */
template<typename T> 
[[nodiscard]] auto greater_or_equal_tol(T x, T y, T tol) -> bool;

/// Binary predicate comparing two numbers using a tolerance.
///
/// @tparam  NumTol  Class wich encodes the number type and tolerance value; must be an instance of the 
///				NumTol concept
///
template<class NumTol>
class NumericLessTol {
public:
	using T = typename NumTol::Type;

	static constexpr T tolerance = NumTol::tolerance;

	NumericLessTol() 
	{ 
		assert(tolerance >= 0); 
	}

	bool operator()(T x, T y) const 
	{ 
		return x < y && y - x > tolerance; 
	}
private:
	static_assert(std::is_arithmetic_v<T>, "Numeric type expected.");
};

template<class NumTol> 
using NumericSetTol = std::set<typename NumTol::Type, NumericLessTol<NumTol>>;

template<class NumTol> 
using NumericMapTol = std::map<typename NumTol::Type, typename NumTol::Type, NumericLessTol<NumTol>>;

/// Numeric tolerance type for comparing double-precision floating-point values with a tolerance of 1E-14.
/// Instance of the NumTol concept.
struct NumTolD14 { using Type = double; static constexpr double tolerance = 1E-14; };

/// Numeric tolerance type for comparing double-precision floating-point values with a tolerance of 1E-13.
/// Instance of the NumTol concept.
struct NumTolD13 { using Type = double; static constexpr double tolerance = 1E-13; };

/// Numeric tolerance type for comparing double-precision floating-point values with a tolerance of 1E-12.
/// Instance of the NumTol concept.
struct NumTolD12 { using Type = double; static constexpr double tolerance = 1E-12; };

/// Numeric tolerance type for comparing double-precision floating-point values with a tolerance of 1E-11.
/// Instance of the NumTol concept.
struct NumTolD11 { using Type = double; static constexpr double tolerance = 1E-11; };

/// Numeric tolerance type for comparing double-precision floating-point values with a tolerance of 1E-10.
/// Instance of the NumTol concept.
struct NumTolD10 { using Type = double; static constexpr double tolerance = 1E-10; };

/// Numeric tolerance type for comparing single-precision floating-point values with a tolerance of 1E-7.
/// Instance of the NumTol concept.
struct NumTolF7 { using Type = float; static constexpr float tolerance = 1E-7F; };

/// Numeric tolerance type for comparing single-precision floating-point values with a tolerance of 1E-6.
/// Instance of the NumTol concept.
struct NumTolF6 { using Type = float; static constexpr float tolerance = 1E-6F; };

/// Numeric tolerance type for comparing single-precision floating-point values with a tolerance of 1E-5.
/// Instance of the NumTol concept.
struct NumTolF5 { using Type = float; static constexpr float tolerance = 1E-5F; };

} 

#include "numeric_utils.ipp"

#endif 

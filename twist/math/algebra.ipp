/// @file algebra.ipp
/// Inline implementation file for "algebra.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace twist::math {

template<typename T>
void multiply(const Matrix<T>& left, const std::vector<T>& right, std::vector<T>& result)
{
	const size_t numRows = left.nof_rows();
	const size_t numCols = left.nof_columns();
	if ((right.size() != numCols) || (result.size() != numRows)) {
		TWIST_THROW(L"Vector/matrix sizes do not match.");
	}

	for (size_t row = 0; row < numRows; ++row) {
		T rowSum = 0; 
		for (size_t col = 0; col < numCols; ++col) {
			rowSum += left.elements_[row][col] * right[col];
		}	
		result[row] = rowSum;
	}
}


template<typename T>
void multiply(const LowerTriangularMatrix<T>& left, const std::vector<T>& right, std::vector<T>& result)
{
    if (ssize(right) != left.size_ || ssize(result) != left.size_) {
        TWIST_THROW(L"The vectors must have the same size as the matrix.");
    }

    for (auto i = 0; i < left.size_; ++i) {
        long double sum = 0;
        for (auto j = 0; j <= i; ++j) {
            sum += left.elements_[i][j] * right[j];
        }
        result[i] = convert_num<T>(sum);
    }
}


template<typename T>
void submatrix(const SymmetricMatrix<T>& matrix, const std::set<Ssize>& rows, const std::set<Ssize>& cols, 
		Matrix<T>& result)
{
	const auto srowcnt = result.nof_rows();
	const auto scolcnt = result.nof_columns();
	if (ssize(rows) != srowcnt || ssize(cols) != scolcnt) {
		TWIST_THROW(L"The size of the resulting submatrix must match the the sizes of the sets of "
				L"row/column indexes.");
	}
	
	Ssize mrow = 0;
	Ssize mcol = 0;
	for (auto srow = 0; srow < srowcnt; ++srow) {
		for (auto scol = 0; scol < scolcnt; ++scol) {		
			mrow = *iter_at(rows, srow);
			mcol = *iter_at(cols, scol);
			if (mrow > mcol) {
				result[srow][scol] = matrix[mrow][mcol]; 
			}
			else {
				result[srow][scol] = matrix[mcol][mrow]; 
			}
		}
	}
}


template<typename T>
void submatrix(const SymmetricMatrix<T>& matrix, const std::set<Ssize>& rows_cols, 
		SymmetricMatrix<T>& submatrix)
{
	const auto sz = ssize(rows_cols);
	if (ssize(rows_cols) != sz) {
		TWIST_THROW(L"The size of the resulting submatrix must be the same as the size of the set of "
				L"row/column indexes.");
	}
	for (auto srow = 0; srow < sz; ++srow) {
		for (auto scol = 0; scol <= srow; ++scol) {
			const auto row = *iter_at(rows_cols, srow);
			const auto col = *iter_at(rows_cols, scol);
			submatrix[srow][scol] = matrix[row][col]; 
		}		
	}
}


template<typename T>
void submatrix(const SymmetricMatrix<T>& matrix, const std::vector<Ssize>& rows_cols, 
		SymmetricMatrix<T>& submatrix)
{
	const auto sz = ssize(rows_cols);
	if (ssize(rows_cols) != sz) {
		TWIST_THROW(L"The size of the resulting submatrix must be the same as the size of the list of "
				L"row/column indexes.");
	}
	for (auto srow = 0; srow < sz; ++srow) {
		for (auto scol = 0; scol <= srow; ++scol) {
			const auto row = rows_cols[srow];
			const auto col = rows_cols[scol];
			submatrix[srow][scol] = matrix[row][col]; 
		}		
	}
}


template<typename T>
void minor(const SymmetricMatrix<T> a, Ssize i, Ssize j, Matrix<T>& result)
{
	if ((result.nof_rows() != result.nof_columns()) || (result.nof_rows() != a.size() - 1)) {
		TWIST_THROW(L"The result matrix must be square and its size must be one less than the size of 'a'.");
	}
	if ((i >= a.size()) || (j >= a.size())) {
		TWIST_THROW(L"Invalid minor row/column index.");
	}
	
    for (auto jLoop = 0; jLoop < a.size(); ++jLoop) {
        for (auto iLoop = 0; iLoop < a.size(); ++iLoop) {
            if ((iLoop != i) && (jLoop != j)) {
                int iDiff = 0;
                int jDiff = 0;
                if (iLoop > i) {
                    iDiff = -1;
                }
                if (jLoop > j) {
                    jDiff = -1;
                }
                result[iLoop + iDiff][jLoop + jDiff] = a[iLoop][jLoop];
            }
        }
    }
}

} 


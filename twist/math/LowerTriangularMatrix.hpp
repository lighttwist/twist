///  @file  LowerTriangularMatrix.hpp
///  LowerTriangularMatrix class template

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_MATH_LOWER_TRIANGULAR_MATRIX_HPP
#define TWIST_MATH_LOWER_TRIANGULAR_MATRIX_HPP

namespace twist::math {

/// This class represents a lower-triangular matrix ie the elements which appear below and on the main 
///   diagonal of a matrix. The elements can be indexed both as being in the lower triangle, and as being in 
///   the upper triangle (if a[i][j] is valid then a[j][i] is also valid and the two are equal).
/// Row and column indexes are always zero-based.
/// The size of the matrix cannot be changed after construction.
///
/// @tparam  T  The type of the matrix elements
///
template<typename T>
class LowerTriangularMatrix {
public:
	/// The data type of the matrix elements
	using ElemType = T;

	/// Proxy class which makes possible double subscript syntax for instances of LowerTriangularMatrix. 
	/// Constant version.
	class SubscriptProxyConst {
	public:
		inline const T& operator[](Ssize col) const;

	private:
		SubscriptProxyConst(const LowerTriangularMatrix<T>* constMatrix, Ssize row);

		const LowerTriangularMatrix<T>*  matrix_;
		Ssize  row_;

		friend class LowerTriangularMatrix<T>;
	};

	/// Proxy class which makes possible double subscript syntax for instances of LowerTriangularMatrix.
	class SubscriptProxy {
	public:
		inline T& operator[](Ssize col);

	private:
		SubscriptProxy(LowerTriangularMatrix<T>* constMatrix, Ssize row);

		LowerTriangularMatrix<T>*  matrix_;
		Ssize  row_;

		friend class LowerTriangularMatrix<T>;
	};

	/// Constructor. After construction, the elements of the matrix have undefined values.
	///
    /// @param[in] size  The size of the matrix (ie the number of rows, which is the same as the number 
	///					of columns)
    ///
	LowerTriangularMatrix(Ssize size);
	
	/// Copy constructor.
	LowerTriangularMatrix(const LowerTriangularMatrix& src);

	/// Destructor.
	~LowerTriangularMatrix();

	/// Copy the contents of another symmetric matrix into this one.
	///
	/// @param[in] original  The other, original symmetric matrix. It must have the same size as this matrix.
	///
	void assign(const LowerTriangularMatrix& src);

	/// Get read-only access to a specific element of the matrix. This is the first operator called when using 
	/// double subscripts and returns the proxy class which deals with the second operator.
	///
	/// @param[in] row  The row index. Behaviour is undefined if it is out of bounds.
	/// @return  Proxy class instance.
	///
	inline SubscriptProxyConst operator[](Ssize row) const;

	/// Get read-write access to a specific element of the matrix. This is the first operator called when 
	/// using double subscripts and returns the proxy class which deals with the second operator.
	///
	/// @param[in] row  The row index. Behaviour is undefined if it is out of bounds.
	/// @return  Proxy class instance.
	///
	inline SubscriptProxy operator[](Ssize row);

	/// Get read-only access to a specific element of the matrix.
	///
	/// @param[in] row  The row index. An exception is thrown if it is invalid.
	/// @param[in] col  The column index. An exception is thrown if it is invalid.
	/// @return  The matrix element.
	///
	const T& elem(Ssize row, Ssize col) const;
	
	/// Get read-only access to a specific element of the matrix.
	///
	/// @param[in] row  The row index. An exception is thrown if it is invalid.
	/// @param[in] col  The column index. An exception is thrown if it is invalid.
	/// @return  The matrix element.
	///
	T& elem(Ssize row, Ssize col);

	/// The size of the matrix (ie the number of rows, which is the same as the number of columns).
	Ssize size() const;

	/// The number of rows in the matrix (which is the same as the number of columns).
	Ssize nof_rows() const;

	/// The number of columns in the matrix (which is the same as the number of rows).
	Ssize nof_columns() const;

	/// Set all elements in the matrix to zero.
	void make_zero();

    /// Sets all elements on the diagonal of the matrix to a specific value.
    ///
    /// @param[in] value  The value.
    ///
	void set_diagonal(const T& value);   
	
    /// Multiply this lower triangular matrix with a (transposed) vector. This matrix is the left factor in 
	/// the multiplication. The size of the matrix, the input vector and the output vector must be the same.
    ///
    /// @param[in] vector  The vector (the right factor in the multiplication); it is treated as a 
	///					transposed vector
    /// @param[in] result_vector  Vector that will be populated with the results of the mutiplication; it is 
	///					treaded as a transposed vector
    ///
	void multiply(const std::vector<T>& vector, std::vector<T>& result_vector) const;

private:	
	template<typename U> 
	friend void multiply(const LowerTriangularMatrix<U>&, const std::vector<U>&, std::vector<U>&);

	LowerTriangularMatrix& operator=(const LowerTriangularMatrix&) = delete;
	
	// Pointer to the memory block where the elements are stored. Only half (one triangle) of the matrix is 
	// actually stored, as the other half mirrors it. The whole main diagonal is stored.
	T**  elements_{};

	// The size of the matrix (i.e. the number of rows, which is the same as the number of columns).
    Ssize  size_;

	TWIST_CHECK_INVARIANT_DECL
};

} 

#include "LowerTriangularMatrix.ipp"

#endif 

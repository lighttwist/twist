/// @file KdTree.ipp
/// Inline implementation file for "KdTree.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/IndexRange.hpp"
#include "twist/meta_misc_traits.hpp"
#include "twist/db/CsvFileReader.hpp"

namespace twist::math {

// --- KdTree class ---

template<int k, class Coord, class Data> 
auto KdTree<k, Coord, Data>::clone() const -> KdTree requires std::copyable<Data>
{
	TWIST_CHECK_INVARIANT
	auto clone = KdTree{};
	clone.root_ = root_;
	for (const auto& node : nodes_) {
		clone.nodes_.push_back(Node{node.data(), node.point()});
	}
	return clone;
}

template<int k, class Coord, class Data> 
auto KdTree<k, Coord, Data>::add_node(Node node) -> void
{
	TWIST_CHECK_INVARIANT
	nodes_.push_back(std::move(node));
}

template<int k, class Coord, class Data> 
auto KdTree<k, Coord, Data>::nof_nodes() const -> Ssize
{
	TWIST_CHECK_INVARIANT
	return ssize(nodes_);
}

template<int k, class Coord, class Data>
auto KdTree<k, Coord, Data>::build_tree() -> void
{
	TWIST_CHECK_INVARIANT
	if (root_) {
		TWIST_THROW(L"The tree has already been built.");
	}
	if (nodes_.empty()) {
		TWIST_THROW(L"At least one node must be added to the tree before building it.");	
	}
	
	auto node_indexes = std::vector<Ssize>(nodes_.size());
	rg::iota(node_indexes, 0);

	root_ = build_tree_impl(node_indexes.data(), ssize(nodes_), 0);
}

template<int k, class Coord, class Data>
auto KdTree<k, Coord, Data>::is_built() const -> bool
{
	TWIST_CHECK_INVARIANT
	return root_;
}

template<int k, class Coord, class Data>  
auto KdTree<k, Coord, Data>::find_nearest(const Point& test_point) const -> std::tuple<const Node&, Coord>
{
	TWIST_CHECK_INVARIANT
	if (!root_) {
		TWIST_THROW(L"The tree has not been built.");
	}

	auto best_node = (const Node*)nullptr;
	auto best_dist_squared = std::optional<Coord>{};
	find_nearest_impl(root_, test_point, 0, best_node, best_dist_squared);
	assert(best_node && best_dist_squared);

	return {*best_node, *best_dist_squared};
}

template<int k, class Coord, class Data>
auto KdTree<k, Coord, Data>::root() const -> const Node&
{
	TWIST_CHECK_INVARIANT
	if (!root_) {
		TWIST_THROW(L"The tree has not been built.");
	}
	return *root_;
}

template<int k, class Coord, class Data>
auto KdTree<k, Coord, Data>::build_tree_impl(Ssize* node_indexes, Ssize nof_nodes, Ssize depth) -> Node*
{
	TWIST_CHECK_INVARIANT
	if (nof_nodes <= 0) {
		return nullptr;
	}

	const int dim = depth % k;
	const auto mid = (nof_nodes - 1) / 2;

	std::nth_element(node_indexes, node_indexes + mid, node_indexes + nof_nodes, [this, dim](auto lhs, auto rhs) {
		return nodes_[lhs].point()[dim] < nodes_[rhs].point()[dim];
	});

	auto& node = nodes_[node_indexes[mid]];
	node.set_left(build_tree_impl(node_indexes, mid, depth + 1));
	node.set_right(build_tree_impl(node_indexes + mid + 1, nof_nodes - mid - 1, depth + 1));

	return &node;
}

template<int k, class Coord, class Data>
auto KdTree<k, Coord, Data>::find_nearest_impl(const Node* cur_node, const Point& test_point, int dim, 
                                               const Node*& best_node, std::optional<Coord>& best_dist_squared) -> void
{
    if (!cur_node) {
		return; // reached a leaf
	}

    const auto dx = cur_node->point()[dim] - test_point[dim];

    find_nearest_impl(dx > 0 ? cur_node->left() : cur_node->right(), 
	                  test_point, 
					  (dim + 1) % k, 
	                  best_node, 
					  best_dist_squared);

    const auto dist_squared = euclidian_distance_squared(cur_node->point(), test_point);  
    if (!best_dist_squared || dist_squared < best_dist_squared) {
        best_dist_squared = dist_squared;
        best_node = cur_node;
    }
 
    if (*best_dist_squared == 0) {
		return; // only bother with this if chance of exact match is high
	}

	// If there is a point in that is closer to the test point that our current guess, it must lie in the 
	// hypershpere centered at the test point and which that passes through the current guess.
	if (dx * dx >= *best_dist_squared) { 
		return;  
	}
	
	find_nearest_impl(dx > 0 ? cur_node->right() : cur_node->left(), 
	                  test_point, 
					  (dim + 1) % k, 
	                  best_node, 
					  best_dist_squared);
}

#ifdef _DEBUG
template<int k, class Coord, class Data>
void KdTree<k, Coord, Data>::check_invariant() const noexcept
{
	if (root_ != nullptr) {
		assert(!nodes_.empty());
	}
}
#endif

// --- KdNodeBase class ---

template<int k, class Coord, class Node> 
KdNodeBase<k, Coord, Node>::KdNodeBase(const Point& point)
	: point_{point}
{
	TWIST_CHECK_INVARIANT
}

template<int k, class Coord, class Node> 
KdNodeBase<k, Coord, Node>::~KdNodeBase()
{
	TWIST_CHECK_INVARIANT
}

template<int k, class Coord, class Node>
auto KdNodeBase<k, Coord, Node>::point() const -> const Point&
{
	TWIST_CHECK_INVARIANT
	return point_;
}

template<int k, class Coord, class Node>
const Node* KdNodeBase<k, Coord, Node>::left() const
{
	TWIST_CHECK_INVARIANT
	return left_;
}

template<int k, class Coord, class Node>
Node* KdNodeBase<k, Coord, Node>::left()
{
	TWIST_CHECK_INVARIANT
	return left_;
}

template<int k, class Coord, class Node>
void KdNodeBase<k, Coord, Node>::set_left(Node* node)
{
	TWIST_CHECK_INVARIANT
	left_ = node;
}

template<int k, class Coord, class Node>
const Node* KdNodeBase<k, Coord, Node>::right() const
{
	TWIST_CHECK_INVARIANT
	return right_;
}

template<int k, class Coord, class Node>
Node* KdNodeBase<k, Coord, Node>::right()
{
	TWIST_CHECK_INVARIANT
	return right_;
}

template<int k, class Coord, class Node>
void KdNodeBase<k, Coord, Node>::set_right(Node* node)
{
	TWIST_CHECK_INVARIANT
	right_ = node;
}

#ifdef _DEBUG
template<int k, class Coord, class Node>
void KdNodeBase<k, Coord, Node>::check_invariant() const noexcept
{
}
#endif

// --- KdNode class ---

template<int k, class Coord, class Data> 
KdNode<k, Coord, Data>::KdNode(Data data, const Point& point)
	: KdNodeBase<k, Coord, KdNode<k, Coord, Data>>{point}
	, data_{std::move(data)}
{
	TWIST_CHECK_INVARIANT
}

template<int k, class Coord, class Data>
auto KdNode<k, Coord, Data>::data() const -> const Data&
{
	TWIST_CHECK_INVARIANT
	return data_;
}

#ifdef _DEBUG
template<int k, class Coord, class Data>
void KdNode<k, Coord, Data>::check_invariant() const noexcept
{
}
#endif

// --- KdNode<k, Coord, void> class template specialisation ---

template<int k, class Coord> 
KdNode<k, Coord, void>::KdNode(const Point& point)
	: KdNodeBase<k, Coord, KdNode<k, Coord, void>>{point}
{
	TWIST_CHECK_INVARIANT
}

// --- Free functions ---

template<rg::input_range PointRange>
auto create_kd_tree(const PointRange& points) -> KdTree<rg::range_value_t<PointRange>::k, 
                                                        typename rg::range_value_t<PointRange>::Coord, 
														Ssize>
{
	using Tree = KdTree<rg::range_value_t<PointRange>::k, 
                        typename rg::range_value_t<PointRange>::Coord, 
						Ssize>;
	using Node = typename Tree::Node;

	auto tree = Tree{};
	auto idx = Ssize{0};
	for (const auto& pt : points) {
		tree.add_node(Node{idx++, pt});
	}
	return tree;
}

template<int k, class Coord, class Data, class CreateNodeFromRow>
requires std::is_invocable_r_v<typename KdTree<k, Coord, Data>::Node, CreateNodeFromRow, std::vector<std::string>>
auto load_kd_tree_from_csv(fs::path csv_path, CreateNodeFromRow create_node_from_row) -> KdTree<k, Coord, Data>
{
	auto tree = KdTree<k, Coord, Data>{};
	auto reader = twist::db::CsvFileReader<char>{std::move(csv_path)};	
	while (reader.read_next_row()) {
		tree.add_node(create_node_from_row(reader.row_ref()));
	}
	return tree;
}

}

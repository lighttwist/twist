///  @file  MultivarDistrSample.ipp
///  Inline implementation file for "MultivarDistrSample.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

namespace twist::math {

template<typename Sam, typename VarId>
MultivarDistrSample<Sam, VarId>::MultivarDistrSample(Ssize size) 
	: size_{size}
{
	TWIST_CHECK_INVARIANT
}

template<typename Sam, typename VarId>
MultivarDistrSample<Sam, VarId>::MultivarDistrSample(const std::vector<VarId>& var_ids, 
		const std::vector<std::wstring>& var_names, Ssize size) 
	: size_{size}
{
	if (var_ids.size() != var_names.size()) {
		TWIST_THROW(L"The number of random variable IDs differs from the number of random variable names.");
	}
	
	auto id_it = var_ids.begin();
	auto name_it = var_names.begin();
	for (; id_it != var_ids.end(); ++id_it, ++name_it) {
		create_var_sample(*id_it, *name_it);	
	}
	TWIST_CHECK_INVARIANT
}

template<typename Sam, typename VarId>
MultivarDistrSample<Sam, VarId>* MultivarDistrSample<Sam, VarId>::get_struct_clone() const
{
	TWIST_CHECK_INVARIANT
	std::unique_ptr<MultivarDistrSample> clone{new MultivarDistrSample(size_)};
	for (auto it = sample_list_.begin(); it != sample_list_.end(); ++it) {
		clone->sample_list_.push_back(new UnivarDistrSample<Sam>(size_), it->getId(), it->getName());
	}
	return clone.release();
}

template<typename Sam, typename VarId>
MultivarDistrSample<Sam, VarId>* MultivarDistrSample<Sam, VarId>::get_clone() const
{
	TWIST_CHECK_INVARIANT
	// Get a structure clone.
	std::unique_ptr<MultivarDistrSample> clone{get_struct_clone()};
	// Copy all sample values across.
	auto it = sample_list_.begin();
	auto clone_it = clone->sample_list_.begin();
	for (; it != sample_list_.end(); ++it, ++clone_it) {
		const UnivarDistrSample<Sam>* varSample = it->getObj();
		UnivarDistrSample<Sam>* cloneVarSample = clone_it->getObj();
		std::copy(varSample->begin(), varSample->end(), cloneVarSample->begin());
	}
	return clone.release();
}

template<typename Sam, typename VarId>
Ssize MultivarDistrSample<Sam, VarId>::get_size() const
{
	TWIST_CHECK_INVARIANT
	return size_;
}

template<typename Sam, typename VarId>
void MultivarDistrSample<Sam, VarId>::resize(Ssize new_size)
{
	TWIST_CHECK_INVARIANT
	for (auto& sample : sample_list_) {
		sample->resize(new_size);
	}
}

template<typename Sam, typename VarId>
UnivarDistrSample<Sam>& MultivarDistrSample<Sam, VarId>::create_var_sample(VarId var_id, const std::wstring& var_name)
{
	TWIST_CHECK_INVARIANT
	if (sample_list_.find(var_name) != nullptr) {
		TWIST_THROW(L"Variable \"%s\" already exists in the joint distribution.", var_name.c_str());
	}
	
	auto* var_sample = new UnivarDistrSample<Sam>(size_);
	sample_list_.push_back(var_sample, var_id, var_name);
	
	return *var_sample;
}

template<typename Sam, typename VarId>
void MultivarDistrSample<Sam, VarId>::add_var_sample(VarId var_id, const std::wstring& var_name, 
		                                             std::unique_ptr<UnivarDistrSample<Sam>> sample)
{
	TWIST_CHECK_INVARIANT
	if (sample_list_.find(var_name) != nullptr) {
		TWIST_THROW(L"Variable \"%s\" already exists in the joint distribution.", var_name.c_str());
	}
	sample_list_.push_back(sample.release(), var_id, var_name);	
	TWIST_CHECK_INVARIANT
}

template<typename Sam, typename VarId>
const UnivarDistrSample<Sam>& MultivarDistrSample<Sam, VarId>::get_var_sample(VarId var_id) const
{
	TWIST_CHECK_INVARIANT	
	return sample_list_.get(var_id);
}

template<typename Sam, typename VarId>
UnivarDistrSample<Sam>& MultivarDistrSample<Sam, VarId>::get_var_sample(VarId var_id) 
{
	TWIST_CHECK_INVARIANT
	return sample_list_.get(var_id);
}

template<typename Sam, typename VarId>	
const UnivarDistrSample<Sam>* MultivarDistrSample<Sam, VarId>::find_var_sample(VarId var_id) const
{
	TWIST_CHECK_INVARIANT
	return sample_list_.find(var_id);
}

template<typename Sam, typename VarId>	
UnivarDistrSample<Sam>* MultivarDistrSample<Sam, VarId>::find_var_sample(VarId var_id)
{
	TWIST_CHECK_INVARIANT
	return sample_list_.find(var_id);
}

template<typename Sam, typename VarId>
const UnivarDistrSample<Sam>& MultivarDistrSample<Sam, VarId>::get_var_sample(const std::wstring& var_name) const
{
	TWIST_CHECK_INVARIANT	
	return sample_list_.get(var_name);
}

template<typename Sam, typename VarId>
UnivarDistrSample<Sam>& MultivarDistrSample<Sam, VarId>::get_var_sample(const std::wstring& var_name) 
{
	TWIST_CHECK_INVARIANT
	return sample_list_.get(var_name);
}

template<typename Sam, typename VarId>
const UnivarDistrSample<Sam>* MultivarDistrSample<Sam, VarId>::find_var_sample(const std::wstring& var_name) const
{
	TWIST_CHECK_INVARIANT
	return sample_list_.find(var_name);
}

template<typename Sam, typename VarId>
UnivarDistrSample<Sam>* MultivarDistrSample<Sam, VarId>::find_var_sample(const std::wstring& var_name) 
{
	TWIST_CHECK_INVARIANT
	return sample_list_.find(var_name);
}

template<typename Sam, typename VarId>
const UnivarDistrSample<Sam>& MultivarDistrSample<Sam, VarId>::get_var_sample_at(Ssize pos) const
{
	TWIST_CHECK_INVARIANT
	return sample_list_.at(pos);
}

template<typename Sam, typename VarId>
UnivarDistrSample<Sam>& MultivarDistrSample<Sam, VarId>::get_var_sample_at(Ssize pos)
{
	TWIST_CHECK_INVARIANT
	return sample_list_.at(pos);
}

template<typename Sam, typename VarId>
Ssize MultivarDistrSample<Sam, VarId>::get_var_sample_pos(VarId var_id) const
{
	TWIST_CHECK_INVARIANT
	return sample_list_.get_pos(var_id);
}

template<typename Sam, typename VarId>
Ssize MultivarDistrSample<Sam, VarId>::get_var_sample_pos(const std::wstring& var_name) const
{
	TWIST_CHECK_INVARIANT
	return sample_list_.get_pos(var_name);
}

template<typename Sam, typename VarId>
std::vector<const UnivarDistrSample<Sam>*> 
MultivarDistrSample<Sam, VarId>::get_var_csamples(const std::vector<std::wstring>& var_names) const
{
	TWIST_CHECK_INVARIANT
	std::vector<const UnivarDistrSample<Sam>*> samples;
	transform(var_names, std::back_inserter(samples), [&](const auto& var_name) {
		return &sample_list_.get(var_name);
	});
	return samples;
}

template<typename Sam, typename VarId>
std::vector<const UnivarDistrSample<Sam>*> MultivarDistrSample<Sam, VarId>::get_var_csamples() const
{
	TWIST_CHECK_INVARIANT
	std::vector<const UnivarDistrSample<Sam>*> samples;
	transform(sample_list_, std::back_inserter(samples), [](const auto& el) {
		return el.obj();
	});
	return samples;
}

template<typename Sam, typename VarId>
std::vector<UnivarDistrSample<Sam>*> MultivarDistrSample<Sam, VarId>::get_var_samples(
		const std::vector<std::wstring>& var_names)
{
	TWIST_CHECK_INVARIANT
	std::vector<UnivarDistrSample<Sam>*> samples;
	transform(var_names, std::back_inserter(samples), [&](const auto& var_name) {
		return &sample_list_.get(var_name);
	});
	return samples;
}

template<typename Sam, typename VarId>
std::vector<UnivarDistrSample<Sam>*> MultivarDistrSample<Sam, VarId>::get_var_samples()
{
	TWIST_CHECK_INVARIANT
	std::vector<UnivarDistrSample<Sam>*> samples;
	transform(sample_list_, std::back_inserter(samples), [](auto& el) {
		return el.obj();
	});
	return samples;
}

template<typename Sam, typename VarId>
void MultivarDistrSample<Sam, VarId>::delete_var_sample(VarId var_id)
{
	TWIST_CHECK_INVARIANT
	sample_list_.erase(var_id);
	TWIST_CHECK_INVARIANT
}

template<typename Sam, typename VarId>
std::unique_ptr<UnivarDistrSample<Sam>> MultivarDistrSample<Sam, VarId>::extract_var_sample(VarId var_id)
{
	TWIST_CHECK_INVARIANT
	std::unique_ptr<UnivarDistrSample<Sam>> sample( sample_list_.extract(var_id) );
	TWIST_CHECK_INVARIANT
	return sample;
}

template<typename Sam, typename VarId>
void MultivarDistrSample<Sam, VarId>::delete_all_var_samples()
{
	TWIST_CHECK_INVARIANT
	sample_list_.clear();
	TWIST_CHECK_INVARIANT
}

template<typename Sam, typename VarId>
void MultivarDistrSample<Sam, VarId>::delete_var_sample(const std::wstring& var_name)
{
	TWIST_CHECK_INVARIANT
	sample_list_.erase(var_name);
	TWIST_CHECK_INVARIANT
}

template<typename Sam, typename VarId>
std::wstring MultivarDistrSample<Sam, VarId>::get_var_name_at(Ssize pos) const
{
	TWIST_CHECK_INVARIANT
	const auto it = sample_list_.begin() + pos;
	return it->getName();
}

template<typename Sam, typename VarId>
Ssize MultivarDistrSample<Sam, VarId>::count_vars() const
{
	TWIST_CHECK_INVARIANT
	return twist::size32(sample_list_);
}

template<typename Sam, typename VarId>
std::vector<std::wstring> MultivarDistrSample<Sam, VarId>::get_var_names() const
{
	TWIST_CHECK_INVARIANT
	std::vector<std::wstring> var_names;
	twist::transform(sample_list_, std::back_inserter(var_names), [](const auto& el) {
		return el.name();
	});
	return var_names;
}

template<typename Sam, typename VarId>
bool MultivarDistrSample<Sam, VarId>::var_exists(const std::wstring& var_name) const
{
	TWIST_CHECK_INVARIANT
	return sample_list_.find(var_name) != nullptr;
}

template<typename Sam, typename VarId> template<typename Fn> 
void MultivarDistrSample<Sam, VarId>::transform(Fn transform)
{
	TWIST_CHECK_INVARIANT
	const Ssize num_vars = sample_list_.size();
	
	if (transform.get_row_size() != num_vars) {
		TWIST_THROW(L"The transform is defined for sample rows of the wrong size.");
	}
	
	std::vector<double> sample_row(num_vars);
	for (auto i = 0; i < size_; ++i) {
		
		for (auto j = 0; j < num_vars; ++j) {
			sample_row[j] = sample_list_.at(j).at(i);
		}

		transform(sample_row);

		for (auto j = 0; j < num_vars; ++j) {
			sample_list_.at(j).at(i) = convert_num<Sam>(sample_row[j]);
		}
	}
	
	TWIST_CHECK_INVARIANT
}

#ifdef _DEBUG
template<typename Sam, typename VarId>
void MultivarDistrSample<Sam, VarId>::check_invariant() const noexcept
{
	assert(size_ > 0);
}
#endif

}

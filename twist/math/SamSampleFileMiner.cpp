///  @file  SamSampleFileMiner.cpp
///  Implementation file for "SamSampleFileMiner.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "SamSampleFileMiner.hpp"

#include "../string_utils.hpp"

#include "UnivarDistrSample.hpp"
#include "numeric_utils.hpp"

namespace twist::math {

static const char  kCommentPrefix  = ';';
static const Ssize  k_line_buffer_len  = 4096;


SamSampleFileMiner::SamSampleFileMiner(const fs::path& path, 
		IsValidRandVarName is_valid_rand_var_name, ProgressProv& get_prog_prov) 
	: SampleFileMiner{ is_valid_rand_var_name, get_prog_prov }
	, path_{ path }
	, file_stream_{}
	, line_buffer_{ std::make_unique<char[]>(k_line_buffer_len) }
	, line_len_{ k_no_size }
{
	if (!exists(path_)) {
		TWIST_THROW(L"Sample file \"%s\" not found.", path_.c_str());
	}
	file_stream_.reset(new InTextFileStream<char>( std::make_unique<FileStd>(path_, FileOpenMode::read) ));
	TWIST_CHECK_INVARIANT
}


SamSampleFileMiner::~SamSampleFileMiner()
{
	TWIST_CHECK_INVARIANT
}


void SamSampleFileMiner::analyse()
{
	TWIST_CHECK_INVARIANT
	if (is_analysed()) {
		TWIST_THROW(L"The sample file has already been analysed.");
	}
	
	get_prog_prov().set_prog_msg(L"Analysing the sample file...");
	get_prog_prov().set_prog_range(0, 1);
	get_prog_prov().set_prog_pos(0);

	std::unique_ptr<std::vector<std::wstring>> randVarNames(new std::vector<std::wstring>());
	std::wstring varName;
	std::wstring varNameErr;
    
	std::stringstream valueConvertor;

    // Read information about the input, and then the output random variables.
    for (auto randVarType = 0; randVarType <= 1; ++randVarType) {
		
		// Read the number of input/output random variables.
		if (!read_next_valid_line_()) {
			TWIST_THROW(L"Invalid SAM file format: cannot read number of %s random variables.", 
					randVarType == 0 ? L"input" : L"output");
		}
		long numRandVars = 0;
		valueConvertor.clear();
		valueConvertor.str(line_buffer_.get());
		valueConvertor >> numRandVars;		
		if (valueConvertor.fail() || !valueConvertor.eof() || (numRandVars < 0)) {
			TWIST_THROW(L"Invalid SAM file format: wrong number of %s random variables.",
					randVarType == 0 ? L"input" : L"output", numRandVars);
		}
		else if ((randVarType == 0) && (numRandVars == 0)) {
			TWIST_THROW(L"Invalid SAM file format: the number of input random variables cannot be zero.");
		}

		// Read the names of the input/output random variables.
		for (long i = 0; i < numRandVars; ++i) {
			if (!read_next_valid_line_()) {
				TWIST_THROW(L"Invalid SAM file format: %s random variable names section incomplete.",
					randVarType == 0 ? L"input" : L"output");
			}
			// Check that the name obeys the random variable naming rules.
			varName.assign(line_buffer_.get(), line_buffer_.get() + line_len_);
			varName = trim_whitespace(varName);
			if (is_valid_rand_var_name(varName, varNameErr)) {
				// Name valid.
				randVarNames->push_back(varName);
			}
			else {
				TWIST_THROW(L"Invalid variable name \"%s\" in sample file: %s", varName.c_str(), varNameErr.c_str());
			}
		}
	}
	
    // Read the last line before the actual sample values. This line details how many sample values should be
    // per random variable.
	if (!read_next_valid_line_()) {
		TWIST_THROW(L"Invalid SAM file format: cannot read number of samples.");
	}
	long numSamples = 0;
	valueConvertor.clear();
	valueConvertor.str(line_buffer_.get());
	valueConvertor >> numSamples;
	if (valueConvertor.fail() || !valueConvertor.eof() || (numSamples <= 0)) {
		TWIST_THROW(L"Invalid SAM file format: wrong number of samples.");
	}
	
	get_prog_prov().set_prog_pos(1);

	set_general_sample_info(move(randVarNames), numSamples);
	TWIST_CHECK_INVARIANT
}


void SamSampleFileMiner::mine()
{
	TWIST_CHECK_INVARIANT
	if (!is_analysed()) {
		TWIST_THROW(L"The sample file has not been analysed yet.");
	}

	const auto numVars = ssize(get_rand_var_names());
	const auto numSamples = get_sample_size();

	get_prog_prov().set_prog_msg(L"Reading the sample values...");

	const Ssize kProgStep = 100;
	const auto numProgSteps = numSamples / kProgStep + 1;
	get_prog_prov().set_prog_range(1, numProgSteps); 
	get_prog_prov().set_prog_pos(0);
	
	// Prepare structure to hold the sample values.
	MultivarSample& sampleMatrix = prepare_multivar_sample_container();
	
	// Read the sample values.
	std::stringstream valueConvertor;
	double value = 0;

	Ssize varIdx = 0;
	Ssize sampleIdx = 0;
	Ssize sampleLineIdx = 1;
	while (read_next_valid_line_()) {
	
		valueConvertor.clear();
		valueConvertor.str(line_buffer_.get());
		valueConvertor >> value; 
		if (valueConvertor.fail() || !valueConvertor.eof()) {					
			TWIST_THROW(L"Invalid value \"%s\" on line %d in the sample file.", ansi_to_string(line_buffer_.get()).c_str(), sampleLineIdx);
		}

		// Store the value in the sample values structure.
		varIdx = (sampleLineIdx - 1) % numVars;
		sampleIdx = (sampleLineIdx - 1) / numVars;
		sampleMatrix.get_var_sample_at(varIdx).at(sampleIdx) = convert_num<float>(value);				
	
		sampleLineIdx++;
		
		if ((varIdx == 0) && (sampleIdx % kProgStep == 0)) { 
			get_prog_prov().inc_prog_pos();		
		}
	}

	if (sampleIdx != numSamples - 1) {
		// There are either less or mode sample lines than there should be.
		TWIST_THROW(L"Error reading sample file: invalid sample file format.");
	}

	TWIST_CHECK_INVARIANT
}


bool SamSampleFileMiner::read_next_valid_line_()
{
	TWIST_CHECK_INVARIANT
	bool lineIsGood = false;
		
	while (!lineIsGood && !file_stream_->is_eof()) {
		file_stream_->get_line(line_buffer_.get(), k_line_buffer_len - 1, line_len_);
		line_buffer_[line_len_] = '\0';
		// Trim whitespace from the beginning and end of the line.
		trim_whitespace(line_buffer_.get());
		const Ssize lineLen = strlen(line_buffer_.get());
		if (lineLen > 0) {
			// Check whether the whole line is a comment.
			if (line_buffer_[0] != kCommentPrefix) {
				// The line is valid. Check whether there is a comment at the end of it.
				char* commentPos = std::find(line_buffer_.get(), line_buffer_.get() + lineLen, kCommentPrefix);
				if (commentPos != line_buffer_.get() + lineLen) {
					// Zap the trailing comment and re-trim the string (in case there was some whitespace between the
					// end of the "valid" line and the comment).
					*commentPos = '\0';
					trim_whitespace(line_buffer_.get());
				}
				lineIsGood = true;
			}
		}
	}
	
	TWIST_CHECK_INVARIANT
	return lineIsGood;
}


#ifdef _DEBUG
void SamSampleFileMiner::check_invariant() const noexcept
{
	assert(!path_.empty());
	assert(line_buffer_);
}
#endif

} 

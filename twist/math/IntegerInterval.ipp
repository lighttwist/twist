///  @file  IntegerInterval.ipp
///  Implementation file for "IntegerInterval.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

namespace twist::math {

template<typename T>
IntegerInterval<T>::IntegerInterval(T lo, T hi) 
	: lo_(lo)
	, hi_(hi)
{
	if (lo_ > hi_) {
		TWIST_THROW(L"The bounds %f and %f of the interval are ill-specified.", lo_, hi_);
	}
	TWIST_CHECK_INVARIANT
}
	

template<typename T>
bool IntegerInterval<T>::operator==(const IntegerInterval& rhs) const 
{
	TWIST_CHECK_INVARIANT
	return (lo_ == rhs.lo_) && (hi_ == rhs.hi_);
}
	

template<typename T>
T IntegerInterval<T>::hi() const 
{
	TWIST_CHECK_INVARIANT
	return hi_;
} 
	

template<typename T>
T IntegerInterval<T>::lo() const 
{
	TWIST_CHECK_INVARIANT
	return lo_;
}


template<typename T>
T IntegerInterval<T>::length() const 
{
	TWIST_CHECK_INVARIANT
	return hi_ - lo_;
}
	

template<typename T>
bool IntegerInterval<T>::includes(const IntegerInterval& other) const 
{			
	TWIST_CHECK_INVARIANT
	return lo_ <= other.lo_ && other.hi_ <= hi_;
}


template<typename T>
bool IntegerInterval<T>::contains(T point) const 
{
	TWIST_CHECK_INVARIANT
	return lo_ <= point && point <= hi_;
}
	

template<typename T>
std::wstring IntegerInterval<T>::as_string() const 
{
	TWIST_CHECK_INVARIANT
	std::wstringstream text;
	text << L"[" << lo_ << L", " << hi_ << L"]";
	return text.str();
}


#ifdef _DEBUG
template<typename T>
void IntegerInterval<T>::check_invariant() const noexcept
{
	assert(lo_ <= hi_);
}
#endif 

// --- Global functions ---

template<typename T>
[[nodiscard]] auto enclose(T point, const IntegerInterval<T>& interv) -> T
{
	if (point < interv.lo()) {
		return interv.lo();
	}
	if (point > interv.hi()) {
		return interv.hi();
	}
	return point;
}

} 


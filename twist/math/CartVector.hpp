/// @file CartVector.hpp
/// CartVector class defintion

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_MATH_CART_VECTOR_HPP
#define TWIST_MATH_CART_VECTOR_HPP

namespace twist::math {

/*! A mathematical vector, with positive magnitude and in two dimensions, whose properties are stored as the orthogonal 
    projections of the vector on the X and Y axes (Cartesian, as opposed to polar coordinates).
 */
template<typename Coord>
class CartVector {
public:
	//! Constructor. Initialises to a vector with magnitude zero.
	CartVector() = default;

	/*! Constructor. 
	    \param[in] x_comp  The vector X-component
	    \param[in] y_comp  The vector Y-component
	 */
	CartVector(Coord x_comp, Coord y_comp) 
        : x_comp_{x_comp}
		, y_comp_{y_comp}
	{
	}
	
	//! The X-component of the vector, ie the orthogonal projection of the vector on the X-axis.
	[[nodiscard]] auto x_comp() const -> Coord 
	{
		return x_comp_;
	}

	//! The Y-component of the vector, ie the orthogonal projection of the vector on the Y-axis.
	[[nodiscard]] auto y_comp() const -> Coord 
	{ 
		return y_comp_;
	}

 	/*! Perform vector addition between this vector and another.
 	    \param[in] rhs  The other vector, to the right-hand-side of the operator
 	    \return  The resultant vector
	 */
    [[nodiscard]] auto operator+(const CartVector& rhs) const -> CartVector 
 	{
 		return {x_comp_ + rhs.x_comp_, y_comp + rhs.y_comp_};
 	}

private:
	Coord x_comp_;
	Coord y_comp_;
};

}

#endif

///  @file  Matrix.ipp
///  Inline implementation file for "Matrix.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

namespace twist::math {

template<typename T>
Matrix<T>::Matrix(Ssize num_rows, Ssize num_cols) 
	: row_count_{ num_rows }
	, col_count_{ num_cols }
{
	// Create the matrix structure.
	elements_ = new T*[row_count_];
	for (auto i = 0; i < row_count_; i++) {
		elements_[i] = new T[col_count_];
	}
	TWIST_CHECK_INVARIANT
}


template<typename T>
Matrix<T>::Matrix(const Matrix& src) 
	: row_count_{ src.row_count_ }
	, col_count_{ src.col_count_ }
{
	// Create the matrix structure.
	elements_ = new T*[row_count_];
	for (auto i = 0; i < row_count_; ++i) {
		elements_[i] = new T[col_count_];
	}
	assign(src);
	TWIST_CHECK_INVARIANT
}

	
template<typename T>
Matrix<T>::~Matrix()
{
	TWIST_CHECK_INVARIANT
	// Destroy the matrix structure.
	for (auto i = 0; i < row_count_; ++i) {
		delete[] elements_[i];	
	}
	delete[] elements_;
}


template<typename T>
void Matrix<T>::assign(const Matrix& src)
{
	TWIST_CHECK_INVARIANT
	if ((row_count_ != src.row_count_) || (col_count_ != src.col_count_)) {
		TWIST_THROW(L"The two matrices must have the same number of rows and the same number of columns");
	}
	// Copy matrix contents across.
	for (auto i = 0; i < row_count_; i++) {
		memcpy(elements_[i], src.elements_[i], col_count_ * sizeof(T));
	}	
	TWIST_CHECK_INVARIANT
}


template<typename T>
void Matrix<T>::assign(const SymmetricMatrix<T>& src)
{
	TWIST_CHECK_INVARIANT
	const auto size = src.size();
	if ((row_count_ != size) || (col_count_ != size)) {
		TWIST_THROW(L"The number of rows and columns of this matrix must equal the size of the symmetric matrix.");
	}
	// Copy matrix contents across.
	for (auto i = 0; i < size; i++) {
		for (auto j = 0; j < size; j++) {
			elements_[i][j] = src[i][j];
		}
	}	
	TWIST_CHECK_INVARIANT
}


template<typename T>
inline typename Matrix<T>::SubscriptProxyConst Matrix<T>::operator[](Ssize row) const
{
	TWIST_CHECK_INVARIANT
	return SubscriptProxyConst(this, row);
}


template<typename T>
inline typename Matrix<T>::SubscriptProxy Matrix<T>::operator[](Ssize row)
{
	TWIST_CHECK_INVARIANT
	return SubscriptProxy(this, row);
}


template<typename T>
const T& Matrix<T>::elem(Ssize row, Ssize col) const
{
	TWIST_CHECK_INVARIANT
	if (row >= row_count_) {
		TWIST_THROW(L"Row subscript out of range");
	}
	if (col >= col_count_) {
		TWIST_THROW(L"Column subscript out of range");
	}
	return elements_[row][col];
}


template<typename T>
T& Matrix<T>::elem(Ssize row, Ssize col)
{
	TWIST_CHECK_INVARIANT
	if (row >= row_count_) {
		TWIST_THROW(L"Row subscript out of range");
	}
	if (col >= col_count_) {
		TWIST_THROW(L"Column subscript out of range");
	}
	TWIST_CHECK_INVARIANT
	return elements_[row][col];
}


template<typename T>
Ssize Matrix<T>::nof_rows() const
{
	TWIST_CHECK_INVARIANT
	return row_count_;
}


template<typename T>
Ssize Matrix<T>::nof_columns() const
{
	TWIST_CHECK_INVARIANT
	return col_count_;
}


template<typename T>
void Matrix<T>::make_zero()
{
	TWIST_CHECK_INVARIANT
	for (auto i = 0; i < row_count_; i++) {
		for (auto j = 0; j < col_count_; j++) {
			elements_[i][j] = 0;
		}
	}	
	TWIST_CHECK_INVARIANT
}


template<typename T>
void Matrix<T>::multiply(const Matrix<T>& right, Matrix<T>& result) const
{
	TWIST_CHECK_INVARIANT
    if (col_count_ != right.row_count_)  {
        TWIST_THROW(L"The number of rows in 'right' must equal the the number of columns in this matrix");
    }
    if ((result.row_count_ != row_count_) || (result.col_count_ != right.col_count_))  {
        TWIST_THROW(L"'result' must have the same number of rows as this matrix and the same number of columns as 'right'");
    }

	long double val = 0;
    for (auto ires = 0; ires < row_count_; ++ires) {
        for (auto jres = 0; jres < right.col_count_; ++jres)  {
            val = 0;
            for (auto k = 0; k < col_count_; k++) {
                val += elements_[ires][k] * right.elements_[k][jres];
            }
            result.elements_[ires][jres] = static_cast<T>(val);
        }
    }
}


template<typename T>
void Matrix<T>::subtract(const Matrix<T>& right, Matrix<T>& result) const
{
	TWIST_CHECK_INVARIANT
    if ((row_count_ != right.row_count_) || (row_count_ != result.row_count_) || 
			(col_count_ != right.col_count_) || (col_count_ != result.col_count_)) {
        TWIST_THROW(L"All three matrices must have the same number of rows and the same number of columns");
    }

    for (auto i = 0; i < row_count_; i++) {
        for (auto j = 0; j < col_count_; j++) {
            result.elements_[i][j] = elements_[i][j] - right.elements_[i][j];
        }
    }
}


template<typename T>
double Matrix<T>::determinant() const
{
	TWIST_CHECK_INVARIANT
    if (row_count_ != col_count_) {
        TWIST_THROW(L"The matrix must be square.");
    }

	double determinant = 0.0;
    const auto size = row_count_;

    Matrix<T> lu(size, size);
    lu.assign(*this);
     
    std::vector<Ssize> indx(size);

	short sign = 1;
    lu.lu_decomposition(indx, sign);
 
    determinant = sign;
    for (auto j = 0; j < size; ++j) {
        determinant *= lu.elements_[j][j];
    }
   
    return determinant;
}


template<typename T>
bool Matrix<T>::determinant_and_inverse(double& determinant, Matrix<T>& inv) const
{
	TWIST_CHECK_INVARIANT
	static const double determinant_tolerance = 1.0E-20;

    if ((row_count_ != col_count_) || (inv.row_count_ != inv.col_count_))  {
        TWIST_THROW(L"Both this matrix and 'inv' must be square");
    }
    if (row_count_ != inv.row_count_) {
        TWIST_THROW(L" This matrix and 'inv' must have the same size");
    }
    const auto size = row_count_;

    bool ret = false;

    Matrix<T> lu(size, size);
    lu.assign(*this);
    
    std::vector<Ssize> indx(size);
    std::vector<T> col(size);

	short sign = 1;
    lu.lu_decomposition(indx, sign);
 
    determinant = sign;
    for (auto j = 0; j < size; j++) {
        determinant *= lu.elements_[j][j];
    }

    if (abs(determinant) >= determinant_tolerance) {
        // Find inverse by columns.
        for (auto j = 0; j < size; ++j) {
            for (auto i = 0; i < size; ++i) {
                col[i] = 0.0;
            }
            col[j] = 1.0;
            lu.lu_backsubst(indx, col);
            for (auto i = 0; i < size; ++i) {
                inv.elements_[i][j] = col[i];
            }
        }
        ret = true;
    }

    return ret;
}


template<typename T>
void Matrix<T>::lu_decomposition(std::vector<Ssize>& indx, short& sign)
{
	TWIST_CHECK_INVARIANT
	static const double tiny_number = 1.0E-20;  // A very small number.

    if (row_count_ != col_count_) {
        TWIST_THROW(L"The matrix must be square");
    }
	const Ssize n = row_count_;

    if (ssize(indx) != n)  {
        TWIST_THROW(L"The index array passed in must have the same size as the square matrix");
    }

	std::vector<double> vv(n);  // vv stores the implicit scaling of each row

    sign = 1;
    Ssize imax = 0;
    double big = 0.0;
    double temp = 0.0;   

    for (auto i = 0; i < n; i++) {
        big = 0.0;        
        for (auto j = 0; j < n; j++) {
            temp = ::abs(elements_[i][j]);
            if (temp > big)  {
                big = temp;
            }
        }        
        if (big == 0.0)  {
            // No non-zero largest element
            TWIST_THROW(L"This is a singular matrix");
        }        
		vv[i] = 1.0 / big;  // Save the scaling
    }
    
    double sum = 0.0;
    double dum = 0.0;
    
    for (auto j = 0; j < n; ++j) {  // This is the loop over columns of Crout’s method.
        for (auto i = 0; i < j; ++i)  {   // This is equation (2.3.12) except for i = j.
            sum = elements_[i][j];
            for (auto k = 0; k < i; ++k) {
                sum = sum - elements_[i][k] * elements_[k][j];
            }
            elements_[i][j] = static_cast<T>(sum);
        }

        big = 0.0;  // Initialize for the search for largest pivot element.  
        for (auto i = j; i < n; ++i) {  // This is  i = j  of equation (2.3.12) and  i = j+1 . . . N  of equation (2.3.13)
            sum = elements_[i][j];
            for (auto k = 0; k < j; ++k) {
                sum = sum - elements_[i][k] * elements_[k][j];
            }
			elements_[i][j] = static_cast<T>(sum);

            dum = vv[i] * ::abs(sum);
			if (dum >= big) {
				// Is the figure of merit for the pivot better than the best so far?
				big = dum;
				imax = i;
			}            
        }

        if (j != imax) {  // Do we need to interchange rows?
            for (auto k = 0; k < n; ++k) {  // Yes,  so...
				dum = elements_[imax][k];
				elements_[imax][k] = elements_[j][k];
				elements_[j][k] = static_cast<T>(dum);
            }
            sign = -sign;         // ...and change the parity of the sign
            vv[imax] = vv[j];     // Also interchange the scale factor
        }

		indx[j] = imax;
		if (elements_[j][j] == 0.0)  {
            elements_[j][j] = static_cast<T>(tiny_number);
        }

		// If the pivot element is zero the matrix is singular (at least to the precision of the algorithm). For some
        // applications on singular matrices, it is desirable to substitute 'tiny_number' for zero.

		if (j != n) {  // Now, finally, divide by the pivot element.
			dum = 1.0 / elements_[j][j];
			for (auto i = j + 1; i < n; ++i) {
                elements_[i][j] = static_cast<T>(elements_[i][j] * dum);
            }
		}
	}  // Go back for the next column in the reduction.
}


template<typename T>
void Matrix<T>::lu_backsubst(const std::vector<Ssize>& indx, std::vector<T>& b) const
{
	TWIST_CHECK_INVARIANT
    if (row_count_ != col_count_) {
        TWIST_THROW(L"The matrix must be square");
    }
    if (ssize(indx) != row_count_ || ssize(b) != row_count_)  {
        TWIST_THROW(L"The arrays passed in must have the same size as the square matrix");
    }

	const long n = static_cast<long>(row_count_);
    long ii = -1;
    Ssize ip = 0;
   
    double sum = 0.0;
    
    for (long i = 0; i < n; i++) {
        // When 'ii' is set to a positive value, it will become the index of the first nonvanishing element of 'b'.
        // We now  the forward substitution, equation (2.3.6). The only new wrinkle is to unscramble the permutation
        // as we go.
        ip = indx[i];
		sum = b[ip];
		b[ip] = b[i];
		if (ii > -1) {
            for (long j = ii; j < i; j++) {
			    sum -= elements_[i][j] * b[j];
            }
        }
		else if (sum != 0) {
            // A non-zero element was encountered, so from now on we will have to do the sums in the loop above.
			ii = i;
        }
		b[i] = static_cast<T>(sum);
    }

    for (long i = n - 1; i >= 0; i--) {
	    // Now we do the backsubstitution, equation (2.3.7).
		sum = b[i];
		for (long j = i + 1; j < n; j++) {
            sum -= elements_[i][j] * b[j];
        }
		b[i] = static_cast<T>(sum / elements_[i][i]);  // Store a component of the solution vector X.
	}   
}


template<typename T>
bool Matrix<T>::is_symmetric(T tolerance) const
{
	TWIST_CHECK_INVARIANT
	bool ret = true;
	
	if (row_count_ == col_count_) {
		for (auto i = 0; i < row_count_; ++i) {		
			for (auto j = 0; j < i; j++) {
				if (abs(elements_[i][j] - elements_[j][i]) > tolerance) {
					ret = false;			
					break;
				}	
			}
			if (!ret) {
				break;
			}			
		}
	}
	
	return ret;
}


template<typename T>
void Matrix<T>::get_lower_triangle(LowerTriangularMatrix<T>& lt_matrix) const
{
	TWIST_CHECK_INVARIANT
	if (row_count_ != col_count_) {
		TWIST_THROW(L"The matrix must be square");
	}
	if (lt_matrix.Size() != row_count_) {
		TWIST_THROW(L"'lt_matrix' must have the same size as this matrix");
	}
	for (auto i = 0; i < row_count_; i++) {
		for (auto j = 0; j <= i; j++) { 
			lt_matrix[i][j] = elements_[i][j];
		}
	}
}


template<typename T>
void Matrix<T>::as_vector(std::vector<T>& vector) const
{
	TWIST_CHECK_INVARIANT
	if ((row_count_ != 1) && (col_count_ != 1)) {
		TWIST_THROW(L"The matrix has to have either one row or one column");
	}
	const bool transposed = (col_count_ == 1);
	const Ssize size = transposed ? row_count_ : col_count_;
	if (ssize(vector) != size) {
		TWIST_THROW(L"The vector passed in has the wrong size");
	}
	for (auto i = 0; i < size; i++) {
		if (!transposed) {
			vector[i] = elements_[0][i];
		}
		else {
			vector[i] = elements_[i][0];		
		}
	}
}


#ifdef _DEBUG
template<typename T>
void Matrix<T>::check_invariant() const noexcept
{
	assert(elements_);
	assert(row_count_ > 0);
	assert(col_count_ > 0);
}
#endif  

//
//  Matrix::SubscriptProxyConst class    
//

template<typename T>
inline Matrix<T>::SubscriptProxyConst::SubscriptProxyConst(const Matrix<T>* matrix, Ssize row) 
	: matrix_{ matrix }
	, row_{ row }
{
}


template<typename T>
inline const T& Matrix<T>::SubscriptProxyConst::operator[](Ssize col) const
{
	return matrix_->elements_[row_][col];
}

//
//  Matrix::SubscriptProxy class    
//

template<typename T>
inline Matrix<T>::SubscriptProxy::SubscriptProxy(Matrix<T>* matrix, Ssize row) 
	: matrix_{ matrix }
	, row_{ row }
{
}


template<typename T>
inline T& Matrix<T>::SubscriptProxy::operator[](Ssize col)
{
	return matrix_->elements_[row_][col];
}

} 


///  @file  LowerTriangularMatrix.ipp
///  Inline implementation file for "LowerTriangularMatrix.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "numeric_utils.hpp"

namespace twist::math {

template<typename T>
LowerTriangularMatrix<T>::LowerTriangularMatrix(Ssize size) 
	: size_(size)
{
	// Create the triangular matrix structure.
	elements_ = new T*[size_];
	for (auto i = 0; i < size_; i++) {
		elements_[i] = new T[i + 1];
	}
	TWIST_CHECK_INVARIANT
}


template<typename T>
LowerTriangularMatrix<T>::LowerTriangularMatrix(const LowerTriangularMatrix& src) 
	: size_(src.size_)
{
	// Create the triangular matrix structure
	elements_ = new T*[size_];
	for (auto i = 0; i < size_; i++) {
		elements_[i] = new T[i + 1];
	}
	assign(src);
	TWIST_CHECK_INVARIANT
}


template<typename T>
LowerTriangularMatrix<T>::~LowerTriangularMatrix()
{
	TWIST_CHECK_INVARIANT
	// Destroy the triangular structure
	for (auto i = 0; i < size_; i++) {
		delete[] elements_[i];	
	}
	delete[] elements_;
}


template<typename T>
void LowerTriangularMatrix<T>::assign(const LowerTriangularMatrix& src)
{
	TWIST_CHECK_INVARIANT
	if (src.size_ != size_) {
		TWIST_THROW(L"The two matrices must have the same size");
	}
	// Copy matrix contents across
	for (auto i = 0; i < size_; i++) {
		memcpy(elements_[i], src.elements_[i], (i + 1) * sizeof(T));
	}
	TWIST_CHECK_INVARIANT
}


template<typename T>
inline typename LowerTriangularMatrix<T>::SubscriptProxyConst 
LowerTriangularMatrix<T>::operator[](Ssize row) const
{
	TWIST_CHECK_INVARIANT
	return SubscriptProxyConst(this, row);
}


template<typename T>
inline typename LowerTriangularMatrix<T>::SubscriptProxy LowerTriangularMatrix<T>::operator[](Ssize row)
{
	TWIST_CHECK_INVARIANT
	return SubscriptProxy(this, row);
}


template<typename T>
const T& LowerTriangularMatrix<T>::elem(Ssize row, Ssize col) const
{
	TWIST_CHECK_INVARIANT
	if (row > col) {
		if (row >= size_) {
			TWIST_THROW(L"Row subscript out of range");
		}
		return elements_[row][col];
	}
	else {
		if (col >= size_) {
			TWIST_THROW(L"Column subscript out of range");
		}
		return elements_[col][row];	
	}
}


template<typename T>
T& LowerTriangularMatrix<T>::elem(Ssize row, Ssize col)
{
	TWIST_CHECK_INVARIANT
	if (row > col) {
		if (row >= size_) {
			TWIST_THROW(L"Row subscript out of range");
		}
		return elements_[row][col];
	}
	else {
		if (col >= size_) {
			TWIST_THROW(L"Column subscript out of range");
		}
		return elements_[col][row];	
	}
}


template<typename T>
Ssize LowerTriangularMatrix<T>::size() const
{
	TWIST_CHECK_INVARIANT
	return size_;
}


template<typename T>
Ssize LowerTriangularMatrix<T>::nof_rows() const
{
	TWIST_CHECK_INVARIANT
	return size_;
}


template<typename T>
Ssize LowerTriangularMatrix<T>::nof_columns() const
{
	TWIST_CHECK_INVARIANT
	return size_;
}


template<typename T>
void LowerTriangularMatrix<T>::make_zero()
{
	TWIST_CHECK_INVARIANT
	for (auto i = 0; i < size_; i++) {
		for (auto j = 0; j <= i; j++) {
			elements_[i][j] = 0;
		}
	}
	TWIST_CHECK_INVARIANT
}


template<typename T>
void LowerTriangularMatrix<T>::set_diagonal(const T& value)
{
	TWIST_CHECK_INVARIANT
	for (auto i = 0; i < size_; i++) {
		elements_[i][i] = value;
	}
	TWIST_CHECK_INVARIANT
}


template<typename T>
void LowerTriangularMatrix<T>::multiply(const std::vector<T>& vector, std::vector<T>& result_vector) const
{
    if (ssize(vector) != size_ || ssize(result_vector) != size_) {
        TWIST_THROW(L"The vectors must have the same size as the matrix.");
    }

    for (auto i = 0; i < size_; ++i) {
        double sum = 0;
        for (auto j = 0; j <= i; ++j) {
			sum += elements_[i][j] * vector[j];
        }
        result_vector[i] = twist::math::convert_num<T>(sum);
    }
}
    
	      
#ifdef _DEBUG
template<typename T>
void LowerTriangularMatrix<T>::check_invariant() const noexcept
{
	assert(elements_);
	assert(size_ > 0);
}
#endif 

//
//  LowerTriangularMatrix::SubscriptProxyConst class    
//

template<typename T>
LowerTriangularMatrix<T>::SubscriptProxyConst::SubscriptProxyConst(const LowerTriangularMatrix<T>* matrix,
		Ssize row) 
	: matrix_{ matrix }
	, row_{ row }
{
}


template<typename T>
inline const T& LowerTriangularMatrix<T>::SubscriptProxyConst::operator[](Ssize col) const
{
	if (row_ > col) {
		return matrix_->elements_[row_][col];
	}
	return matrix_->elements_[col][row_];	
}

//
//  LowerTriangularMatrix::SubscriptProxy class    
//

template<typename T>
LowerTriangularMatrix<T>::SubscriptProxy::SubscriptProxy(LowerTriangularMatrix<T>* matrix, Ssize row) 
	: matrix_{ matrix }
	, row_{ row }
{
}


template<typename T>
inline T& LowerTriangularMatrix<T>::SubscriptProxy::operator[](Ssize col)
{
	if (row_ > col) {
		return matrix_->elements_[row_][col];
	}
	return matrix_->elements_[col][row_];	
}

}  

///  @file  SampleFileMiner.cpp
///  Implementation file for "SampleFileMiner.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "SampleFileMiner.hpp"

#include "UnivarDistrSample.hpp"
#include "sample_utils.hpp"

namespace twist::math {

SampleFileMiner::SampleFileMiner(IsValidRandVarName is_valid_rand_var_name, ProgressProv& prog_prov)
	: rand_var_names_()
	, sample_size_(0)
	, multivar_sample_()
	, is_valid_rand_var_name_(is_valid_rand_var_name) 
	, prog_prov_(prog_prov)
{
	TWIST_CHECK_INVARIANT
}


SampleFileMiner::~SampleFileMiner()
{
	TWIST_CHECK_INVARIANT
}


bool SampleFileMiner::is_analysed() const
{
	TWIST_CHECK_INVARIANT
	return rand_var_names_.get() != nullptr;
}


bool SampleFileMiner::is_mined() const
{
	TWIST_CHECK_INVARIANT
	return multivar_sample_.get() != nullptr;
}


const std::vector<std::wstring>& SampleFileMiner::get_rand_var_names() const
{
	TWIST_CHECK_INVARIANT
	if (!is_analysed()) {
		TWIST_THROW(L"The variable names have not yet been read from the sample file. You must call  analyse()  first.");
	}
	return *rand_var_names_;
}


Ssize SampleFileMiner::get_sample_size() const
{
	TWIST_CHECK_INVARIANT
	return sample_size_;
}


const UnivarDistrSample<float>& SampleFileMiner::get_sample_for_rand_var(const std::wstring& rand_var_name) const
{
	TWIST_CHECK_INVARIANT
	if (!is_mined()) {
		TWIST_THROW(L"The sample file has not yet been mined.");
	}
	return multivar_sample_->get_var_sample(rand_var_name);
}


std::vector<const UnivarDistrSample<float>*> SampleFileMiner::get_samples_for_rand_vars(
		const std::set<std::wstring>& rand_var_names) const
{
	TWIST_CHECK_INVARIANT
	return twist::transform_to_vector(rand_var_names, [&](const std::wstring& el) {
		return &get_sample_for_rand_var(el);
	});
}


std::unique_ptr<SampleFileMiner::MultivarSample> SampleFileMiner::release_multivar_sample()
{
	TWIST_CHECK_INVARIANT
	rand_var_names_.reset();
	sample_size_ = 0;
	return move(multivar_sample_);
}


void SampleFileMiner::set_general_sample_info(std::unique_ptr<std::vector<std::wstring>> rand_var_names, 
		Ssize sample_size)
{
	TWIST_CHECK_INVARIANT
	if (rand_var_names_ || sample_size_ != 0) {
		TWIST_THROW(L"The general sample info has already been set.");
	}
	rand_var_names_ = move(rand_var_names);
	sample_size_ = sample_size;
	TWIST_CHECK_INVARIANT
}


SampleFileMiner::MultivarSample& SampleFileMiner::prepare_multivar_sample_container()
{
	TWIST_CHECK_INVARIANT
	multivar_sample_ = std::make_unique<MultivarSample>(sample_size_);

	size_t rand_var_id = 0;
	for (const auto& el : *rand_var_names_) {
		multivar_sample_->create_var_sample(rand_var_id++, el);
	}
	TWIST_CHECK_INVARIANT
	return *multivar_sample_;
}


bool SampleFileMiner::is_valid_rand_var_name(const std::wstring& name, std::wstring& err) const
{
	TWIST_CHECK_INVARIANT
	return is_valid_rand_var_name_(name, err);
}


ProgressProv& SampleFileMiner::get_prog_prov()
{
	TWIST_CHECK_INVARIANT
	return prog_prov_;
}


#ifdef _DEBUG
void SampleFileMiner::check_invariant() const noexcept
{
	assert(is_valid_rand_var_name_);

	if (rand_var_names_) {
		assert(sample_size_ > 0);
		if (multivar_sample_) {
			assert(ssize(*rand_var_names_) == multivar_sample_->count_vars());
		}
	}
	else {
		assert(sample_size_ == 0);
		assert(!multivar_sample_);
	}

	if (multivar_sample_) {
		assert(rand_var_names_);
	}
}
#endif

} 


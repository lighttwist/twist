/// @file RowwiseMatrix.ipp
/// Inline implementation file for "RowwiseMatrix.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/IndexRange.hpp"
#include "twist/std_vector_utils.hpp"
#include "RowwiseMatrix.hpp"

namespace twist::math {

// --- RowwiseMatrix class template ---

template<class T>
RowwiseMatrix<T>::RowwiseMatrix()
{
	TWIST_CHECK_INVARIANT
}

template<class T>
auto RowwiseMatrix<T>::clone() const -> RowwiseMatrix
{
	TWIST_CHECK_INVARIANT
	auto clone = RowwiseMatrix{};
	clone.nof_cols_ = nof_cols_;
	clone.rows_ = rows_;
	return clone;
}

template<class T>
auto RowwiseMatrix<T>::nof_rows() const -> Ssize
{
	TWIST_CHECK_INVARIANT
	return ssize(rows_);
}

template<class T>
auto RowwiseMatrix<T>::nof_columns() const -> Ssize
{
	TWIST_CHECK_INVARIANT
	return nof_cols_;
}

template<class T>
auto RowwiseMatrix<T>::nof_cells() const -> Ssize
{
	TWIST_CHECK_INVARIANT
	return ssize(rows_) * nof_cols_;
}

template<class T>
auto RowwiseMatrix<T>::operator()(Ssize row_idx, Ssize col_idx) const -> const T&
{
	TWIST_CHECK_INVARIANT
	return rows_[row_idx][col_idx];
}

template<class T>
auto RowwiseMatrix<T>::operator()(Ssize row_idx, Ssize col_idx) -> T&
{
	TWIST_CHECK_INVARIANT
	return rows_[row_idx][col_idx];
}

template<class T>
auto RowwiseMatrix<T>::get_cell(Ssize row_idx, Ssize col_idx) const -> const T&
{
	TWIST_CHECK_INVARIANT
	if (row_idx >= ssize(rows_)) {
		TWIST_THROW(L"Invalid row index %lld.", row_idx);
	}
	if (col_idx >= nof_cols_) {
		TWIST_THROW(L"Invalid column index %lld.", col_idx);
	}	
	return rows_[row_idx][col_idx];
}

template<class T>
auto RowwiseMatrix<T>::set_cell(Ssize row_idx, Ssize col_idx, T value) -> void
{
	TWIST_CHECK_INVARIANT
	if (row_idx >= ssize(rows_)) {
		TWIST_THROW(L"Invalid row index %d.", row_idx);
	}
	if (col_idx >= nof_cols_) {
		TWIST_THROW(L"Invalid column index %d.", col_idx);
	}
	rows_[row_idx][col_idx] = std::move(value);
}

template<class T>
auto RowwiseMatrix<T>::add_row(std::vector<T> row_values) -> Ssize
{
	TWIST_CHECK_INVARIANT
	if (nof_cols_ == 0) {
		// The matrix is empty
		if (row_values.empty()) {
			TWIST_THROW(L"Cannot add an empty row.");
		}
		nof_cols_ = ssize(row_values);
	}
	else if (ssize(row_values) != nof_cols_) {
		TWIST_THROW(L"The number %lld of new row values does not match the number of columns %lld in the matrix.",
					ssize(row_values), nof_cols_);
	}

	rows_.push_back(move(row_values));
	return ssize(rows_) - 1;
}

template<class T>
auto RowwiseMatrix<T>::get_row(Ssize row_idx) const -> const std::vector<T>&
{
	TWIST_CHECK_INVARIANT
	if (row_idx >= ssize(rows_)) {
		TWIST_THROW(L"Invalid row index %d.", row_idx);
	}
	return rows_[row_idx];
}

template<class T>
auto RowwiseMatrix<T>::remove_row(Ssize row_idx) -> void
{
	TWIST_CHECK_INVARIANT
	if (row_idx >= ssize(rows_)) {
		TWIST_THROW(L"Invalid row index %d.", row_idx);
	}
	rows_.erase(begin(rows_) + row_idx);
	if (rows_.empty()) {
		// The last row has been removed. The matrix is empty.
		nof_cols_ = 0;
	}
}

template<class T>
auto RowwiseMatrix<T>::insert_column(Ssize col_idx, const std::vector<T>& col_values) -> void
{
	TWIST_CHECK_INVARIANT
	if (col_values.empty()) {
		TWIST_THROW(L"Cannot add an empty column.");
	}
	if (col_idx > nof_cols_) {
		TWIST_THROW(L"The index %lld of the new column is larger that the number of columns %lld.",
		            col_idx, nof_cols_);
	}

	const auto nof_rows = ssize(rows_);
	if (nof_rows == 0) {
		// The matrix is empty
		for (auto i : IndexRange{ssize(col_values)}) {
			rows_.push_back(std::vector<T>{col_values[i]});
		}
	}
	else {
		if (ssize(col_values) != nof_rows) {
			TWIST_THROW(L"The number %lld of new column values does not match the number of rows %lld in the matrix.",
						ssize(col_values), nof_rows);
		}
		for (auto i : IndexRange{nof_rows}) {
			auto& row = rows_[i];
			row.insert(begin(row) + col_idx, col_values[i]);
		}
	}
	++nof_cols_;
}

template<class T>
auto RowwiseMatrix<T>::remove_column(Ssize col_idx) -> void
{
	TWIST_CHECK_INVARIANT
	if (col_idx >= nof_cols_) {
		TWIST_THROW(L"Invalid column index %d.", col_idx);
	}
	if (nof_cols_ == 1) {
		// The last column is being removed. The matrix is empty.
		rows_.clear();
	}
	else {
		for (auto& row : rows_) {
			row.erase(begin(row) + col_idx);
		}	
	}
	--nof_cols_;
}

template<class T>
auto RowwiseMatrix<T>::extract_column(Ssize col_idx) -> std::vector<T>
{
	TWIST_CHECK_INVARIANT
	if (col_idx >= nof_cols_) {
		TWIST_THROW(L"Invalid column index %d.", col_idx);
	}

	auto col_values = reserve_vector<T>(ssize(rows_));
	for (auto& row : rows_) {
		const auto value_it = begin(row) + col_idx;
		col_values.push_back(*value_it);
		if (nof_cols_ > 1) {
			row.erase(value_it);
		}
	}	

	--nof_cols_;
	if (nof_cols_ == 0) {
		// The last column is being removed. The matrix is empty.
		rows_.clear();
	}

	return col_values;
}

#ifdef _DEBUG
template<class T>
auto RowwiseMatrix<T>::check_invariant() const noexcept -> void
{
	assert(nof_cols_ >= 0);
	assert(rg::all_of(rows_, [this](const auto& row) { return ssize(row) == nof_cols_; }));
}
#endif 

} 

///  @file  UnivarDistrSampleInfo.ipp
///  Inline implementation file for "UnivarDistrSampleInfo.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

namespace twist::math {

template<typename TSam>
std::unique_ptr<UnivarDistrSampleInfo<TSam>> 
UnivarDistrSampleInfo<TSam>::create_cont_sample_info(size_t size, TSam min_val, TSam max_val, double mean, double stddev)
{
	return std::unique_ptr<UnivarDistrSampleInfo>( new UnivarDistrSampleInfo(size, min_val, max_val, mean, stddev) );
}


template<typename TSam>
std::unique_ptr<UnivarDistrSampleInfo<TSam>> 
UnivarDistrSampleInfo<TSam>::create_discr_sample_info(size_t size, TSam min_val, TSam max_val, double mean, double stddev, 
		const SamSet& distinct_discr_values)
{
	std::unique_ptr<UnivarDistrSampleInfo> sample_info( new UnivarDistrSampleInfo(size, min_val, max_val, mean, stddev) );
	sample_info->distinct_discr_values_ = std::make_unique<SamSet>(distinct_discr_values);
	return sample_info;
}


template<typename TSam>
UnivarDistrSampleInfo<TSam>::UnivarDistrSampleInfo(size_t size, TSam minVal, TSam maxVal, double mean, double stddev) 
	: size_(size)
    , min_val_(minVal)
    , max_val_(maxVal)
    , mean_(mean)
    , stddev_(stddev)
    , distinct_discr_values_()
{
	TWIST_CHECK_INVARIANT
}


template<typename TSam>
UnivarDistrSampleInfo<TSam>::UnivarDistrSampleInfo(const UnivarDistrSampleInfo& src) 
    : size_(src.size_)
    , min_val_(src.min_val_)
    , max_val_(src.max_val_)
    , mean_(src.mean_)
    , stddev_(src.stddev_)
    , distinct_discr_values_()
{
	if (src.distinct_discr_values_) {
		distinct_discr_values_ = std::make_unique<SamSet>(*src.distinct_discr_values_);
	}
	TWIST_CHECK_INVARIANT
}


template<typename TSam>
UnivarDistrSampleInfo<TSam>::UnivarDistrSampleInfo(UnivarDistrSampleInfo&& src) 
    : size_(src.size_)
    , min_val_(src.min_val_)
    , max_val_(src.max_val_)
    , mean_(src.mean_)
    , stddev_(src.stddev_)
    , distinct_discr_values_(move(src.distinct_discr_values_))
{
	TWIST_CHECK_INVARIANT
}


template<typename TSam>
UnivarDistrSampleInfo<TSam>::~UnivarDistrSampleInfo()
{
	TWIST_CHECK_INVARIANT
}


template<typename TSam>
void UnivarDistrSampleInfo<TSam>::operator=(const UnivarDistrSampleInfo& rhs)
{
	TWIST_CHECK_INVARIANT
	if (&rhs != this) {
		size_ = rhs.size_;
		min_val_ = rhs.min_val_;
		max_val_ = rhs.max_val_;
		mean_ = rhs.mean_;
		stddev_ = rhs.stddev_;
		distinct_discr_values_.reset();
		if (rhs.distinct_discr_values_) {
			distinct_discr_values_ = std::make_unique<SamSet>(*rhs.distinct_discr_values_);	
		}
	}
	TWIST_CHECK_INVARIANT
}


template<typename TSam>
void UnivarDistrSampleInfo<TSam>::operator=(UnivarDistrSampleInfo&& rhs)
{
	TWIST_CHECK_INVARIANT
	if (&rhs != this) {
		size_ = rhs.size_;
		min_val_ = rhs.min_val_;
		max_val_ = rhs.max_val_;
		mean_ = rhs.mean_;
		stddev_ = rhs.stddev_;
		distinct_discr_values_ = move(rhs.distinct_discr_values_);	
	}
	TWIST_CHECK_INVARIANT
}


template<typename TSam>
size_t UnivarDistrSampleInfo<TSam>::size() const
{
	TWIST_CHECK_INVARIANT
	return size_;
}


template<typename TSam>
TSam UnivarDistrSampleInfo<TSam>::min() const
{
	TWIST_CHECK_INVARIANT
	return min_val_;
}


template<typename TSam>
TSam UnivarDistrSampleInfo<TSam>::max() const
{
	TWIST_CHECK_INVARIANT
	return max_val_;
}


template<typename TSam>
double UnivarDistrSampleInfo<TSam>::mean() const
{
	TWIST_CHECK_INVARIANT
	return mean_;
}


template<typename TSam>
double UnivarDistrSampleInfo<TSam>::stddev() const
{
	TWIST_CHECK_INVARIANT
	return stddev_;
}


template<typename TSam>
bool UnivarDistrSampleInfo<TSam>::is_discrete() const
{
	TWIST_CHECK_INVARIANT
	return distinct_discr_values_.get() != nullptr;
}


template<typename TSam>
const typename UnivarDistrSampleInfo<TSam>::SamSet& 
UnivarDistrSampleInfo<TSam>::distinct_discr_values() const
{
	TWIST_CHECK_INVARIANT
	if (!distinct_discr_values_) {
		TWIST_THROW(L"This  UnivarDistrSampleInfo  instance does not describe a discrete sample.");
	}
	return *distinct_discr_values_;
}


#ifdef _DEBUG
template<typename TSam>
void UnivarDistrSampleInfo<TSam>::check_invariant() const noexcept
{
	assert(size_ > 0);	
	assert(min_val_ <= max_val_);
	if (mean_ != 0) { //++ This is here as sometimes, after appending, the mean and stddev are zapped to zero. This should be corrected asap!!
		assert((min_val_ <= mean_) && (mean_ <= max_val_));
	}
}
#endif 

} 


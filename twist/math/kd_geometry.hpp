/// @file kd_geometry.hpp
/// k-dimensional geometry utilities

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_MATH_KD_GEOMETRY_HPP
#define TWIST_MATH_KD_GEOMETRY_HPP

namespace twist::math {

/*! A point in k-dimensional space.
    \tparam k  The number of dimensions
    \tparam Coord  The coordinate type (a numeric type)
 */
template<int k, class Coord> 
class KdPoint {
public:
	static const int k = k; ///< The number of dimensions
	typedef Coord Coord; ///< The coordinate type (a numeric type)

	/*! Constructor.
	    \tparam  Coords...  Input coordinate types; it must be safe to cast any of these types to the tree's point 
		                    coordinate type
	    \param[in] coords  The point coordinates (k values must be provided)
	 */
	template<class... Coords> 
	explicit KdPoint(Coords... coords);

	/*! Constructor.
	    \param[in] coords  The point coordinates
	 */
	explicit KdPoint(const std::array<Coord, k>& coords);

	/*! Get the point coordinate in one of the k dimensions.
	    \param[in] dim  The dimension index (eg 0 for x, 1 for y, 2 for z, etc); passing in an out-of-bounds index 
		                results in undefined behaviour 
	    \return  The point coordinate
	 */
	[[nodiscard]] auto operator[](int dim) const -> Coord;

	//! The point coordinates as an array, ordered the same as the dimensions.
	[[nodiscard]] auto coords() const -> std::array<Coord, k>;

private:
	static_assert(k > 1);
	static_assert(is_number<Coord>);

	template<int k1, class Coord1>
	[[nodiscard]] friend auto operator==(const KdPoint<k1, Coord1>& lhs, const KdPoint<k1, Coord1>& rhs) -> bool;

	std::array<Coord, k> coords_; 

	TWIST_CHECK_INVARIANT_DECL
};

// -- Free functions ---

/*! Get the square of the Euclidian distance between two point in k dimensions.
    \tparam k  The number of dimensions
    \tparam Coord  The point coordinate type 
    \param[in] pt1  The first point
    \param[in] pt2  The second point
    \return  The square of the distance
 */
template<int k, class Coord> 
[[nodiscard]] auto euclidian_distance_squared(const KdPoint<k, Coord>& pt1, const KdPoint<k, Coord>& pt2) -> Coord;

/*! Perform a linear scan through a range of k-dimensional points and find the point nearest to a given, "test" point,
    using Euclidian distance in k dimensions.
    \tparam k  The number of dimensions
    \tparam Coord  The point coordinate type 
    \tparam PointRange  Point range type; the elements are expected to be KdPoint<> objects
    \param[in] points  The range of points; if empty, an exception is thrown
    \param[in] test_point  The test point
    \param[in] ensure_unique  If true, the function makes sure that only one point in the cloud is at the minimum 
                              distance to the test point; if that is not the case, an exception is thrown
    \return 0. The nearest point
	        1. The index of the nearest point within the cloud
		    2. The distance, squared, to the nearest point
 */        
template<int k, class Coord, rg::input_range PointRange>
[[nodiscard]] auto get_nearest_point_linear(const PointRange& points, 
                                            const KdPoint<k, Coord>& test_point,
                                            bool ensure_unique) -> std::tuple<KdPoint<k, Coord>, twist::Ssize, Coord>;

/*! Generate a cloud of k-dimensional points, bounded in each dimension.   
    \tparam k  The number of dimensions
    \tparam Coord  The point coordinate type
	\param[in] nof_points  The number of points to generate
	\param[in] bounds_per_dim  The bounds of the generated point coordinates in each dimension; the order and number
	                           of intervals must match the point dimensions
	\return  The generated points
 */   
template<int k, class Coord>
[[nodiscard]] auto generate_kd_point_cloud(Ssize nof_points, const std::vector<ClosedInterval<Coord>>& bounds_per_dim) 
	                -> std::vector<KdPoint<k, Coord>>;

}

#include "twist/math/kd_geometry.ipp"

#endif 

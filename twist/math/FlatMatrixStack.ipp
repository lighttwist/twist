/// @file FlatMatrixStack.ipp
/// Inline implementation file for "FlatMatrixStack.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/IndexRange.hpp"

namespace twist::math {

// --- FlatMatrixStack class template ---

template<class T>
FlatMatrixStack<T>::FlatMatrixStack(Ssize nof_rows, Ssize nof_columns, Ssize nof_mats)
	: nof_rows_{nof_rows}
	, nof_cols_{nof_columns}
	, nof_mats_{nof_mats}
	, data_(nof_rows * nof_columns * nof_mats)
{
	check_dimensions();
	TWIST_CHECK_INVARIANT
}

template<class T>
FlatMatrixStack<T>::FlatMatrixStack(Ssize nof_rows, Ssize nof_columns, Ssize nof_mats, const_reference value)
	: nof_rows_{nof_rows}
	, nof_cols_{nof_columns}
	, nof_mats_{nof_mats}
	, data_(nof_rows * nof_columns * nof_mats, value)
{
	check_dimensions();
	TWIST_CHECK_INVARIANT
}

template<class T>
FlatMatrixStack<T>::FlatMatrixStack(Ssize nof_rows, Ssize nof_columns, Ssize nof_mats, std::vector<T> values)
	: nof_rows_{nof_rows}
	, nof_cols_{nof_columns}
	, nof_mats_{nof_mats}
{
	check_dimensions();
	if (ssize(values) != nof_rows_ * nof_cols_ * nof_mats_) {
		TWIST_THRO2(L"The size ({}) of the values vector does not match the matrix dimensions ({}, {}, {}).",
		            ssize(values), nof_rows_, nof_cols_, nof_mats_);
	}
	data_ = move(values);
	TWIST_CHECK_INVARIANT
}

template<class T>
auto FlatMatrixStack<T>::clone() const -> FlatMatrixStack<T>
{
	TWIST_CHECK_INVARIANT
	auto clone = FlatMatrixStack<T>{nof_rows_, nof_cols_, nof_mats_};
	clone.data_ = data_;
	return clone;
}

template<class T>
typename FlatMatrixStack<T>::iterator FlatMatrixStack<T>::begin() 
{ 
	TWIST_CHECK_INVARIANT
	return data_.begin(); 
}

template<class T>
typename FlatMatrixStack<T>::iterator FlatMatrixStack<T>::end() 
{ 
	TWIST_CHECK_INVARIANT
	return data_.end(); 
}

template<class T>
typename FlatMatrixStack<T>::const_iterator FlatMatrixStack<T>::begin() const 
{ 
	TWIST_CHECK_INVARIANT
	return data_.begin(); 
}

template<class T>
typename FlatMatrixStack<T>::const_iterator FlatMatrixStack<T>::end() const 
{ 
	TWIST_CHECK_INVARIANT
	return data_.end(); 
}
	
template<class T>
typename FlatMatrixStack<T>::const_iterator FlatMatrixStack<T>::cbegin() const 
{ 
	TWIST_CHECK_INVARIANT
	return data_.cbegin(); 
}

template<class T>
typename FlatMatrixStack<T>::const_iterator FlatMatrixStack<T>::cend() const 
{ 
	TWIST_CHECK_INVARIANT
	return data_.cend(); 
}

template<class T>
typename FlatMatrixStack<T>::reverse_iterator FlatMatrixStack<T>::rbegin() 
{ 
	TWIST_CHECK_INVARIANT
	return data_.rbegin(); 
}

template<class T>
typename FlatMatrixStack<T>::reverse_iterator FlatMatrixStack<T>::rend() 
{ 
	TWIST_CHECK_INVARIANT
	return data_.rend(); 
}

template<class T>
typename FlatMatrixStack<T>::const_reverse_iterator FlatMatrixStack<T>::rbegin() const 
{ 
	TWIST_CHECK_INVARIANT
	return data_.rbegin(); 
}

template<class T>
typename FlatMatrixStack<T>::const_reverse_iterator FlatMatrixStack<T>::rend() const 
{ 
	TWIST_CHECK_INVARIANT
	return data_.rend(); 
}

template<class T>
typename FlatMatrixStack<T>::const_reverse_iterator FlatMatrixStack<T>::crbegin() const 
{ 
	TWIST_CHECK_INVARIANT
	return data_.crbegin(); 
}

template<class T>
typename FlatMatrixStack<T>::const_reverse_iterator FlatMatrixStack<T>::crend() const 
{ 
	TWIST_CHECK_INVARIANT
	return data_.crend(); 
}

template<class T>
auto FlatMatrixStack<T>::data() const -> const T*
{ 
	TWIST_CHECK_INVARIANT
	return data_.data(); 
}

template<class T>
auto FlatMatrixStack<T>::data() -> T*
{ 
	TWIST_CHECK_INVARIANT
	return data_.data(); 
}

template<class T>
typename FlatMatrixStack<T>::reference FlatMatrixStack<T>::operator()(Ssize row, Ssize col, Ssize mat)
{
	TWIST_CHECK_INVARIANT
	return data_[to_linear_index(row, col, mat)];
}

template<class T>
typename FlatMatrixStack<T>::const_reference FlatMatrixStack<T>::operator()(Ssize row, Ssize col, Ssize mat) const
{
	TWIST_CHECK_INVARIANT
	return data_[to_linear_index(row, col, mat)];
}

template<class T>
typename FlatMatrixStack<T>::reference FlatMatrixStack<T>::at(Ssize row, Ssize col, Ssize mat)
{
	TWIST_CHECK_INVARIANT
	return data_.at(to_linear_index(row, col, mat));
}

template<class T>
typename FlatMatrixStack<T>::const_reference FlatMatrixStack<T>::at(Ssize row, Ssize col, Ssize mat) const
{
	TWIST_CHECK_INVARIANT
	return data_.at(to_linear_index(row, col, mat));
}

template<class T>
auto FlatMatrixStack<T>::resize(Ssize new_nof_rows, Ssize new_nof_cols, Ssize new_nof_mats) -> void
{
	TWIST_CHECK_INVARIANT
	auto tmp = FlatMatrixStack{new_nof_rows, new_nof_cols, new_nof_mats};
	auto mi = std::min(nof_rows_, new_nof_rows);
	auto mj = std::min(nof_cols_, new_nof_cols);
	auto mk = std::min(nof_mats_, new_nof_mats);
	for (auto i : IndexRange{mi}) {
		for (auto j : IndexRange{mj}) {
			auto src = begin() + to_linear_idx(i, j, 0);
			auto dest = tmp.begin() + tmp.to_linear_idx(i, j, 0);
			std::move(src, src + mk, dest);
		}
	}
	*this = std::move(tmp);
}

template<class T>
Ssize FlatMatrixStack<T>::size() const 
{ 
	TWIST_CHECK_INVARIANT
	return ssize(data_); 
}

template<class T>
bool FlatMatrixStack<T>::empty() const 
{ 
	TWIST_CHECK_INVARIANT
	return data_.empty(); 
}
	
template<class T>
auto FlatMatrixStack<T>::nof_rows() const -> Ssize 
{ 
	TWIST_CHECK_INVARIANT
	return nof_rows_; 
}
	
template<class T>
auto FlatMatrixStack<T>::nof_columns() const -> Ssize 
{ 
	TWIST_CHECK_INVARIANT
	return nof_cols_; 
}

template<class T>
auto FlatMatrixStack<T>::nof_matrices() const -> Ssize
{
	TWIST_CHECK_INVARIANT
	return nof_mats_; 
}

template<class T>
auto FlatMatrixStack<T>::nof_cells_in_matrix() const -> Ssize 
{
	TWIST_CHECK_INVARIANT
	return nof_rows_ * nof_cols_; 	
}

template<class T>
auto FlatMatrixStack<T>::nof_cells_in_stack() const -> Ssize 
{
	TWIST_CHECK_INVARIANT
	return ssize(data_); 	
}

template<class T>
auto FlatMatrixStack<T>::view_matrix(Ssize mat) const -> CellRange<const_iterator>
{
	TWIST_CHECK_INVARIANT
	const auto begin_it = std::next(data_.begin(), mat * nof_cells_in_matrix());
	const auto end_it = std::next(begin_it, nof_cells_in_matrix());
	return CellRange<const_iterator>{begin_it, end_it};
}

template<class T>
auto FlatMatrixStack<T>::view_matrix(Ssize mat) -> CellRange<iterator>
{
	TWIST_CHECK_INVARIANT
	const auto begin_it = std::next(data_.begin(), mat * nof_cells_in_matrix());
	const auto end_it = std::next(begin_it, nof_cells_in_matrix());
	return CellRange<iterator>{begin_it, end_it};
}

template<class T>
auto FlatMatrixStack<T>::view_stack() const -> CellRange<const_iterator>
{
	TWIST_CHECK_INVARIANT
	return CellRange<const_iterator>{data_.begin(), data_.end()};
}

template<class T>
auto FlatMatrixStack<T>::view_stack() -> CellRange<iterator>
{
	TWIST_CHECK_INVARIANT
	return CellRange<iterator>{data_.begin(), data_.end()};
}

template<class T>
void FlatMatrixStack<T>::check_dimensions() const
{
	if (nof_rows_ < 0 || nof_cols_ < 0 || nof_mats_ < 0) {
		TWIST_THRO2(L"Invalid matrix stack dimension sizes {}, {}, {}.", nof_rows_, nof_cols_, nof_mats_);
	}
	if (nof_rows_ != 0 || nof_cols_ != 0 || nof_mats_ != 0) {
		if (nof_rows_ == 0 || nof_cols_ == 0 || nof_mats_ == 0) {
			TWIST_THRO2(L"Invalid matrix stack dimension sizes {}, {}, {}.", nof_rows_, nof_cols_, nof_mats_);		
		}
	}
}

template<class T>
void FlatMatrixStack<T>::check_indexes(Ssize row, Ssize col, Ssize mat) const
{
	TWIST_CHECK_INVARIANT
	if (row >= nof_rows_ || col >= nof_cols_ || mat >= nof_mats_) {
		TWIST_THRO2(L"Invalid cell position ({}, {}, {}).", row, col, mat);
	}
}

template<class T>
auto FlatMatrixStack<T>::to_linear_index(Ssize row, Ssize col, Ssize mat) const -> Ssize
{
	TWIST_CHECK_INVARIANT
    return row * nof_cols_ + col + mat * nof_rows_ * nof_cols_;
}

#ifdef _DEBUG
template<class T>
auto FlatMatrixStack<T>::check_invariant() const noexcept -> void
{
	assert(nof_rows_ >= 0);
	assert(nof_cols_ >= 0);
	assert(nof_mats_ >= 0);
	assert((nof_rows_ == 0) == (nof_cols_ == 0));
	assert((nof_mats_ == 0) == (nof_cols_ == 0));
	assert(ssize(data_) == nof_rows_ * nof_cols_ * nof_mats_);
}
#endif

// --- FlatMatrixStack::CellRange class template ---

template<class T> 
template<class Iter> 
FlatMatrixStack<T>::CellRange<Iter>::CellRange(Iter first, Iter last) 
	: Range<Iter>{first, last} 
{
}

template<class T> 
template<class Iter> 
auto FlatMatrixStack<T>::CellRange<Iter>::data() const -> const T*
{
	assert(this->begin() != this->end());
	return std::addressof(*this->begin());
}

template<class T> 
template<class Iter> 
auto FlatMatrixStack<T>::CellRange<Iter>::data() -> T* requires (not ConstPointer<std::iterator_traits<Iter>::pointer>)
{
	assert(this->begin() != this->end());
	return std::addressof(*this->begin());
}

// --- Free functions ---

template<class T>
auto swap(FlatMatrixStack<T>& a, FlatMatrixStack<T>& b) noexcept -> void
{
	using std::swap;
	a.data_.swap(b.data_);
	swap(a.nof_rows_, b.nof_rows_);
	swap(a.nof_cols_, b.nof_cols_);
	swap(a.nof_mats_, b.nof_mats_);
}

template<class T>
auto operator==(const FlatMatrixStack<T>& lhs, const FlatMatrixStack<T>& rhs) noexcept -> bool
{
	if (lhs.nof_rows() != rhs.nof_rows() || lhs.nof_columns() != rhs.nof_columns() || 
			lhs.nof_matrices() != rhs.nof_matrices()) {
		return false;
	}
	return std::equal(lhs.begin(), lhs.end(), rhs.begin(), rhs.end());
}

template<class T>
auto operator!=(const FlatMatrixStack<T>& lhs, const FlatMatrixStack<T>& rhs) noexcept -> bool
{
	return !(lhs == rhs);
}

template<class T>
auto dimensions(const FlatMatrixStack<T>& mat_stack) -> std::tuple<Ssize, Ssize, Ssize> 
{
	return {mat_stack.nof_rows(), mat_stack.nof_columns(), mat_stack.nof_matrices()};
}

template<class T, class Fn>
requires std::invocable<Fn, const T&, Ssize, Ssize, Ssize>
auto for_each_cell(const FlatMatrixStack<T>& mat_stack, Fn func) -> void
{
	for (auto row : IndexRange{mat_stack.nof_rows()}) {
		for (auto col : IndexRange{mat_stack.nof_columns()}) {
			for (auto mat : IndexRange{mat_stack.nof_matrices()}) {
				std::invoke(func, mat_stack(row, col, mat), row, col, mat);
			}
		}	
	}
}

template<class T, class Fn>
requires std::invocable<Fn, T&, Ssize, Ssize, Ssize>
auto for_each_cell(FlatMatrixStack<T>& mat_stack, Fn func) -> void
{
	for (auto row : IndexRange{mat_stack.nof_rows()}) {
		for (auto col : IndexRange{mat_stack.nof_columns()}) {
			for (auto mat : IndexRange{mat_stack.nof_matrices()}) {
				std::invoke(func, mat_stack(row, col, mat), row, col, mat);
			}
		}	
	}
}

template<class T> 
[[nodiscard]] auto flip_rows(FlatMatrixStack<T>& mat_stack) -> void
{
	if (mat_stack.empty()) {
		return;
	}

    auto swap_rows = [nof_cols = mat_stack.nof_columns(),
					  temp_row_buffer = std::vector<T>(mat_stack.nof_columns())](auto& mat_cells,
																			     auto row1,
																			     auto row2) mutable {
		auto row1_cells_begin = mat_cells.begin() + row1 * nof_cols;
		auto row2_cells_begin = mat_cells.begin() + row2 * nof_cols;
		std::copy(row1_cells_begin, row1_cells_begin + nof_cols, temp_row_buffer.begin());
		std::copy(row2_cells_begin, row2_cells_begin + nof_cols, row1_cells_begin);
		rg::copy(temp_row_buffer, row2_cells_begin);
	};

	for (auto mat : IndexRange{mat_stack.nof_matrices()}) {
		for (auto row : IndexRange{mat_stack.nof_rows() / 2}) {
			auto matrix_view = mat_stack.view_matrix(mat);
			swap_rows(matrix_view, row, mat_stack.nof_rows() - 1 - row);
		}
	}
}

}

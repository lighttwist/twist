#include "phys_units.hpp"
/// @file phys_units.ipp
/// Inline implementation file for "phys_units.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace twist::math {

template<std::floating_point T>
[[nodiscard]] auto kelvin_to_celsius(T k) -> T
{
    return static_cast<T>(k - 273.15);
}

template<std::floating_point T>
[[nodiscard]] auto celsius_to_kelvin(T c) -> T
{
    return static_cast<T>(c + 273.15);
}

template<std::floating_point T>
[[nodiscard]] auto kmph_to_mps(T kmph) -> T
{
    return static_cast<T>(kmph / 3.6);
}

template<std::floating_point T>
[[nodiscard]] auto mps_to_kmph(T mps) -> T
{
    return static_cast<T>(mps * 3.6);    
}

template<std::floating_point T>
[[nodiscard]] auto pascal_to_hectopascal(T p) -> T
{
    return static_cast<T>(p / 100.0);
}

template<std::floating_point T>
[[nodiscard]] auto square_metre_to_hectare(T square_metre) -> T
{
	return static_cast<T>(square_metre * 0.0001);
}

template<std::floating_point T>
[[nodiscard]] auto hectare_to_square_metre(T hectare) -> T
{
	return static_cast<T>(hectare * 10000);
}

template<std::floating_point T>
auto tonne_per_hectare_to_kg_per_m2(T tph) -> T
{
    return static_cast<T>(tph * 0.1);
}

}

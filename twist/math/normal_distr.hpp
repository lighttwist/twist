///  @file  normal_distr.hpp
///  Utilities for working with the univariate normal (Gaussian) distribution

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_MATH_NORMAL__DISTR_HPP
#define TWIST_MATH_NORMAL__DISTR_HPP

namespace twist::math {

/// The standard normal (Gaussian) CDF (cumulative distribution function). 
/// The implementation is based on the algorithm presented in the paper "Evaluating the Normal Distribution", 
/// G. Marsaglia, Journal of Statistical Software, 2004 (DOI: 10.18637/jss.v011.i04).
///
/// @param[in] x  The argument
/// @return  The image
///
double stdnormal_cdf(double x);

/// The inverse of the standard normal (Gaussian) CDF (cumulative distribution function). 
/// The implementation is based on the work of P. J. Acklam, see
///   https://web.archive.org/web/20151030215612/http://home.online.no/~pjacklam/notes/invnorm/ (also as a 
///   document attached to this library). 
/// The relative error has an absolute value less than 1.15e-9 in the entire region. 
/// Full machine precision can be attained by one iteration of Halley's rational method, third order but this 
///   isn't implemented here. See the webpage above for details about that refinement.
///
/// @param[in] p  The argument; must be in the interval (0, 1)
/// @return  The image
///
double stdnormal_invcdf(double p);

/// Calculate value of the PDF (probability density function) for the normal (Gaussian) distribution.
///
/// @param[in] x  Argument of the function
/// @param[in] mu  Mean
/// @param[in] sigma  Standard deviation; must be greater than 0
/// @return  Value of PDF
///
double normal_pdf(double x, double mu, double sigma);

/// Calculate value of the CDF (cumulative distribution function) for the normal (Gaussian) distribution.
///
/// @param[in] x  Realization of the distribution
/// @param[in] mu  Mean
/// @param[in] sigma  Standard deviation; must be greater than 0
/// @return  Value of cdf
///
double normal_cdf(double x, double mu, double sigma);

/// Calculate value of the inverse CDF (cumulative distribution function) for the normal (Gaussian) 
/// distribution.
///
/// @param[in] p  Percentile; must be in the interval (0, 1)
/// @param[in] mu  Mean
/// @param[in] sigma  Standard deviation; must be greater than 0
/// @return  Value of inverse CDF
///
double normal_invcdf(double p, double mu, double sigma);

} 

#endif 

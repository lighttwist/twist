///  @file  SymmetricMatrix.ipp
///  Inline implementation file for "SymmetricMatrix.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

namespace twist::math {

//
//  SymmetricMatrix class    
//

template<typename T>
SymmetricMatrix<T>::SymmetricMatrix(Ssize size) 
	: size_{ size }
{
	// Create the triangular matrix structure
	elements_ = new T*[size_];
	for (auto i = 0; i < size_; ++i) {
		elements_[i] = new T[i + 1];
	}
	TWIST_CHECK_INVARIANT
}


template<typename T>
SymmetricMatrix<T>::SymmetricMatrix(const SymmetricMatrix& src) 
	: size_{ src.size_ }
{
	// Create the triangular matrix structure.
	elements_ = new T*[size_];
	for (auto i = 0; i < size_; ++i) {
		elements_[i] = new T[i + 1];
	}
	assign(src);
	TWIST_CHECK_INVARIANT
}


template<typename T>
SymmetricMatrix<T>::~SymmetricMatrix()
{
	TWIST_CHECK_INVARIANT
	// Destroy the triangular structure.
	for (auto i = 0; i < size_; ++i) {
		delete[] elements_[i];	
	}
	delete[] elements_;
}


template<typename T>
void SymmetricMatrix<T>::assign(const SymmetricMatrix& src)
{
	TWIST_CHECK_INVARIANT
	if (src.size_ != size_) {
		TWIST_THROW(L"The two matrices must have the same size");
	}
	// Copy matrix contents across.
	for (auto i = 0; i < size_; ++i) {
		memcpy(elements_[i], src.elements_[i], (i + 1) * sizeof(T));
	}
	TWIST_CHECK_INVARIANT
}


template<typename T>
inline typename SymmetricMatrix<T>::SubscriptProxyConst SymmetricMatrix<T>::operator[](Ssize row) const
{
	TWIST_CHECK_INVARIANT
	return SubscriptProxyConst(this, row);
}


template<typename T>
inline typename SymmetricMatrix<T>::SubscriptProxy SymmetricMatrix<T>::operator[](Ssize row)
{
	TWIST_CHECK_INVARIANT
	return SubscriptProxy(this, row);
}


template<typename T>
const T& SymmetricMatrix<T>::elem(const Ssize row, Ssize col) const
{
	TWIST_CHECK_INVARIANT
	if (row > col) {
		if (row >= size_) {
			TWIST_THROW(L"Row subscript out of range");
		}
		return elements_[row][col];
	}
	else {
		if (col >= size_) {
			TWIST_THROW(L"Column subscript out of range");
		}
		return elements_[col][row];	
	}
}


template<typename T>
T& SymmetricMatrix<T>::elem(Ssize row, Ssize col)
{
	TWIST_CHECK_INVARIANT
	if (row > col) {
		if (row >= size_) {
			TWIST_THROW(L"Row subscript out of range");
		}
		TWIST_CHECK_INVARIANT
		return elements_[row][col];
	}
	else {
		if (col >= size_) {
			TWIST_THROW(L"Column subscript out of range");
		}
		TWIST_CHECK_INVARIANT
		return elements_[col][row];	
	}
}


template<typename T>
Ssize SymmetricMatrix<T>::size() const
{
	TWIST_CHECK_INVARIANT
	return size_;
}


template<typename T>
Ssize SymmetricMatrix<T>::nof_rows() const
{
	TWIST_CHECK_INVARIANT
	return size_;
}


template<typename T>
Ssize SymmetricMatrix<T>::nof_columns() const
{
	TWIST_CHECK_INVARIANT
	return size_;
}


template<typename T>
void SymmetricMatrix<T>::make_zero()
{
	TWIST_CHECK_INVARIANT
	for (auto i = 0; i < size_; ++i) {
		for (auto j = 0; j <= i; ++j) {
			elements_[i][j] = 0;
		}
	}
	TWIST_CHECK_INVARIANT
}


template<typename T>
void SymmetricMatrix<T>::set_diagonal(const T& value)
{
	TWIST_CHECK_INVARIANT
	for (auto i = 0; i < size_; ++i) {
		elements_[i][i] = value;
	}
	TWIST_CHECK_INVARIANT
}


template<typename T>
void SymmetricMatrix<T>::subtract(const Matrix<T>& right, Matrix<T>& result) const
{
	TWIST_CHECK_INVARIANT
    if ((size_ != right.nof_rows()) || (size_ != result.nof_rows()) || (size_ != right.nof_columns()) || 
			(size_ != result.nof_columns())) {
        TWIST_THROW(L"All three matrices must be square and have the same size.");
    }

    for (auto i = 0; i < size_; ++i) {
        for (auto j = 0; j < size_; ++j) {
			if (i > j) {
				result[i][j] = elements_[i][j] - right[i][j];
			}
			else {
				result[i][j] = elements_[j][i] - right[i][j];
			}
        }
    }
}


template<typename T>
void SymmetricMatrix<T>::subtract(const SymmetricMatrix<T>& right, SymmetricMatrix<T>& result) const
{
	TWIST_CHECK_INVARIANT
    if ((size_ != right.size_) || (size_ != result.size_)) {
        TWIST_THROW(L"All three matrices must have the same number of rows and the same size.");
    }

	for (auto i = 0; i < size_; ++i) {
		for (auto j = 0; j <= i; ++j) {
            result.elements_[i][j] = elements_[i][j] - right.elements_[i][j];
        }
    }
}


template<typename T>
void SymmetricMatrix<T>::cholesky(LowerTriangularMatrix<T>& lt_matrix) const
{
	TWIST_CHECK_INVARIANT
	if (size_ != lt_matrix.size()) {
		TWIST_THROW(L"The symmetric matrix and the lower triangular matrix must have the same size.");
	}
	lt_matrix.make_zero();
	
	double sum = 0;
	for (auto i = 0; i < size_; ++i) {
		// Compute the elements to the left of the diagonal.
		for (auto j = 0; j < i; ++j) {
			sum = 0;
			for (auto k = 0; k < j; ++k) {
				sum += lt_matrix[i][k] * lt_matrix[j][k];
			}			
			lt_matrix[i][j] = static_cast<T>(1.0 / lt_matrix[j][j] * (elements_[i][j] - sum));
		}
		// Compute the diagonal element.
		sum = 0;
		for (auto k = 0; k < i; ++k) {
			sum += lt_matrix[i][k] * lt_matrix[i][k];
		}
		lt_matrix[i][i] = static_cast<T>(sqrt(elements_[i][i] - sum));
	}
}


template<typename T>
double SymmetricMatrix<T>::determinant() const
{
	TWIST_CHECK_INVARIANT
	double determinant = 0.0;

    Matrix<T> lu(size_, size_);
    lu.assign(*this);   
    
    std::vector<Ssize> indx(size_);

	short sign = 1;
    lu.lu_decomposition(indx, sign);
 
    determinant = sign;
    for (auto j = 0; j < size_; ++j) {
        determinant *= lu[j][j];
    }

    return determinant;
}


template<typename T>
bool SymmetricMatrix<T>::determinant_and_inverse(double& det, Matrix<T>& inv) const
{
	TWIST_CHECK_INVARIANT
	const double determinant_tolerance = 1.0E-20;
	
    if (inv.nof_rows() != inv.nof_columns())  {
        TWIST_THROW(L"The 'inv' matrix must be square.");
    }
    if (size_ != inv.nof_rows()) {
        TWIST_THROW(L"This matrix and 'inv' must have the same size.");
    }

    bool retVal = false;
	det = 0.0;

    Matrix<T> lu(size_, size_);
    lu.assign(*this);
    
    std::vector<Ssize> indx(size_);
    std::vector<T> col(size_);

	short sign = 1;
    lu.lu_decomposition(indx, sign);
 
    det = sign;
    for (auto j = 0; j < size_; ++j) {
        det *= lu[j][j];
    }

    if (abs(det) >= determinant_tolerance) {
        // Find inverse by columns.
        for (auto j = 0; j < size_; ++j) {
            for (auto i = 0; i < size_; ++i) {
                col[i] = 0.0;
            }
            col[j] = 1.0;
            lu.lu_backsubst(indx, col);
            for (auto i = 0; i < size_; ++i) {
                inv[i][j] = col[i];
            }
        }
        retVal = true;
    }

    return retVal;
}

	      
#ifdef _DEBUG
template<typename T>
void SymmetricMatrix<T>::check_invariant() const noexcept
{
	assert(elements_);
	assert(size_ > 0);
}
#endif 

//
//  SymmetricMatrix::SubscriptProxyConst class    
//

template<typename T>
SymmetricMatrix<T>::SubscriptProxyConst::SubscriptProxyConst(const SymmetricMatrix<T>* matrix, Ssize row) 
	: matrix_{ matrix }
	, row_(row)
{
}


template<typename T>
inline const T& SymmetricMatrix<T>::SubscriptProxyConst::operator[](Ssize col) const
{
	if (row_ > col) {
		return matrix_->elements_[row_][col];
	}
	return matrix_->elements_[col][row_];	
}

//
//  SymmetricMatrix::SubscriptProxy class    
//

template<typename T>
SymmetricMatrix<T>::SubscriptProxy::SubscriptProxy(SymmetricMatrix<T>* matrix, Ssize row) 
	: matrix_{ matrix }
	, row_(row)
{
}


template<typename T>
inline T& SymmetricMatrix<T>::SubscriptProxy::operator[](Ssize col) 
{
	if (row_ > col) {
		return matrix_->elements_[row_][col];
	}
	return matrix_->elements_[col][row_];	
}

} 

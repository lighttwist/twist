/// @file CsvSampleFileMiner.cpp
/// Implementation file for "CsvSampleFileMiner.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/math/CsvSampleFileMiner.hpp"

#include "twist/InTextFileStream.hpp"
#include "twist/string_utils.hpp"
#include "twist/math/UnivarDistrSample.hpp"
#include "twist/math/numeric_utils.hpp"

namespace twist::math {

static const char*  delimiter = ",";
static const Ssize  def_num_prog_steps = 50;
static const Ssize  buffer_len = 131072;

// Devnote: The implementation should use twist::db::DelimValueFileReader

CsvSampleFileMiner::CsvSampleFileMiner(const fs::path& path, 
		IsValidRandVarName is_valid_rand_var_name, ProgressProv& get_prog_prov) 
	: SampleFileMiner{ is_valid_rand_var_name, get_prog_prov }
	, path_{ path }
	, line_buffer_{ std::make_unique<char[]>(buffer_len) }
	, field_buffer_{ std::make_unique<char[]>(1024) }
{
	if (!exists(path_)) {
		TWIST_THROW(L"Sample file \"%s\" not found.", path_.c_str());
	}
	TWIST_CHECK_INVARIANT
}


CsvSampleFileMiner::~CsvSampleFileMiner()
{
	TWIST_CHECK_INVARIANT
}


void CsvSampleFileMiner::analyse() 
{
	TWIST_CHECK_INVARIANT
	if (is_analysed()) {
		TWIST_THROW(L"The sample file has already been analysed.");
	}
	
	get_prog_prov().set_prog_msg(L"Analysing the sample file...");
	get_prog_prov().set_prog_range(0, 2);
	get_prog_prov().set_prog_pos(0);

	InTextFileStream<char> file_stream( std::make_unique<FileStd>(path_, FileOpenMode::read) );
	
	const char* delimiter_end = delimiter + strlen(delimiter);
	
	// Read the first (non-blank) line of the sample file.	
	Ssize line_len = 0;
	file_stream.get_line(line_buffer_.get(), buffer_len, line_len);
	while (is_whitespace(line_buffer_.get(), line_buffer_.get() + line_len)) {
		file_stream.get_line(line_buffer_.get(), buffer_len, line_len);
	}
	
	// Parse the line for the random variable names.
	std::wstring varName;
	std::wstring varNameErr;
	std::unique_ptr<std::vector<std::wstring>> randVarNames(new std::vector<std::wstring>());

	const Ssize numVars = count_delimited_fields<const char*, const char*>(
			line_buffer_.get(), line_buffer_.get() + line_len, delimiter, delimiter_end);
	for (auto i = 0; i < numVars; ++i) {

		std::pair<const char*, const char*> field = 
				get_delimited_field(line_buffer_.get(), line_buffer_.get() + line_len, delimiter, delimiter_end, i);
		varName.assign(field.first, field.second);
		varName = trim_whitespace(varName);
		varName = trim_char(varName, L'\"');  // Sometimes variable names are quoted in the header line
		varName = trim_char(varName, L'\'');

		// Check that the name obeys the random variable naming rules.
		if (is_valid_rand_var_name(varName, varNameErr)) {
			// Name valid.
			randVarNames->push_back(varName);
		}
		else {
			TWIST_THROW(L"Invalid variable name \"%s\" in sample file: %s", varName.c_str(), varNameErr.c_str());
		}
	}
	get_prog_prov().set_prog_pos(1);
		
	// Get the number of samples, by counting all the non-empty lines (except the first).
	Ssize numSamples = 0;
	while (!file_stream.is_eof()) {
		file_stream.get_line(line_buffer_.get(), buffer_len, line_len);
		if (!is_whitespace(line_buffer_.get(), line_buffer_.get() + line_len)) {
			numSamples++;
		}
	}
	get_prog_prov().set_prog_pos(2);

	set_general_sample_info(move(randVarNames), numSamples);
	TWIST_CHECK_INVARIANT
}


void CsvSampleFileMiner::mine()
{
	TWIST_CHECK_INVARIANT
	if (!is_analysed()) {
		TWIST_THROW(L"The sample file has not been analysed yet.");
	}

	const auto numVars = ssize(get_rand_var_names());
	const auto num_samples = get_sample_size();

	const auto numProgSteps = (num_samples > def_num_prog_steps) ? def_num_prog_steps : num_samples;
	const double progStep = num_samples / static_cast<double>(numProgSteps);
	
	Ssize last_prog_sample_line_idx = 0;
	
	get_prog_prov().set_prog_msg(L"Reading the sample values...");
	get_prog_prov().set_prog_range(0, numProgSteps);
	get_prog_prov().set_prog_pos(0);

	InTextFileStream<char> file_stream( std::make_unique<FileStd>(path_, FileOpenMode::read) );
	
	// Prepare structure to hold the sample values.
	MultivarSample& sample_matrix = prepare_multivar_sample_container();

	Ssize line_len = 0;
	
	// Go past the random variables, ignoring any blank lines.
	file_stream.get_line(line_buffer_.get(), buffer_len, line_len);
	while (is_whitespace(line_buffer_.get(), line_buffer_.get() + line_len)) {
		file_stream.get_line(line_buffer_.get(), buffer_len, line_len);
	}

	// Read the sample values.
	std::stringstream value_converter;
	Ssize sample_line_idx = 1;
	const char* delimiter_end = delimiter + strlen(delimiter);
	double value = 0;

	while (!file_stream.is_eof()) {
		file_stream.get_line(line_buffer_.get(), buffer_len, line_len);
		// Ignore blank lines.
		if (!is_whitespace(line_buffer_.get(), line_buffer_.get() + line_len)) {

			const Ssize num_vals = count_delimited_fields(
					line_buffer_.get(), line_buffer_.get() + line_len, delimiter, delimiter_end);			
			if (num_vals != numVars) {
				std::string lineStr(line_buffer_.get(), line_buffer_.get() + line_len);
				TWIST_THROW(L"Wrong number of values on line %d in sample file: \"%s\"", 
						sample_line_idx + 1, ansi_to_string(lineStr.c_str()).c_str());  
			}
				
			for (auto i = 0; i < numVars; ++i) {
				// Read the text field delimited by the value separators.
				auto value_field = get_delimited_field(
						line_buffer_.get(), line_buffer_.get() + line_len, delimiter, delimiter_end, i);								
				value_field = trim_right(value_field.first, value_field.second, ' ');
				value_field = trim_right(value_field.first, value_field.second, '\t');
				
				const Ssize value_str_len = static_cast<Ssize>(value_field.second - value_field.first);				
				memcpy(field_buffer_.get(), value_field.first, value_str_len);
				field_buffer_[value_str_len] = '\0';

				// Convert the text value field into a floating-point number.
				value_converter.clear();
				value_converter.str(field_buffer_.get());
				value_converter >> value;
				
				if (value_converter.fail() || !value_converter.eof()) {					
					TWIST_THROW(L"Invalid value \"%s\" on line %d in the sample file.", 
							ansi_to_string(field_buffer_.get()).c_str(), sample_line_idx);
				}

				// Store the value in the sample values structure.
				sample_matrix.get_var_sample_at(i).at(sample_line_idx - 1) = convert_num<float>(value);				
			}
			++sample_line_idx;
		
			if (sample_line_idx - last_prog_sample_line_idx >= progStep) {
				get_prog_prov().inc_prog_pos();
				last_prog_sample_line_idx = sample_line_idx;
			}	
		}
	}

	if (sample_line_idx != num_samples + 1) {
		// Something is seriously screwed up - it look as though some sample lines went missing, or appeared, since the 
		// file was analysed.
		TWIST_THROW(L"Error reading sample file: invalid sample file format.");
	}
}


#ifdef _DEBUG
void CsvSampleFileMiner::check_invariant() const noexcept
{
	assert(!path_.empty());
	assert(line_buffer_);
	assert(field_buffer_);
}
#endif

}

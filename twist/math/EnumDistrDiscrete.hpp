/// @file EnumDistrDiscrete.hpp
/// EnumDistrDiscrete class template, inherits DistrDiscrete

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_MATH_ENUM_DISCRETE_DISTRIBUTION_HPP
#define TWIST_MATH_ENUM_DISCRETE_DISTRIBUTION_HPP

#include "twist/math/DistrDiscrete.hpp"

namespace twist::math {

/*! Provides an interface for using DistrDiscrete with the state values being the (distinct) values of an enum.
    \note See also class DistrDiscrete.
	\tparam Enum  The enum type; a function with the signature 'auto is_valid(Enum) -> bool' must be visible when 
	              instantiating the class template
 */
template<class Enum>
class EnumDistrDiscrete : public DistrDiscrete {
public:
	static_assert(is_safe_numeric_cast<std::underlying_type_t<Enum>, double>); 

	/*! A list of state value / state probability pairs. The list is always ordered in increasing order of the state 
	    values (ie the underlying numeric values of the enum values). It cannot contain two states with the same value, 
		or probabilities outside the (0, 1] interval, but it performs no validity checks on how the probabilities 
		add up.
	 */
	class EnumStateList {
	public:
		explicit EnumStateList() = default;

		/*! Add a state to the list.
		    \param[in] value  The state value.
		    \param[in] prob  The state probability. Must be in the interval (0, 1].
		 */
		auto insert_state(Enum value, double prob) -> void;

		/*! Delete a specific state from the list.
		    \param[in] state_pos  The state position in the list; an exception is thrown if the position is invalid
		 */
		auto delete_state(Ssize state_pos) -> void;
		
		/*! Get read-only access to the internal list of state value / state probability pairs.
		    \return  Ref to the list.
		 */
		[[nodiscard]] auto states() const -> std::vector<std::pair<Enum, double>>;	

		/*! Look in the list for a state with a specific value and find out its position.
		    \param[in] state_value  The state value
		    \return  The state's position withing the list, or twist::k_no_pos if no match is found
		 */
		[[nodiscard]] auto get_state_pos(Enum state_value) const -> Ssize;

		/*! Get information about a specific state from the list.
		    \param[in] state_pos  The state position in the list; an exception is thrown if the position is invalid
		    \return  0. The state value
			         1. The state probability
		 */
		[[nodiscard]] auto get_state_at(Ssize state_pos) const -> std::pair<Enum, double>;

		/*! Get the number of states in the list.
		    \return  The list size
		 */
		[[nodiscard]] auto get_size() const -> Ssize;

		/*! Get the sum of all the state probabilities in the list.
		    \return  The sum of probabilities
		 */
		[[nodiscard]] auto get_prob_sum() const -> double;

		/*! If the state probabilities in the list add up to less or more than one (within a reasonable tolerance), 
		    then this method will modify the last state probability so that the states do add up to one. 
		    \return  The amount by which the last state probability was changed
		 */
		auto adjust_last_state_prob() -> double;
		
		//! Clear all states in the list.
		auto clear() -> void;	

		TWIST_NO_COPY_DEF_MOVE(EnumStateList)

	private:
		DistrDiscrete::StateList state_list_;

		friend class EnumDistrDiscrete;
		
		TWIST_CHECK_INVARIANT_DECL
	};

	/*! Constructor.
	    \param[in] states  List of state value / state probability pairs; if the probabilities for all states do not 
		                   add up to one (within a reasonable tolerance) an exception is thrown 
	 */
	explicit EnumDistrDiscrete(EnumStateList states);

	[[nodiscard]] auto getClone() const -> EnumDistrDiscrete* override; // Distribution override

	/*! Get the probability of a specific state in the distribution.
        \param[in] state_value  The value of the state; if no state with this value exists, an exception is thrown
	    \return  The probability of the state
	 */
	[[nodiscard]] auto get_state_prob_for_value(Enum state_value) const -> double;

	/*! Get the value of a specific state in the distribution.
        \param[in] state_idx  The index of the state
	    \return  The value of the state
	 */
	[[nodiscard]] auto get_state_enum_value_at(Ssize state_idx) const -> Enum;

	/*! Check whether the given value is one of the states of the distribution.
	    \param[in] state_value  Presumable state
	    \return  true only if the value is one of the states
	 */
	[[nodiscard]] auto is_state(Enum state_value) const -> bool;

	/*! Find out what the index of a specific state is within the list of states.
	    \param[in] state_value  The state value
	    \return  The state index, or twist::k_no_idx if the value passed in does not match any of the states
	 */
	[[nodiscard]] auto get_state_idx(Enum state_value) const -> Ssize;

	/*! Apply the inverse CDF.
	    \param p  [in] Percentile in interval (0, 1)
	    \return  The corresponding realization of the random variable, as an enum value
	 */
	[[nodiscard]] auto inv_cdf_enum_value(double p) const -> Enum; 

	TWIST_NO_COPY_DEF_MOVE(EnumDistrDiscrete)

private:
	TWIST_CHECK_INVARIANT_DECL
};

}

#include "EnumDistrDiscrete.ipp"

#endif 


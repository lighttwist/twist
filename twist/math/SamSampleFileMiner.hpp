///  @file  SamSampleFileMiner.hpp
///  SamSampleFileMiner class, inherits SampleFileMiner

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_MATH_SAM_FILE_MINER_HPP
#define TWIST_MATH_SAM_FILE_MINER_HPP

#include "twist/InTextFileStream.hpp"

#include "SampleFileMiner.hpp"

namespace twist::math {

/// This class reads the data from a text sample file, which follows the (very nearly obsolete) Unicorn SAM 
/// file format.
class SamSampleFileMiner : public SampleFileMiner {
public:
	/// Constructor.
	///
	/// @param[in] path  The SAM sample filename
	/// @param[in] is_valid_rand_var_name  Functor which checks whether a random variable name is valid 
	///					(doesn't contain forbidden characters)
	/// @param[in] prog_prov  Progress provider for the mining
	///
	SamSampleFileMiner(const fs::path& path, IsValidRandVarName is_valid_rand_var_name, 
			ProgressProv& prog_prov);

	/// Destructor.
	virtual ~SamSampleFileMiner();
	
	void analyse() override;  // SampleFileMiner override
	
	void mine() override;  // SampleFileMiner override		

private:	
    /// Read the next valid line of text from the SAM file into the internal buffer (ended in a null character).
    /// This function will read the next line on from the current position in the file, until it finds a valid line.
    /// Lines considered invalid are blank lines and lines which only contain a comment.
    /// If a valid line is found, any comment is removed from it before it is returned.
    ///
    /// @return  false if the end of file was reached before a valid line could be found. 
    ///
    bool read_next_valid_line_();

	const fs::path  path_;
	
	// Input stream connected to the sample file.
	std::unique_ptr<InTextFileStream<char>>  file_stream_;

	// Buffer where the contents of one line read from the sample file are stored.
	std::unique_ptr<char[]>  line_buffer_;	
	
	// The length of the line that is currently read in the buffer.
	Ssize  line_len_;

	TWIST_CHECK_INVARIANT_DECL
};

}

#endif 


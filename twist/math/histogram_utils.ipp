/// @file histogram_utils.ipp
/// Inline implementation file for "histogram_utils.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/std_vector_utils.hpp"
#include "twist/math/sample_utils.hpp"

namespace twist::math {

// --- Histogram class ---

template<typename Sam>
Histogram<Sam>::~Histogram()
{
}

// --- HistogramCont class ---

template<typename Sam>
template<class Iter, class> 
HistogramCont<Sam>::HistogramCont(Iter samBegin, Iter samEnd, size_t numBins) 
	: bin_count_{numBins}
	, freq_list_(numBins)
	, partition_points_(numBins + 1)
{
	// Find the sample min and max
	auto samMin = Sam{0};
	auto samMax = Sam{0};
	get_sample_range(samBegin, samEnd, samMin, samMax);

	calculate(samBegin, samEnd, samMin, samMax);
	TWIST_CHECK_INVARIANT	
}

template<typename Sam>
template<class Iter, class> 
HistogramCont<Sam>::HistogramCont(Iter samBegin, Iter samEnd, Sam samMin, Sam samMax, size_t numBins) 
	: bin_count_{numBins}
	, freq_list_(numBins)
	, partition_points_(numBins + 1)
{
	calculate(samBegin, samEnd, samMin, samMax);
	TWIST_CHECK_INVARIANT	
}

template<typename Sam>
HistogramCont<Sam>* HistogramCont<Sam>::clone() const
{
	TWIST_CHECK_INVARIANT
	return new HistogramCont(*this);
}

template<typename Sam>
bool HistogramCont<Sam>::compare(const Histogram<Sam>& other) const
{
	TWIST_CHECK_INVARIANT
	if (auto otherCont = dynamic_cast<const HistogramCont*>(&other)) {
		return (bin_count_ == otherCont->bin_count_) &&
			   (freq_list_ == otherCont->freq_list_) &&
			   (partition_points_ == otherCont->partition_points_);
	}
	
	return false;
}

template<typename Sam>
size_t HistogramCont<Sam>::bin_count() const
{
	TWIST_CHECK_INVARIANT
	return bin_count_;
}

template<typename Sam>
size_t HistogramCont<Sam>::get_frequency(size_t binIdx) const
{
	TWIST_CHECK_INVARIANT
	return freq_list_.at(binIdx);
}

template<typename Sam>
size_t HistogramCont<Sam>::max_frequency() const
{	
	TWIST_CHECK_INVARIANT
	return max_freq_;
}

template<typename Sam>
auto HistogramCont<Sam>::sample_value_interval() const -> ClosedInterval<Sam> 
{
	TWIST_CHECK_INVARIANT
    const auto lo = static_cast<Sam>(partition_points_.front());
    assert(lo == partition_points_.front());
    const auto hi = static_cast<Sam>(partition_points_.back());
    assert(hi == partition_points_.back());
    return ClosedInterval<Sam>{lo, hi};
}

template<typename Sam>
inline auto HistogramCont<Sam>::partition_points() const -> std::vector<double>
{
	TWIST_CHECK_INVARIANT
	return partition_points_;
}

template<typename Sam>
template<class Iter, class> 
void HistogramCont<Sam>::calculate(Iter samBegin, Iter samEnd, Sam samMin, Sam samMax)
{  
    // Create the partition points list
    partition_points_[0] = samMin;
    partition_points_[bin_count_] = samMax;
    
    const double binLength = (samMax - samMin) / static_cast<double>(bin_count_);
    for (size_t i = 1; i < bin_count_; ++i) {
        partition_points_[i] = partition_points_[i - 1] + binLength;
    }

    // Create the frequency list    
    max_freq_ = 0;

    if (binLength > 0) {
		size_t binIndex = 0;
        for (Iter it = samBegin; it != samEnd; ++it) {
            binIndex = static_cast<size_t>(floor((*it - samMin) / binLength));
            // Some numerical problems may occur at the end.
            if (binIndex >= bin_count_) {
                binIndex = bin_count_ - 1;
            }
            freq_list_[binIndex] = freq_list_[binIndex] + 1;
            if (max_freq_ < freq_list_[binIndex]) {
                max_freq_ = freq_list_[binIndex];
            }
        }
    }
    else {
        // The sample is constant.
        bin_count_ = 1;
        partition_points_.resize(2);
        freq_list_.resize(1);
        
        const size_t samSize = std::distance(samBegin, samEnd);
        freq_list_[0] = samSize;
        partition_points_[0] = *samBegin;
        partition_points_[1] = *samBegin;
        max_freq_ = samSize;
    }	
}

#ifdef _DEBUG
template<typename Sam>
void HistogramCont<Sam>::check_invariant() const noexcept
{
	assert(bin_count_ > 0);
}
#endif 

// --- HistogramDiscr class ---

template<typename Sam, typename CompPolicy>
template<class Iter, class> 
HistogramDiscr<Sam, CompPolicy>::HistogramDiscr(Iter samBegin, Iter samEnd) 
{
    if (samBegin == samEnd) {
        TWIST_THRO2(L"The sample is empty.");
    }
	get_discrete_sample_states_and_freqs(samBegin, samEnd, freq_map_);	
	bin_count_ = freq_map_.size();
	state_values_ = vw::keys(freq_map_) | rg::to<std::set>();
    max_freq_ = *rg::max_element(vw::values(freq_map_));
	TWIST_CHECK_INVARIANT
}

template<typename Sam, typename CompPolicy>
HistogramDiscr<Sam, CompPolicy>* HistogramDiscr<Sam, CompPolicy>::clone() const
{
	TWIST_CHECK_INVARIANT
	return new HistogramDiscr(*this);
}

template<typename Sam, typename CompPolicy>
bool HistogramDiscr<Sam, CompPolicy>::compare(const Histogram<Sam>& other) const
{
	TWIST_CHECK_INVARIANT
	if (auto otherDiscr = dynamic_cast<const HistogramDiscr*>(&other)) {
		return (freq_map_ == otherDiscr->freq_map_) &&
			   (max_freq_ == otherDiscr->max_freq_) &&
			   (bin_count_ == otherDiscr->bin_count_);
	}	
	return false;
}

template<typename Sam, typename CompPolicy>
size_t HistogramDiscr<Sam, CompPolicy>::bin_count() const
{
	TWIST_CHECK_INVARIANT
	return bin_count_;
}

template<typename Sam, typename CompPolicy>
size_t HistogramDiscr<Sam, CompPolicy>::get_frequency(size_t binIdx) const
{
	TWIST_CHECK_INVARIANT
	auto it = freq_map_.begin();
	std::advance(it, binIdx);
	return it->second;
}

template<typename Sam, typename CompPolicy>
size_t HistogramDiscr<Sam, CompPolicy>::max_frequency() const
{	
	TWIST_CHECK_INVARIANT
	return max_freq_;
}

template<typename Sam, typename CompPolicy>
auto HistogramDiscr<Sam, CompPolicy>::sample_value_interval() const -> ClosedInterval<Sam> 
{
	TWIST_CHECK_INVARIANT
    return ClosedInterval<Sam>{*state_values_.begin(), *state_values_.rbegin()};
}

template<typename Sam, typename CompPolicy>
auto HistogramDiscr<Sam, CompPolicy>::state_values() const -> const std::set<Sam>&
{
	TWIST_CHECK_INVARIANT
	return state_values_;
}

#ifdef _DEBUG
template<typename Sam, typename CompPolicy>
void HistogramDiscr<Sam, CompPolicy>::check_invariant() const noexcept
{
	assert(bin_count_ > 0);
}
#endif 

}

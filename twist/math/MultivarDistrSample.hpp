///  @file  MultivarDistrSample.hpp
///  MultivarDistrSample class template definition

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_MATH_MULTIVAR_DISTR_SAMPLE_HPP
#define TWIST_MATH_MULTIVAR_DISTR_SAMPLE_HPP

#include "../DualKeyedList.hpp"

#include "numeric_utils.hpp"
#include "UnivarDistrSample.hpp"

namespace twist::math {

/// Stores a sample of a joint/multi-dimensional distribution (i.e. it stores multiple random variable 
/// samples) and provides some useful sample-related functionality. The samples of specific random variables 
/// can be accessed using the random variable name, its ID, or the position of the random variable within the 
/// joint distribution. This position relates solely to the order in which random variables are added to the 
/// joint distribution. Name matching is case sensitive. 
///
/// @tparam  Sam  The numeric type which represents one sample value
/// @tparam  VarId  Random variable ID type
///
template<typename Sam, typename VarId>
class MultivarDistrSample {
public:
	/// Constructor. Constructs an empty structure, to which random variable samples must be added later.
	///
	/// @param[in] size  The sample size.
	///
	MultivarDistrSample(Ssize size);
	
	/// Constructor. Constructs the multidimensional structure for the sample of a joint distribution, 
	/// containing a number of random variable samples. More random variable samples can be added later.
	///
	/// @param[in] var_ids  The IDs of the variables.
	/// @param[in] var_names  The names of the variables.
	/// @param[in] size  The sample size.
	///
	MultivarDistrSample(const std::vector<VarId>& var_ids, const std::vector<std::wstring>& var_names, 
			Ssize size);
	
	/// Get a clone of the sample structure. This does not include the actual sample values (they will be undefined in 
	/// the clone).
	///
	/// @return  Pointer to the clone. Caller takes ownership.
	///
	MultivarDistrSample* get_struct_clone() const;

	/// Get a clone of the sample, including both the sample structure and the actual sample values.
	///
	/// @return  Pointer to the clone. Caller takes ownership.
	///
	MultivarDistrSample* get_clone() const;
	
	/// Get the sample size.
	///
	/// @return  The size.
	///
	Ssize get_size() const;

	/// Resize the joint distribution sample to contain a different number of values.
	/// If the new size is smaller than the current size, the content of each marginal sample is reduced to its first values 
	/// making up the new size, the rest being dropped. If the new size is greater than the current size, the content of 
	/// each marginal sample is expanded by inserting at the end undefined values, which may cause reallocations.
	///
	/// @param[in] new_size  The new size
	///
	void resize(Ssize new_size);
	
	/// Create a new random variable sample (with undefined contents) to the multi-dimensional distribution 
	/// sample.
	///
	/// @param[in] var_id  The ID of the new random variable; mustn't already exist in the joint distribution
	/// @param[in] var_name  The name of the new random variable; mustn't already exist in the joint 
	///					distribution
	/// @return  The sample of the new random variable (it has undefined contents)
	///
	UnivarDistrSample<Sam>& create_var_sample(VarId var_id, const std::wstring& var_name);

	/// Add an existing random variable sample to the multi-dimensional distribution sample.
	///
	/// @param[in] var_id  The ID of the new random variable; mustn't already exist in the joint distribution
	/// @param[in] var_name  The name of the new random variable; mustn't already exist in the joint 
	///					distribution
	/// @param[in] sample  The existing sample 
	///  
	void add_var_sample(VarId var_id, const std::wstring& var_name, 
			std::unique_ptr<UnivarDistrSample<Sam>> sample);

	/// Get the sample of a random variable from the multi-dimensional distribution sample.
	///
	/// @param[in] var_id  The random variable ID. An exception is thrown if there is no match.
	/// @return  The random variable sample.
	///
	const UnivarDistrSample<Sam>& get_var_sample(VarId var_id) const;

	/// Get the sample of a random variable from the multi-dimensional distribution sample.
	///
	/// @param[in] var_id  The random variable ID. An exception is thrown if there is no match.
	/// @return  The random variable sample.
	///
	UnivarDistrSample<Sam>& get_var_sample(VarId var_id);

	/// Search for the sample of a random variable from the multi-dimensional distribution sample.
	///
	/// @param[in] var_id  The random variable ID.
	/// @return  The random variable sample, or nullptr if no match is found.
	///
	const UnivarDistrSample<Sam>* find_var_sample(VarId var_id) const;

	/// Search for the sample of a random variable from the multi-dimensional distribution sample.
	///
	/// @param[in] var_id  The random variable ID.
	/// @return  The random variable sample, or nullptr if no match is found.
	///
	UnivarDistrSample<Sam>* find_var_sample(VarId var_id);

	/// Get the sample of a random variable from the multi-dimensional distribution sample.
	///
	/// @param[in] var_name  The random variable name. An exception is thrown if there is no match.
	/// @return  The random variable sample.
	///
	const UnivarDistrSample<Sam>& get_var_sample(const std::wstring& var_name) const;

	/// Get the sample of a random variable from the multi-dimensional distribution sample.
	///
	/// @param[in] var_name  The random variable name. An exception is thrown if there is no match.
	/// @return  The random variable sample.
	///
	UnivarDistrSample<Sam>& get_var_sample(const std::wstring& var_name);

	/// Search for the sample of a random variable from the multi-dimensional distribution sample.
	///
	/// @param[in] var_name  The random variable name. 
	/// @return  The random variable sample, or nullptr if no match is found.
	///
	const UnivarDistrSample<Sam>* find_var_sample(const std::wstring& var_name) const;

	/// Search for the sample of a random variable from the multi-dimensional distribution sample.
	///
	/// @param[in] var_name  The random variable name. 
	/// @return  The random variable sample, or nullptr if no match is found.
	///
	UnivarDistrSample<Sam>* find_var_sample(const std::wstring& var_name);

	/// Get the sample of a random variable sample from the multi-dimensional distribution sample.
	///
	/// @param[in] pos  The position of the random variable name within the distribution.
	/// @return  Ref to the random variable sample.
	///
	const UnivarDistrSample<Sam>& get_var_sample_at(Ssize pos) const;

	/// Get the sample of a random variable sample from the multi-dimensional distribution sample.
	///
	/// @param[in] pos  The position of the random variable name within the distribution.
	/// @return  Ref to the random variable sample.
	///
	UnivarDistrSample<Sam>& get_var_sample_at(Ssize pos);

	/// Get the position of a specific variable's sample within the distribution.
	///
	/// @param[in] var_id  The variable ID. An exception is thrown if there is no match.
	/// @return  The position.
	///
	Ssize get_var_sample_pos(VarId var_id) const;

	/// Get the position of a specific variable's sample within the distribution.
	///
	/// @param[in] var_name  The variable name. An exception is thrown if there is no match.
	/// @return  The position.
	///
	Ssize get_var_sample_pos(const std::wstring& var_name) const;

	/// Get the marginal distribution samples for (a subset of) the variables involved in the joint distribution.
	/// 
	/// @param[in] var_names  List specifying the names of the desired variables, and their order. 
	/// @return  The marginal samples
	///
	std::vector<const UnivarDistrSample<Sam>*> get_var_csamples(const std::vector<std::wstring>& var_names) const; 

	/// Get the marginal distribution samples for the variables involved in the joint distribution.
	/// 
	/// @return  The marginal samples, using the default ordering (ie the order in which the variables samples were added)
	///
	std::vector<const UnivarDistrSample<Sam>*> get_var_csamples() const; 

	/// Get the marginal distribution samples for (a subset of) the variables involved in the joint distribution.
	/// 
	/// @param[in] var_names  List specifying the names of the desired variables, and their order. 
	/// @return  The marginal samples
	///
	std::vector<UnivarDistrSample<Sam>*> get_var_samples(const std::vector<std::wstring>& var_names); 

	/// Get the marginal distribution samples for the variables involved in the joint distribution.
	/// 
	/// @return  The marginal samples, using the default ordering (ie the order in which the variables samples 
	///					were added)
	///
	std::vector<UnivarDistrSample<Sam>*> get_var_samples(); 
	
	/// Delete a variable sample from the multi-dimensional distribution sample.
	///
	/// @param[in] var_id  The random variable ID; an exception is thrown if there is no match
	///
	void delete_var_sample(VarId var_id);

	/// Extract a variable sample, and remove it from the from the multi-dimensional distribution sample.
	///
	/// @param[in] var_id  The random variable ID; an exception is thrown if there is no match
	/// @return  The sample
	///
	std::unique_ptr<UnivarDistrSample<Sam>> extract_var_sample(VarId var_id);
	
	/// Delete all random variable samples from the multi-dimensional distribution sample.
	///
	void delete_all_var_samples();
	
	/// Delete a random variable sample from the multi-dimensional distribution sample.
	///
	/// @param[in] var_name  The random variable name. An exception is thrown if there is no match.
	///
	void delete_var_sample(const std::wstring& var_name);

	/// Get the name of a random variable sample from the multi-dimensional distribution sample.
	///
	/// @param[in] pos  The position of the random variable name within the distribution.
	/// @return  The random variable name.
	///
	std::wstring get_var_name_at(Ssize pos) const;

	/// Get the number of random variables in the joint distribution (the dimensionality of the distribution).
	///
	/// @return  The number of random variables.
	///
	Ssize count_vars() const;	

	/// Get the names of all random variables in the joint distribution, in the order they were added.
	///
	/// @return  The names.
	///
	std::vector<std::wstring> get_var_names() const;

	/// Find out whether a sample for a specific variable exists in the joint distribution sample.
	///
	/// @param[in] var_name  The variable name
	/// @return  true if it exists
	///
	bool var_exists(const std::wstring& var_name) const;
	
	/// Apply a rowwise transformation across all the samples in the joint distribution sample. 
	/// Very importantly, the order of the elements in each row of sample values will match the ordering of 
	///   the random variables in the joint distribution. 
	/// See comments for the FuncTransformSamRow concept for further information.
	///
	/// @tparam  Fn  Functor type defining the transformation; must be a refinement of the  
	///					FuncTransformSamRow  concept
	/// @param[in] transform  The actual transformation functor
	///
	template<typename Fn> 
	void transform(Fn transform);

	TWIST_NO_COPY_DEF_MOVE(MultivarDistrSample)

private:
	using SampleList = DualKeyedList<UnivarDistrSample<Sam>, VarId, wchar_t, true/*case_sens*/, true/*owns_objects*/>;

	SampleList sample_list_;
	Ssize size_;

	TWIST_CHECK_INVARIANT_DECL
};

} 

#include "MultivarDistrSample.ipp"

#endif 

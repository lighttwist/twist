/// @file fp_excep_utils.hpp
/// FPExceptionEnabler class definition
/// FPExceptionDisabler class definition

//  Copyright (c) 2007-2025 Dan Ababei
//	All rights reserved
//
//  Inspired by Bruce Dawson, 2012
//  https://www.gamasutra.com/view/news/169203/Exceptional_floating_point.php
// 
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_MATH_FP_EXCEP_UTILS_HPP
#define TWIST_MATH_FP_EXCEP_UTILS_HPP

#include <cfloat>

namespace twist::math {

/*! Declare an object of this type in a scope in order to enable a specified set of floating-point exceptions 
    temporarily. The old exception state will be reset when the object is destroyed.
 */
class FPExceptionEnabler {
public:
	/*! Constructor.
	    \param[in] enable_bits  The bits corresponding to the exceptions to be enables; overflow, divide-by-zero and 
                                invalid-operation are the FP exceptions most frequently associated with bugs
	 */
    explicit FPExceptionEnabler(unsigned int enable_bits = _EM_OVERFLOW | _EM_ZERODIVIDE | _EM_INVALID);

    ~FPExceptionEnabler();

	TWIST_NO_COPY_NO_MOVE(FPExceptionEnabler)

private:
    unsigned int old_bits_{0};
};

/*! Declare an object of this type in a scope in order to suppress all floating-point exceptions temporarily. 
    The old exception state will be reset when the object is destroyed.
 */
class FPExceptionDisabler {
public:
    explicit FPExceptionDisabler();

    ~FPExceptionDisabler();

	TWIST_NO_COPY_NO_MOVE(FPExceptionDisabler)

private:
    unsigned int old_bits_{0};
};

}

#endif 

/// @file histogram_utils.hpp
/// HistogramData, Histogram, HistogramCont and HistogramDiscrete class definitions

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_MATH_HISTOGRAM__UTILS_HPP
#define TWIST_MATH_HISTOGRAM__UTILS_HPP

#include "twist/math/ClosedInterval.hpp"

namespace twist::math {

//! Class containing all the information necessary to draw a histogram (which represents a sample).
class HistogramData {
public:
	using Frequency = unsigned int;

	/*! Constructor.
	    \param[in] frequencies  List containing as many elements as there are bins in the histogram; the value of each 
	                            element is the number of data values that fall into the corresponding bin (the 
	                            frequencies for each bin)
	    \param[in] max_freq  The maximum number of values (frequency) in a bin
	    \param[in] bin_ends  Histogram bin ends, in increasing order
	    \param[in] sample_size  The size of the sample which the histogram represents
	 */
	HistogramData(const std::vector<Frequency>& frequencies, Frequency max_freq, 
			      const std::vector<double>& bin_ends, size_t sample_size);

	//! The number of bins in the histogram.
	size_t num_bins() const;

	/*! Get the list of bin frequencies, that is, a list containing as many elements as there are bins in the 
        histogram; the value of each element is the number of data values that fall into the corresponding bin (the 
        frequency for that bin).
	 */
	const std::vector<Frequency>& frequencies() const;

	//! The maximum number of values (frequency) in a bin.
	Frequency max_freq() const;
	
	//! The histogram bin end points, in increasing order.
	const std::vector<double>& bin_ends() const;

	//! The size of the sample which the histogram represents.
	size_t sample_size() const;

	//! The smallest value in the sample.
	double sample_min() const;

	//! The greatest value in the sample.
	double sample_max() const;

private:
	std::vector<Frequency> frequencies_;
	Frequency max_freq_;
	std::vector<double> bin_ends_;
	size_t sample_size_;

	TWIST_CHECK_INVARIANT_DECL
};

/*! Abstract base (interface) class for histogram classes.
    \tparam Sam  The type of a sample value
 */
template<typename Sam>
class Histogram {
public:
	using SampleValue = Sam; ///< Sample value type

	virtual ~Histogram() = 0;

	//! Get a clone of this histogram object.
	virtual Histogram* clone() const = 0;

	//! Compare this histogram object with another and return true if the two objects are identical.
	virtual bool compare(const Histogram& other) const = 0;

	//! The number of bins in the histogram.
	virtual size_t bin_count() const = 0;

	//! Get the frequency associated with the bin with index \p binIdx in the histogram.
	virtual size_t get_frequency(size_t binIdx) const = 0;

	//! The maximum frequency in the histogram.
	virtual size_t max_frequency() const = 0;	

    //! The interval between the minimum and maximum values in the sample.
    [[nodiscard]] virtual auto sample_value_interval() const -> ClosedInterval<Sam> = 0;

protected:
	Histogram() {}
};

/*! Class representing the histogram of a sample drawn from a continuous random variable.
    \tparam Sam  The type of a sample value
 */
template<typename Sam>
class HistogramCont : public Histogram<Sam> {
public:
	/*! Constructor. Use the other constructor if you know the sample min and max.
	    \tparam Iter  Input iterator type, for addressing the sample values
	    \param[in] samBegin  Iterator addressing the first value in the sample
	    \param[in] samEnd  Iterator addressing one past the final value in the sample
	    \param[in] numBins  The number of bins; note that if the sample is constant (all values are the same) a 
                            histogram with a single bin will result
	 */
	template<class Iter,
	         class = EnableIfInputIterator<Iter, Sam>>
	HistogramCont(Iter samBegin, Iter samEnd, size_t numBins);

	/*! Constructor. Faster than the constructor which does not take the sample min and max.
	    \tparam Iter  Input iterator type, for addressing the sample values
	    \param[in] samBegin  Iterator addressing the first value in the sample
	    \param[in] samEnd  Iterator addressing one past the final value in the sample
	    \param[in] samMin  The smallest sample value
	    \param[in] samMax  The biggest sample value
	    \param[in] numBins  The number of bins; note that if the sample is constant (all values are the same) a 
                            histogram with a single bin will result
	 */
	template<class Iter,
	         class = EnableIfInputIterator<Iter, Sam>>
	HistogramCont(Iter samBegin, Iter samEnd, Sam samMin, Sam samMax, size_t numBins);		
	
	HistogramCont* clone() const override; // Histogram override

	bool compare(const Histogram<Sam>& other) const override; // Histogram override	
	
	size_t bin_count() const override; // Histogram override
	
	size_t get_frequency(size_t binIdx) const override; // Histogram override
	
	size_t max_frequency() const override; // Histogram override

    [[nodiscard]] auto sample_value_interval() const -> ClosedInterval<Sam> override; // Histogram override

    /*! Get the "partition points" of the histogram, ie the values which define the beginning and end of each bin. 
        The values returned are equally spaced between the smallest and the greatest sample value and including these.
     */
    [[nodiscard]] auto partition_points() const -> std::vector<double>;

private:	
	// Copy constructor to be used for cloning.
	HistogramCont(const HistogramCont&) = default;

	HistogramCont(HistogramCont&&) = delete;  

	HistogramCont& operator=(const HistogramCont&) = delete;  

	HistogramCont& operator=(HistogramCont&&) = delete;

	/*! Calculate the histogram parameters.
	    \tparam Iter  Input iterator type, for addressing the sample values
	    \param[in] samBegin  Iterator addressing the first value in the sample
	    \param[in] samEnd  Iterator addressing one past the final value in the sample
	    \param[in] samMin  The smallest sample value
	    \param[in] samMax  The biggest sample value
	    \param[in] numBins  The number of bins; note that if the sample is constant (all values are the same) a 
                            histogram with a single bin will result
	 */
	template<class Iter,
	         class = EnableIfInputIterator<Iter, Sam>>
	void calculate(Iter samBegin, Iter samEnd, Sam samMin, Sam samMax);

	size_t bin_count_;
	std::vector<size_t> freq_list_;
	std::vector<double> partition_points_;
	size_t max_freq_;

	TWIST_CHECK_INVARIANT_DECL
};

/*! Class representing the histogram of a sample drawn from a discrete random variable.
    \tparam Sam  The type of a sample value
    \tparam CompPolicy  Policy for comparing sample values; determines which state values are considered the same state
 */
template<typename Sam, typename CompPolicy>
class HistogramDiscr : public Histogram<Sam>, private CompPolicy {
	using CompPolicy::greater_or_equal;	
public:
	/*! Constructor. The resulting histogram will have as many bins as there are distinct values in the sample.
        If the sample is empty, an exception is raised.
	    \tparam Iter  Input iterator type, for addressing the sample values
	    \param[in] samBegin  Iterator addressing the first value in the sample
	    \param[in] samEnd  Iterator addressing one past the final value in the sample
	 */	
	template<class Iter,
	         class = EnableIfInputIterator<Iter, Sam>>	
	HistogramDiscr(Iter samBegin, Iter samEnd);		
	
	HistogramDiscr* clone() const override; // Histogram override
	
	bool compare(const Histogram<Sam>& other) const override; // Histogram override
	
	size_t bin_count() const override; // Histogram override

	size_t get_frequency(size_t binIdx) const override; // Histogram override
	
	size_t max_frequency() const override; // Histogram override

    [[nodiscard]] auto sample_value_interval() const -> ClosedInterval<Sam> override; // Histogram override

    //! The discrete state values corresponding to the sample bins.
    [[nodiscard]] auto state_values() const -> const std::set<Sam>&;

private:	
	// Predicate for the frequency map, using the comparison policy class. 
	struct FreqMapPred {
		bool operator()(Sam x1, Sam x2) const {
			return !greater_or_equal(x1, x2);
		}
	};

	using FreqMap = std::map<Sam, size_t, FreqMapPred>;

	// Copy constructor to be used for cloning.
	HistogramDiscr(const HistogramDiscr&) = default;

	HistogramDiscr(HistogramDiscr&&) = delete;  

	HistogramDiscr& operator=(const HistogramDiscr&) = delete;  

	HistogramDiscr& operator=(HistogramDiscr&&) = delete;

	size_t bin_count_{0};
	FreqMap freq_map_;
    std::set<Sam> state_values_;
	size_t max_freq_{0};

	TWIST_CHECK_INVARIANT_DECL
};

}

#include "twist/math/histogram_utils.ipp"

#endif 

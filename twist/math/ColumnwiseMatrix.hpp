/// @file ColumnwiseMatrix.hpp
/// ColumnwiseMatrix class template

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_MATH_COLUMNWISE_MATRIX_HPP
#define TWIST_MATH_COLUMNWISE_MATRIX_HPP

namespace twist::math {

/*! A matrix.
    Column operations (especially insertion and deletion) are efficient. 
	Row operations (especially insertion and deletion) are inefficient.
    \tparam T  The type of a cell's value; its requiremets match those of the std::vector element type
 */
template<class T>
class ColumnwiseMatrix {
public:
	using Value = T;

	//! Constructor. Creates a matrix with zero rows and zero columns.
	ColumnwiseMatrix();

	//! Get a clone of the matrix.
	[[nodiscard]] auto clone() const -> ColumnwiseMatrix;

	/*! Get the number of rows in the matrix.
	    \return  The number of columns
	 */
	[[nodiscard]] auto nof_rows() const -> Ssize;

	/*! Get the number of columns in the matrix.
	    \return  The number of columns
	*/
	[[nodiscard]] auto nof_columns() const -> Ssize;

	/*! Get the number of cells in the matrix.
	    \return  The number of columns
	 */
	[[nodiscard]] auto nof_cells() const -> Ssize;

	/*! Get the value in a specific matrix cell.
	    \param[in] row_idx  The row index; the bahaviour is undefined if it is invalid (there is no validity check)
	    \param[in] col_idx  The column index; the bahaviour is undefined if it is invalid (there is no validity check)
	    \return  The cell value
	 */
	[[nodiscard]] auto operator()(Ssize row_idx, Ssize col_idx) const -> const T&;

	/*! Get the value in a specific matrix cell.
	    \param[in] row_idx  The row index; the bahaviour is undefined if it is invalid (there is no validity check)
	    \param[in] col_idx  The column index; the bahaviour is undefined if it is invalid (there is no validity check)
	    \return  The cell value
	 */
	[[nodiscard]] auto operator()(Ssize row_idx, Ssize col_idx) -> T&;

	/*! Get the value in a specific matrix cell.
	    \param[in] row_idx  The row index; an exception is thrown if it is invalid
	    \param[in] col_idx  The column index; an exception is thrown if it is invalid
	    \return  The cell value
	 */
	[[nodiscard]] auto get_cell(Ssize row_idx, Ssize col_idx) const -> const T&;

	/*! Set the value in a specific matrix cell.
	    \param[in] row_idx  The row index; an exception is thrown if it is invalid
	    \param[in] col_idx  The column index; an exception is thrown if it is invalid
	    \param[in] value  The cell value
	 */
	auto set_cell(Ssize row_idx, Ssize col_idx, T value) -> void;

	/*! Add a row at the bottom of the matrix, with the values of the elements in \p row_values.
	    Size of \p row must match the number of columns in the matrix.
	    \return  The new row's index
	*/
	auto add_row(const std::vector<T>& row_values) -> Ssize;

	//! Remove the row with index \p row_idx from the matrix. If the row index is invalid, an exception is thrown.
	auto remove_row(Ssize row_idx) -> void;

	/*! Get read-only access the values in a specific matrix row.
	    \param[in] row_idx  The row index; an exception is thrown if it is invalid
	    \return  The row values
	 */
	[[nodiscard]] auto get_row(Ssize row_idx) const -> std::vector<T>;

	/*! Insert a new column into the matrix.
	   	\param[in] col_idx  The index which the new column will have; an exception is thrown if it is invalid 
		\param[in] col_values  The values in the new column, from top to bottom; if the number of values does not 
		                       match the number of rows, an exception is thrown
	 */
	auto insert_column(Ssize col_idx, std::vector<T> col_values) -> void;

	/*! Remove a column from the matrix. 
	   	\param[in] col_idx  The column index; an exception is thrown if it is invalid 
	 */
	auto remove_column(Ssize col_idx) -> void;

	/*! Remove a column from the matrix, after extraction its values into a container.
	   	\param[in] col_idx  The column index; an exception is thrown if it is invalid
		\return  The column values, top to bottom
	 */
	[[nodiscard]] auto extract_column(Ssize col_idx) -> std::vector<T>;

	TWIST_NO_COPY_DEF_MOVE(ColumnwiseMatrix)

private:	
	Ssize nof_rows_{0};
	std::vector<std::vector<T>> columns_;

	TWIST_CHECK_INVARIANT_DECL
};

}

#include "twist/math/ColumnwiseMatrix.ipp"

#endif 


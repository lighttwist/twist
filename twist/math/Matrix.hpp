///  @file  Matrix.hpp
///  Matrix class template

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_MATH_MATRIX_HPP
#define TWIST_MATH_MATRIX_HPP

#include "LowerTriangularMatrix.hpp"
#include "SymmetricMatrix.hpp"

namespace twist::math {

//+OBSOLETE: This matrix should be replaced by FlatMatrix, and its "special" public methods should become free 
//           functions templated on Matrix type

/// This class represents a matrix of numeric elements. The template argument is the data type of the elements, 
///	  and it should be a numeric type (it should support the usual numeric operations such as addition, 
///	  multiplication, etc).
/// Row and column indexes are always zero-based.
///	The number of rows and the number of columns in the matrix cannot be changed after construction.
///
/// @tparam  T  The type of the matrix elements
///
template<typename T>
class Matrix {
public:
	using Value = T;

	/// The data type of the matrix elements
	using ElemType = T;

	/// Proxy class which makes possible double subscript syntax for instances of Matrix. Constant version.
	class SubscriptProxyConst {
	public:
		inline const T& operator[](Ssize col) const;

	private:
		inline SubscriptProxyConst(const Matrix<T>* constMatrix, Ssize row);

		const Matrix<T>*  matrix_;
		Ssize  row_;

		friend class Matrix<T>;
	};

	/// Proxy class which makes possible double subscript syntax for instances of Matrix.
	class SubscriptProxy {
	public:
		inline T& operator[](Ssize col);

	private:
		inline SubscriptProxy(Matrix<T>* constMatrix, Ssize row);

		Matrix<T>*  matrix_;
		Ssize  row_;

		friend class Matrix<T>;
	};

	/// Constructor. After construction, the elements of the matrix have undefined values.
	///
    /// @param[in] num_rows The number of rows in the matrix
    /// @param[in] num_cols The number of columns in the matrix
    ///
	Matrix(Ssize num_rows, Ssize num_cols);

	/// Copy constructor.
	Matrix(const Matrix& src);

	/// Destructor.
	~Matrix();

	/// Copy the contents of another matrix into this one.
	///
	/// @param[in] src The original matrix, to be copied into this one; this matrix has to have the same 
	///				number of rows and the same number of columns
	///	
	void assign(const Matrix& src);

	/// Copy the contents of another - symmetric - matrix into this one.
	///
	/// @param[in] src The original - symmetric - matrix, to be copied into this one; the number of rows 
	///				and columns of this matrix must equal the size of the symmetric matrix
	///	
	void assign(const SymmetricMatrix<T>& src);

	/// Get read-only access to a specific element of the matrix. This is the first operator called when using 
	/// double subscripts and returns the proxy class which deals with the second operator.
	///
	/// @param[in] row The row index; behaviour is undefined if it is out of bounds.
	/// @return  Proxy class instance
	///
	inline SubscriptProxyConst operator[](Ssize row) const;

	/// Get read-write access to a specific element of the matrix. This is the first operator called when 
	/// using double subscripts and returns the proxy class wich deals with the second operator.
	///
	/// @param[in] row The row index; behaviour is undefined if it is out of bounds
	/// @return  Proxy class instance
	///
	inline SubscriptProxy operator[](Ssize row);

	/// Get read-only access to a specific element of the matrix.
	///
	/// @param[in] row The row index; an exception is thrown if it is invalid
	/// @param[in] col The column index; an exception is thrown if it is invalid
	/// @return  The matrix element
	///
	inline const T& elem(Ssize row, Ssize col) const;

	/// Get read-only access to a specific element of the matrix.
	///
	/// @param[in] row The row index; an exception is thrown if it is invalid
	/// @param[in] col The column index; an exception is thrown if it is invalid
	/// @return  The matrix element
	///
	inline T& elem(Ssize row, Ssize col);	

	/// The number of rows in the matrix.
	Ssize nof_rows() const;

	/// The number of columns in the matrix.
	Ssize nof_columns() const;
	
	/// Set all elements in the matrix to zero.
	void make_zero();

    /// Multiply this matrix with another matrix. This matrix is the left factor in the multiplication. 
    /// The number of rows and column for the three matrices involved must respect the rules of matrix 
	///   multiplication.
    ///
    /// @param[in] right  The other matrix (the right factor in the multiplication); its number of rows must 
	///					equal the the number of columns in this matrix
    /// @param[in] result  The result of the multiplication. It must have the same number of rows as this 
	///					matrix and the same number of columns as 'right'
    ///
	void multiply(const Matrix<T>& right, Matrix<T>& result) const;
	
    /// Subtract another matrix from this matrix. This matrix is the left term in the subtraction. All three 
    /// matrices involved must have the same number of rows and the same number of columns.
    ///
    /// @param[in] right  The other matrix (the right term in the subtraction).
    /// @param[in] result  The result of the subtraction
    ///
	void subtract(const Matrix<T>& right, Matrix<T>& result) const;

    /// Compute the determinant of this matrix (which must be square of course).
    ///
    /// @return  The determinant.    
    ///
	double determinant() const;
	
    /// Compute the determinant and, if possible (ie if the matrix is not singular, which is to say its 
	/// determinant is non-zero) inverse of this matrix (which must be square of course).
    ///
    /// @param[in] det  The determinant
    /// @param[in] inv  If this matrix is not singular, this will be the inverse of it (must be square and 
	///					have the same size as this matrix 'a'
    /// @return  true only if this matrix is not singular (and so its inverse was computed successfully)
    ///
	bool determinant_and_inverse(double& det, Matrix<T>& inv) const;

	/// Replaces this matrix by the LU decomposition of a rowwise permutation of itself. This matrix must be 
	/// square. After calling this method, the matrix will be arranged thus
	///              | U01 U02 U03 ... |
	///              | L10 U11 U12 ... |   (we know Lii to be 1)
	///              | L20 L21 U22 ... |
	///              | ... ... ... ... |
	/// Notes: This function can be used in combination with 'LUBackSubst' to solve linear equations or invert 
	///   a matrix.
	///
	/// @param[in] indx  If 'n' is the size of the matrix, 'indx[0..n-1]' is an output vector that records 
	///					the row permutation effected by the partial pivoting
	/// @param[in] sign  ±1 depending on whether the number of row interchanges was even or odd, respectively.
	///
	void lu_decomposition(std::vector<Ssize>& indx, short& sign);

	/// This method solves the set of n linear equations A·X = B. This matrix must be square.
	/// Let 'n' be the size of the matrix. A[0..n-1, 0..n-1] is this matrix, not as the matrix A but rather as 
	///   its LU decomposition, so you must call first lu_decomposition() on this object.
	/// Notes: Neither this object nor 'indx' are modified by this routine and can be left in place for 
	///	  successive calls with different right-hand sides 'b'. This routine takes into account the 
	///   possibility that 'b' will begin with many zero elements, so it is efficient for use in matrix 
	///   inversion. 	 
	///
	/// @param[in] indx  indx[0..n-1] is input as the permutation vector returned by lu_decomposition()
	/// @param[in,out]  b  Represents B[0..n-1], input as the right-hand side vector B, and output with the 
	///				solution vector X
	///
	void lu_backsubst(const std::vector<Ssize>& indx, std::vector<T>& b) const;
	
	/// Whether this matrix is square and symmetrical.
	///
	/// @param[in] tolerance The tolerance to be used when comparing the values.
	/// @return  'true' only if it is.
	///
	bool is_symmetric(T tolerance = 0) const;
	
    /// Copy the lower triangle (including the main diagonal) of this matrix into a lower-triangular matrix.
    /// Only call this method if this matrix is square (an exception is thrown otherwise).
    ///
    /// @param[in] lt_matrix  The lower-triangular matrix. Must be the same size as this (square) matrix. 
	///
    void get_lower_triangle(LowerTriangularMatrix<T>& lt_matrix) const;
	
    /// Copy this matrix into a vector. You can only call this method if the matrix has only one line, or only 
	/// one column (an exception is thrown otherwise).
    ///
    /// @param[in] vector  The vector; it must have the same size as either the number of columns in the 
	///					matrix (if the matrix has only one row) or the number of rows in the matrix (if the 
	///					matrix has only one column)
    ///
    void as_vector(std::vector<T>& vector) const;

private:
	template<typename U> 
	friend void multiply(const Matrix<U>&, const std::vector<U>&, std::vector<U>&);	

	Matrix& operator=(const Matrix&) = delete;

	T**  elements_{};

    Ssize  row_count_;
    Ssize  col_count_;

	TWIST_CHECK_INVARIANT_DECL
};

} 

#include "Matrix.ipp"

#endif  

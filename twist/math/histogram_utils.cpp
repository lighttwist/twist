/// @file histogram_utils.cpp
/// Implementation file for "histogram_utils.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/math/histogram_utils.hpp"

namespace twist::math {

// --- HistogramData class ---

HistogramData::HistogramData(const std::vector<Frequency>& frequencies, Frequency max_freq, 
		                     const std::vector<double>& bin_ends, size_t sample_size)
	: frequencies_(frequencies)
	, max_freq_(max_freq)
	, bin_ends_(bin_ends)
	, sample_size_(sample_size)
{
	TWIST_CHECK_INVARIANT
}

size_t HistogramData::num_bins() const
{
	TWIST_CHECK_INVARIANT
	return frequencies_.size();
}

const std::vector<HistogramData::Frequency>& HistogramData::frequencies() const
{
	TWIST_CHECK_INVARIANT
	return frequencies_;
}

HistogramData::Frequency HistogramData::max_freq() const
{
	TWIST_CHECK_INVARIANT
	return max_freq_;
}

const std::vector<double>& HistogramData::bin_ends() const
{
	TWIST_CHECK_INVARIANT
	return bin_ends_;
}

size_t HistogramData::sample_size() const
{
	TWIST_CHECK_INVARIANT
	return sample_size_;
}

double HistogramData::sample_min() const
{
	TWIST_CHECK_INVARIANT
	return bin_ends_.front();
}

double HistogramData::sample_max() const
{
	TWIST_CHECK_INVARIANT
	return bin_ends_.back();
}

#ifdef _DEBUG
void HistogramData::check_invariant() const noexcept
{
	assert(!frequencies_.empty());
	assert(max_freq_ > 0);
	assert(bin_ends_.size() == frequencies_.size() + 1);
	assert(sample_size_ > 0);
}
#endif 

}


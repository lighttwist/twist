/// @file algebra.hpp
/// Main include file for algebraic types (such as vectors and matrices) and operations.   
/// Generally, types are defined elsewhere, and operations which are not members of those types, here.

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_MATH_ALGEBRA_HPP
#define TWIST_MATH_ALGEBRA_HPP

#include "twist/math/LowerTriangularMatrix.hpp"
#include "twist/math/Matrix.hpp"
#include "twist/math/SymmetricMatrix.hpp"

namespace twist::math {

/// Multiply a matrix with a (transposed) vector.
/// 
/// @tparam  T  The matrix and vector element type
/// @param[in] left  The matrix (left factor in the multiplication)
/// @param[in] right  The vector (right factor in the multiplication); it must have as many elements as 
///					there are columns in matrix 'left'
/// @param[in] result  The result (transposed) vector; it must have as many elements as there are rows in 
///					matrix 'left'
///
template<typename T>
void multiply(const Matrix<T>& left, const std::vector<T>& right, std::vector<T>& result);

/// Multiply a lower-triangular matrix with a (transposed) vector.
/// 
/// @tparam  T  The matrix and vector element type
/// @param[in] left  The lower-triangular matrix (left factor in the multiplication)
/// @param[in] right  The vector (right factor in the multiplication); must have the same size as the matrix
/// @param[in] result  The result (transposed) vector; must have the same size as the matrix
///
template<typename T>
void multiply(const LowerTriangularMatrix<T>& left, const std::vector<T>& right, std::vector<T>& result);

/// Get a submatrix formed by selecting certain rows, and certain columns, from a (bigger) symmetric matrix. 
///
/// @tparam  T  The matrix element type
/// @param[in] matrix  The symmetric matrix
/// @param[in] rows  The indexes of the rows to be selected
/// @param[in] cols  The indexes of the columns to be selected
/// @param[in] result  The resulting submatrix; its row number size must be the same as the size of the row 
///					index set, and its column number size of the column index set
///
template<typename T>
void submatrix(const SymmetricMatrix<T>& matrix, const std::set<Ssize>& rows, const std::set<Ssize>& cols, 
		Matrix<T>& result);

/// Get a submatrix formed by selecting certain rows, and the same columns, from a larger (super) symmetric 
/// matrix. The rows/columns from the supermatrix can only appear once in the submatrix, and in the same order 
/// they are in the supermatrix. As the rows and columns are the same, the resulting submatrix will also be a 
/// symmetric matrix.
///
/// @tparam  T  The matrix element type
/// @param[in] matrix  The supermatrix
/// @param[in] rows_cols  The indexes of the rows/columns to be selected
/// @param[in] submatrix  The resulting submatrix; its size must be the same as the size of the 
///					row/column index set passed in
///
template<typename T>
void submatrix(const SymmetricMatrix<T>& matrix, const std::set<Ssize>& rows_cols, 
		SymmetricMatrix<T>& submatrix);

/// Get a submatrix formed by selecting certain rows, and the same columns, from a larger (super) symmetric 
/// matrix. The rows/columns from the supermatrix can appear multiple times in the submatrix, and in any 
/// order. As the rows and columns are the same, the resulting submatrix will also be a symmetric matrix.
///
/// @tparam  T  The matrix element type
/// @param[in] matrix  The supermatrix
/// @param[in] rows_cols  The indexes of the rows/columns to be selected; they will appear in the submatrix 
///					in the same order they appear in the list
/// @param[in] submatrix  The resulting submatrix; its size must be the same as the size of the 
///					row/column index list passed in
///
template<typename T>
void submatrix(const SymmetricMatrix<T>& matrix, const std::vector<Ssize>& rows_cols, 
		SymmetricMatrix<T>& submatrix);

/// Compute a specific minor for a symmetric matrix.
///
/// @tparam  T  The matrix element type
/// @param[in] a  The matrix
/// @param[in] i  The row index for the minor
/// @param[in] j  The column index for the minor
/// @param[in] result  The minor; it must be square, with the size smaller by one than the size of 'a'
///
template<typename T>
void minor(const SymmetricMatrix<T> a, Ssize i, Ssize j, Matrix<T>& result);

} 

#include "algebra.ipp"

#endif 

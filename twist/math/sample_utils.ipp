///  @file  sample_utils.ipp
///  Inline implementation file for "sample_utils.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "numeric_utils.hpp"

#include "twist/functor.hpp"

namespace twist::math {

//
//  Local functions
//

namespace detail {

// Set all the ranks corresponding to the tied vector values to the average rank. 
//
// @param  sampleMem  [in] Pointer to the memory buffer where the sample values are stored.
// @param  ranksMem  [in] Pointer to the memory buffer where the ranks are stored.
// @param  firstPtrIdx  [in] The index (within 'valuePtrsBuffer') of the pointer that points to the first tied
//          value (within this vector).
// @param  tieCount  [in] The number of tied values; in other words, the number of successive pointers (within
//			'valuePtrsBuffer') that point to equal vector values.
//
template<class Sam, typename Rnk> 
void set_tied_ranks_average(const Sam* sampleMem, Rnk* ranksMem, std::vector<const Sam*>& valuePtrsBuffer, 
		size_t firstPtrIdx, size_t tieCount)
{
    // Compute the (one-based) average rank for the ties.
    Rnk rank = static_cast<Rnk>((firstPtrIdx + 1) + (tieCount - 1) / 2.0);
    // Set the rank in the corresponding position for each sample value that is tied.
    for (size_t ptrIdx = firstPtrIdx; ptrIdx < firstPtrIdx + tieCount; ptrIdx++) {
        const Sam* ptr = valuePtrsBuffer[ptrIdx];
        // Calculate the index of the value that 'ptr' points at within 'sample' (using pointer arithmetic).
        const size_t sampleIdx = ptr - sampleMem;
        ranksMem[sampleIdx] = rank;
    }
}

// Set all the ranks corresponding to the tied vector values to the bracket rank. 
//
// @param  sampleMem  [in] Pointer to the memory buffer where the sample values are stored.
// @param  ranksMem  [in] Pointer to the memory buffer where the ranks are stored.
// @param  firstPtrIdx  [in] The index (within 'valuePtrsBuffer') of the pointer that points to the first tied
//          value (within this vector).
// @param  tieCount  [in] The number of tied values; in other words, the number of successive pointers (within
//			'valuePtrsBuffer') that point to equal vector values.
//
template<class Sam, typename Rnk> 
void set_tied_ranks_bracket(const Sam* sampleMem, Rnk* ranksMem, std::vector<const Sam*>& valuePtrsBuffer, 
		size_t firstPtrIdx, size_t tieCount)
{
    // Compute the (one-based) bracket rank for the ties.
	const Rnk rank = static_cast<Rnk>(firstPtrIdx + tieCount - 1);
    // Set the rank in the corresponding position for each sample value that is tied.
    for (size_t ptrIdx = firstPtrIdx; ptrIdx < firstPtrIdx + tieCount; ptrIdx++) {
        const Sam* ptr = valuePtrsBuffer[ptrIdx];
        // Calculate the index of the value that 'ptr' points at within 'sample' (using pointer arithmetic).
        const size_t sampleIdx = ptr - sampleMem;
        ranksMem[sampleIdx] = rank;
    }
}

} 

//
//  Global functions
//

template<typename Iter>
double calc_sample_mean(Iter sample_begin, Iter sample_end)
{
	const double mean = std::accumulate(sample_begin, sample_end, 0.0);  
    return mean / std::distance(sample_begin, sample_end);
}


template<class Rng, class>
double calc_sample_mean(const Rng& sample)
{
	return calc_sample_mean(begin(sample), end(sample));
}


template<class Iter>
void calc_sample_params(Iter sample_begin, Iter sample_end, double& mean, double& stddev)
{
    const double n = static_cast<double>(std::distance(sample_begin, sample_end));
    
    // First pass to get the mean.
    mean = std::accumulate(sample_begin, sample_end, 0.0);  
    mean /= n;  

    // Second pass to get the variance.
    double ep = 0;
    double variance = 0;
    double sum = 0;
    for (auto it = sample_begin; it != sample_end; ++it) {
        sum = *it - mean;
        ep += sum;
        variance += sum * sum;
    }
    variance = (variance - ep * ep / n) / (n - 1);

    stddev = sqrt(variance);   
}


template<class Rng, class>
void calc_sample_params(const Rng& sample, double& mean, double& stddev)
{
	calc_sample_params(begin(sample), end(sample), mean, stddev);
}


template<class Sam, class Rng, class>
void calc_sample_params(const Rng& sample, double& mean, double& stddev, Sam& min, Sam& max)
{
    const size_t n = sample.size();

    // First pass to get the mean.
    mean = 0;
    min = *begin(sample);
    max = min;
    for (const auto& el : sample) {
        mean += el;
        if (el < min) {
			min = el;
        }
        if (el > max) {
			max = el;
        }
    }
    mean /= n;

    // Second pass to get the variance.
    double ep = 0;
    double variance = 0;
    double sum = 0;
    for (const auto& el : sample) {
        sum = el - mean;
        ep += sum;
        variance += sum * sum;
    }
    variance = (variance - ep * ep / n) / (n - 1);

    stddev = sqrt(variance); 
}


template<class Sam, typename Iter>
void get_sample_range(Iter sample_begin, Iter sample_end, Sam& min, Sam& max)
{
	if (sample_begin == sample_end) {
		TWIST_THROW(L"The sample is empty.");
	}
	auto minmax = std::minmax_element(sample_begin, sample_end);
	min = *minmax.first;
	max = *minmax.second;
}


template<class Sam> 
std::unique_ptr<UnivarDistrSampleInfo<Sam>> get_sample_info(const UnivarDistrSample<Sam>& sample, bool discrete)
{
	std::unique_ptr<UnivarDistrSampleInfo<Sam>> samInfo;

	// Calculate sample parameters
	double mean = 0;
	double stddev = 0;
	Sam min_val = 0;
	Sam max_val = 0;
	calc_sample_params(sample, mean, stddev, min_val, max_val);

	if (!discrete) {
		// The sample comes from a continuous distribution.
		samInfo = UnivarDistrSampleInfo<Sam>::create_cont_sample_info(sample.size(), min_val, max_val, mean, stddev);
	}
	else {    
		// The sample comes from a discrete distribution.
		typename UnivarDistrSampleInfo<Sam>::SamSet distinct_discr_values;
		get_discrete_sample_states(sample.begin(), sample.end(), distinct_discr_values);
		samInfo = UnivarDistrSampleInfo<Sam>::create_discr_sample_info(sample.size(), *distinct_discr_values.begin(), 
				*distinct_discr_values.rbegin(), mean, stddev, distinct_discr_values);
	}

	return samInfo;
}


template<typename Iter, class Set>
void get_discrete_sample_states(Iter sample_begin, Iter sample_end, Set& state_values)
{
	state_values.clear();
	for (Iter it = sample_begin; it != sample_end; ++it) {
		state_values.insert(*it);
	}
}


template<typename Iter, class Map>
void get_discrete_sample_states_and_freqs(Iter sample_begin, Iter sample_end, Map& state_map)
{
	state_map.clear();
	for (Iter sam_it = sample_begin; sam_it != sample_end; ++sam_it) {
		auto map_it = state_map.find(*sam_it);
		if (map_it != end(state_map)) {
			map_it->second = map_it->second + 1;
		}
		else {
			state_map.insert(std::make_pair(*sam_it, 1));
		}
	}
}


template<class Iter>
double calc_quantile(Iter sample_begin, Iter sample_end, double prob)
{
	assert(std::is_sorted(sample_begin, sample_end));

	double quantile = 0;

	// See the help for Matlab's  quantile()  function for this implementation

	// Quantiles are specified using cumulative probabilities, from 0 to 1.
    // For an N element vector X, quantiles are computed as follows:
    //   1) The sorted values in X are taken as the (0.5/N), (1.5/N),..., ((N-0.5)/N) quantiles.
    //   2) Linear interpolation is used to compute quantiles for probabilities between (0.5/N) and ((N-0.5)/N)
    //   3) The minimum or maximum values in X are assigned to quantiles for probabilities outside that range.

	const size_t n = std::distance(sample_begin, sample_end);

	const double min_p = 0.5 / n;
	const double max_p = (n - 0.5) / n;

	double quantile_pos = 0;
	if ((prob >= min_p) && (prob <= max_p)) {
		quantile_pos = (prob - min_p) / ((n - 1.0) / n) * (n - 1);
	}
	else if (prob > max_p) {
		quantile_pos = static_cast<double>(n - 1);
	}
	
	const size_t sample_pos = static_cast<size_t>(floor(quantile_pos));
	
	auto pos_it = sample_begin;
	std::advance(pos_it, sample_pos);

    if ((sample_pos < quantile_pos) && (sample_pos < n - 1)) {
		// We use simple linear interpolation.
		auto next_pos_it = pos_it;
		next_pos_it++;		
        quantile = *pos_it + (*next_pos_it - *pos_it) * (quantile_pos - sample_pos);    
	}
	else {
        quantile = *pos_it;
	} 
	
	return quantile;
}


template<class Rng, class>
double calc_quantile(const Rng& sample, double prob)
{
	return calc_quantile(begin(sample), end(sample), prob);
}


template<class Rng, class>
bool get_bounding_quantiles(const Rng& sample, double value, double& lo_quantile, double& hi_quantile)
{
	assert(sample.is_sorted());
	
	bool ret = false;

	// Put together a set of the usual quantiles, and associated probabilities, with a probability step of 0.05.
	const double k_prob_step = 0.05;
	const size_t num_prob = round_to_int(1.0 / k_prob_step);

	double quantile = 0;
	std::set<double> quantiles;
	std::set<double> quant_probs;  
	for (size_t i = 0; i < num_prob; ++i) {
		quantile = calc_quantile(sample, i * k_prob_step);
		quantiles.insert(quantile);
		quant_probs.insert(i * k_prob_step);
	}
	
	// Find out where the value falls.
	typedef std::set<double>::const_iterator SetIter;
	SetIter hi_it = quantiles.upper_bound(value);
	if (hi_it != begin(quantiles) && hi_it != end(quantiles)) {
		
		SetIter lo_it = hi_it;		
		--lo_it;
		const size_t lo_pos = std::distance<SetIter>(begin(quantiles), lo_it);
		const size_t hi_pos = std::distance<SetIter>(begin(quantiles), hi_it);
		
		if (value == *hi_it) {
			lo_quantile = *iter_at(quant_probs, hi_pos);
			hi_quantile = *iter_at(quant_probs, hi_pos);
		}
		else if (value == *lo_it) {
			lo_quantile = *iter_at(quant_probs, lo_pos);
			hi_quantile = *iter_at(quant_probs, lo_pos);
		}
		else {
			lo_quantile = *iter_at(quant_probs, lo_pos);
			hi_quantile = *iter_at(quant_probs, hi_pos);		
		}		
		ret = true;
	}
	
	return ret;
}


template<class Sam>
HistogramData calc_cont_histogram_params(const UnivarDistrSample<Sam>& sample, size_t num_bins)
{
	Sam sample_min = 0;
	Sam sample_max = 0;
	sample.min_max_val(sample_min, sample_max);
	return calc_cont_histogram_params(sample, num_bins, sample_min, sample_max);
}


template<class Sam>
HistogramData calc_cont_histogram_params(const UnivarDistrSample<Sam>& sample, size_t num_bins, 
		Sam sample_min, Sam sample_max)
{
	const unsigned int sample_size = static_cast<unsigned int>(sample.size());
    
    // Compute the bin ends
	std::vector<double> bin_ends(num_bins + 1);
    bin_ends[0] = sample_min;
    bin_ends[num_bins] = sample_max;
    
    const double bin_length = (sample_max - sample_min) / static_cast<double>(num_bins);
    for (size_t i = 1; i < num_bins; i++) {
        bin_ends[i] = bin_ends[i - 1] + bin_length;
    }

    // Compute the bin frequencies    
    std::vector<unsigned int> frequencies(num_bins);
    unsigned int max_freq = 0;

    if (bin_length > 0) {
		size_t bin_idx = 0;
        for (size_t i = 0; i < sample_size; i++) {
            bin_idx = static_cast<size_t>(floor((sample[i] - sample_min) / bin_length));
            // Some numerical problems may occur at the end.
            if (bin_idx >= num_bins) {
                bin_idx = num_bins - 1;
            }
            frequencies[bin_idx] = frequencies[bin_idx] + 1;
            if (max_freq < frequencies[bin_idx]) {
                max_freq = frequencies[bin_idx];
            }
        }
    }
    else {
        // Deal with constants.
        frequencies[0] = sample_size;
        max_freq = sample_size;
    }

	return HistogramData(frequencies, max_freq, bin_ends, sample_size);
}


template<typename Iter> 
double compute_pmc_between_samples(Iter sampleBeginX, Iter sampleEndX, Iter sampleBeginY, Iter sampleEndY)
{
	double retVal = 0;

    const size_t n = std::distance(sampleBeginX, sampleEndX);
    if (n != static_cast<size_t>(std::distance(sampleBeginY, sampleEndY))) {
        TWIST_THROW(L"The two samples must have the same size.");
    }
	if (n == 0) {
		TWIST_THROW(L"Cannot compute the correlation between two empty sample sets.");
	}

	double yt = 0;
	double xt = 0;

	double syy = 0;
	double sxy = 0;
	double sxx = 0;
	double  ay = 0;
	double  ax = 0;

	// Find the means.
	Iter itX = sampleBeginX;
	Iter itY = sampleBeginY;
	for (; itX != sampleEndX; ++itX, ++itY) {	
		ax += *itX;
		ay += *itY;
	}
	ax /= n;
	ay /= n;

	// Compute the correlation coefficient.
	itX = sampleBeginX;
	itY = sampleBeginY;
	for (; itX != sampleEndX; ++itX, ++itY) {	
		xt = *itX - ax;
		yt = *itY - ay;
		sxx += xt * xt;
		syy += yt * yt;
		sxy += xt * yt;
	}

	if ((sxx != 0) && (syy != 0)) {
		retVal = sxy / ::sqrt(sxx * syy);
	}

	return retVal;
}


template<class Sam, class Rnk> 
void rank_sample(const UnivarDistrSample<Sam>& sample, std::vector<Rnk>& ranks, 
		std::vector<const Sam*>& valuePtrsBuffer, TiedRankMethod tiedRankMethod)
{
	void (*funcTiedRank)(const Sam*, Rnk*, std::vector<const Sam*>&, size_t, size_t);

	if (tiedRankMethod == TiedRankMethod::average) {
		funcTiedRank = detail::set_tied_ranks_average<Sam, Rnk>;
	}
	else if (tiedRankMethod == TiedRankMethod::bracket) {
		funcTiedRank = detail::set_tied_ranks_bracket<Sam, Rnk>;
	}
	else {
        TWIST_THROW(L"Invalid tied rank type");
	}

	const size_t sampleSize = sample.size();
    if ((ranks.size() != sampleSize) || (valuePtrsBuffer.size() != sampleSize)) {   
        TWIST_THROW(L"The three containers passed in must have the same size");
    }

    // First we create a list of sorted pointers in the buffer passed in (they will point to increasing values of this vector).
    sample.get_pointers(valuePtrsBuffer.begin());
    std::sort(valuePtrsBuffer.begin(), valuePtrsBuffer.end(), PointeeLessThan{});

    // Initialise tie information.
    size_t tieCount = 1;
    Sam prevValue = 0;

    // Loop through all the sorted pointers. For each pointer, calculate the index of the value it points at within
    // @param  sample', and set the corresponding value in 'ranks' to this (one-based) position. Ties are treated differently.
    for (size_t ptrIndex = 0; ptrIndex < sampleSize; ptrIndex++) {

        const Sam* ptr = valuePtrsBuffer[ptrIndex];
        // Calculate the index of the value that 'ptr' points at within the sample vector (using pointer arithmetic).
        const size_t sampleIndex = ptr - sample.buffer();

        // Check for tie (but not during the first iteration).
        if ((*ptr != prevValue) || (ptrIndex == 0)) {
            // We are not in a tie.
            // Let us check whether we have just come out of a tie.
            if (tieCount > 1) {
                // We have just come out of a tie. Set all the ranks corresponding to the tied sample values to the
                // average rank.
                funcTiedRank(sample.buffer(), &ranks[0], valuePtrsBuffer, ptrIndex - tieCount, tieCount);
                // Reset tie information.
                tieCount = 1;
            }
            // Set the 'ranks' value corresponding to 'ptr' to the (one-based) position of 'ptr' in the pointers list.
            ranks[sampleIndex] = static_cast<Rnk>(ptrIndex + 1);
            prevValue = *ptr;
        }
        else {
            // We are in a tie.
            tieCount++;
            // Normally we will not have a rank to write until we are out of the tie; the one exception is if we have
            // reached the end of the sample while in the tie.
            if (ptrIndex == sampleSize - 1) {
                funcTiedRank(sample.buffer(), &ranks[0], valuePtrsBuffer, ptrIndex - tieCount + 1, tieCount);    
            }
        }
    }
}


template<class Sam, class VarId>
void calc_rank_corr_matrix(const MultivarDistrSample<Sam, VarId>& sample, 
		SymmetricMatrix<double>& rank_corr_matrix) 
{
	const auto numVars = sample.count_vars();
	if (rank_corr_matrix.size() != numVars) {
		TWIST_THROW(L"The output matrix has the wrong size.");
	}

	rank_corr_matrix.set_diagonal(1);

	const size_t sample_size = sample.get_size();

	std::vector<float> iRankedSample(sample_size);
	std::vector<float> jRankedSample(sample_size);
	std::vector<const Sam*> samplePtrsBuffer(sample_size);

    for (auto i = 0; i < numVars - 1; ++i) {
        // Get the sample for the first random variable.
        const UnivarDistrSample<Sam>& iSample = sample.get_var_sample_at(i);
        // Rank the sample.
        rank_sample(iSample, iRankedSample, samplePtrsBuffer, TiedRankMethod::average);        
        // Transform the ranked sample to uniform on (0, 1). We divide each rank by "samplesize + 1" in order to stay
        // in the open interval. 
        for (auto it = begin(iRankedSample); it != end(iRankedSample); ++it) {
			*it = *it / (sample_size + 1);
        }

        for (auto j = i + 1; j < numVars; ++j) {
			// Get the sample for the second random variable.
			const UnivarDistrSample<Sam>& jSample = sample.get_var_sample_at(j);
            // Rank the sample.
            rank_sample(jSample, jRankedSample, samplePtrsBuffer, TiedRankMethod::average);
            // Transform the ranked sample to uniform on (0, 1).  We divide each rank by "samplesize + 1" in order to
            // stay in the open interval. 
			for (auto it = begin(jRankedSample); it != end(jRankedSample); ++it) {
				*it = *it / (sample_size + 1);
			}

            // Compute, and store, the correlation coefficient between the two rank (uniform) samples.
            rank_corr_matrix[i][j] = compute_pmc_between_samples(iRankedSample.begin(), iRankedSample.end(), 
					jRankedSample.begin(), jRankedSample.end());
        }
    }	
}

}



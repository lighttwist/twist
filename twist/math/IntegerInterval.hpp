///  @file  IntegerInterval.hpp
///  IntegerInterval class template

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_MATH_INTEGER_INTERVAL_HPP
#define TWIST_MATH_INTEGER_INTERVAL_HPP

#include "twist/globals.hpp"

namespace twist::math {

/*! Class representing a closed interval in Z, the integer number set (as such, neither bound can be infinite). 
    The template argument is the type of the interval bounds, and it should be an integer number type.
 */
template<typename T>
class IntegerInterval {
public:
	/// Constructor.
	///
	/// @param  lo  [in] The low interval bound.
	/// @param  hi  [in] The high interval bound. Must be greater or equal to the low bound.
	///
	IntegerInterval(T lo, T hi);
	
	/// Equality operator.
	///
	bool operator==(const IntegerInterval& rhs) const;
	
	/// Get the high bound of the interval.
	///
	/// @return  The high bound.
	///
	T hi() const;
	
	/// Get the low bound of the interval.
	///
	/// @return  The high bound.
	///
	T lo() const;

	/// Get the interval length (the difference between its upper and lower bound).
	///
	/// @return  The length
	//
	T length() const;
	
	/// Find out whether this interval includes another.
	///
	/// @param  other  [in] The other interval.
	/// @return  true if the other interval is included in this interval.
	///
	bool includes(const IntegerInterval& other) const;

	/// Find out whether this interval contains a point.
	///
	/// @param  point  [in] The point.
	/// @return  true if the interval contains the point.
	///
	bool contains(T point) const;
		
	/// Get a textual representation of the interval.
	///
	/// @return  The text.
	///
	std::wstring as_string() const;

private:
	T  lo_;
	T  hi_;

	TWIST_CHECK_INVARIANT_DECL
};

// --- Global functions ---

/*! Given a point, get the nearest point to it which falls within an integer interval (enclose the point in an 
    interval). If the original point falls within the interval, the result will be itself; otherwise the result will be 
    the nearest interval bound.
    \param[in] point  The original point
    \param[in] interv  The interval
    \return  The nearest interval point
 */
template<typename T>
[[nodiscard]] auto enclose(T point, const IntegerInterval<T>& interv) -> T; //+TODO: Consider renaming to clamp()

}

#include "twist/math/IntegerInterval.ipp"

#endif 

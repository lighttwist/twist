///  @file  SymmetricMatrix.hpp
///  SymmetricMatrix class template 

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_MATH_SYMMETRIC_MATRIX_HPP
#define TWIST_MATH_SYMMETRIC_MATRIX_HPP

#include "LowerTriangularMatrix.hpp"

namespace twist::math {

template<typename T> class Matrix;

/// This class represents a symmetric matrix, ie a square matrix that is equal to its transpose; in other 
///   words, for all matrix elements a[i,j] the following holds a[i,j] == a[j,i].
/// Row and column indexes are always zero-based.
/// The size of the matrix cannot be changed after construction.
///
/// @tparam  T  The type of the matrix elements
///
template<typename T>
class SymmetricMatrix {
public:
	using Value = T;

	/// The data type of the matrix elements
	using ElemType = T;

	/// Proxy class which makes possible double subscript syntax for instances of SymmetricMatrix. 
	/// Constant version.
	class SubscriptProxyConst {
	public:
		inline const T& operator[](Ssize col) const;

	private:
		SubscriptProxyConst(const SymmetricMatrix<T>* matrix, Ssize row);

		const SymmetricMatrix<T>*  matrix_;
		Ssize  row_;

		friend class SymmetricMatrix<T>;
	};

	/// Proxy class which makes possible double subscript syntax for instances of SymmetricMatrix.
	class SubscriptProxy {
	public:
		inline T& operator[](Ssize col);

	private:
		SubscriptProxy(SymmetricMatrix<T>* matrix, Ssize row);

		SymmetricMatrix<T>*  matrix_;
		Ssize  row_;

		friend class SymmetricMatrix<T>;
	};

	/// Constructor. After construction, the elements of the matrix have undefined values.
	///
    /// @param[in] size  The size of the matrix (ie the number of rows, which is the same as the number 
	///				of columns)
    ///
	SymmetricMatrix(Ssize size);
	
	/// Copy constructor.
	SymmetricMatrix(const SymmetricMatrix& src);

	/// Destructor.
	~SymmetricMatrix();

	/// Copy the contents of another symmetric matrix into this one.
	///
	/// @param[in] src  The other, original symmetric matrix. It must have the same size as this matrix.
	///
	void assign(const SymmetricMatrix& src);

	/// Get read-only access to a specific element of the matrix. This is the first operator called when using double 
	/// subscripts and returns the proxy class which deals with the second operator.
	///
	/// @param[in] row  The row index. Behaviour is undefined if it is out of bounds.
	/// @return  Proxy class instance.
	///
	inline SubscriptProxyConst operator[](Ssize row) const;

	/// Get read-write access to a specific element of the matrix. This is the first operator called when using double 
	/// subscripts and returns the proxy class which deals with the second operator.
	///
	/// @param[in] row  The row index. Behaviour is undefined if it is out of bounds.
	/// @return  Proxy class instance.
	///
	inline SubscriptProxy operator[](Ssize row);

	/// Get read-only access to a specific element of the matrix.
	///
	/// @param[in] row  The row index. An exception is thrown if it is invalid.
	/// @param[in] col  The column index. An exception is thrown if it is invalid.
	/// @return  The matrix element.
	///
	inline const T& elem(Ssize row, Ssize col) const;
	
	/// Get read-write access to a specific element of the matrix.
	///
	/// @param[in] row  The row index. An exception is thrown if it is invalid.
	/// @param[in] col  The column index. An exception is thrown if it is invalid.
	/// @return  The matrix element.
	///
	inline T& elem(Ssize row, Ssize col);

	/// The size of the matrix (ie the number of rows, which is the same as the number of columns).
	Ssize size() const;

	/// The number of rows in the matrix (which is the same as the number of columns).
	Ssize nof_rows() const;

	/// The number of columns in the matrix (which is the same as the number of rows).
	Ssize nof_columns() const;

	/// Set all elements in the matrix to zero.
	void make_zero();

    /// Sets all elements on the main diagonal of the matrix to a specific value.
    ///
    /// @param[in] value  The value
    ///
	void set_diagonal(const T& value);  
	
    /// Subtract another matrix from this matrix. This matrix is the left term in the subtraction. 
	/// All three matrices involved must have the same size.
    ///
    /// @param[in] right  The other matrix (the right term in the subtraction)
    /// @param[in] result  The result of the subtraction
    ///
	void subtract(const Matrix<T>& right, Matrix<T>& result) const; 

	/// Subtract another symmetric matrix from this matrix. This matrix is the left term in the subtraction. 
	/// All three matrices involved must have the same size.
    ///
    /// @param[in] right  The other matrix (the right term in the subtraction)
    /// @param[in] result  The result of the subtraction
    ///
	void subtract(const SymmetricMatrix<T>& right, SymmetricMatrix<T>& result) const; 
    
    /// Compute the determinant of this matrix.
    ///
    /// @return  The determinant.    
    ///
	double determinant() const;
	
    /// Compute the determinant and, if possible (ie if the matrix is not singular, which is to say its 
	/// determinant is non-zero) inverse of this matrix  (which must be square of course).
    ///
    /// @param[in] det  The determinant
    /// @param[in] inv  If this matrix is not singular, this will be the inverse of it (must be square and 
	///					have the same size as this matrix)
    /// @return  true only if this matrix is not singular (and so its inverse was computed successfully)
    ///
	bool determinant_and_inverse(double& det, Matrix<T>& inv) const;
	
    /// Perform Cholesky decomposition on this matrix. This matrix must be positive definite, or an exception 
	/// will be thrown at some point in the computation.
    ///
    /// @param[in] lt_matrix  Matrix that will be populated with the contents of the lower triangular matrix 
	///					from the Cholesky decomposition; it must have the same size as this matrix
    ///
    void cholesky(LowerTriangularMatrix<T>& lt_matrix) const;

private:
	SymmetricMatrix& operator=(const SymmetricMatrix&) = delete;

	// Pointer to the memory block where the elements are stored. Only half (one triangle) of the matrix is 
	// actually stored, as the other half mirrors it. The whole main diagonal is stored.
	T**  elements_;

    Ssize  size_;

	TWIST_CHECK_INVARIANT_DECL
};

} 

#include "SymmetricMatrix.ipp"

#endif 

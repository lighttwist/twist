///  @file  RandNumGenerator.cpp
///  Implementation file for "RandNumGenerator.hpp"

//   Copyright (c) 1997-2019, Makoto Matsumoto, Takuji Nishimura, Daniel Lewandowski and Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "RandNumGenerator.hpp"

namespace twist { namespace math {

unsigned long  RandNumGenerator::state_[k_n] = {0x0UL};
int			   RandNumGenerator::p_ = 0;
bool		   RandNumGenerator::init_ = false;


void RandNumGenerator::gen_state() 
{
	for (int i = 0; i < (k_n - k_m); ++i) {
		state_[i] = state_[i + k_m] ^ twiddle(state_[i], state_[i + 1]);
	}
	for (int i = k_n - k_m; i < (k_n - 1); ++i) {
		state_[i] = state_[i + k_m - k_n] ^ twiddle(state_[i], state_[i + 1]);
	}
	state_[k_n - 1] = state_[k_m - 1] ^ twiddle(state_[k_n - 1], state_[0]);
	p_ = 0;
}


void RandNumGenerator::set_seed(unsigned long s) 
{
	state_[0] = s;
	for (int i = 1; i < k_n; ++i) {
		state_[i] = 1812433253UL * (state_[i - 1] ^ (state_[i - 1] >> 30)) + i;
	}
	p_ = k_n;
}


void RandNumGenerator::set_seed_random() 
{
	state_[0] = (unsigned long)clock();
	for (int i = 1; i < k_n; ++i) {
		state_[i] = 1812433253UL * (state_[i - 1] ^ (state_[i - 1] >> 30)) + i;
	}
	p_ = k_n;
}


double RandNumGenerator::get_sample()
{
	return (static_cast<double>(rand_int32()) + .5) * (1. / 4294967296.); 
}

} } // namespace math, namespace twist

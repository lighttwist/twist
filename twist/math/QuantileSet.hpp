///  @file  QuantileSet.hpp
///  QuantileSet class template defintion

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_MATH_QUANTILE_SET_HPP
#define TWIST_MATH_QUANTILE_SET_HPP

#include "numeric_utils.hpp"

namespace twist::math {

/// Immutable set of quantiles, with their associated probabilities.
/// Guaranteed to contain at least two quantiles.
///
/// @tparam  NumTol  Class wich encodes the number type and tolerance value; must be an instance of the 
///					NumTol concept
///
template<class NumTol>
class QuantileSet {
public:
	using T = typename NumTol::Type;
	using NumericSet = NumericSetTol<NumTol>;
	using NumericMap = NumericMapTol<NumTol>;

	/// Constructor
	///
	/// @param  quant_vals  [in] Set containing the quantile values. Must contain at least two values.
	/// @param  quant_probs  [in] Set containing the corresponding probabilities.
	///
	QuantileSet(const NumericSet& quant_vals, const NumericSet& quant_probs);
	
	/// Get the set size
	///
	/// @return  The size
	///
	size_t size() const;

	/// Get the quantile values.
	///
	/// @return  Set of values
	///
	const NumericSet& vals() const;

	/// Get the quantile probabilities.
	///
	/// @return  Set of probabilities
	///
	const NumericSet& probs() const;
	
	/// Get the lowest quantile value.
	///
	/// @return  The quantile value
	///
	T lowest_val() const;
	 
	/// Get the highest quantile value.
	///
	/// @return  The value.
	///
	T highest_val() const;
	
	/// Get the lowest quantile probability
	///
	/// @return  The probability
	///
	T lowest_prob() const;
	 
	/// Get the highest quantile probability
	///
	/// @return  The probability
	///
	T highest_prob() const;
	
	/// Get the probability corresponding to a quantile value. 
	/// The method can be used to work only for exact matches, or to interpolate if the value falls between 
	/// existing quantile values. 
	///
	/// @param  quant_val  The quantile value. An exception is raised if it falls outside the range of 
	///				quantiles in the set.
	/// @param  interpolate  If false, the method only works if the quantile value exists in the set. An 
	///				exception is raised otherwise. If true, the method interpolates if the value falls between 
	///				exiting values. 
	///
	T get_prob(T quant_val, bool interpolate = true) const;
	
	/// Get the quantile value corresponding to a probability. 
	/// The method can be used to work only for exact matches, or to interpolate if the probability falls 
	/// between existing quantile probabilities. 
	///
	/// @param  quant_prob  The probability. An exception is raised if it falls outside the range of quantile 
	///				probabilities in the set.
	/// @param  interpolate  If false, the method only works if the quantile probability exists in the set. 
	///				An exception is raised otherwise. If true, the method interpolates if the probability 
	///				falls between exiting quantile probabilities. 
	///
	T get_val(T quant_prob, bool interpolate = true) const;

	/// Get the set as a map with values as keys and corresponding probabilities as values.
	///
	/// @return  The map.
	///
	NumericMap as_map() const;
	
private:
	T get_other(T val, bool is_quant, bool interpolate) const;

	NumericSet  quant_vals_;
	NumericSet  quant_probs_;

	TWIST_CHECK_INVARIANT_DECL	
};

} 

#include "QuantileSet.ipp"

#endif

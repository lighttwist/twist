///  @file  SampleFileMiner.hpp
///  SampleFileMiner abstract class

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_MATH_SAMPLE_FILE_MINER_HPP
#define TWIST_MATH_SAMPLE_FILE_MINER_HPP

#include "twist/ProgressProv.hpp"

#include "MultivarDistrSample.hpp"

namespace twist::math {

///
///  This is an abstract base class for all classes which read the data from one (or more) sample files into a random 
///  variable bag.
///
class SampleFileMiner : public NonCopyable {
public:
	// Functor which checks whether a random variable name is valid (doesn't contain forbidden characters). The first parameter 
	// is the variable name to be examined, the second is a message explaining the error in the name (if there is one).
	typedef std::function<bool (const std::wstring&, std::wstring&)>  IsValidRandVarName;
	
	// The random variable ID is the zero-based position of the variable column in the sample file 
	typedef MultivarDistrSample<float, size_t>  MultivarSample;  

	/// Destructor.
	///
	virtual ~SampleFileMiner();
	
	/// Analyse the sample file. This must be called before  mine()  and reads general information such as the number of random 
	/// variables and the number of samples.
	///
	virtual void analyse() = 0;

	/// Read all sample data from the sample file into this class. Call this after calling  analyse() .
	///
	virtual void mine() = 0;	

	/// Find out whether the sample file has been analysed.
	///
	/// @return  true if it has
	/// 
	bool is_analysed() const;

	/// Find out whether the sample file has been mined.
	///
	/// @return  true if it has
	/// 
	bool is_mined() const;

	/// Get the names of all random variables from the sample file.
	/// Only call this method after a successful call to  analyse() .
	///
	const std::vector<std::wstring>& get_rand_var_names() const;
	
	/// Get the number of samples (per random variable) in the sample file. 
	/// Only call this method after a successful call to  analyse() .
	///
	/// @return  The number of samples.
	///
	Ssize get_sample_size() const;

	/// Get the sample for a specific random variable in the sample file.
	///
	/// @param[in] rand_var_name  The name of the random variable whose sample is requested; an exception is thrown if the 
	///				name is invalid (i.e. not found in the sample file)
	/// @return  The samples; the list is ordered the same as the set of names passed in
	///
	const UnivarDistrSample<float>& get_sample_for_rand_var(const std::wstring& rand_var_name) const;

	/// Get the samples for a subset of the random variables in the sample file.
	///
	/// @param[in] rand_var_names  The names of the random variables whose samples are requested; an exception is thrown if the 
	///				set contains invalid names (i.e. not found in the sample file)
	/// @return  The samples; the list is ordered the same as the set of names passed in
	///
	std::vector<const UnivarDistrSample<float>*> get_samples_for_rand_vars(
			const std::set<std::wstring>& rand_var_names) const;

	/// Acquire the full joint distribution sample (for all random variables in the file) as mined from the file.
	/// This object releases the sample and will henceforth be in a pre-analysis state.
	///
	/// @return  The joint distribution sample 
	///
	std::unique_ptr<MultivarSample> release_multivar_sample();

protected:
	/// Constructor.
	///
	/// @param[in] is_valid_rand_var_name  Functor which checks whether a random variable name is valid (doesn't contain 
	///				forbidden characters)
	/// @param[in] prog_prov  Progress provider for the mining.
	///
	SampleFileMiner(IsValidRandVarName is_valid_rand_var_name, ProgressProv& prog_prov);

	/// Set general sample information.
	///
	/// @param[in] rand_var_names  The random variable names. 
	/// @param[in] sample_size  The number of samples.
	///
	void set_general_sample_info(std::unique_ptr<std::vector<std::wstring>> rand_var_names, Ssize sample_size);

	/// Prepare the container to hold all sample values for all the variables in the sample file being mined.
	/// It only makes sense to call this method after a call to  analyse() .
	///
	/// @return  The container
	///
	MultivarSample& prepare_multivar_sample_container();

	/// Find out whether a random variable name read from the sample file is valid (ie it doesn't contain forbidden characters).
	///
	/// @param[in] name  The random variable name
	/// @param[in] err  If the name is invalid, this is a message explaining why.
	/// @return  true if the name is valid.
	///
	bool is_valid_rand_var_name(const std::wstring& name, std::wstring& err) const;

	/// Get access to the progress provider.
	///
	/// @return  Ref to the provider.
	///
	ProgressProv& get_prog_prov();

private:	
	std::unique_ptr<const std::vector<std::wstring>>  rand_var_names_;
	Ssize  sample_size_;
	std::unique_ptr<MultivarSample>  multivar_sample_;  // Matches the list of random variable names
	IsValidRandVarName  is_valid_rand_var_name_;
	ProgressProv&  prog_prov_;
	
	TWIST_CHECK_INVARIANT_DECL
};

} 

#endif 


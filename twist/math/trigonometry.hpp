/// @file trigonometry.hpp
/// Miscellaneous trigonometric utilities.   

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_MATH_TRIGONOMETRY_HPP
#define TWIST_MATH_TRIGONOMETRY_HPP

#include "twist/meta_misc_traits.hpp"
#include "twist/math/defs.hpp"

#include <concepts>

namespace twist::math {

/*! Bring the an angle value, in radians, to the interval [0, 2*pi).
    \param[in] radians  The input angle value (radians)
    \return  The normalised angle value
 */
template<std::floating_point T>
[[nodiscard]] auto normalise_to_2pi(T radians) -> T;

/*! Bring the an angle value, in degrees, to the interval [0, 360).
    \param[in] deg  The input angle value (degrees)
    \return  The normalised angle value
 */
template<class T,
         class = EnableIfNumber<T>>
[[nodiscard]] auto normalise_to_360(T deg) -> T;

//! Convert the angle value \rad from radians to degrees.
template<std::floating_point T>
[[nodiscard]] auto radians_to_degrees(T rad) -> T;

//! Convert the angle value \p rad from radians to degrees, normalised to the interval [0, 360).
template<std::floating_point T>
[[nodiscard]] auto radians_to_degrees_to_360(T rad) -> T;

//! Convert the angle value \p deg from degrees to radians.
template<std::floating_point T>
[[nodiscard]] auto degrees_to_radians(T deg) -> T;

//! Convert the angle value \p deg from degrees to radians, normalised to the interval [0, 2*pi).
template<std::floating_point T>
[[nodiscard]] auto degrees_to_radians_to_2pi(T deg) -> T;

} 

#include "twist/math/trigonometry.ipp"

#endif 

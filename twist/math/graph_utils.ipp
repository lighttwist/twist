///  @file  graph_utils.hpp
///  Inline implementation file for "graph_utils.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

namespace twist::math {

template<typename Vtx>
void topological_sort(const DirectedGraphData<Vtx>& graph_data, std::vector<Vtx>& sorted_vertices)
{
	sorted_vertices.clear();

	// Make a local copy of the graph data
	DirectedGraphData<Vtx> data{ graph_data };

	// Go through the (remaining) graph data and find the first vertex that has no parents.
	// Remove that vertex from the data (and append it to the sorted list) and remove it from all sets of parents where 
	// it appears.
	auto removeFirstOrphanVertexFn = [&]() {
		auto findIt = data.end();
		for (auto it = data.begin(); it != data.end(); ++it) {
			if (it->second.size() == 0) {
				findIt = it;
				break;
			}
		}
		if (findIt == data.end()) {
			TWIST_THROW(L"The data does not represent a directed graph.");
		}
		// Firstly, go through all remaining vertices and remove the vertex from wherever it appears in the set of parents.
		for (auto it = data.begin(); it != data.end(); ++it) {
			if (it != findIt) {
				it->second.erase(findIt->first);
			}
		}
		// Secondly, move the vertex from the graph data to the sorted list of vertices.
		sorted_vertices.push_back(findIt->first);
		data.erase(findIt);
	};

	// Repeat the "remove first orphan vertex" procedure until all vertices are moved to the sorted list.
	while (data.size() > 0) {
		removeFirstOrphanVertexFn();
	}
}

}




/// @file PolarVector.hpp
/// PolarVector class defintion

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_MATH_POLAR_VECTOR_HPP
#define TWIST_MATH_POLAR_VECTOR_HPP

#include "twist/math/trigonometry.hpp"

#include <cmath>

namespace twist::math {

/*! A mathematical vector, with positive magnitude and in two dimensions, whose properties are stored as the 
    orthogonal projections of the vector on the X and Y axes (Cartesian, as opposed to polar coordinates).
 */
template<typename Coord>
class PolarVector {
public:
	enum class FromPolar {}; ///< Tag type for constructor tag dispatching
	enum class FromCartesian {}; ///< Tag type for constructor tag dispatching

	static constexpr FromPolar from_polar{}; ///< Tag value for constructor tag dispatching
	static constexpr FromCartesian from_cartesian{}; ///< Tag value for constructor tag dispatching

	//! Constructor. Initialises to a vector with magnitude zero.
	PolarVector() = default;
	
	/*! Create a vector from polar coordinates.
	    \param[in] tag  Tag value used for tag dispatching 
	    \param[in] magnitude  The vector magnitude (positive)
	    \param[in] direction  The vector direction (in radians)
	 */
	PolarVector(FromPolar /*tag*/, Coord magnitude, Coord direction);

	/*! Create a vector from Cartesian coordinates.
	    \param[in] tag  Tag value used for tag dispatching 
	    \param[in] x_component  The X-component of the vector, ie the orthogonal projection of the vector on 
                                the X-axis
	    \param[in] y_component  The Y-component of the vector, ie the orthogonal projection of the vector on 
                                the Y-axis
	 */
	PolarVector(FromCartesian /*tag*/, Coord x_component, Coord y_component);
	
	//! The vector magnitude, a positive value.
	Coord magnitude() const;
	
	//! The vector direction, a value in radians in the interval [0 2pi).
	Coord direction() const;	
	
	//!  The X-component of the vector, ie the orthogonal projection of the vector on the X-axis.
	Coord x_component() const;
	
	//!  The Y-component of the vector, ie the orthogonal projection of the vector on the Y-axis.
	Coord y_component() const;
	
	/*! Perform vector addition between this vector and another.
	    \param[in] other  The other vector
	    \return  The resultant vector
	 */
	PolarVector add(PolarVector other);

private:	
	Coord magnitude_;
	Coord direction_;

	TWIST_CHECK_INVARIANT_DECL
};

}

#include "twist/math/PolarVector.ipp"

#endif

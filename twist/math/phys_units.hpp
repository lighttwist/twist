/// @file phys_units.hpp
/// Conversions between physical and mathematical quantitites and units

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_MATH_PHYS__UNITS_HPP
#define TWIST_MATH_PHYS__UNITS_HPP

#include "twist/metaprogramming.hpp"

namespace twist::math {

//! Convert the temperature value \p k from kelvins to degrees Celsius.
template<std::floating_point T>
[[nodiscard]] auto kelvin_to_celsius(T k) -> T;

//! Convert the temperature value \p c from kelvins to degrees Celsius.
template<std::floating_point T>
[[nodiscard]] auto celsius_to_kelvin(T c) -> T;

//! Convert the speed value \p kmph from km/h to m/s.
template<std::floating_point T>
[[nodiscard]] auto kmph_to_mps(T kmph) -> T;

//! Convert the speed value \p mps from m/s to km/h.
template<std::floating_point T>
[[nodiscard]] auto mps_to_kmph(T mps) -> T;

//! Convert the pressure value \p p from pascals (Pa) to hectopascals (hPa).
template<std::floating_point T>
[[nodiscard]] auto pascal_to_hectopascal(T p) -> T;

//! Convert the area value \p squareMetre (m2) to a hectare (ha) value.
template<std::floating_point T>
[[nodiscard]] auto square_metre_to_hectare(T square_metre) -> T;

//! Convert the area value \p hectare (ha) to a square metre (m2) value.
template<std::floating_point T>
[[nodiscard]] auto hectare_to_square_metre(T hectare) -> T;

/*! Convert the tonne, aka metric ton, per hectare (t/ha) load value \p tph to a kilogram per square metre (kg/m2) 
    value.
 */
template<std::floating_point T>
[[nodiscard]] auto tonne_per_hectare_to_kg_per_m2(T tph) -> T;

/*! Converts specific humidity to relative humidity.
    \param[in] psurf  Surface pressure (hPa)
    \param[in] spec_hum  Specific humidity (kg/kg)
    \param[in] tk  Air temperature (K)
    \return  The relative humidity (%)
 */
[[nodiscard]] auto calc_rel_humidity(double spec_hum, double psurf, double tk) -> double;

/*! Transfor the wind direction value \p deg given in degrees, following the "wind rose" convention (0/360 are North, 
    90 is East, etc) to a radian value following the "unit circle" convention (0 is East, pi/2 is North, etc).
    Then reverse the direction: while the input "wind rose" value signifies the direction which the wind is blowing 
    *from*, the output value is the direction the wind is blowing *towards*.
    The returned value is in the interval [0, 2*pi].
 */
[[nodiscard]] auto wind_rose_degrees_to_reversed_unit_circle_radians(double deg) -> double;

/* Transform a wind vector specified in polar coordinates, with the wind direction value \p deg given in degrees, 
   following the "wind rose" convention (0/360 are North, 90 is East, etc), to the components (projections) of the 
   vector on the X and Y axes.
   \param[in] speed  The wind speed
   \param[in] deg  The wind direction (degrees)
   \return 0. The X (eastward) component of the wind vector (same units as \p speed)
           1. The Y (northward) component of the wind vector (same units as \p speed)
 */
[[nodiscard]] auto wind_rose_vector_with_degrees_to_cartesian(double speed, double deg) -> std::tuple<double, double>;

} 

#include "twist/math/phys_units.ipp"

#endif 

/// @file space_grid.hpp
/// SpaceGrid class definition
/// SquareSpaceGrid class definititon

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_MATH_SPACE_GRID_HPP
#define TWIST_MATH_SPACE_GRID_HPP

#include "twist/concepts.hpp"
#include "twist/grid_utils.hpp"
#include "twist/math/geometry.hpp"

namespace twist::math {

// --- SpaceGrid abstract class template ---

/*! A spatial grid, ie a grid of rectangular (including sqare) cells of non-zero size. The cell indexing is the same as 
    it would be in a matrix. The grid is flat, and unit-neutral, ie its unit could be anything expressible as numbers. 
    The grid uses the mathematical coordinate system: smaller is left and down.
    \tparam Coord  The coordinate type (numeric)
 */
template<class Coord>
class SpaceGrid {
	static_assert(is_number<Coord>, "Coord must be a numeric type.");
public:
	/*! Constructor. 
	    \param[in] left  The left coordinate of the grid perimeter
	    \param[in] bottom  The bottom coordinate of the grid perimeter
	    \param[in] cell_width  The cell width
	    \param[in] cell_height  The cell height
	    \param[in] nof_rows  The number of rows
	    \param[in] nof_cols  The number of columns
	 */
	SpaceGrid(Coord left, Coord bottom, Coord cell_width, Coord cell_height, Ssize nof_rows, Ssize nof_cols);

	//! The number of rows in the grid.
    [[nodiscard]] auto nof_rows() const -> Ssize;
	
	//! The number of columns in the grid.
	[[nodiscard]] auto nof_columns() const -> Ssize;

	//! The size of a cell's horizontal side in the spatial units used by the grid coordinates.
	[[nodiscard]] auto cell_width() const -> Coord;

	//! The size of a cell's vertical side in the spatial units used by the grid coordinates.
	[[nodiscard]] auto cell_height() const -> Coord;

	//! The X coordinate of the left side of the grid perimeter.
	[[nodiscard]] auto left() const -> Coord;
	
	//! The Y coordinate of the bottom side of the grid perimeter.
	[[nodiscard]] auto bottom() const -> Coord;
	
	//! The X coordintate of the right side of the grid perimeter.
	[[nodiscard]] auto right() const -> Coord;
	
	//! The Y coordinate of the top side of the grid perimeter.
	[[nodiscard]] auto top() const -> Coord;

private:
	Coord left_;
	Coord bottom_;
	Coord cell_width_;
	Coord cell_height_;
	Ssize nof_rows_;
	Ssize nof_cols_;

	TWIST_CHECK_INVARIANT_DECL
};

// --- SquareSpaceGrid class template ---

/*! A spatial grid, ie a matrix of square cells of non-zero size. The cell indexing is the same as it would be in a 
    matrix. The grid is flat, and unit-neutral, ie its unit could be anything expressible as numbers. The grid uses the 
    mathematical coordinate system: smaller is left and down.
    \tparam Coord  The coordinate type (numeric)
 */
template<class Coord>
class SquareSpaceGrid : public SpaceGrid<Coord> {
public:
	/*! Constructor. 
	    \param[in] left  The left coordinate of the grid perimeter
	    \param[in] bottom  The bottom coordinate of the grid perimeter
	    \param[in] cell_size  The cell size
	    \param[in] nof_rows  The number of rows
	    \param[in] nof_cols  The number of columns
	 */
	SquareSpaceGrid(Coord left, Coord bottom, Coord cell_size, Ssize nof_rows, Ssize nof_cols);

	//! The size of a cell's side (cells are square) in the spatial units used by the grid coordinates.
	[[nodiscard]] auto cell_size() const -> Coord;

    // Note that this class should have no data members, as the base does not have a virtual destructor.
};

// -- Non-member functions ---

//! Default tolerance for snapping to a grid's row/column boundaries (eg when calculating a subgrid)
const auto def_grid_snap_tol = 1e-8;

//! Given a vertical edge between two cells, these strategies indicate which of the two cells to choose
enum class RowFromEdgeChoice {
	top = 1,
	bottom = 2
};

//! Given a horizontal edge between two cells, these strategies indicate which of the two cells to choose
enum class ColFromEdgeChoice {
	left = 1,
	right = 2
};

/*! Get the dimensions (the number of columns and the number of rows) of a space grid.
    \tparam Coord  The grid coordinate type
    \param[in] grid  The grid
    \return  0. The number of rows
			 1. The number of columns
 */
template<class Coord>
[[nodiscard]] auto dims(const SquareSpaceGrid<Coord>& grid) -> std::tuple<Ssize, Ssize>;

//! The number of cells in the grid \p grid.
template<class Coord>
[[nodiscard]] auto nof_cells(const SpaceGrid<Coord>& grid) -> Ssize;

//! Get the perimeter rectangle of the space grid \p a grid with coordinate type \p Coord.
template<class Coord>
[[nodiscard]] auto perimeter_rect(const SpaceGrid<Coord>& grid) -> Rectangle<Coord>;

//! The cell index with the greatest value in the grid (it identifies the bottom-right cell).
template<class Coord>
[[nodiscard]] auto max_cell_index(const SpaceGrid<Coord>& grid) -> GridCellIndex;

//! The area of a cell (cells are square) in the spatial units used by the grid coordinates.
template<class Coord>
[[nodiscard]] auto cell_area(const SquareSpaceGrid<Coord>& grid) -> Coord;

/*! Calculate the "grid cell index" of a space grid cell from the cell row and column indexes.
    \tparam Coord  The grid coordinate type
    \param[in] row  The cell row index; an exception is thrown if it is too large
    \param[in] col  The cell column index; an exception is thrown if it is too large
    \param[in] row_count  The number of rows in the grid
    \param[in] col_count  The number of columns in the grid
    \return  The "grid cell index"
 */
template<class Coord>
[[nodiscard]] auto get_cell_index_from_row_col(const SquareSpaceGrid<Coord>& grid, Ssize row, Ssize col) 
                    -> GridCellIndex;

/*! Calculate the cell row and column indexes of a space grid cell from the "grid cell index".
    \tparam Coord  The grid coordinate type
    \param[in] row_count  The number of rows in the grid
    \param[in] col_count  The number of columns in the grid
    \param[in] cell_index  The "grid cell index"
    \return  0. The cell row index
             1. The cell column index
 */
template<class Coord>
[[nodiscard]] auto get_cell_row_col_from_index(const SquareSpaceGrid<Coord>& grid, GridCellIndex cell_index) 
                    -> std::tuple<Ssize, Ssize>;

/*! Given a space grid and a position in space, find the grid cell which contains that position.
    \tparam Coord  The grid coordinate type
    \param[in] grid  The grid
    \param[in] pos  The position; a PosOutsideGrid exception is thrown if it falls outside the grid
    \param[in] row_choice  The "row from edge" choice
    \param[in] col_choice  The "column from edge" choice
    \return  0. the row index of the cell
             1. the column index of the cell
 */
template<class Coord>
[[nodiscard]] auto get_cell_row_col_from_pos(const SpaceGrid<Coord>& grid,
                                             Point<Coord> pos,
                                             RowFromEdgeChoice row_choice = RowFromEdgeChoice::bottom,
                                             ColFromEdgeChoice col_choice = ColFromEdgeChoice::left)
                    -> std::tuple<Ssize, Ssize>;

/*! Given a space grid and a position in space, find the grid cell which contains that position.
    \tparam Coord  The grid coordinate type
    \param[in] grid  The grid
    \param[in] pos  The position; a PosOutsideGrid exception is thrown if it falls outside the grid
    \param[in] row_choice  The "row from edge" choice
    \param[in] col_choice  The "column from edge" choice
    \return  The "grid cell index" fo the cell
 */
template<class Coord>
[[nodiscard]] auto get_cell_index_from_pos(const SquareSpaceGrid<Coord>& grid, Point<Coord> pos,
                                           RowFromEdgeChoice row_choice = RowFromEdgeChoice::bottom,
                                           ColFromEdgeChoice col_choice = ColFromEdgeChoice::left) -> GridCellIndex;

/*! Given a space grid, get the perimeter rectangle (a square) for a specific cell.
    \tparam Coord  The grid coordinate type
    \param[in] grid  The grid
    \param[in] row  The cell row index
    \param[in] col  The cell column index
    \return  The cell rectangle
 */
template<class Coord>
[[nodiscard]] auto get_cell_rect_from_row_col(const SquareSpaceGrid<Coord>& grid, Ssize row, Ssize col) 
                    -> Rectangle<Coord>;

/*! Given a space grid, get the cenre point of the perimeter square for a specific cell.
    \tparam Coord  The grid coordinate type
    \param[in] grid  The grid
    \param[in] row  The cell row index
    \param[in] col  The cell column index
    \return  The cell centre point
 */
template<std::floating_point Coord>
[[nodiscard]] auto get_cell_centre_from_row_col(const SquareSpaceGrid<Coord>& grid, Ssize row, Ssize col) 
                    -> Point<Coord>;

/*! Given a space grid, get the centre point of the perimeter square for a specific cell.
    \tparam OutCoord  The coordinat type for the output point
    \tparam Coord  The grid coordinate type
    \param[in] grid  The grid
    \param[in] row  The cell row index
    \param[in] col  The cell column index
    \return  The cell centre point
 */
template<std::floating_point Coord>
[[nodiscard]] auto get_cell_centre_from_index(const SquareSpaceGrid<Coord>& grid, GridCellIndex cell_index) 
                    -> Point<Coord>;

/*! Given a space grid, get the cenre point of the perimeter square for a specific cell.
    \tparam OutCoord  The coordinat type for the output point
    \tparam Coord  The grid coordinate type
    \param[in] grid  The grid
    \param[in] row  The cell row index
    \param[in] col  The cell column index
    \return  The cell centre point
 */
template<std::floating_point OutCoord, std::integral Coord>
[[nodiscard]] auto get_cell_centre_from_row_col(const SquareSpaceGrid<Coord>& grid, Ssize row, Ssize col) 
                    -> Point<OutCoord>;

/*! Given a space grid, get the centre point of the perimeter square for a specific cell.
    \tparam OutCoord  The coordinat type for the output point
    \tparam Coord  The grid coordinate type
    \param[in] grid  The grid
    \param[in] row  The cell row index
    \param[in] col  The cell column index
    \return  The cell centre point
 */
template<std::floating_point OutCoord, std::integral Coord>
[[nodiscard]] auto get_cell_centre_from_index(const SquareSpaceGrid<Coord>& grid, GridCellIndex cell_index) 
                    -> Point<OutCoord>;

/*! Get a subgrid of a given grid (referred to here as the supergrid).
    \tparam Coord  The grid coordinate type
    \param[in] grid  The supergrid
    \param[in] subgrid_info  The subgrid info
    \return  The subgrid
 */
template<class Coord>
[[nodiscard]] auto get_subgrid(const SpaceGrid<Coord>& grid, SubgridInfo subgrid_info) -> SpaceGrid<Coord>;

/*! Get a subgrid of a given square grid (referred to here as the supergrid).
    \tparam Coord  The grid coordinate type
    \param[in] grid  The supergrid
    \param[in] subgrid_info  The subgrid info
    \return  The subgrid
 */
template<class Coord>
[[nodiscard]] auto get_subgrid(const SquareSpaceGrid<Coord>& grid, SubgridInfo subgrid_info) -> SquareSpaceGrid<Coord>;

/*!  Get the perimeter rectangle of a specific subgrid of a given grid (referred to here as the supergrid).
    \tparam Coord  The grid coordinate type
    \param[in] grid  The supergrid
    \param[in] subgrid_info  The subgrid info
    \return  The subgrid rectangle
 */
template<class Coord>
[[nodiscard]] auto get_subgrid_rect(const SquareSpaceGrid<Coord>& grid, SubgridInfo subgrid_info) -> Rectangle<Coord>;

/*! Find out which is the smallest subgrid of a space grid containing a specific rectangle (or the intersection of that 
    rectangle with the grid rectangle). A tolerance can be used for the coordinate comparisons (if a side of the 
    rectangle is near within that tolerance to a row/column boundary, the subgrid will be snapped to that boundary).
    \tparam Coord  The grid coordinate type
    \param[in] grid  The space grid (the supergrid)
    \param[in] rect  The rectangle
    \param[in] snap_tol  The "snap" tolerance; pass in zero for no tolerance
    \return  The subgrid info, if successful; or nullopt if the subgrid does not exist
 */
template<class Coord>
[[nodiscard]] auto get_info_of_subgrid_containing_rect(const SpaceGrid<Coord>& grid, 
		                                               const Rectangle<Coord>& rect,
                                                       double snap_tol = def_grid_snap_tol) 
                    -> std::optional<SubgridInfo>;

/*! Find out which is the biggest subgrid of a space grid (with square cells) which is contained inside a specific 
    rectangle (or the intersection of that rectangle with the grid rectangle).
    \tparam Coord  The grid coordinate type
    \param[in] grid  The space grid (the supergrid)
    \param[in] rect  The rectangle
    \return  The subgrid info, if successful; or nullopt if the subgrid does not exist
 */
template<class Coord>
[[nodiscard]] auto get_info_of_subgrid_contained_in_rect(const SquareSpaceGrid<Coord>& grid, 
                                                         const Rectangle<Coord>& rect) -> std::optional<SubgridInfo>;

/*! Given the space grid (with square cells) \p grid and the rectangle \p rect, get the biggest subgrid (if any) such 
    that the centroids of all its cell rectangles fall within the rectangle.
    \tparam Coord  The grid coordinate type
    \return  The subgrid info; or nullopt if the subgrid does not exist
 */template<class Coord>
[[nodiscard]] auto get_info_of_subgrid_with_cell_centres_in_rect(const SquareSpaceGrid<Coord>& grid,
                                                                 const Rectangle<Coord>& rect) 
                    -> std::optional<SubgridInfo>;

/*! Compare two space grids.
    \tparam Coord  The grid coordinate type
    \param[in] grid1  The first grid
    \param[in] grid2  The second grid
    \return  true if the grids are identical
 */
template<class Coord>
[[nodiscard]] auto compare(const SpaceGrid<Coord>& grid1, const SpaceGrid<Coord>& grid2) -> bool;

/*! Compare two space grids, using a tolerance for comparing their coordinates.
    \tparam Coord  The grid coordinate type
    \param[in] grid1  The first grid
    \param[in] grid2  The second grid
    \param[in] tol  The tolerance: any two coordinate values which are equal within this tolerance
   					will be considered equal  
    \return  true if the grids are the same within the toleance
 */
template<class Coord>
[[nodiscard]] auto compare_tol(const SpaceGrid<Coord>& grid1, const SpaceGrid<Coord>& grid2, double tol) -> bool;

/*! Find out whether a space grid is a subgrid of another (that is, if it is aligned with, and is contained 
    inside/coincides with, another), using a tolerance for the coordinate comparisons.
    \tparam Coord  The grid coordinate type
    \param[in] grid1  The first grid (we are checking to see if it is a subgrid of the other)
    \param[in] grid2  The second grid (we are checking to see if it is a supergrid of the other)
    \param[in] tol  The tolerance
    \return  If the first grid is a subgrid of the second, information about the subgrid; otherwise nullopt
 */
template<class Coord>
[[nodiscard]] auto is_subgrid_tol(const SquareSpaceGrid<Coord>& grid1, const SquareSpaceGrid<Coord>& grid2, double tol) 
                    -> std::optional<SubgridInfo>;

/*! Convert the SpaceGrid object \p grid to a SquareSpaceGrid, if the grid has square cells (ie the width and height of
    its cells are exactly equal). Otherwise throw an exception.
    \tparam Coord  The grid coordinate type
 */
template<class Coord>
[[nodiscard]] auto to_square_grid(const SpaceGrid<Coord>& grid) -> SquareSpaceGrid<Coord>;

/*! Apply the callable type \p func one for each cell in grid \p grid, with the cell index as the only parameter.
    \tparam Coord  The grid coordinate type
    \tparam Fn  The callable type
 */
template<class Coord, 
         InvocableR<void, GridCellIndex> Fn>
auto apply_to_all_cells(const SpaceGrid<Coord>& grid, Fn func) -> void;

/*! Find out whether the horizontal and vertical cell edges of grid \p grid1 and \p grid2 coincide perfectly, or would
    coincide perfectly if the grids overlapped (the test can be positive even if they do overlap at all).
    \note  Naturally a necessary condition is that the cells have the same dimensions
    \tparam Coord  The grid coordinate type
 */
template<class Coord>
[[nodiscard]] auto are_aligned(const SpaceGrid<Coord>& grid1, const SpaceGrid<Coord>& grid2) -> bool;

/*! Information about the cells in an under-grid fall under a cell in an over-grid, where the size of an over-cell is 
    a multiple of the size of an under-cell and the cell edges are not necessarily aligned.
    The information if relative to the "central under-cell", that is the under-cell which contains the centre of the 
    over-cell (if the centre falls on an under-cell edge, one of the under-cells with that edge is chosen arbitrarily, 
    see also get_cell_row_col_from_pos()).
    An under-cell is considered to fall under an over-cell if its centre is inside the over-cell.
    \see  get_cells_under_cell_info()
 */
struct UndercellsInfo { 
    //! Number of under-cells left of the central under-cell which fall under the over-cell
    Ssize cells_left_from_central; 
    //! Number of under-cells right of the central under-cell which fall under the over-cell
    Ssize cells_right_from_central;
    //! Number of under-cells up from the central under-cell which fall under the over-cell
    Ssize cells_down_from_central;
    //! Number of under-cells down from the central under-cell which fall under the over-cell
    Ssize cells_up_from_central;
    //! The total number of under-cells fall under an over-cell
    Ssize total_cells; 
};

/*! Given two overlaping square space grids, where the cell size of one (the over-grid) is a multiple of the cell size 
    of the other (the under-grid), but whose cell edges are not necessarily aligned, get some information about the 
    under-grid cells (under-cells) which fall under any over-grid cell (over-cell) in the area where the two grids 
    overlap.
    \see  UndercellsInfo.
    \tparam Coord  The grid coordinate type
    \param overgrid  The over-grid
    \param undergrid  The under-grid
    \return  Cell overlap info
 */
template<class Coord>
[[nodiscard]] auto get_undercells_info(const SquareSpaceGrid<Coord>& overgrid,
                                       const SquareSpaceGrid<Coord>& undergrid) -> UndercellsInfo;

/*! Given two overlaping square space grids, where the cell size of one (the over-grid) is a multiple of the cell size 
    of the other (the under-grid), but whose cell edges are not necessarily aligned, invoke a callable for each 
    overgrid cell, whose arguments are the row and column index of the over-cell and of the under-grid cells whose 
    centres fall under the over-cell.
    \tparam Fn  Callable type
    \tparam Coord  The grid coordinate type
    \param[in] overgrid  The over-grid
    \param[in] undergrid  The under-grid
    \param[in] func  The callable
 */
template<class Coord,
         std::invocable<Ssize /*overcell_row*/, 
                        Ssize /*overcell_col*/, 
                        const SubgridInfo& /*under_subgrid_info*/> Fn>
auto foreach_overcell(const SquareSpaceGrid<Coord>& overgrid, 
                      const SquareSpaceGrid<Coord>& undergrid, 
                      Fn func) -> void;

/*! Given two overlaping square space grids, where the cell size of one (the over-grid) is a multiple of the cell size 
    of the other (the under-grid), but whose cell edges are not necessarily aligned, and the data associated with the 
    under-grid, invoke a callable for each overgrid cell, whose arguments are the row and column index of the over-cell 
    and of the under-grid cells whose centres fall under the over-cell, together with the values in those undercells 
    and their frequencies in the over-cell.
    \tparam Coord  The grid coordinate type
    \tparam UndercellVal  The data type of an under-cell value
    \tparam Fn  Callable type
    \param[in] overgrid  The over-grid
    \param[in] undergrid  The under-grid
    \param[in] undergrid_data  The data matrix associated with the under-grid; the matrix dimensions must match the 
                               under-grid dimensions 
    \param[in] func  The callable 
 */
template<class Coord,
         class UndercellVal,
         std::invocable<Ssize /*overcell_row*/, 
                        Ssize /*overcell_col*/,
                        const SubgridInfo& /*under_subgrid_info*/,
                        const std::map<UndercellVal, Ssize>& /*undercell_value_frequencies*/> Fn>
auto foreach_overcell(const SquareSpaceGrid<Coord>& overgrid, 
                      const SquareSpaceGrid<Coord>& undergrid, 
                      const twist::math::FlatMatrix<UndercellVal>& undergrid_data,
                      Fn func) -> void;

}

#include "twist/math/space_grid.ipp"

#endif 

/// @file Dag.hpp
/// Dag class and related classes and free functions

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_MATH_DAG_HPP
#define TWIST_MATH_DAG_HPP

#include "twist/meta_misc_traits.hpp"

namespace twist::math {

template<class Data>
class Dag;

template<class Data>
class DagNode;

template<class Data>
using DagNodePtr = gsl::not_null<DagNode<Data>*>;

template<class Data>
using DagNodePtrs = std::vector<DagNodePtr<Data>>;

//! Concept requiring T to be a range whose element type is DagNodePtr<>
template<typename T>
concept DagNodeRange = IsSpecializationOf<DagNode, NotNullPointee<rg::range_value_t<T>>>;

template<DagNodeRange Rng>
using DagNodePtrFromRange = rg::range_value_t<Rng>;

template<DagNodeRange Rng>
using DagNodePtrsFromRange = std::vector<DagNodePtrFromRange<Rng>>;

template<DagNodeRange Rng>
using DagDataFromRange = typename NotNullPointee<DagNodePtrFromRange<Rng>>::Data;

/*! A node in a directed acyclic graph (DAG).
    \tparam Data  Data type associated with a graph node
*/
template<class Data>
class DagNode {
    static_assert(std::is_move_constructible_v<Data>);

public:
    using NodeIter = std::vector<gsl::not_null<DagNode*>>::const_iterator; 
    typedef Data Data;

	DagNode(const DagNode&) = delete;  

	auto operator=(const DagNode&) -> DagNode& = delete;  

    //! Data associated with the node.
    [[nodiscard]] auto data() -> const Data&;

    //! The number of nodes which are children of this node.
    [[nodiscard]] auto nof_children() const -> Ssize;

    //! Access to the nodes which are children of this node.
    [[nodiscard]] auto children() const -> Range<NodeIter>;

    //! The number of nodes which are parents of this node.
    [[nodiscard]] auto nof_parents() const -> Ssize;

    //! Access to the nodes which are parents of this node.
    [[nodiscard]] auto parents() const -> Range<NodeIter>;

private:
    explicit DagNode(Data data);

	DagNode(DagNode&&) = default;  

	auto operator=(DagNode&&) -> DagNode& = default;

    friend class Dag<Data>;

    Data data_;
    DagNodePtrs<Data> parents_;
    DagNodePtrs<Data> children_;

    TWIST_CHECK_INVARIANT_DECL
};

/*! A directed acyclic graph (DAG).
    An edge in the directed graph is called here an arc, and the node where the arc starts (the feather end, if 
    thinking of the arc as an arrow) is the parent node, while the other node is the child node.
    The class itself does not enforce that the graph is connected.
    The class itself does not enforce acyclicity: it is up to the class user to ensure that the graph contains no 
    "strongly connected components"/cycles as the DAG is being built.
    \tpararm Data  Data type associated with a graph node
 */
template<class Data>
class Dag {
    static_assert(std::is_move_constructible_v<Data>);

public:
    using Node = DagNode<Data>;
    using NodePtr = DagNodePtr<Data>;
    using NodeIter = std::vector<std::unique_ptr<Node>>::const_iterator;
    using PrintData = std::function<std::wstring(const Data&)>;

    /* Constructor.
       \param[in] print_data  Callable which returns a string representation of a node's data
     */
    explicit Dag(PrintData print_data);

    //! The number of nodes in the graph.
    [[nodiscard]] auto nof_nodes() const -> Ssize;

    /*! Access to all nodes in the graph.
        \return  An object satisfying the DagNodeRange concept
     */
    [[nodiscard]] auto nodes() const;

    /*! Add a node to the graph. It will not be connected to any arcs.
        \param[in] data  The node data
        \return  Pointer to the newly added node; it will be valid as long as the graph exists and the node remains in 
                 the graph
     */
    [[nodiscard]] auto add_node(Data data) -> NodePtr;

    /*! Add an arc to the graph.
        If the arc already exists, an exception is thrown.
        \param[in] parent  The parent node
        \param[in] child  The child node
     */
    auto add_arc(NodePtr parent, NodePtr child) -> void;

    //! Get a string representation of a node's data.
    [[nodiscard]] auto print_data(NodePtr node) const -> std::wstring;

    TWIST_NO_COPY_DEF_MOVE(Dag)

private:
    std::vector<std::unique_ptr<Node>> nodes_;
    PrintData print_data_; 

    TWIST_CHECK_INVARIANT_DECL
};

// --- Free functions ---

//! Whether directed graph node \p child is a child of node \p parent.
template<class Data>
[[nodiscard]] auto is_child_of(DagNodePtr<Data> parent, DagNodePtr<Data> child) -> bool;

/*! Given a directed graph/a directed ayclic graph (DAG) under construction, find all connected subgraphs.
    \tpararm Rng  Type of range of graph nodes
    \param[in] nodes  The graph nodes; all nodes connected to any nodes in this list should also appear in the list
    \return  A list of all connected subgraphs, each subgraph represented as a set of nodes; if the graph is connected,
             a single set containing all its nodes is returned
 */ 
template<DagNodeRange Rng>
[[nodiscard]] auto get_connected_graphs(const Rng& nodes) -> std::vector<std::unordered_set<DagNodePtrFromRange<Rng>>>;

/*! Given a directed graph/a directed ayclic graph (DAG) under construction, find all "strongly connected components" 
    (SCCs) in the graph. An SCC is a subgraph whose every node is reachable from every other node. 
    In a directed graph, an SCC will be made up of one or several connected cycles. If the directed graph contains no 
    SCCs, then it is a true DAG and a list of all its nodes, in topological ordering, is returned. In this list, each 
    node's parents come before it in the list. Such an ordering is often non-unique.
    \tpararm Rng  Type of range of graph nodes
    \param[in] nodes  The graph nodes; all nodes connected to any nodes in this list should also appear in the list
    \return  0. A list of all SCCs in the graph, each SCC represented as list of nodes
             1. If there are no SCCs in the graph (ie the graph is a true DAG), a list of all its nodes, in topological 
                ordering; otherwise an empty list
 */
template<DagNodeRange Rng>
[[nodiscard]] auto find_sccs_or_topol_order(const Rng& nodes) -> std::tuple<std::vector<DagNodePtrsFromRange<Rng>>, 
                                                                            DagNodePtrsFromRange<Rng>>;

/*! Find out whether the DAG nodes in a list are ordered topologically, ie whether for each node in the list, all its 
    parents come before it in the list. 
    \param[in] nodes  The graph nodes; all nodes connected to any nodes in this list should also appear in the list
    \return  Whether the list is ordered topologically
*/
[[nodiscard]] auto are_topol_ordered(const DagNodeRange auto& nodes) -> bool;

/*! Get the data objects associated with each DAG node in a list of nodes.
    \tpararm Rng  Type of range of graph nodes
    \param[in] nodes  The graph nodes
    \return  List of data objects; matches the order of the node list
*/
template<DagNodeRange Rng>
requires std::is_copy_constructible_v<DagDataFromRange<Rng>>
[[nodiscard]] auto extract_data(const Rng& nodes) -> std::vector<DagDataFromRange<Rng>>;

}

#include "twist/math/Dag.ipp"

#endif

/// @file FlatMatrixStack.hpp
/// FlatMatrixStack class template definition and declarations of the free functions which are part of the class interface

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_MATH_FLAT_MATRIX_STACK_HPP
#define TWIST_MATH_FLAT_MATRIX_STACK_HPP

#include "twist/concepts.hpp"
#include "twist/globals.hpp"
#include "twist/twist_stl.hpp"

#include <iterator>
#include <vector>

namespace twist::math {

/*! A "stack" (collection) of matrices which internally uses a "flat array" (a contiguous block of memory) to store all 
    cell values for all matrices. All matrices in the stack have the same dimensions. A matrix cell is indexed by row 
	index, column index and matrix index, analogous to a three-dimensional array. One way to think about the structure 
	is each matrix being a horizontal rectangle, with the second matrix floating just above of the first, the third 
	just above the second, and so on, in a stack of aligned matrices.
	The cell values for each matrix in the stack are stored contiguously row-wise, the contents of the first matrix 
	first, followed by the second, etc.
    \tparam T  The type of a cell's value; its requiremets match those of the std::vector element type
 */
template<class T>
class FlatMatrixStack {
public:
	using Value = T;

	using value_type = T;
	using size_type = Ssize;

	using reference = typename std::vector<T>::reference;
	using const_reference = typename std::vector<T>::const_reference;

	using iterator = typename std::vector<T>::iterator;
	using const_iterator = typename std::vector<T>::const_iterator;

	using reverse_iterator = typename std::vector<T>::reverse_iterator;
	using const_reverse_iterator = typename std::vector<T>::const_reverse_iterator;

	/*! A range of cells defined by two cell iterators.
        \tparam Iter  The iterator type
	 */
	template<class Iter> 
	class CellRange : public Range<Iter> {
	public:	
		/* Constructor.
		   \param[in] first  Iterator addressing the first cell in the range
		   \param[in] last  Iterator addressing one past the final cell in the range
		 */
		CellRange(Iter first, Iter last); 

		/* Get a direct pointer addressing the first cell in the range, inside the "flat" memory array used internally 
		   by the matrix to store its cells.
		   \return  Pointer to the memory array, allowing read-only access
		 */
		[[nodiscard]] auto data() const -> const T*;

		/* Get a direct pointer addressing the first cell in the range, inside the "flat" memory array used internally 
		   by the matrix to store its cells.
		   \return  Pointer to the memory array, allowing read-write access
		 */
		[[nodiscard]] auto data() -> T* requires (not ConstPointer<std::iterator_traits<Iter>::pointer>);
	};

	//! Constructor. Creates a matrix with zero rows and columns.
	FlatMatrixStack() = default;

	/*! Constructor. Initialises the cell values of each matrix in the stack with T{}.
	    Zero-sized (empty) matrix stacks are allowed, but then all dimensions must be zero.
	    \param[in] nof_rows  The number of rows in each matrix
	    \param[in] nof_columns  The number of columns in each matrix
	    \param[in] nof_matrices  The number of matrices in the stack 
	 */
	FlatMatrixStack(Ssize nof_rows, Ssize nof_columns, Ssize nof_matrices);

	/*! Constructor.  Initialises the cell values of each matrix in the stack with a specific value.
	    Zero-sized (empty) matrix stacks are allowed, but then all dimensions must be zero.
	    \param[in] nof_rows  The number of rows in each matrix
	    \param[in] nof_columns  The number of columns in each matrix
	    \param[in] nof_matrices  The number of matrices in the stack 
	    \param[in] val  The fill value
	 */
	FlatMatrixStack(Ssize nof_rows, Ssize nof_columns, Ssize nof_matrices, const_reference value);

	/*! Constructor. Creates a matrix stack with specific dimensions, and intialises all matrices' cell values by 
	    moving a std::vector instance to the internal storage. Does not invoke any move, copy on individual cell values. 
		The vector size must match the matrix stack dimensions and its elements must match the internal storage 
		structure of the matrix stack.
	    \param[in] nof_rows  The number of rows in each matrix
	    \param[in] nof_columns  The number of columns in each matrix
	    \param[in] nof_matrices  The number of matrices in the stack 
	    \param[in] values  Vector containing the cell values
	 */
	FlatMatrixStack(Ssize nof_rows, Ssize nof_columns, Ssize nof_matrices, std::vector<T> values);

	//! Get a clone of this matrix stack.
	[[nodiscard]] auto clone() const -> FlatMatrixStack<T>;

	/*! An iterator addressing the value in the first (top-left) cell of the first matrix in the stack.
	    The iterator type allows read-write access to the addressed value.
	    If the stack is empty, this iterator is the same as the iterator returned by end().
     */
	[[nodiscard]] iterator begin();

	/*! An iterator addressing one position past the value in the last (bottom-right) cell in the last matrix in 
	    the stack. The iterator type allows read-write access to the addressed value.
     */
	[[nodiscard]] iterator end();

	/*! An iterator addressing one position past the value in the last (bottom-right) cell in the last matrix in 
	    the stack. The iterator type allows read-only access to the addressed value.
	    If the matrix is empty, this iterator is the same as the iterator returned by end().
     */
	[[nodiscard]] const_iterator begin() const;

	/*! An iterator addressing one position past the value in the last (bottom-right) cell in the last matrix in 
	    the stack. The iterator type allows read-only access to the addressed value.
     */
	[[nodiscard]] const_iterator end() const;
	
	/*! An iterator addressing one position past the value in the last (bottom-right) cell in the last matrix in 
	    the stack. The iterator type allows read-only access to the addressed value.
	    If the matrix is empty, this iterator is the same as the iterator returned by end().
     */
	[[nodiscard]] const_iterator cbegin() const;
	
	/*! An iterator addressing one position past the value in the last (bottom-right) cell in the last matrix in 
	    the stack. The iterator type allows read-only access to the addressed value.
     */
	[[nodiscard]] const_iterator cend() const;
	
	/*! A reverse iterator addressing the last (bottom-right) cell in the last matrix in the stack. The iterator type 
	    allows read-write access to the addressed value. If the matrix is empty, this iterator is the same as the 
	    iterator returned by rend().
     */
	[[nodiscard]] reverse_iterator rbegin();
	
	/*! A reverse iterator addressing one position before the value in the first (top-left) cell in the first matrix in
	    the stack. The iterator type allows read-write access to the addressed value. 
     */
	[[nodiscard]] reverse_iterator rend();
	
	/*! A reverse iterator addressing the last (bottom-right) cell in the last matrix in the stack. The iterator type 
	    allows read-only access to the addressed value. If the matrix is empty, this iterator is the same as the 
	    iterator returned by rend().
     */
	[[nodiscard]] const_reverse_iterator rbegin() const;
	
	/*! A reverse iterator addressing one position before the value in the first (top-left) cell in the first matrix in
	    the stack. The iterator type allows read-only access to the addressed value. 
     */
	[[nodiscard]] const_reverse_iterator rend() const;
	
	/*! A reverse iterator addressing the last (bottom-right) cell in the last matrix in the stack. The iterator type 
	    allows read-only access to the addressed value. If the matrix is empty, this iterator is the same as the 
	    iterator returned by rend().
     */
	[[nodiscard]] const_reverse_iterator crbegin() const;
	
	/*! A reverse iterator addressing one position before the value in the first (top-left) cell in the first matrix in
	    the stack. The iterator type allows read-only access to the addressed value. 
     */
	[[nodiscard]] const_reverse_iterator crend() const;

	/*! A direct pointer, allowing read-only access to the "flat" memory array used internally to store the the matrix 
	     stack cell values.
     */
	[[nodiscard]] auto data() const -> const T*;

	/*! A direct pointer, allowing read-write access to the "flat" memory array used internally to store the the matrix 
	     stack cell values.
     */
	[[nodiscard]] auto data() -> T*;

	/*! Get read-write access to a specific cell in a specific matrix in the stack.
	    \param[in] row  The matrix row index; undefined behaviour if it is out of bounds  
	    \param[in] col  The matrix column index; undefined behaviour if it is out of bounds  
	    \param[in] mat  The matrix index within the stack; undefined behaviour if it is out of bounds  
	    \return  Reference to the cell value
	 */
	[[nodiscard]] reference operator()(Ssize row, Ssize col, Ssize mat);

	/*! Get read-only access to a specific cell in a specific matrix in the stack.
	    \param[in] row  The matrix row index; undefined behaviour if it is out of bounds  
	    \param[in] col  The matrix column index; undefined behaviour if it is out of bounds  
	    \param[in] mat  The matrix index within the stack; undefined behaviour if it is out of bounds  
	    \return  Reference to the cell value
	 */
	[[nodiscard]] const_reference operator()(Ssize row, Ssize col, Ssize mat) const;

	/*! Get read-write access to a specific cell in a specific matrix in the stack.
	    \param[in] row  The matrix row index; an exception is thrown if it is out of bounds  
	    \param[in] col  The matrix column index; an exception is thrown if it is out of bounds  
	    \param[in] mat  The matrix index within the stack; an exception is thrown if it is out of bounds  
	    \return  Reference to the cell value
	 */
	[[nodiscard]] reference at(Ssize row, Ssize col, Ssize mat);

	/*! Get read-only access to a specific cell in a specific matrix in the stack.
	    \param[in] row  The matrix row index; an exception is thrown if it is out of bounds  
	    \param[in] col  The matrix column index; an exception is thrown if it is out of bounds  
	    \param[in] mat  The matrix index within the stack; an exception is thrown if it is out of bounds  
	    \return  Reference to the cell value
	 */
	[[nodiscard]] const_reference at(Ssize row, Ssize col, Ssize mat) const;

	/*! Change the dimensions of the matrix stack. The cell values are preserved where possible, while any new cells 
	    are initialised to T{}.
	    \param[in] new_nof_rows  The new number of rows in each matrix
	    \param[in] new_nof_cols  The new number of columns in each matrix
	    \param[in] new_nof_mats  The new number of matrices in the stack
	 */
	auto resize(Ssize new_nof_rows, Ssize new_nof_cols, Ssize new_nof_mats) -> void;

	//! The total number of cells across all matrices in the stack.	
	[[nodiscard]] Ssize size() const;
	
	//! Whether the matrix stack is empty, that is if all its dimensions are zero.
	[[nodiscard]] bool empty() const;

	//! The number of rows in each matrix.
	[[nodiscard]] auto nof_rows() const -> Ssize;

	//! The number of columns in each matrix.
	[[nodiscard]] auto nof_columns() const -> Ssize;

	//! The number of matrices in the stack.
	[[nodiscard]] auto nof_matrices() const -> Ssize;

	//! The number of cells in each matrix.
	[[nodiscard]] auto nof_cells_in_matrix() const -> Ssize;

	//! The total number of cells across all matrices in the stack.	
	[[nodiscard]] auto nof_cells_in_stack() const -> Ssize;

	/*! Get a read-only view of the a specific matrix in the stack.
	    \param[in] mat  The matrix index within the stack; an exception is thrown if it is out of bounds  	    
		\return  Range addressing the cell values in the matrix, in the order used internally
	*/
	[[nodiscard]] auto view_matrix(Ssize mat) const -> CellRange<const_iterator>;

	/*! Get a read-only view of the a specific matrix in the stack.
	    \param[in] mat  The matrix index within the stack; an exception is thrown if it is out of bounds  	    
		\return  Range addressing the cell values in the matrix, in the order used internally
	*/
	[[nodiscard]] auto view_matrix(Ssize mat) -> CellRange<iterator>;

	/*! Get a read-only view of the whole matrix stack.
	    \return  Range addressing all cell values in the matrix stack, in the order used internally
	*/
	[[nodiscard]] auto view_stack() const -> CellRange<const_iterator>;

	/*! Get a read-write view of the whole matrix.
	    \return  Range addressing all cell values in the matrix stack, in the order used internally
	*/
	[[nodiscard]] auto view_stack() -> CellRange<iterator>;

	TWIST_NO_COPY_DEF_MOVE(FlatMatrixStack)  

private:
	auto check_dimensions() const -> void;

	auto check_indexes(Ssize row, Ssize col, Ssize mat) const -> void;

	[[nodiscard]] auto to_linear_index(Ssize row, Ssize col, Ssize mat) const -> Ssize;

	Ssize nof_rows_;
	Ssize nof_cols_;
	Ssize nof_mats_;
	std::vector<T> data_;

	template<class U> 
	friend auto swap(FlatMatrixStack<U>&, FlatMatrixStack<U>&) noexcept -> void;

	TWIST_CHECK_INVARIANT_DECL
};

// --- Free functions ---

/*! Exchange the contents of a matrix with those of other. Does not invoke any move, copy, or swap operations on 
    individual cell values. All iterators and references remain valid (but will be pointing in the other container). 
	The past-the-end iterators are invalidated. The dimensions of each matrix may change.
    \tparam T  The cell value type
    \param[in] a  The first matrix  
    \param[in] b  The second matrix  
 */
template<class T> 
auto swap(FlatMatrixStack<T>& a, FlatMatrixStack<T>& b) noexcept -> void;

//! Equality operator.
template<class T> 
[[nodiscard]] auto operator==(const FlatMatrixStack<T>& lhs, const FlatMatrixStack<T>& rhs) noexcept -> bool;

//! Inequality operator.
template<class T> 
[[nodiscard]] auto operator!=(const FlatMatrixStack<T>& lhs, const FlatMatrixStack<T>& rhs) noexcept -> bool;

/*! Get the dimensions of the matrix stack.
    \tparam T  The cell value type
    \param[in] mat_stack  The matrix stack
    \return  0. The number of rows in each matrix
 			 1. The number of columns in each matrix
	         2. The number of matrices in the stack 
 */
template<class T> 
[[nodiscard]] auto dimensions(const FlatMatrixStack<T>& mat_stack) -> std::tuple<Ssize, Ssize, Ssize>;

/*! Call a user-supplied function with the read-only value in each cell of each matric in a matrix stack.
    \tparam T  The cell value type
    \tparam Fn  Type of the function to call; the second, third and fourth parameters are the i, j and k indexes
    \param[in] mat_stack  The matrix stack  
    \param[in] func  The function
 */
template<class T, class Fn>
requires std::invocable<Fn, const T&, Ssize, Ssize, Ssize>
auto for_each_cell(const FlatMatrixStack<T>& mat_stack, Fn func) -> void;

/*! Call a user-supplied function with the read-write value in each cell of the matrix.
    \tparam T  The cell value type
    \tparam Fn  Type of the function to call; the second, third and fourth parameters are the i, j and k indexes
    \param[in] mat_stack  The matrix stack
    \param[in] func  The function
 */
template<class T, class Fn>
requires std::invocable<Fn, T&, Ssize, Ssize, Ssize>
auto for_each_cell(FlatMatrixStack<T>& mat_stack, Fn func) -> void;

/*! Flip the rows in each matrix of the matrix stack \p mat_stack so that the first row is swapped with the last, the 
    second row with the row before last, etc.
 */
template<class T> 
[[nodiscard]] auto flip_rows(FlatMatrixStack<T>& mat_stack) -> void;

}

#include "twist/math/FlatMatrixStack.ipp"

#endif

/// @file CsvSampleFileMiner.hpp
/// CsvSampleFileMiner class, inherits SampleFileMiner

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_MATH_CSV_FILE_MINER_HPP
#define TWIST_MATH_CSV_FILE_MINER_HPP

#include "SampleFileMiner.hpp"

namespace twist::math {

/// This class reads the data from a text sample file, which follows the CSV file format.
class CsvSampleFileMiner : public SampleFileMiner {
public:
	/// Constructor.
	///
	/// @param[in] path  The sample file path
	/// @param[in] is_valid_rand_var_name  Functor which checks whether a random variable name is valid 
	///					(doesn't contain forbidden characters) 
	/// @param[in] prog_prov  Progress provider for the mining
	///
	CsvSampleFileMiner(const fs::path& path, IsValidRandVarName is_valid_rand_var_name, 
			ProgressProv& prog_prov);

	/// Destructor.
	virtual ~CsvSampleFileMiner();
	
	void analyse() override;  // SampleFileMiner override
	
	void mine() override;  // SampleFileMiner override	

private:	
	const fs::path  path_;
	
	std::unique_ptr<char[]>  line_buffer_;
	std::unique_ptr<char[]>  field_buffer_;

	TWIST_CHECK_INVARIANT_DECL
};

}

#endif 

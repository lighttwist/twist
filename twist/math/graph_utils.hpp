///  @file  graph_utils.hpp
///  Generic utilities for working with graphs

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_MATH_GRAPH__UTILS_HPP
#define TWIST_MATH_GRAPH__UTILS_HPP

namespace twist::math {

/// Type for storing data which describes a directed graph.
/// It is an unsorted list containing one element for each vertex of the graph. 
/// Each element is a pair formed by the vertex, and the set of vertices which are the vertex's parents in 
///   the graph.
///
/// @tparam  Vtx  Type which represents a vertex
/// 
template<typename Vtx>
using DirectedGraphData = std::vector<std::pair<Vtx, std::set<Vtx>>>;

/// Sort a list of vertices in a directed graph topologically.
/// That is, find a linear ordering of the vertices such that, for every directed edge  u -> v ,  u  comes before  v  
/// in the ordering. We call  u  the parent vertex and  v  the child vertex.
///
/// @param  Vtx  [targ] Vertex type.
/// @param  graph_data  [in] The graph data.
/// @param  sorted_vertices  [out] The graph vertices, sorted in topological order.
///
template<typename Vtx>
void topological_sort(const DirectedGraphData<Vtx>& graph_data, std::vector<Vtx>& sorted_vertices);

}

#include "graph_utils.ipp"

#endif 

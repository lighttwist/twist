/// @file numeric_utils.ipp
/// Inline implementation file for "math_utils.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include <cmath>
#include <numeric>

namespace twist::math {

// Structures used by  convert_num()
template<typename Dest, typename Src>
struct NumConverter {
	static Dest convert(Src src) { return src; }
};
template<>
struct NumConverter<float, double> {
	static float convert(double src) { 
		float ret = 0.;
		if (src < -float_max) {
			ret = -float_inf;
		}
		else if (src >= -float_min && src < 0) {
			ret = -float_min; //+ should we not assert?
		}
		else if (src > 0 && src <= float_min) {
			ret = float_min;
		}
		else if (src > float_max) {
			ret = float_inf;
		}
		else {
			ret = static_cast<float>(src);
		}
		return ret;
	} 
};
template<>
struct NumConverter<float, long double> {
	static float convert(long double src) { 
		float ret = 0.;
		if (src < -float_max) {
			ret = -float_inf;
		}
		else if (src >= -float_min && src < 0) {
			ret = -float_min;
		}
		else if (src > 0 && src <= float_min) {
			ret = float_min;
		}
		else if (src > float_max) {
			ret = float_inf;
		}
		else {
			ret = static_cast<float>(src);
		}
		return ret;
	} 
};
template<>
struct NumConverter<double, long double> {
	static double convert(long double src) { 
		return static_cast<double>(src);  // Same in the MSVC compiler 
	} 
};

// --- Global functions ---

template<typename Dest, typename Src>
auto convert_num(Src src) -> Dest
{ 
	return NumConverter<Dest, Src>::convert(src); 
}

template<class T, class>
T trunc_decimals(T x, int decimal_count)
{
	assert(decimal_count > 0);
	const auto factor = pow(10.0, decimal_count);
	return static_cast<T>(trunc(x * factor) / factor);
}


template<class T, class>
T round_decimals(T x, int decimal_count)
{
	assert(decimal_count > 0);
	const auto factor = pow(10.0, decimal_count);
	return static_cast<T>(round(x * factor) / factor);
}

template<class T>
requires is_number<T>
[[nodiscard]] auto divide_exactly(T x, T y, double tolerance) -> std::optional<Ssize>
{
    const auto quot = static_cast<double>(x) / y;
    if (quot > std::numeric_limits<Ssize>::max()) {
        TWIST_THRO2(L"The quotient exceeds Ssize limit.");
    }
    const auto quot_int = round_to_ssize(quot);
    if (!equal_tol(quot, static_cast<double>(quot_int), tolerance)) {
        return std::nullopt;
    }
    return quot_int;
}

template<std::integral T>
[[nodiscard]] auto is_perfect_square(T x) -> bool
{
	const auto sq_root = static_cast<T>(std::round(static_cast<double>(sqrt(x))));
	return sq_root * sq_root == x;
}

template<std::integral T>
auto perfect_square_root(T x) -> T
{
	const auto sq_root = static_cast<T>(std::round(static_cast<double>(sqrt(x))));
	if (sq_root * sq_root != x) {
		TWIST_THRO2(L"{} is not a perfect square.", x);
	}
	return sq_root;
}

template<class T, class I>
bool in_closed_interval(T x, I interv_lo, I interv_hi)  
{
	return interv_lo <= x && x <= interv_hi;
}


template<typename T> 
inline long sign(T x) 
{ 
	if (x < 0) {
		return -1;
	}
	else if (x == 0) {
		return 0;
	}
	else {
		return 1; 
	}
}

template<class T, class>
[[nodiscard]] constexpr auto sqr(T x) -> T
{
	return x * x;
}

[[nodiscard]] auto is_multiple(std::integral auto multiple, std::integral auto divisor) -> bool
{
	if (divisor == 0) {
		TWIST_THROW(L"Warning devision by zero impossible!");
	}
	return multiple % divisor == 0;
}

template<std::floating_point T>
[[nodiscard]] auto is_multiple_tol(T multiple, T divisor, T tol) -> bool
{
    return equal_tol(std::fmod(multiple, divisor), T{0}, tol);
}

template<typename T> 
auto equal_tol(T x, T y, T tol) -> bool
{
	assert(tol >= 0);
	return abs(x - y) <= tol;
}

template<typename T> 
auto greater_tol(T x, T y, T tol) -> bool 
{
	assert(tol >= 0);
	return x - y > tol;
}

template<typename T> 
auto greater_or_equal_tol(T x, T y, T tol) -> bool 
{
	assert(tol >= 0);
	return greater_tol(x, y, tol) || equal_tol(x, y, tol);
}

}

/// @file Distribution.hpp
/// Distribution class

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_MATH_DISTRIBUTION_HPP
#define TWIST_MATH_DISTRIBUTION_HPP

namespace twist::math {
class DistributionVisitor;
}

namespace twist::math {

//! Abstract base class for all distribution classes.
class Distribution {
public:
	//! The distribution type
	enum Type {
		kDiscrete    =  0,	///< Discrete
		kUniform     =  1,	///< Uniform
		kLogUniform  =  2,	///< LogUniform
		kNormal      =  3,	///< Normal
		kLogNormal   =  4,	///< LogNormal
		kExponential =  5,	///< Exponential
		kWeibull     =  6,	///< Weibull
		kTriangular  =  7,	///< Triangular
		kEmpiric     =  8,	///< Empirical (sample-based)
		kGamma       =  9,	///< Gamma
		kBeta        = 10,	///< Beta
		students_t   = 11,	///< Student's t
	};

	using Types = std::vector<Type>;

	virtual ~Distribution() = 0;
	
	//! Get a clone of the distribution object. CAller takes ownership of the returned pointer.
	virtual auto getClone() const -> Distribution* = 0;

	//! The distribution type.
	virtual auto getType() const -> Type = 0;
	
	//! The distribution type name.
	virtual auto getTypeName() const -> std::wstring = 0;

	/*! Get the mean and standard deviation for the distribution.
	    \note This implementation throws an exception.
	    \param[out] mean  The mean
	    \param[out] stddev  The standard deviation
	 */
	virtual auto getMeanStddev(double& mean, double& stddev) const -> void; 
	
	//! Accept a visit from the distribution visitor \p visitor (see the "Visitor" design pattern).
	virtual auto acceptVisit(DistributionVisitor& visitor) const -> void = 0;

	TWIST_NO_COPY_DEF_MOVE(Distribution)

protected:
	Distribution();

private:
	TWIST_CHECK_INVARIANT_DECL
};

} 

#endif 

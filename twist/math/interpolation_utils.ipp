/// @file interpolation_utils.cpp
/// Inline implementation file for "interpolation_utils.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include <cmath>

namespace twist::math {

template<std::floating_point T,
         rg::input_range Rng>
requires std::is_same_v<rg::range_value_t<Rng>, T>
[[nodiscard]] auto calc_inverse_distance_weights(const Rng& distances, double power_param, std::vector<T>& weights) 
                    -> void
{
	using std::ssize;

	assert(ssize(distances) > 0);
	if (ssize(weights) != ssize(distances)) {
		TWIST_THRO2(L"The weight list size ({}) differs from the number of distances ({}).", 
		            ssize(weights), ssize(distances));
	}

	if (auto zero_dist_pos = position_of(distances, 0); zero_dist_pos != k_no_pos) {
		rg::fill(weights, 0);
		weights[zero_dist_pos] = 1;
		return;
	}

	const auto sum_inv_distances = std::accumulate(begin(distances), 
												   end(distances), 
												   0.0, 
												   [power_param](auto acc, auto dist) { 
												       return acc + 1.0 / pow(dist, power_param); 
												   });
	auto i = 0;
	for (auto dist : distances) {
		weights[i++] = static_cast<T>((1.0 / pow(dist, power_param)) / sum_inv_distances);
	}
}

}

/// @file UnivarDistrSample.hpp
/// UnivarDistrSample class template

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_MATH_UNIVAR_DISTR_SAMPLE_HPP
#define TWIST_MATH_UNIVAR_DISTR_SAMPLE_HPP

namespace twist::math {

/// Stores a sample of a one-dimensional distribution and provides some useful sample-related functionality.
///
/// @tparam  Sam  Numeric type which represents one sample value.
///
template<class Sam>
class UnivarDistrSample {
public:	
	using value_type = Sam;  ///< The sample value type
	using const_iterator = const Sam*;  ///< Constant iterator addressing a sample value
	using iterator = Sam*;  ///< Iterator addressing a sample value 

	/// Constructor. The sample values will be undefined.
	///
	/// @param[in] size  The number of samples
	///
	UnivarDistrSample(Ssize size); 

	/// Constructor. Copies over values from another sample.
	///
	/// @tparam  Iter  Forward iterator type, for addressing the sample values.
	/// @param[in] sample_begin  Iterator addressing the first value in the sample.
	/// @param[in] sample_end  Iterator addressing one past the final value in the sample.
	///
	template<class Iter,
	         class = EnableIfInputIterator<Iter, Sam>>
	UnivarDistrSample(Iter sample_begin, Iter sample_end); 

	/// Constructor. Use this to take ownership of an already existing sample buffer.
	///
	/// @param[in] buffer  The buffer containing the sample values
	/// @param[in] buffer_size  The buffer size (the number of values in the buffer)
	///
	UnivarDistrSample(std::unique_ptr<Sam[]> buffer, Ssize buffer_size);
	
	/// Get a clone of this distribution object.
	///
	/// @return  The clone.
	///
	std::unique_ptr<UnivarDistrSample> get_clone() const;

	/// Get a specific sample value.
	///
	/// @param[in] idx  The sample value index; the behaviour is undefined if the index is out of bounds
	/// @return  The sample value.
	///
	const Sam& operator[](Ssize idx) const; 

	/// Get access to a specific sample value.
	///
	/// @param[in] idx  The sample value index; the behaviour is undefined if the index is out of bounds
	/// @return  Ref to the sample value
	///
	Sam& operator[](Ssize idx); 
	
	/// Get a specific sample value.
	///
	/// @param[in] idx  The sample value index; an exception is thrown if the index is out of bounds
	/// @return  The sample value.
	///
	const Sam& at(Ssize idx) const; 

	/// Get access to a specific sample value.
	///
	/// @param[in] idx  The sample value index; an exception is thrown if the index is out of bounds
	/// @return  Ref to the sample value.
	///
	Sam& at(Ssize idx); 
	
	/// Get the first value in the sample.
	///
	/// @return  The first value.
	///
	Sam first() const; 
	
	/// Get the last value in the sample. 
	///
	/// @return  The last value.
	///
	Sam last() const; 
	
	/// Get an iterator addressing the first value in the sample.
	///
	/// @return  The iterator.
	///
	const_iterator begin() const; 
	
	/// Get an iterator addressing the first value in the sample.
	///
	/// @return  The iterator.
	///
	iterator begin(); 

	/// Get an iterator addressing one past the last value in the sample.
	///
	/// @return  The iterator.
	///
	const_iterator end() const; 

	/// Get an iterator addressing one past the last value in the sample.
	///
	/// @return  The iterator.
	///
	iterator end(); 

	/// Get the sample size (i.e. the number of values in the sample).
	///
	/// @return  The sample size.
	///
	Ssize size() const; 
	
	/// Get access to the internal sample buffer. Use only when absolutely necessary.
	///
	/// @return  The address of the buffer.
	///
	const Sam* buffer() const; 

	/// Get access to the internal sample buffer. Use only when absolutely necessary.
	///
	/// @return  The address of the buffer.
	///
	Sam* buffer(); 

	/// Get access to the internal sample buffer. Use only when absolutely necessary.
	///
	/// @return  The address of the buffer.
	///
	const Sam* data() const; 
	
	/// Sort the sample.
	///
	void sort(); 
	
	/// Found out whether the sample is sorted.
	///
	/// @return  true if it is.
	///
	bool is_sorted() const; 

	/// Get the smallest value in the sample.
	///
	/// @return  The smallest value.
	///	
	Sam min_val() const; 
	
	/// Get the greatest value in the sample.
	///
	/// @return  The greatest value.
	///	
	Sam max_val() const; 
	
	/// Get the smallest and the greatest values in the sample.
	///
	/// @param[in] min  The smallest (minimum) value
	/// @param[in] max  The greatest (maximum) value
	///	
	void min_max_val(Sam& min, Sam& max) const; 

	/// Get the smallest and the greatest values in the sample.
	///
	/// @return  0. The smallest (minimum) value
	///          1. The greatest (maximum) value
	///	
	std::tuple<Sam, Sam> min_max_val() const; 

	/// Fill the whole sample with the same value.
	///
	/// @param[in] val  The value.
	///	
	void fill(Sam val); 

	/// Resize the sample to contain a different number of values.
	/// If the new size is smaller than the current size, the content is reduced to its first values making up 
	///   the new size, the rest being dropped. If the new size is greater than the current size, the content 
	///   is expanded by inserting at the end undefined values, which may cause a reallocation.
	///
	/// @param[in] new_size  The new size
	///
	void resize(Ssize new_size); 
	
	/// Get a subset of this sample.
	///
	/// @tparam  Iter  Forward iterator type, for addressing sample value indexes.
	/// @param[in] val_indexes_begin  Iterator addressing the first element in the range of indexes of values 
	///					to be part of the subsample
	/// @param[in] val_indexes_end  Iterator addressing one past the final element in the range of indexes 
	///					of values to be part of the subsample
	/// @param[in] subsample  The subsample, it will be filled with the selected sample values.
	///
	template<class Iter,
	         class = EnableIfInputIterator<Iter, Ssize>>
	void subsample(Iter val_indexes_begin, Iter val_indexes_end, UnivarDistrSample& subsample) const; 

	/// Get pointers to the values in this sample. The pointers will be ordered the same as the values in the 
	/// sample.
	///
	/// @tparam  OutIter  Output iterator type, for outputting the sample value pointers
	/// @param[in] out_it  Output iterator
	/// 
	template<class OutIter,
	         class = EnableIfOutIterator<OutIter, Sam*>>
	void get_pointers(OutIter out_it) const; 

	/// Get pointers to the values in this sample. The pointers will be ordered the same as the values in the 
	/// sample.
	///
	/// @tparam  OutIter  Output iterator type, for outputting the sample value pointers
	/// @param[in] out_it  Output iterator
	/// 
	template<class OutIter,
	         class = EnableIfOutIterator<OutIter, Sam*>>
	void get_pointers(OutIter out_it); 

	/// Resample to a new sample; the new sample is drawn from the existing one using the "sampling with 
	/// replacement" technique.
	///
	/// @param[in] new_sample  The new sample (filed with values chosen randomly from this sample); can have 
	///					any size
	/// @param[in] rand_seed  The seed for the random number generator; pass in zero for a random seed value
	///
	void resample(UnivarDistrSample& new_sample, unsigned long rand_seed = 0) const; 

	TWIST_NO_COPY_DEF_MOVE(UnivarDistrSample<Sam>)

private:	
	Ssize  size_;
	std::unique_ptr<Sam[]>  sample_;
};

// --- Free functions ---

/// Get an iterator addressing the first value in a distribution sample.
///
/// @tparam  Sam  The type of a sample value 
/// @param[in] sample  The sample
/// @return  The iterator.
///
template<typename Sam>
typename UnivarDistrSample<Sam>::const_iterator begin(const UnivarDistrSample<Sam>& sample);  
	
/// Get an iterator addressing the first value in a distribution sample.
///
/// @tparam  Sam  The type of a sample value 
/// @param[in] sample  The sample
/// @return  The iterator.
///
template<typename Sam>
typename UnivarDistrSample<Sam>::iterator begin(UnivarDistrSample<Sam>& sample);

/// Get an iterator addressing one past the last value in a distribution sample.
///
/// @tparam  Sam  The type of a sample value 
/// @param[in] sample  The sample
/// @return  The iterator.
///
template<typename Sam>
typename UnivarDistrSample<Sam>::const_iterator end(const UnivarDistrSample<Sam>& sample);  

/// Get an iterator addressing one past the last value in a distribution sample.
///
/// @tparam  Sam  The type of a sample value 
/// @param[in] sample  The sample
/// @return  The iterator.
///
template<typename Sam>
typename UnivarDistrSample<Sam>::iterator end(UnivarDistrSample<Sam>& sample); 

}

#include "twist/math/UnivarDistrSample.ipp"

#endif 

/// @file PolarVector.ipp
/// Inline implementation file for "PolarVector.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace twist::math {

template<typename Coord>
PolarVector<Coord>::PolarVector(FromPolar, Coord magnitude, Coord direction)
{
	if (magnitude < 0) {
		magnitude = abs(magnitude);
		direction += k_pi;
	}
	magnitude_ = magnitude;
	direction_ = normalise_to_2pi(direction);
	TWIST_CHECK_INVARIANT
}


template<typename Coord>
PolarVector<Coord>::PolarVector(FromCartesian, Coord x_component, Coord y_component)
{	
	magnitude_ = sqrt(pow(x_component, 2) + pow(y_component, 2)),
	direction_ = normalise_to_2pi(atan2(y_component, x_component));  
	TWIST_CHECK_INVARIANT
}

	
template<typename Coord>
Coord PolarVector<Coord>::magnitude() const
{
	TWIST_CHECK_INVARIANT
	return magnitude_; 
}

	
template<typename Coord>
Coord PolarVector<Coord>::direction() const
{
	TWIST_CHECK_INVARIANT
	return direction_; 
}	

	
template<typename Coord>
Coord PolarVector<Coord>::x_component() const 
{
	TWIST_CHECK_INVARIANT
	return magnitude_ * cos(direction_);
}	
	

template<typename Coord>
Coord PolarVector<Coord>::y_component() const
{
	TWIST_CHECK_INVARIANT
	return magnitude_ * sin(direction_);
}	
	

template<typename Coord>
PolarVector<Coord> PolarVector<Coord>::add(PolarVector other)
{
	TWIST_CHECK_INVARIANT
	// We add the X- and Y-components separately
	auto resultant_x_component = x_component() + other.x_component();
	auto resultant_y_component = y_component() + other.y_component();
	return { from_cartesian, resultant_x_component, resultant_y_component };
}


#ifdef _DEBUG
template<typename Coord>
void PolarVector<Coord>::check_invariant() const noexcept
{
    if (magnitude_ == 0) {
        assert(direction_ == 0);
    }
}
#endif

}

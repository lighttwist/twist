/// @file FlatMatrix.ipp
/// Inline implementation file for "FlatMatrix.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/IndexRange.hpp"
#include "FlatMatrix.hpp"

namespace twist::math {

// --- FlatMatrix class template ---

template<class T>
FlatMatrix<T>::FlatMatrix(Ssize nof_rows, Ssize nof_cols)
	: nof_rows_{nof_rows}
	, nof_cols_{nof_cols}
	, data_(nof_rows * nof_cols)
{
	check_dimensions();
	TWIST_CHECK_INVARIANT
}

template<class T>
FlatMatrix<T>::FlatMatrix(Ssize nof_rows, Ssize nof_cols, const_reference value)
	: nof_rows_{nof_rows}
	, nof_cols_{nof_cols}
	, data_(nof_rows * nof_cols, value)
{
	check_dimensions();
	TWIST_CHECK_INVARIANT
}

template<class T>
FlatMatrix<T>::FlatMatrix(Ssize nof_rows, Ssize nof_cols, std::vector<T> values)
	: nof_rows_{nof_rows}
	, nof_cols_{nof_cols}
{
	check_dimensions();
	if (ssize(values) != nof_rows_ * nof_cols_) {
		TWIST_THRO2(L"The size ({}) of the values vector does not match the matrix dimensions ({}, {}).",
		            ssize(values), nof_rows_, nof_cols_);
	}
	data_ = move(values);
	TWIST_CHECK_INVARIANT
}

template<class T>
inline auto FlatMatrix<T>::clone() const -> FlatMatrix<T>
{
	TWIST_CHECK_INVARIANT
	auto clone = FlatMatrix<T>{nof_rows_, nof_cols_};
	clone.data_ = data_;
	return clone;
}

template<class T>
typename FlatMatrix<T>::iterator FlatMatrix<T>::begin() 
{ 
	TWIST_CHECK_INVARIANT
	return data_.begin(); 
}

template<class T>
typename FlatMatrix<T>::iterator FlatMatrix<T>::end() 
{ 
	TWIST_CHECK_INVARIANT
	return data_.end(); 
}

template<class T>
typename FlatMatrix<T>::const_iterator FlatMatrix<T>::begin() const 
{ 
	TWIST_CHECK_INVARIANT
	return data_.begin(); 
}

template<class T>
typename FlatMatrix<T>::const_iterator FlatMatrix<T>::end() const 
{ 
	TWIST_CHECK_INVARIANT
	return data_.end(); 
}
	
template<class T>
typename FlatMatrix<T>::const_iterator FlatMatrix<T>::cbegin() const 
{ 
	TWIST_CHECK_INVARIANT
	return data_.cbegin(); 
}

template<class T>
typename FlatMatrix<T>::const_iterator FlatMatrix<T>::cend() const 
{ 
	TWIST_CHECK_INVARIANT
	return data_.cend(); 
}

template<class T>
typename FlatMatrix<T>::reverse_iterator FlatMatrix<T>::rbegin() 
{ 
	TWIST_CHECK_INVARIANT
	return data_.rbegin(); 
}

template<class T>
typename FlatMatrix<T>::reverse_iterator FlatMatrix<T>::rend() 
{ 
	TWIST_CHECK_INVARIANT
	return data_.rend(); 
}

template<class T>
typename FlatMatrix<T>::const_reverse_iterator FlatMatrix<T>::rbegin() const 
{ 
	TWIST_CHECK_INVARIANT
	return data_.rbegin(); 
}

template<class T>
typename FlatMatrix<T>::const_reverse_iterator FlatMatrix<T>::rend() const 
{ 
	TWIST_CHECK_INVARIANT
	return data_.rend(); 
}

template<class T>
typename FlatMatrix<T>::const_reverse_iterator FlatMatrix<T>::crbegin() const 
{ 
	TWIST_CHECK_INVARIANT
	return data_.crbegin(); 
}

template<class T>
typename FlatMatrix<T>::const_reverse_iterator FlatMatrix<T>::crend() const 
{ 
	TWIST_CHECK_INVARIANT
	return data_.crend(); 
}

template<class T>
const T* FlatMatrix<T>::data() const 
{ 
	TWIST_CHECK_INVARIANT
	return data_.data(); 
}

template<class T>
T* FlatMatrix<T>::data() 
{ 
	TWIST_CHECK_INVARIANT
	return data_.data(); 
}

template<class T>
typename FlatMatrix<T>::reference FlatMatrix<T>::operator()(Ssize row, Ssize col)
{
	TWIST_CHECK_INVARIANT
	return data_[nof_cols_ * row + col];
}

template<class T>
typename FlatMatrix<T>::const_reference FlatMatrix<T>::operator()(Ssize row, Ssize col) const
{
	TWIST_CHECK_INVARIANT
	return data_[nof_cols_ * row + col];
}

template<class T>
typename FlatMatrix<T>::reference FlatMatrix<T>::operator()(GridCellIndex cell_index)
{
	TWIST_CHECK_INVARIANT
	return data_[static_cast<Ssize>(cell_index)];
}

template<class T>
typename FlatMatrix<T>::const_reference FlatMatrix<T>::operator()(GridCellIndex cell_index) const
{
	TWIST_CHECK_INVARIANT
	return data_[static_cast<Ssize>(cell_index)];
}

template<class T>
typename FlatMatrix<T>::reference FlatMatrix<T>::at(Ssize row, Ssize col)
{
	TWIST_CHECK_INVARIANT
	return data_.at(nof_cols_ * row + col);
}

template<class T>
typename FlatMatrix<T>::const_reference FlatMatrix<T>::at(Ssize row, Ssize col) const
{
	TWIST_CHECK_INVARIANT
	return data_.at(nof_cols_ * row + col);
}

template<class T>
typename FlatMatrix<T>::reference FlatMatrix<T>::at(GridCellIndex cell_index)
{
	TWIST_CHECK_INVARIANT
	return data_.at(cell_index);
}

template<class T>
typename FlatMatrix<T>::const_reference FlatMatrix<T>::at(GridCellIndex cell_index) const
{
	TWIST_CHECK_INVARIANT
	return data_.at(cell_index);
}

template<class T>
auto FlatMatrix<T>::resize(Ssize new_row_count, Ssize new_col_count) -> void
{
	TWIST_CHECK_INVARIANT
	auto tmp = FlatMatrix{new_row_count, new_col_count};
	auto mc = std::min(nof_cols_, new_col_count);
	auto mr = std::min(nof_rows_, new_row_count);
	for (auto i : IndexRange{mr}) {
		auto row = begin() + i * nof_cols_;
		auto tmp_row = tmp.begin() + i * new_col_count;
		std::move(row, row + mc, tmp_row);
	}
	*this = std::move(tmp);
}

template<class T>
Ssize FlatMatrix<T>::size() const 
{ 
	TWIST_CHECK_INVARIANT
	return ssize(data_); 
}

template<class T>
bool FlatMatrix<T>::empty() const 
{ 
	TWIST_CHECK_INVARIANT
	return data_.empty(); 
}
	
template<class T>
auto FlatMatrix<T>::nof_rows() const -> Ssize 
{ 
	TWIST_CHECK_INVARIANT
	return nof_rows_; 
}
	
template<class T>
auto FlatMatrix<T>::nof_columns() const -> Ssize 
{ 
	TWIST_CHECK_INVARIANT
	return nof_cols_; 
}

template<class T>
auto FlatMatrix<T>::nof_cells() const -> Ssize 
{
	TWIST_CHECK_INVARIANT
	return nof_rows_ * nof_cols_; 	
}

template<class T>
auto FlatMatrix<T>::get_row(Ssize row_idx) const -> CellRange<const_iterator> 
{
	TWIST_CHECK_INVARIANT
	return view_row_cells(row_idx, 0, nof_cols_);		
}

template<class T>
auto FlatMatrix<T>::view_row_cells(Ssize row_idx) const -> CellRange<const_iterator> 
{
	TWIST_CHECK_INVARIANT
	return view_row_cells(row_idx, 0, nof_cols_);		
}

template<class T>
auto FlatMatrix<T>::view_row_cells(Ssize row) -> CellRange<iterator>
{
	TWIST_CHECK_INVARIANT
	return view_row_cells(row, 0, nof_cols_);		
}

template<class T>
FlatMatrix<T>::CellRange<typename FlatMatrix<T>::const_iterator> 
FlatMatrix<T>::view_row_cells(Ssize row, Ssize begin_col, Ssize cell_count) const
{
	TWIST_CHECK_INVARIANT
	check_row_col(row, begin_col);
	if (cell_count == 0 || begin_col + cell_count > nof_cols_) {
		TWIST_THROW(L"Invalid begin/end columns %d/%d.", begin_col, begin_col + cell_count - 1);
	}
	return CellRange<const_iterator>{data_.cbegin() + nof_cols_ * row + begin_col, 
									 data_.cbegin() + nof_cols_ * row + begin_col + cell_count};
}

template<class T>
FlatMatrix<T>::CellRange<typename FlatMatrix<T>::iterator> 
FlatMatrix<T>::view_row_cells(Ssize row, Ssize begin_col, Ssize cell_count)
{
	TWIST_CHECK_INVARIANT
	check_row_col(row, begin_col);
	if (cell_count == 0 || begin_col + cell_count > nof_cols_) {
		TWIST_THROW(L"Invalid begin/end columns %d/%d.", begin_col, begin_col + cell_count - 1);
	}
	return CellRange<iterator>{data_.begin() + nof_cols_ * row + begin_col, 
			                   data_.begin() + nof_cols_ * row + begin_col + cell_count};
}

template<class T>
FlatMatrix<T>::CellRange<typename FlatMatrix<T>::const_iterator> 
FlatMatrix<T>::view_rows(Ssize begin_row, Ssize end_row) const
{
	TWIST_CHECK_INVARIANT
	check_begin_end_rows(begin_row, end_row);

	return CellRange<const_iterator>{data_.begin() + nof_cols_ * begin_row, 
			                         data_.begin() + nof_cols_ * (end_row + 1)};
}

template<class T>
auto FlatMatrix<T>::view_matrix() const -> CellRange<const_iterator>
{
	TWIST_CHECK_INVARIANT
	return CellRange<const_iterator>{data_.begin(), data_.end()};
}

template<class T>
auto FlatMatrix<T>::view_matrix() -> CellRange<iterator>
{
	TWIST_CHECK_INVARIANT
	return CellRange<iterator>{data_.begin(), data_.end()};
}

template<class T>
void FlatMatrix<T>::check_begin_end_rows(Ssize begin_row, Ssize end_row) const
{
	TWIST_CHECK_INVARIANT
	if (begin_row < 0 || nof_rows_ <= begin_row) {
		TWIST_THROW(L"Invalid begin row index %d.", begin_row);
	}
	if (end_row < 0 || nof_rows_ <= end_row) {
		TWIST_THROW(L"Invalid end row index %d.", begin_row);
	}
	if (begin_row > end_row) {
		TWIST_THROW(L"The end row index %d is smaller than the begin row index %d.", end_row, begin_row);
	}
}

template<class T>
void FlatMatrix<T>::check_row_col(Ssize row, Ssize col) const
{
	TWIST_CHECK_INVARIANT
	if (row >= nof_rows_ || col >= nof_cols_) {
		TWIST_THRO2(L"Invalid cell position ({}, {}).", row, col);
	}
}

template<class T>
auto FlatMatrix<T>::check_dimensions() const -> void
{
	if (nof_rows_ < 0 || nof_cols_ < 0) {
		TWIST_THRO2(L"Invalid matrix dimension sizes {}, {}.", nof_rows_, nof_cols_);
	}
	if ((nof_rows_ == 0) != (nof_cols_ == 0)) {
		TWIST_THRO2(L"Invalid matrix dimension sizes {}, {}.", nof_rows_, nof_cols_);		
	}
}

#ifdef _DEBUG
template<class T>
auto FlatMatrix<T>::check_invariant() const noexcept -> void
{
	assert(nof_rows_ >= 0);
	assert(nof_cols_ >= 0);
	assert((nof_rows_ == 0) == (nof_cols_ == 0));
	assert(ssize(data_) == nof_rows_ * nof_cols_);
}
#endif

// --- FlatMatrix::CellRange class template ---

template<class T> 
template<class Iter> 
FlatMatrix<T>::CellRange<Iter>::CellRange(Iter first, Iter last) 
	: Range<Iter>{first, last} 
{
}

template<class T> 
template<class Iter> 
auto FlatMatrix<T>::CellRange<Iter>::data() const -> const T*
{
	assert(this->begin() != this->end());

	const auto& first = *this->begin();
	return &first;
}

template<class T> 
template<class Iter> 
auto FlatMatrix<T>::CellRange<Iter>::data() -> T*
{
	assert(this->begin() != this->end());

	auto& first = *this->begin();
	return &first;
}

// --- Free functions ---

template<class T>
void swap(FlatMatrix<T>& a, FlatMatrix<T>& b) noexcept
{
	using std::swap;
	a.data_.swap(b.data_);
	swap(a.nof_rows_, b.nof_rows_);
	swap(a.nof_cols_, b.nof_cols_);
}

template<class T>
auto operator==(const FlatMatrix<T>& lhs, const FlatMatrix<T>& rhs) -> bool
{
	if (lhs.nof_rows() != rhs.nof_rows() || lhs.nof_columns() != rhs.nof_columns()) {
		return false;
	}
	return std::equal(lhs.begin(), lhs.end(), rhs.begin(), rhs.end());
}

template<class T>
auto operator!=(const FlatMatrix<T>& lhs, const FlatMatrix<T>& rhs) -> bool
{
	return !(lhs == rhs);
}

template<class T>
auto dims(const FlatMatrix<T>& mat) -> std::tuple<Ssize, Ssize> 
{
	return {mat.nof_rows(), mat.nof_columns()};
}

template<class T> 
auto is_valid_submatrix_of(const SubgridInfo& submatrix_info, const FlatMatrix<T>& mat) -> bool
{
	return submatrix_info.begin_row() <= mat.nof_rows() && submatrix_info.end_row() <= mat.nof_rows() &&
		   submatrix_info.begin_column() <= mat.nof_columns() && submatrix_info.end_column() <= mat.nof_columns();
}

template<class T, class Fn>
requires std::invocable<Fn, const T&, Ssize, Ssize>
auto for_each_cell(const FlatMatrix<T>& mat, Fn func) -> void
{
	for (auto i : IndexRange{mat.nof_rows()}) {
		for (auto j : IndexRange{mat.nof_columns()}) {
			std::invoke(func, mat(i, j), i, j);
		}	
	}
}

template<class T, class Fn>
requires std::invocable<Fn, T&, Ssize, Ssize>
auto for_each_cell(FlatMatrix<T>& mat, Fn func) -> void
{
	for (auto i : IndexRange{mat.nof_rows()}) {
		for (auto j : IndexRange{mat.nof_columns()}) {
			std::invoke(func, mat(i, j), i, j);
		}	
	}
}

template<class SrcT, class DestT> 
auto copy_into(const FlatMatrix<SrcT>& src_mat, FlatMatrix<DestT>& dest_mat,
	           std::optional<SubgridInfo> src_submatrix_info, Ssize dest_top_row, Ssize dest_left_col) -> void
{
	if (src_submatrix_info && !is_valid_submatrix_of(*src_submatrix_info, src_mat)) {
		TWIST_THROW(L"The source submatrix info does not represent a valid submatrix of the source matrix.");
	}

	const auto nof_src_rows = src_submatrix_info ? nof_rows(*src_submatrix_info) : src_mat.nof_rows();
	const auto nof_src_cols = src_submatrix_info ? nof_columns(*src_submatrix_info) : src_mat.nof_columns();
	if (nof_src_cols + dest_left_col > dest_mat.nof_columns() || 
			nof_src_rows + dest_top_row > dest_mat.nof_rows()) {
		TWIST_THROW(L"The source (sub)matrix exceeds the boundaries of the destination matrix.");
	}

	const auto src_begin_row = src_submatrix_info ? src_submatrix_info->begin_row() : 0;
	const auto src_end_row = src_submatrix_info ? src_submatrix_info->end_row() : src_mat.nof_rows() - 1;

	// Check for a special case: is the data to be copied contiguous both at source and at destination?
	// If so, copy it in one chunk.
	if (!src_submatrix_info || nof_columns(*src_submatrix_info) == src_mat.nof_columns()) {
		// The data is contiguous in the dource matrix
		if (dest_mat.nof_columns() == nof_src_cols) {			
			assert(dest_left_col == 0); // Sanity check
			// The data will be stored contiguously in the destination matrix
			rg::copy(src_mat.view_rows(src_begin_row, src_end_row), 
			             dest_mat.begin() + dest_top_row * nof_src_cols);
			return;
		}
	}

	// General case: copy data row by row
	const auto src_begin_col = src_submatrix_info ? src_submatrix_info->begin_column() : 0;
	const auto dest_begin = dest_mat.begin() + dest_top_row * dest_mat.nof_columns() + dest_left_col;
	for (auto src_row : IndexRange{src_begin_row, src_end_row + 1}) {
		rg::copy(src_mat.view_row_cells(src_row, src_begin_col, nof_src_cols),
			         dest_begin + (src_row - src_begin_row) * dest_mat.nof_columns());
	}
}

template<class T>
auto flip_rows(FlatMatrix<T>& mat) -> void
{
	if (mat.empty()) {
		return;
	}

	auto swap_rows = [&mat, temp_row_buffer = std::vector<T>(mat.nof_columns())](auto i1, auto i2) mutable {
		auto row1_cells = mat.view_row_cells(i1);
		auto row2_cells = mat.view_row_cells(i2);
		rg::copy(row1_cells, temp_row_buffer.begin());
		rg::copy(row2_cells, row1_cells.begin());
		rg::copy(temp_row_buffer, row2_cells.begin());
	};

	for (auto i : IndexRange{mat.nof_rows() / 2}) {
		swap_rows(i, mat.nof_rows() - 1 - i);
	}
}

}

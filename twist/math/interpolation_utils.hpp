/// @file interpolation_utils.hpp
/// Utilities related to various interpolation techinques

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_MATH_INTERPOLATION__UTILS_HPP
#define TWIST_MATH_INTERPOLATION__UTILS_HPP

namespace twist::math {

/*! Get the list of weights to be used for a interpolating some value associated with a point in space, from a set of 
    other known values each associated with other points in space, using the basic form of Shepard's method for 
	"inverse distance weighting".
    \note See also https://en.wikipedia.org/wiki/Inverse_distance_weighting
    \param[in] distances  The distances from each of the points with known values to the interpolation point
    \param[in] power_param  The "power parameter" in Shepard's formula; 2 is a fairly typical value
    \param[in] weights  List which will be populated with the weights for each of the known values; in the same order 
                        as the distances passed in; if the list size differs from the number of distances, an exception
                        is thrown
 */
template<std::floating_point T,
         rg::input_range Rng>
requires std::is_same_v<rg::range_value_t<Rng>, T>
[[nodiscard]] auto calc_inverse_distance_weights(const Rng& distances, double power_param, std::vector<T>& weights)
                    -> void;

}

#include "twist/math/interpolation_utils.ipp"

#endif 

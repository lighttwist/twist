/// @file Dag.ipp
/// Inline implementation file for "Dag.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include <stack>

namespace twist::math {

// --- DagNode class template ---
 
template<class Data>
DagNode<Data>::DagNode(Data data)
    : data_{std::move(data)}
{
	TWIST_CHECK_INVARIANT
}

template<class Data>
auto DagNode<Data>::data() -> const Data&
{
	TWIST_CHECK_INVARIANT
    return data_;
}

template<class Data>
auto DagNode<Data>::nof_parents() const -> Ssize
{
	TWIST_CHECK_INVARIANT
	return ssize(parents_);
}

template<class Data>
auto DagNode<Data>::parents() const -> Range<NodeIter>
{
	TWIST_CHECK_INVARIANT
	return Range{parents_.begin(), parents_.end()};
}

template<class Data>
auto DagNode<Data>::nof_children() const -> Ssize
{
	TWIST_CHECK_INVARIANT
	return ssize(children_);
}

template<class Data>
auto DagNode<Data>::children() const -> Range<NodeIter>
{
	TWIST_CHECK_INVARIANT
	return Range{children_.begin(), children_.end()};
}

#ifdef _DEBUG
template<class Data>
auto DagNode<Data>::check_invariant() const noexcept -> void
{
}
#endif

// --- Dag class template ---

template<class Data>
Dag<Data>::Dag(PrintData print_data)
    : print_data_{move(print_data)}
{
	TWIST_CHECK_INVARIANT
}

template<class Data>
auto Dag<Data>::nof_nodes() const -> Ssize
{
	TWIST_CHECK_INVARIANT
    return ssize(nodes_);
}

template<class Data>
auto Dag<Data>::nodes() const 
{
	TWIST_CHECK_INVARIANT
    return nodes_ | vw::transform([](const auto& node) { return NodePtr{node.get()}; });
}

template<class Data>
auto Dag<Data>::add_node(Data data) -> NodePtr
{
	TWIST_CHECK_INVARIANT
    auto node = std::unique_ptr<Node>{new Node{std::move(data)}};
    nodes_.push_back(move(node));
    return NodePtr{nodes_.back().get()};
}

template<class Data>
auto Dag<Data>::add_arc(NodePtr parent, NodePtr child) -> void
{
	TWIST_CHECK_INVARIANT
    if (has(parent->children_, child)) {
		TWIST_THRO2(L"Node \"{}\" is already a child of node \"{}\".", 
		            print_data_(child->data_), print_data_(parent->data_));
	}
    if (has(child->parents_, parent)) {
		TWIST_THRO2(L"Node \"{}\" is already a parent of node \"{}\".", 
		            print_data_(parent->data_), print_data_(child->data_));
	}	
    // Add the child to the parent's children
    parent->children_.push_back(child);	
    // Add the parent to the child's parents
    child->parents_.push_back(parent);
}

template<class Data>
auto Dag<Data>::print_data(NodePtr node) const -> std::wstring
{
	TWIST_CHECK_INVARIANT
    return print_data_(node->data());
}

#ifdef _DEBUG
template<class Data>
auto Dag<Data>::check_invariant() const noexcept -> void
{
    assert(print_data_);
}
#endif

// --- Free functions ---

template<class Data>
auto is_child_of(DagNodePtr<Data> parent, DagNodePtr<Data> child) -> bool
{
    return twist::has(child->parents(), parent);
}

namespace detail {

template<class Data>
struct NodeInfo {
	NodeInfo(DagNodePtr<Data> the_node) : node{the_node} {}
	DagNodePtr<Data> node;
	int index{-1};
	int lowlink{-1};
	bool on_stack{false};
    TWIST_NO_COPY_DEF_MOVE(NodeInfo)
};

template<class Data>
class StrongconnectImpl {
public:
    using NodeInfo = NodeInfo<Data>;    
    using NodePtr = DagNodePtr<Data>;
    using NodePtrs = DagNodePtrs<Data>;

    template<class DagNodeRange>
    StrongconnectImpl(const DagNodeRange& nodes)
    {
        for (const auto& node : nodes) { 
            nodes_info_.emplace(node.get(), NodeInfo{node.get()}); 
        }
    }

    auto run() -> std::tuple<std::vector<NodePtrs>, NodePtrs>
    {
        // Implemented using Tarjan's strongly connected components algorithm, see pseudocode at
        // https://en.wikipedia.org/wiki/Tarjan%27s_strongly_connected_components_algorithm

        auto index = 0;
        for (auto& [_, v] : nodes_info_) {
            if (v.index == -1) {
                strongconnect(v, index);
            }
        }
        if (!sccs_.empty()) {
            topol_nodes_.clear();
        }

        assert(sccs_.empty() ? ssize(topol_nodes_) == ssize(nodes_info_) : topol_nodes_.empty());

        return {move(sccs_), move(topol_nodes_)};
    }
        
private:
    auto strongconnect(NodeInfo& v, int& index) -> void
    {
        // Set the depth index for v to the smallest unused index
        v.index = index;
        v.lowlink = index;
        ++index;
        vertex_stack_.push(&v);
        v.on_stack = true;
      
        // Consider successors of v
        for (auto pw : v.node->children()) {
            auto& w = nodes_info_.at(pw);
            if (w.index == -1) {
                // Successor w has not yet been visited; recurse on it
                strongconnect(w, index);
                v.lowlink = std::min(v.lowlink, w.lowlink);
            }
            else if (w.on_stack) {
                // Successor w is in stack vertex_stack_ and hence in the current SCC
                // If w is not on stack, then (v, w) is an edge pointing to an SCC already found and must be ignored
                // Note: The next line may look odd - but is correct.
                // It says w->index not w->lowlink; that is deliberate and from the original paper
                v.lowlink = std::min(v.lowlink, w.index);
            }
        }

        // If v is a root node, pop the stack and generate an SCC
        if (v.lowlink == v.index) {
            auto scc = std::vector<NodePtr>{};
            auto w = (NodeInfo*)nullptr;
            do {
                w = vertex_stack_.top();
                vertex_stack_.pop();
                w->on_stack = false;
                scc.push_back(w->node);
            }
            while (w != &v);

            if (ssize(scc) == 1) {
                topol_nodes_.insert(begin(topol_nodes_), scc[0]);   
            }
            else {
                sccs_.push_back(move(scc));
            }
        }
    }

    std::unordered_map<NodePtr, NodeInfo> nodes_info_;
    std::stack<NodeInfo*> vertex_stack_;
    std::vector<NodePtr> topol_nodes_;
    std::vector<std::vector<NodePtr>> sccs_;
};

template<class Data>
auto store_and_recurse_on_connected_nodes(DagNodePtr<Data> node, std::unordered_set<DagNodePtr<Data>>& store) -> void 
{
    store.insert(node);
    for (auto parent : node->parents()) {
        if (!store.contains(parent)) {
            store_and_recurse_on_connected_nodes(parent, store);
        }
    }
    for (auto child : node->children()) {
        if (!store.contains(child)) {
            store_and_recurse_on_connected_nodes(child, store);
        }
    }
}

}

template<DagNodeRange Rng>
auto get_connected_graphs(const Rng& nodes) -> std::vector<std::unordered_set<DagNodePtrFromRange<Rng>>>
{
    auto conn_graphs = std::vector<std::unordered_set<DagNodePtrFromRange<Rng>>>{};

    auto is_processed = [&conn_graphs](auto node) {
        for (const auto& conn_graph : conn_graphs) {
            if (conn_graph.contains(node)) {
                return true;  
            }
        }
        return false;
    };

    for (auto node : nodes) {
        if (!is_processed(node)) {
            auto conn_graph = std::unordered_set<DagNodePtrFromRange<Rng>>{};
            detail::store_and_recurse_on_connected_nodes(node, conn_graph);
            conn_graphs.push_back(move(conn_graph));
        }
    }

    return conn_graphs;
}

template<DagNodeRange Rng>
auto find_sccs_or_topol_order(const Rng& nodes) -> std::tuple<std::vector<DagNodePtrsFromRange<Rng>>, 
                                                              DagNodePtrsFromRange<Rng>>
{
    return detail::StrongconnectImpl<DagDataFromRange<Rng>>{nodes}.run();
}

auto are_topol_ordered(const DagNodeRange auto& nodes) -> bool
{
    auto ensure_all_parents_before = [&nodes](auto i) {
        auto prev_nodes = Range{begin(nodes), next(begin(nodes), i)};
        for (auto parent : nodes[i]->parents()) {
            if (!has(prev_nodes, parent)) {
                return false;
            }
        }
        return true;
    };
    for (auto i : IndexRange{Ssize{1}, ssize(nodes)}) {
        if (!ensure_all_parents_before(i)) {
            return false;
        }
    }
    return true;
}

template<DagNodeRange Rng>
requires std::is_copy_constructible_v<DagDataFromRange<Rng>>
auto extract_data(const Rng& nodes) -> std::vector<DagDataFromRange<Rng>>
{
    return nodes | vw::transform([](auto node) { return node->data(); }) | rg::to<std::vector>();
}

}

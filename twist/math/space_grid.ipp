/// @file SquareSpaceGrid.ipp
/// Inline implementation file for "SquareSpaceGrid.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace twist::math {

// --- Local functions ---

namespace detail {

template<class Coord>
void ensure_valid_row_col(const SpaceGrid<Coord>& grid, Ssize row, Ssize col)
{
	if (row < 0 || row >= grid.nof_rows() || col < 0 || col >= grid.nof_columns()) {
		TWIST_THRO2(L"Invalid grid cell position ({}, {}).", row, col);
	}
}

}

// --- SpaceGrid class template ---

template<class Coord>
SpaceGrid<Coord>::SpaceGrid(Coord left, Coord bottom, Coord cell_width, Coord cell_height, 
                            Ssize nof_rows, Ssize nof_cols) 
	: left_{left}
	, bottom_{bottom}
	, cell_width_{cell_width}
	, cell_height_{cell_height}
	, nof_rows_{nof_rows}
	, nof_cols_{nof_cols}
{
	TWIST_CHECK_INVARIANT
}

template<class Coord>
auto SpaceGrid<Coord>::nof_rows() const -> Ssize
{
	TWIST_CHECK_INVARIANT
	return nof_rows_;
}

template<class Coord>
auto SpaceGrid<Coord>::nof_columns() const -> Ssize
{
	TWIST_CHECK_INVARIANT
	return nof_cols_;
}

template<class Coord>
auto SpaceGrid<Coord>::cell_width() const -> Coord
{
	TWIST_CHECK_INVARIANT
	return cell_width_;
}

template<class Coord>
auto SpaceGrid<Coord>::cell_height() const -> Coord
{
	TWIST_CHECK_INVARIANT
	return cell_height_;
}

template<class Coord>
auto SpaceGrid<Coord>::left() const -> Coord
{
	TWIST_CHECK_INVARIANT
	return left_;
}

template<class Coord>
auto SpaceGrid<Coord>::bottom() const -> Coord
{
	TWIST_CHECK_INVARIANT
	return bottom_;
}

template<class Coord>	
auto SpaceGrid<Coord>::right() const -> Coord
{
	TWIST_CHECK_INVARIANT
	return left_ + static_cast<Coord>(cell_width_ * nof_cols_);
}

template<class Coord>
auto SpaceGrid<Coord>::top() const -> Coord
{
	TWIST_CHECK_INVARIANT
	return bottom_ + static_cast<Coord>(cell_height_ * nof_rows_);
}

#ifdef _DEBUG
template<class Coord>
auto SpaceGrid<Coord>::check_invariant() const noexcept -> void 
{
	assert(cell_width_ > 0);
	assert(cell_height_ > 0);
	assert(nof_rows_ > 0);
	assert(nof_cols_ > 0);
}
#endif

// --- SquareSpaceGrid class template ---

template<class Coord>
SquareSpaceGrid<Coord>::SquareSpaceGrid(Coord left, Coord bottom, Coord cell_size, Ssize nof_rows, Ssize nof_cols) 
	: SpaceGrid<Coord>{left, bottom, cell_size, cell_size, nof_rows, nof_cols}
{
}

template<class Coord>
auto SquareSpaceGrid<Coord>::cell_size() const -> Coord
{
	return this->cell_width();
}

// --- Non-member functions ---

template<class Coord>
auto dims(const SquareSpaceGrid<Coord>& grid) -> std::tuple<Ssize, Ssize>
{
	return {grid.nof_rows(), grid.nof_columns()};
}

template<class Coord>
[[nodiscard]] auto nof_cells(const SpaceGrid<Coord>& grid) -> Ssize
{
	return grid.nof_rows() * grid.nof_columns();
}

template<class Coord>
auto perimeter_rect(const SpaceGrid<Coord>& grid) -> Rectangle<Coord>
{
	return Rectangle<Coord>{grid.left(), grid.bottom(), grid.right(), grid.top()};
}

template<class Coord>
[[nodiscard]] auto max_cell_index(const SpaceGrid<Coord>& grid) -> GridCellIndex
{
	return GridCellIndex{nof_cells(grid) - 1};
}

template<class Coord>
[[nodiscard]] auto cell_area(const SquareSpaceGrid<Coord>& grid) -> Coord
{
	return grid.cell_size() * grid.cell_size();
}

template<class Coord>
[[nodiscard]] auto get_cell_index_from_row_col(const SquareSpaceGrid<Coord>& grid, Ssize row, Ssize col) 
                    -> GridCellIndex
{
	return grid_cell_index_from_row_col(row, col, grid.nof_rows(), grid.nof_columns());
}

template<class Coord>
[[nodiscard]] auto get_cell_row_col_from_index(const SquareSpaceGrid<Coord>& grid, GridCellIndex cell_index) 
                    -> std::tuple<Ssize, Ssize>
{
	return grid_row_col_from_cell_index(cell_index, grid.nof_rows(), grid.nof_columns());
}

template<class Coord>
[[nodiscard]] auto get_cell_row_col_from_pos(const SpaceGrid<Coord>& grid,
                                             Point<Coord> pos,
                                             RowFromEdgeChoice row_choice,
                                             ColFromEdgeChoice col_choice) -> std::tuple<Ssize, Ssize>
{
	if (pos.x() < grid.left() || pos.x() > grid.right() || pos.y() < grid.bottom() || pos.y() > grid.top()) {
		TWIST_THRO2(L"Position ({}, {}) falls outside of the grid.", pos.x(), pos.y());
	}

	double row_double = (grid.top() - pos.y())  / grid.cell_height();
	double col_double = (pos.x() - grid.left()) / grid.cell_width();
	auto row = floor_to_int(row_double);
	auto col = floor_to_int(col_double);

	if (row == row_double) {
		if ((row_choice == RowFromEdgeChoice::top && row > 0) || (row == grid.nof_rows())) {
			--row;
		}
	}
	if (col == col_double) { 
		if ((col_choice == ColFromEdgeChoice::left && col > 0) || (col == grid.nof_columns())) {
			--col;
		}
	}

	return {row, col};
}

template<class Coord>
[[nodiscard]] auto get_cell_index_from_pos(const SquareSpaceGrid<Coord>& grid,
                                           Point<Coord> pos,
                                           RowFromEdgeChoice row_choice,
                                           ColFromEdgeChoice col_choice) -> GridCellIndex
{
	const auto [row, col] = get_cell_row_col_from_pos(grid, pos, row_choice, col_choice);
	return grid_cell_index_from_row_col(row, col, grid.nof_rows(), grid.nof_columns());
}

template<class Coord>
[[nodiscard]] auto get_cell_rect_from_row_col(const SquareSpaceGrid<Coord>& grid,
                                              Ssize row,
                                              Ssize col) -> Rectangle<Coord>
{
	detail::ensure_valid_row_col(grid, row, col);

	return {grid.left() + grid.cell_size() * col,
			grid.top()  - grid.cell_size() * (row + 1),
			grid.left() + grid.cell_size() * (col + 1),
			grid.top()  - grid.cell_size() * row};
}

template<std::floating_point Coord>
[[nodiscard]] auto get_cell_centre_from_row_col(const SquareSpaceGrid<Coord>& grid, Ssize row, Ssize col) 
                    -> Point<Coord>
{
	detail::ensure_valid_row_col(grid, row, col);

	return {grid.left() + static_cast<Coord>(grid.cell_size() * (col + 0.5)),
			grid.top()  - static_cast<Coord>(grid.cell_size() * (row + 0.5))};
}

template<std::floating_point Coord>
[[nodiscard]] auto get_cell_centre_from_index(const SquareSpaceGrid<Coord>& grid, GridCellIndex cell_index) 
                    -> Point<Coord>
{
	const auto [row, col] = grid_row_col_from_cell_index(cell_index, grid.nof_rows(), grid.nof_columns());
	return get_cell_centre_from_row_col(grid, row, col);
}

template<class Coord>
[[nodiscard]] auto get_subgrid(const SpaceGrid<Coord>& grid, SubgridInfo subgrid_info) -> SpaceGrid<Coord>
{
	detail::ensure_valid_row_col(grid, subgrid_info.begin_row(), subgrid_info.begin_column());
	detail::ensure_valid_row_col(grid, subgrid_info.end_row(), subgrid_info.end_column());

	return {grid.left() + grid.cell_width() * subgrid_info.begin_column(),
			grid.top() - grid.cell_height() * (subgrid_info.end_row() + 1),
			grid.cell_width(),
			grid.cell_height(),
			nof_rows(subgrid_info),
			nof_columns(subgrid_info)};
}

template<class Coord>
[[nodiscard]] auto get_subgrid(const SquareSpaceGrid<Coord>& grid, SubgridInfo subgrid_info) -> SquareSpaceGrid<Coord>
{
	detail::ensure_valid_row_col(grid, subgrid_info.begin_row(), subgrid_info.begin_column());
	detail::ensure_valid_row_col(grid, subgrid_info.end_row(), subgrid_info.end_column());

	return {grid.left() + grid.cell_size() * subgrid_info.begin_column(),
			grid.top() - grid.cell_size() * (subgrid_info.end_row() + 1),
			grid.cell_size(),
			nof_rows(subgrid_info),
			nof_columns(subgrid_info)};
}

template<class Coord>
[[nodiscard]] auto get_subgrid_rect(const SquareSpaceGrid<Coord>& grid, SubgridInfo subgrid_info) -> Rectangle<Coord> 
{
	detail::ensure_valid_row_col(grid, subgrid_info.begin_row(), subgrid_info.begin_column());
	detail::ensure_valid_row_col(grid, subgrid_info.end_row(), subgrid_info.end_column());

	return {grid.left() + subgrid_info.begin_column() * grid.cell_size(),
			grid.top()  - subgrid_info.begin_row() * grid.cell_size(),
			grid.left() + (subgrid_info.end_column() + 1) * grid.cell_size(),
			grid.top()  - (subgrid_info.end_row() + 1) * grid.cell_size()};
}

template<class Coord>
[[nodiscard]] auto get_info_of_subgrid_containing_rect(const SpaceGrid<Coord>& grid,
		                                               const Rectangle<Coord>& rect,
													   double snap_tol) -> std::optional<SubgridInfo>
{
	assert(snap_tol >= 0);

	auto intersect_rect = intersect(perimeter_rect(grid), rect);
	if (!intersect_rect) {
		return {};
	}

	const auto cell_width_dbl = static_cast<double>(grid.cell_width());
	const auto cell_height_dbl = static_cast<double>(grid.cell_height());

	auto j_start = floor_to_int((intersect_rect->left() - grid.left()) / cell_width_dbl);
	auto i_start = floor_to_int((grid.top() - intersect_rect->top()) / cell_height_dbl);

	// Count the columns, complete or incomplete, which appear to the left of the right edge of the rectangle
	const auto cols_before_rect_right_unrounded = (intersect_rect->right() - grid.left()) / cell_width_dbl;
	const auto cols_before_rect_right = 
			equal_tol(cols_before_rect_right_unrounded, floor(cols_before_rect_right_unrounded), snap_tol)
					? static_cast<Ssize>(floor(cols_before_rect_right_unrounded))
					: static_cast<Ssize>(ceil(cols_before_rect_right_unrounded));

	auto j_stop = cols_before_rect_right - 1;
	if (grid.left() + cols_before_rect_right * cell_width_dbl != intersect_rect->right()) {  
		// The right edge of the rect does not fall along a column edge
		if (j_stop == j_start) {
			return {}; // The rectangle is not wide enough to cover a whole cell //+TODO: Is it possible to hit this, as we already check for that? DAN 15May'24
		}
	}

	// Count the rows, complete or incomplete, which appear above of the bottom edge of the rectangle
	const auto rows_above_rect_bottom_unrounded = (grid.top() - intersect_rect->bottom()) / cell_height_dbl;
	const auto rows_above_rect_bottom = 
			equal_tol(rows_above_rect_bottom_unrounded, floor(rows_above_rect_bottom_unrounded), snap_tol)
					? static_cast<Ssize>(floor(rows_above_rect_bottom_unrounded))
					: static_cast<Ssize>(ceil(rows_above_rect_bottom_unrounded));

	auto i_stop = rows_above_rect_bottom - 1;
	if (grid.top() - rows_above_rect_bottom * cell_height_dbl != intersect_rect->bottom()) {  
		// The bottom edge of the rectangle does not fall along a row edge
		if (i_stop == i_start) {
			return {}; // The rectangle is not wide enough to cover a whole cell //+TODO: Is it possible to hit this, as we already check for that? DAN 15May'24
		}
	}

	return SubgridInfo{i_start, i_stop, j_start, j_stop};
}

template<class Coord>
[[nodiscard]] auto get_info_of_subgrid_contained_in_rect(const SquareSpaceGrid<Coord>& grid, 
                                                         const Rectangle<Coord>& rect) -> std::optional<SubgridInfo>
{
	auto intersect_rect = intersect(perimeter_rect(grid), rect);
	if (!intersect_rect) {
		return {};
	}
	if (rect.width() < grid.cell_size() || rect.height() < grid.cell_size()) {
		return {}; // The rectangle is not big enough to cover a whole cell
	}

	const auto cell_size_dbl = static_cast<double>(grid.cell_size());

	const auto j_start = ceil_to_int((intersect_rect->left() - grid.left()) / cell_size_dbl);
	const auto i_start = ceil_to_int((grid.top() - intersect_rect->top()) / cell_size_dbl);

	// Count the complete columns which appear to the left of the right edge of the rectangle
	const auto cols_before_rect_right = floor_to_int((intersect_rect->right() - grid.left()) / cell_size_dbl);
	const auto j_stop = cols_before_rect_right - 1;

	// Count the complete rows which appear above of the bottom edge of the rectangle
	const auto rows_above_rect_bottom = floor_to_int((grid.top() - intersect_rect->bottom()) / cell_size_dbl);
	const auto i_stop = rows_above_rect_bottom - 1;

	return SubgridInfo{i_start, i_stop, j_start, j_stop};
}

template<class Coord>
[[nodiscard]] auto get_info_of_subgrid_with_cell_centres_in_rect(const SquareSpaceGrid<Coord>& grid,
                                                                 const Rectangle<Coord>& rect) 
                    -> std::optional<SubgridInfo>
{
	auto intersect_rect = intersect(perimeter_rect(grid), rect);
	if (!intersect_rect) {
		return {};
	}

    //+TODO: Do we need a tolerance? DAN 21Oct'24

    const auto min_cell_centre = get_cell_centre_from_row_col(grid, grid.nof_rows() - 1, 0);

    auto calc_row = [min_cell_centre_y = min_cell_centre.y(), &grid](double y, bool is_far) {
        auto dy = y - min_cell_centre_y;
        if (dy <= 0) {
            return grid.nof_rows() - 1;
        }
        const auto rows = is_far ? floor_to_ssize(dy / grid.cell_size()) : ceil_to_ssize(dy / grid.cell_size());
        return grid.nof_rows() - 1 - rows;    
    };
    auto calc_col = [min_cell_centre_x = min_cell_centre.x(), &grid](double x, bool is_far) {
        auto dx = x - min_cell_centre_x;
        if (dx <= 0) {
            return Ssize{0};
        }
        return is_far ? floor_to_ssize(dx / grid.cell_size()) : ceil_to_ssize(dx / grid.cell_size()); 
    };

    return SubgridInfo{calc_row(intersect_rect->top(), true/*is_far*/),
                       calc_row(intersect_rect->bottom(), false/*is_far*/),
                       calc_col(intersect_rect->left(), false/*is_far*/),
                       calc_col(intersect_rect->right(), true/*is_far*/)};
}

template<class Coord>
[[nodiscard]] auto compare(const SpaceGrid<Coord>& grid1, const SpaceGrid<Coord>& grid2) -> bool
{
	return grid1.left() == grid2.left() && 
			grid1.bottom() == grid2.bottom() && 
			grid1.cell_width() == grid2.cell_width() &&
			grid1.cell_height() == grid2.cell_height() &&
			grid1.nof_rows() == grid2.nof_rows() && 
			grid1.nof_columns() == grid2.nof_columns();
}

template<class Coord>
[[nodiscard]] auto compare_tol(const SpaceGrid<Coord>& grid1, const SpaceGrid<Coord>& grid2, double tol) -> bool
{
	if (grid1.nof_rows() != grid2.nof_rows() || grid1.nof_columns() != grid2.nof_columns()) {
		return false;
	}	
	return equal_tol(grid1.cell_width(), grid2.cell_width(), tol) &&
	        equal_tol(grid1.cell_height(), grid2.cell_height(), tol) &&
			equal_tol(grid1.left(), grid2.left(), tol) &&
	        equal_tol(grid1.bottom(), grid2.bottom(), tol) &&
	        equal_tol(grid1.right(), grid2.right(), tol) &&
	        equal_tol(grid1.top(), grid2.top(), tol);
}

template<class Coord>
[[nodiscard]] auto is_subgrid_tol(const SquareSpaceGrid<Coord>& grid1, const SquareSpaceGrid<Coord>& grid2, double tol) 
      -> std::optional<SubgridInfo>
{
	const auto cells_size_dbl = static_cast<double>(grid1.cell_size());

	if (!equal_tol(grid2.cell_size(), cells_size_dbl, tol)) {
		return std::nullopt;
	}
	if (!contains(perimeter_rect(grid2), perimeter_rect(grid1))) {
		return std::nullopt;
	}

    auto calc_nof_cells_between = [cells_size_dbl, tol](auto x, auto y) {
        return divide_exactly(static_cast<double>(x - y), cells_size_dbl, tol);
    };

	const auto begin_row = calc_nof_cells_between(grid2.top(), grid1.top());
    if (!begin_row) {
		return std::nullopt;    
    }
	const auto end_row = calc_nof_cells_between(grid2.top(), grid1.bottom());
    if (!end_row) {
		return std::nullopt;    
    }
	const auto begin_col = calc_nof_cells_between(grid1.left(), grid2.left());
    if (!begin_col) {
		return std::nullopt;    
    }
	const auto end_col = calc_nof_cells_between(grid1.right(), grid2.left());
    if (!end_col) {
		return std::nullopt;    
    }

	return SubgridInfo{*begin_row, *end_row - 1, *begin_col, *end_col - 1};
}

template<class Coord>
[[nodiscard]] auto to_square_grid(const SpaceGrid<Coord>& grid) -> SquareSpaceGrid<Coord>
{
	const auto cell_size = grid.cell_width();
	if (grid.cell_height() != cell_size) {
		TWIST_THRO2(L"The grid has unsquare cells ({}x{}).", grid.cell_width(), grid.cell_height());
	}
	return SquareSpaceGrid<Coord>{grid.left(), grid.bottom(), cell_size, grid.nof_rows(), grid.nof_columns()};
}

template<class Coord, 
         InvocableR<void, GridCellIndex> Fn>
auto apply_to_all_cells(const SpaceGrid<Coord>& grid, Fn func) -> void
{
    for (auto cell_idx_val : IndexRange{Ssize{0}, as_underlying(max_cell_index(grid)) + 1}) {
		std::invoke(func, GridCellIndex{cell_idx_val});
	}
}

template<class Coord>
[[nodiscard]] auto are_aligned(const SpaceGrid<Coord>& grid1, const SpaceGrid<Coord>& grid2) -> bool
{
    if (grid1.cell_width() != grid2.cell_width() || grid1.cell_height() != grid2.cell_height()) {
        return false;
    }
    const auto dx = (grid1.left() - grid2.left()) / static_cast<double>(grid1.cell_width());
    const auto dy = (grid1.bottom() - grid2.bottom()) / static_cast<double>(grid1.cell_height());
    return dx == std::round(dx) && dy == std::round(dy);
}

template<class Coord>
[[nodiscard]] auto get_undercells_info(const SquareSpaceGrid<Coord>& overgrid,
                                       const SquareSpaceGrid<Coord>& undergrid) -> UndercellsInfo
{
    if constexpr (std::is_floating_point_v<Coord>) {
        static const auto tol = 1e-13; // Pretty arbitrary, yes
        if (!is_multiple_tol(overgrid.cell_size(), undergrid.cell_size(), tol)) {
            TWIST_THRO2(L"The cell size {} of the over-grid is not a multiple of the cell size {} of the under-grid.",
                        overgrid.cell_size(), undergrid.cell_size());
        }
    }
    else {
        if (!is_multiple(overgrid.cell_size(), undergrid.cell_size())) {
            TWIST_THRO2(L"The cell size {} of the over-grid is not a multiple of the cell size {} of the under-grid.",
                        overgrid.cell_size(), undergrid.cell_size());
        }
    }

    auto intersect_rect = intersect(perimeter_rect(overgrid), perimeter_rect(undergrid));
    if (!intersect_rect) {
        TWIST_THRO2(L"The intersection of the over-grid and under-grid is too small.");        
    }
    intersect_rect = inflate(*intersect_rect, -overgrid.cell_size());
    auto over_subgrid_info = get_info_of_subgrid_containing_rect(overgrid, *intersect_rect);
    auto under_subgrid_info = get_info_of_subgrid_containing_rect(undergrid, *intersect_rect);
    if (!over_subgrid_info || !under_subgrid_info) {
        TWIST_THRO2(L"The intersection of the over-grid and under-grid is too small.");        
    }
    // Take the top-left cell of the subgrid of the overgrid, and find the smallest subgrid of the undergrid which 
    // contains it
    const auto overcell_rect = get_cell_rect_from_row_col(overgrid,
                                                          over_subgrid_info->begin_row(),
                                                          over_subgrid_info->begin_column());
    under_subgrid_info = get_info_of_subgrid_containing_rect(undergrid, overcell_rect);
    assert(under_subgrid_info);
    const auto under_subgrid = get_subgrid(undergrid, *under_subgrid_info);
    if (!contains(perimeter_rect(under_subgrid), overcell_rect)) {
        TWIST_THRO2(L"The intersection of the over-grid and under-grid is too small.");       
    }

    // Find the "central" under-cell, which contains the centre of the over-cell
    const auto [row, col] = get_cell_row_col_from_pos(under_subgrid, overcell_rect.centroid());

    auto is_under_overcell = [&overcell_rect, &under_subgrid](auto row, auto col) {
        return contains(overcell_rect, get_cell_centre_from_row_col(under_subgrid, row, col));
    };
    auto cols_left = 0;
    while ((col - cols_left > 0) && is_under_overcell(row, col - cols_left - 1)) {
        ++cols_left;
    }
    auto cols_right = 0;
    while ((col + cols_right < under_subgrid.nof_columns() - 1) && is_under_overcell(row, col + cols_right + 1)) {
        ++cols_right;
    }
    auto rows_down = 0;
    while ((row + rows_down < under_subgrid.nof_rows() - 1) && is_under_overcell(row + rows_down + 1, col)) {
        ++rows_down;
    }
    auto rows_up = 0;
    while ((row - rows_up > 0) && is_under_overcell(row - rows_up - 1, col)) {
        ++rows_up;
    }

    return {cols_left, 
            cols_right, 
            rows_down, 
            rows_up,  
            (cols_left + 1 + cols_right) * (rows_down + 1 + rows_up)};
}

template<class Coord, std::invocable<Ssize, Ssize, const SubgridInfo&> Fn>
auto foreach_overcell(const SquareSpaceGrid<Coord>& overgrid, const SquareSpaceGrid<Coord>& undergrid, Fn func) -> void                                    
{
    const auto nof_in_cells_under_overcell = get_undercells_info(overgrid, undergrid).total_cells;
    assert(nof_in_cells_under_overcell > 0);

    for (auto i_over : IndexRange{overgrid.nof_rows()}) {
        for (auto j_over : IndexRange{overgrid.nof_columns()}) {
            const auto overcell_rect = get_cell_rect_from_row_col(overgrid, i_over, j_over);
            const auto under_subgrid_info = get_info_of_subgrid_with_cell_centres_in_rect(undergrid, overcell_rect);
            if (not under_subgrid_info or nof_cells(*under_subgrid_info) != nof_in_cells_under_overcell) {
                TWIST_THRO2(L"Not enough cells fall under overgrid cell ({}, {}).", i_over, j_over);
            }
            std::invoke(func, i_over, j_over, *under_subgrid_info);
        }
    }
}

template<class Coord, 
         class UndercellVal, 
         std::invocable<Ssize, Ssize, const SubgridInfo&, const std::map<UndercellVal, Ssize>&> Fn>
auto foreach_overcell(const SquareSpaceGrid<Coord>& overgrid, 
                      const SquareSpaceGrid<Coord>& undergrid, 
                      const twist::math::FlatMatrix<UndercellVal>& undergrid_data,
                      Fn func) -> void
{
    if (dims(undergrid_data) != dims(undergrid)) {
        TWIST_THRO2(L"The under-grid data matrix dimensions ({}x{}) do not match the under-grid dimensions ({}x{}).",
                    undergrid_data.nof_rows(), undergrid_data.nof_columns(),
                    undergrid.nof_rows(), undergrid.nof_columns());
    }

    const auto nof_in_cells_under_overcell = get_undercells_info(overgrid, undergrid).total_cells;
    assert(nof_in_cells_under_overcell > 0);

    for (auto i_over : IndexRange{overgrid.nof_rows()}) {
        for (auto j_over : IndexRange{overgrid.nof_columns()}) {
            const auto overcell_rect = get_cell_rect_from_row_col(overgrid, i_over, j_over);
            const auto under_subgrid_info = get_info_of_subgrid_with_cell_centres_in_rect(undergrid, overcell_rect);
            if (not under_subgrid_info or nof_cells(*under_subgrid_info) != nof_in_cells_under_overcell) {
                TWIST_THRO2(L"Not enough cells fall under overgrid cell ({}, {}).", i_over, j_over);
            }
            auto undercell_value_frequencies = std::map<UndercellVal, Ssize>{};
            for (auto i_under : row_range(*under_subgrid_info)) {
                for (auto j_under : column_range(*under_subgrid_info)) {
                    undercell_value_frequencies[undergrid_data(i_under, j_under)]++;
                }
            }
            std::invoke(func, i_over, j_over, *under_subgrid_info, undercell_value_frequencies);
        }
    }

}

} 

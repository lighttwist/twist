/// @file matrix_utils.ipp
/// Inline implementation file for "matrix_utils.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace twist::math {

template<class Mat, rg::input_range Rng>
auto equal_to_range(const Mat& matrix, const Rng& range) -> bool
{
	using std::begin;
	using std::end;

	if (rg::ssize(range) != matrix.nof_cells()) {
		TWIST_THROW(L"Range has wrong size %lld, expected %lld.", rg::ssize(range), matrix.nof_cells());
	}
	const auto nof_cols = matrix.nof_columns();
	for (auto i : IndexRange{matrix.nof_rows()}) {
		const auto& row = matrix.get_row(i);
		if (!std::equal(begin(row), end(row), begin(range) + nof_cols * i, begin(range) + nof_cols * (i + 1))) {
			return false;
		}
	}
	return true;
}

template<class Mat, class Pred>
requires std::is_invocable_r_v<bool, Pred, std::vector<typename Mat::Value>>
auto remove_rows_if(Mat& matrix, Pred pred) -> Ssize
{
	using std::rend;

	auto row_indexes = std::vector<Ssize>{};
	for (auto i : IndexRange{matrix.nof_rows()}) {
		if (std::invoke(pred, matrix.get_row(i))) {
			row_indexes.push_back(i);
		}
	}
	for (auto it = rbegin(row_indexes); it != rend(row_indexes); ++it) {
		matrix.remove_row(*it);
	}
	return ssize(row_indexes);
}

} 

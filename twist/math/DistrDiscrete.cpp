/// @file DistrDiscrete.hpp
/// Implementation file for "DistrDiscrete.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/math/DistrDiscrete.hpp"

#include "twist/math/DistributionVisitor.hpp"
#include "twist/math/numeric_utils.hpp"

namespace twist::math {

// --- DistrDiscrete::StateList class ---

DistrDiscrete::StateList::StateList() 
	: state_prob_map_{BPredLessTolerance<double>(float_epsilon)}
{
	TWIST_CHECK_INVARIANT
}

auto DistrDiscrete::StateList::insert_state(double value, double prob) -> void
{
	TWIST_CHECK_INVARIANT
	if ((prob <= 0) || (prob > 1)) {
		TWIST_THROW(L"The probability of a discrete distribution state must be in the interval (0, 1]");
	}
	
	const auto ret = state_prob_map_.insert(DblToDblTolMap::value_type(value, prob));
	if (!ret.second) {
		TWIST_THROW(L"A discrete distribution state with this value already exists.");
	}
}

auto DistrDiscrete::StateList::get_state_at(Ssize state_pos) const -> std::pair<double, double>
{
	TWIST_CHECK_INVARIANT
	if (state_pos >= ssize(state_prob_map_)) {
		TWIST_THROW(L"State position %d out of bounds.", state_pos);
	}
	auto it = begin(state_prob_map_);
	advance(it, state_pos);
	return std::make_pair(it->first, it->second);
}

auto DistrDiscrete::StateList::get_size() const -> Ssize
{
	TWIST_CHECK_INVARIANT
	return ssize(state_prob_map_);
}

auto DistrDiscrete::StateList::get_prob_sum() const -> double
{
	TWIST_CHECK_INVARIANT
	double sum_prob = 0;	
	for (const auto& el : state_prob_map_) {
		sum_prob += el.second;
	}
	return sum_prob;
}

auto DistrDiscrete::StateList::adjust_last_state_prob() -> double 
{
	TWIST_CHECK_INVARIANT
	// Adjust the last state probability, if necessary, so that the probabilities add up to 1 (within a reasonable 
	// tolerance)
	double prob_sum = 0;	
	const Ssize num_states = ssize(state_prob_map_);
	auto it = begin(state_prob_map_);
	for (auto i = 0; i < num_states; ++i) {
		prob_sum += it->second;
		if (i < num_states - 1) {
			++it;
		}
	}
	
	// The iterator should now be pointing at the last element in the map; check and, if necessary, adjust the 
	// probability of the last state
	if (prob_sum != 1 && abs(prob_sum - 1) >= double_epsilon) {
		const double sum_prob_diff = 1 - prob_sum; 
		it->second += sum_prob_diff;

		assert(sum_prob_diff <= float_epsilon); // The way this methods should be used, it should be a pretty small error
		return sum_prob_diff;
	}

	return 0;
}

auto DistrDiscrete::StateList::delete_state(Ssize state_pos) -> void
{
	TWIST_CHECK_INVARIANT
	if (state_pos >= ssize(state_prob_map_)) {
		TWIST_THROW(L"State position %d out of bounds.", state_pos);
	}
	auto it = begin(state_prob_map_);
	advance(it, state_pos);
	state_prob_map_.erase(it);
}

auto DistrDiscrete::StateList::states() const -> const DistrDiscrete::DblToDblTolMap&
{
	TWIST_CHECK_INVARIANT
	return state_prob_map_;
}

auto DistrDiscrete::StateList::get_state_pos(double state_val) const -> Ssize
{
	TWIST_CHECK_INVARIANT	
	if (const auto it = state_prob_map_.find(state_val); it != end(state_prob_map_)) {
		return distance(begin(state_prob_map_), it);
	}
	return k_no_pos;
}

auto DistrDiscrete::StateList::clear() -> void 
{
	TWIST_CHECK_INVARIANT
	state_prob_map_.clear();
}

#ifdef _DEBUG
void DistrDiscrete::StateList::check_invariant() const noexcept
{
	//+TODO: Check uniqueness of state values?  Dan 16Jun'22
}
#endif 

// --- DistrDiscrete class ---

DistrDiscrete* DistrDiscrete::makeUniformDistr(const StateValList& stateValues)
{
	if (stateValues.size() == 0) {
		TWIST_THROW(L"The distribution should have at least one state.");
	}
	const double prob = 1.0 / stateValues.size();
	StateList states;
	for (auto it = stateValues.begin(); it != stateValues.end(); ++it) {
		states.insert_state(*it, prob);
	}
	
	return new DistrDiscrete(std::move(states));
}

// Using an arbitrary tolerance here, as depending on the precision of the input the states will often not really add 
// up to one (if that even makes sense on a computer). Creating a discrete from R for example will sometimes fail if 
// using the "double epsilon" tolerance, so we go with the "float epsilon" tolerance in this constructor.   
DistrDiscrete::DistrDiscrete(StateList states)
	: state_prob_map_{move(states.states())}
	, cum_prob_state_map_{}
	, state_cum_prob_map_{make_map_double_tol<double, double>(float_epsilon)}
	, highest_state_{0}
{
	if (ssize(state_prob_map_) == 0) {
		TWIST_THROW(L"There should be at least one state in the distribution.");
	}
	
	// Check that the state probabilities add up to one (within a reasonable tolerance)
	double prob_sum = 0;
	for (const auto& el : state_prob_map_) {
		prob_sum += el.second;
	}
	if (prob_sum != 1 && abs(prob_sum - 1) > float_epsilon) {
		TWIST_THROW(L"The state probabilities do not add up to one, but rather to %.9f.", prob_sum);
	} 
	
	// Fill in the map of cumulative probabilities probabilities / state value pairs, and the map of 
	// state value / cumulative probabilities pairs.
	auto it = begin(state_prob_map_);
	cum_prob_state_map_.emplace(it->second, it->first);
	state_cum_prob_map_.emplace(it->first, it->second);
	highest_state_ = it->first;
	auto iterCumSum = cum_prob_state_map_.begin();
	++it;
	for (; it != state_prob_map_.end(); it++) {
		cum_prob_state_map_.emplace(iterCumSum->first + it->second, it->first);
		state_cum_prob_map_.emplace(it->first, iterCumSum->first + it->second);
		if (it->first > highest_state_) {
			highest_state_ = it->first;
		}
		iterCumSum++;
	}

	TWIST_CHECK_INVARIANT
}

DistrDiscrete::DistrDiscrete(const DistrDiscrete& src)
	: state_prob_map_{src.state_prob_map_}
	, cum_prob_state_map_{src.cum_prob_state_map_}
	, state_cum_prob_map_{src.state_cum_prob_map_}
	, highest_state_{src.highest_state_}
{
	TWIST_CHECK_INVARIANT
}

DistrDiscrete::~DistrDiscrete() = default;

auto DistrDiscrete::getClone() const -> DistrDiscrete*
{
	TWIST_CHECK_INVARIANT
	return new DistrDiscrete{*this};
}

bool DistrDiscrete::compare(const DistrDiscrete& other) const
{
	TWIST_CHECK_INVARIANT
	bool retVal = false;
	
	if (ssize(state_prob_map_) == ssize(other.state_prob_map_)) {
		// Compare each state.
		retVal = true;
		for (auto it = begin(state_prob_map_), otherIt = begin(other.state_prob_map_);
				it != state_prob_map_.end(); ++it, ++otherIt) {			
			// If either the state values or the state probabilities differ, the distributions are different.
			if (!compareStateVals(it->first, otherIt->first) || !compareStateProbs(it->second, otherIt->second)) {
				retVal = false;
				break;
			}
		}		
	}
	return retVal;
}

auto DistrDiscrete::getType() const -> Distribution::Type
{
	TWIST_CHECK_INVARIANT
	return kDiscrete;
}

auto DistrDiscrete::getTypeName() const -> std::wstring 
{
	TWIST_CHECK_INVARIANT
	return L"discrete";
}

auto DistrDiscrete::countParams() const -> Ssize
{
	TWIST_CHECK_INVARIANT
	return 2 * ssize(state_prob_map_);
}

void DistrDiscrete::acceptVisit(DistributionVisitor& visitor) const
{
	TWIST_CHECK_INVARIANT
	visitor.visit(*this);
}

double DistrDiscrete::cdf(double x) const
{
	double p = 0;
	auto findIt = state_cum_prob_map_.find(x);
	if (findIt == state_cum_prob_map_.end()) {
		TWIST_THROW(L"The value passed in must be a discrete state value.");	
	}
	
	p = findIt->second;	
	if (p == 1) {
		// If the biggest state value was passed in, the probability will be one.
		// Keep the return value in (0,1)
		p = 1.0f - float_epsilon;
	}
	return p;
}

double DistrDiscrete::virtual_cdf(double x) const
{
	double p = 0;
	auto findIt = state_cum_prob_map_.find(x);
	if (findIt == state_cum_prob_map_.end()) {
		TWIST_THROW(L"The value passed in must be a discrete state value.");	
	}
	
	p = findIt->second;	
	if (p == 1) {
		// If the biggest state value was passed in, the probability will be one.
		// Keep the return value in (0,1)
		p = 1.0f - float_epsilon;
	}
	return p;
}

DistrParametric::ParamNamesValues DistrDiscrete::get_param_names_values() const 
{
	TWIST_CHECK_INVARIANT
	ParamNamesValues ret;
	Ssize i = 1;
	for (const auto& el : state_prob_map_) {
		ret.push_back(std::pair{format_str(L"state %d realisation", i), el.first});
		ret.push_back(std::pair{format_str(L"state %d probability", i), el.second});		
		++i;
	}
	return ret;
}

double DistrDiscrete::invCdf(double p) const
{
	TWIST_CHECK_INVARIANT
	if ((p <= 0) || (p >= 1)) {
		TWIST_THROW(L"Percentile 'p' must be in the interval (0,1)");
	}
	
	double retVal = highest_state_; // This is the probability of the biggest state value
	
	auto findIt = cum_prob_state_map_.upper_bound(p);
	if (findIt != cum_prob_state_map_.end()) {
		retVal = findIt->second;
	}
	// If 'upper_bound' fails to find an elemet with a key bigger than 'p', it means we should return the
	// biggest state value. Which we are doing.

	return retVal;
}

double DistrDiscrete::virtual_invCdf(double p) const
{
	TWIST_CHECK_INVARIANT
	return invCdf(p);
}

auto DistrDiscrete::nof_states() const -> Ssize
{
	TWIST_CHECK_INVARIANT
	return ssize(state_prob_map_);
}

auto DistrDiscrete::get_state_prob_for_value(double state_value) const -> double
{
	TWIST_CHECK_INVARIANT
	if (const auto it = state_prob_map_.find(state_value); it != end(state_prob_map_)) {
		return it->second;
	}
	TWIST_THROW(L"State with value %f not found in the discrete distribution.", state_value);
}

auto DistrDiscrete::get_state_value_at(Ssize state_idx) const -> double 
{
	TWIST_CHECK_INVARIANT
	if (state_idx >= ssize(state_prob_map_)) {
		TWIST_THROW(L"Discrete distribution state index %d out of bounds.", state_idx);
	}
	auto it = begin(state_prob_map_);
	advance(it, state_idx);
	return it->first;
}

auto DistrDiscrete::get_state_prob_at(Ssize state_idx) const -> double
{
	TWIST_CHECK_INVARIANT
	if (state_idx >= ssize(state_prob_map_)) {
		TWIST_THROW(L"Discrete distribution state index %d out of bounds.", state_idx);
	}
	auto it = begin(state_prob_map_);
	advance(it, state_idx);
	return it->second;
}

void DistrDiscrete::get_states(StateList& states) const
{
	TWIST_CHECK_INVARIANT
	states.state_prob_map_ = state_prob_map_;
}

double DistrDiscrete::get_cumul_state_prob(Ssize state_idx) const
{
	TWIST_CHECK_INVARIANT
	if (state_idx >= ssize(state_prob_map_)) {
		TWIST_THROW(L"Discrete distribution state index %d out of bounds.", state_idx);
	}
	auto it = begin(state_cum_prob_map_);
	advance(it, state_idx);
	return it->second;
}

auto DistrDiscrete::is_state(double state_value) const -> bool
{
	TWIST_CHECK_INVARIANT
	return get_state_idx(state_value) != k_no_idx;
}

auto DistrDiscrete::get_state_idx(double state_value) const -> Ssize 
{
	TWIST_CHECK_INVARIANT	
	if (const auto it = state_prob_map_.find(state_value); it != end(state_prob_map_)) {
		return distance(begin(state_prob_map_), it);
	}
	return k_no_idx;	
}

void DistrDiscrete::getStateValues(double loStateVal, double hiStateVal, StateValList& stateValues) const
{
	TWIST_CHECK_INVARIANT
	stateValues.clear();
	for (auto it = begin(state_prob_map_); it != state_prob_map_.end(); ++it) {
		if ((it->first >= loStateVal) && (it->first <= hiStateVal)) {
			stateValues.push_back(it->first);
		}
	}
}

bool DistrDiscrete::compareStateVals(double stateVal1, double stateVal2) const
{
	TWIST_CHECK_INVARIANT
	// Use the same tolerance used by the state maps for comparing their keys.
	auto comparePred = state_prob_map_.key_comp();
	const bool equalVals = (comparePred(stateVal1, stateVal2) == comparePred(stateVal2, stateVal1));
	return equalVals;
}

bool DistrDiscrete::compareStateProbs(double stateProb1, double stateProb2) const
{
	TWIST_CHECK_INVARIANT
	// Use the same tolerance used by the state maps for comparing their keys.
	auto comparePred = state_prob_map_.key_comp();
	const bool equalProbs = (comparePred(stateProb1, stateProb2) == comparePred(stateProb2, stateProb1));
	return equalProbs;
}

#ifdef _DEBUG
void DistrDiscrete::check_invariant() const noexcept
{
	assert(ssize(state_prob_map_) > 0);
}
#endif 

} 


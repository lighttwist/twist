/// @file DistrDiscrete.hpp
/// DistrDiscrete class, inherits DistrParametric

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_MATH_DISTR_DISCRETE_HPP
#define TWIST_MATH_DISTR_DISCRETE_HPP

#include "twist/math/DistrParametric.hpp"
#include "twist/math/maps.hpp"

namespace twist::math {

/*! This class represents a discrete distribution. The distribution can have one or more states. A state constists of a
    value (or realisation) - a real number - and a probability - a real number in the interval (0, 1]. All realizations 
	take distinct values and their associated probabilities add up to 1.
 */
class DistrDiscrete : public DistrParametric {
public:
	using DblToDblTolMap = MapDoubleTol<double, double>::type;	
	using StateValList = std::vector<double>;

	/*! A list of state value / state probability pairs. The list is always ordered in increasing order of the state 
	    values. It cannot contain two states with the same value, or probabilities outside the (0, 1] interval, but it 
		performs no validity checks on how the probabilities add up.
	 */
	class StateList {
	public:
		explicit StateList();

		/*! Add a state to the list.
		    \param[in] value  The state value.
		    \param[in] prob  The state probability. Must be in the interval (0, 1].
		 */
		auto insert_state(double value, double prob) -> void;

		/*! Delete a specific state from the list.
		    \param[in] state_pos  The state position in the list; an exception is thrown if the position is invalid
		 */
		auto delete_state(Ssize state_pos) -> void;
		
		/*! Get read-only access to the internal list of state value / state probability pairs.
		    \return  Ref to the list.
		 */
		[[nodiscard]] auto states() const -> const DblToDblTolMap&;	

		/*! Look in the list for a state with a specific value and find out its position.
		    \param[in] state_val  The state value
		    \return  The state's position withing the list, or twist::k_no_pos if no match is found
		 */
		[[nodiscard]] auto get_state_pos(double state_val) const -> Ssize;

		/*! Get information about a specific state from the list.
		    \param[in] state_pos  The state position in the list; an exception is thrown if the position is invalid
		    \return  0. The state value
			         1. The state probability
		 */
		[[nodiscard]] auto get_state_at(Ssize state_pos) const -> std::pair<double, double>;

		/*! Get the number of states in the list.
		    \return  The list size
		 */
		[[nodiscard]] auto get_size() const -> Ssize;

		/*! Get the sum of all the state probabilities in the list.
		    \return  The sum of probabilities
		 */
		[[nodiscard]] auto get_prob_sum() const -> double;

		/*! If the state probabilities in the list add up to less or more than one (within a reasonable tolerance), 
		    then this method will modify the last state probability so that the states do add up to one. 
		    \return  The amount by which the last state probability was changed
		 */
		auto adjust_last_state_prob() -> double;
		
		//! Clear all states in the list.
		auto clear() -> void;	

	private:
		// The internal map of state value / state probability pairs, using the same tolerance as the distribution 
		// class
		DblToDblTolMap state_prob_map_;

		friend class DistrDiscrete;
	
		TWIST_CHECK_INVARIANT_DECL
	};

	/// Create a discrete distribution whose states have all the same probability.
	///
	/// @param  stateValues  [in] List of the state values.
	/// @return  The discrete distribution. Caller takes ownership.
	///
	static DistrDiscrete* makeUniformDistr(const StateValList& stateValues);

	/*! Constructor.
	    \param[in] states  List of state value / state probability pairs; if the probabilities for all states do not 
		                   add up to one (within a reasonable tolerance) an exception is thrown 
	 */
	DistrDiscrete(StateList states);

	DistrDiscrete(DistrDiscrete&&) = default;  

	~DistrDiscrete() override;

	DistrDiscrete& operator=(const DistrDiscrete&) = delete;  

	DistrDiscrete& operator=(DistrDiscrete&&) = default;
	
	[[nodiscard]] auto getClone() const -> DistrDiscrete* override; // Distribution override

	[[nodiscard]] auto getType() const -> Type override; // Distribution override
	
	[[nodiscard]] auto getTypeName() const -> std::wstring override; // Distribution override

	[[nodiscard]] auto countParams() const -> Ssize override; // DistrParametric override

	ParamNamesValues get_param_names_values() const override; // DistrParametric override
	
	void acceptVisit(DistributionVisitor& visitor) const override; // Distribution override

	/// Compare this distribution with another discrete distribution.
	///
	/// @param  other  [in] The other distribution.
	/// @return  true only if the two distributions are identical.
	///
	bool compare(const DistrDiscrete& other) const;
	
	/// Apply the CDF.
	///
	/// @param  x  [in] Argument of the function (a discrete state value).
	/// @return  Value of the CDF.
	///
	double cdf(double x) const;
	virtual double virtual_cdf(double x) const; //+OBSOLETE

	/*! Apply the inverse CDF.
	    \param p  [in] Percentile in interval (0, 1)
	    \return  The corresponding realization of the random variable
	 */
	[[nodiscard]] auto invCdf(double p) const -> double;
	virtual double virtual_invCdf(double p) const; //+OBSOLETE
	
	/// Get the number of states of the distribution.
	///
	/// @return  The state count.
	///
	[[nodiscard]] auto nof_states() const -> Ssize;

	/*! Get the probability of a specific state in the distribution.
        \param[in] state_value  The value of the state; if no state with this value exists, an exception is thrown
	    \return  The probability of the state
	 */
	[[nodiscard]] auto get_state_prob_for_value(double state_value) const -> double;

	/*! Get the value of a specific state in the distribution.
        \param[in] state_idx  The index of the state
	    \return  The value of the state
	 */
	[[nodiscard]] auto get_state_value_at(Ssize state_idx) const -> double;
	
	/*! Get the probability of a specific state in the distribution.
	    \param[in] state_idx  The index of the state
	    \return  The probability of the state
	 */
	[[nodiscard]] auto get_state_prob_at(Ssize state_idx) const -> double;

	/// Get a list of all states of the distribution (the state values and corresponding probabilities).
	///
	/// @param  states  [out] The states
	///
	void get_states(StateList& states) const;
	
	/// Get the cumulative probability of a specific state in the distribution.
	///
	/// @param  state_idx  [in] The index of the state.
	/// @return  The probability of the state.
	///
	double get_cumul_state_prob(Ssize state_idx) const;
	
	/*! Check whether the given value is one of the states of the distribution.
	    \param[in] state_value  Presumable state
	    \return  true only if the value is one of the states
	 */
	[[nodiscard]] auto is_state(double state_value) const -> bool;

	/*! Find out what the index of a specific state is within the list of states.
	    \param[in] state_value  The state value
	    \return  The state index, or twist::k_no_idx if the value passed in does not match any of the states
	 */
	[[nodiscard]] auto get_state_idx(double state_value) const -> Ssize;
	
	/// Get the state values between two specific state values.
	/// 
	/// @param  loStateVal  [in] The lower state value. The matching states will have a bigger or equal state value.
	/// @param  hiStateVal  [in] The upper state value. The matching states will have a smaller or equal state value.
	/// @param  stateValues  [out] The matching state values.
	///
	void getStateValues(double loStateVal, double hiStateVal, StateValList& stateValues) const;

private:	
	using DblToDblMap = std::map<double, double>;
		
	DistrDiscrete(const DistrDiscrete& src);  
	
	/// Compare two state values (making use of the custom tolerance defined for the state maps).
	///
	/// @return  true only if the two values are equal
	///
	bool compareStateVals(double stateVal1, double stateVal2) const;

	/// Compare two state probabilities (making use of the custom tolerance defined for the state maps).
	///
	/// @return  true only if the two values are equal
	///
	bool compareStateProbs(double stateProb1, double stateProb2) const;

	DblToDblTolMap state_prob_map_; // The internal list of state value / state probability pairs
	DblToDblMap cum_prob_state_map_; // Map of cumulative probabilities / state value pairs
	DblToDblTolMap state_cum_prob_map_; // Map of state value / cumulative probabilities pairs
	double highest_state_; // The highest (biggest) state value

	TWIST_CHECK_INVARIANT_DECL
};

} 

#endif 

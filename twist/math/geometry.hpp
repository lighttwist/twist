/// @file geometry.hpp
/// Geometry utilities

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_MATH_GEOMETRY_HPP
#define TWIST_MATH_GEOMETRY_HPP

#include "twist/math/FlatMatrix.hpp" 
#include "twist/math/numeric_utils.hpp"

namespace twist::math {

/*! A point in 2D space, using the mathematical coordinate system: lower is left and down.
    \tparam Coordinate  The coordinate type (numeric)
 */
template<class Coordinate>
class Point {
public:
	using Coord = Coordinate;

	Point();

	Point(Coord x, Coord y);

	[[nodiscard]] auto x() const -> Coord;

	[[nodiscard]] auto y() const -> Coord;

	[[nodiscard]] auto move(Coord dx, Coord dy) const -> Point;

private:
	Coord x_;
	Coord y_;
};

template<class Coord> 
bool operator==(const Point<Coord>& lhs, const Point<Coord>& rhs);

/// 
///  A vector in 2D space, using the mathematical coordinate system: lower is left and down.
/// 
///  @tparam  Coord  The coordinate type (numeric)
/// 
template<class Coord>
class Vector {
public:
	using Coordinate = Coord;

	Vector();	

	Vector(double x, double y);

	Vector(const Vector& orig);

	void operator=(const Vector& rhs);

	void operator+=(const Vector& rhs);

	Vector operator-() const;
	
	Vector operator+(const Vector& rhs) const;

	void operator*=(double rhs);
	
	Vector operator*(double rhs) const;
	
	Coord x() const;

	void set_x(double x);

	Coord y() const; 

	void set_y(double y);
	
	void set(double x, double y);

	double length() const; 

private:
	Coord  x_;
	Coord  y_;
};

//! A rectangle, using the mathematical coordinate system: lower is left and down.
template<class Coord>
class Rectangle {
public:
	using Coordinate = Coord;

	/*! Constructor.
	    \param[in] x1  The X coordinate of one of the vertical sides 
	    \param[in] y1  The Y coordinate of one of the horizontal sides 
	    \param[in] x2  The X coordinate of the other vertical side
	    \param[in] y2  The Y coordinate of the other horizontal side
	 */
	Rectangle(Coord x1, Coord y1, Coord x2, Coord y2);

	/*! Constructor.
	    \param[in] pt1  One of the corners
	    \param[in] pt2  The opposite corner 
	 */ 
	Rectangle(const Point<Coord>& pt1, const Point<Coord>& pt2);

	auto operator==(const Rectangle& rhs) const -> bool;

	auto operator!=(const Rectangle& rhs) const -> bool;

	[[nodiscard]] auto left() const -> Coord;

	[[nodiscard]] auto right() const -> Coord;

	[[nodiscard]] auto bottom() const -> Coord;

	[[nodiscard]] auto top() const -> Coord;

	[[nodiscard]] auto width() const -> Coord;

	[[nodiscard]] auto height() const -> Coord;

	[[nodiscard]] auto bottom_left() const -> Point<Coord>;

	[[nodiscard]] auto bottom_right() const -> Point<Coord>;

	[[nodiscard]] auto top_left() const -> Point<Coord>; 

	[[nodiscard]] auto top_right() const -> Point<Coord>; 

	[[nodiscard]] auto centroid() const -> Point<Coord>; 

private:
	Coord left_;
	Coord bottom_;
	Coord right_;
	Coord top_;

	TWIST_CHECK_INVARIANT_DECL
};

//! A rectangle's quadrants.
enum class RectQuadrant { top_right, top_left, bottom_left, bottom_right };

//! Euclidian distance between 2d points \p pt1 and \p pt2.
template<class Coord>
[[nodiscard]] auto distance(Point<Coord> pt1, Point<Coord> pt2) -> double;

/*! Compute the area of a rectangle.
    \tparam Coord  The coordinate type 
    \param[in] rect  The rectangle
    \return  The area
 */ 
template<class Coord>
[[nodiscard]] auto area(const Rectangle<Coord>& rect) -> Coord;

/*! Move all sides of a rectangle outwards or inwards by the same distance.
    \tparam Coord  The coordinate type 
    \param[in] rect  The rectangle
    \param[in] d  The distace by which the sides should move; if negative, the sides move inwards (the rectangle is 
	              deflated) and the distance must be smaller than the smallest distance between a side and the centre
				  of the rectangle
    \return  The new, inflated (or deflated) rectangle
 */
template<class Coord>
[[nodiscard]] auto inflate(const Rectangle<Coord>& rect, Coord d) -> Rectangle<Coord>;

/// Find the rectangle which is the intersection of two given rectangles.
///
/// @tparam  Coord  The coordinate type 
/// @param[in] rect1  The first given rectangle
/// @param[in] rect2  The second given rectangle
/// @return  The intersection rectangle, if the two rectangles intersect; nullopt if they do not
///
template<class Coord>
std::optional<Rectangle<Coord>> intersect(const Rectangle<Coord>& rect1, const Rectangle<Coord>& rect2);

/*! Find out whether a rectangle contains a point.
    The second point is considered included if it falls on one of the rectangle's sides. 
    \tparam Coord  The coordinate type 
    \param[in] rect  The rectangle
    \param[in] pt  The point
    \return  true if the rectangle contains the point
 */
template<class Coord> 
[[nodiscard]] auto contains(const Rectangle<Coord>& rect, const Point<Coord>& pt) -> bool;

/// Given a rectangle find out whether it contains a point, and if so calculate which quadrant the point 
/// falls in.
///
/// @tparam  Coord  The coordinate type 
/// @param[in] rect  The rectangle
/// @param[in] pt  The point
/// @return  The quadrant; or null if the point does not fall inside the rectangle
///
template<class Coord>
std::optional<RectQuadrant> contains_in_quadrant(const Rectangle<Coord>& rect, const Point<Coord>& pt);

/// Find out whether a rectangle contains another.
/// The second rectangle is considered included if it shares one or more sides with the first, as long as it is 
/// otherwise inside it.
///
/// @tparam  Coord  The coordinate type 
/// @param[in] rect1  The first (outer) rectangle
/// @param[in] rect2  The second (inner) rectangle
/// @return  true if the first rectangle contains the second
///
template<class Coord>
bool contains(const Rectangle<Coord>& rect1, const Rectangle<Coord>& rect2);

/// Find out whether a rectangle contains another; the coordinate comparisons are done using a tolerance.
/// The second rectangle is considered included if it shares one or more sides with the first, as long as it 
/// is otherwise inside it.
///
/// @tparam  Coord  The coordinate type 
/// @param[in] rect1  The first (outer) rectangle
/// @param[in] rect2  The second (inner) rectangle
/// @param[in] tol  The tolerance
/// @return  true if the first rectangle contains the second
///
template<class Coord>
bool contains_tol(const Rectangle<Coord>& rect1, const Rectangle<Coord>& rect2, Coord tol);

/*! Given the matrix of 2-D points \p grid, find out if the points form a true (regular) grid in 2-D space, ie whether
    the horizontal distance between two horizontally adjacent points is always the same (within the tolerance \p tol) 
	and the vertical distance between two vertically adjacent points is always the same (within the same tolerance).
    \tparam Point2D  A 2-D point type similar to twist::math::Point
    \return  If the points form a regular grid [0. true
	                                            1. the signed horizontal distance from a point to the point to its left
											    2. the signed vertical distance from a point to the point above];
             otherwise [0. false; 1. the default value of the point coordinate type; 2. same as 1].
*/
template<class Point2D>
[[nodiscard]] auto is_regular_point_grid_tol(const FlatMatrix<Point2D>& grid, 
                                             typename Point2D::Coord tol) 
                    -> std::tuple<bool, typename Point2D::Coord, typename Point2D::Coord>;

} 

#include "twist/math/geometry.ipp"

#endif 

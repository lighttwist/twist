///  @file  textualise.ipp
///  Inline implementation file for "textualise.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include <iomanip>

namespace twist::math {

template<typename T>
std::wstring to_string(const Matrix<T>& matrix, const std::wstring& col_sep) 
{
	std::wstringstream text;
	text << std::fixed;

	for (size_t i = 0; i < matrix.nof_rows(); i++) {
		for (size_t j = 0; j < matrix.nof_columns(); j++) {
			text << matrix.elem(i, j); 
			if (j < matrix.nof_columns() - 1) {
				text << col_sep;
			}
		}
		if (i < matrix.nof_rows() - 1) {
			text << L"\n";
		}
	}
	return text.str();
}


template<typename T>
std::wstring to_string(const Matrix<T>& matrix, const std::wstring& col_sep, size_t precision) 
{
	std::wstringstream text;
	text << std::setprecision(static_cast<std::streamsize>(precision));	
	
	for (size_t i = 0; i < matrix.nof_rows(); i++) {
		for (size_t j = 0; j < matrix.nof_columns(); j++) {
			text << matrix.elem(i, j); 
			if (j < matrix.nof_columns() - 1) {
				text << col_sep;
			}
		}
		if (i < matrix.nof_rows() - 1) {
			text << L"\n";
		}
	}
	return text.str();
}


template<typename T>
std::wstring to_string(const SymmetricMatrix<T>& matrix, const std::wstring& col_sep) 
{
	std::wstringstream text;
	text << std::fixed;

	for (auto i = 0; i < matrix.size(); ++i) {
		for (auto j = 0; j < matrix.size(); ++j) {
			text << matrix.elem(i, j); 
			if (j < matrix.size() - 1) {
				text << col_sep;
			}
		}
		if (i < matrix.size() - 1) {
			text << L"\n";
		}
	}
	return text.str();
}


template<typename T>
std::wstring to_string(const SymmetricMatrix<T>& matrix, const std::wstring& col_sep, 
		size_t precision) 
{
	std::wstringstream text;
	text << std::setprecision(static_cast<std::streamsize>(precision));	

	for (auto i = 0; i < matrix.size(); ++i) {
		for (auto j = 0; j < matrix.size(); ++j) {
			text << matrix.elem(i, j); 
			if (j < matrix.size() - 1) {
				text << col_sep;
			}
		}
		if (i < matrix.size() - 1) {
			text << L"\n";
		}
	}
	return text.str();
}


template<typename T>
std::wstring to_string(const LowerTriangularMatrix<T>& matrix, const std::wstring& col_sep) 
{
	std::wstringstream text;
	text << std::fixed;

	for (auto i = 0; i < matrix.size(); ++i) {
		for (auto j = 0; j < matrix.size(); ++j) {
			text << matrix.elem(i, j); 
			if (j < matrix.size() - 1) {
				text << col_sep;
			}
		}
		if (i < matrix.size() - 1) {
			text << L"\n";
		}
	}
	return text.str();
}


template<typename T>
std::wstring to_string(const LowerTriangularMatrix<T>& matrix, const std::wstring& col_sep, size_t precision) 
{
	std::wstringstream text;
	text << std::setprecision(static_cast<std::streamsize>(precision));	

	for (auto i = 0; i < matrix.size(); ++i) {
		for (auto j = 0; j < matrix.size(); ++j) {
			text << matrix.elem(i, j); 
			if (j < matrix.size() - 1) {
				text << col_sep;
			}
		}
		if (i < matrix.size() - 1) {
			text << L"\n";
		}
	}
	return text.str();
}
	  
	      
template<typename Coord> 
std::wstring to_string(const Rectangle<Coord>& rect)
{
	static const int k_precision = 2;

	std::wstringstream str;
	str << std::fixed << std::setprecision(k_precision);
	str << L"(" << "l:" << rect.left()   << " "
	            << "b:" << rect.bottom() << " "
				<< "r:" << rect.right()  << " "
				<< "t:" << rect.top()    << L")";

	return str.str();
}

}


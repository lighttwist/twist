/// @file ClosedInterval.hpp
/// ClosedInterval class template definition and declarations of the free functions which are part of the class 
// interface

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_MATH_CLOSED_INTERVAL_HPP
#define TWIST_MATH_CLOSED_INTERVAL_HPP

#include "twist/IndexRange.hpp"

#include <optional>

namespace twist::math {

/*! A closed numeric interval (as such, neither bound can be infinite). 
    \tparam  The type of the interval bounds, it must be a number or enum type
 */
template<class T>
class ClosedInterval {
public:
	/*! Constructor.
	    \param[in] lo  The low interval bound
	    \param[in] hi  The high interval bound; must be greater or equal to the low bound
     */
	ClosedInterval(T lo, T hi);
		
	//! The high bound of the interval.
	[[nodiscard]] auto hi() const -> T;
	
	//! The low bound of the interval.
	[[nodiscard]] auto lo() const -> T;

private:
	static_assert(std::is_integral_v<T> || std::is_enum_v<T> || std::is_floating_point_v<T>, 
			      "ClosedInterval class template argument T must be a numeric or enum type.");
	T lo_;
	T hi_;

	TWIST_CHECK_INVARIANT_DECL
};

//! Equality operator.
template<class T> bool operator==(const ClosedInterval<T>& lhs, const ClosedInterval<T>& rhs);

//! Inequality operator.
template<class T> bool operator!=(const ClosedInterval<T>& lhs, const ClosedInterval<T>& rhs);

/*! Get the length of a closed numeric interval (the difference between its upper and lower bound).
    \tparam T  The type of the interval bounds
    \param[in] interv  The interval  
    \return  The length
 */
template<class T> 
[[nodiscard]] auto length(const ClosedInterval<T>& interv) -> T;
	
/*! Find out whether a closed numeric interval includes another.
    \tparam T  The type of the interval bounds
    \param[in] interv1  The first interval (tested as containing the other) 
    \param[in] interv2  The second interval (tested as being contained by the other)
    \return  true if the first interval includes the second
 */
template<class T> 
[[nodiscard]] auto includes(const ClosedInterval<T>& interv1, const ClosedInterval<T>& interv2) -> bool;

/*! Find out whether closed numeric interval contains a (numeric) point.
    \tparam T  The type of the interval bounds
    \param[in] interv  The interval  
    \param[in] point  The point
    \return  true if the interval contains the point.
 */
template<class T> 
[[nodiscard]] auto contains(const ClosedInterval<T>& interv, T point) -> bool;

/*! Count the elements in the number set denoted by a closed numeric interval.
    This function only exists for closed intervals of integer numbers.
    \tparam T  The type of the interval bounds; must be an integer type
    \param[in] interv  The interval
    \return  The number of elements in the set
 */
template<class T,
	     class = std::enable_if_t<std::is_integral_v<T>>> 
[[nodiscard]] auto count_elements(const ClosedInterval<T>& interv) -> Ssize;

/*! Enumerate the elements in the number set denoted by a closed numeric interval.
    This function only exists for closed intervals of integer numbers.
    \tparam T  The type of the interval bounds; must be an integer type
    \param[in] interv  The interval
    \return  The elements in the set, in increasing order
 */
template<class T,
	     class = std::enable_if_t<std::is_integral_v<T>>> 
std::vector<T> enumerate_elements(const ClosedInterval<T>& interv);

/*! For each element in the number set denoted by a closed numeric interval, call a user-supplied function 
    with that element as argument.
    \tparam T  The type of the interval bounds; must be an integer type
    \tparam Fn  Callable type compatible with signature (T) -> auto
    \param[in] interv  The interval
    \param[in] func  Callable object to be invoked with each element in the interval, in order  
 */
template<class T, class Fn,
	     class = std::enable_if_t<std::is_integral_v<T>>,
	     class = std::enable_if_t<std::is_invocable_v<Fn, T>>> 
void for_each_element(const ClosedInterval<T>& interv, Fn&& func);

/*! Get the intersection of two closed numeric intervals, if it exists.
    \tparam T  The type of the interval bounds
    \param[in] interv1  The first interval
    \param[in] interv2  The second interval
    \return  The intersection, or nullopt if the two intervals are disjoint 
 */
template<class T>
[[nodiscard]] auto intersect(const ClosedInterval<T>& interv1, const ClosedInterval<T>& interv2) 
                    -> std::optional<ClosedInterval<T>>;

/*! Get the interval which is the union of two closed numeric intervals (if it exists).
    \tparam T  The type of the interval bounds
    \param[in] interv1  The first interval
    \param[in] interv2  The second interval
    \return  The union, or nullopt if the two intervals are disjoint 
 */
template<class T>
[[nodiscard]] auto unite(const ClosedInterval<T>& interv1, const ClosedInterval<T>& interv2) 
                    -> std::optional<ClosedInterval<T>>;

/*! Get the smallest interval which contains (covers) two closed numeric intervals.
    \tparam T  The type of the interval bounds
    \param[in] interv1  The first interval
    \param[in] interv2  The second interval
    \return  The smallest interval which covers both 
 */
template<class T>
[[nodiscard]] auto cover(const ClosedInterval<T>& interv1, const ClosedInterval<T>& interv2) -> ClosedInterval<T>;

/*! Given a point, get the nearest point to it which falls within a numric interval (enclose the point in an 
      interval).
    If the original point falls within the interval, the result will be itself; otherwise the result will be 
      the nearest interval bound.
    \tparam T  The type of the interval bounds
    \param[in] interv  The interval
    \param[in] point  The original point
    \return  The nearest interval point
 */
template<class T>
T enclose(const ClosedInterval<T>& interv, T point);

/* Create an index range enumerating all the elements in the interval \p interv. This means the upper bound of the 
   range will be the upper bound of the interval, plus one.
*/
template<class T>
[[nodiscard]] auto to_index_range(const ClosedInterval<T>& interv) -> IndexRange<T>;

/*! Get a textual representation of a closed numeric interval.
    \tparam T  The type of the interval bounds
    \param[in] interv  The interval
    \return  The text
 */
template<class T>
[[nodiscard]] auto as_string(const ClosedInterval<T>& interv) -> std::wstring;

}

#include "twist/math/ClosedInterval.ipp"

#endif 


/// @file DistrParametric.cpp
/// Implementation file for "DistrParametric.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/math/DistrParametric.hpp"

namespace twist::math {

DistrParametric::DistrParametric() 
	: Distribution()
{
	TWIST_CHECK_INVARIANT
}


DistrParametric::~DistrParametric()
{
	TWIST_CHECK_INVARIANT
}


#ifdef _DEBUG
void DistrParametric::check_invariant() const noexcept
{
}
#endif // _DEBUG


//
//  Free functions
//

std::wstring get_info(const DistrParametric& distr, DistrInfoFormat format)
{
	std::wstringstream stream;

	stream << distr.getTypeName();
	if (format != DistrInfoFormat::k_no_params) {

		const auto param_names_values = distr.get_param_names_values();

		if (distr.getType() == Distribution::kDiscrete) {
			stream << L" " << param_names_values.size() / 2 << L" states";			
		}

		std::wstring param_sep;
		if (format == DistrInfoFormat::k_params_comma || format == DistrInfoFormat::k_compact) {
			stream << L" (";
			param_sep = L", ";
		}
		else if (format == DistrInfoFormat::k_params_newln) {
			stream << L"\n";
			param_sep = L"\n";
		}

		if (distr.getType() != Distribution::kDiscrete) {			
			const size_t num_params = param_names_values.size();
			for (size_t i = 0; i < num_params; ++i) {
				if (i > 0) {
					stream << param_sep;
				}
				stream << param_names_values[i].first << "=" << param_names_values[i].second;
			}
		}
		else {
			// We are describing the "discrete" distribution
			const size_t num_states = param_names_values.size() / 2;
			for (size_t i = 0; i < num_states; ++i) {
				if (i > 0) {
					stream << param_sep;
				}
				if (format == DistrInfoFormat::k_compact) {
					// Only output the state values
					stream << param_names_values[i * 2].second;
				}
				else {
					stream << L"state " << i + 1 << L" val/prob = " << param_names_values[i * 2].second
						    << L" / "  << param_names_values[i * 2 + 1].second;
				}
			}
		}

		if (format == DistrInfoFormat::k_params_comma || format == DistrInfoFormat::k_compact) {
			stream << L")";
		}
	}

	return stream.str();
}

} // namespace math

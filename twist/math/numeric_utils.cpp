/// @file numeric_utils.cpp
/// Implementation file for "math_utils.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/math/numeric_utils.hpp"

#include <cmath>
#include <numeric>

namespace twist::math {

auto round_to_int(double x) -> int
{
	return static_cast<int>(x > 0 ? x + 0.5 : x - 0.5);
}

auto floor_to_int(double x) -> int
{
	return static_cast<int>(std::floor(x));
}

int ceil_to_int(double x)
{
	return static_cast<int>(std::ceil(x));
}

auto round_to_long(double x) -> long
{
	return static_cast<long>(x > 0 ? x + 0.5 : x - 0.5);
}

auto floor_to_long(double x) -> long
{
	return static_cast<long>(std::floor(x));
}

auto floor_to_ssize(double x) -> Ssize
{
	return static_cast<Ssize>(std::floor(x));
}

auto ceil_to_long(double x) -> long
{
	return static_cast<long>(std::ceil(x));
}

auto ceil_to_ssize(double x) -> Ssize
{
	return static_cast<Ssize>(std::ceil(x));
}

auto round_to_ssize(double x) -> Ssize
{
	return static_cast<Ssize>(std::llround(x));
}

double get_nan()
{
	return std::numeric_limits<double>::quiet_NaN();
}

} 


///  @file  textualise.hpp
///  Utilities for creating textual representations of various objects

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_MATH_TEXTUALISE_HPP
#define TWIST_MATH_TEXTUALISE_HPP

#include "geometry.hpp"
#include "Matrix.hpp"
#include "SymmetricMatrix.hpp"

namespace twist::math {

/// Get a textual representation of a matrix. The columns are separated using the string supplied, and 
/// the rows are separated by newline. The values are output using the default precision. 
///
/// @param[in] matrix  The matrix  
/// @param[in] col_sep  The column separator.
/// @return  The textual representation.	
///
template<typename T>
std::wstring to_string(const Matrix<T>& matrix, const std::wstring& col_sep);	
  
/// Get a textual representation of a matrix. The columns are separated using the string supplied, and 
/// the rows 
/// are separated by newline. The values are output using a certain precision.
///
/// @param[in] matrix  The matrix  
/// @param[in] val_sep  The values separator
/// @param[in] precision  The precision (ie the maximum number of decimals to be displayed for each value); 
///					ignored if the matrix elements are not floating-point values
/// @return  The textual representation	
///
template<typename T>
std::wstring to_string(const Matrix<T>& matrix, const std::wstring& col_sep, size_t precision);

/// Get a textual representation of a symmetric matrix. The values are separated using the string supplied, 
/// the rows are separated by NEWLINE, and, if the values are floating-point values, they are output using 
/// the default precision.
///
/// @param[in] matrix  The matrix  
/// @param[in] val_sep  The values separator
/// @return  The textual representation
///
template<typename T>
std::wstring to_string(const SymmetricMatrix<T>& matrix, const std::wstring& val_sep);	
    
/// Get a textual representation of a symmetric matrix. The values are separated using the string supplied, 
/// the rows are separated by NEWLINE, and, if the values are floating-point values, they are output using 
/// a certain precision.
///
/// @param[in] matrix  The matrix  
/// @param[in] val_sep  The values separator
/// @param[in] precision  The precision (ie the maximum number of decimals to be displayed for each value); 
///					ignored if the matrix elements are not floating-point values
/// @return  The textual representation	
///
template<typename T>
std::wstring to_string(const SymmetricMatrix<T>& matrix, const std::wstring& val_sep, size_t precision);

/// Get a textual representation of a lower-triangular matrix. The values are separated using the string 
/// supplied, the rows are separated by NEWLINE, and, if the values are floating-point values, they are output 
/// using the default precision.
///
/// @param[in] matrix  The matrix  
/// @param[in] val_sep  The values separator
/// @return  The textual representation.	
///
template<typename T>
std::wstring to_string(const LowerTriangularMatrix<T>& matrix, const std::wstring& val_sep);	
    
/// Get a textual representation of a lower-triangular matrix. The values are separated using the string 
/// supplied, the rows are separated by NEWLINE, and, if the values are floating-point values, they are output 
/// using a certain precision.
///
/// @param[in] val_sep  The values separator
/// @param[in] precision  The precision (ie the maximum number of decimals to be displayed for each value); 
///					ignored if the matrix elements are not floating-point values
/// @return  The textual representation.	
///
template<typename T>
std::wstring to_string(const LowerTriangularMatrix<T>& matrix, const std::wstring& val_sep, 
		size_t precision);	

/// Get a succinct textual representation of a rectangle's coordinates.  
///
/// @tparam  Coord  The rectangle coordinate type
/// @param[in] rect  The rectangle
/// @return  The textual representation	
///
template<typename Coord> 
std::wstring to_string(const Rectangle<Coord>& rect);

}

#include "textualise.ipp"

#endif

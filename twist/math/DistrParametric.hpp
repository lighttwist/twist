/// @file DistrParametric.hpp
/// DistrParametric class, inherits Distribution

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_MATH_DISTR_PARAMETRIC_HPP
#define TWIST_MATH_DISTR_PARAMETRIC_HPP

#include "twist/math/Distribution.hpp"

namespace twist::math {

//! Abstract base class for all classes representing a known (paramateric) distribution.
class DistrParametric : public Distribution {
public:
	using ParamNamesValues = std::vector<std::pair<std::wstring, double>>;

	~DistrParametric() override;
	
	DistrParametric* getClone() const override = 0; // Distribution override
	
	/// Apply the CDF.
	///
	/// @param  x  [in] Argument of the function.
	/// @return  Value of the CDF.
	///
	virtual double virtual_cdf(double x) const = 0; //+OBSOLETE

	/*! Apply the inverse CDF.
	    \param[in] p  Cumulative probability value; must be in the interval (0, 1) 
	    \return  The corresponding realization
	 */
	virtual auto virtual_invCdf(double p) const -> double = 0; //+OBSOLETE

	/// Get the number of distribution parameters.
	///
	/// @return  The parameters count.
	///
	virtual twist::Ssize countParams() const = 0;

	///+TODO: Comment
	virtual ParamNamesValues get_param_names_values() const = 0;

	// --- DistrParametric concept methods  ---
	
	// Find out whether a set of parameter values is valid for the distribution.
	//
	// @param  pn  [in] The n-th parameter value
	// @param  err  [out] If the parameters set is invalid, a string describing the error
	// @return  true if the set of parameters is valid
	//
	// static bool validate_params(double p1, ..., std::wstring& err) const;

	TWIST_NO_COPY_DEF_MOVE(DistrParametric)

protected:
	DistrParametric();

private:
	TWIST_CHECK_INVARIANT_DECL
};

// --- Free functions ---

// Formats of a distribution's information string
enum DistrInfoFormat {
	k_no_params     =  1,  // Only the distribution type name, no parameters 
	k_params_comma  =  2,  // The distribution type name and parameters (names and values) separated by commas
	k_params_newln  =  3,  // The distribution type name and parameters (names and values) separated by new line
	k_compact       =  4   // The distribution type name and a compact representation of the parameters
};

/// Get a string containing information about a distribution instance, including the distribution type name and the parameters.
///
/// @param  distr  [in] The distribution
/// @param  format  [in] The information format
/// @return  The description
///
std::wstring get_info(const DistrParametric& distr, DistrInfoFormat format = DistrInfoFormat::k_params_comma);

} 

#endif 

///  @file  RealInterval.hpp
///  RealInterval class definition

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_MATH_REAL_INTERVAL_HPP
#define TWIST_MATH_REAL_INTERVAL_HPP

namespace twist::math {

///
///  Class representing an interval in R, the real number set. 
///  The template argument is the type of the interval bounds, and it should be a floating-point number type 
///    which can represent infinity.
///
template<typename T>
class RealInterval {
public:
	// Interval types
	enum Type {
		closed          = 1,  // Closed interval, of the form [a, b]
		open            = 2,  // Open interval, of the form (a, b)
		semi_open_left  = 3,  // Semi-open interval, of the form (a, b]
		semi_open_right = 4	  // Semi-open interval, of the form [a, b)
	};

	// Infinity constant
	static T infinity;

	/// Infer the interval type from the bound types.
	///
	/// @param[in] left_open  Whether the left bound is open. 
	/// @param[in] right_open  Whether the right bound is open. 
	///
	static Type infer_type(bool left_open, bool right_open);

	/// Default constructor; initialises the interval to (-inf, +inf).
	///
	RealInterval();

	/// Constructor.
	///
	/// @param[in] lo  The low interval bound.
	/// @param[in] hi  The high interval bound. Must be greater or equal to the low bound.
	/// @param[in] type  The interval type.
	///
	RealInterval(T lo, T hi, Type type = closed);
	
	/// Equality operator "equal to".
	///
	bool operator==(const RealInterval& rhs) const;	

	/// Equality operator "not equal to".
	///
	bool operator!=(const RealInterval& rhs) const;
	
	/// Get the high bound of the interval.
	///
	/// @return  The high bound.
	///
	T hi() const;
	
	/// Get the low bound of the interval.
	///
	/// @return  The high bound.
	///
	T lo() const;
	
	/// Get the interval type.
	///
	/// @return  The type.
	///
	Type type() const;

	/// Get the interval length (the difference between its upper and lower bound).
	///
	/// @return  The length
	///
	T length() const;
	
	/// Find out whether this interval includes another.
	///
	/// @param[in] other  The other interval.
	/// @return  true if the other interval is included in this interval.
	///
	bool includes(const RealInterval& other) const;

	/// Find out whether this interval contains a point.
	///
	/// @param[in] point  The point.
	/// @return  true if the interval contains the point.
	///
	bool contains(T point) const;

	/// Find out whether the left interval bound is open or closed.
	///
	/// @return  true if the left bound is open.
	///
	bool left_open() const;

	/// Find out whether the right interval bound is open or closed.
	///
	/// @return  true if the right bound is open.
	///
	bool right_open() const;

private:
	static_assert(std::is_floating_point_v<T>, 
			"RealInterval class template argument T must be a floating-point type.");

	/// Find out whether a point is contained on the left in the interval.
	///
	/// @param[in] point  The point.
	/// @result  true if it is.
	///
	bool left_bound_ok(T point) const;

	/// Find out whether another interval is included on the left in the interval.
	///
	/// @param[in] other  The other interval.
	/// @result  true if it is.
	///
	bool left_bound_ok(const RealInterval& other) const;

	/// Find out whether a point is contained on the right in the interval.
	///
	/// @param[in] point  The point.
	/// @result  true if it is.
	///
	bool right_bound_ok(T point) const;

	/// Find out whether another interval is included on the right in the interval.
	///
	/// @param[in] other  The other interval.
	/// @result  true if it is.
	///
	bool right_bound_ok(const RealInterval& other) const;

	T  lo_;
	T  hi_;
	Type  type_;

	TWIST_CHECK_INVARIANT_DECL
};

template<typename T> T RealInterval<T>::infinity = std::numeric_limits<T>::infinity();

//
//  Global functions
//

/// Get a textual representation of an interval.
///
/// @param[in] interval  The interval
/// @param[in] precision  The number precision. Pass in zero for default.
/// @return  The text.
///
template<typename T> 
std::wstring to_str(const RealInterval<T>& interval, const size_t precision = 0);

/// "Connect" two intervals to obtain another, greater interval, which starts from the lowest of the lower 
/// interval bounds to the highest of the upper interval bounds.
///
/// @tparam  T  Type of interval bounds.
/// @param[in] interval1  The first interval.
/// @param[in] interval2  The second interval.
/// @return  The "connected" interval.
/// 
template<typename T> 
RealInterval<T> connect(const RealInterval<T>& interval1, const RealInterval<T>& interval2);

/// Partition an interval into multiple sub-intervals, given a range of points included in the interval.
/// The partition is such that the union of the sub-intervals is the original interval, while their intersection is null, 
///   and their bounds consist of the original interval bounds and the points. All sub-intervals (with the possible exception 
///   of the first and last) will be semi-open.
/// The point range must be sorted.
/// The sub-intervals will be output in ascending order of their bounds.
///
/// @tparam  T  Type of interval bounds
/// @tparam  Iter  Type of input iterator for the points
/// @tparam  OutIter  Type of output iterator for the sub-intervals
/// @param[in] interval  The original interval
/// @param[in] first_point  Iterator addressing the first point in the sorted point range
/// @param[in] last_point  Iterator addressing one past the last point in the sorted point range
/// @param[in] out_it  The output iterator for sub-intervals
/// @param[in] left_open  Whether the sub-intervals should be open on the left (true) or on the right (false)
///
template<class T, class Iter, class OutIter> 
void partition(const RealInterval<T>& interval, Iter first_point, Iter last_point, OutIter out_it, 
		bool left_open);

/// Find an interval, within a range of intervals, which contains a specific point.
/// The interval search starts from a given position in the range of intervals and moves out from that 
///   position to the left and right until both ends of the range have been reached or the first interval 
///   containing the point is found.
/// 
/// @tparam  T  Type of interval bounds
/// @tparam  Iter  Type of input iterator for the points (must be bidirectional)
/// @param[in] point  The point
/// @param[in] first_interval  Iterator addressing the first interval in the range
/// @param[in] last_interval  Iterator addressing one past the final interval in the range
/// @param[in] start_interval  The interval where the search starts
/// @return  Iterator addressing the containing range, or last_interval if no range contains the point
///
template<typename T, typename Iter>
Iter find_real_interval(T point, Iter first_interval, Iter last_interval, Iter start_interval);

} 

#include "RealInterval.ipp"

#endif 



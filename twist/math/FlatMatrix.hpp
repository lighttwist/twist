/// @file FlatMatrix.hpp
/// FlatMatrix class template definition and declarations of the free functions which are part of the class interface

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_MATH_FLAT_MATRIX_HPP
#define TWIST_MATH_FLAT_MATRIX_HPP

#include "twist/grid_utils.hpp"
#include "twist/twist_stl.hpp"

namespace twist::math {

/*! A matrix which internally uses a "flat array" (a contiguous block of memory) to store its cell values.
    \tparam T  The type of a cell's value; its requiremets match those of the std::vector element type
 */
template<class T>
class FlatMatrix {
public:
	using Value = T;

	using value_type = T;
	using size_type = Ssize;

	using reference = typename std::vector<T>::reference;
	using const_reference = typename std::vector<T>::const_reference;

	using iterator = typename std::vector<T>::iterator;
	using const_iterator = typename std::vector<T>::const_iterator;

	using reverse_iterator = typename std::vector<T>::reverse_iterator;
	using const_reverse_iterator = typename std::vector<T>::const_reverse_iterator;

	/*! A range of cells defined by two cell iterators.
        \tparam Iter  The iterator type
	 */
	template<class Iter> 
	class CellRange : public Range<Iter> {
	public:	
		/* Constructor.
		   \param[in] first  Iterator addressing the first cell in the range
		   \param[in] last  Iterator addressing one past the final cell in the range
		 */
		CellRange(Iter first, Iter last); 

		/* Get a direct pointer addressing the first cell in the range, inside the "flat" memory array used internally 
		   by the matrix to store its cells.
		   \return  Pointer to the memory array, allowing read-only access
		 */
		[[nodiscard]] auto data() const -> const T*;

		/* Get a direct pointer addressing the first cell in the range, inside the "flat" memory array used internally 
		   by the matrix to store its cells.
		   \return  Pointer to the memory array, allowing read-write access
		 */
		[[nodiscard]] auto data() -> T*;
	};

	//! Constructor. Creates a matrix with zero rows and columns.
	explicit FlatMatrix() = default;

	/*! Constructor. Creates a matrix with specific dimensions, and fills it with the default value for the cell 
	    value type.
	    \param[in] nof_rows  The row count
	    \param[in] nof_cols  The column count
	 */
	explicit FlatMatrix(Ssize nof_rows, Ssize nof_cols);

	/*! Constructor. Creates a matrix with specific dimensions, and fills it with a specific cell value.
	    \param[in] nof_rows  The row count
		\param[in] nof_cols  The column count
	    \param[in] value  The fill value
	 */
	explicit FlatMatrix(Ssize nof_rows, Ssize nof_cols, const_reference value);

	/*! Constructor. Creates a matrix with specific dimensions, and intialises all cell values by moving a std::vector 
	    instance to the internal storage. Does not invoke any move, copy on individual cell values. The vector size 
		must match the matrix dimensions and its elements must match the internal storage structure of the matrix.
	    \param[in] nof_rows  The number of rows in each matrix
	    \param[in] nof_cols  The number of columns in each matrix
	    \param[in] values  Vector containing the cell values
	 */
	explicit FlatMatrix(Ssize nof_rows, Ssize nof_cols, std::vector<T> values);

	//! Get a clone of this matrix.
	[[nodiscard]] auto clone() const -> FlatMatrix<T>;

	/*! Get an iterator addressing the value in the first (top-left) cell in the matrix.
	    The iterator type allows read-write access to the addressed value.
	    If the matrix is empty, this iterator is the same as the iterator returned by end().
     */
	[[nodiscard]] iterator begin();

	/*! Get an iterator addressing one position past the value in the last (bottom-right) cell in the matrix.
	    The iterator type allows read-write access to the addressed value.
     */
	[[nodiscard]] iterator end();

	/*! An iterator addressing the value in the first (top-left) cell in the matrix.
	    The iterator type allows read-only access to the addressed value.
	    If the matrix is empty, this iterator is the same as the iterator returned by end().
     */
	[[nodiscard]] const_iterator begin() const;

	/*! An iterator addressing one position past the value in the last (bottom-right) cell in the matrix.
	    The iterator type allows read-only access to the addressed value.
     */
	[[nodiscard]] const_iterator end() const;
	
	/*! An iterator addressing the value in the first (top-left) cell in the matrix.
	    The iterator type allows read-only access to the addressed value.
	    If the matrix is empty, this iterator is the same as the iterator returned by end().
     */
	[[nodiscard]] const_iterator cbegin() const;
	
	/*! An iterator addressing one position past the value in the last (bottom-right) cell in the matrix.
	    The iterator type allows read-only access to the addressed value.
     */
	[[nodiscard]] const_iterator cend() const;
	
	/*! A reverse iterator addressing the last (bottom-right) cell in the matrix. The iterator type allows 
	    read-write access to the addressed value. If the matrix is empty, this iterator is the same as the 
	    iterator returned by rend().
     */
	[[nodiscard]] reverse_iterator rbegin();
	
	/*! A reverse iterator addressing one position before the value in the first (top-left) cell in the 
	    matrix. The iterator type allows read-write access to the addressed value. 
     */
	[[nodiscard]] reverse_iterator rend();
	
	/*! A reverse iterator addressing the last (bottom-right) cell in the matrix. The iterator type allows 
	    read-only access to the addressed value. If the matrix is empty, this iterator is the same as the 
	    iterator returned by rend().
     */
	[[nodiscard]] const_reverse_iterator rbegin() const;
	
	/*! A reverse iterator addressing one position before the value in the first (top-left) cell in the 
	    matrix. The iterator type allows read-only access to the addressed value. 
     */
	[[nodiscard]] const_reverse_iterator rend() const;
	
	/*! A reverse iterator addressing the last (bottom-right) cell in the matrix. The iterator type allows 
	    read-only access to the addressed value. If the matrix is empty, this iterator is the same as the 
	    iterator returned by rend().
     */
	[[nodiscard]] const_reverse_iterator crbegin() const;
	
	/*! A reverse iterator addressing one position before the value in the first (top-left) cell in the 
	    matrix. The iterator type allows read-only access to the addressed value. 
     */
	[[nodiscard]] const_reverse_iterator crend() const;

	/// Get a direct pointer to the "flat" memory array used internally by the matrix to store its cells.
	///
	/// @return  Pointer to the memory array, allowing read-only access
	///
	[[nodiscard]] const T* data() const;

	/// Get a direct pointer to the "flat" memory array used internally by the matrix to store its cells.
	///
	/// @return  Pointer to the memory array, allowing read-write access
	///
	[[nodiscard]] T* data();

	/*! Get read-write access to a specific cell.
	    \param[in] row  The row index; undefined behaviour if it is out of bounds  
	    \param[in] col  The column index; undefined behaviour if it is out of bounds  
	    \return  Reference to the cell value
	 */
	[[nodiscard]] reference operator()(Ssize row, Ssize col);

	/*! Get read-only access to a specific cell.
	    \param[in] row  The row index; undefined behaviour if it is out of bounds  
	    \param[in] col  The column index; undefined behaviour if it is out of bounds  
	    \return Reference to the cell
	 */
	[[nodiscard]] const_reference operator()(Ssize row, Ssize col) const;

	/*! Get read-write access to a specific cell.
	    \param[in] cell_index  The "grid cell index" for the cell; undefined behaviour if it is out of bounds  
	    \return Reference to the cell value
	 */
	[[nodiscard]] reference operator()(GridCellIndex cell_index);

	/*! Get read-only access to a specific cell.
	    \param[in] row  The row index; undefined behaviour if it is out of bounds  
	    \param[in] col  The column index; undefined behaviour if it is out of bounds  
	    \return Reference to the cell value
	 */
	[[nodiscard]] const_reference operator()(GridCellIndex cell_index) const;

	/*! Get read-write access to a specific cell.
	    \param[in] row  The row index; an exception is thrown if it is out of bounds  
	    \param[in] col  The column index; an exception is thrown if it is out of bounds  
	    \return Reference to the cell value
	 */
	[[nodiscard]] reference at(Ssize row, Ssize col);

	/*! Get read-only access to a specific cell.
	    \param[in] row  The row index; an exception is thrown if it is out of bounds  
	    \param[in] col  The column index; an exception is thrown if it is out of bounds  
	    \return Reference to the cell value
	 */
	[[nodiscard]] const_reference at(Ssize row, Ssize col) const;

	/*! Get read-write access to a specific cell.
	    \param[in] cell_index  The "grid cell index" for the cell; undefined behaviour if it is out of bounds  
	    \return Reference to the cell value
	 */
	[[nodiscard]] reference at(GridCellIndex cell_index);

	/*! Get read-only access to a specific cell.
	    \param[in] row  The row index; an exception is thrown if it is out of bounds    
	    \param[in] col  The column index; an exception is thrown if it is out of bounds    
	    \return Reference to the cell value
	 */
	[[nodiscard]] const_reference at(GridCellIndex cell_index) const;

	/*! Change the dimensions of the matrix. The cell values are preserved where possible.
	    \param[in] new_row_count  The new row count
	    \param[in] new_col_count  The new column count
	 */
	auto resize(Ssize new_row_count, Ssize new_col_count) -> void;

	//! The number of cells in the matrix.
	[[nodiscard]] Ssize size() const;
	
	//! Whether the matrix is empty, that is if all its dimensions are zero.
	[[nodiscard]] bool empty() const;

	///! The number of rows in the matrix.
	[[nodiscard]] auto nof_rows() const -> Ssize;

	///! The number of columns in the matrix.
	[[nodiscard]] auto nof_columns() const -> Ssize;

	///! The number of cells in the matrix.
	[[nodiscard]] auto nof_cells() const -> Ssize;

	/*! Get a read-only view of a whole matrix row.
	    \param[in] row_idx  The row index; an exception is thrown if it is out of bounds  
	    \return  Range addressing the cells in the row
	 */
	[[nodiscard]] auto get_row(Ssize row_idx) const -> CellRange<const_iterator>;
	
	/*! Get a read-only view of a whole matrix row.
	    \param[in] row  The row index; an exception is thrown if it is out of bounds  
	    \return  Range addressing the cells in the row
	 */
	[[nodiscard]] auto view_row_cells(Ssize row_idx) const -> CellRange<const_iterator>; //+OBSOLETE: Use get_row()

	/*! Get a read - write view of a whole matrix row.
	    \param[in] row  The row index; an exception is thrown if it is out of bounds  
	    \return  Range addressing the cells in the row
	*/
	[[nodiscard]] auto view_row_cells(Ssize row) -> CellRange<iterator>;

	/// Get a read-only view of a set of contiguous cells in a matrix row.
	///
	/// @param[in] row  The row index; an exception is thrown if it is out of bounds  
	/// @param[in] begin_col  The column index of the first cell in the view; an exception is thrown if it is 
	///					out of bounds  
	/// @param[in] cell_count  The number of cells in the view; an exception is thrown if it is out of bounds  
	/// @return  Range addressing the set of cells 
	///
	[[nodiscard]] CellRange<const_iterator> view_row_cells(Ssize row, Ssize begin_col, Ssize cell_count) const;

	/// Get a read-write view of a set of contiguous cells in a matrix row.
	///
	/// @param[in] row  The row index; an exception is thrown if it is out of bounds  
	/// @param[in] begin_col  The column index of the first cell in the view; an exception is thrown if it is 
	///					out of bounds  
	/// @param[in] cell_count  The number of cells in the view; an exception is thrown if it is out of bounds  
	/// @return  Range addressing the set of cells 
	///
	[[nodiscard]] CellRange<iterator> view_row_cells(Ssize row, Ssize begin_col, Ssize cell_count); 

	/// Get a read-write view of a set of contiguous rows in the matrix.
	///
	/// @param[in] begin_row  The index of the first row in the view  
	/// @param[in] end_row  The index of the last row in the view  
	/// @return  Range addressing the cells in the rows
	///
	[[nodiscard]] CellRange<const_iterator> view_rows(Ssize begin_row, Ssize end_row) const;

	/*! Get a read-only view of the whole matrix.
	    \return  Range addressing all cells in the matrix
	*/
	[[nodiscard]] auto view_matrix() const -> CellRange<const_iterator>;

	/*! Get a read-write view of the whole matrix.
	    \return  Range addressing all cells in the matrix
	*/
	[[nodiscard]] auto view_matrix() -> CellRange<iterator>;

	TWIST_NO_COPY_DEF_MOVE(FlatMatrix)  

private:
	void check_row_col(Ssize row, Ssize col) const;

	void check_begin_end_rows(Ssize begin_row, Ssize end_row) const;

	auto check_dimensions() const -> void;

	Ssize nof_rows_;
	Ssize nof_cols_;
	std::vector<T> data_;

	template<class U> 
	friend auto swap(FlatMatrix<U>&, FlatMatrix<U>&) noexcept -> void;

	TWIST_CHECK_INVARIANT_DECL
};

// --- Free functions ---

/*! Exchange the contents of a matrix with those of other. Does not invoke any move, copy, or swap operations on 
    individual cell values. All iterators and references remain valid (but will be pointing in the other container). 
	The past-the-end iterators are invalidated. The dimensions of each matrix may change.
    \tparam T  The cell value type
    \param[in] a  The first matrix  
    \param[in] b  The second matrix  
 */
template<class T> 
auto swap(FlatMatrix<T>& a, FlatMatrix<T>& b) noexcept -> void;

//! Equality operator.
template<class T> 
[[nodiscard]] auto operator==(const FlatMatrix<T>& lhs, const FlatMatrix<T>& rhs) -> bool;

//! Inequality operator.
template<class T> 
[[nodiscard]] auto operator!=(const FlatMatrix<T>& lhs, const FlatMatrix<T>& rhs) -> bool;

/*! Get the dimensions (the number of row and columns) of a matrix.
    \tparam T  The cell value type
    \param[in] mat  The matrix
    \return  0. The number of rows
 			 1. The number of columns
 */
template<class T> 
[[nodiscard]] auto dims(const FlatMatrix<T>& mat) -> std::tuple<Ssize, Ssize>;

/*! Whether the submatrix described by \p submatrix_info represents a valid submatrix of the matrix \p mat.
	\tparam T  The cell value type
*/
template<class T> 
[[nodiscard]] auto is_valid_submatrix_of(const SubgridInfo& submatrix_info, const FlatMatrix<T>& mat) -> bool;

/*! Call a user-supplied function with the read-only value in each cell of the matrix.
    \tparam T  The cell value type
    \tparam Fn  Type of the function to call; the second and third parameters are the row and column index
    \param[in] mat  The matrix  
    \param[in] func  The function
 */
template<class T, class Fn>
requires std::invocable<Fn, const T&, Ssize, Ssize>
auto for_each_cell(const FlatMatrix<T>& mat, Fn func) -> void;

/*! Call a user-supplied function with the read-write value in each cell of the matrix.
    \tparam T  The cell value type
    \tparam Fn  Type of the function to call; the second and third parameters are the row and column index
    \param[in] mat  The matrix  
    \param[in] func  The function
 */
template<class T, class Fn>
requires std::invocable<Fn, T&, Ssize, Ssize>
auto for_each_cell(FlatMatrix<T>& mat, Fn func) -> void;

/*! Copy the values in a matrix, or a submatrix of itself, into another matrix.
	\tparam SrcT  The cell value type of the source matrix
	\tparam T  The cell value type of the destination matrix
	\param[in] src_mat  The source matrix  
	\param[in] dest_mat  The destination matrix  
	\param[in] src_submatrix_info  Optional information about the submatrix of the source matrix which should be 
	                               copied; if nullopt, the whole source matrix is copied
	\param[in] dest_top_row  The destination row index where the top row of the source will be after the copy 
	\param[in] dest_left_col  The destination column index where the leftmost column of the source will be after the 
	                          copy 
 */
template<class SrcT, class DestT> 
auto copy_into(const FlatMatrix<SrcT>& src_mat, 
               FlatMatrix<DestT>& dest_mat,
               std::optional<SubgridInfo> src_submatrix_info = std::nullopt,
               Ssize dest_top_row = 0, 
			   Ssize dest_left_col = 0) -> void;

/*! Flip the rows in matrix \p mat so that the first row is swapped with the last, the second row with the row before 
    last, etc.
 */
template<class T> 
[[nodiscard]] auto flip_rows(FlatMatrix<T>& mat) -> void;

}

#include "twist/math/FlatMatrix.ipp"

#endif

/// @file ColumnwiseMatrix.ipp
/// Inline implementation file for "ColumnwiseMatrix.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/IndexRange.hpp"
#include "twist/std_vector_utils.hpp"

namespace twist::math {

// --- ColumnwiseMatrix class template ---

template<class T>
ColumnwiseMatrix<T>::ColumnwiseMatrix()
{
	TWIST_CHECK_INVARIANT
}

template<class T>
auto ColumnwiseMatrix<T>::clone() const -> ColumnwiseMatrix
{
	TWIST_CHECK_INVARIANT
	auto clone = ColumnwiseMatrix{};
	clone.nof_rows_ = nof_rows_;
	clone.columns_ = columns_;
	return clone;
}

template<class T>
auto ColumnwiseMatrix<T>::nof_rows() const -> Ssize
{
	TWIST_CHECK_INVARIANT
	return nof_rows_;
}

template<class T>
auto ColumnwiseMatrix<T>::nof_columns() const -> Ssize
{
	TWIST_CHECK_INVARIANT
	return ssize(columns_);
}

template<class T>
auto ColumnwiseMatrix<T>::nof_cells() const -> Ssize
{
	TWIST_CHECK_INVARIANT
	return nof_rows_ * ssize(columns_);
}

template<class T>
auto ColumnwiseMatrix<T>::operator()(Ssize row_idx, Ssize col_idx) const -> const T&
{
	TWIST_CHECK_INVARIANT
	return columns_[col_idx][row_idx];
}

template<class T>
auto ColumnwiseMatrix<T>::operator()(Ssize row_idx, Ssize col_idx) -> T&
{
	TWIST_CHECK_INVARIANT
	return columns_[col_idx][row_idx];
}

template<class T>
auto ColumnwiseMatrix<T>::get_cell(Ssize row_idx, Ssize col_idx) const -> const T&
{
	TWIST_CHECK_INVARIANT
	if (row_idx >= nof_rows_) {
		TWIST_THROW(L"Invalid row index %lld.", row_idx);
	}
	if (col_idx >= ssize(columns_)) {
		TWIST_THROW(L"Invalid column index %lld.", col_idx);
	}	
	return columns_[col_idx][row_idx];
}

template<class T>
auto ColumnwiseMatrix<T>::set_cell(Ssize row_idx, Ssize col_idx, T value) -> void
{
	TWIST_CHECK_INVARIANT
	if (row_idx >= nof_rows_) {
		TWIST_THROW(L"Invalid row index %lld.", row_idx);
	}
	if (col_idx >= ssize(columns_)) {
		TWIST_THROW(L"Invalid column index %lld.", col_idx);
	}	
	columns_[col_idx][row_idx] = std::move(value);
}

template<class T>
auto ColumnwiseMatrix<T>::add_row(const std::vector<T>& row_values) -> Ssize
{
	TWIST_CHECK_INVARIANT
	if (row_values.empty()) {
		TWIST_THROW(L"Cannot add an empty row.");
	}
	if (columns_.empty()) {
		// The matrix is empty
		for (auto j : IndexRange{ssize(row_values)}) {
			columns_.push_back(std::vector<T>{row_values[j]});
		}
	}
	else {
		if (ssize(row_values) != ssize(columns_)) {
			TWIST_THROW(L"The number %lld of new row values does not match the number of columns %lld in the matrix.",
						ssize(row_values), ssize(columns_));
		}
		for (auto j : IndexRange{ssize(columns_)}) {
			auto& col = columns_[j];
			col.push_back(row_values[j]);
		}
	}
	return nof_rows_++;
}

template<class T>
auto ColumnwiseMatrix<T>::remove_row(Ssize row_idx) -> void
{
	TWIST_CHECK_INVARIANT
	if (row_idx >= nof_rows_) {
		TWIST_THROW(L"Invalid row index %d.", row_idx);
	}
	for (auto& col : columns_) {
		col.erase(begin(col) + row_idx);
	}
	--nof_rows_;
	if (nof_rows_ == 0) {
		// The last row has been removed. The matrix is empty.
		columns_.clear();
	}
}

template<class T>
auto ColumnwiseMatrix<T>::get_row(Ssize row_idx) const -> std::vector<T>
{
	TWIST_CHECK_INVARIANT
	if (row_idx >= nof_rows_) {
		TWIST_THROW(L"Invalid row index %d.", row_idx);
	}
	auto row_values = reserve_vector<T>(ssize(columns_));
	for (const auto& col : columns_) {
		row_values.push_back(col[row_idx]);
	}
	return row_values;
}

template<class T>
auto ColumnwiseMatrix<T>::insert_column(Ssize col_idx, std::vector<T> col_values) -> void
{
	TWIST_CHECK_INVARIANT
	if (col_idx > ssize(columns_)) {
		TWIST_THROW(L"The index %lld of the new column is larger that the number of columns %lld.",
		            col_idx, ssize(columns_));
	}

	if (nof_rows_ == 0) {
		// The matrix is empty
		if (col_values.empty()) {
			TWIST_THROW(L"Cannot add empty column.");
		}
		nof_rows_ = ssize(col_values);
	}
	else if (ssize(col_values) != nof_rows_) {
		TWIST_THROW(L"The number %lld of new column values does not match the number of rows %lld in the matrix.",
					ssize(col_values), nof_rows_);
	}

	columns_.insert(begin(columns_) + col_idx, std::move(col_values));
}

template<class T>
auto ColumnwiseMatrix<T>::remove_column(Ssize col_idx) -> void
{
	TWIST_CHECK_INVARIANT
	if (col_idx > ssize(columns_)) {
		TWIST_THROW(L"The index %lld of the new column is larger that the number of columns %lld.",
		            col_idx, ssize(columns_));
	}
	columns_.erase(begin(columns_) + col_idx);
	if (columns_.empty()) {
		// The last column has been removed. The matrix is empty.
		nof_rows_ = 0;
	}
}

template<class T>
auto ColumnwiseMatrix<T>::extract_column(Ssize col_idx) -> std::vector<T>
{
	TWIST_CHECK_INVARIANT
	if (col_idx > ssize(columns_)) {
		TWIST_THROW(L"The index %lld of the new column is larger that the number of columns %lld.",
		            col_idx, ssize(columns_));
	}

	auto it = begin(columns_) + col_idx;
	auto col_values = move(*it);

	columns_.erase(it);
	if (columns_.empty()) {
		// The last column has been removed. The matrix is empty.
		nof_rows_ = 0;
	}

	return col_values;
}

#ifdef _DEBUG
template<class T>
auto ColumnwiseMatrix<T>::check_invariant() const noexcept -> void
{
	assert(nof_rows_ >= 0);
	assert(rg::all_of(columns_, [this](const auto& col) { return ssize(col) == nof_rows_; }));
}
#endif 

} 

/// @file random_utils.hpp
/// Utilities for generating random numbers

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_MATH_RANDOM__UTILS_HPP
#define TWIST_MATH_RANDOM__UTILS_HPP

namespace twist::math {

//! Pseudo-random number generator engine types.
enum class RandEngineType {
	mt_19937 = 1, ///< Mersenne Twister 19937 generator
};

/*! Generate a random sample of floating-point numbers from a uniform distribution, in the interval [lo, hi).
    \tparam T  The floating-point number type
    \param[in] size  The size of the sample to be generated
    \param[in] lo  The lower bound of the distribution; the generated numbers will be greater or equal to it  
    \param[in] hi  The upper bound of the distribution; the generated numbers will smaller than it  
    \param[in] rand_engine_type  The pseudo-random number generator engine type to be used; an engine object
                                 will be created for generating the sample
    \return  The generated sample
 */
template<class T,
         class = EnableIfFloatingPoint<T>> 
[[nodiscard]] auto generate_uniform_real_sample(Ssize size, T lo, T hi, 
                                                RandEngineType rand_engine_type = RandEngineType::mt_19937) 
                    -> std::vector<T>;

/*! Generate a random sample of integer numbers from a uniform distribution, in the interval[lo, hi].
    \tparam T  The integer number type
    \param[in] size  The size of the sample to be generated
    \param[in] lo  The lower bound of the distribution; the generated numbers will be greater or equal to it  
    \param[in] hi  The upper bound of the distribution; the generated numbers will smaller or equal to it  
    \param[in] rand_engine_type  The pseudo-random number generator engine type to be used; an engine object
                                 will be created for generating the sample
    \return  The generated sample
 */
template<class T,
         class = EnableIfInteger<T>> 
[[nodiscard]] auto generate_uniform_int_sample(Ssize size, T lo, T hi, 
		                                       RandEngineType rand_engine_type = RandEngineType::mt_19937) 
                    -> std::vector<T>;

/*! Generate a random sample of floating-point numbers from a uniform distribution, in the interval [lo, hi).
    \tparam T  The floating-point number type
    \tparam Generator  Uniform random bit generator type; expected to satisfy the requirements of 
                       "UniformRandomBitGenerator"
    \param[in] size  The size of the sample to be generated
    \param[in] lo  The lower bound of the distribution; the generated numbers will be greater or equal to it  
    \param[in] hi  The upper bound of the distribution; the generated numbers will smaller than it  
    \param[in] gen  Uniform random bit generator 
    \return  The generated sample
 */
template<class T, 
         class Generator,
		 class = EnableIfFloatingPoint<T>> 
[[nodiscard]] auto generate_uniform_real_sample(Ssize size, T lo, T hi, Generator& gen) -> std::vector<T>;

/*! Generate a random sample of integer numbers from a uniform distribution, in the interval [lo, hi].
    \tparam T  The integer number type
    \tparam Generator  Uniform random bit generator type; expected to satisfy the requirements of 
                       "UniformRandomBitGenerator"
    \param[in] size  The size of the sample to be generated
    \param[in] lo  The lower bound of the distribution; the generated numbers will be greater or equal to it  
    \param[in] hi  The upper bound of the distribution; the generated numbers will smaller or equal to it  
    \param[in] gen  Uniform random bit generator 
    \return  The generated sample
 */
template<class T, 
         class Generator,
		 class = EnableIfInteger<T>> 
[[nodiscard]] auto generate_uniform_int_sample(Ssize size, T lo, T hi, Generator& gen) -> std::vector<T>;

/*! Generate a random sample of numbers from a uniform distribution, in the interval [lo, hi] if generating integral
    values, or in the interval [lo, hi) if generating floating-point values.
    \note  Many implementations will occassionally generate the hi value for floating-point values too.
    \tparam T  The number type
    \tparam Generator  Uniform random bit generator type; expected to satisfy the requirements of 
                       "UniformRandomBitGenerator"
    \param[in] size  The size of the sample to be generated
    \param[in] lo  The lower bound of the distribution; the generated numbers will be greater or equal to it  
    \param[in] hi  The upper bound of the distribution; the generated numbers will smaller than it  
    \param[in] gen  Uniform random bit generator 
    \return  The generated sample
 */
template<class T, 
         class Generator,
		 class = EnableIfNumber<T>> 
[[nodiscard]] auto generate_uniform_sample(Ssize size, T lo, T hi, Generator& gen) -> std::vector<T>;

/*! Generate a random number from a random distribution which allows generation in the interval [min, max) while 
    guaranteeing that the generated number is in (min, max).
    \tparam RandDistr  The random distribution class; expected to satisfy the requirements of 
                       "RandomNumberDistribution"
    \tparam Generator  The uniform random bit generator class; expected to satisfy the requirements of 
                       "UniformRandomBitGenerator"
    \param[in] rand_distr  The random distribution
    \param[in] gen  Uniform random bit generator 
    \return  The generated random number
 */
template<class RandDistr, class Generator>
[[nodiscard]] auto generate_rand_value_greater_than_min(RandDistr& rand_distr, Generator& gen) 
                    -> typename RandDistr::result_type;
} 

#include "twist/math/random_utils.ipp"

#endif 

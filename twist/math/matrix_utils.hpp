/// @file matrix_utils.hpp
/// Utilities for working with matrix classes

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_MATH_MATRIX__UTILS_HPP
#define TWIST_MATH_MATRIX__UTILS_HPP

namespace twist::math {

/*! Compare the values in the cells of a matrix with those in a range. The size of the range is expected to match the
    number of cells in the matrix. The comparison is performed row-wise, starting from the top left of the matrix, 
	using operator==.
	\tparam Mat  The matrix type (not all matrix classes are yet supported)
	\tparam Rng  The range type
	\return  true if all values match
 */
template<class Mat, rg::input_range Rng>
[[nodiscard]] auto equal_to_range(const Mat& matrix, const Rng& range) -> bool;

/*! Remove the rows in the matrix \p matrix whose values satisfy the predicate \p pred.
	\tparam Mat  The matrix type (not all matrix classes are yet supported)
	\tparam Pred  The predicate type; its parameter is the set of values in the row, starting from the left
	\return  The number of rows removed
 */
template<class Mat, class Pred>
requires std::is_invocable_r_v<bool, Pred, std::vector<typename Mat::Value>>
[[nodiscard]] auto remove_rows_if(Mat& matrix, Pred pred) -> Ssize;
}

#include "matrix_utils.ipp"

#endif 


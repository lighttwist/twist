/// @file EnumDistrDiscrete.ipp
/// Inline implementation file for "EnumDistrDiscrete.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/math/numeric_utils.hpp"

namespace twist::math {

// --- EnumDistrDiscrete<Enum>::EnumStateList class ---

template<typename Enum>
auto EnumDistrDiscrete<Enum>::EnumStateList::insert_state(Enum value, double prob) -> void
{
	TWIST_CHECK_INVARIANT
	state_list_.insert_state(static_cast<double>(value), prob);
}

template<typename Enum>
auto EnumDistrDiscrete<Enum>::EnumStateList::get_state_at(Ssize state_pos) const -> std::pair<Enum, double>
{
	TWIST_CHECK_INVARIANT
	const auto [value, prob] = state_list_.get_state_at(state_pos);
	return {static_cast<Enum>(value), prob};
}

template<typename Enum>
auto EnumDistrDiscrete<Enum>::EnumStateList::get_size() const -> Ssize
{
	TWIST_CHECK_INVARIANT
	return state_list_.get_size();
}

template<typename Enum>
auto EnumDistrDiscrete<Enum>::EnumStateList::get_prob_sum() const -> double
{
	TWIST_CHECK_INVARIANT
	return state_list_.get_prob_sum();
}

template<typename Enum>
auto EnumDistrDiscrete<Enum>::EnumStateList::adjust_last_state_prob() -> double 
{
	TWIST_CHECK_INVARIANT
	return state_list_.adjust_last_state_prob();
}

template<typename Enum>
auto EnumDistrDiscrete<Enum>::EnumStateList::delete_state(Ssize state_pos) -> void
{
	TWIST_CHECK_INVARIANT
	state_list_.delete_state(state_pos);
}

template<typename Enum>
auto EnumDistrDiscrete<Enum>::EnumStateList::states() const -> std::vector<std::pair<Enum, double>>
{
	TWIST_CHECK_INVARIANT
	return state_list_.states() | vw::transform([](const auto& el) { 
									  return std::pair{static_cast<Enum>(el.first), el.second}; 
								  })
	                            | rg::to<std::vector>();
}

template<typename Enum>
auto EnumDistrDiscrete<Enum>::EnumStateList::get_state_pos(Enum state_value) const -> Ssize
{
	TWIST_CHECK_INVARIANT
	return state_list_.get_state_pos(static_cast<double>(state_value));
}

template<typename Enum>
auto EnumDistrDiscrete<Enum>::EnumStateList::clear() -> void 
{
	TWIST_CHECK_INVARIANT
	state_list_.clear();
}

#ifdef _DEBUG
template<typename Enum>
void EnumDistrDiscrete<Enum>::EnumStateList::check_invariant() const noexcept
{
	//+TODO: Check uniqueness of state values?  Dan 16Jun'22
}
#endif 

// --- EnumDistrDiscrete<Enum> class ---

template<typename Enum>
EnumDistrDiscrete<Enum>::EnumDistrDiscrete(EnumStateList states)
	: DistrDiscrete{std::move(states.state_list_)}
{
	TWIST_CHECK_INVARIANT
}

template<class Enum>
auto EnumDistrDiscrete<Enum>::getClone() const -> EnumDistrDiscrete* 
{	
	TWIST_CHECK_INVARIANT
	auto clone_states = EnumStateList{};
	get_states(clone_states.state_list_);
	return new EnumDistrDiscrete{std::move(clone_states)};
}

template<class Enum>
auto EnumDistrDiscrete<Enum>::get_state_prob_for_value(Enum state_value) const -> double
{
	TWIST_CHECK_INVARIANT
	assert(is_valid(state_value));
	return DistrDiscrete::get_state_prob_for_value(static_cast<double>(state_value));
}

template<class Enum>
auto EnumDistrDiscrete<Enum>::get_state_enum_value_at(Ssize state_idx) const -> Enum
{
	TWIST_CHECK_INVARIANT
	return static_cast<Enum>(get_state_value_at(state_idx));
}

template<class Enum>
auto EnumDistrDiscrete<Enum>::is_state(Enum state_value) const -> bool
{
	TWIST_CHECK_INVARIANT
	assert(is_valid(state_value));
	return DistrDiscrete::is_state(static_cast<double>(state_value));
}

template<class Enum>
auto EnumDistrDiscrete<Enum>::get_state_idx(Enum state_value) const -> Ssize
{
	TWIST_CHECK_INVARIANT
	assert(is_valid(state_value));
	return DistrDiscrete::get_state_idx(static_cast<double>(state_value));
}

template<class Enum>
inline auto EnumDistrDiscrete<Enum>::inv_cdf_enum_value(double p) const -> Enum
{
	TWIST_CHECK_INVARIANT
	const auto value = DistrDiscrete::invCdf(p);
	assert(round(value) == value);
	const auto state_value = static_cast<Enum>(round_to_int(value));
	assert(is_valid(state_value));
	return state_value;
}

#ifdef _DEBUG
template<typename Enum>
void EnumDistrDiscrete<Enum>::check_invariant() const noexcept
{
	//+TODO: Check uniqueness of state values?  Dan 16Jun'22
}
#endif 

} 

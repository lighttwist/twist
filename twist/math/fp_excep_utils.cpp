/// @file fp_excep_utils.cpp
/// Implementation file for "fp_excep_utils.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/math/fp_excep_utils.hpp"

namespace twist::math {

// --- FPExceptionEnabler class ---

FPExceptionEnabler::FPExceptionEnabler(unsigned int enable_bits)
{
    // Retrieve the current state of the exception flags. This must be done before changing them. 
	// _MCW_EM is a bit mask representing all available exception masks.
    _controlfp_s(&old_bits_, _MCW_EM, _MCW_EM);

    // Make sure no non-exception flags have been specified, to avoid accidental changing of rounding 
	// modes, etc.
    enable_bits &= _MCW_EM;

    // Clear any pending FP exceptions. This must be done prior to enabling FP exceptions since otherwise 
	// there may be a 'deferred crash' as soon the exceptions are enabled.
    _clearfp();

    // Zero out the specified bits, leaving other bits alone.
    _controlfp_s(0, ~enable_bits, enable_bits);
}

FPExceptionEnabler::~FPExceptionEnabler()
{
    // Reset the exception state
    _controlfp_s(0, old_bits_, _MCW_EM);
}

// --- FPExceptionDisabler class ---

FPExceptionDisabler::FPExceptionDisabler()
{
    // Retrieve the current state of the exception flags. This must be done before changing them. 
	// _MCW_EM is a bit mask representing all available exception masks.
    _controlfp_s(&old_bits_, _MCW_EM, _MCW_EM);

    // Set all of the exception flags, which suppresses FP exceptions on the x87 and SSE units.
    _controlfp_s(0, _MCW_EM, _MCW_EM);
}

FPExceptionDisabler::~FPExceptionDisabler()
{
    // Clear any pending FP exceptions. This must be done prior to enabling FP exceptions since otherwise 
	// there may be a 'deferred crash' as soon the exceptions are enabled.
    _clearfp();

    // Reset (possibly enabling) the exception status.
    _controlfp_s(0, old_bits_, _MCW_EM);
}

}

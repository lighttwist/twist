/// @file UnsquareSpaceGrid.hpp
/// UnsquareSpaceGrid class definition. 

//  Copyright (c) 2007-2024, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_MATH_UNSQUARE_SPACE_GRID_HPP
#define TWIST_MATH_UNSQUARE_SPACE_GRID_HPP

#include "twist/math/geometry.hpp"

namespace twist::math {

/*! A space grid, ie a matrix of rectangular cells of non-zero size. The cell indexing is the same as it would be in a 
    matrix. The grid is flat, and unit-neutral, ie its unit could be anything expressible as numbers. The grid uses the 
	mathematical coordinate system: smaller is left and down.
    \tparam Coord  The coordinate type (numeric)
 */
template<class Coord>
class UnsquareSpaceGrid {
	static_assert(is_number<Coord>, "Coord must be a numeric type.");
public:
	/*! Constructor. 
	    \param[in] left  The left coordinate of the grid perimeter
	    \param[in] bottom  The bottom coordinate of the grid perimeter
	    \param[in] cell_width  The cell width
	    \param[in] cell_height  The cell height
	    \param[in] nof_rows  The number of rows
	    \param[in] nof_cols  The number of columns
	 */
	UnsquareSpaceGrid(Coord left, Coord bottom, Coord cell_width, Coord cell_height, Ssize nof_rows, Ssize nof_cols);

	//! The number of rows in the grid.
    [[nodiscard]] auto nof_rows() const -> Ssize;
	
	//! The number of columns in the grid.
	[[nodiscard]] auto nof_columns() const -> Ssize;

	//! The size of a cell's horizontal side in the spatial units used by the grid coordinates.
	[[nodiscard]] auto cell_width() const -> Coord;

	//! The size of a cell's vertical side in the spatial units used by the grid coordinates.
	[[nodiscard]] auto cell_height() const -> Coord;

	//! The X coordinate of the left side of the grid perimeter.
	[[nodiscard]] auto left() const -> Coord;
	
	//! The Y coordinate of the bottom side of the grid perimeter.
	[[nodiscard]] auto bottom() const -> Coord;
	
	//! The X coordintate of the right side of the grid perimeter.
	[[nodiscard]] auto right() const -> Coord;
	
	//! The Y coordinate of the top side of the grid perimeter.
	[[nodiscard]] auto top() const -> Coord;

private:
	Coord left_;
	Coord bottom_;
	Coord cell_width_;
	Coord cell_height_;
	Ssize nof_rows_;
	Ssize nof_cols_;

	TWIST_CHECK_INVARIANT_DECL
};

// -- Non-member functions ---

//! Get the perimeter rectangle of the space grid \p a grid with coordinate type \p Coord.
template<class Coord>
[[nodiscard]] auto perimeter_rect(const UnsquareSpaceGrid<Coord>& grid) -> Rectangle<Coord>;

}

#include "twist/math/UnsquareSpaceGrid.ipp"

#endif

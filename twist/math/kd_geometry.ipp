/// @file kd_geometry.ipp
/// Inline implementation file for "kd_geometry.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/IndexRange.hpp"
#include "twist/meta_misc_traits.hpp"
#include "twist/math/random_utils.hpp"

#include <random>

namespace twist::math {

// --- KdPoint class ---

template<int k, class Coord> 
template<class... Coords>
KdPoint<k, Coord>::KdPoint(Coords... coords)
{
	static_assert(sizeof...(Coords) == k, "Wrong number of coordinates passed in for k dimensions.");
	static_assert((is_safe_numeric_cast<Coords, Coord> && ...), 
			      "Input coordinate type cannot be safely cast to the tree node coordinate type.");

	auto i = 0;
	((coords_[i++] = coords), ...);
	TWIST_CHECK_INVARIANT
}

template<int k, class Coord>
KdPoint<k, Coord>::KdPoint(const std::array<Coord, k>& coords)
{
	TWIST_CHECK_INVARIANT
	rg::copy(coords, begin(coords_));
}

template<int k, class Coord>
auto KdPoint<k, Coord>::operator[](int dim) const -> Coord
{
	TWIST_CHECK_INVARIANT
	assert(dim < k);
	return coords_[dim];
}

template<int k, class Coord>
auto KdPoint<k, Coord>::coords() const -> std::array<Coord, k>
{
	TWIST_CHECK_INVARIANT
	return coords_;
}

#ifdef _DEBUG
template<int k, class Coord>
auto KdPoint<k, Coord>::check_invariant() const noexcept -> void
{
	assert(ssize(coords_) == k);
}
#endif

// -- Free functions ---

template<int k, class Coord> 
auto operator==(const KdPoint<k, Coord>& lhs, const KdPoint<k, Coord>& rhs) -> bool
{
	return std::equal(begin(lhs.coords_), end(lhs.coords_), begin(rhs.coords_));
}

template<int k, class Coord> 
auto euclidian_distance_squared(const KdPoint<k, Coord>& pt1, const KdPoint<k, Coord>& pt2) -> Coord
{
	auto dist = Coord{0};
    for (auto dim : IndexRange{k}) {
        const auto d = pt1[dim] - pt2[dim];
        dist += d * d;
    }
    return dist;
}

template<int k, class Coord, rg::input_range PointRange>
auto get_nearest_point_linear(const PointRange& points, 
                              const KdPoint<k, Coord>& test_point, 
                              bool ensure_unique) -> std::tuple<KdPoint<k, Coord>, twist::Ssize, Coord>
{
	auto get_distance_squared = [test_point](const auto& pt) { return euclidian_distance_squared(pt, test_point); };
	const auto distances_squared = vw::transform(points, get_distance_squared) | rg::to<std::vector>();
	const auto [min_distance_squared_it, _] = rg::minmax_element(distances_squared);
	const auto min_distance_squared = *min_distance_squared_it;
	if (ensure_unique) {
		// Make sure that only one point in the cloud is at the minimum distance to the test point
		if (rg::count(distances_squared, min_distance_squared) > 1) {
			TWIST_THRO2(L"More than one point in the cloud is at the minimum distance ({}) from the test point.",
			            min_distance_squared);		
		}
	}	
	const auto nearest_pt_cloud_idx = distance(distances_squared.begin(), min_distance_squared_it);
	const auto nearest_pt = *(points.begin() + nearest_pt_cloud_idx);
	
	return {nearest_pt, nearest_pt_cloud_idx, min_distance_squared};
}

template<int k, class Coord>
auto generate_kd_point_cloud(Ssize nof_points, const std::vector<ClosedInterval<Coord>>& bounds_per_dim) 
      -> std::vector<KdPoint<k, Coord>>
{
	if (ssize(bounds_per_dim) != k) { 
		TWIST_THRO2(L"The number of bounds {} does not match the number of dimensions {}.", ssize(bounds_per_dim), k); 
	}

    auto rd = std::random_device{};
    auto gen = std::mt19937{rd()}; 
	
	auto coords_per_dimension = reserve_vector<std::vector<Coord>>(k);
	for (const auto& bounds : bounds_per_dim) {
		coords_per_dimension.push_back(generate_uniform_sample(nof_points, bounds.lo(), bounds.hi(), gen));
	}

	auto cloud = reserve_vector<KdPoint<k, Coord>>(nof_points);

	auto coords = std::array<Coord, k>{};
	const auto dim_index_range = IndexRange{k};
	for (auto i : IndexRange{nof_points}) {
		for (auto dim : dim_index_range) {
			coords[dim] = coords_per_dimension[dim][i];
		}
		cloud.emplace_back(coords);
	}

	return cloud;
}

}

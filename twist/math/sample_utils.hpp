///  @file  sample_utils.hpp
///  Global utilities for working with random variable samples

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_MATH_SAMPLE__UTILS_HPP
#define TWIST_MATH_SAMPLE__UTILS_HPP

#include "MultivarDistrSample.hpp"
#include "histogram_utils.hpp"
#include "SymmetricMatrix.hpp"
#include "UnivarDistrSample.hpp"
#include "UnivarDistrSampleInfo.hpp"

namespace twist::math {

/// Method for dealing with tied ranks
enum class TiedRankMethod {
	/// The ranks of the tied sample values are set to the average of the tied values' positions within the 
	/// sorted sample
	average = 1,
	/// The ranks of the tied sample values are set to the highest of the tied values' positions within the 
	/// sorted sample
	bracket = 2
};

/// Calculate the mean of a given sample.
///
/// @tparam  Iter  Forward iterator type, for addressing the sample values
/// @param[in] sample_begin  Iterator addressing the first value in the sample
/// @param[in] sample_end  Iterator addressing one past the final value in the sample
/// @return  The mean
///
template<typename Iter>
double calc_sample_mean(Iter sample_begin, Iter sample_end);

/// Calculate the mean of a given sample.
///
/// @tparam  Rng  The type of the range of sample values
/// @param[in] sample  The range of sample values
/// @return  The mean
///
template<class Rng,
		class = EnableIfNumberRange<Rng>>
double calc_sample_mean(const Rng& sample);

/// Calculate a few useful parameters for a given sample.
///
/// @tparam  Iter  Forward iterator type, for addressing the sample values
/// @param[in] sample_begin  Iterator addressing the first value in the sample
/// @param[in] sample_end  Iterator addressing one past the final value in the sample
/// @param[in] mean  The sample mean
/// @param[in] stddev  The sample standard deviation
///
template<class Iter>
void calc_sample_params(Iter sample_begin, Iter sample_end, double& mean, double& stddev); 

/// Calculate a few useful parameters for a given sample.
///
/// @tparam  Rng  The type of the range of sample values
/// @param[in] sample  The range of sample values
/// @param[in] mean  The sample mean
/// @param[in] stddev  The sample standard deviation
///
template<class Rng,
		class = EnableIfNumberRange<Rng>>
void calc_sample_params(const Rng& sample, double& mean, double& stddev); 

/// Calculate a few useful parameters for a given sample.
///
/// @tparam  Sam  The type of a sample value
/// @tparam  Rng  The type of the range of sample values
/// @param[in] sample  The range of sample values
/// @param[in] mean  The sample mean
/// @param[in] stddev  The sample standard deviation
/// @param[in] min  The minimum value
/// @param[in] max  The maximum value
///
template<class Sam, 
         class Rng,
		 class = EnableIfNumberRange<Rng>>
void calc_sample_params(const Rng& sample, double& mean, double& stddev, Sam& min, Sam& max); 

/// Find the range (min and max) of a sample.
///
/// @tparam  Iter  Forward iterator type, for addressing the sample values.
/// @param[in] sample_begin  Iterator addressing the first value in the sample.
/// @param[in] sample_end  Iterator addressing one past the final value in the sample.
/// @param[in] min  The minimum sample value.
/// @param[in] max  The maximum sample value.
/// 
template<class Sam, typename Iter>
void get_sample_range(Iter sample_begin, Iter sample_end, Sam& min, Sam& max);

/// Analyse a sample and create an object describing a few of its characteristics.
///
/// @param[in] sample  The sample.
/// @param[in] discrete  Whether the sample is drawn from a discrete distribution.
/// @return  Object containing information about the sample.
/// 
template<class Sam> 
std::unique_ptr<UnivarDistrSampleInfo<Sam>> get_sample_info(const UnivarDistrSample<Sam>& sample, 
		bool discrete);

/// Analyse a discrete sample (i.e. a sample whose associated random variable is discrete) and find out the - 
/// distinct - discrete values in the sample.
///
/// @tparam  Iter  Forward iterator type, for addressing the sample values
/// @tparam  Set  The type of the set to be populated with the state values; it probably should remove 
///					duplicates
/// @param[in] sample_begin  Iterator addressing the first value in the sample
/// @param[in] sample_end  Iterator addressing one past the final value in the sample
/// @param[in] state_values  Set that will be populated with the state values
///
template<typename Iter, class Set>
void get_discrete_sample_states(Iter sample_begin, Iter sample_end, Set& state_values);

/// Analyse a discrete sample (i.e. a sample whose associated random variable is discrete) and find out the - 
/// distinct - discrete values in the sample, and their frequencies.
///
/// @tparam  Iter  Forward iterator type, for addressing the sample values
/// @tparam  Map  The type of the map to be populated with the state values, as keys, and their respective 
///					frequencies, as values. It probably should remove duplicate keys
/// @param[in] sample_begin  Iterator addressing the first value in the sample
/// @param[in] sample_end  Iterator addressing one past the final value in the sample
/// @param[in] state_map  Map populated with the discrete state values, as keys, and their respective 
///					frequencies, as values
///
template<typename Iter, class Map>
void get_discrete_sample_states_and_freqs(Iter sample_begin, Iter sample_end, Map& state_map);

/// Calculate a specific quantile for a given, sorted, sample. 
/// Note: The sample is expected to be sorted, if it is not the result will be wrong.
///
/// @tparam  Iter  Forward iterator type, for addressing the sample values
/// @param[in] sample_begin  Iterator addressing the first value in the sample
/// @param[in] sample_end  Iterator addressing one past the final value in the sample
/// @param[in] prob  Probability realized by the returned quantile.
/// @return  The quantile from sample.
///
template<class Iter>
double calc_quantile(Iter sample_begin, Iter sample_end, double prob); 

/// Calculate a specific quantile for a given, sorted, sample. 
/// Note: The sample is expected to be sorted, if it is not the result will be wrong.
///
/// @tparam  Rng  The type of the range of sample values
/// @param[in] sample  The range of sample values
/// @param[in] prob  Probability realized by the returned quantile.
/// @return  The quantile from sample.
///
template<class Rng,
         class = EnableIfNumberRange<Rng>>
double calc_quantile(const Rng& sample, double prob); 

/// Locate a specific value within a given, sorted, sample. If the value is within the sample bounds, the two 
///   "standard" bounding quantiles are returned. The quantile step used for the investigation is 0.5. 
/// Note: The sample is expected to be sorted, if it is not the results will be wrong.
///
/// @tparam  Rng  The type of the range of sample values
/// @param[in] sample  The range of sample values
/// @param[in] value  The value.
/// @param[in] lo_quantile  The lower bounding quantile. 
/// @param[in] hi_quantile  The higher bounding quantile. 
/// @return  true only if the value is within the sample bounds.
///
template<class Rng,
		 class = EnableIfNumberRange<Rng>>
bool get_bounding_quantiles(const Rng& sample, double value, double& lo_quantile, double& hi_quantile); 

/// Calculate the parameters needed for the histogram of a sample of a continuous random variable.
/// This routine will not work for samples of discrete random variables.
/// This routine is slightly less efficient than the overload which takes the sample min and max, as it will 
/// need to find these values first.
///
/// @tparam  Sam  The type of a sample value
/// @param[in] sample  The sample to be evaluated for the histogram
/// @param[in] num_bins  The number of bins in the histogram  
/// @return  The histogram data
///
template<class Sam>
HistogramData calc_cont_histogram_params(const UnivarDistrSample<Sam>& sample, size_t num_bins); 

/// Calculate the parameters needed for the histogram of a sample of a continuous random variable.
/// This routine will not work for samples of discrete random variables.
///
/// @tparam  Sam  The type of a sample value.
/// @param[in] sample  The sample to be evaluated for the histogram
/// @param[in] num_bins  The number of bins in the histogram  
/// @param[in] sample_min  The minimum value in the sample
/// @param[in] sample_max  The maximum value in the sample
/// @return  The histogram data
///
template<class Sam>
HistogramData calc_cont_histogram_params(const UnivarDistrSample<Sam>& sample, size_t num_bins, 
		Sam sample_min, Sam sample_max); 

/// Compute the product moment correlation between two random variables X and Y, described by two correlated samples.
///
/// @tparam  Iter  Forward iterator type, for addressing the sample values.
/// @param[in] sampleBeginX  Iterator addressing the first value in the sample for X.
/// @param[in] sampleEndX  Iterator addressing one past the final value in the sample for X.
/// @param[in] sampleBeginY  Iterator addressing the first value in the sample for Y.
/// @param[in] sampleEndY  Iterator addressing one past the final value in the sample for Y.
///
template<typename Iter> 
double compute_pmc_between_samples(Iter sampleBeginX, Iter sampleEndX, Iter sampleBeginY, Iter sampleEndY);

/// Rank a sample, i.e. obtain a vector where each of the sample's values is replaced by its "rank". The "rank" of a sample 
/// value is its position within the sorted sample, or, if it is part of a "tie" (a number of identical sample values), its 
/// rank is the average of the positions that these identical values have in the sorted sample.
///
/// @tparam  Sam  The type of a sample value (equivalent to the type of a sample vector element).
/// @tparam  Rnk  The type of a rank value. Should be a floating point type, as some ranks are averages!
/// @param[in] sample  The sample to be ranked.
/// @param[in] ranks  The vector of ranks. Must have the same size as the sample vector.
/// @param[in] valuePtrsBuffer  A buffer to be used by this routine to hold the sorted pointers to the sample 
///					values, ie the first pointer in the buffer will point to the lowest sample value, the last 
///					pointer in the array to the highest sample value, etc. This is passed in for efficiency 
///					purposes, but the caller may well find the output useful.
/// @param[in] tiedRankMethod  The method to be used for tied ranks.
///
template<class Sam, class Rnk> 
void rank_sample(const UnivarDistrSample<Sam>& sample, std::vector<Rnk>& ranks, 
		std::vector<const Sam*>& valuePtrsBuffer, TiedRankMethod tiedRankMethod); 

/// Calculate the rank correlation matrix of a joint distribution sample.
/// The order of the rows/columns in the matrix will match the order of the random variables in the joint 
///   distribution.
///
/// @param[in] sample  The joint distribution sample
/// @param[in] rank_corr_matrix  The rank correlation matrix; must have the correct size
///
template<class Sam, class VarId>
void calc_rank_corr_matrix(const MultivarDistrSample<Sam, VarId>& sample, 
		SymmetricMatrix<double>& rank_corr_matrix);

} 

#include "twist/math/sample_utils.ipp"

#endif 

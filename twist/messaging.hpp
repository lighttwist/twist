/// @file messaging.hpp
/// Utilities for synchronous, decoupled, safely-typed messaging; listeners can be registered for any number of message 
/// types with arbitraty associate data types, and messages are broadcast to registered listeners.

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_MESSAGING_HPP
#define TWIST_MESSAGING_HPP

#include "twist/Function.hpp"
#include "twist/type_info_utils.hpp"

namespace twist {

// A message ID is the type info for the message class
using MessageId = std::type_info;

//! Abstract base class for classes containing the data for one or more specific message types.
class MessageDataBase {
public:
	virtual ~MessageDataBase() = 0;

protected:
	MessageDataBase();
};

/*! Message class template, templated on message class and message data class. It cannot be instantiated.
    Do not use this class directly, use instead the helper macro TWIST_DEF_MESSAGE().
 */
template<class MsgT, class MsgDataT>
class MessageBase {
public:	
	using Data = MsgDataT;
	using Listener = Function2<void, const MessageId&, const MsgDataT&>;

	MessageBase() 
	{ 
		static_assert(always_false<MsgT>, "Messages classes cannot be instantiated."); 
	}
	
	static const MessageId& id() 
	{ 
		return typeid(MsgT); 
	}
	
	static_assert(std::is_base_of_v<MessageDataBase, MsgDataT>, 
			      "The message data class must be derived from class MessageDataBase.");		
};

//! Partial message class template specialisation for messages which have no associated data.
template<class MsgT>
class MessageBase<MsgT, void> {
public:	
	using Data = void;
	using Listener = Function1<void, const MessageId&>;
	
	static const MessageId& id()  
	{ 
		return typeid(MsgT); 
	}
};

/*! Helper macro for defining a new message class.
    \param[in] MessageClass  The new message class name (the name mustn't be used)
    \param[in] MessageDataClass  The data class associated with the message (must be derived from MessageDataBase), or 
	                             void if there is none
 */
#define TWIST_DEF_MESSAGE(MessageClass, MessageDataClass)  \
		        class MessageClass : public MessageBase<MessageClass, MessageDataClass> {};

/*! Generic messenger class, used for registering listeners to any number of messages and broadcasting messages to 
    registered listeners. You can think of it as facilitating a more elegant, decoupled version of the "observer" 
	pattern.
 */
class Messenger {
public:
	/*! Add a class member function as a listener for a specific message. If the same listener is already registered 
	    for the same message, the listener will not be registered again.
	    \tparam MsgT  The message type
	    \tparam MemFuncT  The member function type
	    \param[in] ObjT  The class of the object on which the member function is to be called
	    \param[in] mem_func  Pointer to the member function 
	    \param[in] obj  The object on which the member function is to be called
	    \return  true if the listener is registered successfully
	 */ 	
	template<class MsgT, typename MemFuncT, class ObjT> 
	bool add_listener(MemFuncT mem_func, ObjT& obj);

	/*! Add a static function as a listener for a specific message. If the same listener is already registered for the 
	    same message, the listener will not be registered again.
	    \tparam MsgT  The message type
	    \tparam StaticFuncT  The static function type
	    \param[in] static_func  Pointer to teh static function 
	    \return  true if the listener is registered successfully
	 */ 	
	template<class MsgT, typename StaticFuncT> 
	bool add_listener(StaticFuncT static_func);

	/*! Remove a class member function listener registered a specific message. 
	    \tparam MsgT  The message type
	    \tparam MemFuncT  The member function type
	    \param[in] ObjT  The class of the object on which the member function is to be called
	    \param[in] mem_func  Pointer to the member function 
	    \param[in] obj  The object on which the member function is to be called
	    \return  true if the listener was found to be registered for that message and was unregistered successfully 
	 */ 
	template<class MsgT, typename MemFuncT, class ObjT> 
	bool remove_listener(MemFuncT mem_func, ObjT& obj);

	/*! Remove a static function listener registered a specific message. 
	    \tparam MsgT  The message type
	    \tparam StaticFuncT  The static function type
	    \param[in] static_func  Pointer to the static function 
	    \return  true if the listener was found to be registered for that message and was unregistered successfully 
	 */
	template<class MsgT, typename StaticFuncT> 
	bool remove_listener(StaticFuncT static_func);

	/*! Broadcast a message (which has an associated data class) to all listeners registered for that message 
	    \tparam MsgT  The message type
	    \param[in] msg_data  The message data
	 */
	template<class MsgT> 
	void broadcast_message(const typename MsgT::Data& msg_data);

	/*! Broadcast a message (which has no associated data class) to all listeners registered for that message 
	    \tparam MsgT  The message type
	 */
	template<class MsgT> 
	void broadcast_message();

	TWIST_NO_COPY_NO_MOVE(Messenger)

private:	
	//! Abstract base class for wrappers for message listeners 
	class ListenerWrapperBase {
	public:
		ListenerWrapperBase();
		
		virtual ~ListenerWrapperBase() = 0;
		
		virtual auto call(const MessageId& msg_id, const MessageDataBase& msg_data) -> void;
		
		virtual auto call(const MessageId& msg_id) -> void;
	};

	//! Wrapper for message listeners 
	template<class MsgDataT>
	class ListenerWrapper : public ListenerWrapperBase {
	public:
		using Listener = Function2<void, const MessageId&, const MsgDataT&>;

		ListenerWrapper(Listener func);
		
		virtual ~ListenerWrapper();
		
		virtual auto call(const MessageId& msg_id, const MessageDataBase& msg_data) -> void;
		
		[[nodiscard]] auto compare(const ListenerWrapper& other) const -> bool;
	
	private:
		Listener func_;
	};

	//! Explicit specialisation of the wrapper for message listeners, for messages with no associated data
	template<>
	class ListenerWrapper<void> : public ListenerWrapperBase {
	public:
		using Listener = Function1<void, const MessageId&>;
		
		ListenerWrapper(Listener func);
		
		virtual void call(const MessageId& msg_id);
		
		bool compare(const ListenerWrapper& other) const;
	
	private:
		Listener func_;
	};

	using ListenerWrapperMap = std::multimap<const MessageId*, std::unique_ptr<ListenerWrapperBase>, TypeInfoLess>;

	//! Internal helper for the  add_listener()  methods
	template<class MsgT> 
	auto internal_add_listener(typename MsgT::Listener listener) -> bool;

	//! Internal helper for the  remove_listener()  methods
	template<class MsgT> 
	auto internal_remove_listener(typename MsgT::Listener listener) -> bool;

	/*! Find a specific message listener functor in the listerener wrappers' map.
	    \tparam MsgT  The message type
	    \param[in] listener  The listener to look for
	    \return  Iterator addressing the wrapper of the matching listener, or the end of the map if no match is found.  
	 */
	template<class MsgT> 
	[[nodiscard]] auto find_listener(typename MsgT::Listener listener) -> ListenerWrapperMap::iterator;

	ListenerWrapperMap listeners_;

	TWIST_CHECK_INVARIANT_DECL
};

/*! Class which facilitates registering the member functions of a specific object (the "listener object") as message 
    listeners; when an instance of the class is destroyed, any listeners registered using that instance are 
	automatically unregistered.
    \tparam ListenerObj  The listener object type
 */
template<class ListenerObj>
class ScopedMessageRegistrar {
public:
	/*! Constructor.
	    \param[in] obj  The listener ojbect
	    \param[in] messenger  The messenger to be used for registering and unregistering listeners
	 */
	ScopedMessageRegistrar(ListenerObj& obj, Messenger& messenger);

	~ScopedMessageRegistrar();

	/*! Add a member function of the listener object as a listener for a specific message. If the same listener is 
	    already registered for the same message, the listener will not be registered again.
	    \tparam Msg  The message type
	    \tparam MemFunc  The member function type
	    \param[in] mem_func  Pointer to the member function 
	    \return  true if the listener is registered successfully
	 */ 	
	template<class Msg, typename MemFunc> 
	auto add(MemFunc mem_func) -> bool;

private:
	ListenerObj& obj_;
	Messenger& messenger_;
	std::vector<std::function<void ()>> listener_removers_;
};

} 

#include "twist/messaging.ipp"

#endif 


/// @file grid_utils.cpp
/// Implementation file for "grid_utils.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/grid_utils.hpp"

namespace twist {

// --- SubgridInfo class ---

SubgridInfo::SubgridInfo(Ssize begin_row, Ssize end_row, Ssize begin_col, Ssize end_col)
	: begin_row_{begin_row}
	, end_row_{end_row}
	, begin_col_{begin_col}
	, end_col_{end_col}
{
	TWIST_CHECK_INVARIANT
}

auto SubgridInfo::begin_row() const -> Ssize
{
	TWIST_CHECK_INVARIANT
	return begin_row_;
}

auto SubgridInfo::end_row() const -> Ssize
{
	TWIST_CHECK_INVARIANT
	return end_row_;
}

auto SubgridInfo::begin_column() const -> Ssize
{
	TWIST_CHECK_INVARIANT
	return begin_col_;
}

auto SubgridInfo::end_column() const -> Ssize
{
	TWIST_CHECK_INVARIANT
	return end_col_;
}

#ifdef _DEBUG
void SubgridInfo::check_invariant() const noexcept
{
	assert(begin_row_ >= 0 && begin_col_ >= 0);
	assert(begin_row_ <= end_row_ && begin_col_ <= end_col_);
}
#endif

// --- Free functions ---

auto nof_rows(const SubgridInfo& subgrid_info) -> Ssize
{
	return subgrid_info.end_row() - subgrid_info.begin_row() + 1;
}

auto nof_columns(const SubgridInfo& subgrid_info) -> Ssize
{
	return subgrid_info.end_column() - subgrid_info.begin_column() + 1;
}

auto nof_cells(const SubgridInfo& subgrid_info) -> Ssize
{
	return nof_rows(subgrid_info) * nof_columns(subgrid_info);
}

auto contains_cell(const SubgridInfo& subgrid_info, Ssize row, Ssize col) -> bool
{
	return row >= subgrid_info.begin_row() && row <= subgrid_info.end_row() && 
		   col >= subgrid_info.begin_column() && col <= subgrid_info.end_column();
}

auto dims(const SubgridInfo& subgrid_info) -> std::tuple<Ssize, Ssize>
{
	return {subgrid_info.end_row() - subgrid_info.begin_row() + 1, 
	        subgrid_info.end_column() - subgrid_info.begin_column() + 1};
}

auto row_range(const SubgridInfo& subgrid_info) -> IndexRange<Ssize>
{
    return IndexRange{subgrid_info.begin_row(), subgrid_info.end_row() + 1};
}

auto column_range(const SubgridInfo& subgrid_info) -> IndexRange<Ssize>
{
    return IndexRange{subgrid_info.begin_column(), subgrid_info.end_column() + 1};
}

auto grid_cell_index_from_row_col(Ssize row, Ssize col, Ssize row_count, Ssize col_count) -> GridCellIndex
{
	if (row >= row_count || col >= col_count) {
		TWIST_THRO2(L"Invalid grid cell row/col indexes ({}, {}).", row, col);
	}
	return GridCellIndex{row * col_count + col};	
}

auto grid_row_col_from_cell_index(GridCellIndex cell_index, Ssize row_count, Ssize col_count) 
      -> std::tuple<Ssize, Ssize>
{
	const auto cell_index_int = static_cast<Ssize>(cell_index);
	if (cell_index_int >= row_count * col_count) {
		TWIST_THRO2(L"Invalid grid cell index {}.", static_cast<Ssize>(cell_index));
	}
	const auto div_result = std::div(cell_index_int, col_count);
	return {div_result.quot, div_result.rem};
}

}

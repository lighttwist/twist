/// @file array_utils.hpp
/// Utilities for working with std::array

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_ARRAY__UTILS_HPP
#define TWIST_ARRAY__UTILS_HPP

#include <array>

namespace twist {

/// Get (at compile-time) the position of a specific element in a C-style array. The search runs in linear 
/// time.
///
/// @tparam  T  The array element type
/// @tparam  size  The array size
/// @param[in] arr  The array
/// @param[in] elem  The element to search for  
/// @return  The position of the first occurrence of the element within the array; or -1 if no match is found
///
template<class T, std::size_t size>
constexpr int get_pos(T (&arr)[size], T elem);

/// Find out (at compile-time) whether the elements in a C-style array are sorted in increasing order, ie no 
/// element compares as "smaller" than the element before it. 
///
/// @tparam  T  The array element type; it must support comparison with the "<" operator
/// @tparam  size  The array size
/// @param[in] arr  The array
/// @return  true if the array is sorted in increasing order
///
template<class T, std::size_t size>
constexpr int is_increasing(T (&arr)[size]);

/// Find out (at compile-time) whether the elements in a C-style array are sorted in strictly increasing 
/// order, ie no element compares as "not smaller" that the element before it. 
///
/// @tparam  T  The array element type; it must support comparison with the "<" operator
/// @tparam  size  The array size
/// @param[in] arr  The array
/// @return  true if the array is sorted in strictly increasing order
///
template<class T, std::size_t size>
constexpr int is_strictly_increasing(T (&arr)[size]);

/// Find (at compile-time) an element in a sorted C-style array, using a binary search. The average 
/// performance is O(log n).
///
/// @tparam  T  The array element type
/// @tparam  size  The array size
/// @param[in] arr  The array
/// @param[in] elem  The element to search for  
/// @return  The position of the occurrence of the matching element (or one of the matching elements if there 
///				are multiple, contiguous matching elements) within the array; or -1 if no match is found
///
template<class T, std::size_t size>
constexpr int binary_search(T (&arr)[size], T elem);
 
 namespace detail {

template<class> struct IsRefWrapper : std::false_type {};

template<class T> struct IsRefWrapper<std::reference_wrapper<T>> : std::true_type {};
 
template<class T>
using NotRefWrapper = std::negation<IsRefWrapper<std::decay_t<T>>>;
 
template <class D, class...> struct ReturnTypeHelper { using type = D; };

template <class... Types>
struct ReturnTypeHelper<void, Types...> : std::common_type<Types...> {
    static_assert(std::conjunction_v<NotRefWrapper<Types>...>,
                "Types cannot contain reference_wrappers when D is void"); 
				// Arrays of references are forbidden
};
 
template <class D, class... Types>
using ReturnType = std::array<typename ReturnTypeHelper<D, Types...>::type, sizeof...(Types)>;

template<class T, std::size_t N, std::size_t... I>
constexpr std::array<std::remove_cv_t<T>, N> to_array_impl(T (&a)[N], std::index_sequence<I...>)
{
    return { { a[I]... } };
}

}

/// Creates a std::array whose size is equal to the number of arguments and whose elements are initialized 
///   from the corresponding arguments. The return type is std::array<VT, sizeof...(Types)> (see below for the 
///   deduced type VT).
/// Devnote: currently an experimental function in the standard lib.
///
/// @tparam  D  If D is void, then the deduced type VT is std::common_type_t<Types...>; otherwise, it is D
/// @tparam  Types   The types of the arguments; if D is void and any of std::decay_t<Types>... is a 
///				specialization of std::reference_wrapper, the program is ill-formed
/// @tparam  The array elements
/// @return  The built std::array
///
template<class D = void, class... Types>
constexpr twist::detail::ReturnType<D, Types...> make_array(Types&&... t);

/// Create a std::array from a C-style array. The elements of the std::array are copy-initialized from the 
/// corresponding element of the C-style array passed in.
///
/// @tparam  T  The input array element type
/// @tparam  size  The input array size
/// @param[in] arr  The input array
/// @return  The built std::array
///
template<class T, std::size_t size>
constexpr std::array<std::remove_cv_t<T>, size> to_array(T (&arr)[size]);

}

#include "twist/array_utils.ipp"

#endif

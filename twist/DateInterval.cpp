/// @file DateInterval.cpp
/// Implementation file for "DateInterval.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/DateInterval.hpp"

namespace twist {

// --- DateInterval class ---

DateInterval::DateInterval()
{
	check();
	TWIST_CHECK_INVARIANT
}

DateInterval::DateInterval(const Date& lo, const Date& hi)
	: lo_{lo}
	, hi_{hi}
{
	check();
	TWIST_CHECK_INVARIANT
}

auto DateInterval::lo() const -> Date
{
	TWIST_CHECK_INVARIANT
	return lo_;
}

auto DateInterval::hi() const -> Date
{
	TWIST_CHECK_INVARIANT
	return hi_;
}

auto DateInterval::check() const -> void
{
	if (lo_ > hi_) {
		TWIST_THRO2(L"Invalid date interval {} to {}.", date_to_str(lo_, DateStrFormat::day_mon_year),
		                                                date_to_str(hi_, DateStrFormat::day_mon_year));
	}
}

#ifdef _DEBUG
void DateInterval::check_invariant() const noexcept
{
	assert(lo_ <= hi_);
}
#endif 

// --- Non-member functions ---

auto length_days(const DateInterval& date_interval) -> int
{
	return get_diff_in_days(date_interval.lo(), date_interval.hi()) + 1;
}

}

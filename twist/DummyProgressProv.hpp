///  @file  DummyProgressProv.hpp
///  DummyProgressProv class, inherits ProgressProv

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_DUMMY_PROGRESS_PROV_HPP
#define TWIST_DUMMY_PROGRESS_PROV_HPP

#include "ProgressProv.hpp"

namespace twist {

///
///  An implementation of  ProgressProv  which does absolutely nothing.
///
class DummyProgressProv : public ProgressProv {
public:
	/// Constructor.
	///
	DummyProgressProv() : ProgressProv() {}

	virtual void init_message_mode() override {}  //  ProgressProv  override
	
	virtual void init_hist_mode(long) override {}  //  ProgressProv  override

	virtual void set_prog_msg(std::wstring_view, bool) override {}  //  ProgressProv  override

	virtual void add_new_hist_line(const std::wstring&) override {}  //  ProgressProv  override

	virtual void set_present_hist_line(const std::wstring&) override {}  //  ProgressProv  override

	virtual void get_prog_range(Ssize&, Ssize&) const override {}  //  ProgProvBase  override

    virtual void hide() override {}  //  ProgressProv  override

	virtual void set_prog_range(Ssize, Ssize) override {}  //  ProgProvBase  override

	virtual Ssize get_prog_pos() const override { return 0; }  //  ProgProvBase  override

	virtual void set_prog_pos(Ssize, bool) override {}  //  ProgProvBase  override

	virtual void inc_prog_pos(bool) override {}  //  ProgProvBase  override
};

} 

#endif

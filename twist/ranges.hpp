/// @file ranges.hpp
/// The range concept and the Range class template, extensions to the STL algorithms taking range parameters and 
/// utilities for creating ranges 

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_RANGES_HPP
#define TWIST_RANGES_HPP

#include "twist/IndexRange.hpp"
#include "twist/meta_ranges.hpp"
#include "twist/math/ClosedInterval.hpp"

#include <ranges>

namespace twist {

//  template<class Rng>
//  concept Range {
//      { begin(Rng&&) } -> Iter; 
//      { end(Rng&&) } -> Iter;
//      { IsAnyInputIterator<Iter>::type };
//  };
//
//  The twist::Range class and all STL container classes satisfy the Range concept requirements.

///  Class representing a range as a pair of iterators.
///  It models the  Range  concept.
///  As such it is a level of abstraction that allows non-trivial (sub)ranges (eg the keys in a map) to be 
///    represented by one object, and be passed into functions which expect one range object.
///
/// @tparam  Iter  The iterator type; must satisfy the InputIterator requiremenents (as tested by the 
///				IsAnyInputIterator metafunction)
///
template<class Iter>
class Range {
	static_assert(IsAnyInputIterator<Iter>::value, 
			"The template argument does not satisfy the InputIterator concept requirements.");

public: 
	using const_iterator = Iter;
	using iterator = Iter;
	using value_type = typename std::iterator_traits<Iter>::value_type;
	using difference_type = typename std::iterator_traits<Iter>::difference_type;

	/// Constructor.
	///
	/// @param[in] first  Iterator addressing the first element in the range
	/// @param[in] last  Iterator addressing one past the final element in the range
	///
	Range(Iter first, Iter last) 
		: first_{ first }
		, last_{ last }
	{
		using IterCategory = typename std::iterator_traits<Iter>::iterator_category;
		if constexpr (std::is_same_v<IterCategory, std::random_access_iterator_tag>) {
			if (first > last) {
				TWIST_THROW(L"The first iterator must come before the last.");
			}
		}
	}

	/// Constructor; creates a range spanning all elements of a container, from beginning to end.
	///
	/// @tparam  Cont  The container type
	/// @param[in] cont  The container
	///
	template<class Cont> 
	explicit Range(const Cont& cont) 
		: first_{ std::begin(cont) }
		, last_{ std::end(cont) }
	{
	}

	/// Get the iterator addressing the first element in the range.
	///
	/// @return  The iterator
	///
	[[nodiscard]] Iter begin() const 
	{
		return first_;
	}

	/// Get the iterator addressing one past the final element in the range.
	/// If the range is empty, this function retruns a iterator equal to that returned by end().
	///
	/// @return  The iterator
	///
	[[nodiscard]] Iter end() const 
	{
		return last_;
	}

	/// Find out whether the range is empty (contains no elements).
	///
	/// @return  true if empty
	///
	[[nodiscard]] bool empty() const 
	{
		return first_ == last_;
	}

	/// Find out the size of the range (the number of elements it contains).
	///
	/// @return  The range size
	///
	[[nodiscard]] std::size_t size() const 
	{
		return std::distance(first_, last_);
	}

private:
	Iter  first_;
	Iter  last_;
};

/// Get an iterator addressing the first element in a Range<> object. 
/// If the range is empty, this function retruns a iterator equal to that returned by end().
///
/// @tparam  Iter  The iterator type
/// @param[in] range  The range
/// @return  The iterator
///
template<typename Iter> 
[[nodiscard]] Iter begin(const Range<Iter>& range)
{
	return range.begin();
}

/// Get an iterator addressing one pas the final element in a Range<> object. 
/// If the range is empty, this function retruns a iterator equal to that returned by begin().
///
/// @tparam  Iter  The iterator type
/// @param[in] range  The range
/// @return  The iterator
///
template<typename Iter> 
[[nodiscard]] Iter end(const Range<Iter>& range)
{
	return range.end();
}

/// Create a twist::Range instance from a pair of iterators.
///
/// @tparam  Iter  The iterator type
/// @param[in] first  Iterator addressing the first element in the range
/// @param[in] last  Iterator addressing one past the final element in the range
/// @return  The range object
///
template<typename Iter> 
[[nodiscard]] Range<Iter> make_range(Iter first, Iter last)
{
	return Range<Iter>{ first, last };
}

/// Get an iterator addressing the element at a specific position in a range.
///
/// @tparam  Rng  The range typeconcept
/// @param[in] range  The range
/// @param[in] pos  The position of the desired element
/// @return  The iterator
///
template<class Rng, 
		 class = EnableIfAnyRange<Rng>> 
[[nodiscard]] auto iter_at(const Rng& range, Ssize pos);

/// Get the element at a specific position within a range.
///
/// @tparam  Rng  The range type
/// @param[in] range  The range 
/// @param[in] pos  The position of the desired element; undefined behaviour if it is invalid
/// @return  The element
///
template<class Rng,
		 class = EnableIfAnyRange<Rng>> 
[[nodiscard]] RangeElementCref<Rng> elem_at(const Rng& range, Ssize pos);   

/// Get the last element in a range. Undefined behavious if the range is empty.
///
/// @tparam  Rng  The range type; it must supply a random-acees or bidirectional iterator
/// @param[in] range  The range 
/// @return  The value of the last element
///
template<class Rng, 
		 class = EnableIfAnyRange<Rng>> 
[[nodiscard]] RangeElementCref<Rng> last_elem(const Rng& range);

/// Given a container of std::pair<> elements (such as  std::map<>), this function creates a range object 
/// representing the first elements in each pair, accessible through constant iterators.
///
/// @tparam  Cont   The container type
/// @param[in] cont  The container 
/// @return  The range
///
template<class Cont>
[[nodiscard]] Range<CiteratorFirst<typename Cont::const_iterator>> 
crange_first(const Cont& cont)
{
	return Range<CiteratorFirst<typename Cont::const_iterator>>(cbegin_first(cont), cend_first(cont));
}

/// Given a container of  std::pair<>  elements ( such as  std::map<> ), this function creates a range object 
/// representing the first elements in each pair, accessible through non-constant iterators.
///
/// @tparam  Cont   The container type
/// @param[in] cont  The container 
/// @return  The range
///
template<class Cont>
[[nodiscard]] Range<IteratorFirst<typename Cont::iterator>> 
range_first(Cont& cont)
{
	return Range<IteratorFirst<typename Cont::iterator>>(begin_first(cont), end_first(cont));
}

/// Given a container of  std::pair<>  elements ( such as  std::map<> ), this function creates a range object 
/// representing the second elements in each pair, accessible through constant iterators.
///
/// @tparam  Cont   The container type
/// @param[in] cont  The container 
/// @return  The range
///
template<class Cont>
[[nodiscard]] auto crange_second(const Cont& cont)
{
	return Range{ cbegin_second(cont), cend_second(cont) };
}

/// Given a container of  std::pair<>  elements ( such as  std::map<> ), this function creates a range object 
/// representing the second elements in each pair, accessible through non-constant iterators.
///
/// @tparam  Cont   The container type
/// @param[in] cont  The container 
/// @return  The range
///
template<class Cont>
[[nodiscard]] Range<IteratorSecond<typename Cont::iterator>> 
range_second(Cont& cont)
{
	return Range<IteratorSecond<typename Cont::iterator>>(begin_second(cont), end_second(cont));
}

// --- Helper aliases ---

/// A range based on the std::vector::iterator type.
template<class T,
         class Alloc = std::allocator<T>>
using VectorElemRange = Range<typename std::vector<T, Alloc>::iterator>;

/// A range based on the std::vector::const_iterator type.
template<class T,
        class Alloc = std::allocator<T>>
using VectorCelemRange = Range<typename std::vector<T, Alloc>::const_iterator>;

/// A range of const iterators to keys in an STL map.
template<class TKey,
		 class TValue,
		 class Pred = std::less<TKey>,
		 class TAlloc = std::allocator<std::pair<const TKey, TValue>>> 
using MapKeyRange = 
		Range<CiteratorFirst<
				typename std::map<TKey, TValue, Pred, TAlloc>::const_iterator>>; 

/// A range of const iterators to values in an STL map.
template<class TKey,
		 class TValue,
		 class Pred = std::less<TKey>,
		 class TAlloc = std::allocator<std::pair<const TKey, TValue>>> 
using MapCvalueRange = 
		Range<CiteratorSecond<
				typename std::map<TKey, TValue, Pred, TAlloc>::const_iterator>>; 

/***  Range-based algorithms  ***/

/// Range-based version of std::accumulate()
template<class Rng, 
         class Val, 
		 class BinaryOp,
		 class = EnableIfAnyRange<Rng>,
		 class = EnableIfFuncR<Val, BinaryOp, Val, RangeElementCref<Rng>>>
Val accumulate(const Rng& range, Val init_value, BinaryOp op)
{
	using std::begin;
	using std::end;
	return std::accumulate(begin(range), end(range), init_value, op);
}

/// Range-based version of std::find()
template<class Rng, 
         class Val,
		 class = EnableIfEqComparable<RangeElementCref<Rng>, const Val&>>
[[nodiscard]] auto find(const Rng& range, const Val& value)
{
	using std::begin;
	using std::end;
	return std::find(begin(range), end(range), value);	
}

/// Range-based version of std::find()
template<class Rng, 
         class Val,
		 class = EnableIfAnyRange<Rng>>
[[nodiscard]] auto find(Rng& range, const Val& value)
{
	using std::begin;
	using std::end;
	return std::find(begin(range), end(range), value);	
}

/// Range-based version of std::find_if()
template<class Rng, 
         class Pred,
		 class = EnableIfAnyRange<Rng>,
		 class = EnableIfPred<Pred, RangeElementCref<Rng>>>
[[nodiscard]] auto find_if(const Rng& range, Pred pred)
{
	using std::begin;
	using std::end;
	return std::find_if(begin(range), end(range), pred);	
}

/// Range-based version of std::find_if()
template<class Rng, 
         class Pred,
		 class = EnableIfAnyRange<Rng>,
		 class = EnableIfPred<Pred, RangeElementRef<Rng>>>
[[nodiscard]] auto find_if(Rng& range, Pred pred)
{
	using std::begin;
	using std::end;
	return std::find_if(begin(range), end(range), pred);
}

/// Range-based version of std::find_if_not()
template<class Rng, 
         class Pred,
		 class = EnableIfAnyRange<Rng>,
		 class = EnableIfPred<Pred, RangeElementCref<Rng>>>
[[nodiscard]] auto find_if_not(Rng& range, Pred pred)
{
	using std::begin;
	using std::end;
	return std::find_if_not(begin(range), end(range), pred);
}

/// Range-based version of std::find_if_not()
template<class Rng, 
         class Pred,
		 class = EnableIfAnyRange<Rng>>
[[nodiscard]] auto find_if_not(const Rng& range, Pred pred)
{
	using std::begin;
	using std::end;
	return std::find_if_not(begin(range), end(range), pred);
}

/// Range-based version of std::count()
template<class Rng, 
         class Val,
		 class = EnableIfAnyRange<Rng>,
		 class = EnableIfEqComparable<Val, RangeElement<Rng>>>
[[nodiscard]] std::size_t count(const Rng& range, const Val& value) 
{
	using std::begin;
	using std::end;
	return std::count(begin(range), end(range), value);	
}

/// Range-based version of std::count_if()
template<class Rng, 
         class Pred,
		 class = EnableIfAnyRange<Rng>,
		 class = EnableIfPred<Pred, RangeElementCref<Rng>>>
[[nodiscard]] std::size_t count_if(const Rng& range, Pred pred) 
{
	using std::begin;
	using std::end;
	return std::count_if(begin(range), end(range), pred);	
}

/// Range-based version of std::for_each()
template<class Rng, 
         class Fn,
		 class = EnableIfFunc<Fn, RangeElementRef<Rng>>>
Fn for_each(Rng& range, Fn func)
{
	using std::begin;
	using std::end;
	return std::for_each(begin(range), end(range), func);	
}

/// Range-based version of std::for_each()
template<class Rng, 
         class Fn,
		 class = EnableIfFunc<Fn, RangeElementCref<Rng>>>
Fn for_each(const Rng& range, Fn func)
{
	using std::begin;
	using std::end;
	return std::for_each(begin(range), end(range), func);	
}

/// Range-based version of std::copy()
template<class Rng, 
         class OutIter,
		 class = EnableIfOutIterator<OutIter, RangeElementCref<Rng>>>
OutIter copy(const Rng& range, OutIter dest)
{
	using std::begin;
	using std::end;
	return std::copy(begin(range), end(range), dest);
}

/// Range-based version of std::copy_if()
template<class Rng, 
         class OutIter,
         class Pred,
		 class = EnableIfOutIterator<OutIter, RangeElementCref<Rng>>,
		 class = EnableIfPred<Pred, RangeElementCref<Rng>>>
OutIter copy_if(const Rng& range, OutIter dest, Pred pred)
{
	using std::begin;
	using std::end;
	return std::copy_if(begin(range), end(range), dest, pred);
}

/// Range-based version of std::transform()
template<class Rng, 
         class OutIter,
         class UnaryOp,
		 class = EnableIfOutIterator<OutIter, 
				                     std::invoke_result_t<UnaryOp, RangeElementCref<Rng>>>>
OutIter transform(const Rng& range, OutIter dest, UnaryOp op)
{
	using std::begin;
	using std::end;
	return std::transform(begin(range), end(range), dest, op);
}

/// Range-based version of std::transform()
template<class Rng1, 
         class Rng2,
         class OutIter,
         class BinaryOp,
		 class = EnableIfOutIterator<OutIter, 
				                     std::invoke_result_t<BinaryOp, 
									                      RangeElementCref<Rng1>, 
													      RangeElementCref<Rng2>>>>
OutIter transform(const Rng1& range1, const Rng2& range2, OutIter dest, BinaryOp op)
{
	using std::begin;
	using std::end;
	return std::transform(begin(range1), end(range1), begin(range2), dest, op);
}

//! Range-based version of std::sort()
template<class Rng,
		 class = EnableIfHasLessThan<RangeElementCref<Rng>>>
void sort(Rng& range)
{
	using std::begin;
	using std::end;
	std::sort(begin(range), end(range));
}

//! Range-based version of std::sort()
template<class Rng, 
         class Compare,
		 class = EnableIfAnyRange<Rng>,
		 class = EnableIfPred<Compare, RangeElementCref<Rng>, RangeElementCref<Rng>>>
void sort(Rng& range, Compare comp)
{
	using std::begin;
	using std::end;
	std::sort(begin(range), end(range), comp);
}

/*! Similar to the range-based version of std::sort(), but it copies the sorted elements in the original range to a 
    vector and returns that.
 */   
template<rg::input_range Rng,
		 class = EnableIfHasLessThan<RangeElementCref<Rng>>> 
[[nodiscard]] auto sort_to_vector(const Rng& range) -> std::vector<rg::range_value_t<Rng>>
{
	using std::begin;
	using std::end;

	auto copy = std::vector<rg::range_value_t<Rng>>(rg::ssize(range));
	std::partial_sort_copy(begin(range), end(range), begin(copy), end(copy));

	return copy;
}

//! Range-based version of std::is_sorted()
template<class Rng,
		 class = EnableIfHasLessThan<RangeElementCref<Rng>>>
bool is_sorted(Rng& range)
{
	using std::begin;
	using std::end;
	return std::is_sorted(begin(range), end(range));
}

/// Range-based version of std::is_sorted()
template<class Rng, 
         class Compare,
		 class = EnableIfPred<Compare, RangeElementCref<Rng>, RangeElementCref<Rng>>>
bool is_sorted(Rng& range, Compare comp)
{
	return std::is_sorted(begin(range), end(range), comp);
}

/// Range-based version of std::fill()
template<class Rng, 
         class Val,
		 class = EnableIfRangeTakes<Rng, Val>>
void fill(Rng& range, const Val& value)
{
	using std::begin;
	using std::end;
	std::fill(begin(range), end(range), value);
}

/// Range-based version of std::equal()
template<class Rng1, 
         class Rng2,
		 class = EnableIfEqComparable<RangeElement<Rng1>, RangeElement<Rng2>>>
[[nodiscard]] bool equal(const Rng1& range1, const Rng2& range2)
{
	using std::begin;
	using std::end;
	return std::equal(begin(range1), end(range1), begin(range2));
}

/// Range-based version of std::equal()
template<class Rng1, 
         class Rng2,
		 class BinaryPred,
		 class = EnableIfPred<BinaryPred, RangeElementCref<Rng1>, RangeElementCref<Rng2>>>
[[nodiscard]] bool equal(const Rng1& range1, const Rng2& range2, BinaryPred pred)
{
	using std::begin;
	using std::end;
	return std::equal(begin(range1), end(range1), begin(range2), pred);
}

//! Range-based modification of std::equal(). Checks first that the containers have the same sizes.
template<rg::input_range Rng1, 
         rg::input_range Rng2,
		 class = EnableIfEqComparable<RangeElement<Rng1>, RangeElement<Rng2>>>
[[nodiscard]] bool equal_same_size(const Rng1& range1, const Rng2& range2)
{
	using std::begin;
	using std::end;

	if (rg::ssize(range1) != rg::ssize(range2)) {
		return false;
	}
	return std::equal(begin(range1), end(range1), begin(range2));
}

/// Range-based version of std::equal() . Checks first that the containers have the same sizes.
template<class Rng1, 
         class Rng2,
		 class BinaryPred,
		 class = EnableIfPred<BinaryPred, RangeElementCref<Rng1>, RangeElementCref<Rng2>>>
[[nodiscard]] bool equal_same_size(const Rng1& range1, const Rng2& range2, BinaryPred pred)
{
	if (range1.size() != range2.size()) {
		return false;
	}
	using std::begin;
	using std::end;
	return std::equal(begin(range1), end(range1), begin(range2), pred);
}

/// Range-based version of std::iota()
template<class Rng, 
         class Val,
		 class = EnableIfRangeTakes<Rng, Val>>
void iota(Rng& range, Val value)
{
	using std::begin;
	using std::end;
	std::iota(begin(range), end(range), value);
}

/// Range-based version of std::shuffle()
template<class Rng, 
         class URBG,
		 class = EnableIfAnyRange<Rng>>
void shuffle(Rng& range, URBG&& g)
{
	using std::begin;
	using std::end;
	std::shuffle(begin(range), end(range), g);
}

/// Range-based version of std::is_permutation()
template<class Rng1, 
         class Rng2,
		 class = EnableIfEqComparable<RangeElement<Rng1>, RangeElement<Rng2>>>
[[nodiscard]] bool is_permutation(const Rng1& range1, const Rng2& range2)
{
	using std::begin;
	using std::end;
	return std::is_permutation(begin(range1), end(range1), begin(range2));
}

/// Range-based version of std::is_permutation()
template<class Rng1, 
         class Rng2,
		 class BinaryPred,
		 class = EnableIfPred<BinaryPred, RangeElementCref<Rng1>, RangeElementCref<Rng2>>> 
[[nodiscard]] bool is_permutation(const Rng1& range1, const Rng2& range2, BinaryPred pred)
{
	using std::begin;
	using std::end;
	return std::is_permutation(begin(range1), end(range1), begin(range2), pred);
}

/// Range-based version of std::is_permutation() . Checks first that the containers have the same sizes.
template<class Rng1, 
         class Rng2,
		 class = EnableIfEqComparable<RangeElement<Rng1>, RangeElement<Rng2>>>
[[nodiscard]] bool is_permutation_same_size(const Rng1& range1, const Rng2& range2)
{
	if (range1.size() != range2.size()) {
		return false;
	}
	using std::begin;
	using std::end;
	return std::is_permutation(begin(range1), end(range1), begin(range2));
}

/// Range-based version of std::unique()
template<class Rng, 
		 class = EnableIfEqComparable<RangeElement<Rng>, RangeElement<Rng>>>
auto unique(Rng& range)
{
	using std::begin;
	using std::end;
	return std::unique(begin(range), end(range));
}

/// Range-based version of std::unique()
template<class Rng, 
		 class BinaryPred,
		 class = EnableIfAnyRange<Rng>,
		 class = EnableIfPred<BinaryPred, RangeElementCref<Rng>, RangeElementCref<Rng>>> 
auto unique(Rng& range, BinaryPred pred)
{
	using std::begin;
	using std::end;
	return std::unique(begin(range), end(range), pred);
}

/// Range-based version of std::set_difference()
template<class Rng1, 
         class Rng2,
		 class OutIter,
		 class = EnableIfHaveLessThan<RangeElementCref<Rng1>, RangeElementCref<Rng2>>,
		 class = EnableIfOutIterator<OutIter, RangeElementCref<Rng1>>>
OutIter set_difference(const Rng1& range1, const Rng2& range2, OutIter result)
{
	using std::begin;
	using std::end;
	return std::set_difference(begin(range1), end(range1), 
			begin(range2), end(range2), result);
}

/// Range-based version of std::set_intersection()
template<class Rng1, 
         class Rng2,
		 class OutIter,
		 class = EnableIfHaveLessThan<RangeElementCref<Rng1>, RangeElementCref<Rng2>>,
		 class = EnableIfOutIterator<OutIter, RangeElementCref<Rng1>>>
OutIter set_intersection(const Rng1& range1, const Rng2& range2, OutIter result)
{
	using std::begin;
	using std::end;
	return std::set_intersection(begin(range1), end(range1), 
			begin(range2), end(range2), result);
}

/// Range-based version of std::set_intersection()
template<class Rng1, 
         class Rng2,
		 class OutIter,
		 class Compare,
		 class = EnableIfOutIterator<OutIter, RangeElementCref<Rng1>>,
		 class = EnableIfPred<Compare, RangeElementCref<Rng1>, RangeElementCref<Rng2>>>
OutIter set_intersection(const Rng1& range1, const Rng2& range2, OutIter result, Compare compare)
{
	using std::begin;
	using std::end;
	return std::set_intersection(begin(range1), end(range1), 
			begin(range2), end(range2), result, compare);
}

/// Range-based version of std::max_element()
template<class Rng,
		 class = EnableIfAnyRange<Rng>>
[[nodiscard]] auto max_element(const Rng& range)
{
	using std::begin;
	using std::end;
	return std::max_element(begin(range), end(range));
}

/// Range-based version of std::max_element()
template<class Rng,
         class Compare,
		 class = EnableIfAnyRange<Rng>,
		 class = EnableIfPred<Compare, RangeElementCref<Rng>, RangeElementCref<Rng>>>
[[nodiscard]] auto max_element(const Rng& range, Compare comp)
{
	using std::begin;
	using std::end;
	return std::max_element(begin(range), end(range), comp);	
}

/// Range-based version of std::lower_bound()
template<class Rng, 
         class Val,
		 class = EnableIfHaveLessThan<RangeElementCref<Rng>, const Val&>>
[[nodiscard]] auto lower_bound(const Rng& range, const Val& value)
{
	return std::lower_bound(begin(range), end(range), value);
}

/// Range-based version of std::upper_bound()
template<class Rng, 
         class Val,
		 class = EnableIfHaveLessThan<const Val&, RangeElementCref<Rng>>>
[[nodiscard]] auto upper_bound(const Rng& range, const Val& value)
{
	using std::begin;
	using std::end;
	return std::upper_bound(begin(range), end(range), value);
}

// --- Other algorithms ---

/// Range comparison results
enum class RangeCompResult {
	identical   = 1, ///< The two ranges are identical: they contain the same elements in the same order
	permutation = 2, ///< The two ranges are similar: they contain the same elements but in a different order
	different   = 3	 ///< The two ranges are different: they do not contain the same elements
};

/// Compare two ranges, using operator==. Neither range is expected to be sorted.
///
/// @tparam  Rng1  The first range type
/// @tparam  Rng2  The second range type
/// @param[in] range1  The first range 
/// @param[in] range2  The second range 
/// @return  The result of the comparison
///
template<class Rng1, 
         class Rng2,
		 class = EnableIfEqComparable<RangeElementCref<Rng1>, RangeElementCref<Rng2>>> 
[[nodiscard]] RangeCompResult compare_ranges(const Rng1& range1, const Rng2& range2);   

/// Find the n-th occurrence of an element, within a range, which matches a predicate.
///
/// @tparam  Rng  The range type
/// @param[in] range  The range 
/// @param[in] pred  The predicate
/// @param[in] match_idx  The (zero-based) index of the matching element
/// @return  Iterator addressing the element, if one satisfying the conditions is found, or one past the 
///					final element in the range otherwise
///
template<class Rng, 
         class Pred,
		 class = EnableIfPred<Pred, RangeElementCref<Rng>>>
[[nodiscard]] auto find_nth_if(const Rng& range, Ssize match_idx, Pred pred);

/*! Get the position, or index, of the first element with a specific value in a range.
    \tparam Rng  Range type
    \tparam Elem  Range element type
    \param[in] range  The range
    \param[in] elem  The element value to look for (using the == operator)
    \return  The (zero-based) position of the first matching element, or k_no_pos if no match is found
 */
template<class Rng, class Elem>
requires std::equality_comparable_with<Elem, RangeElementCref<Rng>> 
[[nodiscard]] auto position_of(const Rng& range, const Elem& elem) -> Ssize
{
	using std::begin;
	using std::end;

	const auto range_begin = begin(range);
	const auto range_end = end(range);

	const auto it = std::find(range_begin, range_end, elem);
	if (it == range_end) {
		return k_no_pos;
	}

	return std::distance(range_begin, it);
}

/*! Get the position, or index, of the first element in a range which satisfies a predicate.
    \tparam Rng  Range type
    \tparam Pred  Predicate type
    \param[in] range  The range
    \param[in] pred  Predicate wihich identifies the element to look for 
    \return  The (zero-based) position of the first matching element, or k_no_pos if no match is found
 */
template<class Rng, class Pred>
requires std::is_invocable_r_v<bool, Pred, RangeElementCref<Rng>> 
[[nodiscard]] auto position_of_if(const Rng& range, Pred&& pred) -> Ssize
{
	using std::begin;
	using std::end;

	const auto range_begin = begin(range);
	const auto range_end = end(range);

	const auto it = std::find_if(range_begin, range_end, std::forward<Pred>(pred));
	if (it == range_end) {
		return k_no_pos;
	}

	return std::distance(range_begin, it);
}

/*! Get the positions, or indexes, of all elements with a specific value in a range.
    \tparam Rng  Range type
    \tparam Elem  Range element type
    \param[in] range  The range
    \param[in] elem  The element value to look for (using the == operator)
    \return  The (zero-based) positions of the all matching elements
 */
template<class Rng, class Elem>
requires std::equality_comparable_with<Elem, RangeElementCref<Rng>> 
[[nodiscard]] auto positions_of(const Rng& range, const Elem& elem) -> std::set<Ssize>
{
	using std::begin;
	using std::end;

	auto ret = std::set<Ssize>{};

	const auto range_begin = begin(range);
	const auto range_end = end(range);
	auto it = range_begin;
	while ((it = std::find(it, range_end, elem)) != range_end) {
		ret.insert(std::distance(range_begin, it));
		++it;
	}

	return ret;
}

/*! Get the positions, or indexes, of all elements in a range which satisfy a predicate..
    \tparam Rng  Range type
    \tparam Pred  Predicate type
    \param[in] range  The range
    \param[in] pred  Predicate wihich identifies the element to look for 
    \return  The (zero-based) positions of the all matching elements
 */
template<class Rng, class Pred>
requires std::is_invocable_r_v<bool, Pred, RangeElementCref<Rng>> 
[[nodiscard]] auto positions_of_if(const Rng& range, Pred pred) -> std::set<Ssize>
{
	using std::begin;
	using std::end;

	auto ret = std::set<Ssize>{};

	const auto range_begin = begin(range);
	const auto range_end = end(range);
	auto it = range_begin;
	while ((it = std::find_if(it, range_end, pred)) != range_end) {
		ret.insert(std::distance(range_begin, it));
		++it;
	}

	return ret;
}

/*! Given a sorted range containing numeric elements and a closed interval in the same number set, get the bounds of 
    range indexes for the elements which fall inside the interval.
    \tparam Pos  The vector position type (must be an integer type)
    \tparam Rng  The range type
    \param[in] range  The range
    \param[in] interv  The closed interval
    \return  The bounds of the of range positions, or nullopt if no range elements fall inside the given interval
 */
template<class Pos, 
         class Rng,
		 class = EnableIfInteger<Pos>,
		 class = EnableIfAnyRange<Rng>>
[[nodiscard]] auto get_bounded_pos_range(const Rng& range, 
		                                 const twist::math::ClosedInterval<RangeElement<Rng>>& interv)
                    -> std::optional<twist::math::ClosedInterval<Pos>>;

/*! Find out whether a range contains any duplicates, using the default "less than" and "equal to" operators.
    \tparam Rng  The range type
    \param[in] range  The range; it will be sorted by this function
	\return  true if the range contains any duplicate elements; false otherwise
 */
template<rg::random_access_range Rng>
[[nodiscard]] auto has_duplicates(Rng& range) -> bool
{
	using std::end;

	rg::sort(range);
	return rg::adjacent_find(range) != end(range);
}

/*! Find out whether a range contains any duplicates, using the default "less than" and "equal to" operators.
    \tparam Rng  The range type
    \param[in] range  The range; it will be sorted by this function
	\return  true if the range contains any duplicate elements; false otherwise
 */
template<rg::random_access_range Rng>
requires std::is_rvalue_reference_v<Rng&&>
[[nodiscard]] auto has_duplicates(Rng&& range) -> bool
{
	using std::end;

	rg::sort(range);
	return rg::adjacent_find(range) != end(range);
}

/*! Look for duplicates inside a (potentially unsorted) range.
    \tparam Rng  The range type
    \tparam LessThan  Binary predicate type, which when applied to two elements should return true if the first 
	                   element should come first while sorting the range
    \tparam EqualTo  Binary predicate type, which when applied to two elements should return true if they are equal
    \param[in] range  The range; it will be sorted by this function
    \param[in] less_than  "Less than" predicate
    \param[in] less_than  "Equal to" predicate
    \return  A list of all pairs of equal (given the predicate) elements in the range; or an empty list if there are no 
	         duplicates in the range
 */
template<class Rng, 
         class LessThan,
		 class EqualTo>
	requires std::is_invocable_r_v<bool, LessThan, RangeElementCref<Rng>, RangeElementCref<Rng>>
		  && std::is_invocable_r_v<bool, EqualTo, RangeElementCref<Rng>, RangeElementCref<Rng>>
[[nodiscard]] auto find_duplicates(Rng& range, LessThan less_than, EqualTo equal_to) -> 
		std::vector<std::pair<gsl::not_null<RangeElementCptr<Rng>>, gsl::not_null<RangeElementCptr<Rng>>>>
{
	using std::begin;
	using std::end;

	using ElementPtr = gsl::not_null<RangeElementCptr<Rng>>;
	auto ret = Pairs<ElementPtr, ElementPtr>{};

	rg::sort(range, less_than);
	auto it = begin(range);
	while (it != end(range)) {
		it = std::adjacent_find(it, end(range), equal_to);
		if (it != end(range)) {
			auto next_it = std::next(it);
			assert(next_it != end(range) && equal_to(*it, *next_it)); // sanity check
			// We found a group of equal elements, at least two; make a list of all elements in the group                
			auto group = std::vector<ElementPtr>{std::addressof(*it)};
			while (next_it != end(range) && equal_to(*it, *next_it)) {
				group.push_back(std::addressof(*next_it));
				++next_it;
			}
			it = next_it;
			// Save all pairs in the group
			const auto group_size = ssize(group);
			assert(group_size >= 2); // sanity check		
			for (auto i : IndexRange{group_size}) {
				for (auto j : IndexRange{i + 1, group_size}) {
					ret.emplace_back(group[i], group[j]);
				}
			}
		}
	}
	return ret;
}

/*! Count the mismatching pairs of elements between ranges \p range1 and \p range2.
    The elements in each pair are compared using the predicate \p pred.
    The function compares pairs until it arrives at the end of whichever range is shorter.
 */
template<rg::input_range Rng1, 
         rg::input_range Rng2, 
		 class Pred = rg::equal_to>
requires std::is_invocable_r_v<bool, Pred, rg::range_reference_t<Rng1>, rg::range_reference_t<Rng2>> 
[[nodiscard]] auto count_mismatches(Rng1&& range1, Rng2&& range2, Pred pred = {}) -> Ssize
{
	using std::begin;
	using std::end;

	auto ret = Ssize{0};

	auto it1 = begin(range1);
	auto it2 = begin(range2);
	const auto end1 = end(range1);
	const auto end2 = end(range2);
	while (it1 != end1 && it2 != end2) {
		if (!std::invoke(pred, *it1++, *it2++)) {
			++ret;
		}
	}

	return ret;
}

//! Find out whether the two sorted ranges \p set1 and \p set2 are disjoint, ie have no elements in common. 
template<rg::input_range Rng1,
	     rg::input_range Rng2>
[[nodiscard]] auto are_disjoint_sets(const Rng1& set1, const Rng2& set2)
{
	assert(rg::is_sorted(set1));
	assert(rg::is_sorted(set2));

	if (set1.empty() || set2.empty()) {
		return true;
	}

	auto it1 = set1.begin();
	const auto end1 = set1.end();
	auto it2 = set2.begin();
	const auto end2 = set2.end();

	if (*it1 > *set2.rbegin() || *it2 > *set1.rbegin()) {
		return true;
	}

	while (it1 != end1 && it2 != end2) {
		if (*it1 == *it2) {
			return false;
		}
		if (*it1 < *it2) {
			++it1;
		}
		else {
			++it2;
		}
	}

	return true;
}

/*! Given the range of data values \t range, test that the difference between each element (but first) and the element 
    before is the same across the range. If that is so, return true and that difference; otherwaise return false and
	the default for a range element value.
	If the range contains less than two elements or the step is not the same across the range, an exception is thrown.
 */
template<rg::input_range Rng>
[[nodiscard]] auto is_equally_spaced(const Rng& range) -> std::tuple<bool, rg::range_value_t<Rng>>;

/*! Given the range of data values \t range, tests that the difference between each element (but first) and the element 
    before is the same across the range, within the tolerance \p tol. If that is so, return true and that difference; 
	otherwaise return false and the default for a range element value.
	If the range contains less than two elements or the step is not the same across the range, an exception is thrown.
 */
template<rg::input_range Rng>
[[nodiscard]] auto is_equally_spaced_tol(const Rng& range, rg::range_value_t<Rng> tol = 0) 
                    -> std::tuple<bool, rg::range_value_t<Rng>>;

}

#include "twist/ranges.ipp"

#endif

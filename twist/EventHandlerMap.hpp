///  @file  EventHandlerMap.hpp
///  EventHandlerMap class

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_EVENT_HANDLER_MAP_HPP
#define TWIST_EVENT_HANDLER_MAP_HPP

namespace twist {

///
///  Map associating an event handler function with an event and a trigger; events have associated data.
///  Template parameters:
///    TEvent  Event identifier type. Must be comparable with operator < .
///    TTrigger  Trigger identifier type. Must be comparable with operator < .
///    TData  Event data type.
///
template<typename TEvent, typename TTrigger, typename TData>
class EventHandlerMap : public NonCopyable {
public:
	typedef TEvent  Event;
	typedef TTrigger  Trigger;
	typedef TData  Data;
	typedef std::function<void (Event, Trigger, const Data&)>  Handler;

	/// Constructor.
	///	
	EventHandlerMap();
	
	/// Destructor.
	///
	virtual ~EventHandlerMap();

	/// Add an event handler to the map. Call this method when the handler is a non-static class member 
	/// function.
	///
	/// @tparam  TMemFunc  The member function (or a pointer to it)
	/// @tparam  TObj  The class whose member the handler is
	/// @param[in] event  The event
	/// @param[in] trigger  The trigger
	/// @param[in] mem_func  The member function (or a pointer to it)
	/// @param[in] obj  The object on which the member function will be called
	/// @return  The old handler, if a handler for this event and trigger already existed, otherwise an 
	///					empty functor
	///
	template<typename TMemFunc, class TObj> 
	Handler add(Event event, Trigger trigger, TMemFunc mem_func, TObj& obj);

	/// Remove an event handler from the map. 
	///
	/// @param[in] event  The event
	/// @param[in] trigger  The trigger
	/// @return  The removed handler, if a handler for this event and trigger existed, otherwise an empty 
	///					functor
	///
	Handler remove(Event event, Trigger trigger);

	/// Call a specific event handler from the map. 
	///
	/// @param[in] event  The event
	/// @param[in] trigger  The trigger
	/// @param[in] data  The data associated with the event
	///
	/// @return  true if a handler for this event and trigger exists and has been called, otherwise false
	///
	bool call(Event event, Trigger trigger, const Data& data) const;

private:	
	typedef std::pair<Event, Trigger>  Key;
	
	struct KeyCompare {
		bool operator()(const Key& k1, const Key& k2) const;
	};

	std::map<Key, Handler, KeyCompare>  handlers_;

	TWIST_CHECK_INVARIANT_DECL
};

} 

#include "EventHandlerMap.ipp"

#endif 


/// @file std_vector_utils.hpp
/// Utilities for working with std::vector;  additional to those in "twsit_stl.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST__STD__VECTOR__UTILS_HPP
#define TWIST__STD__VECTOR__UTILS_HPP

#include <gsl/gsl>

#include <vector>

namespace twist {

/*! Create a vector and reserve memory for a specific number of elements.
    \tparam Elem  Vector element type
    \tparam Alloc  Vector allocator type
    \param[in] size  The size to be allocated
    \return  The vector
 */ 
template<class Elem, class Alloc = std::allocator<Elem>>
[[nodiscard]] auto reserve_vector(Ssize size) -> std::vector<Elem, Alloc>;

/*! Create a vector of pointers in gsl::not_null wrappers and reserve memory for a specific number of elements.
    \tparam T  Pointer type
    \tparam Alloc  Vector allocator type
    \param[in] size  The size to be allocated
    \return  The vector
 */ 
template<class T, class Alloc = std::allocator<gsl::not_null<T>>>
[[nodiscard]] auto reserve_not_nulls(Ssize size) -> std::vector<gsl::not_null<T>, Alloc>;

/*! Create a vector containing the elements \p elems. The vector element type will be the type of the first element 
    passed in, and so the following element types must be convertible to that type.
*/
template<class... Types>
[[nodiscard]] auto create_vector(Types&&... elems);

/*! Append the contents of a vector to another.
    \param[in] src  The source vector, whose contents will be moved at the end of the other
    \param[in] dest  The destination vector
    \return  Iterator addressing the first appended element; or end(dest) if no elements were appended
 */
template<class Elem>
auto append(std::vector<Elem> src, std::vector<Elem>& dest) -> typename std::vector<Elem>::iterator;

/*! Get the position in a vector of an element pointed at by an iterator.
    \tparam Elem  Vector element type
    \tparam Iter  Type of the iterator pointing at the value 
    \tparam Alloc  Vector allocator type
    \param[in] vector  The vector
    \param[in] iter  Iterator pointing at the value; the function assumes it points inside the vector, or at the end of 
	                 the vector
    \return  The (zero-based) position of the element; or the size of the vector if the iterator points at the end of 
	         the vector
 */
template<class Elem, class Iter, class Alloc>
[[nodiscard]] auto position_of_iter(const std::vector<Elem, Alloc>& vector, const Iter& iter) -> Ssize;

/// Create a vector containing elements read from a stack C-array. 
///
/// @tparam  Elem  The C-array/vector element type
/// @tparam  t_arr_size  The number of elements in the C-array
/// @param[in] arr  The C-array 
/// @return  The vector
/// 
template<typename Elem, size_t t_arr_size>
std::vector<Elem> make_vector(Elem(&arr)[t_arr_size])  //+DEPRECATED
{
	return std::vector<Elem>(arr, arr + t_arr_size);
}

/// Create a vector containing elements based on the elements of a stack C-array. 
///
/// @tparam  TVectorElem  The vector element type
/// @tparam  TArrayElem  The C-array element type
/// @tparam  t_arr_size  The number of elements in the C-array
/// @param[in] arr  The C-array 
/// @return  The vector
/// 
template<typename TVectorElem, typename TArrElem, size_t t_arr_size>
std::vector<TVectorElem> make_vector(TArrElem(&arr)[t_arr_size])  //+DEPRECATED
{
	return std::vector<TVectorElem>(arr, arr + t_arr_size);
}

/// Create a vector containing a specific number of elements.
///
/// @tparam  T  The vector element type
/// @param[in] eln  The n-th element 
/// @return  The vector
/// 
template<typename T> std::vector<T> make_vector(const T& el1)  //+DEPRECATED
{
	return std::vector<T>(1, el1);
}

/// Create a vector containing a specific number of elements
///
/// @tparam  T  The vector element type
/// @param[in] eln  The n-th element 
/// @return  The vector
/// 
template<typename T> std::vector<T> make_vector(T&& el1)  //+DEPRECATED
{
	return std::vector<T>(1, std::forward<T>(el1));
}

/// Create a vector containing a specific number of elements
///
/// @tparam  T  The vector element type
/// @param[in] eln  The n-th element 
/// @return  The vector
/// 
template<typename T> std::vector<T> make_vector(const T& el1, const T& el2)  //+DEPRECATED
{
	std::vector<T> v(1, el1);
	v.push_back(el2);
	return v;
}

/// Create a vector containing a specific number of elements
///
/// @tparam  T  The vector element type
/// @param[in] eln  The n-th element 
/// @return  The vector
/// 
template<typename T> std::vector<T> make_vector(T&& el1, T&& el2)  //+DEPRECATED
{
	std::vector<T> v(1, std::forward<T>(el1));
	v.push_back(std::forward<T>(el2));
	return v;
}

/// Create a vector containing a specific number of elements
///
/// @tparam  T  The vector element type
/// @param[in] eln  The n-th element 
/// @return  The vector
/// 
template<typename T> 
std::vector<T> make_vector(const T& el1, const T& el2, const T& el3)  //+DEPRECATED
{
	std::vector<T> v(1, el1);
	v.push_back(el2);
	v.push_back(el3);
	return v;
}

/// Create a vector containing a specific number of elements
///
/// @tparam  T  The vector element type
/// @param[in] eln  The n-th element 
/// @return  The vector
/// 
template<typename T> 
std::vector<T> make_vector(T&& el1, T&& el2, T&& el3)  //+DEPRECATED
{
	std::vector<T> v(1, std::forward<T>(el1));
	v.push_back(std::forward<T>(el2));
	v.push_back(std::forward<T>(el3));
	return v;
}

/// Create a vector containing a specific number of elements
///
/// @tparam  T  The vector element type
/// @param[in] eln  The n-th element 
/// @return  The vector
/// 
template<typename T> 
std::vector<T> make_vector(const T& el1, const T& el2, const T& el3, const T& el4)  //+DEPRECATED
{
	std::vector<T> v(1, el1);
	v.push_back(el2);
	v.push_back(el3);
	v.push_back(el4);
	return v;
}

/// Create a vector containing a specific number of elements
///
/// @tparam  T  The vector element type
/// @param[in] eln  The n-th element 
/// @return  The vector
/// 
template<typename T> 
std::vector<T> make_vector(T&& el1, T&& el2, T&& el3, T&& el4)  //+DEPRECATED
{
	std::vector<T> v(1, std::forward<T>(el1));
	v.push_back(std::forward<T>(el2));
	v.push_back(std::forward<T>(el3));
	v.push_back(std::forward<T>(el4));
	return v;
}

/// Create a vector containing a specific number of elements
///
/// @tparam  T  The vector element type
/// @param[in] eln  The n-th element 
/// @return  The vector
/// 
template<typename T> 
std::vector<T> make_vector(const T& el1, const T& el2, const T& el3, const T& el4, const T& el5)  //+DEPRECATED
{
	std::vector<T> v(1, el1);
	v.push_back(el2);
	v.push_back(el3);
	v.push_back(el4);
	v.push_back(el5);
	return v;
}

/// Create a vector containing a specific number of elements
///
/// @tparam  T  The vector element type
/// @param[in] eln  The n-th element 
/// @return  The vector
/// 
template<typename T> 
std::vector<T> make_vector(T&& el1, T&& el2, T&& el3, T&& el4, T&& el5)  //+DEPRECATED
{
	std::vector<T> v(1, std::forward<T>(el1));
	v.push_back(std::forward<T>(el2));
	v.push_back(std::forward<T>(el3));
	v.push_back(std::forward<T>(el4));
	v.push_back(std::forward<T>(el5));
	return v;
}

/// Create a vector containing a specific number of elements
///
/// @tparam  T  The vector element type
/// @param[in] eln  The n-th element 
/// @return  The vector
/// 
template<typename T> std::vector<T> make_vector(const T& el1, const T& el2, const T& el3, const T& el4, 
		const T& el5, const T& el6)  //+DEPRECATED
{
	std::vector<T> v(1, el1);
	v.push_back(el2);
	v.push_back(el3);
	v.push_back(el4);
	v.push_back(el5);
	v.push_back(el6);
	return v;
}

/// Create a vector containing a specific number of elements
///
/// @tparam  T  The vector element type
/// @param[in] eln  The n-th element 
/// @return  The vector
/// 
template<typename T> std::vector<T> make_vector(T&& el1, T&& el2, T&& el3, T&& el4, T&& el5, T&& el6)  //+DEPRECATED
{
	std::vector<T> v(1, std::forward<T>(el1));
	v.push_back(std::forward<T>(el2));
	v.push_back(std::forward<T>(el3));
	v.push_back(std::forward<T>(el4));
	v.push_back(std::forward<T>(el5));
	v.push_back(std::forward<T>(el6));
	return v;
}


/// Create a vector containing a specific number of elements
///
/// @tparam  T  The vector element type
/// @param[in] eln  The n-th element 
/// @return  The vector
/// 
template<typename T> std::vector<T> make_vector(const T& el1, const T& el2, const T& el3, const T& el4, 
		const T& el5, const T& el6, const T& el7)  //+DEPRECATED
{
	std::vector<T> v(1, el1);
	v.push_back(el2);
	v.push_back(el3);
	v.push_back(el4);
	v.push_back(el5);
	v.push_back(el6);
	v.push_back(el7);
	return v;
}

/// Create a vector containing a specific number of elements
///
/// @tparam  T  The vector element type
/// @param[in] eln  The n-th element 
/// @return  The vector
/// 
template<typename T> std::vector<T> make_vector(T&& el1, T&& el2, T&& el3, T&& el4, T&& el5, T&& el6, T&& el7)  //+DEPRECATED
{
	std::vector<T> v(1, std::forward<T>(el1));
	v.push_back(std::forward<T>(el2));
	v.push_back(std::forward<T>(el3));
	v.push_back(std::forward<T>(el4));
	v.push_back(std::forward<T>(el5));
	v.push_back(std::forward<T>(el6));
	v.push_back(std::forward<T>(el7));
	return v;
}

}

#include "twist/std_vector_utils.ipp"

#endif

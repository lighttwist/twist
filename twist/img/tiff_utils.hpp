/// @file tiff_utils.hpp
/// Utilities for working with the Tiff class 

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_IMG_TIFF__UTILS_HPP
#define TWIST_IMG_TIFF__UTILS_HPP

#include "twist/img/Tiff.hpp"

namespace twist::img {

/*! Read all pixel data from the TIFF \p tiff, whose pixel data is organised in tiles.
    If the file does not use the tiled format, an exception is thrown.
    \tparam PixelVal  The type of a pixel value
    \param[in] flip_y  Whether the TIFF data is flipped on the Y axis (first row in the pixel data is the last row 
                       in model space)
 */ 
template<class PixelVal>
[[nodiscard]] auto read_tiled_tiff(const Tiff& tiff, bool flip_y = false) -> Tiff::DataGrid<PixelVal>;

/*! Read all pixel data from the TIFF while at \p path, whose pixel data is organised in tiles.
    If the file does not use the tiled format, an exception is thrown.
    The pixel data grid is returned, together with an open connection to the TIFF file.
    \tparam PixelVal  The type of a pixel value
    \param[in] flip_y  Whether the TIFF data is flipped on the Y axis (first row in the pixel data is the last row 
                       in model space)
 */ 
template<class PixelVal>
[[nodiscard]] auto read_tiled_tiff(fs::path path, bool flip_y = false) -> std::tuple<Tiff::DataGrid<PixelVal>, Tiff>;

/*! Given the grid of pixel data \p pixel_data, create a new TIFF file at \p path, whose pixel data is organised in 
    square tiles containing \p nof_cells_per_tile pixels (\p nof_cells_per_tile must be a perfect square and a 
    multiple of 256). 
    If a file already exists at \p path, it is deleted.
    \p bigtiff specifies whether the output geotiff should be marked as BigTIFF (bigger than 4GB).
 */
template<class PixelVal>
auto write_tiled_tiff(fs::path path, const Tiff::DataGrid<PixelVal>& pixel_data, Ssize nof_cells_per_tile, 
                      bool bigtiff = false) -> Tiff;

/*! Read the value of a specific pixel from a TIFF whose pixel data is organised in scanlines.
    \tparam PixelVal  The type of a pixel value
    \param[in] tiff  Connection to the TIFF file
    \param[in] row  The pixel row
    \param[in] col  The pixel column
    \param[in] flip_y  Whether the TIFF data is flipped on the Y axis (first row in the pixel data is the last row 
                       in model space)
    \return  The pixel value
 */
template<class PixelVal>
[[nodiscard]] auto read_pixel_scanlined(const Tiff& tiff, Ssize row, Ssize col, bool flip_y = false) -> PixelVal;

/*! Read all pixel data from the TIFF \p tiff (regardless of how it is organised). 
    \tparam PixelVal  The type of a pixel value
    \param[in] flip_y  Whether the TIFF data is flipped on the Y axis (first row in the pixel data is the last row 
                       in model space)
    \note  This function only works with single-plane TIFFs.
 */ 
template<class PixelVal>
[[nodiscard]] auto read_pixel_data(const Tiff& tiff, bool flip_y = false) -> Tiff::DataGrid<PixelVal>;

/*! Compare all pixel data between two TIFF inages.
    \tparam PixelVal  The type of a pixel value
    \param[in] tiff1  Connection to the first TIFF 
    \param[in] tiff2  Connection to the second TIFF 
    \param[in] tol  Tolerance to be used for comparing each pair of pixel values
    \param[in] flip_y1  Whether the first TIFF's data is flipped on the Y axis (first row in the pixel data is the last 
                        row in model space)
    \param[in] flip_y2  Whether the second TIFF's data is flipped on the Y axis (first row in the pixel data is the 
                        last row in model space)
    \return  true if all pixel values are equal
    \note  At the moment, this function loads all TIFF data in memory, so it can be quite wasteful of memory.
    \note  At the moment, this function only works with single-plane TIFFs.
 */
template<class PixelVal>
[[nodiscard]] auto compare_pixel_data(const Tiff& tiff1,
                                      const Tiff& tiff2,
                                      PixelVal tol = 0,
                                      bool flip_y1 = false,
                                      bool flip_y2 = false) -> bool;

}

#include "twist/img/tiff_utils.ipp"

#endif

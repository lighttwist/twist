/// @file Tiff.ipp
/// Inline implementation file for "Tiff.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace twist::img {

template<class PixelVal> 
auto Tiff::read_scanlined(OptSubgridInfo subgrid_info, uint16_t plane) const -> DataGrid<PixelVal>
{
	TWIST_CHECK_INVARIANT
	validate_pixel_val_type<PixelVal>();

	if constexpr (std::is_same_v<PixelVal, int8_t>) {   
		return read_scanlined_int8(subgrid_info, plane);
	}
	else if constexpr (std::is_same_v<PixelVal, uint8_t>) {  
		return read_scanlined_uint8(subgrid_info, plane);
	}
	else if constexpr (std::is_same_v<PixelVal, int16_t>) {  
		return read_scanlined_int16(subgrid_info, plane);
	}
	else if constexpr (std::is_same_v<PixelVal, uint16_t>) { 
		return read_scanlined_uint16(subgrid_info, plane);
	}
	else if constexpr (std::is_same_v<PixelVal, int32_t>) {  
		return read_scanlined_int32(subgrid_info, plane);
	}
	else if constexpr (std::is_same_v<PixelVal, uint32_t>) { 
		return read_scanlined_uint32(subgrid_info, plane);
	}
	else if constexpr (std::is_same_v<PixelVal, float>) {    
		return read_scanlined_float32(subgrid_info, plane);
	}
	else if constexpr (std::is_same_v<PixelVal, double>) {   
		return read_scanlined_float64(subgrid_info, plane);
	}
	else { 	
		static_assert(always_false<PixelVal>, "Unsupported pixel type");
	}
}

template<class PixelVal> 
auto Tiff::write_scanlined(const DataGrid<PixelVal>& data, OptSubgridInfo subgrid_info, uint16_t plane) -> void
{
	TWIST_CHECK_INVARIANT
	validate_pixel_val_type<PixelVal>();
	return write_scanlined<PixelVal>(data.data(), subgrid_info, plane);
}

template<class PixelVal> 
auto Tiff::write_scanlined(const PixelVal* data, OptSubgridInfo subgrid_info, uint16_t plane) -> void
{
	TWIST_CHECK_INVARIANT
	validate_pixel_val_type<PixelVal>();

	if constexpr (std::is_same_v<PixelVal, int8_t>) {  
		return write_scanlined_int8(data, subgrid_info, plane);
	}
	else if constexpr (std::is_same_v<PixelVal, uint8_t>) {
		return write_scanlined_uint8(data, subgrid_info, plane);
	}
	else if constexpr (std::is_same_v<PixelVal, int16_t>) {  
		return write_scanlined_int16(data, subgrid_info, plane);
	}
	else if constexpr (std::is_same_v<PixelVal, uint16_t>) { 
		return write_scanlined_uint16(data, subgrid_info, plane);
	}
	else if constexpr (std::is_same_v<PixelVal, int32_t>) {  
		return write_scanlined_int32(data, subgrid_info, plane);
	}
	else if constexpr (std::is_same_v<PixelVal, uint32_t>) { 
		return write_scanlined_uint32(data, subgrid_info, plane);
	}
	else if constexpr (std::is_same_v<PixelVal, float>) {    
		return write_scanlined_float32(data, subgrid_info, plane);
	}
	else if constexpr (std::is_same_v<PixelVal, double>) {   
		return write_scanlined_float64(data, subgrid_info, plane);
	}
	else {
		static_assert(always_false<PixelVal>, "Unsupported pixel type");
	}
}

template<class PixelVal> 	
auto Tiff::read_tile(Ssize tile_idx, DataGrid<PixelVal>& data_buffer) const -> void
{
	TWIST_CHECK_INVARIANT
	validate_tile_params<PixelVal>(tile_idx, data_buffer);

	read_tile_impl(tile_idx, data_buffer.data(), data_buffer.nof_cells() * sizeof(PixelVal));
}

template<class PixelVal> 	
auto Tiff::write_tile(Ssize tile_idx, const DataGrid<PixelVal>& data_buffer) -> void
{
	TWIST_CHECK_INVARIANT
	validate_tile_params<PixelVal>(tile_idx, data_buffer);

	write_tile_impl(tile_idx, data_buffer.data(), data_buffer.nof_cells() * sizeof(PixelVal));
}

template<class PixelVal>
auto Tiff::validate_tile_params(Ssize tile_idx, const DataGrid<PixelVal>& tile_data_buffer) const -> void
{
	TWIST_CHECK_INVARIANT
	validate_pixel_val_type<PixelVal>();

	if (!tile_fmt_info_) {
		TWIST_THROW(L"The pixel data in TIFF file \"%s\" is not organised in tiles.", path_.filename().c_str());
	}
    if (tile_idx >= get_tile_format_info().nof_tiles()) {
        TWIST_THROW(L"Tile index %lld out of range.", tile_idx);
    }  
	if (tile_data_buffer.nof_columns() != tile_fmt_info_->full_tile_width() || 
			tile_data_buffer.nof_rows() != tile_fmt_info_->full_tile_height()) {
		TWIST_THROW(L"Data buffer has the wrong dimensions (%lldx%lld); expected %lldx%lld.",
		            tile_data_buffer.nof_columns(), tile_data_buffer.nof_rows(), 
		            tile_fmt_info_->full_tile_width(), tile_fmt_info_->full_tile_height());
	}
}

template<class PixelVal>
auto Tiff::validate_pixel_val_type() const -> void
{
	TWIST_CHECK_INVARIANT
	// First, compile-time check for supported types
	static_assert(is_one_of<PixelVal, int8_t, uint8_t, int16_t, uint16_t, int32_t, uint32_t, float, double>); 

	// Second, run-time check that the template pixel value argument matches the sample data type
	if (twist::gis::enum_for_geo_grid_data_type<PixelVal> != sample_data_type_) {
		TWIST_THROW(L"The template pixel value type \"%s\" does not match the TIFF sample data type.",
					type_wname<PixelVal>().c_str());
	}
}

}

/// @file tiff_utils.ipp
/// Inline implementation file for "tiff_utils.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/grid_utils.hpp"
#include "twist/gis/gis_globals.hpp"
#include "twist/math/numeric_utils.hpp"

namespace twist::img {

// --- Local functions --- 

template<class PixelVal>
auto copy_tile_pixel_data(const Tiff::DataGrid<PixelVal>& img_pixel_data, Tiff::DataGrid<PixelVal>& tile_pixel_data, 
                          Ssize tile_idx, const Tiff::TileFormatInfo& tile_fmt_info) -> void
{
    const auto [actual_tile_width, actual_tile_height] = tile_fmt_info.get_actual_tile_size(tile_idx);
    const auto [img_pixel_row, img_pixel_col] = tile_fmt_info.get_tile_top_left_pixel_row_col(tile_idx);

    // Copy each pixel row in the current tile across to the tile buffer
    auto pixel_pos = img_pixel_row * img_pixel_data.nof_columns() + img_pixel_col;
    auto buffer_pos = Ssize{0};
    for ([[maybe_unused]] auto i : IndexRange{actual_tile_height}) {

        auto src = img_pixel_data.data() + pixel_pos;
        std::copy(src, src + actual_tile_width, tile_pixel_data.data() + buffer_pos);

        pixel_pos += img_pixel_data.nof_columns();
        buffer_pos += tile_pixel_data.nof_columns();
    }
}

// --- Global functions --- 

template<class PixelVal>
auto read_tiled_tiff(const Tiff& tiff, bool flip_y) -> Tiff::DataGrid<PixelVal>
{
    const auto tile_fmt_info = tiff.get_tile_format_info();

    auto pixel_data = Tiff::DataGrid<PixelVal>{tiff.height(), tiff.width()};
    auto tile_data = Tiff::DataGrid<PixelVal>{tile_fmt_info.full_tile_height(), tile_fmt_info.full_tile_width()};

    for (auto i : IndexRange{tile_fmt_info.nof_tiles()}) {

        tiff.read_tile(i, tile_data);

        const auto [actual_tile_width, actual_tile_height] = tile_fmt_info.get_actual_tile_size(i);
        auto [pixel_row, pixel_col] = tile_fmt_info.get_tile_top_left_pixel_row_col(i);
        auto tile_begin_row = Ssize{0};
        
        if (flip_y) {
            flip_rows(tile_data);
            tile_begin_row = tile_fmt_info.full_tile_height() - actual_tile_height;
            pixel_row = tiff.height() - pixel_row - actual_tile_height;
        }        
        
        copy_into(tile_data, 
                  pixel_data, 
                  SubgridInfo{tile_begin_row, tile_begin_row + actual_tile_height - 1, 0, actual_tile_width - 1}, 
                  pixel_row,
                  pixel_col);
    }

    return pixel_data;
}

template<class PixelVal>
auto read_tiled_tiff(fs::path path, bool flip_y) -> std::tuple<Tiff::DataGrid<PixelVal>, Tiff>
{
    auto tiff = Tiff{Tiff::open_read, std::move(path)};
    auto pixel_data = read_tiled_tiff<PixelVal>(tiff, flip_y);
    return {std::move(pixel_data), std::move(tiff)};
}

template<class PixelVal>
auto write_tiled_tiff(fs::path path, const Tiff::DataGrid<PixelVal>& pixel_data, Ssize nof_cells_per_tile, 
                      bool bigtiff) -> Tiff
{
    using namespace twist::math;

	if (!is_multiple(nof_cells_per_tile, 256) || !is_perfect_square(nof_cells_per_tile)) {
		TWIST_THRO2(L"The number of cells per tile {} must be a multiple of 256 and a perfect square.",
			        nof_cells_per_tile);
	} 

	auto tiff = Tiff{Tiff::open_write, 
                     std::move(path), 
                     twist::gis::enum_for_geo_grid_data_type<PixelVal>,
	                 pixel_data.nof_columns(), 
                     pixel_data.nof_rows(), 
                     1/*nof_planes*/,
                     bigtiff};
	
    const auto nof_cells_on_tile_side = perfect_square_root(nof_cells_per_tile);
	tiff.set_tile_format(nof_cells_on_tile_side, nof_cells_on_tile_side);
	const auto tile_fmt_info = tiff.get_tile_format_info();
	
	auto tile_pixel_data = Tiff::DataGrid<PixelVal>{tile_fmt_info.full_tile_height(), tile_fmt_info.full_tile_width()}; 

    for (auto i : IndexRange{tile_fmt_info.nof_tiles()}) {
        copy_tile_pixel_data(pixel_data, tile_pixel_data, i, tile_fmt_info);            
        tiff.write_tile(i, tile_pixel_data);
    } 

    return tiff;
}

template<class PixelVal>
auto read_pixel_scanlined(const Tiff& tiff, Ssize row, Ssize col, bool flip_y) -> PixelVal
{
    if (flip_y) {
        row = tiff.height() - row - 1;
    }
    auto data_grid = tiff.read_scanlined<PixelVal>(SubgridInfo{row, row, col, col}, 0/*plane*/);
    assert(data_grid.nof_rows() == 1 && data_grid.nof_columns() == 1);
    return data_grid(0, 0);
}

template<class PixelVal>
[[nodiscard]] auto read_pixel_data(const Tiff& tiff, bool flip_y) -> Tiff::DataGrid<PixelVal>
{
    if (tiff.nof_planes() != 1) {
        TWIST_THRO2(L"This function only works with single-plane TIFFs.");
    }
    if (tiff.is_tiled()) {
        return read_tiled_tiff<PixelVal>(tiff, flip_y);
    }
    auto data = tiff.read_scanlined<PixelVal>(std::nullopt/*subgrid_info*/, 0/*plane*/);
    if (flip_y) {
        flip_rows(data);
    }
    return data;
}

template<class PixelVal>
[[nodiscard]] auto compare_pixel_data(const Tiff& tiff1,
                                      const Tiff& tiff2,
                                      PixelVal tol,
                                      bool flip_y1,
                                      bool flip_y2) -> bool
{
    if (tiff1.width() != tiff2.width() || tiff1.height() != tiff2.height()) {
        return false;
    }
    return rg::equal(read_pixel_data<PixelVal>(tiff1, flip_y1), 
                         read_pixel_data<PixelVal>(tiff2, flip_y2), 
                         [tol](auto p1, auto p2) { return twist::math::equal_tol(p1, p2, tol); });
}

}

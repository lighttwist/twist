/// @file simple_svg_ex.hpp
/// Implementation file for "simple_svg_ex.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/img/svg/simple_svg_ex.hpp"

namespace twist::img::svg {

[[nodiscard]] auto to_svg_point(twist::gfx::WorldPoint pt) -> svg::Point
{
    return svg::Point{pt.x(), pt.y()};
}

[[nodiscard]] auto to_world_point(const svg::Point& pt) -> twist::gfx::WorldPoint
{
    return twist::gfx::WorldPoint{pt.x, pt.y};
}

[[nodiscard]] auto to_svg_line(const twist::gfx::WorldSegment& segm, const Stroke& stroke) -> svg::Line
{
    return svg::Line{to_svg_point(segm.get_p1()), to_svg_point(segm.get_p2()), stroke};
}

[[nodiscard]] auto to_svg_colour(twist::Colour colour) -> svg::Color
{
    return svg::Color{colour.red(), colour.green(), colour.blue()};
}

} 

/// @file simple_svg_ex.hpp
/// Extensions to the SVG code in "simple_svg.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef SIMPLE_SVG_EX_HPP
#define SIMPLE_SVG_EX_HPP

#include "twist/Colour.hpp"
#include "twist/gfx/graphics.hpp"
#include "twist/img/svg/simple_svg.hpp"

#include <cassert>
#include <memory>
#include <vector>

namespace twist::img::svg {

/*! The "path" shape. This is the most general shape that can be used in SVG. Using a path element, you can draw 
    rectangles (with or without rounded corners), circles, ellipses, polylines, and polygons - basically any of the 
    other types of shapes, bezier curves, quadratic curves, and more. A path contains a list of commands.
    This class currenty supports a subset of the path commands.
*/
class PathEx : public Serializeable {
public:
    //! Abstract base for command classes.
    class Command {
    public:
        virtual ~Command()
        {
        }

        [[nodiscard]] virtual auto toString(const Layout& layout) const -> std::string = 0;

    protected:
        Command()
        {
        }
    };

    //! The absolute move command.
    class Move : public Command {
    public:
        Move(Point pt)
            : pt_{pt}
        {
        }

        [[nodiscard]] auto point() const -> Point
        {
            return pt_;
        }

        [[nodiscard]] auto toString(const Layout& layout) const -> std::string override
        {
            return std::format("M {} {}", translateX(pt_.x, layout), translateY(pt_.y, layout));
        }

    private:
        Point pt_;
    };

    //! The absolute arc command.
    class Arc : public Command {
    public:
        Arc(double rx, double ry, double x_axis_rotation, bool large_arc, bool sweep, Point end_pt)
            : rx_{rx}
            , ry_{ry}
            , x_axis_rotation_{x_axis_rotation}
            , large_arc_{large_arc}
            , sweep_{sweep}
            , end_pt_{end_pt}
        {
        }

        [[nodiscard]] auto rx() const -> double
        {
            return rx_;
        }

        [[nodiscard]] auto ry() const -> double
        {
            return ry_;
        }

        [[nodiscard]] auto x_axis_rotation() const -> double
        {
            return x_axis_rotation_;
        }

        [[nodiscard]] auto large_arc() const -> bool
        {
            return large_arc_;
        }

        [[nodiscard]] auto sweep_flag() const -> bool
        {
            return sweep_;
        }

        [[nodiscard]] auto end_point() const -> Point
        {
            return end_pt_;
        }

        [[nodiscard]] auto toString(const Layout& layout) const -> std::string override
        {
            return std::format("A {} {} {} {} {} {} {}", 
                               rx_, ry_,
                               x_axis_rotation_,
                               large_arc_ ? "1" : "0",
                               sweep_ ? "1" : "0",
                               translateX(end_pt_.x, layout), translateY(end_pt_.y, layout));
        }

    private:
        double rx_;
        double ry_;
        double x_axis_rotation_;
        bool large_arc_;
        bool sweep_;
        Point end_pt_;
    };

    PathEx()
    {
    }

    template<std::derived_from<Command> T>
    auto operator<<(T&& cmd) -> PathEx&
    {
        commands_.push_back(std::make_shared<T>(std::move(cmd)));
        return *this;
    }

    [[nodiscard]] auto toString(const Layout& layout) const -> std::string override
    {
        assert(!commands_.empty());

        auto ss = std::stringstream{};
        ss << elemStart("path") << "d=\"";
        auto first = true;
        for (const auto& cmd : commands_) {
            if (first) {
                first = false;
            }
            else {
                ss << " ";
            }
            ss << cmd->toString(layout);
        }
        ss << "\" " << emptyElemEnd();
        return ss.str();
    }

private:
    std::vector<std::shared_ptr<Command>> commands_;
};

//! Text shape, horizontally and vertically centred about a point.
class CentredText : public Shape {
public:
    // https://stackoverflow.com/questions/5546346/how-to-place-and-center-text-in-an-svg-rectangle
    // https://stackoverflow.com/questions/28128491/center-text-in-circle-with-svg

    CentredText(Point centre, 
                std::string content,
                Fill fill = Fill{}, 
                Font font = Font{},
                Stroke stroke = Stroke{})
        : Shape{fill, stroke}
        , centre_{centre} 
        , content_{move(content)}
        , font_{std::move(font)}
    {
    }

    [[nodiscard]] auto toString(const Layout& layout) const -> std::string override
    {
        auto ss = std::stringstream{};
        ss << elemStart("text") << attribute("x", translateX(centre_.x, layout)) 
                                << attribute("y", translateY(centre_.y, layout))
                                << "text-anchor=\"middle\" dominant-baseline=\"middle\" "
                                << fill.toString(layout) << stroke.toString(layout)
                                << font_.toString(layout) << ">" << content_ 
           << elemEnd("text");

        return ss.str();
    }

    auto offset(const Point& offset) -> void override
    {
        centre_.x += offset.x;
        centre_.y += offset.y;
    }

    //! Get the bounding box of the text. 
    [[nodiscard]] auto getBoundingBox() const -> Box
    {
        //+TODO: The result is not great. Improve. DAN 23Jan'25
        double width = measureTextWidth(content_, font_);
        double height = measureTextHeight(font_);
        return Box{Point{centre_.x - width / 2.0, centre_.y - height / 2.0}, Size{width, height}};
    }

private:
    static double measureTextWidth(std::string const &text, Font const &font) 
    {
        // Implement text width measurement based on font
        return text.length() * font.getSize() / 1.5; // approximation
    }

    static double measureTextHeight(Font const &font) 
    {
        // Implement text height measurement based on font
        return font.getSize(); // approximation
    }

    Point centre_; 
    std::string content_;
    Font font_;
};

// --- Free functions ---

[[nodiscard]] auto to_svg_point(twist::gfx::WorldPoint pt) -> svg::Point;

[[nodiscard]] auto to_world_point(const svg::Point& pt) -> twist::gfx::WorldPoint;

[[nodiscard]] auto to_svg_line(const twist::gfx::WorldSegment& segm, const Stroke& stroke = Stroke{}) -> svg::Line;

[[nodiscard]] auto to_svg_colour(twist::Colour colour) -> svg::Color;

} 

#endif

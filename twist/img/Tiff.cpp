/// @file Tiff.cpp
/// Implementation file for "Tiff.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/img/Tiff.hpp"

#include "twist/os_utils.hpp"
#include "twist/type_info_utils.hpp"

#include "external/libtiff/include/tiffio.h"

#define CHECK_TIFF_FUNC(ret)  if (ret != 1) TWIST_THROW(L"Error returned by %s", ansi_to_string(#ret).c_str());

using namespace twist::gis;
using namespace twist::math;

namespace twist::img {

// --- Local functions ---

GeoGridDataType sample_format_to_geogrid_data_type(uint16_t sample_format, uint16_t bits_per_sample)
{
	if (sample_format == SAMPLEFORMAT_UINT) {  // unsigned integer data 
		if (bits_per_sample == 8) {
			return GeoGridDataType::uint8;
		}
		if (bits_per_sample == 16) {
			return GeoGridDataType::uint16;
		}
		if (bits_per_sample == 32) {
			return GeoGridDataType::uint32;
		}
		TWIST_THROW(L"Unsupported data type \"SAMPLEFORMAT_UINT\" with %d bits-per-sample.", bits_per_sample);
	}
	else if (sample_format == SAMPLEFORMAT_INT) {  // signed integer data 
		if (bits_per_sample == 8) {
			return GeoGridDataType::int8;
		}
		if (bits_per_sample == 16) {
			return GeoGridDataType::int16;
		}
		if (bits_per_sample == 32) {
			return GeoGridDataType::int32;
		}
		TWIST_THROW(L"Unsupported data type \"SAMPLEFORMAT_INT\" with %d bits-per-sample.", bits_per_sample);
	}
	else if (sample_format == SAMPLEFORMAT_IEEEFP) {  // IEEE floating point data 
		if (bits_per_sample == 32) {
			return GeoGridDataType::float32;
		}
		if (bits_per_sample == 64) {
			return GeoGridDataType::float64;
		}
		TWIST_THROW(L"Unsupported data type \"IEEEFP\" with %d bits-per-sample.", bits_per_sample);
	}
	else if (sample_format == SAMPLEFORMAT_VOID) {  // untyped data
		TWIST_THROW(L"Unsupported data type \"VOID\".");
	}
	else if (sample_format == SAMPLEFORMAT_COMPLEXINT) {  // complex signed int 
		TWIST_THROW(L"Unsupported data type \"COMPLEXINT\".");
	}
	else if (sample_format == SAMPLEFORMAT_COMPLEXIEEEFP) {  // complex IEE floating point 
		TWIST_THROW(L"Unsupported data type \"COMPLEXIEEEFP\".");
	}
	else {
		TWIST_THROW(L"Unrecognized data type ID %d.", sample_format);
	}
}


std::tuple<uint16_t /*sample_format*/, uint16_t /*bits_per_sample*/> 
geogrid_data_type_to_sample_format(GeoGridDataType valtype)
{
	switch (valtype) {
	case GeoGridDataType::uint8: return std::make_tuple(uint16_t{SAMPLEFORMAT_UINT}, uint16_t{8});
	case GeoGridDataType::uint16: return std::make_tuple(uint16_t{SAMPLEFORMAT_UINT}, uint16_t{16});
	case GeoGridDataType::uint32: return std::make_tuple(uint16_t{SAMPLEFORMAT_UINT}, uint16_t{32});
	case GeoGridDataType::int8: return std::make_tuple(uint16_t{SAMPLEFORMAT_INT}, uint16_t{8});
	case GeoGridDataType::int16: return std::make_tuple(uint16_t{SAMPLEFORMAT_INT}, uint16_t{16});
	case GeoGridDataType::int32: return std::make_tuple(uint16_t{SAMPLEFORMAT_INT}, uint16_t{32});
	case GeoGridDataType::float32: return std::make_tuple(uint16_t{SAMPLEFORMAT_IEEEFP}, uint16_t{32});
	case GeoGridDataType::float64: return std::make_tuple(uint16_t{SAMPLEFORMAT_IEEEFP}, uint16_t{64});
	default:
		TWIST_THROW(L"Unsupported geo-grid data type value '%d'.", valtype);
	}
}


TIFFDataType geogrid_to_tiff_data_type(GeoGridDataType valtype)
{
	switch (valtype) {
	case GeoGridDataType::uint8: return TIFF_BYTE;
	case GeoGridDataType::uint16: return TIFF_SHORT;
	case GeoGridDataType::uint32: return TIFF_LONG;
	case GeoGridDataType::int8: return TIFF_SBYTE;
	case GeoGridDataType::int16: return TIFF_SSHORT;
	case GeoGridDataType::int32: return TIFF_SLONG;
	case GeoGridDataType::float32: return TIFF_FLOAT;
	case GeoGridDataType::float64: return TIFF_DOUBLE;
	default:
		TWIST_THROW(L"Unsupported geo-grid data type value '%d'.", valtype);
	}
}


std::wstring assemble_message(const char* module, const char* format, va_list args)
{
	static const size_t buffer_size = 512;
	static char buffer[buffer_size];

	vsnprintf(buffer, buffer_size, format, args);

	auto message = ansi_to_string(buffer);
	if (module != nullptr) {
		message += L" [" + ansi_to_string(module) + L"].";
	}

	return message;
}


void error_handler(const char* module, const char* format, va_list args)
{
	TWIST_THROW(assemble_message(module, format, args).c_str());
}


void warning_handler(const char* module, const char* format, va_list args)
{
	out_debug_str(L"\n" + assemble_message(module, format, args));
}

//+TODO: To do things properly when writing a geotiff, we should use a tag extender as below. This will avoid an IDF 
//       rewrite when non-standard fields are defined. 
//       Note however that writing the IDF before the image data seems impossible via libtiff:
//       "libtiff provides no mechanism for controlling the placement of data in a file; image data is typically 
//       written before directory information."  http://www.libtiff.org/libtiff.html  DAN 15Aug'24
//
//  auto _ParentExtender = TIFFSetTagExtender(_XTIFFDefaultDirectory);
// 
//static void _XTIFFDefaultDirectory(TIFF *tif)
//{
//	auto def_field = [tif](uint32_t tag, TIFFDataType data_type, bool passcount = true) {
//		auto field_name = format_ansi("Tag {%d}", tag);
//
//		TIFFFieldInfo field_info;
//		field_info.field_tag = tag;
//		field_info.field_readcount = TIFF_VARIABLE2;
//		field_info.field_writecount = TIFF_VARIABLE2;	
//		field_info.field_type = data_type;
//		field_info.field_bit = FIELD_CUSTOM;  // bit in fieldsset bit vector 		
//		field_info.field_oktochange = true;  // if true, can change while writing 
//		field_info.field_passcount = passcount;  // if true, pass dir count on set
//		field_info.field_name = field_name.data();		
//	
//		TIFFFieldInfo fields_info[]{ field_info };
//		if (TIFFMergeFieldInfo(tif, fields_info, 1) != 0) {
//			TWIST_THROW(L"Call to TIFFMergeFieldInfo() failed.");
//		}    
//	};
//
//	auto def_data_type_field = [tif, def_field](uint32_t tag, GeoGridDataType data_type) {
//		def_field(tag, geogrid_to_tiff_data_type(data_type));
//	};
//	auto def_ascii_field = [tif, def_field](uint32_t tag, bool passcount = true) {
//		def_field(tag, TIFF_ASCII, passcount);
//	};
//
//	// Tag values for the fields where TIFFs store GeoTIFF-specific information
//	static const auto model_pixel_scale_tifftag = 33550u;
//	static const auto model_transformation_tag  = 34264u; 
//	static const auto model_tie_point_tifftag   = 33922u;
//	static const auto geo_key_directory_tifftag = 34735u;  // Alias: ProjectionInfoTag, CoordSystemInfoTag
//	static const auto geo_double_params_tifftag = 34736u; 
//	static const auto geo_ascii_params_tifftag  = 34737u;
//	static const auto nodata_tifftag            = 42113u;
//
//	def_data_type_field(model_pixel_scale_tifftag, GeoGridDataType::float64); 
//	def_data_type_field(model_transformation_tag, GeoGridDataType::float64); 
//	def_data_type_field(model_tie_point_tifftag, GeoGridDataType::float64); 	
//	def_data_type_field(geo_key_directory_tifftag, GeoGridDataType::uint16);
//	def_data_type_field(geo_double_params_tifftag, GeoGridDataType::float64);
//	def_ascii_field(geo_ascii_params_tifftag, false);  
//	def_ascii_field(nodata_tifftag);
//
//  if (_ParentExtender)
//      (*_ParentExtender)(tif);
//}

// --- Tiff::TileFormatInfo class ---

Tiff::TileFormatInfo::TileFormatInfo(Ssize full_tile_width, Ssize full_tile_height, Ssize img_width, Ssize img_height)
	: full_tile_width_{full_tile_width}
	, full_tile_height_{full_tile_height}
	, img_width_{img_width}
	, img_height_{img_height}
{
	const auto [tile_row_quot, tile_row_rem] = std::div(img_height_, full_tile_height_);
	nof_tile_rows_ = tile_row_quot + (tile_row_rem != 0 ? 1 : 0);

	const auto [tile_col_quot, tile_col_rem] = std::div(img_width_, full_tile_width_);
	nof_tile_cols_ = tile_col_quot + (tile_col_rem != 0 ? 1 : 0);
	TWIST_CHECK_INVARIANT
}

auto Tiff::TileFormatInfo::nof_tiles() const -> Ssize
{
	TWIST_CHECK_INVARIANT
	return nof_tile_rows_ * nof_tile_cols_;
}

auto Tiff::TileFormatInfo::nof_tile_rows() const -> Ssize
{
	TWIST_CHECK_INVARIANT
	return nof_tile_rows_;
}

auto Tiff::TileFormatInfo::nof_tile_columns() const -> Ssize
{
	TWIST_CHECK_INVARIANT
	return nof_tile_cols_;
}

auto Tiff::TileFormatInfo::full_tile_width() const -> Ssize
{
	TWIST_CHECK_INVARIANT
	return full_tile_width_;
}

auto Tiff::TileFormatInfo::full_tile_height() const -> Ssize
{
	TWIST_CHECK_INVARIANT
	return full_tile_height_;
}

auto Tiff::TileFormatInfo::right_edge_tile_width() const -> Ssize
{
	TWIST_CHECK_INVARIANT
	const auto nof_cols_beyond_full_tiles = img_width_ % full_tile_width_;
	return nof_cols_beyond_full_tiles != 0 ? nof_cols_beyond_full_tiles : full_tile_width_;
}

auto Tiff::TileFormatInfo::bottom_edge_tile_height() const -> Ssize
{
	TWIST_CHECK_INVARIANT
	const auto nof_rows_beyond_full_tiles = img_height_ % full_tile_height_;
	return nof_rows_beyond_full_tiles != 0 ? nof_rows_beyond_full_tiles : full_tile_height_;
}

auto Tiff::TileFormatInfo::get_actual_tile_size(Ssize tile_idx) const -> std::tuple<Ssize, Ssize>
{
	TWIST_CHECK_INVARIANT
	const auto tile_width = (tile_idx + 1) % nof_tile_cols_ != 0 ? full_tile_width_ 
	                                                             : right_edge_tile_width();
	const auto tile_height = tile_idx / nof_tile_cols_ < nof_tile_rows_ - 1 ? full_tile_height_ 
	                                                                        : bottom_edge_tile_height();
	return {tile_width, tile_height};
}

auto Tiff::TileFormatInfo::get_tile_row_col(Ssize tile_idx) const -> std::tuple<Ssize, Ssize>
{
	TWIST_CHECK_INVARIANT
	return {tile_idx / nof_tile_cols_, tile_idx % nof_tile_cols_};
}

auto Tiff::TileFormatInfo::get_tile_top_left_pixel_row_col(Ssize tile_idx) const -> std::tuple<Ssize, Ssize>
{
	TWIST_CHECK_INVARIANT
	const auto [tile_row, tile_col] = get_tile_row_col(tile_idx);
	return {tile_row * full_tile_height_, tile_col * full_tile_width_};
}

#ifdef _DEBUG
auto Tiff::TileFormatInfo::check_invariant() const noexcept -> void
{
	assert(full_tile_width_ > 0);
	assert(full_tile_height_ > 0);
	assert(img_width_ >= full_tile_width_);
	assert(img_height_ >= full_tile_height_);
	assert(nof_tile_rows_ > 0);
	assert(nof_tile_cols_ > 0);
}
#endif

// --- Tiff::Impl struct ---

struct Tiff::Impl {

	Impl(const fs::path& path, const std::string& open_mode) 
		: handle_{nullptr}
	{ 
		TIFFSetErrorHandler(error_handler);
		TIFFSetWarningHandler(warning_handler);

		if (open_mode.starts_with("r") && !exists(path)) {
			TWIST_THRO2(L"Cannot find TIFF file \"{}\".", path.c_str());
		}

		handle_ = TIFFOpen(path.string().c_str(), open_mode.c_str());
		TWIST_CHECK_INVARIANT
	}

	~Impl() 
	{ 
		TWIST_CHECK_INVARIANT_NO_SCOPE
		try {
			TIFFClose(handle_);
		}
		catch ([[maybe_unused]] const std::exception& ex) {
			assert(!ex.what());
		}
	}

	TIFF* handle() const 
	{ 
		TWIST_CHECK_INVARIANT
		return handle_; 
	}
	
	template<typename Val> 
	std::vector<Val> get_field_values(uint32_t tag) const
	{
		TWIST_CHECK_INVARIANT
		uint32_t field_count = 0;
		Val* values = nullptr;
		CHECK_TIFF_FUNC( TIFFGetField(handle_, tag, &field_count, &values) );

		return std::vector<Val>(values, values + field_count);
	}
	
	template<typename Val> 
	void set_field_values(uint32_t tag, const std::vector<Val>& values) 
	{
		TWIST_CHECK_INVARIANT
		CHECK_TIFF_FUNC( TIFFSetField(handle_, tag, values.size(), values.data()) );
	}
	
	void define_field(uint32_t tag, TIFFDataType data_type)
	{
		TWIST_CHECK_INVARIANT
		auto field_name = format_ansi("Tag {%d}", tag);

		TIFFFieldInfo field_info;
		field_info.field_tag = tag;
		field_info.field_readcount = TIFF_VARIABLE2;
		field_info.field_writecount = TIFF_VARIABLE2;	
		field_info.field_type = data_type;
		field_info.field_bit = FIELD_CUSTOM;  // bit in fieldsset bit vector 		
		field_info.field_oktochange = true;  // if true, can change while writing 
		field_info.field_passcount = true;  // if true, pass dir count on set
		field_info.field_name = field_name.data();		
	
		TIFFFieldInfo fields_info[]{ field_info };
		if (TIFFMergeFieldInfo(handle_, fields_info, 1) != 0) {
			TWIST_THROW(L"Call to TIFFMergeFieldInfo() failed.");
		}    
	}

private:
	TIFF* handle_;

	TWIST_CHECK_INVARIANT_DECL
};

#ifdef _DEBUG
void Tiff::Impl::check_invariant() const noexcept
{
	assert(handle_);
}
#endif

// --- Tiff class ---

Tiff::Tiff(OpenReadMode, fs::path path)
	: Tiff{std::move(path), "r"}
{
	TWIST_CHECK_INVARIANT
}

Tiff::Tiff(OpenReadWriteMode, fs::path path)
	: Tiff{std::move(path), "r+"}
{
	TWIST_CHECK_INVARIANT
}

Tiff::Tiff(OpenWriteMode, 
           fs::path path, 
		   GeoGridDataType sample_data_type, 
		   Ssize width, 
		   Ssize height, 
		   uint16_t nof_planes,
		   bool bigtiff)
	: impl_{std::make_unique<Impl>(path, bigtiff ? "w8" : "w")}
	, path_{std::move(path)}
	, width_{width}
	, height_{height}
	, sample_data_type_{sample_data_type}
	, nof_planes_{nof_planes}
{
	const auto [sample_format, bits_per_sample] = geogrid_data_type_to_sample_format(sample_data_type);
	bits_per_sample_ = bits_per_sample;

	// Set some of the fundamentals of the new file
	CHECK_TIFF_FUNC(TIFFSetField(impl_->handle(), TIFFTAG_BITSPERSAMPLE, bits_per_sample_));
	CHECK_TIFF_FUNC(TIFFSetField(impl_->handle(), TIFFTAG_SAMPLESPERPIXEL, nof_planes_));
	CHECK_TIFF_FUNC(TIFFSetField(impl_->handle(), TIFFTAG_IMAGEWIDTH, width_));
	CHECK_TIFF_FUNC(TIFFSetField(impl_->handle(), TIFFTAG_IMAGELENGTH, height_));
	CHECK_TIFF_FUNC(TIFFSetField(impl_->handle(), TIFFTAG_SAMPLEFORMAT, sample_format));

	sample_data_type_ = sample_format_to_geogrid_data_type(sample_format, bits_per_sample_);

	if (nof_planes_ > 1) {
		//+TODO: This is quite unusual but fits my current needs; it will be settable in the future. DAN 15Apr'23
		CHECK_TIFF_FUNC(TIFFSetField(impl_->handle(), TIFFTAG_PLANARCONFIG, PLANARCONFIG_SEPARATE));
		planar_config_ = PlanarConfig::separate;	
	}

	TWIST_CHECK_INVARIANT
}

Tiff::Tiff(OpenAppendMode, fs::path path, GeoGridDataType sample_data_type, Ssize width, Ssize height)
	: impl_{std::make_unique<Impl>(path, "a")}
	, path_{std::move(path)}
	, width_{width}
	, height_{height}
	, sample_data_type_{sample_data_type}
	, nof_planes_{1}
{
	const auto [sample_format, bits_per_sample] = geogrid_data_type_to_sample_format(sample_data_type);
	bits_per_sample_ = bits_per_sample;
	sample_data_type_ = sample_format_to_geogrid_data_type(sample_format, bits_per_sample_);

	//+TODO: If the image we are appending to is empty, set fundamentals
	//+TODO: If not, verify that sample_data_type, width, height match the image we are appending to
	TWIST_CHECK_INVARIANT
}

Tiff::Tiff(fs::path path, const std::string& open_read_mode)
	: impl_{std::make_unique<Impl>(path, open_read_mode)}
	, path_{std::move(path)}
	, width_{0}
	, height_{0}
	, sample_data_type_{GeoGridDataType{0}}
	, bits_per_sample_{0}
	, nof_planes_{0}
{
	// Get the number of bits per plane (bits per sample)
	CHECK_TIFF_FUNC( TIFFGetField(impl_->handle(), TIFFTAG_BITSPERSAMPLE, &bits_per_sample_) );

	// Get the number of planes (samples per pixel) 
	CHECK_TIFF_FUNC( TIFFGetField(impl_->handle(), TIFFTAG_SAMPLESPERPIXEL, &nof_planes_) );

	if (nof_planes_ > 1) {
		uint16_t planar_config = 0;
		CHECK_TIFF_FUNC(TIFFGetField(impl_->handle(), TIFFTAG_PLANARCONFIG, &planar_config));
		switch (planar_config) {
		case PLANARCONFIG_CONTIG:
			planar_config_ = PlanarConfig::contig;
			break;
		case PLANARCONFIG_SEPARATE:
			planar_config_ = PlanarConfig::separate;
			break;
		default:
			TWIST_THRO2(L"Unrecognised value {} for tag TIFFTAG_PLANARCONFIG.", planar_config);
		}
	}

	CHECK_TIFF_FUNC( TIFFGetField(impl_->handle(), TIFFTAG_IMAGEWIDTH, &width_) );
	CHECK_TIFF_FUNC( TIFFGetField(impl_->handle(), TIFFTAG_IMAGELENGTH, &height_) );

	if (TIFFIsTiled(impl_->handle()) != 0) {
		read_tile_format_info();	
	}

	uint16_t sample_format = 0;  // data sample format
	CHECK_TIFF_FUNC( TIFFGetField(impl_->handle(), TIFFTAG_SAMPLEFORMAT, &sample_format) );

	sample_data_type_ = sample_format_to_geogrid_data_type(sample_format, bits_per_sample_);

	TWIST_CHECK_INVARIANT
}


Tiff::Tiff(Tiff&& src)
	: impl_{std::move(src.impl_)}
    , path_{std::move(src.path_)}
	, width_{src.width_}
	, height_{src.height_}
	, sample_data_type_{src.sample_data_type_}
	, bits_per_sample_{src.bits_per_sample_}
	, nof_planes_{src.nof_planes_}
{
	swap(tile_fmt_info_, src.tile_fmt_info_);
}

Tiff::~Tiff()
{
	// We do not check the class invariant as move operations can leave the object in an invalid state
}

auto Tiff::path() const -> fs::path
{
	TWIST_CHECK_INVARIANT
	return path_;
}

auto Tiff::width() const -> Ssize
{
	TWIST_CHECK_INVARIANT
	return width_;
}

auto Tiff::height() const -> Ssize
{
	TWIST_CHECK_INVARIANT
	return height_;
}

auto Tiff::sample_data_type() const -> GeoGridDataType 
{
	TWIST_CHECK_INVARIANT
	return sample_data_type_;
}

auto Tiff::bits_per_sample() const -> uint16_t
{
	TWIST_CHECK_INVARIANT
	return bits_per_sample_;
}

auto Tiff::nof_planes() const -> uint16_t
{
	TWIST_CHECK_INVARIANT
	return nof_planes_;
}

auto Tiff::bits_per_pixel() const -> uint16_t
{
	TWIST_CHECK_INVARIANT
	return bits_per_sample_ * nof_planes_;
}

auto Tiff::is_tiled() const -> bool
{
	TWIST_CHECK_INVARIANT
	return tile_fmt_info_.has_value();
}

auto Tiff::get_tile_format_info() const -> const TileFormatInfo&
{
	TWIST_CHECK_INVARIANT
	if (!tile_fmt_info_) {
		TWIST_THROW(L"The pixel data in TIFF file \"%s\" is not organised in tiles.", path_.filename().c_str());
	}
	return *tile_fmt_info_;
}

auto Tiff::set_tile_format(Ssize full_tile_width, Ssize full_tile_height) -> void
{
	TWIST_CHECK_INVARIANT
	if (tile_fmt_info_) {
		TWIST_THROW(L"Tile format already set for TIFF file \"%s\".", path_.filename().c_str());
	}
    CHECK_TIFF_FUNC( TIFFSetField(impl_->handle(), TIFFTAG_TILEWIDTH, full_tile_width) );        
    CHECK_TIFF_FUNC( TIFFSetField(impl_->handle(), TIFFTAG_TILELENGTH, full_tile_height) );
	read_tile_format_info();
}

auto Tiff::is_bigtiff() const -> bool
{
	TWIST_CHECK_INVARIANT
	return TIFFIsBigTIFF(impl_->handle()) == 1;
}

auto Tiff::read_scanlined_int8(OptSubgridInfo subgrid_info, uint16_t plane) const -> DataGrid<int8_t>
{
	TWIST_CHECK_INVARIANT
	return read_scl<int8_t>(subgrid_info, plane);
}

auto Tiff::read_scanlined_uint8(OptSubgridInfo subgrid_info, uint16_t plane) const -> DataGrid<uint8_t>
{
	TWIST_CHECK_INVARIANT
	return read_scl<uint8_t>(subgrid_info, plane);
}

auto Tiff::read_scanlined_int16(OptSubgridInfo subgrid_info, uint16_t plane) const -> DataGrid<int16_t>
{
	TWIST_CHECK_INVARIANT
	return read_scl<int16_t>(subgrid_info, plane);
}

auto Tiff::read_scanlined_uint16(OptSubgridInfo subgrid_info, uint16_t plane) const -> DataGrid<uint16_t>
{
	TWIST_CHECK_INVARIANT
	return read_scl<uint16_t>(subgrid_info, plane);
}

auto Tiff::read_scanlined_int32(OptSubgridInfo subgrid_info, uint16_t plane) const -> DataGrid<int32_t>
{
	TWIST_CHECK_INVARIANT
	return read_scl<int32_t>(subgrid_info, plane);
}

auto Tiff::read_scanlined_uint32(OptSubgridInfo subgrid_info, uint16_t plane) const -> DataGrid<uint32_t>
{
	TWIST_CHECK_INVARIANT
	return read_scl<uint32_t>(subgrid_info, plane);
}

auto Tiff::read_scanlined_float32(OptSubgridInfo subgrid_info, uint16_t plane) const -> DataGrid<float>
{
	TWIST_CHECK_INVARIANT
	return read_scl<float>(subgrid_info, plane);
}

auto Tiff::read_scanlined_float64(OptSubgridInfo subgrid_info, uint16_t plane) const -> DataGrid<double>
{
	TWIST_CHECK_INVARIANT
	return read_scl<double>(subgrid_info, plane);
}

void Tiff::write_scanlined_int8(const int8_t* data, OptSubgridInfo subgrid_info, uint16_t plane)
{
	TWIST_CHECK_INVARIANT
	write_scl(data, subgrid_info, plane);
}

void Tiff::write_scanlined_uint8(const uint8_t* data, OptSubgridInfo subgrid_info, uint16_t plane)
{
	TWIST_CHECK_INVARIANT
	write_scl(data, subgrid_info, plane);
}

void Tiff::write_scanlined_int16(const int16_t* data, OptSubgridInfo subgrid_info, uint16_t plane)
{
	TWIST_CHECK_INVARIANT
	write_scl(data, subgrid_info, plane);
}

void Tiff::write_scanlined_uint16(const uint16_t* data, OptSubgridInfo subgrid_info, uint16_t plane)
{
	TWIST_CHECK_INVARIANT
	write_scl(data, subgrid_info, plane);
}

void Tiff::write_scanlined_int32(const int32_t* data, OptSubgridInfo subgrid_info, uint16_t plane)
{
	TWIST_CHECK_INVARIANT
	write_scl(data, subgrid_info, plane);
}

void Tiff::write_scanlined_uint32(const uint32_t* data, OptSubgridInfo subgrid_info, uint16_t plane)
{
	TWIST_CHECK_INVARIANT
	write_scl(data, subgrid_info, plane);
}

void Tiff::write_scanlined_float32(const float* data, OptSubgridInfo subgrid_info, uint16_t plane)
{
	TWIST_CHECK_INVARIANT
	write_scl(data, subgrid_info, plane);
}

void Tiff::write_scanlined_float64(const double* data, OptSubgridInfo subgrid_info, uint16_t plane)
{
	TWIST_CHECK_INVARIANT
	write_scl(data, subgrid_info, plane);
}

template<typename T> 
auto Tiff::read_scl(OptSubgridInfo subgrid_info, uint16_t plane) const -> DataGrid<T>
{
	TWIST_CHECK_INVARIANT
	if (plane != 0) {
		TWIST_THROW(L"Reading from planes other than 0 in not implemented yet.");
	}

	auto scanline_buf = prepare_scanline<T>(plane);

	const auto first_row = subgrid_info ? subgrid_info->begin_row() : 0;
	const auto  last_row = subgrid_info ? subgrid_info->end_row() : height_ - 1;
	const auto first_col = subgrid_info ? subgrid_info->begin_column() : 0;
	const auto  last_col = subgrid_info ? subgrid_info->end_column() : width_ - 1;
	const auto row_count = last_row - first_row + 1; 
	const auto col_count = last_col - first_col + 1; 

	auto matrix = DataGrid<T>{row_count, col_count};

	for (auto i = first_row; i <= last_row; ++i) {
		CHECK_TIFF_FUNC(TIFFReadScanline(impl_->handle(), scanline_buf.get(), static_cast<uint16_t>(i)));
		auto matrix_row_cells = matrix.view_row_cells(i - first_row);
		std::copy(scanline_buf.get() + first_col, scanline_buf.get() + last_col + 1, matrix_row_cells.begin());
	}

	return matrix;
}

template<typename T>
auto Tiff::write_scl(const T* data_buffer, OptSubgridInfo subgrid_info, uint16_t plane) -> void 
{
	TWIST_CHECK_INVARIANT
	if (subgrid_info) {
		TWIST_THROW(L"Tiff::write_scl() does not implement writing of subgrids yet!");
	}

	auto rows_per_strip = uint32_t{0};
	if (TIFFGetField(impl_->handle(), TIFFTAG_ROWSPERSTRIP, &rows_per_strip) == 1) {
		if (rows_per_strip != 1) {
			TWIST_THRO2(L"Unexpected TIFFTAG_ROWSPERSTRIP value {}; expected value 1.", rows_per_strip);
		}
	}
	else {			
		CHECK_TIFF_FUNC(TIFFSetField(impl_->handle(), TIFFTAG_ROWSPERSTRIP, 1));
	}

	auto scanline_buf = prepare_scanline<T>(plane);

	for (auto i : IndexRange{static_cast<uint32_t>(height_)}) {
		const auto buffer_row_cells = Range<const T*>{data_buffer + width_ * i, 
			                                          data_buffer + width_ * (i + 1)};
		rg::copy(buffer_row_cells, scanline_buf.get());
		CHECK_TIFF_FUNC(TIFFWriteScanline(impl_->handle(), scanline_buf.get(), i, plane));
	}
}

template<typename T> 
auto Tiff::prepare_scanline(uint16_t plane) const -> std::shared_ptr<T>
{
	TWIST_CHECK_INVARIANT
	if (tile_fmt_info_) {
		TWIST_THRO2(L"TIFF \"{}\" does not use scanlines (it is tiled).", path_.filename().c_str());
	}
	if (nof_planes_ <= plane) {
		TWIST_THRO2(L"TIFF \"{}\" contains {} planes. Invalid plane {} requested.", 
				    path_.filename().c_str(), nof_planes_, plane);
	}

	// Check that the bits-per-sample value matches the size of the template type
	const auto templ_type_bits = sizeof(T) * 8;
	if (bits_per_sample_ != templ_type_bits) {
		TWIST_THRO2(L"The size of type \"{}\" ({} bits) does not match the bits-per-sample value {} of TIFF \"{}\".", 
				    type_wname<T>().c_str(), templ_type_bits, bits_per_sample_, path_.filename().c_str());
	}	

	const auto scanline_bytes = TIFFScanlineSize(impl_->handle());
	assert(scanline_bytes / ssizeof<T> == width_);

	auto scanline_buf = std::shared_ptr<T>{static_cast<T*>(_TIFFmalloc(scanline_bytes)), _TIFFfree};
	assert(scanline_buf);

	return scanline_buf;
}

bool Tiff::has_field(uint32_t tag) const
{
	TWIST_CHECK_INVARIANT
	const auto* field_info = TIFFFindField(impl_->handle(), tag, TIFF_ANY);
	return field_info != nullptr;
}

void Tiff::define_field(uint32_t tag, GeoGridDataType data_type)
{
	TWIST_CHECK_INVARIANT
	impl_->define_field(tag, geogrid_to_tiff_data_type(data_type));
}

void Tiff::define_ascii_field(uint32_t tag)
{
	TWIST_CHECK_INVARIANT
	impl_->define_field(tag, TIFF_ASCII);
}

std::vector<int16_t> Tiff::get_int16_field_values(uint32_t tag) const
{
	TWIST_CHECK_INVARIANT
	return impl_->get_field_values<int16_t>(tag);
}

std::vector<uint16_t> Tiff::get_uint16_field_values(uint32_t tag) const
{
	TWIST_CHECK_INVARIANT
	return impl_->get_field_values<uint16_t>(tag);
}

void Tiff::set_uint16_field_values(uint32_t tag, const std::vector<uint16_t>& values)
{
	TWIST_CHECK_INVARIANT
	return impl_->set_field_values(tag, values);
}

std::vector<double> Tiff::get_double_field_values(uint32_t tag) const
{
	TWIST_CHECK_INVARIANT
	return impl_->get_field_values<double>(tag);
}

void Tiff::set_double_field_values(uint32_t tag, const std::vector<double>& values)
{
	TWIST_CHECK_INVARIANT
	impl_->set_field_values(tag, values);
}

std::string Tiff::get_string_field_value(uint32_t tag) const
{
	TWIST_CHECK_INVARIANT
	const auto chars = impl_->get_field_values<char>(tag);
	if (chars.empty()) return {};
	assert(chars.back() == '\0');
	return std::string{ begin(chars), end(chars) - 1 };
}

void Tiff::set_string_field_value(uint32_t tag, const std::string& value) const
{
	assert(!value.empty());

	const std::vector<char> chars{ value.c_str(), value.c_str() + value.size() + 1 };	
	assert(chars.back() == '\0');

	impl_->set_field_values(tag, chars);
}

auto Tiff::read_tile_format_info() -> void
{
	// No invariant checking: called in the ctor
	assert(!tile_fmt_info_);

	if (bits_per_sample_ % 8 != 0) {
		TWIST_THROW(L"Unusual \"bits per sample\" value %d, cannot compute tile size.", bits_per_sample_);
	}
	const auto bytes_per_sample = bits_per_sample_ / 8;

	auto bytes_per_tile = TIFFTileSize(impl_->handle());
	if (bytes_per_tile % bytes_per_sample != 0) {
		TWIST_THROW(L"Invalid \"bytes per tile\" value %d, cannot compute tile size.", bytes_per_tile);	
	}

	const auto bytes_per_tile_row = TIFFTileRowSize(impl_->handle());
	if (bytes_per_tile_row % bytes_per_sample != 0 || bytes_per_tile % bytes_per_tile_row != 0) {
		TWIST_THROW(L"Invalid \"bytes per tile row\" value %d, cannot compute tile size.", bytes_per_tile_row);	
	}

	const auto nof_pixel_cols_in_tile = static_cast<Ssize>(bytes_per_tile_row / bytes_per_sample);
	const auto nof_pixel_rows_in_tile = static_cast<Ssize>(bytes_per_tile / bytes_per_tile_row);

	tile_fmt_info_ = TileFormatInfo{nof_pixel_cols_in_tile, nof_pixel_rows_in_tile, width_, height_};

	assert(tile_fmt_info_->nof_tiles() == static_cast<Ssize>(TIFFNumberOfTiles(impl_->handle())));
}

auto Tiff::read_tile_impl(Ssize tile_idx, void* buffer, Ssize buffer_size) const -> void
{
	TWIST_CHECK_INVARIANT
	if (TIFFReadEncodedTile(impl_->handle(), static_cast<uint32_t>(tile_idx), buffer, buffer_size) == -1) {
		TWIST_THROW(L"Failed to read data for tile with index %lld.", tile_idx);
	}
}

auto Tiff::write_tile_impl(Ssize tile_idx, const void* buffer, Ssize buffer_size) -> void
{
	TWIST_CHECK_INVARIANT
	if (TIFFWriteEncodedTile(impl_->handle(), static_cast<uint32_t>(tile_idx), const_cast<void*>(buffer), buffer_size) 
			== -1) {
		TWIST_THROW(L"Failed to write data to tile with index %lld.", tile_idx);
	}
}

auto Tiff::flush() -> void
{
	TWIST_CHECK_INVARIANT
	if (TIFFFlush(impl_->handle()) == 0) {
		TWIST_THRO2(L"Failed to flush TIFF file \"{}\".", path_.filename().c_str());
	}
}

#ifdef _DEBUG
void Tiff::check_invariant() const noexcept
{
	assert(impl_);
	assert(!path_.empty());
	assert(width_ > 0);
	assert(height_ > 0);
	assert(sample_data_type_ >= GeoGridDataType::int8 && sample_data_type_ <= GeoGridDataType::float64);
	assert(bits_per_sample_ > 0);
	assert(nof_planes_ > 0);
}
#endif

}


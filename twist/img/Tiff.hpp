/// @file Tiff.hpp
/// Tiff class definition

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_IMG_TIFF_HPP
#define TWIST_IMG_TIFF_HPP

#include "twist/file_io.hpp"
#include "twist/grid_utils.hpp"
#include "twist/gis/GeoDataGridInfo.hpp"
#include "twist/gis/gis_globals.hpp"
#include "twist/math/FlatMatrix.hpp"

namespace twist::img {
class Tiff;
}
namespace twist::gis {
// Forward declarations for Tiff class friend functions.
class GeotiffProjection;
auto read_geo_data_grid_info(const twist::img::Tiff&, bool& flip_y) -> std::optional<twist::gis::GeoDataGridInfo>;	
auto read_geo_data_grid_info(const twist::img::Tiff&) -> std::optional<twist::gis::GeoDataGridInfo>;	
auto write_geo_data_grid_info(twist::img::Tiff&, const twist::gis::GeoDataGridInfo&) -> void;
auto read_projection_info(const twist::img::Tiff&) -> std::optional<twist::gis::GeotiffProjection>;
auto write_projection_info(twist::img::Tiff&, const twist::gis::GeotiffProjection&) -> void;
}

namespace twist::img {

//! An open connection to a TIFF (Tagged Image File Format) file.
class Tiff {
public:
	//! Tag types for constructors tag dispatching
	enum class OpenReadMode {};
	enum class OpenReadWriteMode {};
	enum class OpenWriteMode {};
	enum class OpenAppendMode {};

	//! Data grid structure used for storing the data contained in the image grid (or a subgrid thereof).
	template<class PixelVal> 
	using DataGrid = twist::math::FlatMatrix<PixelVal>;

	//! Optional type which specifies a subgrid of the image data grid.
	using OptSubgridInfo = std::optional<twist::SubgridInfo>;

	//! Information about the tile format for TIFFs whose pixel data is organised in tiles.
	class TileFormatInfo {
	public:
		//! The number of tiles in the image.
		[[nodiscard]] auto nof_tiles() const -> Ssize;

		/*! The number of "tile rows", ie how many tiles appear vertically one under the other in the image (if an  
		    incomplete tile appears at the bottom, it is included in the count).
		 */
		[[nodiscard]] auto nof_tile_rows() const -> Ssize;

		/*! The number of "tile columns", ie how many tiles appear horizontally one next to the other in the image (if 
		    an incomplete tile appears on the right, it is included in the count).
		 */
		[[nodiscard]] auto nof_tile_columns() const -> Ssize;

		//! The width of a "full" tile, ie a tile which does not intersect the right edge of the image (pixels).
		[[nodiscard]] auto full_tile_width() const -> Ssize;

		//! The height of a "full" tile, ie a tile which does not intersect the bottom edge of the image (pixels).
		[[nodiscard]] auto full_tile_height() const -> Ssize;

		/*! The width of the "edge" tiles in the rightmost tile column (pixels).
			\note If the tiles end exactly at the right edge of the image, then this is the same width as the "full"
				  tiles.
		 */
		[[nodiscard]] auto right_edge_tile_width() const -> Ssize;

		/*! The height of the "edge" tiles in the bottommost tile row (pixels).
			\note If the tiles end exactly at the right edge of the image, then this is the same width as the "full"
				  tiles.
		 */
		[[nodiscard]] auto bottom_edge_tile_height() const -> Ssize;

		/*! Get the size of a specific tile ("full" or "edge").
		    \param[in] tile_idx  The tile index; tiles are ordered left-to-right and top-to-bottom
		    \return  0. The tile width (pixels)
			         1. The tile height (pixels)
	     */
		[[nodiscard]] auto get_actual_tile_size(Ssize tile_idx) const -> std::tuple<Ssize, Ssize>;

		//! Get the tile row and column corresponding to tile index \p tile_idx.
		[[nodiscard]] auto get_tile_row_col(Ssize tile_idx) const -> std::tuple<Ssize, Ssize>;

		//! Get the pixel row and column, within the image, of the top-left pixe of the tile with index \p tile_idx.
		[[nodiscard]] auto get_tile_top_left_pixel_row_col(Ssize tile_idx) const -> std::tuple<Ssize, Ssize>;

	private:
		explicit TileFormatInfo(Ssize full_tile_width, Ssize full_tile_height, Ssize img_width, Ssize img_height);

		Ssize full_tile_width_;
		Ssize full_tile_height_;
		Ssize img_width_;
		Ssize img_height_;
		Ssize nof_tile_rows_{0};
		Ssize nof_tile_cols_{0};

		friend class Tiff;
		
		TWIST_CHECK_INVARIANT_DECL
	};

	//! Tag values for constructor tag dispatching
	static const OpenReadMode open_read{}; 
	static const OpenReadWriteMode open_read_write{}; 
	static const OpenWriteMode open_write{};
	static const OpenAppendMode open_append{}; 

	 /*! Constructor; open an existing TIFF file only for reading.	   
	    \param[in] tag  Tag object used for tag dispatching
	    \param[in] path  The TIFF file path
	  */ 
	explicit Tiff(OpenReadMode tag, fs::path path);

	 /*! Constructor; open an existing TIFF file for reading and writing.	   
	    \param[in] tag  Tag object used for tag dispatching
	    \param[in] path  The TIFF file path
	  */ 
	explicit Tiff(OpenReadWriteMode tag, fs::path path);

	/*! Constructor; open a new or existing TIFF file for writing; all data in an existing TIFF will be lost after the 
	    constructor call.	   
	    \param[in] tag  Tag object used for tag dispatching
	    \param[in] path  The TIFF file path
	    \param[in] sample_data_type  The type of a pixel value 
	    \param[in] width  The image width (pixels)
	    \param[in] height  The image height (pixels)
		\param[in] nof_planes  The number of planes in the TIFF; if bigger than one, the value for the "planar config" 
		                       tag will be set to "separate", which allows efficient calls to write_scanlined()
		\param[in] bigtiff  Whether the new TIFF will use the BigTIFF format (compulsory for all TIFFs bigger than 4GB)
	 */ 
	explicit Tiff(OpenWriteMode tag, 
				  fs::path path, 
				  twist::gis::GeoGridDataType sample_data_type, 
				  Ssize width, 
				  Ssize height,
				  uint16_t nof_planes = 1,
				  bool bigtiff = false);

	/*! Constructor; open a new or existing TIFF file for appending; existing data will not be touched, instead new 
	    data will be written as additional subfiles.	   
	    \param[in] tag  Tag object used for tag dispatching
	    \param[in] path  The TIFF file path
	    \param[in] sample_data_type  The type of a pixel value 
	    \param[in] width  The image width (pixels)
	    \param[in] height  The image height (pixels)
	 */ 
    explicit Tiff(OpenAppendMode tag,
                  fs::path path,
                  twist::gis::GeoGridDataType sample_data_type,
                  Ssize width,
                  Ssize height);

	Tiff(const Tiff&) = delete;

	Tiff(Tiff&& src);

	~Tiff();

	[[nodiscard]] auto operator=(const Tiff&) -> Tiff& = delete;

	[[nodiscard]] auto operator=(Tiff&&) -> Tiff& = delete;

	//! The TIFF file path.
	[[nodiscard]] auto path() const -> fs::path;

	//! The TIFF image width (pixels).
	[[nodiscard]] auto width() const -> Ssize;

	//! The TIFF image height (pixels).
	[[nodiscard]] auto height() const -> Ssize;
	
	//! The type ID for a pixel value.
	[[nodiscard]] auto sample_data_type() const -> twist::gis::GeoGridDataType;

	//! The size, in bits, of a pixel value, in one plane.
	[[nodiscard]] auto bits_per_sample() const -> uint16_t;

	//! The number of planes in the image.
	[[nodiscard]] auto nof_planes() const -> uint16_t;

	//! The size, in bits, of a pixel value across all planes in the image.
	[[nodiscard]] auto bits_per_pixel() const -> uint16_t;

	//! Whether the pixel data is organised in tiles.
	[[nodiscard]] auto is_tiled() const -> bool;

	//! Get information about the tile format. If the pixel data is not organised in tiles, an exception is thrown.
	[[nodiscard]] auto get_tile_format_info() const -> const TileFormatInfo&;

	/*! Set the format of the pixel data in the TIFF to "tiled" and set the tiling information.
	    \note If the tiling information has alredy been set, an exception is thrown.
		\param[in] full_tile_width  The width of a "full" tile, ie a tile which does not intersect the right edge of 
		                            the image (pixels)
		\param[in] full_tile_height  The height of a "full" tile, ie a tile which does not intersect the bottom edge of 
		                             the image (pixels)  
	 */
	auto set_tile_format(Ssize full_tile_width, Ssize full_tile_height) -> void;

	//! Whether the TIFF uses the BigTIFF format (which needs to be used by all TIFFs bigger than 4GB).
	[[nodiscard]] auto is_bigtiff() const -> bool;

	/*! Read, from an image following the "scanline" format, the data stored in the pixel grid (or a subgrid thereof), 
	    in a specific plane. If the image follows the "tiled" format, an exception is thrown.
	    Devnote: Reading from planes other than 0 in not implemented yet.	   
	    \tparam PixelVal  The type of a pixel value; a compile error occurs if it is not supported by this class
	    \param[in] subgrid_info  Information about the pixel subgrid whose data is to be read; or nullopt to read the 
		                         whole pixel grid
	    \param[in] plane  The plane index
        \param[in] flip_y  Whether the TIFF data is flipped on the Y axis (first row in the pixel data is the last row 
                           in model space)
	    \return  Data grid containing the pixel values read from the image
	 */   
	template<class PixelVal> 
	[[nodiscard]] auto read_scanlined(OptSubgridInfo subgrid_info = std::nullopt, 
                                      uint16_t plane = 0) const -> DataGrid<PixelVal>;

	/*! Set, in an image following the "scanline" format, the values of the pixels in the pixel grid (or a subgrid 
	    thereof), in a specific plane. If the image follows the "tiled" format, an exception is thrown.
	    Devnote: Writing to a subgrid is not implemented yet.
	    \tparam PixelVal  The type of a pixel value; a compile error occurs if it is not supported by this class
	    \param[in] data  Data grid containing the pixel values to be read from the image
	    \param[in] subgrid_info  Information about the pixel subgrid which data is to be written to (must match the 
								 dimensions of the input data grid); or nullopt to write to the whole pixel grid
	    \param[in] plane  The plane index
	 */	
	template<class PixelVal> 
	auto write_scanlined(const DataGrid<PixelVal>& data, 
	                     OptSubgridInfo subgrid_info = std::nullopt, 
	                     uint16_t plane = 0) -> void;

	/*! Set, in an image following the "scanline" format, the values of the pixels in the pixel grid (or a subgrid 
	    thereof), in a specific plane. The scanline height is set to one row. If the image follows the "tiled" format, 
		an exception is thrown.
	    Devnote: Writing to a subgrid is not implemented yet.
	    \tparam PixelVal  The type of a pixel value; a compile error occurs if it is not supported by this class
	    \param[in] data  Address of the buffer where the pixel values are stored contiguously, line after line starting
		                 from the top. The size of the buffer must be at least width() * height().
	    \param[in] subgrid_info  Information about the pixel subgrid which data is to be written to (must match the 
								 dimensions of the input data grid); or nullopt to write to the whole pixel grid
	    \param[in] plane  The plane index
	 */	
	template<class PixelVal> 
	auto write_scanlined(const PixelVal* data, OptSubgridInfo subgrid_info = std::nullopt, uint16_t plane = 0) 
	      -> void;

	/*! Read (and decompress, if necessary) a tile from the pixel data.
	    \tparam PixelVal  The type of a pixel value; a compile error occurs if it is not supported by this class
		\param[in] tile_idx  The tile index; tiles are ordered left-to-right and top-to-bottom 
		\param[in] data_buffer  Buffer where the data will be copied; must have the same dimensions as a full tile
	 */ 
	template<class PixelVal> 	
	auto read_tile(Ssize tile_idx, DataGrid<PixelVal>& data_buffer) const -> void;

	/*! Append (after decompressiong, if necessary) data to a tile of pixel data.
	    \tparam PixelVal  The type of a pixel value; a compile error occurs if it is not supported by this class
		\param[in] tile_idx  The tile index; tiles are ordered left-to-right and top-to-bottom 
		\param[in] data_buffer  Buffer containing the data to be copied; must have the same dimensions as a full tile
	 */ 
	template<class PixelVal> 	
	auto write_tile(Ssize tile_idx, const DataGrid<PixelVal>& data_buffer) -> void;

private:
	//! Storage organization for multiple planes
	enum class PlanarConfig {
		contig = 1, ///< single image plane 
		separate = 2 ///< separate planes of data
	};

	struct Impl;

	Tiff(fs::path path, const std::string& open_read_mode);

	[[nodiscard]] auto read_scanlined_int8(OptSubgridInfo subgrid_info, uint16_t plane) const -> DataGrid<int8_t>;

	[[nodiscard]] auto read_scanlined_uint8(OptSubgridInfo subgrid_info, uint16_t plane) const -> DataGrid<uint8_t>;

	[[nodiscard]] auto read_scanlined_int16(OptSubgridInfo subgrid_info, uint16_t plane) const -> DataGrid<int16_t>;

	[[nodiscard]] auto read_scanlined_uint16(OptSubgridInfo subgrid_info, uint16_t plane) const -> DataGrid<uint16_t>;

	[[nodiscard]] auto read_scanlined_int32(OptSubgridInfo subgrid_info, uint16_t plane) const -> DataGrid<int32_t>;

	[[nodiscard]] auto read_scanlined_uint32(OptSubgridInfo subgrid_info, uint16_t plane) const -> DataGrid<uint32_t>;

	[[nodiscard]] auto read_scanlined_float32(OptSubgridInfo subgrid_info, uint16_t plane) const -> DataGrid<float>;

	[[nodiscard]] auto read_scanlined_float64(OptSubgridInfo subgrid_info, uint16_t plane) const -> DataGrid<double>;

	void write_scanlined_int8(const int8_t* data, OptSubgridInfo subgrid_info, uint16_t plane);  

	void write_scanlined_uint8(const uint8_t* data, OptSubgridInfo subgrid_info, uint16_t plane);

	void write_scanlined_int16(const int16_t* data, OptSubgridInfo subgrid_info, uint16_t plane);  

	void write_scanlined_uint16(const uint16_t* data, OptSubgridInfo subgrid_info, uint16_t plane);

	void write_scanlined_int32(const int32_t* data, OptSubgridInfo subgrid_info, uint16_t plane);  

	void write_scanlined_uint32(const uint32_t* data, OptSubgridInfo subgrid_info, uint16_t plane);

	void write_scanlined_float32(const float* data, OptSubgridInfo subgrid_info, uint16_t plane);  

	void write_scanlined_float64(const double* data, OptSubgridInfo subgrid_info, uint16_t plane);

	template<class T> 
	[[nodiscard]] auto read_scl(OptSubgridInfo subgrid_info, uint16_t plane) const -> DataGrid<T>;

	template<class T> 
	auto write_scl(const T* data_buffer, OptSubgridInfo subgrid_info, uint16_t plane) -> void;

	template<class T> 
	[[nodiscard]] auto prepare_scanline(uint16_t plane) const -> std::shared_ptr<T>;

	bool has_field(uint32_t tag) const;

	void define_field(uint32_t tag, twist::gis::GeoGridDataType data_type);

	void define_ascii_field(uint32_t tag);

	std::vector<int16_t> get_int16_field_values(uint32_t tag) const;

	std::vector<uint16_t> get_uint16_field_values(uint32_t tag) const;

	void set_uint16_field_values(uint32_t tag, const std::vector<uint16_t>& values);

	std::vector<double> get_double_field_values(uint32_t tag) const;

	void set_double_field_values(uint32_t tag, const std::vector<double>& values);

	std::string get_string_field_value(uint32_t tag) const;

	void set_string_field_value(uint32_t tag, const std::string& value) const;

	auto read_tile_format_info() -> void;

	auto read_tile_impl(Ssize tile_idx, void* buffer, Ssize buffer_size) const -> void;

	auto write_tile_impl(Ssize tile_idx, const void* buffer, Ssize buffer_size) -> void;

	auto flush() -> void;

	template<class PixelVal>
	auto validate_tile_params(Ssize tile_idx, const DataGrid<PixelVal>& tile_data_buffer) const -> void;

	template<class PixelVal>
	auto validate_pixel_val_type() const -> void;

	std::unique_ptr<Impl> impl_;
	fs::path path_;
	Ssize width_;
	Ssize height_;
	twist::gis::GeoGridDataType sample_data_type_;
	uint16_t bits_per_sample_;
	uint16_t nof_planes_;
	std::optional<PlanarConfig> planar_config_;
	std::optional<TileFormatInfo> tile_fmt_info_;

	friend auto twist::gis::read_geo_data_grid_info(const Tiff&, bool& flip_y) 
                 -> std::optional<twist::gis::GeoDataGridInfo>;	
	friend auto twist::gis::write_geo_data_grid_info(Tiff&, const twist::gis::GeoDataGridInfo&) -> void;
	
	friend auto twist::gis::read_projection_info(const Tiff&) -> std::optional<twist::gis::GeotiffProjection>;
	friend auto twist::gis::write_projection_info(Tiff&, const twist::gis::GeotiffProjection&) -> void;

	TWIST_CHECK_INVARIANT_DECL
};

}

#include "twist/img/Tiff.ipp"

#endif

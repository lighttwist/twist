/// @file ExplicitLibType.hpp
/// ExplicitLibType class template 

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_EXPLICIT_LIB_TYPE_HPP
#define TWIST_EXPLICIT_LIB_TYPE_HPP

#include "twist/string_utils.hpp"

namespace twist {

/*! Class template that makes a basic numeric type into an explicit (separate) type. 
    \tparam TNum  The basic numeric type 
    \tparam t_def_value  The default value, that is, the value to which an instance of the class is intialised by the 
 					     default constructor
    \tparam t_lib_id  The unique ID of the owner library
    \tparam t_type_id  ID of the type; must be unique within the owner library
 */
template<typename TNum, TNum t_def_value, unsigned long t_lib_id, unsigned long t_type_id>
class ExplicitLibType {
public:
	//! The underlying numeric type
	using Type = TNum;

	//! The library ID
	static const unsigned long lib_id = t_lib_id;

	//! The default value of this type
	static inline const TNum def_value = t_def_value;

	//! Constructor. Initialises the new object to the default value.
	ExplicitLibType() : value_(t_def_value)
	{
	}
	
	//! Constructor. Initialises the new object to \p value.
	explicit ExplicitLibType(TNum value) : value_(value)
	{
	}

	ExplicitLibType(const ExplicitLibType& src) : value_(src.value_)
	{
	}

	ExplicitLibType& operator=(const ExplicitLibType& rhs)
	{
		if (this != &rhs) {
			value_ = rhs.value_;
		}
		return *this;
	}

	bool operator==(const ExplicitLibType& rhs) const
	{
		return value_ == rhs.value_;
	}

	bool operator<(const ExplicitLibType& rhs) const
	{
		return value_ < rhs.value_;
	}

	bool operator>(const ExplicitLibType& rhs) const
	{
		return value_ > rhs.value_;
	}

	bool operator<=(const ExplicitLibType& rhs) const
	{
		return value_ <= rhs.value_;
	}

	bool operator>=(const ExplicitLibType& rhs) const
	{
		return value_ >= rhs.value_;
	}

	bool operator!=(const ExplicitLibType& rhs) const
	{
		return value_ != rhs.value_;
	}

	ExplicitLibType& operator++()
	{
		++value_; 
		return *this;
	}	

 	ExplicitLibType operator++(int)
	{
		ExplicitLibType temp(*this);
		++(*this);
		return temp;	
	}

	//! The value.
	TNum value() const
	{
		return value_;
	}

	//! Whether the object has the default value.
	bool has_def_value() const
	{
		return value_ == def_value;
	}

private:	
	TNum value_;
};

//! Get a textual represtentation of an explicitly typed number object.
template<typename TNum, TNum t_def_value, unsigned long t_lib_id, unsigned long t_type_id>
[[nodiscard]] auto to_wstring(ExplicitLibType<TNum, t_def_value, t_lib_id, t_type_id> obj) -> std::wstring
{
	return std::to_wstring(obj.value());
}

} 

#endif 

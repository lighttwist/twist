/// @file "Colour.ipp"
/// Inline implementation file for "Colour.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace twist {

constexpr Colour::Colour(std::uint8_t r, std::uint8_t g, std::uint8_t b) 
	: colour_ref_{colour_ref_from_rgb(r, g, b)}
{ 
}

constexpr Colour::Colour(ColourRef colour_ref) 
	: colour_ref_{colour_ref}
{ 
}
	
constexpr auto Colour::rgb() const -> std::tuple<std::uint8_t, std::uint8_t, std::uint8_t>
{
	return {lobyte(colour_ref_),
            lobyte(((std::uint16_t)colour_ref_) >> 8),
            lobyte(colour_ref_ >> 16)};
}

constexpr auto Colour::red() const -> std::uint8_t
{
    return lobyte(colour_ref_);
}

constexpr auto Colour::green() const -> std::uint8_t
{
    return lobyte(((std::uint16_t)colour_ref_) >> 8);
}

constexpr auto Colour::blue() const -> std::uint8_t
{
    return lobyte(colour_ref_ >> 16);
}

constexpr auto Colour::colour_ref() const -> ColourRef
{
	return colour_ref_;
}

constexpr auto Colour::colour_ref_from_rgb(std::uint8_t r, std::uint8_t g, std::uint8_t b) -> ColourRef
{
	return r | (((std::uint16_t)g) << 8) | (((std::uint32_t)b) << 16);
}

static constexpr auto Colour::lobyte(auto x) -> std::uint8_t
{
	return (std::uint8_t)(((std::uint32_t)(x)) & 0xff);
}

//+TODO: Check in the class invariant that ColourRef is of the form 0x00rrggbb

} 


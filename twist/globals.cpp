/// @file globals.cpp
/// Implementation file for "globals.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/globals.hpp"

#include "twist/RuntimeError.hpp"
#include "twist/string_utils.hpp"

#include <cassert>
#include <cstdarg>
#include <cstring>
#include <sstream>
#include <vector>

namespace twist {

std::wstring get_strerror()
{
	return to_string(strerror(errno));
}

namespace detail {

auto make_runtime_error_new(std::wstring msg, const char* func_name) -> RuntimeError
{
	return RuntimeError{move(msg), ansi_to_string(func_name)};
}

auto make_runtime_error_new(std::string msg, const char* func_name) -> RuntimeError
{
	return RuntimeError{ansi_to_string(msg), ansi_to_string(func_name)};
}

auto make_runtime_error_old(const char* func_name, const wchar_t* msg_format, ...) -> RuntimeError
{
	auto args = static_cast<va_list>(nullptr);
	va_start(args, msg_format);
	auto msg = format_str(msg_format, args);
	va_end(args);

	return RuntimeError{move(msg), ansi_to_string(func_name)};
}

}

}

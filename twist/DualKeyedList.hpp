///  @file  DualKeyedList.hpp
///  DualKeyedList class template

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_DUAL_KEYED_LIST_HPP
#define TWIST_DUAL_KEYED_LIST_HPP

#include "string_utils.hpp"
#include "metaprogramming.hpp"

namespace twist {

///  A container of objects, which is efficiently keyed both on object ID and object name, and which retains 
///    the order of its elements (being, as such, more than a map). Both object IDs and object names are 
///    expected to be unique. 
///  The name matching can be either case sensitive of case insensitive. Locale functionality is built into 
///    this class, however it is yet to be exposed (e.g. passed into the constructor).
///
/// @tparam  Id  represents the type of the object IDs
/// @tparam  Obj  represents the type of the objects
/// @tparam  Chr  The string character type
/// @tparam  case_sens  compile-time flag specifying whether the name search should be case sensitive or not
///
template<typename Obj, typename Id, typename Chr, bool case_sens, bool owns_objects>
class DualKeyedList final {
public:
	using String = std::basic_string<Chr>;

	/// Class storing object pointer, ID and name.
	class Elem {
	public:		
		Elem(Obj* obj, Id id, String name) 
			: obj_{obj}
			, id_{id}
			, name_{move(name)}
		{ 
		}

		bool operator==(const Elem& rhs) const 
		{ 
			return id_ == rhs.id_; 
		}

		const Obj* obj() const 
		{ 
			return obj_; 
		}

		Obj* obj() 
		{ 
			return obj_; 
		}

		Id id() const 
		{ 
			return id_; 
		}

		const String& name() const 
		{ 
			return name_; 
		}

	private:
		friend class DualKeyedList<Obj, Id, Chr, case_sens, owns_objects>;	
		Obj* obj_;
		Id id_;
		String name_;
	};
	
	using List = std::vector<Elem>;

	// Iterator types; an iterator addresses an Elem object
	using const_iterator = typename List::const_iterator;  
	using iterator = typename List::iterator;

	/// Constructor.
	///
	/// @param[in] owns_objects  Whether this container owns its objects/elements
	///
	DualKeyedList();

	/// Destructor.
	~DualKeyedList();
	
	/// Copy the elements of another list into this one. Any elements existing in this list are lost
	///
	/// @param[in] src  The source list.
	///
	void assign(const DualKeyedList& src);

	/// Get a specific object from the list.
	///
	/// @param[in] pos  The position.
	/// @return  Ref to the object.
	///
	const Obj& at(Ssize pos) const;

	/// Get a specific object from the list.
	///
	/// @param[in] pos  The position.
	/// @return  Ref to the object.
	///
	Obj& at(Ssize pos);

	/// Look for a specific object in the list.
	///
	/// @param[in] id  The object ID. 
	/// @return  Pointer to the object, if an object with this ID is found in the list; nullptr otherwise.
	///
	const Obj* find(Id id) const;

	/// Look for a specific object in the list.
	///
	/// @param[in] id  The object ID. 
	/// @return  Pointer to the object, if an object with this ID is found in the list; nullptr otherwise.
	///
	Obj* find(Id id);
	
	/// Look for a specific object in the list.
	///
	/// @param[in] name  The object name. 
	/// @return  Pointer to the object, if an object with this name is found in the list; nullptr otherwise.
	///
	const Obj* find(const String& name) const;

	/// Look for a specific object in the list.
	///
	/// @param[in] name  The object name. 
	/// @return  Pointer to the object, if an object with this name is found in the list; nullptr otherwise.
	///
	Obj* find(const String& name);
			
	/// Get a specific object from the list.
	///
	/// @param[in] id  The object ID. An exception is thrown if no match is found.
	/// @return  The matching list element.
	///
	const Obj& get(Id id) const;
			
	/// Get a specific object from the list.
	///
	/// @param[in] id  The object ID. An exception is thrown if no match is found.
	/// @return  The matching list element.
	///
	Obj& get(Id id);

	/// Get the position of a specific object in the list. This is a lot less efficient than calling any of 
	/// the get() methods.
	///
	/// @param[in] id  The object ID
	/// @return  The position of the node withing the BBN node list, or k_no_pos if no node with this ID 
	///					is found
	///
	Ssize get_pos(Id id) const;
	
	/// Get a specific object from the list.
	///
	/// @param[in] name  The object name; if an object with this name is not found in the list, an exception 
	///					is thrown
	/// @return  The object
	///
	const Obj& get(const String& name) const;

	/// Get a specific object from the list.
	///
	/// @param[in] name  The object name. If an object with this name is not found in the list, an exception 
	///					is thrown.
	/// @return  The object
	///
	Obj& get(const String& name);

	/// Get the position of a specific object in the list. This is a lot less efficient than calling any of 
	/// the get() methods.
	///
	/// @param[in] name  The object name 
	/// @return  The position of the node withing the BBN node list, or k_no_pos if no object with this name 
	///					is found
	///
	Ssize get_pos(const String& name) const;

	/// Get the ID value corresponding to a specific name.
	///
	/// @param[in] name  The name; if this name is not found as a key in the list, an exception is thrown
	/// @return  The ID value
	///
	Id get_id_for_name(const String& name) const;

	/// Get an iterator addressing the first element in the list. If the list is empty, the method returns the 
	/// same iterator as end().
	///
	/// @return  The iterator.
	///	
	const_iterator begin() const;

	/// Get an iterator addressing the first element in the list. If the list is empty, the method returns the 
	/// same iterator as end().
	///
	/// @return  The iterator.
	///	
	iterator begin();

	/// Get an iterator addressing one past the last element of the list. 
	///
	/// @return  The iterator.
	///
	const_iterator end() const;

	/// Get an iterator addressing one past the last element of the list. 
	///
	/// @return  The iterator.
	///
	iterator end();

	/// Get the number of elements in the list.
	///
	/// @return  The list size.
	///
	Ssize size() const;

	/// Find out whether the list is empty.
	///
	/// @return  true if the list contains no elements, false otherwise
	///
	bool empty() const;

	/// Add an object at the end of the list. If the object, the ID or the name already exist in the list, an 
	/// exception is thrown (as the list stores unique objects, so it will not store the same pointer twice).
	///
	/// @param[in] obj  The object to be added to the list. If the list owns its elements, it will take 
	///					ownership of the pointer
	/// @param[in] id  The ID key for this object
	/// @param[in] name  The name key for this object
	/// @return  The object which was added to the list
	///
	Obj& push_back(Obj* obj, Id id, const String& name);

	/// Erase an element from the list. If the list owns its elements, the corresponding object will be destroyed.
	///
	/// @param[in] id  The object ID. If an object with this ID is not found in the list, an exception is thrown.	
	///
	void erase(Id id);

	/// Erase an element from the list. If the list owns its elements, the corresponding object will 
	/// be destroyed.
	///
	/// @param[in] name  The object name. If an object with this name is not found in the list, an exception 
	///					is thrown.	
	///
	void erase(const String& name);
	
	/// Extract an object from the list. The corresponding list element is erased, but the object is not 
	/// destroyed.
	///
	/// @param[in] id  The object ID; if an object with this ID is not found in the list, an exception 
	///					is thrown
	/// @return  Pointer to the object extracted; if this list owned the object, then caller takes ownership
	///
	Obj* extract(Id id);

	/// Extract an object from the list. The corresponding list element is erased, but the object is not 
	/// destroyed.
	///
	/// @param[in] name  The object name; if an object with this name is not found in the list, an exception 
	///					is thrown
	/// @return  Pointer to the object extracted; if this list owned the object, then caller takes ownership
	///
	Obj* extract(const String& name);

	/// Clear the list. All elements are erased and, if the list owns its elements, all objects are destroyed.
	///
	void clear();
	
	/// Sort the list by object ID (ascending). Extremely efficient.
	///
	void sort_by_id();  

	/// Sort the list by object name (ascending). Extremely efficient.
	///
	void sort_by_name();
	
	/// Change an object name (a name key) in the list. The object keeps its position in the list.
	///
	/// @param[in] old_name  The old name key. An exception is thrown if an object with this name is not 
	///					found in the list
	/// @param[in] new_name  The new name key. Must not already exist in the list
	/// @return  Reference to the object whose name has just been changed
	///
	Obj& change_name(const String& old_name, const String& new_name);

	/// Get the largest ID value currently in the map. An exception is thrown if the map is empty.
	///
	/// @return  The largest ID value currently in the map
	///
	Id get_largest_id() const;

	TWIST_NO_COPY_DEF_MOVE(DualKeyedList)

private:
	using IdElemMap = std::map<Id, Elem>;
	using NameElemMap = std::conditional_t<case_sens, std::map<String, Elem>, 
	                                                  std::map<String, Elem, LessStringsNoCase<Chr>>>;
													  
	List elem_list_;  
	IdElemMap id_elem_map_;  
	NameElemMap name_elem_map_;  

	TWIST_CHECK_INVARIANT_DECL
};

} 

#include "twist/DualKeyedList.ipp"

#endif 

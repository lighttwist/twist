/// @file hash_utils.hpp
/// Utilities for working with hash containers

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_HASH__UTILS_HPP
#define TWIST_HASH__UTILS_HPP

#include <bit>

namespace twist {

namespace detail {

template<typename T>
[[nodiscard]] inline auto xorshift(const T& n, int i) -> T
{
    return n ^ (n >> i);
}

// a hash function with another name as to not confuse with std::hash
[[nodiscard]] inline auto distribute(const uint32_t& n) -> uint32_t
{
  static const auto p = 0x55555555ul; // pattern of alternating 0 and 1
  static const auto c = 3423571495ul; // random uneven integer constant
  return c * xorshift(p * xorshift(n, 16), 16);
}

// a hash function with another name as to not confuse with std::hash
[[nodiscard]] inline auto distribute(const uint64_t& n) -> uint64_t
{
  static const auto p = 0x5555555555555555ull; // pattern of alternating 0 and 1
  static const auto c = 17316035218449499591ull; // random uneven integer constant 
  return c * xorshift(p * xorshift(n, 32), 32);
}

}

/*! Call this function with the existing hash value (the seed) \p seed and a new value (the key) \p value, which will 
    be first hashed and then combined into a new seed value (or the final hash).
 */
template <class T>
[[nodiscard]] inline auto hash_combine(std::size_t seed, const T& value) -> std::size_t
{
    // See https://stackoverflow.com/questions/35985960/c-why-is-boosthash-combine-the-best-way-to-combine-hash-values
    using namespace twist::detail;
    return std::rotl(seed, std::numeric_limits<std::size_t>::digits / 3) ^ distribute(std::hash<T>{}(value));
}

/*! Same as hash_combine() but using a less "good", but faster, algorithm. It is the default hash combining algorithm 
    for the "boost" library.
*/
template <class T>
[[nodiscard]] inline auto boost_hash_combine(std::size_t seed, const T& value) -> std::size_t
{
    // See https://stackoverflow.com/questions/35985960/c-why-is-boosthash-combine-the-best-way-to-combine-hash-values
    return seed ^ (std::hash<T>{}(value) + 0x9e3779b9 + (seed << 6) + (seed >> 2));
}

}

#endif

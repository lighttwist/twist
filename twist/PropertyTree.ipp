///  @file  PropertyTree.ipp
///  Inline implementation file for "PropertyTree.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

namespace twist {

//
//  PropertyTreeNode  class
//

template<class TNamedPropSet>
PropertyTreeNode<TNamedPropSet>::PropertyTreeNode(PropertyTreeNodeId id, const std::wstring& name, 
		std::shared_ptr<TNamedPropSet> prop_set)
	: id_(id)
	, name_(name)
	, data_(0)
	, prop_set_(prop_set)
	, children_()
{
	TWIST_CHECK_INVARIANT
}


template<class TNamedPropSet>
PropertyTreeNode<TNamedPropSet>::PropertyTreeNode(const PropertyTreeNode& src)
	: id_(src.id_)
	, name_(src.name_)
	, data_(src.data_)
	, prop_set_(src.prop_set_)
	, children_(src.children_)
{
	TWIST_CHECK_INVARIANT
}


template<class TNamedPropSet>
PropertyTreeNode<TNamedPropSet>::PropertyTreeNode(PropertyTreeNode&& src)
	: id_(src.id_)
	, name_(src.name_)
	, data_(src.data_)
	, prop_set_(src.prop_set_)
	, children_(move(src.children_))
{
	TWIST_CHECK_INVARIANT
}


template<class TNamedPropSet>
template<typename TData> 
PropertyTreeNode<TNamedPropSet>::PropertyTreeNode(PropertyTreeNodeId id, const std::wstring& name, TData data, 
		std::shared_ptr<TNamedPropSet> prop_set)
	: id_(id)
	, name_(name)
	, data_(reinterpret_cast<const void*>(data))
	, prop_set_(prop_set)
	, children_()
{
	TWIST_CHECK_INVARIANT
}


template<class TNamedPropSet>
PropertyTreeNode<TNamedPropSet>::~PropertyTreeNode()
{
	TWIST_CHECK_INVARIANT
}


template<class TNamedPropSet>
PropertyTreeNode<TNamedPropSet>& PropertyTreeNode<TNamedPropSet>::operator=(const PropertyTreeNode<TNamedPropSet>& rhs)
{
	TWIST_CHECK_INVARIANT
	if (this != &rhs) {
		id_ = rhs.id_;
		name_ = rhs.name_;
		data_ = rhs.data_;
		prop_set_ = rhs.prop_set_;
		children_ = rhs.children_;
	}
	TWIST_CHECK_INVARIANT
	return *this;
}


template<class TNamedPropSet>
PropertyTreeNode<TNamedPropSet>& PropertyTreeNode<TNamedPropSet>::operator=(PropertyTreeNode<TNamedPropSet>&& rhs)
{
	TWIST_CHECK_INVARIANT
	id_ = rhs.id_;
	name_ = rhs.name_;
	data_ = rhs.data_;	
	prop_set_ = rhs.prop_set_;
	children_ = move(rhs.children_);
	TWIST_CHECK_INVARIANT
	return *this;
}


template<class TNamedPropSet>
PropertyTreeNodeId PropertyTreeNode<TNamedPropSet>::get_id() const
{
	TWIST_CHECK_INVARIANT
	return id_;
}


template<class TNamedPropSet>
std::wstring PropertyTreeNode<TNamedPropSet>::get_name() const
{
	TWIST_CHECK_INVARIANT
	return name_;
}


template<class TNamedPropSet>
const TNamedPropSet& PropertyTreeNode<TNamedPropSet>::get_prop_set() const
{
	TWIST_CHECK_INVARIANT
	return *prop_set_;
}


template<class TNamedPropSet>
TNamedPropSet& PropertyTreeNode<TNamedPropSet>::get_prop_set() 
{
	TWIST_CHECK_INVARIANT
	return *prop_set_;
}


template<class TNamedPropSet>
const std::vector<PropertyTreeNode<TNamedPropSet>>& PropertyTreeNode<TNamedPropSet>::get_children() const
{
	TWIST_CHECK_INVARIANT
	return children_;
}


template<class TNamedPropSet>
template<typename TData> 
TData PropertyTreeNode<TNamedPropSet>::get_data() const
{
	TWIST_CHECK_INVARIANT
		return reinterpret_cast<TData>(data_);
}


#ifdef _DEBUG
template<class TNamedPropSet>
void PropertyTreeNode<TNamedPropSet>::check_invariant() const noexcept
{
	assert(id_ != k_no_prop_tree_node_id);
	assert(!name_.empty());
	assert(prop_set_ != nullptr);
}
#endif


//
//  PropertyTree  class
//

template<class TNamedPropSet>
PropertyTree<TNamedPropSet>::PropertyTree(PropTreeNode top_node) 
	: top_node_(top_node)
	, highest_node_id_(top_node.get_id())
{
	TWIST_CHECK_INVARIANT
}


template<class TNamedPropSet>
PropertyTree<TNamedPropSet>::~PropertyTree()
{
	TWIST_CHECK_INVARIANT
}


template<class TNamedPropSet>
std::unique_ptr<PropertyTree<TNamedPropSet>> PropertyTree<TNamedPropSet>::get_clone() const
{
	TWIST_CHECK_INVARIANT
	std::unique_ptr<PropertyTree> clone( new PropertyTree(top_node_) );
	clone->highest_node_id_ = highest_node_id_;
	return clone;
}


template<class TNamedPropSet>
const typename PropertyTree<TNamedPropSet>::PropTreeNode& PropertyTree<TNamedPropSet>::get_top_node() const
{
	TWIST_CHECK_INVARIANT
	return top_node_;
}


template<class TNamedPropSet>
const typename PropertyTree<TNamedPropSet>::PropTreeNode& PropertyTree<TNamedPropSet>::get_node(PropertyTreeNodeId node_id) const
{
	TWIST_CHECK_INVARIANT
	const PropTreeNode* matching_node = find_node(node_id);
	if (matching_node == nullptr) {
		TWIST_THROW(L"NodeT with ID %d not found in the tree.", node_id.value());
	}	
	return *matching_node;
}


template<class TNamedPropSet>
typename PropertyTree<TNamedPropSet>::PropTreeNode& PropertyTree<TNamedPropSet>::get_node(PropertyTreeNodeId node_id) 
{
	TWIST_CHECK_INVARIANT
	PropTreeNode* matching_node = find_node(node_id);
	if (matching_node == nullptr) {
		TWIST_THROW(L"NodeT with ID %d not found in the tree.", node_id.value());
	}	
	TWIST_CHECK_INVARIANT
	return *matching_node;
}


template<class TNamedPropSet>
std::vector<const typename PropertyTree<TNamedPropSet>::PropTreeNode*> PropertyTree<TNamedPropSet>::get_all_nodes() const
{
	TWIST_CHECK_INVARIANT
	std::vector<const PropTreeNode*> nodes;

	// Define a recursive functor which adds a tree node at the end of the list, and all its descendant nodes too 
	std::function<void (const PropTreeNode& node)> add_node = [&](const PropTreeNode& node) {
		// Add the node to the list
		nodes.push_back(&node);
		// Recurse for the node's child nodes
		for (const auto& el : node.get_children()) {
			add_node(el);
		}
	};

	// Save the whole tree to the archive, starting with the top node
	add_node(top_node_);

	return nodes;
}


template<class TNamedPropSet>
void PropertyTree<TNamedPropSet>::add_node(PropTreeNode child_node, PropertyTreeNodeId parent_id)
{
	TWIST_CHECK_INVARIANT
	// First, find the parent node 
	PropTreeNode& parent_node = PropertyTree::get_node(parent_id);
	// Second, check that the child node ID is not already used in the tree
	if (find_node(child_node.get_id()) != nullptr) {
		TWIST_THROW(L"A node with ID %d already exists in the tree.", child_node.get_id().value());
	}
	// Third, add the new child node to the parent node's list of children
	parent_node.children_.push_back(child_node);
	// Update the highest node ID, if necessary
	if (highest_node_id_ < child_node.get_id()) {
		highest_node_id_ = child_node.get_id();
	}
}


template<class TNamedPropSet>
PropertyTreeNodeId PropertyTree<TNamedPropSet>::get_next_avail_node_id() const
{
	TWIST_CHECK_INVARIANT
	return ++PropertyTreeNodeId(highest_node_id_);
}


template<class TNamedPropSet>
const typename PropertyTree<TNamedPropSet>::PropTreeNode* PropertyTree<TNamedPropSet>::find_node(PropertyTreeNodeId node_id) const
{
	TWIST_CHECK_INVARIANT
	return recursive_find(top_node_, node_id);
}


template<class TNamedPropSet>
typename PropertyTree<TNamedPropSet>::PropTreeNode* PropertyTree<TNamedPropSet>::find_node(PropertyTreeNodeId node_id) 
{
	TWIST_CHECK_INVARIANT
	return recursive_find(top_node_, node_id);
}


template<class TNamedPropSet>
template<typename TData> 
const typename PropertyTree<TNamedPropSet>::PropTreeNode* PropertyTree<TNamedPropSet>::find_node_with_data(TData data) const
{
	TWIST_CHECK_INVARIANT
	return recursive_find(top_node_, reinterpret_cast<const void*>(data));
}


template<class TNamedPropSet>
template<typename NodeT>  
NodeT* PropertyTree<TNamedPropSet>::recursive_find(NodeT& cur_node, PropertyTreeNodeId node_id) const
{
	TWIST_CHECK_INVARIANT		
	NodeT* matching_node = nullptr;
	if (cur_node.id_ == node_id) {
		matching_node = &cur_node;
	}
	else {
		for (const auto& child, cur_node.children_) {
			matching_node = recursive_find(*child, node_id);
			if (matching_node != nullptr) {
				break;
			}
		}
	}
	return matching_node;
}


template<class TNamedPropSet>
template<typename NodeT> 
NodeT* PropertyTree<TNamedPropSet>::recursive_find(NodeT& cur_node, const void* data) const
{
	TWIST_CHECK_INVARIANT		
	NodeT* matching_node = nullptr;
	if (cur_node.data_ == data) {
		matching_node = &cur_node;
	}
	else {
		for (const auto& child, cur_node.children_) {
			matching_node = recursive_find(*child, data);
			if (matching_node != nullptr) {
				break;
			}
		}
	}
	return matching_node;
}


#ifdef _DEBUG
template<class TNamedPropSet>
void PropertyTree<TNamedPropSet>::check_invariant() const noexcept
{
}
#endif

} // namespace twist


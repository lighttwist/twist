/// @file cast.hpp
/// Type casting utilities 

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_CAST_HPP
#define TWIST_CAST_HPP

#include <gsl/pointers>

namespace twist {

/*! Perform a dynamic_cast on a pointer and wrap the result in a gsl::not_null object. 
    \tparam Dest  The cast destination type
    \tparam Src  The cast source type
    \param[in] src  The cast source
    \return  The wrapped cast result
 */
template<class Dest, class Src>  
[[nodiscard]] auto dyn_nonull_cast(Src src) -> gsl::not_null<Dest>;

/*! Perform a dynamic_cast on a pointer wrapped in a gsl::not_null object and wrap the result in a gsl::not_null 
    object. 
    \tparam Dest  The cast destination type
    \tparam Src  The cast source type
    \param[in] src  The cast source
    \return  The wrapped cast result
 */
template<class Dest, class Src>  
[[nodiscard]] auto dyn_nonull_cast(gsl::not_null<Src> src) -> gsl::not_null<Dest>;

/*! Perform a static_cast on a pointer and wrap the result in a gsl::not_null object.
    \tparam Dest  The cast destination type
    \tparam Src  The cast source type
    \param[in] src  The cast source
    \return  The wrapped cast result
 */
template<class Dest, class Src>  
[[nodiscard]] auto static_nonull_cast(Src src) -> gsl::not_null<Dest>;

//! Cast the enum value \p val to the type underlying its enum type \p T.
template<class T>
requires std::is_enum_v<T>
[[nodiscard]] auto as_underlying(T val) -> std::underlying_type_t<T>;

/*! Convert a tuple into another with the same number of elements, by performing static_cast on its elements, with the 
    destination types provided.
    \tparam DestArgs  The destination types to be used for each static_cast, in the order of the elements of the source
                      tuple; the pack must have the same size as \p SrcArgs
   \tparam SrcArgs  The types of the elements in the source tuple
   \param src  The source tuple
   \return  The resulting tuple
 */
template<class... DestArgs, class... SrcArgs>
[[nodiscard]] auto static_cast_tuple(const std::tuple<SrcArgs...>& src) -> std::tuple<DestArgs...>;

}

#include "twist/cast.ipp"

#endif

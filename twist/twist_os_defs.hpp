///  @file  twist_os_defs.hpp
///  Preprocessor definitions for identifying the operating system that the "twist" library is running in

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_OS__DEFS_HPP
#define TWIST_OS__DEFS_HPP

// Figure out if we are dealing with Linux or Windows or some other operating system. After the following block 
// of preprocess macros, all of TWIST_OS_UNIX, TWIST_OS_WIN, and TWIST_OS_OTHER will defined to either 1 
// or 0. One of the three will be 1. The other two will be 0.
# if !defined(TWIST_OS_WIN) || !defined(TWIST_OS_LINUX) || !defined(TWIST_OS_OTHER)

#   undef TWIST_OS_WIN
#   undef TWIST_OS_LINUX
#   undef TWIST_OS_OTHER
#   undef TWIST_OS_32BIT
#   undef TWIST_OS_64BIT

#   if defined(_WIN32) || defined(WIN32) || defined(__CYGWIN__) || defined(__MINGW32__) || defined(__BORLANDC__)

#     define TWIST_OS_WIN   1
#     define TWIST_OS_LINUX 0
#     define TWIST_OS_OTHER 0
#     if _WIN64
#       define TWIST_OS_32BIT 0
#       define TWIST_OS_64BIT 1
#     else 
#       define TWIST_OS_32BIT 1
#       define TWIST_OS_64BIT 0
#     endif

#   elif defined(__linux__)

#     define TWIST_OS_WIN   0
#     define TWIST_OS_LINUX 1
#     define TWIST_OS_OTHER 0
#     if __x86_64__ || __ppc64__
#       define TWIST_OS_32BIT 0
#       define TWIST_OS_64BIT 1
#     else 
#       define TWIST_OS_32BIT 1
#       define TWIST_OS_64BIT 0
#     endif

#   else

#     define TWIST_OS_WIN   0
#     define TWIST_OS_LINUX 0
#     define TWIST_OS_OTHER 1

#   endif

#   if TWIST_OS_WIN
#     if _MSC_VER && !__INTEL_COMPILER
#       define TWIST_COMPILER_MSVC 1
#     else 
#       define TWIST_COMPILER_MSVC 0
#     endif
#   else
#     define TWIST_COMPILER_MSVC 0
#   endif

# endif


#endif

/// @file string_utils.hpp
///	Genetic string utilities

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_STRING__UTILS_HPP
#define TWIST_STRING__UTILS_HPP

#include <locale>
#include <ranges>
#include <string>

#include "twist/concepts.hpp"
#include "twist/metaprogramming.hpp"

namespace std {

//! This was left out of the standard for no good reason.
template<typename Chr> 
auto operator+(const std::basic_string<Chr>& a, std::basic_string_view<Chr> b) -> std::basic_string<Chr>;

//! This was left out of the standard for no good reason.
template<typename Chr> 
auto operator+(std::basic_string_view<Chr> a, const std::basic_string<Chr>& b) -> std::basic_string<Chr> ;

}

namespace twist {

/// Binary predicate: whether two strings are the same, ignoring case. 
/// The predicate is case-insensitive and takes locale into account.
///
///  @tparam  Chr  The string character type
///
template<class Chr>
class CompStringsNoCase {
public:
    CompStringsNoCase(const std::locale& locale = std::locale::classic()) 
        : locale_{ locale }
        , ctype_{ &std::use_facet<std::ctype<Chr>>(locale) } 
    {
    }

    /// Returns true if the two strings are the same (ignoring case).
    bool operator()(std::basic_string_view<Chr> str1, std::basic_string_view<Chr> str2) const 
    {
        if (str1.size() != str2.size()) {
            return false;
        }
        return std::equal(str1.begin(), str1.end(), str2.begin(), CompChar{ *ctype_ });
    }

private:
    struct CompChar {		
        CompChar(const std::ctype<Chr>& chr) 
            : ctype_{ &chr }
        {
        }		

        bool operator()(Chr x, Chr y) const 
        {
            return ctype_->toupper(x) == ctype_->toupper(y);
        }

        const std::ctype<Chr>*  ctype_;
    };
    
    std::locale  locale_;
    const std::ctype<Chr>*  ctype_;
};

///  Binary predicate: "less" function for comparison of two strings. 
///  The predicate is case-insensitive and takes locale into account.
///
///  @tparam  Chr  The string character type
///
template<typename Chr> 
class LessStringsNoCase {
public:
    LessStringsNoCase(const std::locale& locale = std::locale::classic()) 
        : locale_(locale) 
        , ctype_(&std::use_facet<std::ctype<Chr>>(locale)) 
    {
    }

    //  Returns true if the first string is lexicographically less than the second string (ignoring case).
    bool operator()(const std::basic_string<Chr>& x, const std::basic_string<Chr>& y) const 
    {
        return std::lexicographical_compare(x.begin(), x.end(), y.begin(), y.end(), CompChar(*ctype_));
    }
    
private:
    struct CompChar {		
        CompChar(const std::ctype<Chr>& chr) : ctype_(&chr) 
        {
        }		

        bool operator()(Chr x, Chr y) const 
        {
            return ctype_->toupper(x) < ctype_->toupper(y);
        }

        const std::ctype<Chr>*  ctype_;
    };
    
    std::locale  locale_;
    const std::ctype<Chr>*  ctype_;
};

/// Floating point number format type, for conversion to strings.
enum class FloatStrFormat {
    /// Fixed-point notation; a specific number of digits (the precision) are displayed after the 
    /// decimal point
    fixed = L'f',
    /// Exponential notation
    exp = L'e',
    /// Short notation. Used either fixed-point or exponential notation, whichever is more appropriate for the 
    /// magnitude. This type differs slightly from fixed-point notation in that insignificant zeroes to the 
    /// right of the decimal point are not included. Also, the decimal point is not included on whole numbers.
    brief = L'g'
};

/*! Convert between two strings using different character types. 
    \note  Only a few combinations are currently implemented.
    \tparam DestChr  The character type of the destination string
    \tparam SrcChr  The character type of the source string
    \param[in] src  The source string
    \return  The converted string
 */
template<class DestChr, class SrcChr>
[[nodiscard]] auto convert_string(const std::basic_string<SrcChr>& src) -> std::basic_string<DestChr>;

/// Create a std::basic_string_view<> object based on two character pointers.
///
/// @tparam  Chr  The string character type
/// @param[in] first  Pointer addressing the first character in the view
/// @param[in] last  Pointer addressing one past the final character in the view
/// @return  The string view
///
template<class Chr> 
std::basic_string_view<Chr> make_string_view(const Chr* first, const Chr* last);

/*! Convert a string to an integer value.
    \tparam  Chr  The string character type
    \param[in] str  The string 
    \return  The integer value; or zero is returned when the string is non-convertible 
 */
template<class Chr> 
[[nodiscard]] auto to_int(const std::basic_string<Chr>& str) -> int;

/*! Convert a string to the equivalent int value.
    \tparam Chr  The string character type
    \param[in] str  The string; if it contains any characters that are not convertible or is blank, then an 
                    exception is thrown
    \return  The number
 */
template<class Chr> 
[[nodiscard]] auto to_int_checked(const std::basic_string<Chr>& str) -> int;

/*! Convert a string to the equivalent unsigned 32-bit integer value.
    \tparam Chr  The string character type
    \param[in] str  The string; if it contains any characters that are not convertible or is blank, then an 
                    exception is thrown
    \return  The number
 */
template<class Chr> 
[[nodiscard]] auto to_uint32_checked(const std::basic_string<Chr>& str) -> uint32_t;

/*! Convert a string to the equivalent short value.
    \tparam Chr  The string character type
    \param[in] str  The string; if it is not convertible, an exception is thrown
    \return  The number
 */
template<class Chr> 
[[nodiscard]] auto to_short_checked(const std::basic_string<Chr>& str) -> short;

/*! Convert a string to the equivalent unsigned char value.
    \tparam Chr  The string character type
    \param[in] str  The string; if it is not convertible, an exception is thrown
    \return  The number
 */
template<class Chr> 
[[nodiscard]] auto to_uchar_checked(const std::basic_string<Chr>& str) -> unsigned char;

/*! Convert a string to the equivalent long long value.
    \tparam Chr  The string character type
    \param[in] str  The string; if it contains any characters that are not convertible or is blank, then an 
                    exception is thrown
    \return  The number
 */
template<class Chr> 
[[nodiscard]] auto to_longlong_checked(const std::basic_string<Chr>& str) -> long long;

/*! Convert a string to the equivalent Ssize value.
    \tparam Chr  The string character type
    \param[in] str  The string; if it contains any characters that are not convertible or is blank, then an 
                    exception is thrown
    \return  The number
 */
template<class Chr> 
[[nodiscard]] auto to_ssize_checked(const std::basic_string<Chr>& str) -> Ssize;

/*! Convert a string to the equivalent double-precision floating-point number.
    \tparam  Chr  The string character type
    \param[in] str  The string; zero is returned when the string is non-convertible 
    \return  The number
 */
template<class Chr> 
[[nodiscard]] auto to_double(const std::basic_string<Chr>& str) -> double;

/*! Convert a string to the equivalent double-precision floating-point number.
    \tparam  Chr  The string character type
    \param[in] str  The string; if it contains any characters that are not convertible or is blank, then an 
   					exception is thrown
    \return  The number
 */
template<class Chr> 
[[nodiscard]] auto to_double_checked(const std::basic_string<Chr>& str) -> double;

/*! Convert a string to the equivalent single-precision floating-point number.
    \tparam Chr  The string character type
    \param[in] str  The string; zero is returned when the string is non-convertible 
    \return  The number
 */
template<class Chr> 
[[nodiscard]] auto to_float(const std::basic_string<Chr>& str) -> float;

/* Convert a string to the equivalent single-precision floating-point number.
   \tparam Chr  The string character type
   \param[in] str  The string; if it contains any characters that are not convertible or is blank, then an 
   					exception is thrown
   \return  The number
 */
template<class Chr> 
[[nodiscard]] auto to_float_checked(const std::basic_string<Chr>& str) -> float;

/* Convert a string to the equivalent number.
   \tparam Num  The output number type
   \tparam Chr  The string character type
   \param[in] str  The string; if it contains any characters that are not convertible to the given number type or is 
                   blank, then an exception is thrown
   \return  The number
 */
template<class Num, class Chr>
requires is_number<Num>
[[nodiscard]] auto to_number_checked(const std::basic_string<Chr>& str) -> Num;

/// Give a string view, create another which references the portion of the original string obtained by 
/// trimming all whitespace characters from the beginning and the end of the string.
///
/// @tparam  Chr  The string character type
/// @param[in] str_view  The input string view
/// @return  The "trimmed" string view
///
template<class Chr> 
std::basic_string_view<Chr> trim_whitespace_to_view(std::basic_string_view<Chr> str_view);

/*! Concatenate all the strings in a range.
    \tparam Chr  The string character type
    \tparam Rng  The string range type
    \param[in] range  The string range
    \param[in] separator  The string to be used to separate successive strings (pass in blank for none)
    \param[in] quote  The string to be used to quote around each string in the container (pass in blank for none)
    \param[in] and_str  If non-empty, then this string will be used to separate the last two strings in the container,
                        instead of the separator
    \return  The concatenated string
 */
template<class Chr, rg::input_range Rng> 
[[nodiscard]] auto concat(const Rng& range, 
                          std::basic_string_view<Chr> separator, 
                          std::basic_string_view<Chr> quote = {}, 
                          std::basic_string_view<Chr> and_str = {}) -> std::basic_string<Chr>;

/*! Transform all elements in a range to strings and concatenate those into a single string.
    \tparam Chr  The string character type
    \tparam Rng  The range type
    \tparam Transform  Transformation callable type 
    \param[in] range  The string range
    \param[in] tranform  The transformation callable
    \param[in] separator  The string to be used to separate successive strings (pass in blank for none)
    \param[in] quote  The string to be used to quote around each string in the container (pass in blank for none)
    \param[in] and_str  If non-empty, then this string will be used to separate the last two strings in the container,
                        instead of the separator
    \return  The concatenated string
 */
template<class Chr, 
         rg::input_range Rng,
         std::invocable<rg::range_const_reference_t<Rng>> Transform> 
[[nodiscard]] auto concat(const Rng& range, 
                          Transform transform,
                          std::basic_string_view<Chr> separator, 
                          std::basic_string_view<Chr> quote = {}, 
                          std::basic_string_view<Chr> and_str = {}) -> std::basic_string<Chr>;

//! Alias for concat<wchar_t>().
template<rg::input_range Rng> 
[[nodiscard]] auto wconcat(const Rng& range, 
                           std::wstring_view separator, 
                           std::wstring_view quote = {}, 
                           std::wstring_view and_str = {}) -> std::wstring;

//! Alias for concat<wchar_t>().
template<rg::input_range Rng,
         std::invocable<rg::range_const_reference_t<Rng>> Transform> 
[[nodiscard]] auto wconcat(const Rng& range, 
                           Transform transform,
                           std::wstring_view separator, 
                           std::wstring_view quote = {}, 
                           std::wstring_view and_str = {}) -> std::wstring;

/*! Split a given string into component fields, given a delimiter character. Text found between paired quote 
    characters is always considered one field. All fields are trimmed of whitespace. If the string starts with 
    the delimiter (preceded or not by whitespace) then it is considered that there is an empty field before 
    the first delimiter. If the string ends in a delimiter (followed or not by whitespace) then it is 
    considered that there is an empty field after the last delimiter.
    +TODO: This does not seem to work properly with Chr = wchar_t
    \tparam Chr  The string character type 
    \param[in] str  The input string
    \param[in] delim_char  The delimiter character
    \param[in] quote_char  The quote character
    \return  The component fields
 */
template<class Chr>
[[nodiscard]] auto split_with_quotes(std::basic_string_view<Chr> str, Chr delim_char = ',', Chr quote_char = '\"')
                    -> std::vector<std::basic_string<Chr>>;

/*! Check if a character is a wide whitespace character as classified by the currently installed C locale.
    \tparam Chr  The character type 
    \param[in] chr  The character value
    \return  true if the character is a whitespace character
 */
template<class Chr>
auto is_whitespace_char(Chr chr) -> bool;

/*! Split a string into tokens, which are sequences of contiguous characters separated by any of a set of
    delimiter characters. An user supplied function is called for each token found.
    This function is not thread safe.
    \tparam Chr  The string character type
    \tparam TokenFunc  The type of the functtion which will be called for each token, as a const Chr* parameter
    \param[in] str  The input string
    \param[in] delims  A string containing all the delimiter characters
    \param[in] token_func  The function which will be called for each token
 */   
template<class Chr, class TokenFunc> 
auto tokenise(std::basic_string_view<Chr> str, gsl::not_null<const Chr*> delims, TokenFunc token_func) -> void;

/*! Split a string into tokens, which are sequences of contiguous characters separated by any of a set of
    delimiter characters. 
    This function is not thread safe.   
    \tparam Chr  The string character type
    \param[in] str  The input string
    \param[in] delims  A string containing all the delimiter characters
    \return  Vector containing all tokens found
 */  
template<class Chr> 
[[nodiscard]] auto tokenise(std::basic_string_view<Chr> str, gsl::not_null<const Chr*> delims) 
                    -> std::vector<std::basic_string<Chr>>;

// --- Wide-char string functions ---

/// Convert, as well as possible, a string to a numeric value (leading whitespace is ignored and as many 
/// characters as possible, starting from the beginning, are converted).
///
/// @tparam  T  The numeric type
/// @param[in] str  The string
/// @return  The result of the conversion, or zero if the conversion fails
///
template<class T, class = EnableIfNumber<T>>  
[[nodiscard]] T to_number(std::wstring_view str);  

/// Convert, as well as possible, a string to a numeric value (leading whitespace is ignored and as many 
/// characters as possible, starting from the beginning, are converted).
///
/// @tparam  T  The numeric type
/// @param[in] str  The string
/// @return  The result of the conversion, or zero if the conversion fails
///
template<class T, class = EnableIfNumber<T>>  
[[nodiscard]] T to_number(const std::wstring& str);  

/// Convert a double-precision floating-point value to a string in fixed-point notation (insignificant zeroes 
/// to the right of the decimal point are included up to the desired precision).
///
/// @param[in] value  The value
/// @param[in] precision  The precision (this means different things depending on the format); pass in zero 
///				for the default precision
/// @param[in] format  The format in which the floating-point number should be displayed
/// @param[in] decimal_sep  The decimal separator character 
/// @return  The string
///
std::wstring to_str(double value, unsigned int precision = 0, FloatStrFormat format = FloatStrFormat::brief, 
        wchar_t decimal_sep = L'.'); 

/// Create a string containing a single character
///
/// @param[in] chr  The character
/// @return  The string 
///
std::wstring to_str(wchar_t chr); 

/// Convert a string to a 32bit integer value.
///
/// @param[in] str  The string
/// @return  The integer value, if the conversion is successful, or zero otherwise. 
///
long to_long(const std::wstring& str);

/// Check whether all characters in a string are decimal digits (so negative numbers or floating-point numbers 
/// or scientific notation not supported).
///
/// @param[in] str  The string
/// @return  true if they are; false otherwise (including if the string is empty)
///
bool is_numeric(std::wstring_view str);

/// Check whether all characters in a string are decimal digits or the characters used in floating-pont 
///   numbers, incuding scientific notation.
/// Devnote: Simplistic implementation, only checks for the possible presence of one decimal separator and/or 
///   one 'e' or 'E' character.
///
/// @param[in] str  The string
/// @return  true if they are; false otherwise (including if the string is empty)
///
bool is_numeric_or_fp(const std::wstring& str);

/*! Case insensitive string comparison, which takes locale into consideration.
    \tparam Chr  The character type of the strings
    \param[in] str1  The first string
    \param[in] str2  The second string
    \param[in] locale  The string locale
    \return  true only if the two strings are the same (ignoring case)
 */
template<class Chr>
[[nodiscard]] auto equal_no_case(std::basic_string_view<Chr> str1,
                                 std::basic_string_view<Chr> str2,
                                 const std::locale& locale = std::locale::classic()) -> bool;

//! Alias for wequal_no_case()
[[nodiscard]] auto wequal_no_case(std::wstring_view str1,
                                  std::wstring_view str2,
                                  const std::locale& locale = std::locale::classic()) -> bool;

/// Examine a string for any "non text" characters (the following character types are considered "text" 
/// characters: alphanumeric, punctualtion and whitespace).
///
/// @param[in] str  The string
/// @return  The number of "non text" characters present in the string
///
unsigned int count_non_text_chars(std::wstring_view str);

/// Delete all non-text characters from a string.
///
/// @param[in] str  The string.
///
std::wstring strip_non_text_chars(const std::wstring& str);

//! Whether the string \str is made up entirely of whitespace, or is empty.
[[nodiscard]] auto is_whitespace(std::wstring_view str) -> bool;

//! Whether the string \str is made up entirely of whitespace, or is empty.
[[nodiscard]] auto is_whitespace(const std::wstring& str) -> bool;

/*! Whether the ANSI character sequence starting at \p str and ending one before \p str_end is made up entirely of 
    whitespace, or is empty.
 */
[[nodiscard]] auto is_whitespace(const wchar_t* str, const wchar_t* str_end) -> bool;

//! Whether the string \str contains any whitespace characters.
[[nodiscard]] auto has_whitespace(std::wstring_view str) -> bool;

/// Examine a string for any whitespace characters.
///
/// @param[in] str  The string.
/// @return  The number of whitespace characters present in the string.
///
unsigned int count_whitespace_chars(const std::wstring& str);

/// Delete all whitespace characters from a string.
///
/// @param[in] str  The string.
/// @return  The resulting string
///
std::wstring strip_whitespace_chars(const std::wstring& str);

/// Trim all whitespace characters from the beginning of a string.
///
/// @param[in] str  The string.
/// @return  The resulting string
///
std::wstring trim_lead_whitespace(std::wstring_view str);

/// Trim all whitespace characters from the end of a string.
///
/// @param[in] str  The string.
/// @return  The resulting string
///
std::wstring trim_trail_whitespace(std::wstring_view str);

/// Trim all whitespace characters from the beginning and the end of a string.
///
/// @param[in] str  The string.
/// @return  The resulting string
///
std::wstring trim_whitespace(std::wstring_view str);

/// Trim all consecutive occurrences of a specific character from the beginning of a string.
///
/// @param[in] str  The string
/// @param[in] chr  The character
/// @return  The trimmed string
///
std::wstring trim_lead_char(const std::wstring& str, wchar_t chr);

/// Trim all consecutive occurrences of a specific character from the end of a string.
///
/// @param[in] str  The string
/// @param[in] chr  The character
/// @return  The trimmed string
///
std::wstring trim_trail_char(const std::wstring& str, wchar_t chr);

/// Trim all consecutive occurrences of a specific character from both the beginning and the end of a string.
///
/// @param[in] str  The string
/// @param[in] chr  The character
/// @return  The trimmed string
///
std::wstring trim_char(const std::wstring& str, wchar_t chr);

/*! Replace, within a string, all occurrences of an old substring with a new substring.
    \tparam Chr  The character type of the strings
    \param[in] str  The string
    \param[in] old_substr  The old substring
    \param[in] new_substr  The new substring
    \return  The new string
 */
template<class Chr>
[[nodiscard]] auto replace_all_substr(std::basic_string_view<Chr> str,
                                      std::basic_string_view<Chr> old_substr, 
                                      std::basic_string_view<Chr> new_substr) -> std::basic_string<Chr>;

/*! Replace, within a string, all occurrences of an old substring with a new substring.
    \tparam Chr  The character type of the strings
    \param[in] str  The string; this will be modified by the function
    \param[in] old_substr  The old substring
    \param[in] new_substr  The new substring
 */
template<class Chr>
auto replace_all_substr_in_place(std::basic_string<Chr>& str, 
								 std::basic_string_view<Chr> old_substr, 
								 std::basic_string_view<Chr> new_substr) -> void;
    
/// Replace exactly one occurrence of specific substring in a string.
/// Devnote: Make template, and introduce std::basic_string_view
///
/// @param[in] str  The string
/// @param[in] old_substr  The substring to be replaced
/// @param[in] new_substr  The substring to replace the old substring
/// @param[in] pos  The position of the first letter of the substring occurrence to be replaced; if an 
///					occurrence of the substring to be replaced does not start at this position, an exception 
///					is thrown
/// @return  The new string
///	
std::wstring replace_substr_at(const std::wstring& str, const std::wstring& old_substr, 
        const std::wstring& new_substr, size_t pos);

/// Find all the occurrences of a specific substring in a string.
/// Devnote: Make template, and introduce std::basic_string_view
///
/// @param[in] str  The string
/// @param[in] substr  The substring
/// @return  A list containing, in ascending order, the positions of all occurrences of the substring within 
///					the string
///
std::vector<size_t> find_all_substr(std::wstring_view str, std::wstring_view substr);

/// Counts all the occurrences of a substring within a string.
/// Devnote: Make template, and introduce std::basic_string_view
///
/// @param[in] str  The string.
/// @param[in] substr  The substring.
/// @return  The count
///
unsigned int count_substr(const std::wstring& str, const std::wstring& substr);

/*! Format a string.
    \param[in] format  The format string. The following variadic arguments (if any) must match the format. The 
                       formatting rules are exactly the same as for printf().
    \return  The formatted string. Note that there is a maximum size (currently 8192) for the string.
 */ 
std::wstring format_str(const wchar_t* format, ...); //+OBSOLETE: Use std::format() / twist::formatv() instead

/*! Format a string, printf style. 
    @param[in] format   The format string. The following variadic argument list must match the format. The formatting 
                        rules are exactly the same as for printf().
    @param[in] arg_list  Provides access to the variadic argument list (which can be empty). 
    @return  The formatted string. Note that there is a maximum size (currently 8192) for the string.
 */
std::wstring format_str(const wchar_t*, va_list arg_list); //+OBSOLETE: Use std::format() / twist::formatv() instead

/*! Format a string.
    \param[in] format  The format string. The following variadic arguments (if any) must match the format. The 
                       formatting rules are the same as for std::format().
    \return  The formatted string
 */ 
template<class... Args>
[[nodiscard]] auto formatv(std::wstring_view format, const Args&... args) -> std::wstring;

/// Find out whether a string ends contains another right at the end.
/// Devnote: Make template, and introduce std::basic_string_view
///
/// @param[in] str  The string
/// @param[in] substr  The substring
/// @return  true if the string ends with the substring (including if they are the same)
///
bool ends_in(const std::wstring& str, const std::wstring& substr);

//! Create a string consisting of \p count spaces.
[[nodiscard]] auto spaces(Ssize count) -> std::wstring;

/// Get the number of fields (substrings), within a string, as delimited by a certain delimiter string
///   (e.g. a comma). Zero-length substrings (between adjacent delimiters) are not counted.
/// Note: A special situation is when there is no occurrence of the delimiter within the string. This is a 
///   valid situation if and only if the string is not empty. In this case, there is exactly one field and 
///   that is the whole string
/// Devnote: Make template, and introduce std::basic_string_view
///
/// @param[in] str  The string.
/// @param[in] delimiter  The delimiter.
/// @return  The field count. 
///
Ssize count_delimited_fields(std::wstring_view str, const std::wstring& delimiter);

/// Get a specific field (substring), within the string, as delimited by a certain delimiter string (eg 
///   a comma).
/// Note: A special situation is when there is no occurrence of the delimiter within the string. This is a 
///   valid situation if and only if the index of the field required is zero. In this case, the field is the 
///   whole string.
/// Devnote: Make template, and introduce std::basic_string_view
///
/// @param[in] str  The string.
/// @param[in] delimiter  The delimiter.
/// @param[in] field_idx  The (zero-based) index of the field, among all the delimited fields. Bear in mind 
///					that zero-length fields (between adjacent delimiters) are not counted. 
/// @param[in] field  The field, if one matching the criteria passed in is found.
/// @return  true only if a matching field is found.
///
bool get_delimited_field(std::wstring_view str, const std::wstring& delimiter, Ssize field_idx, 
        std::wstring& field);

/*! Split a string into a number of fields (substrings) as delimited by a certain delimiter (eg a comma, a tab, etc). 
    Zero-length substrings (between adjacent delimiters) are ignored. +TODO: This is pretty odd. Dan 25Oct'23
    \note  A special situation is when there is no occurrence of the delimiter within the string. This is a valid 
           situation if and only if the string is not empty. In this case, there is exactly one field and that is the 
           whole string.   
    \tparam OutIter  Output iterator type for outputting the substrings
    \param[in] str  The string to be split
    \param[in] del  The delimiter
    \param[in] dest  Iterator for outputting the substrings
    \param[in] trim  If true, each field substring will be trimmed (of whitespace) before being output
    \return  The field count
 */   
template<std::output_iterator<std::wstring> OutIter>
auto extract_delimited_fields(const std::wstring& str, const std::wstring& del, OutIter dest, bool trim = false) 
      -> Ssize;

/*! Put together a string listing a range of fields (substrings) delimited by a specific string.
    \tparam Iter  Iterator type addressing the fields
    \param[in] first  Input iterator addressing the first field in the range
    \param[in] last  Input iterator addressing one past the final field in the range
    \param[in] del  The delimiter
    \param[in] trim  If true, each field substring will be trimmed (of whitespace) before being added to the string
    \return  The string
 */      
template<std::input_iterator Iter>
[[nodiscard]] auto assemble_delimited_fields(Iter first, Iter last, const std::wstring& del, bool trim = false)
                    -> std::wstring;

/// Find the first letter in a string and change it to uppercase (if it exists, isn't already in upper case).
/// Not locale sensitive.
///
/// @param[in] str  The input string
/// @return  The string with the first letter in uppercase
///
std::wstring capitalise_first_letter(const std::wstring& str);

/// Checks if the provided string is from a real number
/// Devnote: This function had multiple issues, unclear if all were fixed please do not use!
///
/// Will it work with strings that contain the current C runtime library locale decimal separator. 
/// It will automatically return false if the provided string is empty.   
///
/// @param[in] str  The string  
///
bool is_real_number(const std::wstring& str);

/// Checks if the provided string is from a real number
/// Devnote: This function had multiple issues, unclear if all were fixed please do not use!
///
/// Will it work with strings that contain the current C runtime library locale decimal separator. 
/// It will automatically return false if the provided string is empty.   
///
/// @param[in] str  The string  
///
bool is_integer_number(const std::wstring& str);

/// Check if a specific string is a valid representation of a positive integer.
///
/// @param[in] str  The string  
/// @return  true if it is; false if not (including if the string is empty)
///
bool is_posint(const std::wstring& str);

/// Get the decimal separator character in the current C runtime library locale.
/// Bear in mind that this is not necessarily the same as the operating system locale.
/// If the decimal separator does not consist of one character, an exception is thrown.
///
/// @return  The decimal separator
///
wchar_t get_c_runtime_decimal_sep();

/// Make sure that a name is unique. The intended name, as well as a list of existing names, are provided to 
/// the function. If the name already exists, then the name is changed, following a set of rules, so that it 
/// becomes unique.
/// 
/// @param[in] name  The intended name.
/// @param[in] existing_names  List of existing names.
/// @param[in] first_dif  The "differentiator" to be used first, if the name is found to already exist; 
///					the first differentiator is a string which is inserted, as it is, at a given position in 
///					the name
/// @param[in] subseq_difs_mask  The mask for subsequent differentiators; these are strings that are inserted 
///					in the name, at a given position; they containing an index (which increases as more 
///					differentiators are tried), otherwise they are identical; the mask must contain a 
///					placeholder substring for the index
/// @param[in] idx_placeholder  The placeholder substring for subsequent differentiators; must occur exactly 
///					once in the subsequent differentiators mask
/// @param[in] case_sens  Whether the name matching should be case sensitive.
/// @param[in] dif_pos  The position where differentiators should be inserted in the name. Must be between 
///					zero and the length of the name, or std::wstring::npos (for the end)
/// @return  The unique name.
/// 
std::wstring ensure_unique(const std::wstring& name, const std::vector<std::wstring>& existing_names, 
        const std::wstring& first_dif, const std::wstring& subseq_difs_mask, const std::wstring& idx_placeholder, 
        bool case_sens = false, size_t dif_pos = std::wstring::npos);

/*! Converts all lowercase letters in a string to upper-case. 
    \tparam  Chr  The character type of the strings
    \param[in] str  The string to convert
    \param[in] loc  The string locale
    \return  The converted string
 */
template<typename Chr> 
void to_upper(std::basic_string<Chr>& str, const std::locale& loc = std::locale{});

/*! Converts all uppercase letters in a string to lower-case. 
    \tparam  Chr  The character type of the strings
    \param[in] str  The string to convert
    \param[in] loc  The string locale
    \return  The converted string
 */
template<typename Chr>  
void to_lower(std::basic_string<Chr>& str, const std::locale& loc = std::locale{});

/// Indent a string with a specific (whitespace) indentation.
/// In multiline strings, all lines will be indented similarly.
///
/// @param[in] str  The string
/// @param[in] indent_str  The indentation string
/// @return  The indented string
///
std::wstring indent(std::wstring_view str, std::wstring_view indent_str);

/// Copy a string, including the terminating null character, to a vector of characters.
///
/// @param[in] str  The string
/// @return  The vector; will always contain at least one element (terminating null character)
///
std::vector<wchar_t> string_to_vector(const wchar_t* str);

/// Copy a string, including the terminating null character, to a vector of characters.
///
/// @param[in] str  The string
/// @return  The vector; will always contain at least one element (terminating null character)
///
std::vector<wchar_t> string_to_vector(const std::wstring& str);

/*! Replace all instances of regex metacharacters in a non-regex string with their regex escape sequences. 
    The function does not check whether the metacharacters are already escaped, as the input string is not 
    expected to be a regex pattern. 
    \param[in] str  The non-regex string
    \return  The string with escaped metacharacters
 */
[[nodiscard]] auto escape_regex(std::wstring str) -> std::wstring;

/// Given a string containing exactly one instance of a wildcard substring (the pattern), and another string 
///   not containing the wildcard, check whether the latter matched the former, where the wildcard is a 
///   placeholder for any positive integer.
/// For example, the string "george2s" will match the pattern "george#s" with wildcard "#", while the strings 
///   "georges" or "george-2s" will not.
///
/// @param[in] pattern  The pattern string; an exception is thrown if does not contain the wildcard substring 
///					exactly once
/// @param[in] wcard  The wildcard substring
/// @param[in] str  The string to match against the pattern 
/// @param[in] int_val  The integer value corresponding to the wildcard (if there is a match)
/// @return  true if there is a match
///
bool match_pattern_with_single_posint_wildcard(const std::wstring& pattern, const std::wstring& wcard, 
        const std::wstring& str, int& int_val);

/*! Correctly reset a string stream's the contents and error state.
    \tparam Chr  The string character type
	\param[in] sstream  The string stream
 */
template<class Chr>
auto reset(std::basic_stringstream<Chr>& sstream) -> void;

// --- ANSI (non-Unicode) string functions ---

/// Convert a narrow character string to a wide character string.
///
/// @param[in] ansi_str  A view pf the input narrow character string
/// @return  The wide character string
///
std::wstring to_string(std::string_view ansi_str);

/// Convert a wide character string to itself.
/// This is an identity function for the wide character string, and it is an useful overload when converting 
/// string types with templated character types to wide character strings.
///
/// @param[in] str  The input string
/// @return  The input string, unchanged
///
std::wstring to_string(const std::wstring& str);

/// Convert a  std::string_view  object to a  std::wstring  object.
///
/// @param[in] str  The object to be converted
/// @return  The  std::wstring  object
///
std::wstring ansi_to_string(std::string_view str);  //+DEPRECATED: Use to_string() instead

/// Convert a wide character string to a narrow (ANSI) character string.
/// Strange things may happen if any of the wide characters are not convertible to narrow characters.
///
/// @param[in] str  The wide character string
/// @return  The ANSI string
///
std::string string_to_ansi(std::wstring_view str);

/// Create a new narrow (ANSI) character string from a narrow (ANSI) character string.
///
/// @param[in] str  The input ANSI character string
/// @return  The new ANSI string
///
std::string string_to_ansi(std::string_view str);

/// Convert a filesystem path to a narrow (ANSI) character string.
/// Strange things may happen if any of the wide characters are not convertible to narrow characters.
///
/// @param[in] path  The path
/// @return  The ANSI string
///
std::string path_to_ansi(const fs::path& path);

/// Format a string.
///
/// @param[in] format  The format string. The following arguments (if any) must match the format. The 
///					formatting rules are exactly the same as for  printf() 
/// @return  The formatted string. Note that there is a maximum size for the string
/// 
std::string format_ansi(const char* format, ...); //+OBSOLETE: Use std::format() / twist::formatv() instead

/*! Format a string.
    \param[in] format  The format string. The following variadic arguments (if any) must match the format. The 
                       formatting rules are exactly the same as for std::format().
    \return  The formatted string
 */ 
template<class... Args>
[[nodiscard]] auto formatv(std::string_view format, const Args&... args);

/// Convert an ANSI string to a string using a specific character type.
///
/// @tparam  Chr  The character type of the output string
/// @param[in] ansi_str  The input string
/// @return  The result of the conversion, or a copy of the input string if the output character type is char
///
template<class Chr>
[[nodiscard]] std::basic_string<Chr> to_string_of_type(std::string_view ansi_str);

/// Convert, as well as possible, a string to a numeric value (leading whitespace is ignored and as many 
/// characters as possible, starting from the beginning, are converted).
///
/// @tparam  T  The numeric type
/// @param[in] str  The string
/// @return  The result of the conversion, or zero if the conversion fails
///
template<class T, class = EnableIfNumber<T>>   
[[nodiscard]] T to_number(std::string_view str);  

/// Convert a string to a numeric value (leading whitespace is ignored). 
///
/// @tparam  T  The numeric type
/// @param[in] str  The string
/// @return  The result of the conversion; if the conversion fails, including if the string contains trailing 
///					characters which cannot be converted, an exception is thrown
///
template<class T, class = EnableIfNumber<T>>  
[[nodiscard]] T to_number_checked(std::string_view str);  

//! Whether the ANSI string \str is made up entirely of whitespace, or is empty.
[[nodiscard]] auto is_whitespace(std::string_view str) -> bool;

/*! Whether the ANSI character sequence starting at \p str and ending one before \p str_end is made up entirely of 
    whitespace, or is empty.
 */
[[nodiscard]] auto is_whitespace(const char* str, const char* str_end) -> bool;

//! Whether the ANSI string \str contains any whitespace characters.
[[nodiscard]] auto has_whitespace(std::string_view str) -> bool;

/// Trim all whitespace characters from the beginning of an ANSI string.
///
/// @param[in] str  The string
/// @return  The resulting string
///
std::string ansi_trim_lead_whitespace(std::string_view str);

/// Trim all whitespace characters from the end of an ANSI string.
///
/// @param[in] str  The string.
/// @return  The resulting string
///
std::string ansi_trim_trail_whitespace(std::string_view str);

/// Trim all whitespace characters from the beginning and the end of an ANSI string.
///
/// @param[in] str  The string.
/// @return  The resulting string
///
std::string trim_whitespace(std::string_view str);

/// Trims all whitespace characters from the beginning and the end of a null-terminated string buffer, 
/// by modifying the buffer.
///
/// @param[in] str  The string buffer address; it will, if necessary, be modified
///
void trim_whitespace(char* str);

/// Trim all consecutive occurrences of a specific character from the end of an ANSI string.
///
/// @param[in] str  The string
/// @param[in] chr  The character
/// @return  The trimmed string
///
std::string trim_trail_char(const std::string& str, char chr);

//! Create a string consisting of \p count spaces.
[[nodiscard]] auto ansi_spaces(Ssize count) -> std::string;

/*! Find out whether a specific wide character is the special "new line" character.
    Note that this function is guaranteed to also work correctly for narrow character (char) arguments.
    \param[in] chr  The character 
    \return  true if the character is the "new line" character
 */
[[nodiscard]] constexpr auto is_newline(wchar_t chr) -> bool;

/*! Find out whether a specific character is the special "carriage return" character.
    Note that this function is guaranteed to also work correctly for narrow character (char) arguments.
    \param[in] chr  The character 
    \return  true if the character is the "carriage return" character
 */
template<class Chr> 
[[nodiscard]] constexpr auto is_carriage_return(Chr chr) -> bool;

/*! Find out whether a specific wide character is the "comma" character.
    Note that this function is guaranteed to also work correctly for narrow character (char) arguments.
    \param[in] chr  The character 
    \return  true if the character is the "comma" character
 */
[[nodiscard]] constexpr auto is_comma(wchar_t chr) -> bool;

/*! Find out whether a specific wide character is the "double quote" character.
    Note that this function is guaranteed to also work correctly for narrow character (char) arguments.
    \param[in]  chr  The character 
    \return  true if the character is the "double quote" character
 */
[[nodiscard]] constexpr auto is_double_quote(wchar_t chr) -> bool;

/*! Get the "newline" character for a specific character type.
    \tparam Chr  The character type
    \return  The "double quote" character
 */
template<class Chr>
[[nodiscard]] constexpr auto newline_char() -> Chr;

/*! Get the "comma" character for a specific character type.
    \tparam Chr  The character type
    \return  The "comma" character
 */
template<class Chr>
[[nodiscard]] constexpr auto comma_char() -> Chr;

/*! Get a NULL-terminated containing only the "comma" character, for a specific character type.
    \tparam Chr  The character type
    \return The "comma" character as a NULL-terminated string
 */
template<class Chr>
[[nodiscard]] constexpr auto comma_str() -> const Chr*;

/*! Get the "underscore" character for a specific character type.
    \tparam Chr  The character type
    \return  The "underscore" character
 */
template<class Chr>
[[nodiscard]] constexpr auto underscore_char() -> Chr;

} 

#include "twist/string_utils.ipp"

#endif 

///  @file  PropertyTree.hpp
///  PropertyTreeNode  class
///  PropertyTree  class

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_PROPERTY_TREE_HPP
#define TWIST_PROPERTY_TREE_HPP

#include "ExplicitLibType.hpp"

namespace twist {

// Property tree node ID
typedef ExplicitLibType<unsigned long, 0, k_lib_id, k_typeid_prop_tree_node_id>  PropertyTreeNodeId;  

static const PropertyTreeNodeId  k_no_prop_tree_node_id(0);  // Invalid property tree node ID

typedef std::vector<PropertyTreeNodeId>  PropertyTreeNodeIds;

///
///  A node in a property tree. It groups together the named property set, a unique ID and a name.
///  The template argument is the named property set type.
/// 
template<class TNamedPropSet>
class PropertyTreeNode {
public:
	/// Constructor
	///
	/// @param[in] id  The node ID 
	/// @param[in] name  The node name
	/// @param[in] prop_set  The property set
	///
	PropertyTreeNode(PropertyTreeNodeId id, const std::wstring& name, std::shared_ptr<TNamedPropSet> prop_set);

	/// Constructor
	///
	/// @tparam  TData  TData type. Must be convertible to a  const void*  value. 
	/// @param[in] id  The node ID 
	/// @param[in] name  The node name
	/// @param[in] data  A data value to be associated with this node
	/// @param[in] prop_set  The property set
	/// @param[in] data  The data
	///
	template<typename TData> PropertyTreeNode(PropertyTreeNodeId id, const std::wstring& name, TData data, 
			std::shared_ptr<TNamedPropSet> prop_set);

	/// Copy constructor.
	///
	PropertyTreeNode(const PropertyTreeNode& src);

	/// Move constructor.
	///
	PropertyTreeNode(PropertyTreeNode&& src);

	/// Destructor
	///
	~PropertyTreeNode();

	/// Copy-assignment operator.
	///
	PropertyTreeNode& operator=(const PropertyTreeNode& rhs);

	/// Move-assignment operator.
	///
	PropertyTreeNode& operator=(PropertyTreeNode&& rhs);

	/// Get the node ID
	///
	/// @return  The node ID
	///
	PropertyTreeNodeId get_id() const;

	/// Get the node name
	///
	/// @return  The node name
	///
	std::wstring get_name() const; 
	
	/// Get the property set
	///
	/// @return  The property set
	///
	const TNamedPropSet& get_prop_set() const;
	
	/// Get the property set
	///
	/// @return  The property set
	///
	TNamedPropSet& get_prop_set();

	/// Get the child nodes of this node (if any).
	///
	/// @return  The list of child nodes
	///
	const std::vector<PropertyTreeNode>& get_children() const;

	/// Get the data value associated with this node, if any. The default pointer value is nullptr.
	///
	/// @tparam  TData  TData type. A  const void*  value must be convertible to it. 
	/// @return  The data.
	///
	template<typename TData> TData get_data() const;

private:
	PropertyTreeNodeId  id_;
	std::wstring  name_;
	const void*  data_;
	std::shared_ptr<TNamedPropSet>  prop_set_; 

	std::vector<PropertyTreeNode>  children_;

	template<class TNamedPropSet> friend class PropertyTree;

	TWIST_CHECK_INVARIANT_DECL
};


///
///  A property tree, that is a tree structure whose nodes are property sets.
///  A property tree is used to model the properties and sub-properties of complex, tree-like structures. 
///  A property tree will always contain at least one node.
///  The template argument is the named property set type.
///
template<class TNamedPropSet>
class PropertyTree : public NonCopyable {
public:
	typedef PropertyTreeNode<TNamedPropSet>  PropTreeNode;

	/// Constructor.
	///
	/// @param[in] The top node in the tree.
	///
	PropertyTree(PropTreeNode top_node);
	
	/// Destructor.
	///
	virtual ~PropertyTree();

	/// Get a clone of the tree.
	///
	/// @return  The clone
	///
	std::unique_ptr<PropertyTree> get_clone() const;

	/// Get a the top node in the tree.
	///
	/// @return  The top node
	///
	const PropTreeNode& get_top_node() const;

	/// Get a specific node from the tree.
	///
	/// @param[in] node_id  The node ID. An exception is thrown if no node with this ID exists in the tree.
	/// @return  The tree node.
	///
	const PropTreeNode& get_node(PropertyTreeNodeId node_id) const;

	/// Get a specific node from the tree.
	///
	/// @param[in] node_id  The node ID. An exception is thrown if no node with this ID exists in the tree.
	/// @return  The tree node.
	///
	PropTreeNode& get_node(PropertyTreeNodeId node_id);

	/// Get all the nodes in the tree. 
	///
	/// @return  List containing all tree nodes; sorted topologically (no child appears before a parent)
	///
	std::vector<const PropTreeNode*> get_all_nodes() const;

	/// Find the first tree node associated to a specific data value.
	///
	/// @tparam  TData  TData type. Must be convertible to a  const void*  value. 
	/// @param[in] data  The data value.
	/// @return  The first matching tree node, or  nullptr  if there is no match.
	///
	template<typename TData> const PropTreeNode* find_node_with_data(TData data) const;

	/// Add a new node to the tree.
	/// Note that new nodes are always child nodes.
	///
	/// @param[in] node  The new node
	/// @param[in] parent_id  The ID of the node which will be its parent
	///
	void add_node(PropTreeNode child_node, PropertyTreeNodeId parent_id);

	/// Get the smallest tree node ID which is not used in this tree.
	///
	/// @return  The node ID
	///
	PropertyTreeNodeId get_next_avail_node_id() const;

private:	
	/// Look for a specific node in the tree.
	///
	/// @param[in] node_id  The node ID. 
	/// @return  The tree node, if a match is found, nullptr otherwise.
	///
	const PropTreeNode* find_node(PropertyTreeNodeId node_id) const;

	/// Look for a specific node in the tree.
	///
	/// @param[in] node_id  The node ID. 
	/// @return  The tree node, if a match is found, nullptr otherwise.
	///
	PropTreeNode* find_node(PropertyTreeNodeId node_id);

	/// Recursive, const-neutral function for finding a node with a specific ID in the tree.
	///
	template<typename NodeT> NodeT* recursive_find(NodeT& cur_node, PropertyTreeNodeId node_id) const;

	/// Recursive, const-neutral function for finding a node with a specific data value in the tree.
	///
	template<typename NodeT> NodeT* recursive_find(NodeT& cur_node, const void* data) const;

	PropTreeNode  top_node_;
	PropertyTreeNodeId  highest_node_id_;

	TWIST_CHECK_INVARIANT_DECL
};

} 

#include "PropertyTree.ipp"

#endif 

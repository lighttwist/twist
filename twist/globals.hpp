/// @file globals.hpp
/// Globals for the "twist" library

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_GLOBALS_HPP
#define TWIST_GLOBALS_HPP

#include "twist/namespace_aliases.hpp"
#include "twist/twist_os_defs.hpp"

#include <gsl/pointers>

#include <exception>
#include <set>
#include <string>
#include <tuple>
#include <vector>

namespace twist {
class RuntimeError;
}

namespace twist {

//! The typical type for sizes of, and indexes into, containers defined in the "twist" library; a signed type
using Ssize = std::ptrdiff_t;

template<class... T>
using Tuples = std::vector<std::tuple<T...>>;

template<class T1, class T2>
using Pairs = std::vector<std::pair<T1, T2>>;

template<class T>
using NotNulls = std::vector<gsl::not_null<T>>;

template<class T>
using NotNullSet = std::set<gsl::not_null<T>>;

constexpr Ssize  k_no_idx  = -1;  ///< Invalid index within a collection.
constexpr Ssize  k_no_pos  = -1;  ///< Invalid position within a collection.
constexpr Ssize  k_no_size = -1;  ///< Invalid size of a collection.

/// @cond 
// Unique library identifier. Useful for things such as explicit library types.
constexpr unsigned long  k_twist_lib_id = 3'141'592'653;

// Library type IDs
constexpr unsigned long  k_typeid_prop_id			     =  1;
constexpr unsigned long  k_typeid_prop_type			     =  2;
constexpr unsigned long  k_typeid_prop_constr_type	     =  3;
constexpr unsigned long  k_typeid_prop_tree_node_id      =  4;

constexpr unsigned long  k_typeid_zlib_compress_result   =  100;  //+move to twist::zlib, and create a k_twist_zlib_lib_id
constexpr unsigned long  k_typeid_zlib_uncompress_result =  101;
/// @endcond

/// @cond
namespace detail {

/// Invariant checker class, used by the TWIST_CHECK_INVARIANT macros.
template<class T>
class InvariantChecker {
public:
	InvariantChecker(gsl::not_null<const T*> obj) 
		: obj_{obj} 
	{ 
		obj_->check_invariant(); 
	}

	~InvariantChecker() 
	{ 
		obj_->check_invariant(); 
	}

	InvariantChecker(const InvariantChecker&) = delete;  

	InvariantChecker(InvariantChecker&&) = delete; 
	
	auto operator=(const InvariantChecker&) -> InvariantChecker& = delete;  

	auto operator=(InvariantChecker&&) -> InvariantChecker& = delete;

private:
	const gsl::not_null<const T*> obj_;
};

template<class T>
InvariantChecker<T> make_invariant_checker(T* obj)
{
	return InvariantChecker<T>{ obj };
}

[[nodiscard]] auto make_runtime_error_new(std::wstring msg, const char* func_name) -> RuntimeError;

[[nodiscard]] auto make_runtime_error_new(std::string msg, const char* func_name) -> RuntimeError;

[[nodiscard]] auto make_runtime_error_old(const char* func_name, const wchar_t* msg_format, ...) -> RuntimeError;

}
/// @endcond

/// Interpret the value of errno, generating a string with a message that describes the error condition as if 
/// set to errno by a function of the library. The error strings produced may be specific to each system and 
/// library implementation.
///
/// @return  The error string
///
std::wstring get_strerror();

}

// --- Macros ---

// TWIST_FOR is obsolete as of Visual Studio 2012; its role as a stand-in for the C++11 "range for" feature 
// has ended. 
/// @cond
#define __TWIST_APPEND_LN_3(name, ln)  name##ln
#define __TWIST_APPEND_LN_2(name, ln)  __TWIST_APPEND_LN_3(name, ln)
#define TWIST_APPEND_LN(name)  __TWIST_APPEND_LN_2(name, __LINE__)
/// @endcond
#define TWIST_FOR(it, cont)  \
				auto&& TWIST_APPEND_LN(__cont) = cont;  \
				for (auto it = std::begin(TWIST_APPEND_LN(__cont)),  \
					 __end = std::end(TWIST_APPEND_LN(__cont));  \
					 it != __end; ++it)

#define TWIST_DELETE(ptr)  delete ptr;  ptr = nullptr;

#define TWIST_DELETE_ARRAY(ptr)  delete[] ptr;  ptr = nullptr; 

#if TWIST_OS_LINUX
  #define TWIST_FUNCTION_NAME  __PRETTY_FUNCTION__
#else
  #define TWIST_FUNCTION_NAME  __FUNCTION__
#endif 

/*! Throw a twist::RuntimeError object containing a given error message and the name of the function where this macro 
    was used. The message format string and following arguments will be formatted into the message using std::format().
	The message format string must be a compile-time constant.
 */
#define TWIST_THRO2(msg_format, ...) \
	            throw twist::detail::make_runtime_error_new(std::format(msg_format, ##__VA_ARGS__), \
				                                            TWIST_FUNCTION_NAME);

/*! Throw a twist::RuntimeError object containing a given error message and the name of the function where this macro 
    was used. The message format string and following arguments will be formatted into the message using std::format().
 */
#define TWIST_VTHRO2(msg_format, ...) \
		        throw twist::detail::make_runtime_error_new(twist::formatv(msg_format, ##__VA_ARGS__), \
				                                            TWIST_FUNCTION_NAME);

/// Throw a twist::RuntimeError object containing a given error message and the name of the function where 
/// this macro was used
#define TWIST_THROW(msg_format, ...) \
		        throw twist::detail::make_runtime_error_old(TWIST_FUNCTION_NAME, msg_format, ##__VA_ARGS__);
         //+OBSOLETE: Use TWIST_THRO2/TWIST_VTHRO2

/// Disable the copy constructor and copy assignment operator for the enclosing class
#define TWIST_NO_COPY(T)  \
				T(const T&) = delete;  \
				T& operator=(const T&) = delete;  

/// Disable the copy and move constructors and assignment operators for the enclosing class
#define TWIST_NO_COPY_NO_MOVE(T)  \
				T(const T&) = delete;  \
				T(T&&) = delete;  \
				T& operator=(const T&) = delete;  \
				T& operator=(T&&) = delete;

/// Disable the move constructor and move assignment operator, and enable the default copy constructor and 
/// copy assignment operator for the enclosing class
#define TWIST_DEF_COPY_NO_MOVE(T)  \
				T(const T&) = default;  \
				T(T&&) = delete;  \
				T& operator=(const T&) = default;  \
				T& operator=(T&&) = delete;

/// Disable the copy constructor and copy assignment operator, and enable the default move constructor and 
/// move assignment operator for the enclosing class
#define TWIST_NO_COPY_DEF_MOVE(T)  \
				T(const T&) = delete;  \
				T(T&&) = default;  \
				T& operator=(const T&) = delete;  \
				T& operator=(T&&) = default;

/// Disable the move constructors and copy and move assignment operators for the enclosing class
#define TWIST_NO_COPY_ASSIGN_NO_MOVE(T)  \
				T(T&&) = default;  \
				T& operator=(const T&) = delete;  \
				T& operator=(T&&) = default;

#ifdef _DEBUG
	#define TWIST_ASSERT(x)  assert(x); 

	#define TWIST_CHECK_INVARIANT_FRIEND_DECL  \
				template<class TWIST_APPEND_LN(T)> friend class twist::detail::InvariantChecker;

	#define TWIST_CHECK_INVARIANT_DECL  \
				TWIST_CHECK_INVARIANT_FRIEND_DECL  \
				void check_invariant() const noexcept;

	#define TWIST_CHECK_INVARIANT  \
				const auto& TWIST_APPEND_LN(invariant_checker_) = twist::detail::make_invariant_checker(this);  \
				TWIST_APPEND_LN(invariant_checker_);

	#define TWIST_CHECK_INVARIANT_NO_SCOPE  { TWIST_CHECK_INVARIANT }

	#define TWIST_CHECK_STRUCT_INVARIANT(s)  (s).check_invariant();

#else			
	#define TWIST_ASSERT(x)  ((void)(x));
	#define TWIST_TEST_INVARIANT_DECL  
	#define TWIST_CHECK_INVARIANT_FRIEND_DECL
	#define TWIST_CHECK_INVARIANT_DECL
	#define TWIST_CHECK_INVARIANT
	#define TWIST_CHECK_INVARIANT_NO_SCOPE  
	#define TWIST_CHECK_STRUCT_INVARIANT(s)
#endif 

#endif 

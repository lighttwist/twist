/// @file cast.ipp
/// Inline implementation file for "cast.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace twist {

namespace detail {

template<class DestTuple, class... SrcArgs, size_t... I>
[[nodiscard]] auto static_cast_tuple_impl(const std::tuple<SrcArgs...>& src,  std::index_sequence<I...>) -> DestTuple
{
    return std::make_tuple(static_cast<std::tuple_element_t<I, DestTuple>>(std::get<I>(src))...);
}

}

template<class Dest, class Src>  
[[nodiscard]] auto dyn_nonull_cast(Src src) -> gsl::not_null<Dest> 
{
	return gsl::not_null{dynamic_cast<Dest>(src)};
}

template<class Dest, class Src>  
[[nodiscard]] auto dyn_nonull_cast(gsl::not_null<Src> src) -> gsl::not_null<Dest> 
{
	return gsl::not_null{dynamic_cast<Dest>(src.get())};
}

template<class Dest, class Src>  
[[nodiscard]] auto static_nonull_cast(Src src) -> gsl::not_null<Dest>
{
	return gsl::not_null{static_cast<Dest>(src)};
}

template<class T>
requires std::is_enum_v<T>
[[nodiscard]] auto as_underlying(T val) -> std::underlying_type_t<T>
{
    return static_cast<std::underlying_type_t<T>>(val);
}

template<class... DestArgs, class... SrcArgs>
[[nodiscard]] auto static_cast_tuple(const std::tuple<SrcArgs...>& src) -> std::tuple<DestArgs...>
{
    static const auto N = sizeof...(SrcArgs);
    static_assert(sizeof...(DestArgs) == N, "Wrong number of template parameters.");
    return detail::static_cast_tuple_impl<std::tuple<DestArgs...>>(src, std::make_index_sequence<N>{});
}

}

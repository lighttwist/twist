/// @file std_variant_utils.ipp
/// Inline implementation file for "twist_std_variant_utils.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace twist {

namespace detail {

template<typename Variant, typename T, std::size_t index = 0>
[[nodiscard]] constexpr auto variant_index_impl() -> std::size_t
{
	if constexpr (index == std::variant_size_v<Variant>) {
		return index;
	} 
	else if constexpr (std::is_same_v<std::variant_alternative_t<index, Variant>, T>) {
		return index;
	} 
	else {
		return variant_index_impl<Variant, T, index + 1>();
	}
} 

}

template<typename Variant, typename Ty>
[[nodiscard]] constexpr auto variant_index() -> std::size_t 
{
	return detail::variant_index_impl<Variant, Ty, 0>();
} 

//  Very simple syntactic sugar functions

template<class... Types>
[[nodiscard]] constexpr uint16_t get_uint16(const std::variant<Types...>& var)
{
	return std::get<uint16_t>(var);
}

template<class... Types>
[[nodiscard]] constexpr double get_double(const std::variant<Types...>& var)
{
	return std::get<double>(var);
}

template<class... Types>
[[nodiscard]] constexpr const std::string& get_string(const std::variant<Types...>& var)
{
	return std::get<std::string>(var);
}

template<class... Types>
[[nodiscard]] constexpr const std::wstring& get_wstring(const std::variant<Types...>& var)
{
	return std::get<std::wstring>(var);
}

template<class Elem, class... Types>
[[nodiscard]] constexpr const std::vector<Elem>& get_vector(const std::variant<Types...>& var)
{
	return std::get<std::vector<Elem>>(var);
}

template<class Elem, class... Types>
[[nodiscard]] constexpr std::vector<Elem>& get_vector(std::variant<Types...>& var)
{
	return std::get<std::vector<Elem>>(var);
}

template<class... Types>
[[nodiscard]] constexpr bool holds_uint16(const std::variant<Types...>& var) noexcept
{	
	return std::holds_alternative<uint16_t>(var);
}

template<class... Types>
[[nodiscard]] constexpr bool holds_double(const std::variant<Types...>& var) noexcept
{	
	return std::holds_alternative<double>(var);
}

template<class... Types>
[[nodiscard]] constexpr bool holds_float(const std::variant<Types...>& var) noexcept
{	
	return std::holds_alternative<float>(var);
}

template<class... Types>
[[nodiscard]] constexpr bool holds_string(const std::variant<Types...>& var) noexcept
{	
	return std::holds_alternative<std::string>(var);
}

template<class... Types>
[[nodiscard]] constexpr bool holds_wstring(const std::variant<Types...>& var) noexcept
{	
	return std::holds_alternative<std::wstring>(var);
}

template<class Elem, class... Types>
[[nodiscard]] constexpr bool holds_vector(const std::variant<Types...>& var) noexcept
{	
	return std::holds_alternative<std::vector<Elem>>(var);
}

template<class... Types>
[[nodiscard]] constexpr bool holds_monostate(const std::variant<Types...>& var) noexcept
{	
	return std::holds_alternative<std::monostate>(var);
}

}



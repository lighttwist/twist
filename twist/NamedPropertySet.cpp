///  @file  NamedPropertySet.cpp
///  Implementation file for "NamedPropertySet.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "NamedPropertySet.hpp"

namespace twist {

NamedPropertySet::NamedPropertySet() 
	: prop_set_(std::make_unique<PropertySet>())
	, prop_ids_to_text_()
	, prop_names_to_ids_()
{
	TWIST_CHECK_INVARIANT
}


NamedPropertySet::NamedPropertySet(std::unique_ptr<PropertySet> prop_set) 
	: prop_set_(move(prop_set))
	, prop_ids_to_text_()
	, prop_names_to_ids_()
{
	TWIST_CHECK_INVARIANT
}


NamedPropertySet::NamedPropertySet(const NamedPropertySet& src)
	: prop_set_(std::make_unique<PropertySet>(*src.prop_set_))
	, prop_ids_to_text_(src.prop_ids_to_text_)
	, prop_names_to_ids_(src.prop_names_to_ids_)
{
	TWIST_CHECK_INVARIANT
}


NamedPropertySet::NamedPropertySet(NamedPropertySet&& src)
	: prop_set_(move(src.prop_set_))
	, prop_ids_to_text_(move(src.prop_ids_to_text_))
	, prop_names_to_ids_(move(src.prop_names_to_ids_))
{
	TWIST_CHECK_INVARIANT
}


NamedPropertySet::~NamedPropertySet()
{
	TWIST_CHECK_INVARIANT
}


NamedPropertySet& NamedPropertySet::operator=(const NamedPropertySet& rhs)
{
	TWIST_CHECK_INVARIANT
	if (&rhs != this) {
		prop_set_ = std::make_unique<PropertySet>(*rhs.prop_set_);
		prop_ids_to_text_ = rhs.prop_ids_to_text_;
		prop_names_to_ids_ = rhs.prop_names_to_ids_;
	}
	TWIST_CHECK_INVARIANT
	return *this;
}


NamedPropertySet& NamedPropertySet::operator=(NamedPropertySet&& rhs)
{
	TWIST_CHECK_INVARIANT
	if (&rhs != this) {
		prop_set_ = move(rhs.prop_set_);
		prop_ids_to_text_ = move(rhs.prop_ids_to_text_);
		prop_names_to_ids_ = move(rhs.prop_names_to_ids_);
	}
	TWIST_CHECK_INVARIANT
	return *this;
}


void NamedPropertySet::define_int_prop(PropertyId prop_id, std::wstring prop_name, int val, std::wstring prop_descr)
{
	TWIST_CHECK_INVARIANT
	define_prop(prop_id, prop_name, val, prop_descr);
	TWIST_CHECK_INVARIANT
}


void NamedPropertySet::define_float_prop(PropertyId prop_id, std::wstring prop_name, double val, std::wstring prop_descr)
{
	TWIST_CHECK_INVARIANT
	define_prop(prop_id, prop_name, val, prop_descr);
	TWIST_CHECK_INVARIANT
}


void NamedPropertySet::define_string_prop(PropertyId prop_id, std::wstring prop_name, std::wstring val, std::wstring prop_descr)
{
	TWIST_CHECK_INVARIANT
	define_prop(prop_id, prop_name, val, prop_descr);
	TWIST_CHECK_INVARIANT
}


void NamedPropertySet::define_bool_prop(PropertyId prop_id, std::wstring prop_name, bool val, std::wstring prop_descr)
{
	TWIST_CHECK_INVARIANT
	define_prop(prop_id, prop_name, val, prop_descr);
	TWIST_CHECK_INVARIANT
}


int NamedPropertySet::get_int_value(std::wstring prop_name) const
{
	TWIST_CHECK_INVARIANT
	return prop_set_->get_int_value(get_prop_id(prop_name));
}


void NamedPropertySet::set_int_value(std::wstring prop_name, int val)
{
	TWIST_CHECK_INVARIANT
	prop_set_->set_int_value(get_prop_id(prop_name), val);
	TWIST_CHECK_INVARIANT
}


double NamedPropertySet::get_float_value(std::wstring prop_name) const
{
	TWIST_CHECK_INVARIANT
	return prop_set_->get_float_value(get_prop_id(prop_name));
}


void NamedPropertySet::set_float_value(std::wstring prop_name, double val)
{
	TWIST_CHECK_INVARIANT
	prop_set_->set_float_value(get_prop_id(prop_name), val);
	TWIST_CHECK_INVARIANT
}


std::wstring NamedPropertySet::get_string_value(std::wstring prop_name) const
{
	TWIST_CHECK_INVARIANT
	return prop_set_->get_string_value(get_prop_id(prop_name));
}


void NamedPropertySet::set_string_value(std::wstring prop_name, std::wstring val)
{
	TWIST_CHECK_INVARIANT
	prop_set_->set_string_value(get_prop_id(prop_name), val);
	TWIST_CHECK_INVARIANT
}


bool NamedPropertySet::get_bool_value(std::wstring prop_name) const
{
	TWIST_CHECK_INVARIANT
	return prop_set_->get_bool_value(get_prop_id(prop_name));
}


void NamedPropertySet::set_bool_value(std::wstring prop_name, bool val)
{
	TWIST_CHECK_INVARIANT
	prop_set_->set_bool_value(get_prop_id(prop_name), val);
	TWIST_CHECK_INVARIANT
}


PropertyId NamedPropertySet::get_prop_id(std::wstring prop_name) const
{
	TWIST_CHECK_INVARIANT
	auto it = prop_names_to_ids_.find(prop_name);
	if (it == end(prop_names_to_ids_)) {
		TWIST_THROW(L"A property named \"%s\" is not defined.", prop_name.c_str());
	}
	TWIST_CHECK_INVARIANT
	return it->second;
}


PropertyType NamedPropertySet::get_prop_type(std::wstring prop_name) const
{
	TWIST_CHECK_INVARIANT
	return get_prop_type(get_prop_id(prop_name));
}


std::wstring NamedPropertySet::get_prop_name(PropertyId prop_id) const
{
	TWIST_CHECK_INVARIANT
	return get_prop_text(prop_id).first;
}


std::wstring NamedPropertySet::get_prop_descr(PropertyId prop_id) const
{
	TWIST_CHECK_INVARIANT
	return get_prop_text(prop_id).second;
}


std::pair<std::wstring, std::wstring> NamedPropertySet::get_prop_text(PropertyId prop_id) const
{
	TWIST_CHECK_INVARIANT
	auto it = prop_ids_to_text_.find(prop_id);
	if (it == end(prop_ids_to_text_)) {
		TWIST_THROW(L"A property with ID %d is not defined.", prop_id.value());
	}
	TWIST_CHECK_INVARIANT
	return it->second;
}


int NamedPropertySet::get_int_value(PropertyId prop_id) const
{
	TWIST_CHECK_INVARIANT
	return prop_set_->get_int_value(prop_id);
}


void NamedPropertySet::set_int_value(PropertyId prop_id, int val)
{
	TWIST_CHECK_INVARIANT
	return prop_set_->set_int_value(prop_id, val);
}


double NamedPropertySet::get_float_value(PropertyId prop_id) const
{
	TWIST_CHECK_INVARIANT
	return prop_set_->get_float_value(prop_id);
}


void NamedPropertySet::set_float_value(PropertyId prop_id, double val)
{
	TWIST_CHECK_INVARIANT
	return prop_set_->set_float_value(prop_id, val);
}


std::wstring NamedPropertySet::get_string_value(PropertyId prop_id) const
{
	TWIST_CHECK_INVARIANT
	return prop_set_->get_string_value(prop_id);
}


void NamedPropertySet::set_string_value(PropertyId prop_id, std::wstring val)
{
	TWIST_CHECK_INVARIANT
	return prop_set_->set_string_value(prop_id, val);
}


bool NamedPropertySet::get_bool_value(PropertyId prop_id) const
{
	TWIST_CHECK_INVARIANT
	return prop_set_->get_bool_value(prop_id);
}


void NamedPropertySet::set_bool_value(PropertyId prop_id, bool val)
{
	TWIST_CHECK_INVARIANT
	return prop_set_->set_bool_value(prop_id, val);
}


bool NamedPropertySet::prop_exists(PropertyId prop_id) const
{
	TWIST_CHECK_INVARIANT
	return prop_set_->prop_exists(prop_id);
}


PropertyType NamedPropertySet::get_prop_type(PropertyId prop_id) const
{
	TWIST_CHECK_INVARIANT
	return prop_set_->get_prop_type(prop_id);
}


std::map<PropertyId, PropertyType> NamedPropertySet::get_props() const
{
	TWIST_CHECK_INVARIANT
	return prop_set_->get_props();
}


void NamedPropertySet::define_prop_text(PropertyId prop_id, const std::wstring& prop_name, const std::wstring& prop_descr)
{
	TWIST_CHECK_INVARIANT
	// First, check that the name is valid
	trim_whitespace(prop_name);
	if (prop_name.empty()) {
		TWIST_THROW(L"Property names cannot be blank.");
	}

	// Second, check that the name isn't already used
	if (prop_names_to_ids_.count(prop_name) > 0) {
		TWIST_THROW(L"A property named \"%s\" already exists in the set.", prop_name.c_str());
	}

	// Fourth, store the name and description in this class.
	prop_ids_to_text_.insert( std::make_pair(prop_id, std::make_pair(prop_name, prop_descr)) );
	prop_names_to_ids_.insert( std::make_pair(prop_name, prop_id) );
	TWIST_CHECK_INVARIANT
}


const PropertySet& NamedPropertySet::get_prop_set() const
{
	TWIST_CHECK_INVARIANT
	return *prop_set_;
}


PropertySet& NamedPropertySet::get_prop_set()
{
	TWIST_CHECK_INVARIANT
	return *prop_set_;
}


#ifdef _DEBUG
void NamedPropertySet::check_invariant() const noexcept
{
	assert(prop_set_);
	assert(prop_ids_to_text_.size() == prop_names_to_ids_.size());
}
#endif

} // namespace twist


/// @file string_utils.cpp
/// Implementation file for "string_utils.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/string_utils.hpp"

#include "twist/globals.hpp"
#include "twist/os_utils.hpp"
#include "twist/math/numeric_utils.hpp"

#include <cstdarg>

namespace twist {

// --- Types ---

// Unary predicate: whether an ANSI character is not whitespace.
struct IsNotWspace {
	bool operator()(char chr) const { return !isspace(chr); }
};

// --- Local functions ---

// Count the number of occurrences of a subrange within a range.
//
// @tparam  TIter  Iterator type for the range. Must be an input iterator.
// @param[in] first1  An iterator addressing the position of the first element in the range.
// @param[in] last1  An input iterator addressing the position that is one past the final element in the range.
// @param[in] first2  An iterator addressing the position of the first element in the subrange.
// @param[in] last2  An input iterator addressing the position that is one past the final element in the subrange.
// @return  The count.
//
template<typename TIter>
size_t count_subranges(TIter first1, TIter last1, TIter first2, TIter last2)
{
	size_t count = 0;

	const size_t subrangeLen = std::distance(first2, last2);
	TIter it = first1;

	while ((it = std::search(it, last1, first2, last2)) != last1) {
		std::advance(it, subrangeLen);
		count++;
	}

	return count;
}

// --- Global functions ---

std::wstring to_str(double value, unsigned int precision, FloatStrFormat format, wchar_t decimal_sep)
{
	static const wchar_t k_def_decimal_sep = L'.';

	// Put together the mask string
	wchar_t mask_str[32];
	size_t i = 0;
	mask_str[i++] = L'%';
	
	if (precision > 0) {
		mask_str[i++] = k_def_decimal_sep;
		
		const auto precision_str = std::to_wstring(precision);
		for (auto c : precision_str) {
			mask_str[i++] = c;		
		}
	}		

	mask_str[i++] = static_cast<wchar_t>(format);
	mask_str[i] = L'\0'; 

	// Format the number
	static const size_t k_max_text = 64;
	wchar_t text[k_max_text];
	swprintf(text, k_max_text, mask_str, value);

	if (decimal_sep != k_def_decimal_sep) {
		wchar_t* text_decimal_sep = wcschr(text, k_def_decimal_sep);
		if (text_decimal_sep != nullptr) {
			*text_decimal_sep = decimal_sep;
		}
	}

	return text;
}


std::wstring to_str(wchar_t chr)
{
	return std::wstring(1, chr);
}


long to_long(const std::wstring& str)
{
	wchar_t* end{};
	return std::wcstol(str.c_str(), &end, 10/*radix*/);
}


bool is_numeric(std::wstring_view str)
{
	if (str.empty()) return false;
	
	const bool has_non_digit_char = has_if(str, [](wchar_t el) {
		return !iswdigit(el);
	});
	return !has_non_digit_char;
}


bool is_numeric_or_fp(const std::wstring& str)
{
	if (str.empty()) return false;
	
	const auto dec_sep = get_os_decimal_sep();

	bool dec_sep_found = false;
	bool e_found = false;

	for (auto ch: str) {
		if (ch == dec_sep) {
			if (dec_sep_found) return false;
			dec_sep_found = true;
		}
		else if (ch == 'e' || ch == 'E') {
			if (e_found) return false;
			e_found = true;
		} 
		else if (!iswdigit(ch) && ch != '-' && ch != '+') {
			return false;
		}
	}
	return true;
}


wchar_t get_c_runtime_decimal_sep()
{
	const char* decimal_sep_str = localeconv()->decimal_point;
	const size_t decimal_sel_len = strlen(decimal_sep_str);
	if (decimal_sel_len != 1) {
		TWIST_THROW(L"The decimal separator \"%s\" for the current locale consists of %d characters.", 
			decimal_sep_str, decimal_sel_len);
	}
	return *decimal_sep_str;
}

auto wequal_no_case(std::wstring_view str1, std::wstring_view str2, const std::locale& locale) -> bool
{
    return equal_no_case<wchar_t>(str1, str2, locale);
}

unsigned int count_non_text_chars(std::wstring_view str) 
{
	unsigned int ret = 0;
	for (const auto el : str) {
		if (!iswalpha(el) && !iswdigit(el) && !iswpunct(el) && !iswspace(el)) {
			++ret;
		}
	}
	return ret;
}


std::wstring strip_non_text_chars(const std::wstring& str)
{
	std::wstring ret;
	for (const auto el : str) {
		
		if (iswalpha(el) || iswdigit(el) || iswpunct(el) || iswspace(el)) {
			ret += el;
		}
	}
	return ret;
}

auto is_whitespace(std::wstring_view str) -> bool
{
	return count_if(str, [](auto c) { return !iswspace(c); } ) == 0;
}

auto is_whitespace(const std::wstring& str) -> bool
{
	return is_whitespace(std::wstring_view{str});
}

auto is_whitespace(const wchar_t* str, const wchar_t* str_end) -> bool
{
	return std::count_if(str, str_end, [](auto c) { return !iswspace(c); } ) == 0;
}

auto has_whitespace(std::wstring_view str) -> bool
{
	return rg::any_of(str, iswspace);
}

unsigned int count_whitespace_chars(const std::wstring& str) 
{
	return static_cast<unsigned int>(count_if(str, iswspace));
}

std::wstring strip_whitespace_chars(const std::wstring& str)
{
	std::wstring ret;
	for (const auto el : str) {		
		
		if (!isspace(el)) {
			ret += el;
		}
	}
	return ret;
}

std::wstring trim_lead_whitespace(std::wstring_view str)
{
	std::wstring ret;

	bool non_wpace_char_found = false;
	for (const auto el : str) {

		if (!iswspace(el)) {
			non_wpace_char_found = true;
		}
		if (non_wpace_char_found) {
			ret += el;
		}
	}

	return ret;
}

std::wstring trim_trail_whitespace(std::wstring_view str)
{
	std::wstring ret;

	bool non_wpace_char_found = false;
	for (auto rit = str.rbegin(); rit != str.rend(); ++rit) {
		const wchar_t chr = *rit;
		if (!iswspace(chr)) {
			non_wpace_char_found = true;
		}
		if (non_wpace_char_found) {
			// Insert the character at the beginning of the ret string.
			ret.insert(ret.begin(), chr);
		}
	}

	return ret;
}


std::wstring trim_whitespace(std::wstring_view str)
{
	const auto ret = trim_lead_whitespace(str);
	return trim_trail_whitespace(ret);
}


std::wstring trim_lead_char(const std::wstring& str, wchar_t chr)
{
	const auto iters = trim_left(begin(str), end(str), chr);
	return { iters.first, iters.second };
}


std::wstring trim_trail_char(const std::wstring& str, wchar_t chr)
{
	const auto iters = trim_right(begin(str), end(str), chr);
	return { iters.first, iters.second };
}


std::wstring trim_char(const std::wstring& str, wchar_t chr)
{
	const auto iters = trim(begin(str), end(str), chr);
	return { iters.first, iters.second };
}


std::vector<size_t> find_all_substr(std::wstring_view str, std::wstring_view substr)
{
	std::vector<size_t> positions;
	const size_t substr_len = substr.size();

 	size_t pos = 0;
	while ((pos = str.find(substr, pos)) != std::wstring::npos) {
		positions.push_back(pos);
		pos += substr_len;
	}

	return positions;
}


std::wstring replace_substr_at(const std::wstring& str, const std::wstring& old_substr, 
		const std::wstring& new_substr, size_t pos)
{
	if (pos > str.length() - old_substr.length()) {
		TWIST_THROW(L"Invalid substring position.");
	}
	if (old_substr.compare(str.substr(pos, old_substr.length())) != 0) {
		TWIST_THROW(L"Substring not found at the position.");
	}
	return str.substr(0, pos) + new_substr + str.substr(pos + old_substr.length(), std::wstring::npos);
}


unsigned int count_substr(const std::wstring& str, const std::wstring& substr) 
{
	unsigned int ret = 0;

	const auto substr_len = substr.size();
	size_t pos = 0;
	while ((pos = str.find(substr, pos)) != std::wstring::npos) {
		++ret;
		pos += substr_len;
	}

	return ret;
}

std::wstring format_str(const wchar_t* format, ...)
{
	auto args = static_cast<va_list>(nullptr);
	va_start(args, format);
	auto ret = format_str(format, args);
	va_end(args);
	return ret;
}

std::wstring format_str(const wchar_t* format, va_list arg_list)
{
	static const size_t buf_size = 8192;
	auto buffer = std::vector<wchar_t>(buf_size);

	vswprintf(buffer.data(), buf_size, format, arg_list);

	return std::wstring{buffer.data()};
}

bool ends_in(const std::wstring& str, const std::wstring& substr)
{
	return str.find(substr) == str.size() - substr.size();
}

auto spaces(Ssize count) -> std::wstring
{
    assert(count >= 0);
    return std::wstring(static_cast<size_t>(count), L' ');
}

Ssize count_delimited_fields(std::wstring_view str, const std::wstring& delimiter) 
{
	const wchar_t* begin = str.data();
	const wchar_t* delimiter_begin = delimiter.data();
    return count_delimited_fields(begin, begin + str.size(), delimiter_begin, delimiter_begin + delimiter.size());
}


bool get_delimited_field(std::wstring_view str, const std::wstring& delimiter, Ssize field_idx, 
		std::wstring& field) 
{
    bool ret = false;
	const wchar_t* begin = str.data();
	const wchar_t* end = begin + str.size();
	const wchar_t* delimiter_begin = delimiter.data();

    auto result = get_delimited_field(
			begin, end, delimiter_begin, delimiter_begin + delimiter.size(), field_idx);
	if (result.first != end) {

		field.assign(result.first, result.second);
		ret = true;
	}
    return ret;
}


std::wstring capitalise_first_letter(const std::wstring& str)
{
	std::wstring ret;
	bool found = false;
	transform(str, back_inserter(ret), [&](wchar_t el)->wchar_t {
		if (!found && iswalpha(el) && !iswdigit(el)) {
			found = true;
			return towupper(el);
		}
		else {
			return el;
		}
	});
	return ret;
}


bool is_real_number(const std::wstring& str)
{
    const auto last = end(str);     //+ Use iterators, iteration is (often considerably) faster than indexing with []
    auto first = begin(str);
    if (first != last && *first == L'-') {
        // Advance past the leading minus character to obtain the unsigned string
        ++first;
    }
    if (first == last) {
        // The string is either empty or consists only of the minus character
        return false;
    }

    const wchar_t decimal_sep = get_c_runtime_decimal_sep();  //+ You only want to call this once, outside the loop
    bool decimal_sep_found = false;

    const auto it = find_if(first, last, [&](wchar_t el) {

        if (!iswdigit(el)) {
            if (el != decimal_sep || decimal_sep_found) {
                // The current character is either invalid, or the second decimal separator
                return true;
            }
            if (distance(first, last) == 1) {
                // The (unsigned) string consists only of the decimal separator
                return true;
            }
            decimal_sep_found = true;
        }
        return false;    
    });

    return it == last;

}


bool is_integer_number(const std::wstring& str)
{
    const auto last = end(str);     
    auto first = begin(str);
    if (first != last && *first == L'-') {
        // Advance past the leading minus character to obtain the unsigned string
        ++first;
    }
    if (first == last) {
        // The string is either empty or consists only of the minus character
        return false;
    }

	const auto it  = find_if(first, last, [](wchar_t el) { 
			return !iswdigit(el); 
	});
	
	return it == last;
}


bool is_posint(const std::wstring& str)
{
	return !str.empty() && find_if_not(str, iswdigit) == end(str);
}


std::wstring ensure_unique(const std::wstring& name, const std::vector<std::wstring>& existing_names, 
		const std::wstring& first_dif, const std::wstring& subseq_difs_mask, const std::wstring& idx_placeholder, 
		bool case_sens, size_t dif_pos)
{
	// Check that the differentiator position makes sense
	if (dif_pos > name.size() && dif_pos != std::wstring::npos) {
		TWIST_THROW(L"Invalid 'differentiator' position %d.", dif_pos);
	}
	// Check that the differentiator contains exactly one instance of the index placeholder
	if (count_subranges(begin(subseq_difs_mask), end(subseq_difs_mask), begin(idx_placeholder), end(idx_placeholder)) != 1) {
		TWIST_THROW(L"The 'differentiator' mask \"%s\" must contain exactly one instance of the index placeholder \"%s\".", subseq_difs_mask.c_str(), idx_placeholder.c_str());
	}

	auto name_exists = [&](const std::wstring& name_candidate)->bool {
		if (case_sens) {
			return std::find(begin(existing_names), end(existing_names), name_candidate) 
					!= end(existing_names);
		}
		else {
			const CompStringsNoCase<wchar_t> comp_strings;
			const auto it = std::find_if(begin(existing_names), end(existing_names), 
					[=](const std::wstring& el) {
						return comp_strings(el, name_candidate);	
					});
			return it != end(existing_names); 
		}
	};

	std::function<std::wstring (const std::wstring&, unsigned int)> make_unique_name = 
			[&](const std::wstring& name_candidate, unsigned int counter)->std::wstring {
		
				if (!name_exists(name_candidate)) {
					// Bingo
					return name_candidate;
				}
				else {
					// We need to recurse
					std::wstring new_name_candidate = name;

					// Work out the insertion position  					
					const auto pos = dif_pos != std::wstring::npos ? dif_pos : new_name_candidate.size();
					// Work out the differentiator string  					
					const auto dif = counter == 0 ? first_dif : 
							replace_all_substr<wchar_t>(
									subseq_difs_mask, idx_placeholder, std::to_wstring(counter + 1));					
					// Create the new name candidate
					new_name_candidate.insert(pos, dif);

					// Recurse
					return make_unique_name(new_name_candidate, ++counter);
				}
			};

	return make_unique_name(name, 0);
}
 

std::wstring indent(std::wstring_view str, std::wstring_view indent_str)
{
	assert(!indent_str.empty());
	assert(is_whitespace(indent_str));
	assert(!has(indent_str, '\n'));

	const std::wstring indent_string{indent_str};
	return replace_all_substr<wchar_t>(indent_string + str, L"\n", L"\n" + indent_string);
}


std::vector<wchar_t> string_to_vector(const wchar_t* str)
{
	const size_t len = wcslen(str);

	const wchar_t* str_end_it = str + len + 1;

	std::vector<wchar_t> vec(len + 1);
	std::copy(str, str_end_it, begin(vec));

	return vec;
}


std::vector<wchar_t> string_to_vector(const std::wstring& str)
{
	const size_t len = str.size();

	std::vector<wchar_t> vec(len + 1);
	copy(str, begin(vec));
	vec[len] = L'\0';

	return vec;
}

auto escape_regex(std::wstring regex) -> std::wstring
{
	auto replace = [&](auto old_str, auto new_str) {
		regex = replace_all_substr<wchar_t>(regex, old_str, new_str);
	};
    
	replace(L"\\", L"\\\\");
    replace(L"^", L"\\^");
    replace(L".", L"\\.");
    replace(L"$", L"\\$");
    replace(L"|", L"\\|");
    replace(L"(", L"\\(");
    replace(L")", L"\\)");
    replace(L"[", L"\\[");
    replace(L"]", L"\\]");
    replace(L"*", L"\\*");
    replace(L"+", L"\\+");
    replace(L"?", L"\\?");
    replace(L"/", L"\\/");

	return regex;
}

bool match_pattern_with_single_posint_wildcard(const std::wstring& pattern, const std::wstring& wcard, 
		const std::wstring& str, int& int_val)
{
	const auto wcard_pos = pattern.find(wcard);
	if (wcard_pos == std::wstring::npos) {
		TWIST_THROW(L"The pattern string \"%s\" does not contain the wildcard \"%s\".", pattern.c_str(), wcard.c_str());
	}
	const auto pattern_head = pattern.substr(0, wcard_pos);
	const auto pattern_tail = pattern.substr(wcard_pos + wcard.size(), std::wstring::npos);
	if (pattern_tail.find(wcard) != std::wstring::npos) {
		TWIST_THROW(L"The pattern string \"%s\" contains multiple instances of the wildcard \"%s\".", pattern.c_str(), wcard.c_str());
	}
	if (str.size() <= pattern_head.size() + pattern_tail.size()) {
		return false;  // The string is too small to match the pattern
	}
	if (str.substr(0, pattern_head.size()) != pattern_head) {
		return false;  // The string beginning does not match the pattern head
	}
	if (str.substr(str.size() - pattern_tail.size(), std::wstring::npos) != pattern_tail) {
		return false;  // The string ending does not match the pattern tail
	}
	const auto wcard_match = str.substr(pattern_head.size(), str.size() - pattern_head.size() - pattern_tail.size());
	if (!is_posint(wcard_match)) {
		return false;
	}
	int_val = to_int(wcard_match);
	return true;
}

std::wstring to_string(std::string_view ansi_str)
{
	return {ansi_str.data(), ansi_str.data() + ansi_str.size()};
}

std::wstring to_string(const std::wstring& str)
{
	return str;
}

std::wstring ansi_to_string(std::string_view ansi_str)
{
	return to_string(ansi_str);
}

std::string string_to_ansi(std::wstring_view str)
{
	std::string ansi_str;
	ansi_str.reserve(str.size());
	for (const auto c : str) {
		if (auto ansi_c = static_cast<char>(c); ansi_c != 0) {  //+why the zero check?
			ansi_str += ansi_c;
		}
	}
	return ansi_str; 
}

std::string string_to_ansi(std::string_view str)
{
	return std::string{ str };
}

std::string path_to_ansi(const fs::path& path)
{
	return string_to_ansi(path.wstring());
}

std::string format_ansi(const char* format, ...)
{
	static const auto k_buf_size = size_t{4096};
	auto buffer = std::vector<char>(k_buf_size);

	auto args = static_cast<va_list>(nullptr);
	va_start(args, format);
	vsprintf(buffer.data(), format, args);
	va_end(args);

	return buffer.data();
}

auto is_whitespace(std::string_view str) -> bool
{
	const auto str_end = end(str);
	return std::find_if(begin(str), str_end, IsNotWspace{}) == str_end;
}

auto is_whitespace(const char* str, const char* str_end) -> bool
{
	return std::find_if(str, str_end, IsNotWspace{}) == str_end;
}

auto has_whitespace(std::string_view str) -> bool
{
	return rg::any_of(str, isspace);
}

std::string ansi_trim_lead_whitespace(std::string_view str)
{
	std::string ret;

	bool non_whitespace_char_found = false;
	for (const auto el : str) {
		if (isascii(el) && !isspace(el)) {
			non_whitespace_char_found = true;
		}
		if (non_whitespace_char_found) {
			ret += el;
		}
	}

	return ret;
}

std::string ansi_trim_trail_whitespace(std::string_view str)
{
	std::string ret;

	bool non_whitespace_char_found = false;
	for (auto rit = str.rbegin(); rit != str.rend(); ++rit) {
		const char chr = *rit;
		if (isascii(chr) && !isspace(chr)) {
			non_whitespace_char_found = true;
		}
		if (non_whitespace_char_found) {
			// Insert the character at the beginning of the ret string.
			ret.insert(ret.begin(), chr);
		}
	}

	return ret;
}


std::string trim_whitespace(std::string_view str)
{
	const auto ret = ansi_trim_lead_whitespace(str);
	return ansi_trim_trail_whitespace(ret);
}


void trim_whitespace(char* str)
{
	char* str_end = str + strlen(str);
	if (str != str_end) {
		std::pair<char*, char*> substr(find_first_last_if(str, str_end, IsNotWspace()));
		if (substr.first == str_end) {
			// The string is entirely made up of whitespace. Zap it.
			*str = '\0';
		}
		else {
			if (substr.first != str) {
				// There is some leading whitespace.
				// Shift the valid substring to the beginning of the string.
				std::copy(substr.first, substr.second, str);
			}
			// Make sure a null character follows the subrange.
			const size_t substr_len = substr.second - substr.first;
			*(str + substr_len) = '\0';
		}
	}
}

std::string trim_trail_char(const std::string& str, char chr)
{
	const auto iters = trim_right(begin(str), end(str), chr);
	return std::string(iters.first, iters.second);
}

auto ansi_spaces(Ssize count) -> std::string
{
    assert(count >= 0);
    return std::string(static_cast<size_t>(count), ' ');
}

namespace detail {

void compile_time_tests()
{
	// I am sure that this is guaranteed in the standard, but as I haven't actually seen it written...
	static_assert(is_newline('\n'), "Narrow to wide character mismatch for '\\n'");
	static_assert(is_carriage_return('\r'), "Narrow to wide character mismatch for '\\r'");	
	static_assert(is_comma(','), "Narrow to wide character mismatch for ','");	
	static_assert(is_double_quote('\"'), "Narrow to wide character mismatch for '\"'");	
}

}

}

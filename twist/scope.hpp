///  @file  scope.hpp
///  C++ scope guard and scope pseudo-statement implementation
 
//   Based on the work of Nicolas Guillemot
//   http://cppsecrets.blogspot.ca/2013/11/ds-scope-statement-in-c.html
//
//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_SCOPE_HPP
#define TWIST_SCOPE_HPP

#include <exception>
#include <utility>
#include <type_traits>

template<typename F, typename CleanupPolicy>
class scope_guard : CleanupPolicy, CleanupPolicy::installer {
	using CleanupPolicy::cleanup;
	using CleanupPolicy::installer::install;

	typename std::remove_reference<F>::type f_;

public:
	scope_guard(F&& f) : f_(std::forward<F>(f)) 
	{ 
		install();
	}

	~scope_guard() 
	{
		cleanup(f_);
	}
};

//////////////////////////////////////////////
// Installation policies
//////////////////////////////////////////////

struct unchecked_install_policy {
	void install() 
	{ 
	}
};

struct checked_install_policy {
	void install() 
	{
		if (std::uncaught_exceptions() > 0) {
			std::terminate(); // sorry
		}
	}
};

//////////////////////////////////////////////
// Cleanup policies
//////////////////////////////////////////////

struct exit_policy {
	typedef unchecked_install_policy installer;

	template<typename F> void cleanup(F& f) 
	{
		f();
	}
};

struct failure_policy {
	typedef checked_install_policy installer;

	template<typename F> void cleanup(F& f) 
	{
		// Only cleanup if we're exiting from an exception.
		if (std::uncaught_exceptions() > 0) {
			f();
		}
	}
};

struct success_policy {
	typedef checked_install_policy installer;

	template<typename F> void cleanup(F& f) 
	{
		// Only cleanup if we're NOT exiting from an exception.
		if (std::uncaught_exceptions() == 0) {
			f();
		}
	}
};

//////////////////////////////////////////////
// Syntactical sugar
//////////////////////////////////////////////

template<typename CleanupPolicy>
struct scope_guard_builder { };

template<typename F, typename CleanupPolicy>
scope_guard<F,CleanupPolicy> operator+(scope_guard_builder<CleanupPolicy> /*builder*/, F&& f)
{
	return std::forward<F>(f);
}

// typical preprocessor utility stuff.
#define PASTE_TOKENS2(a,b) a ## b
#define PASTE_TOKENS(a,b) PASTE_TOKENS2(a,b)

//
//  The  scope  statement.
//
//  Cleanup policies:
//    exit    : cleanup will happen come hell or high water.
//    failure : cleanup will happen only if the scope is exited by an exception.
//    success : cleanup will happen only if the scope is exited normally.
//
//  ONE CANNOT USE EITHER OF scope (failure) OR scope (success) WITHIN A DESTRUCTOR! 
//  This includes functions called from destructors.
//  scope (exit), on the other hand, is always ok. 
//

#define scope(condition) \
	auto PASTE_TOKENS(_scopeGuard, __LINE__) = scope_guard_builder<condition##_policy>() + [&]

#endif

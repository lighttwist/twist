/// @file test_math.cpp
/// Implementation file for "test_distributions.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/test/math/test_distributions.hpp"

#include "twist/test/test_globals.hpp"

#include "twist/feedback_providers.hpp"
#include "twist/math/EnumDistrDiscrete.hpp" 

using namespace twist;
using namespace twist::math;

namespace twist::test::math {

enum class Boy : short { bob, rob, dob, blob };

auto is_valid(Boy b) { return Boy::bob <= b && b <= Boy::blob; }

TEST_CASE("Test for class twist::math::EnumDistrDiscrete", "[twist::math]")
{
	auto states = twist::math::EnumDistrDiscrete<Boy>::EnumStateList{};
	states.insert_state(Boy::bob, 0.24); // 0
	states.insert_state(Boy::dob, 0.57); // 2
	states.insert_state(Boy::rob, 0.19); // 1
	states.adjust_last_state_prob();

	REQUIRE(states.get_state_at(1) == std::pair{Boy::rob, 0.19});
	REQUIRE(states.get_state_pos(Boy::dob) == 2);

	auto distr = EnumDistrDiscrete<Boy>{std::move(states)};
	REQUIRE(distr.inv_cdf_enum_value(0.23) == Boy::bob);
	REQUIRE(distr.inv_cdf_enum_value(0.42) == Boy::rob);
	REQUIRE(distr.inv_cdf_enum_value(0.99) == Boy::dob);

	REQUIRE(distr.get_state_idx(Boy::bob) == 0);
	REQUIRE(distr.get_state_idx(Boy::rob) == 1);
	REQUIRE(distr.get_state_idx(Boy::dob) == 2);
	
	REQUIRE(distr.get_state_prob_for_value(Boy::bob) == 0.24);
	REQUIRE(distr.get_state_enum_value_at(1) == Boy::rob);

	REQUIRE(distr.is_state(Boy::rob));
	REQUIRE(!distr.is_state(Boy::blob));
}

}

//  Inline implementation file for "test_index_range.hpp"
//
//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,  
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/test/math/test_space_grid.hpp"

#include "twist/math/space_grid.hpp"
#include "twist/test/test_globals.hpp"

#pragma warning(disable: 4100)

using namespace twist::math;

namespace twist::test::math {

TEST_CASE("Test 1 for twist::math::SquareSpaceGrid", "[twist::math]")
{
    {
        auto grid = SquareSpaceGrid<int>{0, 0, 10, 3, 4};
        auto rect = Rectangle<int>{15, 5, 35, 25};
        auto subgrid_info = get_info_of_subgrid_containing_rect(grid, rect);
        REQUIRE(subgrid_info);
        CHECK(subgrid_info->begin_row() == 0);
        CHECK(subgrid_info->end_row() == 2);
        CHECK(subgrid_info->begin_column() == 1);
        CHECK(subgrid_info->end_column() == 3);
    }
    {
        // Rectangle intersects but is not inside the grid rectangle
        auto grid = SquareSpaceGrid<int>{0, 0, 10, 30, 20};
        auto rect = Rectangle<int>{15, 25, 255, 145};
        auto subgrid_info = get_info_of_subgrid_containing_rect(grid, rect);
        REQUIRE(subgrid_info);
        CHECK(subgrid_info->begin_row() == 15);
        CHECK(subgrid_info->end_row() == 27);
        CHECK(subgrid_info->begin_column() == 1);
        CHECK(subgrid_info->end_column() == 19);
    }
    {
        const auto left = 3.14159265;
        const auto bottom = 7.62846596;
        const auto cell_size = 9.87654321;
        auto grid = SquareSpaceGrid<double>{left, bottom, cell_size, 50, 100};
        auto subgrid = SquareSpaceGrid<double>{left + cell_size * 42, bottom + cell_size * 23, cell_size, 17, 38};
        auto subgrid_info = is_subgrid_tol(subgrid, grid, 1e-9);
        REQUIRE(subgrid_info);
        CHECK(subgrid_info->begin_row() == 10);
        CHECK(subgrid_info->end_row() == 26);
        CHECK(subgrid_info->begin_column() == 42);
        CHECK(subgrid_info->end_column() == 79);     
    }
    {
        auto grid = SquareSpaceGrid<int>{0, 0, 10, 3, 4};
        auto rect = Rectangle<int>{15, 5, 35, 25};
        auto subgrid_info = get_info_of_subgrid_contained_in_rect(grid, rect);
        REQUIRE(subgrid_info);
        CHECK(subgrid_info->begin_row() == 1);
        CHECK(subgrid_info->end_row() == 1);
        CHECK(subgrid_info->begin_column() == 2);
        CHECK(subgrid_info->end_column() == 2);
    }
    {
        // Rectangle intersects but is not inside the grid rectangle
        auto grid = SquareSpaceGrid<int>{0, 0, 10, 30, 20};
        auto rect = Rectangle<int>{15, 25, 255, 145};
        auto subgrid_info = get_info_of_subgrid_contained_in_rect(grid, rect);
        REQUIRE(subgrid_info);
        CHECK(subgrid_info->begin_row() == 16);
        CHECK(subgrid_info->end_row() == 26);
        CHECK(subgrid_info->begin_column() == 2);
        CHECK(subgrid_info->end_column() == 19);
    }
}

TEST_CASE("Test get_info_of_subgrid_with_cell_centres_in_rect()", "[twist::math]")
{
    auto equal = [](const auto& sg, const std::initializer_list<int>& range) {
        const auto sg_range = {sg.begin_row(), sg.end_row(), sg.begin_column(), sg.end_column()};
        return rg::equal(sg_range, range);
    };
    {
        auto coarse_grid = SquareSpaceGrid<double>{0, 0, 9, 3, 3};
        auto fine_grid = SquareSpaceGrid<double>{7, -4, 3, 8, 9};

        auto sg = get_info_of_subgrid_with_cell_centres_in_rect(fine_grid, perimeter_rect(coarse_grid));
        REQUIRE(sg);
        CHECK(equal(*sg, {0, 6, 0, 6}));        
    }
    {
        auto coarse_grid = SquareSpaceGrid<float>{3, 3, 8, 2, 1};
        auto fine_grid = SquareSpaceGrid<float>{0, 0, 4, 6, 4};

        auto sg = get_info_of_subgrid_with_cell_centres_in_rect(fine_grid, perimeter_rect(coarse_grid));
        REQUIRE(sg);
        CHECK(equal(*sg, {1, 4, 1, 2}));      
    }    
    {
        auto coarse_grid = SquareSpaceGrid<float>{-1, -1, 8, 4, 4};
        auto fine_grid = SquareSpaceGrid<float>{0, 0, 4, 6, 4};

        auto sg = get_info_of_subgrid_with_cell_centres_in_rect(fine_grid, perimeter_rect(coarse_grid));
        REQUIRE(sg);
        CHECK(equal(*sg, {0, 5, 0, 3}));
    } 
}

auto test_space_grid1(MessageFeedbackProv& feedback_prov) -> void
{
}

}

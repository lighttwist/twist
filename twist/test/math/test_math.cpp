/// @file test_math.cpp
/// Implementation file for "test_math.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/test/math/test_math.hpp"

#include "twist/test/test_globals.hpp"

#include "twist/feedback_providers.hpp"
#include "twist/math/numeric_utils.hpp"
#include "twist/math/phys_units.hpp"
#include "twist/math/random_utils.hpp"
#include "twist/math/space_grid.hpp"
#include "twist/math/trigonometry.hpp"

#include <numbers>

#pragma warning(disable:4189)

using namespace twist;
using namespace twist::math;

namespace twist::test::math {

TEST_CASE("Test for functions in \"math/random_utils.hpp\"", "[twist::math]") 
{
	const auto sample_size = 10'000u;
	{
		const auto sample = generate_uniform_int_sample(sample_size, 0l, 60l);
		static_assert(std::is_same_v<decltype(sample)::value_type, long>);

		REQUIRE(sample.size() == sample_size);
		for (auto x : sample) {
			REQUIRE((0l <= x && x <= 60l));
		}
	}
	{
		const auto sample = generate_uniform_real_sample(sample_size, 0.0, 60.0);
		static_assert(std::is_same_v<decltype(sample)::value_type, double>);

		REQUIRE(sample.size() == sample_size);
		for (auto x : sample) {
			REQUIRE((0.0 <= x && x < 60.0));
		}
	}
}

TEST_CASE("Test for functions in \"math/phys_units.hpp\"", "[twist::math]") 
{
    using std::numbers::pi;

    {   // Wind
        static const auto tol = std::numeric_limits<double>::epsilon();
        auto comp1 = [](double deg, double rad) {
            return equal_tol(wind_rose_degrees_to_reversed_unit_circle_radians(deg), rad, tol);
        };

        REQUIRE(comp1(0,   3*pi/2));
        REQUIRE(comp1(360, 3*pi/2));
        REQUIRE(comp1(90,  pi));
        REQUIRE(comp1(180, pi/2));
        REQUIRE(comp1(270, 0));
        REQUIRE(comp1(135, 3*pi/4));

        auto comp2 = [](double speed, double deg, double x_comp, double y_comp) {
            auto [calc_x_comp, calc_y_comp] = wind_rose_vector_with_degrees_to_cartesian(speed, deg);
            return equal_tol(calc_x_comp, x_comp, tol) and equal_tol(calc_y_comp, y_comp, tol);
        };

        REQUIRE(comp2(1,   0,           0, -1));
        REQUIRE(comp2(1, 360,           0, -1));
        REQUIRE(comp2(1,  90,          -1,  0));
        REQUIRE(comp2(1, 180,           0,  1));
        REQUIRE(comp2(1, 270,           1,  0));
        REQUIRE(comp2(1,  45,  -1/sqrt(2), -1/sqrt(2)));
        REQUIRE(comp2(1, 135,  -1/sqrt(2),  1/sqrt(2)));
        REQUIRE(comp2(1, 225,   1/sqrt(2),  1/sqrt(2)));
        REQUIRE(comp2(1, 315,   1/sqrt(2), -1/sqrt(2)));

        // Right angle triangle with sides 3, 4, 5
        auto deg = 90 + radians_to_degrees(atan2(3, 4));
        REQUIRE(comp2(5, deg, -4, 3));
    }
}

void test_math1(twist::MessageFeedbackProv& feedback_prov)
{
	double x = 0.9232648324;
	std::string s;
	auto y = trunc_decimals(x, 4);
	auto z = round_decimals(x, 4);
	feedback_prov.set_prog_msg(to_str(y));
}

}

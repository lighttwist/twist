/// @file test_matrix.cpp
/// Implementation file for "test_matrix.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/test/math/test_matrix.hpp"

#include "twist/test/test_globals.hpp"

#include "twist/feedback_providers.hpp"
#include "twist/math/FlatMatrixStack.hpp" 
#include "twist/math/FlatMatrix.hpp" 
#include "twist/math/matrix_utils.hpp" 
#include "twist/math/ColumnwiseMatrix.hpp" 
#include "twist/math/RowwiseMatrix.hpp" 

using namespace twist;
using namespace twist::math;

namespace twist::test::math {

TEST_CASE("Test for class twist::math::FlatMatrix", "[twist::math]")
{
	static const auto src_values = {10, 10, 10, 10, 10,
	                                10, 20, 20, 20, 20,
									10, 30, 30, 30, 30, 
									10, 10, 10, 10, 10};

	auto orig_src = FlatMatrix<int>{4, 5};
	REQUIRE(dims(orig_src) == std::tuple{4, 5});

	std::copy(src_values.begin(), src_values.end(), orig_src.begin());

	const auto orig_dest = FlatMatrix<int>{6, 7, 100};

	{
		static const auto expected_values = { 10,  10,  10,  10,  10, 100, 100,
											  10,  20,  20,  20,  20, 100, 100, 
											  10,  30,  30,  30,  30, 100, 100, 
			                                  10,  10,  10,  10,  10, 100, 100, 
											 100, 100, 100, 100, 100, 100, 100, 
											 100, 100, 100, 100, 100, 100, 100};
		auto dest = orig_dest.clone();
		copy_into(orig_src, dest);
		REQUIRE(equal_same_size(dest.view_matrix(), expected_values));
	}
	{
		static const auto expected_values = { 20,  20,  20,  20, 100, 100, 100,
											  30,  30,  30,  30, 100, 100, 100, 
											 100, 100, 100, 100, 100, 100, 100, 
			                                 100, 100, 100, 100, 100, 100, 100, 
											 100, 100, 100, 100, 100, 100, 100, 
											 100, 100, 100, 100, 100, 100, 100};
		auto dest = orig_dest.clone();
		REQUIRE(dest == orig_dest);

		copy_into(orig_src, dest, SubgridInfo{1, 2, 1, 4});
		REQUIRE(equal_same_size(dest.view_matrix(), expected_values));
	}
	{
		static const auto expected_values = {100, 100, 100, 100, 100, 100, 100,
											 100, 100, 100, 100, 100, 100, 100, 
											 100, 100, 100, 100, 100, 100, 100, 
			                                 100, 100, 100, 100,  20,  20,  20, 
											 100, 100, 100, 100,  30,  30,  30, 
											 100, 100, 100, 100,  10,  10,  10};
		auto dest = orig_dest.clone();
		copy_into(orig_src, dest, SubgridInfo{1, 3, 1, 3}, 3, 4);
		REQUIRE(equal_same_size(dest.view_matrix(), expected_values));
	}
	{
		static const auto expected_values = { 100, 100, 100, 100, 100,
		                                       10,  10,  10,  10,  10, 
											   10,  20,  20,  20,  20, 
											   10,  30,  30,  30,  30,  
			                                   10,  10,  10,  10,  10,  
											  100, 100, 100, 100, 100};
		auto dest = FlatMatrix<int>{6, 5, 100};
		copy_into(orig_src, dest, std::nullopt, 1, 0);
		REQUIRE(equal_same_size(dest.view_matrix(), expected_values));
	}
	{
		static const auto expected_values = { 100, 100, 100, 100, 100,
			                                  100, 100, 100, 100, 100,
											   10,  20,  20,  20,  20, 
											   10,  30,  30,  30,  30,  
			                                   10,  10,  10,  10,  10,  
											  100, 100, 100, 100, 100};
		auto dest = FlatMatrix<int>{6, 5, 100};
		copy_into(orig_src, dest, SubgridInfo{1, 3, 0, 4}, 2, 0);
		REQUIRE(equal_same_size(dest.view_matrix(), expected_values));
	}

	//+TODO: Test resize(), swap()
}

TEST_CASE("Test for class twist::math::FlatMatrixStack", "[twist::math]")
{   
	//                              <-cols(3)->                    
	static const auto orig_values = { 9, 14, 26,   // ^            ^
	                                 55, 73,  7,   // | rows(4)    |
									 91,  4,  1,   // |            |
									 46, 69, 82,   // v            |
									               //              | matrices(2)
									 24, 90,  3,   //              |
									  5, 33, 87,   //              |
									 64, 25,  6,   //              |
									 81,  2, 47 }; //              v

	auto orig = FlatMatrixStack<int>{4, 3, 2};
	REQUIRE(dimensions(orig) == std::tuple{4, 3, 2});

	std::copy(orig_values.begin(), orig_values.end(), orig.begin());

	REQUIRE(orig.at(0, 2, 1) ==  3);
	REQUIRE(orig.at(2, 0, 0) == 91);
	REQUIRE(orig.at(1, 1, 0) == 73);
	REQUIRE(orig.at(3, 0, 1) == 81);

	auto mat2 = orig.clone();
	REQUIRE(dimensions(mat2) == std::tuple{4, 3, 2});
	REQUIRE(equal_same_size(orig.view_stack(), mat2));
	REQUIRE(orig == mat2);

	for_each_cell(mat2, [](int& val, auto r, auto c, auto m) {
		val += static_cast<int>(r + c + m);
	});

	static const auto expected_values = { 9, 15, 28,   
										 56, 75, 10,    
										 93,  7,  5,   
										 49, 73, 87,  

										 25, 92,  6,   
										  7, 36, 91,   
										 67, 29, 11,   
										 85,  7, 53 };  

	auto it = std::mismatch(mat2.begin(), mat2.end(), expected_values.begin());


	REQUIRE(equal_same_size(mat2.view_stack(), expected_values));

	auto mat3_values = std::vector(expected_values);
	auto mat3 = FlatMatrixStack{4, 3, 2, move(mat3_values)};
	REQUIRE(mat2 == mat3);

	//+TODO: Test resize(), swap()
}

template<class Mat>
auto test_rowwise_or_columnwise_matrix(Mat& m) -> void
{
	m.insert_column(0, {2, 20, 200, 2000});
	m.insert_column(0, {1, 10, 100, 1000});
	m.insert_column(2, {3, 30, 300, 3000});
	{
		static const auto expected = {   1,    2,    3,
		                                10,   20,   30, 
		                               100,  200,  300, 
		                              1000, 2000, 3000}; 
		REQUIRE(m.nof_rows() == 4);
		REQUIRE(m.nof_columns() == 3);
		REQUIRE(equal_to_range(m, expected));
	}

	m.insert_column(1, {9, 90, 900, 9000});
	{
		static const auto expected = {   1,    9,    2,    3,
									    10,   90,   20,   30, 
									   100,  900,  200,  300, 
									  1000, 9000, 2000, 3000}; 
		REQUIRE(m.nof_rows() == 4);
		REQUIRE(m.nof_columns() == 4);
		REQUIRE(equal_to_range(m, expected));
	}
	
	m.remove_column(3);
	{
		static const auto expected = {   1,    9,    2,
		                                10,   90,   20, 
		                               100,  900,  200, 
		                              1000, 9000, 2000}; 
		REQUIRE(m.nof_rows() == 4);
		REQUIRE(m.nof_columns() == 3);
		REQUIRE(equal_to_range(m, expected));
	}
	
	const auto col_values = m.extract_column(1);
	{
		static const auto expected_col = {9, 90, 900, 9000};
		REQUIRE(equal_same_size(col_values, expected_col));

		static const auto expected = {   1,    2,
		                                10,   20, 
		                               100,  200, 
		                              1000, 2000}; 
		REQUIRE(m.nof_rows() == 4);
		REQUIRE(m.nof_columns() == 2);
		REQUIRE(equal_to_range(m, expected));
	}

	m.insert_column(2, col_values);
	{
		static const auto expected = {   1,    2,    9,
		                                10,   20,   90,
		                               100,  200,  900,
		                              1000, 2000, 9000}; 
		REQUIRE(m.nof_rows() == 4);
		REQUIRE(m.nof_columns() == 3);
		REQUIRE(equal_to_range(m, expected));
	}

	m.remove_row(1);
	{
		static const auto expected = {   1,    2,    9,
		                               100,  200,  900,
		                              1000, 2000, 9000}; 
		REQUIRE(m.nof_rows() == 3);
		REQUIRE(m.nof_columns() == 3);
		REQUIRE(equal_to_range(m, expected));
	}

	const auto nof_removed_rows = remove_rows_if(m, [](const auto& row_values) { 
		return rg::all_of(row_values, [](auto x) { return 100 <= x && x < 1000; }); 
	});
	{
		static const auto expected = {   1,    2,    9,
		                              1000, 2000, 9000}; 
		REQUIRE(nof_removed_rows == 1);
		REQUIRE(m.nof_rows() == 2);
		REQUIRE(m.nof_columns() == 3);
		REQUIRE(equal_to_range(m, expected));
	}

	m.remove_row(1);
	m.remove_row(0);
	REQUIRE(m.nof_rows() == 0);
	REQUIRE(m.nof_columns() == 0);

	m.add_row({2, 4, 6, 8});
	m.add_row({1, 3, 5, 7});
	REQUIRE(m.nof_rows() == 2);
	REQUIRE(m.nof_columns() == 4);

	m.remove_column(3);
	m.remove_column(2);
	m.remove_column(1);
	m.remove_column(0);
	REQUIRE(m.nof_rows() == 0);
	REQUIRE(m.nof_columns() == 0);

	m.insert_column(0, {1, 2, 3});
	m.insert_column(0, {4, 5, 6});
	{
		static const auto expected = {4, 1,
		                              5, 2,
									  6, 3}; 
		REQUIRE(m.nof_rows() == 3);
		REQUIRE(m.nof_columns() == 2);
		REQUIRE(equal_to_range(m, expected));
	}

	m.remove_column(0);
	m.remove_column(0);

	m.add_row({   1,    2,    3});
	m.add_row({  10,   20,   30});
	m.add_row({ 100,  200,  300});
	m.add_row({1000, 2000, 3000});
	{
		static const auto expected = {   1,    2,    3,
		                                10,   20,   30, 
		                               100,  200,  300, 
		                              1000, 2000, 3000}; 
		REQUIRE(m.nof_rows() == 4);
		REQUIRE(m.nof_columns() == 3);
		REQUIRE(equal_to_range(m, expected));
	}
}

TEST_CASE("Test for class twist::math::RowwiseMatrix", "[twist::math]")
{
	auto m = RowwiseMatrix<int>{};
	test_rowwise_or_columnwise_matrix(m);
}

TEST_CASE("Test for class twist::math::ColumnwiseMatrix", "[twist::math]")
{
	auto m = ColumnwiseMatrix<int>{};
	test_rowwise_or_columnwise_matrix(m);
}

}

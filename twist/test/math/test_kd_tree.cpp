/// @file test_kd_tree.cpp
/// Implementation file for "test_kd_tree.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/test/math/test_kd_tree.hpp"

#include "twist/array_utils.hpp"
#include "twist/db/CsvFileReader.hpp"
#include "twist/math/KdTree.hpp"
#include "twist/test/test_globals.hpp"

using namespace twist::math;

namespace twist::test::math {

template<int k, class Coord>
auto test_kd_tree_impl(Ssize nof_points, const std::vector<ClosedInterval<Coord>>& bounds_per_dim)
{
	auto point_cloud = generate_kd_point_cloud<k, Coord>(nof_points + 1, bounds_per_dim);	
	auto test_pt = extract_at(point_cloud, nof_points);

	auto tree = create_kd_tree(point_cloud);
	tree.build_tree();

	const auto [nearest_pt, nearest_pt_cloud_idx, dist_to_nearest_pt_squared] = 
			get_nearest_point_linear(point_cloud, test_pt, true/*ensure_unique*/);

	const auto [tree_best_node, tree_best_dist_squared] = tree.find_nearest(test_pt);

	REQUIRE(nearest_pt == tree_best_node.point());
	REQUIRE(nearest_pt_cloud_idx == tree_best_node.data());
	REQUIRE(dist_to_nearest_pt_squared == tree_best_dist_squared);
}

TEST_CASE("Test twist::math::KdTree", "[twist::math]")
{
	test_kd_tree_impl<2, double>(100000, {ClosedInterval<double>{-1, 5}, 
									      ClosedInterval<double>{-4, 2}});	

	test_kd_tree_impl<3, __int64>(100000, {ClosedInterval<__int64>{-100, 500}, 
									       ClosedInterval<__int64>{-400, 200},
									       ClosedInterval<__int64>{-500, 300}});	

	test_kd_tree_impl<3, float>(100000, {ClosedInterval<float>{-1, 5}, 
									     ClosedInterval<float>{-4, 2},
									     ClosedInterval<float>{-5, 3}});	
}

}

//  Inline implementation file for "test_index_range.hpp"
//
//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,  
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/test/math/test_dag.hpp"

#include "twist/math/Dag.hpp"
#include "twist/test/test_globals.hpp"

#pragma warning(disable: 4702)

namespace twist::test::math {

using Dag = twist::math::Dag<std::string>;
using NodeMap = std::map<std::string, Dag::NodePtr>;

auto make_node_name(int i) -> std::string
{ 
    return "V" + std::to_string(i); 
}

[[nodiscard]] auto get_node(int i, const NodeMap& node_map) -> Dag::NodePtr
{
    return node_map.at(make_node_name(i));
}

auto create_dag(int nof_nodes, const std::map<int, std::set<int>>& arcs) -> std::tuple<Dag, NodeMap>
{
    auto dag = Dag{[](const auto& s) { return ansi_to_string(s); }};

    auto node_map = NodeMap{};
    for (auto i : IndexRange{nof_nodes}) {
        auto node_name = make_node_name(i + 1);
        auto node = dag.add_node(node_name);
        node_map.emplace(std::move(node_name), node);
    }

    for (const auto& [parent_id, child_ids] : arcs) {
        for (auto child_id : child_ids) {
            dag.add_arc(node_map.at(make_node_name(parent_id)), 
                        node_map.at(make_node_name(child_id)));
        }
    }

    return {std::move(dag), move(node_map)};
}

[[nodiscard]] auto is_permutation(const twist::math::DagNodeRange auto& nodes, std::vector<std::string> names) -> bool
{
    const auto node_names = vw::transform(nodes, [](auto node) { return node->data(); });
    return rg::is_permutation(node_names, names);
}

TEST_CASE("Test 1 for twist::math::Dag", "[twist::math]")
{
    const auto [dag, node_map] = create_dag(14, {{1, {2, 3, 5}},
                                                 {2, {3, 4, 11}},
                                                 {3, {4, 7}},
                                                 {4, {6}},
                                                 {5, {3, 6}},
                                                 {6, {8}},
                                                 {7, {6, 5}},
                                                 {8, {9, 10}}, 
                                                 {9, {4}},
                                                 {10, {6}}, 
                                                 {11, {12, 13, 14}},
                                                 {12, {13, 14}},
                                                 {13, {14}},
                                                 {14, {2}}});
 
    auto child_of = [&node_map](auto i, auto j) {
        return is_child_of(get_node(j, node_map), get_node(i, node_map));
    };

    REQUIRE( child_of( 3,  1));
    REQUIRE(!child_of( 3,  4));
    REQUIRE( child_of(14, 12));
    REQUIRE(!child_of( 5,  2));

    const auto [sccs, topol_nodes] = find_sccs_or_topol_order(dag.nodes());
    REQUIRE(ssize(sccs) == 3);

    if (ssize(sccs) == 3) {
        REQUIRE(is_permutation(sccs[0], {"V4", "V6", "V8", "V9", "V10"}));
        REQUIRE(is_permutation(sccs[1], {"V3", "V5", "V7"}));
        REQUIRE(is_permutation(sccs[2], {"V2", "V11", "V12", "V13", "V14"}));
    }
}

TEST_CASE("Test 2 for twist::math::Dag", "[twist::math]")
{
    const auto [dag, _] = create_dag(14, {{1, {2, 3, 4, 12, 14}},
                                          {2, {4, 5, 6}},
                                          {3, {5, 8}},
                                          {4, {7, 13}},
                                          {5, {8, 9, 11}},
                                          {6, {8, 12}},
                                          {7, {10, 11}},
                                          {8, {11, 13, 14}},
                                          {9, {12, 13, 14}},
                                          {10, {11, 14}},
                                          {11, {14}}});    
    
    const auto [sccs, topol_nodes] = find_sccs_or_topol_order(dag.nodes());
    REQUIRE(sccs.empty());
    REQUIRE(ssize(topol_nodes) == 14);
    
    REQUIRE(are_topol_ordered(topol_nodes));
} 

TEST_CASE("Test 3 for twist::math::Dag", "[twist::math]")
{
    const auto [dag, _] = create_dag(10, {{1, {2, 6, 10}},
                                          {2, {7}},
                                          {3, {4, 5}},
                                          {5, {4, 8}},
                                          {6, {10}}});

    const auto conn_graphs = get_connected_graphs(dag.nodes());
    REQUIRE(ssize(conn_graphs) == 3);
    if (ssize(conn_graphs) == 3) {
        REQUIRE(is_permutation(conn_graphs[0], {"V1", "V2", "V6", "V7", "V10"}));
        REQUIRE(is_permutation(conn_graphs[1], {"V3", "V4", "V5", "V8"}));
        REQUIRE(is_permutation(conn_graphs[2], {"V9"}));
    }
}

}

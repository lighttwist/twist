/// @file LibraryTestDataManagerBase.hpp
/// LibraryTestDataManagerBase abstract class template

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_LIBRARY_TEST_DATA_MANAGER_BASE_HPP
#define TWIST_LIBRARY_TEST_DATA_MANAGER_BASE_HPP

#include "twist/Singleton.hpp"

namespace twist::test {

//+TODO: Add comments

/*! Abstract base for singleton classes which manage the data used by the test code inside a specific library.
    The library-specific data files and directories are inside one root directory.
	There is also another directory which contains data shared among the test code of all libraries in the codebase.
    \tparam Derived  The derived data manager class type
 */ 
template<class Derived>
class LibraryTestDataManagerBase : public Singleton<Derived> {
public:
	class DirectoryDeletionToken {
	public:
		~DirectoryDeletionToken();

		[[nodiscard]] auto path() const -> fs::path;

		auto cancel_deletion() -> void;

	private:
		explicit DirectoryDeletionToken(fs::path path);

		const fs::path path_;
		bool cancelled_{false};

		friend class LibraryTestDataManagerBase;

		TWIST_NO_COPY_NO_MOVE(DirectoryDeletionToken)
		TWIST_CHECK_INVARIANT_DECL
	};

	virtual ~LibraryTestDataManagerBase() = 0;

	//! Path to the root directory of the library test data.
	[[nodiscard]] auto library_data_root_dir_path() const -> fs::path;

	//! Path to the root directory of the shared test data.
	[[nodiscard]] auto shared_data_root_dir_path() const -> fs::path;

	[[nodiscard]] auto temp_test_output_root_dir_path() const -> fs::path;

	[[nodiscard]] auto prepare_empty_temp_test_output_root_dir() -> DirectoryDeletionToken;

	auto delete_temp_test_output_root_dir() -> bool;

protected:
	/*! Constructor.
	    \param[in] lib_data_root_dir_path  Path to the root directory of the library test data; if the directory does 
		                                   not exist, an exception is thrown
	    \param[in] shared_data_root_dir_path  Path to the root directory of the shared test data; if the directory does 
		                                      not exist, an exception is thrown
 	 */
	LibraryTestDataManagerBase(fs::path lib_data_root_dir_path, fs::path shared_data_root_dir_path);

private:
	static const wchar_t* temp_test_output_dir_name;

	const fs::path lib_data_root_dir_path_;
	const fs::path shared_data_root_dir_path_;

	TWIST_CHECK_INVARIANT_DECL
};

}

#include "LibraryTestDataManagerBase.ipp"

#endif

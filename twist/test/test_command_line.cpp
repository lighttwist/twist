/// @file test_command_line.cpp
/// Implementation file for "test_command_line.hpp"

//  Copyright (c) 2007-2022, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/CommandLine.hpp"
#include "twist/std_vector_utils.hpp"
#include "twist/test/test_globals.hpp"

namespace twist::test {

TEST_CASE("Test for class twist::CommandLine", "[twist]") 
{
	using namespace std::literals;

	auto argc = 8;
	char* argv[] = {(char*)"C:\\Program Files\\Koko\\koko.exe", 
	                (char*)"-h", 
					(char*)"1234", 
					(char*)"-bob", 
					(char*)"13", 
					(char*)"go", 
					(char*)"-5.6", 
					(char*)"-pi"};

	auto cmdln = CommandLine{argc, argv};

	assert(cmdln.has_option("-h"));
	assert(cmdln.has_option("-pi"));
	assert(cmdln.has_option("13"));
	assert(!cmdln.has_option("-po"));
	assert(cmdln.get_option_value("-h") == "1234");
	assert(cmdln.get_option_value("-bob") == "13");
	assert(cmdln.get_option_values("-bob", 3) == create_vector("13"s, "go", "-5.6"));
}

}


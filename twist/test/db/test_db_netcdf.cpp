/// @file "test_db_netcdf.cpp"
/// Implementation file for "test_db_netcdf.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/test/db/test_db_netcdf.hpp"

#include "twist/db/netcdf/DimensionInfo.hpp"
#include "twist/db/netcdf/DimensionValueRange.hpp"
#include "twist/db/netcdf/DualDimensionValueRanges.hpp"
#include "twist/db/netcdf/NetcdfFile.hpp"
#include "twist/db/netcdf/HyperslabAndDimData.hpp"
#include "twist/db/netcdf/HyperslabAndDimDataReader.hpp"
#include "twist/db/netcdf/HyperslabAndDimParams.hpp"
#include "twist/db/netcdf/HyperslabAndDualDimDataReader.hpp"
#include "twist/db/netcdf/HyperslabAndDualDimParams.hpp"
#include "twist/db/netcdf/VariableInfo.hpp"
#include "twist/db/netcdf/netcdf_text_file_utils.hpp"
#include "twist/db/netcdf/netcdf_utils.hpp"
#include "twist/gis/gis_geometry.hpp"
#include "twist/math/ClosedInterval.hpp"
#include "twist/test/test_globals.hpp"

#pragma warning(disable: 4189)

using namespace twist::db::netcdf;
using namespace twist::math;

static const auto black_sat_grd_weather_dir_path =
		LR"(E:\frost\gridded_weather\gridded_weather_black_sat\20090206 193417\)";

static const auto tmpr_file_name = L"IDZ00026_VIC_ADFD_20090206_193417_T_SFC.nc";

namespace twist::test::db {

TEST_CASE("Test \"twist::db::NetcdfFile\" class", "[twist::db]")
{
	const auto dirpath = TwistTestDataManager::get().twist_db_test_data_root_dir() / L"netcdf";
	const auto in_path = dirpath / L"temperature_1h_2060.nc";
	
	auto in_temp_file = std::make_shared<NetcdfFile>(NetcdfFile::open, in_path, true/*read_only*/);
	const auto tas_var_data = in_temp_file->read_var_data<float>("tas");

	using Catch::Approx;

	REQUIRE(tas_var_data[0]	 == Approx(299.955170));
	REQUIRE(tas_var_data[1]  == Approx(298.247528));
	REQUIRE(tas_var_data[2]  == Approx(296.441040));
	REQUIRE(tas_var_data[3]  == Approx(295.945953));
	REQUIRE(tas_var_data[4]  == Approx(296.965424));
	REQUIRE(tas_var_data[5]  == Approx(298.765778));
	REQUIRE(tas_var_data[6]  == Approx(299.639679));
	REQUIRE(tas_var_data[7]  == Approx(298.027771));
	REQUIRE(tas_var_data[8]  == Approx(298.226532));
	REQUIRE(tas_var_data[9]  == Approx(300.207123));
	REQUIRE(tas_var_data[10] == Approx(299.292084));

}


void test_netcdf1()
{
	const auto in_path = fs::path{ black_sat_grd_weather_dir_path } / tmpr_file_name;
	const auto out_path = LR"(F:\junk\netcdf\out_file.nc)";

	using TemprHyslab = HyperslabAndDimData<float, float, float, int>;
	std::shared_ptr<TemprHyslab> tempr_hyslab;
	{
		auto in_file = std::make_shared<NetcdfFile>(NetcdfFile::open, in_path, true/*read_only*/);
		const auto latitude_data = in_file->read_dim_data<float>("latitude");
		const auto longitude_data = in_file->read_dim_data<float>("longitude");
		const auto time_data = in_file->read_dim_data<int>("time");
		const auto tempr_var_data = in_file->read_var_data<float>("T_SFC");

		auto lat_lo = latitude_data->data().front();
		auto lat_hi = latitude_data->data().back();
		auto long_lo = longitude_data->data().front();
		auto long_hi = longitude_data->data().back();
		auto time_lo = time_data->data().front();
		auto time_hi = time_data->data().back();

		using HyslabParams = HyperslabAndDimParams<float, float, int>;
		HyslabParams params{
				{in_file->get_dim_info("latitude"), {-37, -36}},
				{in_file->get_dim_info("longitude"), {146, 148}},
				{in_file->get_dim_info("time"), {1234000000, 1234200000}}};

		HyperslabAndDimDataReader<float, float, float, int> hyslab_reader{in_file, params};
		tempr_hyslab = hyslab_reader.read_hyperslab_with_dims("T_SFC");
	}
	{
		NetcdfFile out_file{ NetcdfFile::create, out_path, tempr_hyslab->dims_info(), 
				tempr_hyslab->all_vars_info() };


		out_file.write_hyperslab_with_dims(tempr_hyslab);
	}

	//auto lat_pt_count = nf.get_dim_length("latitude");
	//auto long_pt_count = nf.get_dim_length("longitude");
	//auto time_pt_count = nf.get_dim_length("time");

	//const auto& lat_data = nf.read_dim_var("latitude");
}


void test_netcdf2()
{
}


void test_narclim1()
{
	using namespace twist::gis;

	const GeoRectangle centralhigh_study_rect{ 144.9289, -38.2728, 146.7073, -36.8826 };

	const fs::path root_dir_path{ 
//			LR"(Z:\SEFS Research Data\Restricted\Bushfire\Spatial Data\worldclim\NARCliM)"};
			LR"(F:\frost\NARCliM)"};
	const auto df_ffdi_dir_path = LR"(ffdi\1990-2009\CSIRO-MK3.0\R1\d02)";

	const auto freq_dir_path = LR"(postprocessed\1990-2009\CSIRO-MK3.0\R1\d02)";

	auto in_temp_file = std::make_shared<NetcdfFile>( 
			NetcdfFile::open, root_dir_path / freq_dir_path / L"CCRC_NARCliM_01H_1990-1990_tas.nc", true/*read_only*/);
	const auto tas_var_data = in_temp_file->read_var_data<float>("tas");


	auto in_df_file = std::make_shared<NetcdfFile>( 
			NetcdfFile::open, root_dir_path / df_ffdi_dir_path / L"OEH_NARCliM_DAY_1990-2009_df.nc", true/*read_only*/);


	auto lat_var_info = in_df_file->get_var_info("lat");
	auto lon_var_info = in_df_file->get_var_info("lon");
	auto df_var_info = in_df_file->get_var_info("df");
	auto lonlat_dim_data = in_df_file->read_dual_dim_data<float, float>("lat", "lon");


	// "time" takes values from 351'396 to 526'692 signifying "hours since 1949-12-01 00:00:00"

	const auto min_lat = static_cast<float>(centralhigh_study_rect.bottom());
	const auto max_lat = static_cast<float>(centralhigh_study_rect.top());
	const auto min_lon = static_cast<float>(centralhigh_study_rect.left());
	const auto max_lon = static_cast<float>(centralhigh_study_rect.right());

	auto time_value_range = make_dim_val_range("time", 400'000.F, 400'050.F, *in_df_file);  

	auto ddim_ranges = make_dual_dim_ranges<float, float>( 
			"lat", "lon", {min_lat, max_lat}, {min_lon, max_lon}, *in_df_file);

	// 1 ddim, 1 sdim  (real data)

	HyperslabAndDualDimParams hyslab_params{ ddim_ranges, time_value_range };
	HyperslabAndDualDimDataReader<float, float, float, float> reader{ in_df_file, hyslab_params };

	auto ddim_pos_border = size_t{1};
	auto hyslab_data = reader.read_hyperslab_with_dims("df", &ddim_pos_border);

	auto out_df_file = std::make_shared<NetcdfFile>( 
			NetcdfFile::create, root_dir_path / df_ffdi_dir_path / L"dan_clipped_df.nc",
			hyslab_data->all_dims_info(), hyslab_data->all_vars_info());

	out_df_file->write_hyperslab_with_dual_dims(hyslab_data);

	export_2d_var_data_to_csv<float>(*in_df_file, "lat", "x", "y", 
			root_dir_path / df_ffdi_dir_path / L"lat_values.csv", 
			[](auto val) { return std::to_string(val); });


//	out_df_file->write_hyperslab_with_dual_dims

	//// 1 ddim, 2 sdims

	//HyperslabAndDualDimParams hyslab_params2{ddim_ranges, time_value_range, time_value_range};
	//HyperslabAndDualDimDataReader<float, float, float, float, float> reader2{in_df_file, hyslab_params2};
	//auto hyslab_data2 = reader2.read_hyperslab_with_dims("df");

	//// 1 ddim, no sdims

	//HyperslabAndDualDimParams hyslab_params3{ddim_ranges};
	//HyperslabAndDualDimDataReader<float, float, float> reader3{in_df_file, hyslab_params3};
	//auto hyslab_data3 = reader3.read_hyperslab_with_dims("df");

	//auto lat_var_info = in_df_file->get_var_info("lat");
	//auto lon_var_info = in_df_file->get_var_info("lon");
	//auto lonlat_dim_data = read_dual_dim_data<float, float>(*in_df_file, "lat", "lon");

	//auto lat_lon_pos_range = get_dual_dim_pos_ranges(
	//		*lonlat_dim_data, {std::move(lat_value_range), std::move(lon_value_range)});

	//auto lon_pos_range = get_dim2_pos_range(
	//		*lonlat_dim_data, 
	//		DimensionValueRange{"lon", 
	//				static_cast<float>(centralhigh_study_rect.left()), 
	//				static_cast<float>(centralhigh_study_rect.right()) });

	//export_2d_var_data_to_csv<float>(*in_df_file, "lat", "x", "y", 
	//		root_dir_path / df_ffdi_dir_path / L"lat_values.csv", 
	//		[](auto val) { return std::to_string(val); });

	//export_2d_var_data_to_csv<float>(*in_df_file, "lon", "x", "y", 
	//		root_dir_path / df_ffdi_dir_path / L"lon_values.csv", 
	//		[](auto val) { return std::to_string(val); });


	//const auto latitude_data = in_df_file->read_dim_data<float>("latitude");
	//const auto longitude_data = in_df_file->read_dim_data<float>("longitude");
	//const auto time_data = in_df_file->read_dim_data<int>("time");
	//const auto tempr_var_data = in_df_file->read_var_data<float>("T_SFC");

}

}

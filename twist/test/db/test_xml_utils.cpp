/// @file test_xml_utils.cpp
/// Implementation file for "test_xml_utils.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/test/db/test_xml_utils.hpp"

#include "twist/db/xml_utils.hpp"
#include "twist/math/numeric_utils.hpp"

#include "twist/test/test_globals.hpp"

using namespace twist;
using namespace twist::db;
using namespace twist::math;

namespace twist::test::db {

TEST_CASE("Test 1 for code in \"xml_utils.hpp/cpp\"", "[twist::db]") 
{
	const auto dirpath = TwistTestDataManager::get().twist_db_test_data_root_dir() / L"xml";
	const auto test_file_path = dirpath / L"test1.xml";

	if (!exists(dirpath)) {
		create_directories(dirpath);
	}
	if (exists(test_file_path)) {
		remove(test_file_path);
	}

	// Write a simple XML file
	{
		auto xdoc = XmlDoc::make_empty();
		auto xroot = xdoc->create_root_elem(L"in_out_test_file"); 
		xroot.set_attr(XmlNodeAttr{L"version", L"42"});
		xroot.set_attr(XmlNodeAttr{L"intversion", 42});

		auto dog_xnode = xdoc->create_elem_node(L"dog", xroot);
		auto dog_name_xnode = xdoc->create_elem_node(L"name", dog_xnode);
		dog_name_xnode.set_text(L"Momo Coo");
		dog_name_xnode.set_text(L"Coco Moo");

		dog_xnode = xdoc->create_elem_node(L"dog", xroot);
		dog_name_xnode = xdoc->create_elem_node(L"name", dog_xnode);
		dog_name_xnode.set_text(L"Scooby Doo");
		auto subdog_xnode = xdoc->create_elem_node(L"dog", dog_xnode);
		xdoc->create_elem_node(L"name", subdog_xnode).set_text(L"Lil Scoobie");
		xdoc->create_elem_node(L"age", subdog_xnode).set_text_from_int(3);

		xdoc->save(test_file_path);
	}

	// Read the file back in
	{
		auto xdoc = XmlDoc::read_from_file(test_file_path);
		auto xroot = xdoc->root_elem();
		REQUIRE(xroot.get_attr(L"version").get_int_value() == 42);
		REQUIRE(xroot.get_attr(L"intversion").get_int_value() == 42);

		auto num_top_dogs = xroot.count_children_with_tag(L"dog");
		REQUIRE(num_top_dogs == 2);

		auto all_dog_nodes = xroot.get_descendants_with_tag(L"dog");
		REQUIRE(all_dog_nodes.size() == 3);

		REQUIRE(!all_dog_nodes[0].has_text());
		REQUIRE(all_dog_nodes[0].get_unique_child_with_tag(L"name").has_text());
		
		REQUIRE(all_dog_nodes[0].get_unique_child_with_tag(L"name").text() == L"Coco Moo");
		REQUIRE(all_dog_nodes[1].get_unique_child_with_tag(L"name").text() == L"Scooby Doo");
		REQUIRE(all_dog_nodes[2].get_unique_child_with_tag(L"name").text() == L"Lil Scoobie");
	}
}


TEST_CASE("Test 2 for code in \"xml_utils.hpp/cpp\"", "[twist::db]") 
{
	struct FileInfo {
		std::wstring origin_name;
		std::wstring origin_descr;
		std::wstring filename;
		bool is_discrete; 
		double temperature;
		std::wstring udf_formula;
	};

	std::vector<FileInfo> rv_files_info{
		{ L"V1", L"vee one", L"V1.rv", false, 23.64, {} },
		{ L"V2", L"vee two", L"V2.rv", true, 19.05, {} } };

	std::vector<FileInfo> fn_files_info{
		{ L"F1", L"eff one", L"F1.fn", true, -10.17, L"2 + 2" },
		{ L"F2", L"eff two", L"F2.fn", false, 1.25, L"pi * 2.3" } };

	static const long kHeaderFileVersion = 3;
	static const wchar_t* kTopXmlNodeName = L"uninet_modular_sample_header";
	static const wchar_t* kTopXmlNodeVersionAttr = L"version";
	static const wchar_t* kXmlNodeNameNumSamples = L"number_of_samples";
	static const wchar_t* kXmlNodeNameVar = L"variable";
	static const wchar_t* kXmlNodeNameVarName = L"name";
	static const wchar_t* kXmlNodeNameVarDescr = L"description";
	static const wchar_t* kXmlNodeNameVarSamFilename = L"binary_sample_filename";
	static const wchar_t* kXmlNodeNameVarIsDiscr = L"is_discrete";
	static const wchar_t* kXmlNodeNameVarIsFunc = L"is_functional";
	static const wchar_t* kXmlNodeNameVarFuncFormula = L"function";
	static const wchar_t* kXmlNodeNameVarTemperature = L"temperature";

	const auto dirpath = TwistTestDataManager::get().twist_db_test_data_root_dir() / L"xml";
	const auto test_file_path = dirpath / L"test2.xml";

	// Write an XML file
	{
		std::wstringstream docRootXml;
		docRootXml << L"<?xml version=\"1.0\"?>"
				   << L"<"  << kTopXmlNodeName << L" " << kTopXmlNodeVersionAttr << L"=\"" 
				   << kHeaderFileVersion << L"\">" 
				   << L"</" << kTopXmlNodeName << L">";

		auto xdoc = XmlDoc::read_from_string(docRootXml.str());

		auto rootElem = xdoc->root_elem();

		xdoc->create_elem_node_with_int(kXmlNodeNameNumSamples, static_cast<long>(10000u), rootElem);

		auto writeFileInfo = [&xdoc](const auto& fileInfo, bool isFunc, auto& xnode) {
			xdoc->create_elem_node_with_text(kXmlNodeNameVarName, fileInfo.origin_name, xnode);
			xdoc->create_elem_node_with_text(kXmlNodeNameVarDescr, fileInfo.origin_descr, xnode);
			xdoc->create_elem_node_with_text(kXmlNodeNameVarSamFilename, fileInfo.filename, xnode); 
			xdoc->create_elem_node_with_bool(kXmlNodeNameVarIsDiscr, fileInfo.is_discrete, xnode);	
			xdoc->create_elem_node_with_float(kXmlNodeNameVarTemperature, fileInfo.temperature, xnode);
			xdoc->create_elem_node_with_bool(kXmlNodeNameVarIsFunc, isFunc, xnode);
			xdoc->create_elem_node_with_text(kXmlNodeNameVarFuncFormula, fileInfo.udf_formula, xnode);
		};

		for (auto& fi : rv_files_info) {
			auto randVarElem = xdoc->create_elem_node(kXmlNodeNameVar, rootElem);
			writeFileInfo(fi, false/*isFunc*/, randVarElem);
		}
		for (auto& fi : fn_files_info) {
			auto randVarElem = xdoc->create_elem_node(kXmlNodeNameVar, rootElem);
			writeFileInfo(fi, true/*isFunc*/, randVarElem);
		}
	
		xdoc->save(test_file_path);	
	}

	// Read the XML file back in
	{
		auto xdoc = XmlDoc::read_from_file(test_file_path);
		auto xroot = xdoc->root_elem();

		REQUIRE(xroot.get_attr(L"version").get_int_value() == 3);
		REQUIRE(text_as_int(xroot.get_unique_child_with_tag(kXmlNodeNameNumSamples)) == 10000u);

		auto files_info = transform_to_vector(
				xdoc->root_elem().get_children_with_tag(kXmlNodeNameVar),
				[](auto el) {

					std::wstring formula;
					auto formula_xnode = el.get_unique_child_with_tag(kXmlNodeNameVarFuncFormula);
					if (formula_xnode.has_text()) {
						formula = formula_xnode.text();
					}

					return FileInfo{
							el.get_unique_child_with_tag(kXmlNodeNameVarName).text(),
							el.get_unique_child_with_tag(kXmlNodeNameVarDescr).text(),
							el.get_unique_child_with_tag(kXmlNodeNameVarSamFilename).text(),
							text_as_bool(el.get_unique_child_with_tag(kXmlNodeNameVarIsDiscr)),
							text_as_double(el.get_unique_child_with_tag(kXmlNodeNameVarTemperature)),
							formula };
				});

		REQUIRE(files_info.size() == 4);
		auto v2 = files_info[1];

		REQUIRE(v2.origin_name == L"V2");
		REQUIRE(v2.origin_descr == L"vee two");
		REQUIRE(v2.filename == L"V2.rv");
		REQUIRE(v2.is_discrete);
		REQUIRE(v2.temperature == Catch::Approx(19.05));
	}
}

}

/// @file test_csv.cpp
/// Implementation file for "test_csv.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/test/db/test_csv.hpp"

#include "twist/chrono_utils.hpp"
#include "twist/feedback_providers.hpp"
#include "twist/db/CsvFileReader.hpp"
#include "twist/db/csv_file_utils.hpp"
#include "twist/db/DelimValueFileReader.hpp"
#include "twist/test/test_globals.hpp"

#pragma warning(disable: 4189)

using namespace twist;
using namespace twist::db;
using namespace twist::math;

namespace twist::test::db {

TEST_CASE("Test for class twist::db::CsvFileReader", "[twist::db]") 
{
	const auto path = TwistTestDataManager::get().twist_db_test_data_root_dir() / L"csv" / L"curing_data_wspace.csv";
	
	CsvFileReader<char> reader{ path };
	const auto col_count = reader.nof_columns();
	REQUIRE(col_count == 7);

	// Check first and last row in the file
	bool first_row{ true };
	std::string row_cells;
	while (reader.read_next_row()) {
		row_cells = reader.cell_ref(0);
		for (int i = 1; i < reader.nof_columns(); ++i) {
			row_cells += "|";
			row_cells += reader.cell_ref(i);
		}
		if (first_row) {
			REQUIRE(row_cells == "JUL|45|100|49|100|54|87");
			first_row = false;
		}
	}
	REQUIRE(row_cells == "JUN|52|100|67|101|69|94");
}


TEST_CASE("Test for class twist::db::DelimValueFileReader", "[twist::db]") 
{
	const auto path = TwistTestDataManager::get().twist_db_test_data_root_dir() / L"csv" 
																				/ L"nswvic_weather_uninet_trunc.csv";

	DelimValueFileReader<char, DelimValueFileFormat::comma_sep> reader{ no_header, path };
	auto data = reader.read_data_into_string_table();

	REQUIRE( data->nof_columns() == 20 );
	REQUIRE( data->nof_rows() == 13 );

	auto compare = [](auto s1, auto s2) { return strcmp(s1, s2) == 0; };

	static const auto expected_first_row_cells = { "precip24hours", "evap", "maxtemp", "mintemp", 
			"airtemp0900", "airtemp1500", "rh0900", "rh1500", "sunshinehours", "maxwind", "windsp0900", 
			"windsp1500", "winddir0900", "winddir1500", "SLP0900", "SLP1500", "cloud0900", "cloud1500", 
			"Long", "Latt" };
	REQUIRE( equal_same_size(data->get_row_cells(0), expected_first_row_cells, compare) );

	static const auto expected_last_row_cells = { "0.60", "0.10", "17.40", "5.30", "10.70", "16.90", "73.00", 
			"45.00", "9.90", "24.10", "5.40", "13.00", "135.00", "180.00", "1026.20", "1024.00", "0.00", 
			"2.00", "145.83", "-31.48" };
	REQUIRE( equal_same_size(data->get_row_cells(12), expected_last_row_cells, compare) );
}

TEST_CASE("Test for function twist::db::read_csv_file_column", "[twist::db]") 
{
	const auto path = TwistTestDataManager::get().twist_db_test_data_root_dir() / L"csv" 
	                                                                            / L"nswvic_weather_uninet_trunc.csv";

	auto evap_column_values = read_csv_file_column<char>(path, "evap", to_double<char>);
	static const auto expected_evap_column_values = {6.0, 5.4, 3.4, 8.0, 3.0, 12.0, 12.8, 7.2, 2.8, 13.2, 3.1, 0.1};

	REQUIRE( equal_same_size(evap_column_values, expected_evap_column_values) );
}

template<bool rowwise>
auto test_write_csv_file()
{
	using Mat = std::conditional_t<rowwise, RowwiseMatrix<int>, ColumnwiseMatrix<int>>;
	using Table = std::conditional_t<rowwise, RowwiseHomogenousTable<int>, ColumnwiseHomogenousTable<int>>;

	auto m = Mat{};
	m.add_row({   1,    2,    3});
	m.add_row({  10,   20,   30});
	m.add_row({ 100,  200,  300});
	m.add_row({1000, 2000, 3000});

	auto& data_mgr = TwistTestDataManager::get();
	auto out_dir_token = data_mgr.prepare_empty_temp_test_output_root_dir();

	write_csv_file<char>(m, out_dir_token.path() / L"m.txt", [](auto x) { return std::to_string(x); });

	{
		auto out_table = Table{{L"tom", L"jerry", L"donald duck"}};
		out_table.add_row({   1,    2,    3});
		out_table.add_row({  10,   20,   30});
		out_table.add_row({ 100,  200,  300});
		out_table.add_row({1000, 2000, 3000});

		write_csv_file<char>(out_table, out_dir_token.path() / L"t.txt", [](auto x) { return std::to_string(x); });

		auto in_table = read_csv_file<char, int>(out_dir_token.path() / L"t.txt", {L"donald duck", L"tom"}, 
												 [](const auto& s) { return to_int_checked(s); });

		static const auto expected_col_names = {L"donald duck", L"tom"};
		REQUIRE(equal_same_size(in_table.column_names(), expected_col_names));

		static const auto expected_values = {   3,    1,
		                                       30,   10,
											  300,  100,
		                                     3000, 1000};
		REQUIRE(in_table.nof_rows() == 4);
		REQUIRE(in_table.nof_columns() == 2);
		REQUIRE(equal_to_range(in_table, expected_values));
	}
}

TEST_CASE("Test for function twist::db::write_csv_file", "[twist::db]") 
{
	test_write_csv_file<true/*rowwise*/>();
	test_write_csv_file<false/*rowwise*/>();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  Unstructured test code

void test_csv_file_utils1(MessageFeedbackProv& feedback_prov)
{
	auto col_names = std::vector<std::string>{"tsfADJ", "BDW_000_005_EV", "CLY_000_005_EV", "PHC_000_005_EV", "bio1", 
	                                          "bio5", "bio18", "B_HZn"};

	auto start_timept = std::chrono::steady_clock::now();

	const auto rootdir = TwistTestDataManager::get().twist_db_test_data_root_dir();

	export_csv_file_columns<char>(rootdir / L"OFH_data_combined_soil_clim_tsf_noquotes.csv", 
								  rootdir / L"out.csv", 
								  col_names);

	const auto duration_ms = millisecs_since(start_timept);
	feedback_prov.set_prog_msg(format_str(L"export_csv_file_columns() run in %lld ms", duration_ms));
}

template<class Chr, DelimValueFileFormat format>
struct HeadRead {
	int header_line_count() const { return 1; }
	std::vector<std::string> get_column_names(const std::vector<std::string>&) const { return {}; };
};

void test_delim_value_file_reader2([[maybe_unused]] MessageFeedbackProv& feedback_prov)
{
	const auto path = TwistTestDataManager::get().twist_db_test_data_root_dir() / 
														L"csv" / L"nswvic_weather_uninet_trunc - BUG.csv";

	DelimValueFileReader<char, DelimValueFileFormat::comma_sep> reader1{
			with_header, path, HeadRead<char, DelimValueFileFormat::comma_sep>{}};
}

auto test_big_csv_read([[maybe_unused]] MessageFeedbackProv& feedback_prov) -> void
{
	auto path = fs::path{
			LR"(E:\frost\curjob\suppression_erica\17Aug2022\data\generated_by_dan\Random_forest_data-noquotes.csv)"};

    auto col_names = std::vector<std::wstring>{
			L"Surface hazard", L"Elevated hazard", L"Bark hazard", L"Elevation", L"Slope", L"Temperature",
	        L"Wind magnitude", L"Wind direction", L"Humidity", L"Response time", L"FFha", L"Tha", L"SOha", 
		    L"Fuel type", L"contained"};

	auto cell_conv = [](const auto& s) { 
		return to_float_checked(s); 
	};

	auto table = read_csv_file<char, float>(path, col_names, cell_conv);
}

}

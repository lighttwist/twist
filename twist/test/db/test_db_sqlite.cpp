/// @file "test_db_sqlite.cpp"
/// Implementation file for "test_db_sqlite.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/test/db/test_db_sqlite.hpp"

#include "twist/File.hpp"
#include "twist/db/SqliteDatabaseBase.hpp"
#include "twist/test/test_globals.hpp"

using namespace twist::db;

namespace twist::test::db {

struct Person {
	long long id;
	std::wstring name;
	int age;
	std::vector<std::byte> photo_data;
};

class PersonDb : public SqliteDatabaseBase<PersonDb> {
public:
	PersonDb(CreateMode /*tag*/, fs::path path)
		: SqliteDatabaseBase{ create, std::move(path) }	
	{
	
	}

	[[nodiscard]] auto table_exists(std::string table_name) const -> bool
	{
		return SqliteDatabaseBase::table_exists(table_name);
	}

	void insert_person(const Person& person)
	{
		SqliteInserter inserter{ conn(), "Person", 
				{ "id", "name", "age", "photo", "photo_compression_type", "photo_uncompressed_size" } };
		inserter.set_int64("id", person.id);
		inserter.set_text("name", &person.name);
		inserter.set_int("age", person.age);

		std::vector<unsigned char> compressed_buf;	
		SqliteBlobCompression compression_type = SqliteBlobCompression::none;
		if (inserter.set_compress_array_blob("photo", &person.photo_data, &compressed_buf)) {
			compression_type = SqliteBlobCompression::zlib;
		}
		assert(compression_type == SqliteBlobCompression::zlib);

		inserter.set_int("photo_compression_type", static_cast<int>(compression_type));
		inserter.set_int64("photo_uncompressed_size", person.photo_data.size());
		inserter.insert();
	}

	Person read_person(long long id)
	{
		Person person{ id };
		person.id = id;
		read_first_record(
				format_ansi("SELECT * FROM Person WHERE id = %lld", id),
				[&](auto& sel) {

					person.name = sel.get_text("name");
					person.age = sel.get_int("age");

					// Find out whether the photo BLOB is compressed
					[[maybe_unused]] const auto compression_type = static_cast<SqliteBlobCompression>(
							sel.get_int("photo_compression_type"));
					assert(compression_type == SqliteBlobCompression::zlib);

					person.photo_data.resize(static_cast<size_t>(sel.get_int64("photo_uncompressed_size")));
					[[maybe_unused]] const size_t result_buf_size = 
							sel.get_uncompress_array_blob("photo", person.photo_data);
				});

		return person;
	}

private:
	static std::string schema_creation_sql()
	{
		return "CREATE TABLE Person ("
					"id INTEGER PRIMARY KEY, "
					"name TEXT NOT NULL, "
					"age INTEGER NOT NULL, "
					"photo LONGBINARY NOT NULL, "
					"photo_compression_type INTEGER NOT NULL, "
					"photo_uncompressed_size INTEGER NOT NULL); ";
	}

	static const int oldest_supported_db_version{ 1 }; 

	static const int current_db_version{ 1 };

	friend class twist::db::SqliteDatabaseBase<PersonDb>;
};

void test_sqlite1()
{
	const auto dirpath = TwistTestDataManager::get().twist_db_test_data_root_dir() / L"sqlite";

	// Create database
	const auto person_db_path = dirpath / L"person.sqlite";
	if (exists(person_db_path)) {
		remove(person_db_path);
	}
	auto person_db = PersonDb{create, person_db_path};

	assert(person_db.table_exists("Person"));
	assert(!person_db.table_exists("Pesron"));

	// Load photo
	std::vector<std::byte> in_photo_data;
	{
		File photo_file{ dirpath / L"old_lady_face.bmp", FileOpenMode::read };
		in_photo_data.resize(static_cast<size_t>(photo_file.get_file_size()));
		read_buf(photo_file, in_photo_data);
	}

	// Add an old lady to the database
	person_db.insert_person({12, L"Claudia", 85, in_photo_data});

	const auto person = person_db.read_person(12);

	assert(equal_same_size(person.photo_data, in_photo_data));
}


TEST_CASE("Test one of Sqlite classes", "[twist::db]")
{
	const auto dirpath = TwistTestDataManager::get().twist_db_test_data_root_dir() / L"sqlite";

	// Create database
	const auto person_db_path = dirpath / L"person.sqlite";
	if (exists(person_db_path)) {
		remove(person_db_path);
	}
	PersonDb person_db{create, person_db_path};

	// Load photo
	std::vector<std::byte> in_photo_data;
	{
		File photo_file{ dirpath / L"old_lady_face.bmp", FileOpenMode::read };
		in_photo_data.resize(static_cast<size_t>(photo_file.get_file_size()));
		read_buf(photo_file, in_photo_data);
	}

	// Add an old lady to the database
	person_db.insert_person({12, L"Claudia", 85, in_photo_data});

	const auto person = person_db.read_person(12);

	REQUIRE( person.id == 12 );
	REQUIRE( person.name == L"Claudia" );
	REQUIRE( person.age == 85 );
	REQUIRE( equal_same_size(person.photo_data, in_photo_data) );
}

}

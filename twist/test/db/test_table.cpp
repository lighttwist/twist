/// @file test_table.cpp
/// Implementation file for "test_table.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/test/db/test_table.hpp"

#include "twist/test/test_globals.hpp"

#include "twist/db/HeterogenousNumericTable.hpp"
#include "twist/db/HomogenousTable.hpp"

#pragma warning(disable: 4100)

using namespace twist;
using namespace twist::db;

namespace twist::test::db {

template<class T, HomogenousTableStorageType storage_type>
void test(HomogenousTable<T, storage_type>& t)
{
	t.add_row({   1,    2,    3});
	t.add_row({  10,   20,   30});
	t.add_row({ 100,  200,  300});
	t.add_row({1000, 2000, 3000});
	{
		static const auto expected = {   1,    2,    3,
		                                10,   20,   30, 
		                               100,  200,  300, 
		                              1000, 2000, 3000}; 
		REQUIRE(t.nof_rows() == 4);
		REQUIRE(t.nof_columns() == 3);
		REQUIRE(t.get_column_name(1) == L"jerry");
		REQUIRE(t.get_column_index(L"tom") == 0);
		REQUIRE(t.get_column_index(L"jerry") == 1);
		REQUIRE(t.get_column_index(L"donald duck") == 2);
		REQUIRE(equal_to_range(t, expected));
	}

	t.insert_column(1, L"kermit", {9, 90, 900, 9000});
	{
		static const auto expected = {   1,    9,    2,    3,
									    10,   90,   20,   30, 
									   100,  900,  200,  300, 
									  1000, 9000, 2000, 3000}; 
		REQUIRE(t.nof_rows() == 4);
		REQUIRE(t.nof_columns() == 4);
		REQUIRE(t.get_column_index(L"tom") == 0);
		REQUIRE(t.get_column_index(L"kermit") == 1);
		REQUIRE(t.get_column_index(L"jerry") == 2);
		REQUIRE(t.get_column_index(L"donald duck") == 3);
		REQUIRE(equal_to_range(t, expected));
	}
	
	t.remove_column(L"donald duck");
	{
		static const auto expected = {   1,    9,    2,
		                                10,   90,   20, 
		                               100,  900,  200, 
		                              1000, 9000, 2000}; 
		REQUIRE(t.nof_rows() == 4);
		REQUIRE(t.nof_columns() == 3);
		REQUIRE(t.get_column_index(L"tom") == 0);
		REQUIRE(t.get_column_index(L"kermit") == 1);
		REQUIRE(t.get_column_index(L"jerry") == 2);
		REQUIRE(!has(t.column_names(), L"donald duck"));
		REQUIRE(equal_to_range(t, expected));
	}
	
	const auto col_values = t.extract_column(L"kermit");
	{
		static const auto expected_col = {9, 90, 900, 9000};
		REQUIRE(equal_same_size(col_values, expected_col));

		static const auto expected = {   1,    2,
		                                10,   20, 
		                               100,  200, 
		                              1000, 2000}; 
		REQUIRE(t.nof_rows() == 4);
		REQUIRE(t.nof_columns() == 2);
		REQUIRE(t.get_column_index(L"tom") == 0);
		REQUIRE(t.get_column_index(L"jerry") == 1);
		REQUIRE(!has(t.column_names(), L"kermit"));
		REQUIRE(equal_to_range(t, expected));
	}

	t.insert_column(2, L"kermit returns", col_values);
	{
		static const auto expected = {   1,    2,    9,
		                                10,   20,   90,
		                               100,  200,  900,
		                              1000, 2000, 9000}; 
		REQUIRE(t.nof_rows() == 4);
		REQUIRE(t.nof_columns() == 3);
		REQUIRE(t.get_column_index(L"tom") == 0);
		REQUIRE(t.get_column_index(L"jerry") == 1);
		REQUIRE(t.get_column_index(L"kermit returns") == 2);
		REQUIRE(equal_to_range(t, expected));
	}

	t.rename_column(L"jerry", L"jerry's cousin");
	REQUIRE(!has(t.column_names(), L"jerry"));
	REQUIRE(has(t.column_names(), L"jerry's cousin"));
	REQUIRE(t.get_column_index(L"jerry's cousin") == 1);

	t.remove_row(2);
	t.remove_column(0);
	t.remove_column(0);
	REQUIRE(t.nof_rows() == 3);
	t.remove_column(0);
	REQUIRE(t.nof_columns() == 0);
	REQUIRE(t.nof_rows() == 0);
	t.insert_column(0, L"tom's diner", {2, 3, 5, 7, 11, 13});
	REQUIRE(t.nof_columns() == 1);
	REQUIRE(t.nof_rows() == 6);

	for ([[maybe_unused]] auto i : IndexRange{6}) {
		t.remove_row(0);
	}
	REQUIRE(t.nof_rows() == 0);
	REQUIRE(t.nof_columns() == 1);
}

TEST_CASE("Test for class twist::db::HomogenousTable", "[twist::db]")
{
	{
		auto t = RowwiseHomogenousTable<int>{{L"tom", L"jerry", L"donald duck"}};
		test(t);
	}
	{
		auto t = ColumnwiseHomogenousTable<int>{{L"tom", L"jerry", L"donald duck"}};
		test(t);
	}
}

TEST_CASE("Test for class twist::db::HeterogenousNumericTable", "[twist::db]")
{
	using enum HeterogenousNumericTableColumnType;
	auto t1 = HeterogenousNumericTable{{{L"surface", float32}, 
	                                    {L"population", int64},
									    {L"nationhood_datetime", float64},
									    {L"years since independence", int32}},
									   3};

    REQUIRE(t1.get_column_name(0) == L"surface");
    REQUIRE(t1.get_column_name(1) == L"population");
    REQUIRE(t1.get_column_name(2) == L"nationhood_datetime");
    REQUIRE(t1.get_column_name(3) == L"years since independence");

    REQUIRE(t1.get_column_index(L"surface") == 0);
    REQUIRE(t1.get_column_index(L"population") == 1);
    REQUIRE(t1.get_column_index(L"nationhood_datetime") == 2);
	REQUIRE(t1.get_column_index(L"years since independence") == 3);

	REQUIRE(t1.get_column_type(0) == float32);
	REQUIRE(t1.get_column_type(1) == int64);
	REQUIRE(t1.get_column_type(2) == float64);
	REQUIRE(t1.get_column_type(3) == int32);

	t1.add_row( 2.3f,  11'000'000'000,  3.45,  145);
	t1.add_row( 3.2f,  99'000'000'000,  5.43,  541);
	t1.add_row(12.49f, 55'000'000'000, 15.789, 379);

	REQUIRE(t1.nof_rows() == 3);
	REQUIRE(t1.get_column_data<float>(0)   == create_vector(2.3f, 3.2f, 12.49f));
	REQUIRE(t1.get_column_data<int64_t>(1) == create_vector(11'000'000'000, 99'000'000'000, 55'000'000'000));
	REQUIRE(t1.get_column_data<double>(2)  == create_vector(3.45, 5.43, 15.789));
	REQUIRE(t1.get_column_data<int32_t>(3) == create_vector(145, 541, 379));
}

}

/// @file test_binmat.cpp
/// Implementation file for "test_binmat.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/test/db/test_binmat.hpp"

#include "twist/feedback_providers.hpp"
#include "twist/db/BinmatFile.hpp"
#include "twist/math/random_utils.hpp"
#include "twist/test/test_globals.hpp"

#include <iostream>
#include <sstream>

#pragma warning(disable: 4189)

using namespace twist::db;
using namespace twist::math;

namespace twist::test::db {

TEST_CASE("Test for class twist::db::Binmat", "[twist::db]") 
{
	auto& data_mgr = TwistTestDataManager::get();
	auto out_dir_token = data_mgr.prepare_empty_temp_test_output_root_dir();

	const auto small_binmat_path = out_dir_token.path() / L"small.binmat";
	const auto big_binmat_path = out_dir_token.path() / L"big.binmat";

	// --- Test binmats storing data for a single matrix ---

	// Test a small binmatrix
	{
		// Create a binmatrix with specific integers
		{
			const auto data_size = 20 * 10;

			auto data = FlatMatrix<int>{20, 10};
			assert(data.size() == data_size);
			std::iota(data.begin(), data.end(), 1);
			auto it = data.begin() + 13;
			*it = -1;

			auto file = BinmatFile<int>::create_for_single_matrix(small_binmat_path, 20, 10, "-1");
			file->write_whole_data_to_whole_file(data);
		}
		// Transform binmat data
		{
			auto file = BinmatFile<int>::open_for_single_matrix(small_binmat_path, 20, 10, "-1", false/*read_only*/);

			transform_binmat(*file, [](int x) {
				if (x == -1) return x;
				if (x % 2 == 0) return x * 3;
				return x * 2;
			});
		}
		// Read and verify binmat data
		{
			static const auto expected_data = { 
					2, 6, 6, 12, 10, 18, 14, 24, 18, 30, 22, 36, 26, -1, 30, 48, 34, 54, 38, 60, 42, 66, 46, 72, 
					50, 78, 54, 84, 58, 90, 62, 96, 66, 102, 70, 108, 74, 114, 78, 120, 82, 126, 86, 132, 90, 138, 
					94, 144, 98, 150, 102, 156, 106, 162, 110, 168, 114, 174, 118, 180, 122, 186, 126, 192, 130, 
					198, 134, 204, 138, 210, 142, 216, 146, 222, 150, 228, 154, 234, 158, 240, 162, 246, 166, 252, 
					170, 258, 174, 264, 178, 270, 182, 276, 186, 282, 190, 288, 194, 294, 198, 300, 202, 306, 206, 
					312, 210, 318, 214, 324, 218, 330, 222, 336, 226, 342, 230, 348, 234, 354, 238, 360, 242, 366, 
					246, 372, 250, 378, 254, 384, 258, 390, 262, 396, 266, 402, 270, 408, 274, 414, 278, 420, 282, 
					426, 286, 432, 290, 438, 294, 444, 298, 450, 302, 456, 306, 462, 310, 468, 314, 474, 318, 480, 
					322, 486, 326, 492, 330, 498, 334, 504, 338, 510, 342, 516, 346, 522, 350, 528, 354, 534, 358, 
					540, 362, 546, 366, 552, 370, 558, 374, 564, 378, 570, 382, 576, 386, 582, 390, 588, 394, 594, 
					398, 600 };

			const auto file = BinmatFile<int>::open_for_single_matrix(small_binmat_path, 20, 10, "-1", true/*read_only*/);
			const auto data = file->read_whole_to_single_matrix();	

			REQUIRE( twist::equal_same_size(data, expected_data) );
		}
	}

	// Test a larger binmat (containing more than a chunk)
	{
		// Create a small binmatrix with specific integers
		const auto nof_rows = 2000; 
		const auto nof_cols = 1000;
		auto in_data = FlatMatrix<int64_t>{nof_rows, 
			                               nof_cols, 
			                               generate_uniform_int_sample<int64_t>(nof_rows * nof_cols, 
											                                    0, 
																				1'000'000'000)};
		{
			auto file = BinmatFile<int64_t>::create_for_single_matrix(big_binmat_path, nof_rows, nof_cols, "");
			file->write_whole_data_to_whole_file(in_data);
		}
		// Transform binmat data
		auto transfunc = [](auto x) { return x * 2 - 12345; };
		{
			auto file = BinmatFile<int64_t>::open_for_single_matrix(big_binmat_path, nof_rows, nof_cols, "", 
				                                                    false/*read_only*/);
			transform_binmat(*file, [transfunc](int64_t x) {
				return transfunc(x);
			});
		}
		// Read and verify binmat data
		{
			const auto file = BinmatFile<int64_t>::open_for_single_matrix(big_binmat_path, nof_rows, nof_cols, "", 
				                                                          true/*read_only*/);
			{
				const auto out_data = file->read_whole_to_single_matrix();	
				REQUIRE(out_data.nof_rows() == nof_rows);
				REQUIRE(out_data.nof_columns() == nof_cols);

				for_each_cell(out_data, [&in_data, transfunc](auto val, auto i, auto j) {
					REQUIRE(val == transfunc(in_data.at(i, j)));
				});
			}
			{
				const auto begin_row = 12;
				const auto end_row = 112;
				auto out_data = FlatMatrix<int64_t>{end_row - begin_row + 1, nof_cols};
				file->read_rows_to_single_matrix(begin_row, end_row, out_data);	

				for_each_cell(out_data, [&in_data, transfunc](auto val, auto i, auto j) {
					REQUIRE(val == transfunc(in_data.at(begin_row + i, j)));
				});
			}
			{
				auto out_data = FlatMatrix<int64_t>{800, 1000};
				auto test = [&file, &in_data, &out_data, transfunc](auto begin_row, auto end_row, 
				                                                    auto begin_col, auto end_col) {
					file->read_submatrix_to_single_matrix(SubgridInfo{begin_row, end_row, begin_col, end_col}, 
					                                      out_data);	
					for (auto i : IndexRange{end_row - begin_row + 1}) {
						for (auto j : IndexRange{end_col - begin_col + 1}) {
							REQUIRE(out_data(i, j) == transfunc(in_data.at(begin_row + i, begin_col + j)));
						}
					}				
				};
				test(0, 1, 0, 1);
				test(0, 1, 0, nof_cols - 1);
				test(1512, 1784, 512, 784);
			}
		}
	}

	// --- Test binmats storing data for a matrix stack ---
	
	{
		//                              <-cols(3)->                    
		static const auto orig_values = { 9, 14, 26,   // ^            ^
										 55, 73,  7,   // | rows(4)    |
										 91,  4,  1,   // |            |
										 46, 69, 82,   // v            |
													   //              | matrices(2)
										 24, 90,  3,   //              |
										  5, 33, 87,   //              |
										 64, 25,  6,   //              |
										 81,  2, 47 }; //              v	
		
		const auto nof_rows = Ssize{4};
		const auto nof_cols = Ssize{3};
		const auto nof_mats = Ssize{2};

		auto in_matrix_stack = FlatMatrixStack<int>{nof_rows, nof_cols, nof_mats};
		rg::copy(orig_values, in_matrix_stack.begin());

		const auto path1 = out_dir_token.path() / L"stack1.binmat";
		{
			auto file = BinmatFile<int>::create_for_matrix_stack(path1, nof_rows, nof_cols, nof_mats, ""/*nodata_value*/);
			file->write_whole_data_to_whole_file(in_matrix_stack);
		}
		{
			auto file = BinmatFile<int>::open_for_matrix_stack(path1, nof_rows, nof_cols, nof_mats, ""/*nodata_value*/,
			                                                   true/*read_only*/);
			auto out_matrix_stack = file->read_whole_to_matrix_stack();
			assert(out_matrix_stack == in_matrix_stack);
		}
	}
}

// --- Unstructured test code ---

void test_binmat([[maybe_unused]] MessageFeedbackProv& feedback_prov)
{
}

}

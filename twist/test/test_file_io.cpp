/// @file test_file_io.cpp
/// Implementation file for "test_file_io.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE

#include "twist/twist_maker.hpp"

#include "twist/test/test_file_io.hpp"

#include "twist/test/test_globals.hpp"

#include "twist/file_io.hpp"

namespace twist::test {

static const auto  path1 = LR"()";
static const auto  path2 = LR"(D:\lighttwist)";
static const auto  path3 = LR"(D:\lighttwist\)";
static const auto  path4 = LR"(D:\lighttwist\include\twist)";
static const auto  path5 = LR"(D:\lighttwist\include\twist\)";
static const auto  path6 = LR"(D:\lighttwist\include\does_not_exist\)";
static const auto  path7 = LR"(D:\lighttwist\include\twist\async_utils.ipp)";
static const auto  path8 = LR"(D:\lighttwist\include\twist\does_not_exist.boom)";
static const auto  path9 = LR"(D:\porkful\include\twist\does_not_exist.boom)";
static const auto path10 = LR"(F:\porkful\include\twist\does_not_exist.boom)";

TEST_CASE("Test code in \"test_file_io\" files", "[twist]") 
{
	auto& data_mgr = TwistTestDataManager::get();
	auto temp_dir_token = data_mgr.prepare_empty_temp_test_output_root_dir(); 

	{	// Test get_depth() function
		REQUIRE(get_depth(path1) == 0);  //+TODO:?
		REQUIRE(get_depth(path2) == 3);
		REQUIRE(get_depth(path3) == 4);
		REQUIRE(get_depth(path4) == 5);
		REQUIRE(get_depth(path5) == 6);
		REQUIRE(get_depth(path6) == 6);
		REQUIRE(get_depth(path7) == 6);
		REQUIRE(get_depth(path8) == 6);
	}
	{	// Test make_relative() function
		REQUIRE(make_relative(path2, path8) == L"include\\twist\\does_not_exist.boom");		
		REQUIRE(make_relative(path3, path8) == L"include\\twist\\does_not_exist.boom");		
		REQUIRE(make_relative(path6, path8) == L"..\\twist\\does_not_exist.boom");		
		REQUIRE(make_relative(path4, path9) == L"..\\..\\..\\porkful\\include\\twist\\does_not_exist.boom");		
		REQUIRE(make_relative(path5, path9) == L"..\\..\\..\\porkful\\include\\twist\\does_not_exist.boom");
	}
	{	// Test change_extension() function
		auto path = change_extension(L"D:\\lighttwist\\include\\claus.txt", ".jpg");
		REQUIRE(path == L"D:\\lighttwist\\include\\claus.jpg");

		path = change_extension(L"D:\\lighttwist\\include\\claustxt", ".jpg");
		REQUIRE(path == L"D:\\lighttwist\\include\\claustxt.jpg");

		path = change_extension(L"D:\\", ".jpg");
		REQUIRE(path == L"D:\\.jpg");

		path = change_extension(L"", ".jpg");
		REQUIRE(path == L".jpg");
	}
	{	// Test get_unused_path() function
		const auto src_dir = data_mgr.twist_test_data_root_dir() / L"test_file";
		const auto dest_dir = temp_dir_token.path();
		const auto filename = L"dummy.txt";
		auto p1 = dest_dir / filename;
		fs::copy_file(src_dir / filename, p1);
		auto p2 = get_unused_path(p1);
		REQUIRE(p2 == dest_dir / L"dummy-2.txt");
		copy_file(p1, p2);
		REQUIRE(get_unused_path(p1) == dest_dir / L"dummy-3.txt");
		REQUIRE(get_unused_path(p2) == dest_dir / L"dummy-2-2.txt");
		REQUIRE(get_unused_path(dest_dir / L"dummy-90210.txt") == dest_dir / L"dummy-90210.txt");
	}
	{	// Test is_inside_dir() function
		auto d1 = fs::path{LR"(D:\parser\master\coco)"};
	    auto p11 = fs::path{LR"(D:\parser\master\coco\..\..\master\coco\bob.txt)"};
	    auto p12 = fs::path{LR"(D:\parser\master\coco\..\..\master\cocos\bob.txt)"};
	    REQUIRE(is_inside_dir(d1, p11));
	    REQUIRE(!is_inside_dir(d1, p12));
		auto d2 = fs::path{LR"(master\coco)"};
	    auto p21 = fs::path{LR"(master\coco\..\..\master\coco\bob.txt)"};
	    auto p22 = fs::path{LR"(master\coco\..\..\master\cocos\bob.txt)"};
	    REQUIRE(is_inside_dir(d2, p21));
	    REQUIRE(!is_inside_dir(d2, p22));
		auto d3 = fs::path{LR"(D:\parser\master\)"};
	    auto p3 = fs::path{LR"(D:\parser\master\coco\bob.txt)"};
	    REQUIRE(is_inside_dir(d3, p3));
	}
}

}

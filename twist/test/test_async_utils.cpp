/// @file test_async_utils.cpp
/// Implementation file for "test_async_utils.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/test/test_async_utils.hpp"

#include "twist/test/test_globals.hpp"

#include "twist/async_utils.hpp"
#include "twist/feedback_providers.hpp"
#include "twist/type_info_utils.hpp"

#pragma warning(disable:4189)

namespace twist::test {

template<class Fn, class... Args> 
std::invoke_result_t<Fn, Args...> run_some_func(Fn&& func, Args&&... args);

TEST_CASE("Test for class twist::Worker", "[twist]") 
{
	auto do_stuff = [](int val) { 
		return std::to_wstring(val); 
	};

	auto format = [](float& num) {
		return format_str(L"number is %.4f", num++);
	};

	Worker<std::wstring> worker{ WorkerDtorAction::join };
	worker.run(do_stuff, 234);
	REQUIRE( worker.get_result() == L"234" );

	auto val = -3243;
	worker.run(do_stuff, val);
	REQUIRE( worker.get_result() == L"-3243" );

	auto aa = 123.832476F;
	REQUIRE( run_some_func(format, aa) == L"number is 123.8325" );

	int xx = 12;
	const int yy = -293874;
	REQUIRE( run_some_func(do_stuff, yy) == L"-293874" );
	REQUIRE( run_some_func(do_stuff, std::ref(xx)) == L"12" );
	REQUIRE( run_some_func(do_stuff, 265) == L"265" );
}

// Helper functions

template<class Fn, class... Args> 
std::invoke_result_t<Fn, Args...> run_some_func(Fn&& func, Args&&... args )
{
	using Result = std::invoke_result_t<Fn, Args...>;

	auto try_func = [](std::remove_reference_t<Fn>&& function, std::remove_reference_t<Args>&&... arguments) {
		try {
			return std::invoke(std::forward<Fn>(function), std::forward<Args>(arguments)...);
		}
		catch (...) {
			// do something
			throw;		
		}
	};

	std::packaged_task<Result(std::remove_reference_t<Fn>&&, std::remove_reference_t<Args>&&...)> task{ try_func };

	auto future = std::make_unique<std::future<std::invoke_result_t<Fn, Args...>>>(task.get_future());

	std::thread thread{ 
			move(task), 
			std::forward<std::remove_reference_t<Fn>>(func), 
			std::forward<std::remove_reference_t<Args>>(args)... };
	thread.detach();
 
	return future->get();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  Unstructured test code

template <class T>
std::decay_t<T> decay_copy(T&& v) 
{ 
	return std::forward<T>(v); 
}

template< class Function, class... Arguments >
void thread_ctor(Function&& f, Arguments&&... args)
{
	//ShowType<Function> typeof_Fn;
	//(ShowType<Arguments>{}, ...);

	std::invoke(decay_copy(std::forward<Function>(f)), 
                decay_copy(std::forward<Arguments>(args))...);
}


void test_worker2([[maybe_unused]] MessageExceptionFeedbackProv& feedback_prov)
{
}

}


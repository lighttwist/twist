///  @file  "test_twist_stl.cpp"
///  Implementation file for "test_twist_stl.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "test_twist_stl.hpp"

#include "test_globals.hpp"

#include "twist/twist_stl.hpp"

#include <array>
#include <ranges> 
#include <tuple>
#include <utility>
		
namespace twist::test {

template<class Rng,
         class Fn, 
		 class = EnableIfFunc<Fn, RangeElementCref<Rng>>> 
auto transform_to_mapx(const Rng& range, Fn func) 
{
	using std::begin;

	using FnRet = std::invoke_result_t<Fn, decltype(*begin(range))>;
	static_assert(std::tuple_size_v<FnRet> == 2, "The transformation must return a tuple-like type with two elements.");

	using Key = std::tuple_element_t<0, FnRet>;
	using Value = std::tuple_element_t<1, FnRet>;

	std::map<Key, Value> map;
	for (const auto& elem : range) {
		auto&& [fn_ret_first, fn_ret_second] = std::invoke(func, elem);
		if (auto [_, ok] = map.emplace(std::move(fn_ret_first), std::move(fn_ret_second)); !ok) { 
			TWIST_THROW(L"Map insertion failed.");
		}
	}

	return map;
}

struct SomeStruct {
	int number;
	std::wstring string;
};

TEST_CASE("Test for transform_to_*() functions", "[twist]") 
{
	static const wchar_t str1[] = { L'n', L's', L'l', L'd', L'j', L'f', L'i', L'e', L'b', L't', L'v', L'y' };  

	const auto map1 = transform_to_mapx(str1, [](auto ch) {
		return std::make_pair(static_cast<int>(ch), std::wstring(1, ch));
	});
	const auto map2 = transform_to_mapx(str1, [](auto ch) {
		return std::make_tuple(static_cast<int>(ch), std::wstring(1, ch));
	});
	//const auto map3 = transform_to_mapx(str1, [](auto ch) {
	//	return SomeStruct{ static_cast<int>(ch), std::wstring(1, ch) };
	//});

	auto check_elem_in_map = [](const auto& map, auto pos, auto key, auto val) {
		REQUIRE(0 <= pos);
		REQUIRE(pos < ssize(map));
		auto it = map.begin();
		advance(it, pos);
		const auto& [map_key, map_val] = *it;
		REQUIRE(map_key == key);
		REQUIRE(map_val == val);
	};

	auto check_elem = [&map1, &map2, check_elem_in_map](auto pos, auto key, auto val) {
		check_elem_in_map(map1, pos, key, val);
		check_elem_in_map(map2, pos, key, val);
	};

	check_elem( 0,  98, L"b");
	check_elem( 1, 100, L"d");
	check_elem( 2, 101, L"e");
	check_elem( 3, 102, L"f");
	check_elem( 4, 105, L"i");
	check_elem( 5, 106, L"j");
	check_elem( 6, 108, L"l");
	check_elem( 7, 110, L"n");
	check_elem( 8, 115, L"s");
	check_elem( 9, 116, L"t");
	check_elem(10, 118, L"v");
	check_elem(11, 121, L"y");
}


void test_twist_stl([[maybe_unused]] MessageFeedbackProv& feedback_prov)
{

}

}

/// @file test_geotiff_utils.cpp
/// Implementation file for "test_geotiff_utils.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/test/gis/test_geotiff_utils.hpp"

#include "twist/gis/geotiff_utils.hpp"
#include "twist/test/test_globals.hpp"
#include "twist/img/Tiff.hpp"
#include "twist/img/tiff_utils.hpp"

#pragma warning(disable:4100) // unreferenced formal parameter

using namespace twist::gis;
using namespace twist::img;

namespace twist::test::gis {

TEST_CASE("Test for functions in twist::gis::geotiff_utils.*", "[twist::gis]")
{
	auto& data_mgr = TwistTestDataManager::get();

	const auto in_tiff_path = data_mgr.twist_gis_test_data_root_dir() / L"geotiff" 
	                                                                  / L"dem_centralhigh_180x180_vicgrid.tif";
	auto in_pixel_data = Tiff::DataGrid<float>{};
	{
		auto in_tiff = Tiff{Tiff::open_read, in_tiff_path};

		REQUIRE(!in_tiff.is_tiled());
		in_pixel_data = in_tiff.read_scanlined<float>();
		REQUIRE(in_pixel_data.nof_rows() == 851);
		REQUIRE(in_pixel_data.nof_columns() == 866);

		auto grid_info = read_geo_data_grid_info(in_tiff);
		REQUIRE(grid_info);
		const auto& space_grid = grid_info->space_grid();
		REQUIRE(space_grid.left() == 2493660.0);
		REQUIRE(space_grid.bottom() == 2358700.0);
		REQUIRE(space_grid.right() == 2649540.0);
		REQUIRE(space_grid.top() == 2511880.0);
		REQUIRE(space_grid.cell_width() == 180.0);
		REQUIRE(space_grid.cell_height() == 180.0);
		REQUIRE(space_grid.nof_rows() == 851);
		REQUIRE(space_grid.nof_columns() == 866);
		REQUIRE(grid_info->data_value_type() == GeoGridDataType::float32);
		REQUIRE(grid_info->nodata_value() == "-9999");
		{
			const auto point_row = 400;
			const auto point_col = 400;
			auto point = GeoPoint{2493660.0 + 180 * point_col + 100, 
									2511880.0 - 180 * point_row - 100};
			auto [row, col] = get_cell_row_col_from_pos(space_grid, point);
			REQUIRE(row == point_row);
			REQUIRE(col == point_col);
			REQUIRE(read_pixel_scanlined<float>(in_tiff, row, col) == in_pixel_data(row, col));
		}

		auto proj_info = read_projection_info(in_tiff);
		REQUIRE(proj_info);
		if (proj_info) {
			REQUIRE(match(*proj_info, KnownGeoRefsysId::vicgrid94));
		}
	}

	auto out_dir_token = data_mgr.prepare_empty_temp_test_output_root_dir();
	const auto out_tiff_path = out_dir_token.path() / L"dem_elevation_cropped_vicgrid.tif";

	const auto crop_start_row = 300;
	const auto crop_nof_rows = 100;
	const auto crop_start_col = 200;
	const auto crop_nof_cols = 300;

	auto crop_rect = GeoRectangle{2493660.0 + 180 * crop_start_col + 100,
                                  2511880.0 - 180 * crop_start_row - 100,
								  2493660.0 + 180 * (crop_start_col + crop_nof_cols) - 100,
								  2511880.0 - 180 * (crop_start_row + crop_nof_rows) + 100};

	crop_geotiff<float>(in_tiff_path, out_tiff_path, crop_rect);
	{
		auto out_tiff = Tiff{Tiff::open_read, out_tiff_path};

		auto grid_info = read_geo_data_grid_info(out_tiff);
		REQUIRE(grid_info);
		const auto& space_grid = grid_info->space_grid();
		REQUIRE(space_grid.left() == 2493660.0 + 180 * crop_start_col);
		REQUIRE(space_grid.bottom() == 2511880.0 - 180 * (crop_start_row + crop_nof_rows));
		REQUIRE(space_grid.right() == 2493660.0 + 180 * (crop_start_col + crop_nof_cols));
		REQUIRE(space_grid.top() == 2511880.0 - 180 * crop_start_row);
		REQUIRE(space_grid.cell_width() == 180.0);
		REQUIRE(space_grid.cell_height() == 180.0);
		REQUIRE(space_grid.nof_rows() == crop_nof_rows);
		REQUIRE(space_grid.nof_columns() == crop_nof_cols);
		REQUIRE(grid_info->data_value_type() == GeoGridDataType::float32);
		REQUIRE(grid_info->nodata_value() == "-9999");

		auto proj_info = read_projection_info(out_tiff);
		REQUIRE(proj_info);
		if (proj_info) {
			REQUIRE(match(*proj_info, KnownGeoRefsysId::vicgrid94));
		}

		REQUIRE(!out_tiff.is_tiled());
		const auto out_pixel_data = out_tiff.read_scanlined<float>();
		REQUIRE(out_pixel_data.nof_rows() == crop_nof_rows);
		REQUIRE(out_pixel_data.nof_columns() == crop_nof_cols);

		for (auto i : IndexRange{crop_nof_rows}) {
			for (auto j : IndexRange{crop_nof_cols}) {
				auto in_pixel_val = in_pixel_data(crop_start_row + i, crop_start_col + j);
				auto out_pixel_val = out_pixel_data(i, j);
				REQUIRE(in_pixel_val == out_pixel_val);
			}
		}
	}
}

void test_geotiff_utils1(const GeoRectangleMetre& geo_rect, MessageFeedbackProv& feedback_prov)
{
}

}


/// @file test_gdal_utils.cpp
/// Implementation file for "test_gdal_utils.hpp"

//  Copyright (c) 2007-2022, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/test/gis/test_gdal_utils.hpp"

#include "twist/feedback_providers.hpp"
#include "twist/gis/geotiff_gdal_utils.hpp"
#include "twist/gis/twist_gdal_utils.hpp"

#pragma warning(disable:4100) // unreferenced formal parameter

//#include "twist/os_utils.hpp"
//#include "twist/scope.hpp"
//#include "twist/gis/ErdasImagineFile.hpp"
//#include "twist/gis/geotiff_utils.hpp"
//#include "twist/gis/twist_gdal_utils.hpp"
//#include "twist/img/Tiff.hpp"
//#include "twist/test/test_globals.hpp"
//
//#pragma warning(disable:4189) // local variable is initialized but not referenced
//
//using namespace twist::gis;
//using namespace twist::img;
//using namespace twist::math;
//
//using Catch::Approx;

namespace twist::test::gis {

auto test_gdal_geotiff_utils1(MessageFeedbackProv& feedback_prov) -> void
{
	using namespace twist::gis;

	auto p = fs::path{
			LR"(E:\\junk\\spark4frost_test\\out\\)"
			L"dem_centralhigh_victoria_30x30_vicgrid_FLT32_MICRO_PADDED_TILED.tif"};

	auto gdal_init = GdalInitialiser{};

	auto t = read_geotiff_block(p, 0, 0);
}

}


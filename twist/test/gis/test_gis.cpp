/// @file test_gis.cpp
/// Implementation file for "test_gis.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/test/gis/test_gis.hpp"

#include "twist/scope.hpp"
#include "twist/gis/ErdasImagineFile.hpp"
#include "twist/gis/GeoCoordProjectorGdal.hpp"
#include "twist/gis/Shapefile.hpp"
#include "twist/img/Tiff.hpp"
#include "twist/math/numeric_utils.hpp"

#pragma warning(disable: 4100)

using namespace twist::gis;
using namespace twist::img;
using namespace twist::math;

namespace twist::test::gis {

void create_depth_from_top_height_canopy_geotiff(const fs::path& top_height_tiff_path);  // fwd decl

void test_erdas_imagine1()
{
	static const auto k_in_img_path = LR"(F:\junk\IC_v3.img)";  // InitialCommunitiesMap for Succession_Biomass in Landis
	static const auto k_in_tif_path = LR"(F:\junk\IC_v3_strips.tif)";  

	Tiff in_tif_file{Tiff::open_read, k_in_tif_path};
	assert(in_tif_file.sample_data_type() == GeoGridDataType::int16);
	const auto in_tif_data = in_tif_file.read_scanlined<int16_t>();
	
	ErdasImagineFile in_img_file{ErdasImagineFile::open, k_in_img_path};
	std::optional<int16_t> missing_data_value;
	const auto in_img_data = in_img_file.read_int16(missing_data_value);

	const auto in_img_map_info = in_img_file.read_map_info();
	assert(in_img_map_info);

	assert(in_img_data->nof_rows() == in_tif_data.nof_rows() && 
			in_img_data->nof_columns() == in_tif_data.nof_columns());
	assert(twist::equal(*in_img_data, in_tif_data));
	for (auto i = 0; i < in_img_data->nof_rows(); ++i) {
		for (auto j = 0; j < in_img_data->nof_columns(); ++j) {
			assert((*in_img_data)(i, j) == in_tif_data(i, j));
		}
	}

	static const auto k_out_img_path = LR"(F:\junk\test_out.img)";  
	{
		ErdasImagineFile out_tif_file{ErdasImagineFile::create, k_out_img_path, 
				(int)in_tif_data.nof_columns(), (int)in_tif_data.nof_rows(),  
				twist::gis::GeoGridDataType::int16};
		out_tif_file.write_map_info(*in_img_map_info);
		out_tif_file.write_int16(in_tif_data, missing_data_value);
	}

	ErdasImagineFile in_img_file2{ErdasImagineFile::open, k_out_img_path};
	const auto in_img_data2 = in_img_file2.read_int16(missing_data_value);
	assert(*in_img_data2 == in_tif_data);

	const auto in_img_map_info2 = in_img_file2.read_map_info();
	assert(in_img_map_info2);
}


void test_erdas_imagine2()
{
	static const auto k_in_img_path = LR"(F:\junk\biomass-anpp-1.img)";  // Landis output, "Landiswater" model
	
	ErdasImagineFile in_img_file{ErdasImagineFile::open, k_in_img_path};
	std::optional<int32_t> missing_data_value;
	const auto in_img_data = in_img_file.read_int32(missing_data_value);

	const auto in_img_map_info = in_img_file.read_map_info();
	assert(in_img_map_info);
}


void test_erdas_imagine()
{
	test_erdas_imagine2();
}


void test_rasterise_vector2()
{
	gdal_initialise();

	fs::path dir{LR"(E:\frost\raw_machinery_data\multi_studyarea\common\shapefile)"};
	auto in_path = dir / LR"(evc_efg\evdefg_rc2.shp)";
	auto out_path = dir / L"evdefg_rc2_test.tif";

	GeoRectangle in_rect{2493654.5381, 2358694.0829, 2649534.5381, 2511874.0829};
	auto out_space_grid = SquareGeoSpaceGrid{in_rect.left(), 
	                                   in_rect.bottom(),
									   30,
									   static_cast<Ssize>(in_rect.height() / 30),
									   static_cast<Ssize>(in_rect.width() / 30)};
	rasterise_vector(in_path, out_path, {"evdefg_rc2"}, "EFG_NUM", out_space_grid, GeoGridDataType::int16);

	gdal_uninitialise();
}

void test_geotiff()
{
	const fs::path data_dir{ LR"(E:\frost\frost_unit_test_data\tiff\)" };
//	Tiff tiff{data_dir / L"canopy_depth.tif"};
	Tiff tiff{ Tiff::open_read, data_dir / L"viewshed_observation_zone_healesville.tif" };

}

auto test_proj_mga_zone_55_94() -> void
{
	const auto dir_path = fs::path{LR"(E:\frost\data_othertest\test_gis\proj\GDA_1994_MGA_zone_55)"};
	const auto geotiff1_filename = L"ash_vs_mixed_mga_55_from_qgis.tif";
	const auto geotiff2_filename = L"houses_from_Brett.tif";
	
	auto test_geotiff_proj = [](auto geotiff_path) {
		auto tiff = Tiff{Tiff::open_read, geotiff_path};
		auto tiff_projection = read_projection_info(tiff);
		if (!tiff_projection) {  
			TWIST_THROW(L"The geotiff \"%s\" does not contain Coordinate Reference System information.", 
						geotiff_path.filename().c_str());
		}
		if (!match(*tiff_projection, KnownGeoRefsysId::mga_zone_55_94)) {
			TWIST_THRO2(L"The geotiff projection info does not matches the known reference system \"{}\".",
						as_long_name(KnownGeoRefsysId::mga_zone_55_94));
		}
	};

	test_geotiff_proj(dir_path / geotiff1_filename);
	test_geotiff_proj(dir_path / geotiff2_filename);

	const auto refsys = refsys_from_prj_file(dir_path / L"data.prj");
	if (!match(refsys, KnownGeoRefsysId::mga_zone_55_94)) {
		TWIST_THRO2(L"The geospatial reference system does not matches the known reference system \"{}\".",
				    as_long_name(KnownGeoRefsysId::mga_zone_55_94));
	}
}

auto test_shapefile([[maybe_unused]] MessageFeedbackProv& feedback_prov) -> void
{
	gdal_initialise();
	scope(exit) { gdal_uninitialise(); }; 

	auto shapefile = Shapefile{Shapefile::open, LR"(E:\frost\junk\reduced\fh.shp)", false/*read_only*/};

	shapefile.add_attribute_column(FeatureFieldInfo{"Koko2", FeatureFieldType::string});

	auto outer_ring = GeoLinearRing({{2'560'000, 2'480'000}, {2'590'000, 2'440'000}, 
	                                 {2'530'000, 2'440'000}, {2'560'000, 2'480'000}});
	auto polygon = GeoPolygon{std::move(outer_ring)};

	auto fid = shapefile.add_feature(polygon);
	shapefile.set_feature_attribute_value(fid, "Koko2", "muaha triangle");

	shapefile.save();
}

auto test_shapefile2([[maybe_unused]] MessageFeedbackProv& feedback_prov) -> void
{
	gdal_initialise();
	scope(exit) { gdal_uninitialise(); }; 

	auto shapefile = Shapefile{Shapefile::open, 
	                           LR"(F:\frost2\target_mach_data\grampian_fromagged_weather_netcdf_data_narclim_1_5\harvest_machine\shapefile\harvestblocks\harvestblocks.shp)", 
							   true/*read_only*/};

	auto polys = shapefile.read_all_features_as_polygons_with_attributes<int, int, double>(
	                     "OBJECTID", "HMZ", "area_ha", MultiPolygonOption::error);

}

auto test_something1(MessageFeedbackProv& feedback_prov) -> void
{
    auto gdal_init = GdalInitialiser{};
    warp_raster(R"(F:\frost2\raw_mach_data\supersimple_fuel_11feb2025\fuel_machine\geotiff\SuperSimpleFuel_ORIG.tif)",
                R"(F:\frost2\raw_mach_data\supersimple_fuel_11feb2025\fuel_machine\geotiff\SuperSimpleFuel.tif)",
                std::nullopt,
                "255",
                std::nullopt,
                GeoGridDataType::uint8, 
                std::nullopt,
                std::nullopt,
                GeotiffCompression::deflate);
}

}


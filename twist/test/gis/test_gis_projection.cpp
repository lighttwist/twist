/// @file test_gis_projection.cpp
/// Implementation file for "test_gis_projection.hpp"

//  Copyright (c) 2007-2022, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/test/gis/test_gis_projection.hpp"

#include "twist/gis/GeoCoordProjectorGdal.hpp"
#include "twist/gis/gis_globals.hpp"
#include "twist/gis/SpatialReferenceSystem.hpp"
#include "twist/gis/twist_gdal_utils.hpp"
#include "twist/test/test_globals.hpp"

#pragma warning(disable: 4100) // unreferenced formal parameter
#pragma warning(disable: 4189) // local variable is initialized but not referenced

using namespace twist::gis;

using Catch::Approx;
using Catch::Matchers::WithinRel;

namespace twist::test::gis {

TEST_CASE("Test for projection utilities", "[twist::gis]")
{
	auto gdal_init = GdalInitialiser{};
	auto wgs84_refsys = refsys_from_wgs84();

	auto verify = [&wgs84_refsys](auto refsys_id, 
	                              const std::vector<GeoPointMetre>& points_metre, 
								  const std::vector<GeoPointDecDegree>& points_degree) {
		auto projector = GeoCoordProjectorGdal{wgs84_refsys, refsys_from_known(refsys_id)};
		const auto nof_points = ssize(points_metre);
		REQUIRE(ssize(points_degree) == nof_points);
		for (auto i : IndexRange{nof_points}) {
			static const auto rel_tol = 1.0e-6;
			auto pt_degree = projector.metre_to_degree(points_metre[i]); 
			REQUIRE_THAT(pt_degree.longitude(), WithinRel(points_degree[i].longitude(), rel_tol));
			REQUIRE_THAT(pt_degree.latitude(), WithinRel(points_degree[i].latitude(), rel_tol));
			auto pt_metre = projector.degree_to_metre(points_degree[i]);
			REQUIRE_THAT(pt_metre.x(), WithinRel(points_metre[i].x(), rel_tol));
			REQUIRE_THAT(pt_metre.y(), WithinRel(points_metre[i].y(), rel_tol));
		}
	};

	verify(KnownGeoRefsysId::vicgrid94,
	       {{2'572'800, 2'307'550}, {2'593'760, 2'302'370}, {2'563'000, 2'309'999}},
		   {{145.83698, -38.73099}, {146.07861, - 38.77574}, {145.7241, -38.70966}});

	verify(KnownGeoRefsysId::nsw_lambert94,
	       {{9'414'000, 4'223'000}, {9'522'000, 4'318'000}},
		   {{148.2603, -35.7424}, {149.4296, -34.8688}});
	
	//+TODO: Test all "known" refsys IDs 
}

auto test_gis_projection1(twist::MessageFeedbackProv& feedback_prov) -> void
{

}

}


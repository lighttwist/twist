/// @file test_gdal_geotiff_utils.cpp
/// Implementation file for "test_gdal_geotiff_utils.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/test/gis/test_gdal_geotiff_utils.hpp"

#include "twist/feedback_providers.hpp"
#include "twist/os_utils.hpp"
#include "twist/scope.hpp"
#include "twist/gis/ErdasImagineFile.hpp"
#include "twist/gis/geotiff_utils.hpp"
#include "twist/gis/twist_gdal_utils.hpp"
#include "twist/img/Tiff.hpp"
#include "twist/test/test_globals.hpp"

#pragma warning(disable:4100) // unreferenced formal parameter
#pragma warning(disable:4189) // local variable is initialized but not referenced

using namespace twist::gis;
using namespace twist::img;
using namespace twist::math;

using Catch::Approx;

namespace twist::test::gis {

static const auto grid_coord_tol = 1e-12;
static const auto val_coord_tol = 1e-14;

TEST_CASE("Test for twist::gis::translate_raster()", "[twist::gis]")
{
	auto gdal_init = GdalInitialiser{};

	auto& data_mgr = TwistTestDataManager::get();
	auto out_dir_token = data_mgr.prepare_empty_temp_test_output_root_dir();
	{
		const auto in_tiff_path = data_mgr.twist_gis_test_data_root_dir() / L"geotiff" 
																		  / L"dem_whole_victoria_30x30_vicgrid.tif";
		// Central Highlands fuel data study grid
		auto dest_space_grid = SquareGeoSpaceGrid{2493654.5381, 2358694.0829, 30, 5106, 5196}; 

		auto check_out_tiff = [dest_space_grid](const auto& path) {
			const auto raster_info = read_raster_info(path);
			REQUIRE(raster_info.height == dest_space_grid.nof_rows());
			REQUIRE(raster_info.width == dest_space_grid.nof_columns());
			REQUIRE(raster_info.geo_info);
			if (raster_info.geo_info) {
				REQUIRE(raster_info.geo_info->origin == GeoPoint(dest_space_grid.left(), dest_space_grid.top()));
				REQUIRE(raster_info.geo_info->pixel_size.x() ==  dest_space_grid.cell_size());
				REQUIRE(raster_info.geo_info->pixel_size.y() == -dest_space_grid.cell_size());
			}
			REQUIRE(ssize(raster_info.bands_info) == 1);
			if (ssize(raster_info.bands_info) == 1) {
				const auto& band_info = raster_info.bands_info.begin()->second;
				REQUIRE(band_info.data_value_type == GeoGridDataType::int16);
				REQUIRE(band_info.no_data_value);
				if (band_info.no_data_value) {
					REQUIRE(*band_info.no_data_value == -9999.0);
				}
			}
			// std::wcout << L"\n" << *raster_info.projection;
		};

		const auto out_tiff_path = out_dir_token.path() / L"dem_centralhigh_victoria.tif";
		translate_raster(in_tiff_path, 
						 out_tiff_path, 
						 RasterFileFormat::geotiff, 
						 dest_space_grid, 
						 std::nullopt/*out_data_type*/, 
						 RasterResampleAlgorithm::mode);
		check_out_tiff(out_tiff_path);
		auto tiff_data1 = Tiff{Tiff::open_read, out_tiff_path}.read_scanlined<int16_t>();

		const auto out_compressed_tiff_path = out_tiff_path.parent_path() / (out_tiff_path.stem().wstring() + 
						                                                     L"_compressed" + 
																			 out_tiff_path.extension().wstring()); 
		translate_raster(in_tiff_path, 
						 out_compressed_tiff_path,
						 RasterFileFormat::geotiff, 
						 dest_space_grid, 
						 std::nullopt/*out_data_type*/, 
						 RasterResampleAlgorithm::mode,
						 std::nullopt/*value_rescale*/,
						 std::nullopt/*geotiff_nof_cells_per_tile*/,
						 GeotiffCompression::deflate);
		check_out_tiff(out_compressed_tiff_path);

		REQUIRE(fs::file_size(out_compressed_tiff_path) < file_size(out_tiff_path));
		
		auto tiff_data2 = Tiff{Tiff::open_read, out_compressed_tiff_path}.read_scanlined<int16_t>();
		REQUIRE(equal_same_size(tiff_data1, tiff_data2));
	}
	
	{
		const auto cells_per_tile_side = 32;
		const auto out_tiled_tiff_path = out_dir_token.path() / L"dem_victoria_tiled.tif";

		const auto in_tiff_path = data_mgr.twist_gis_test_data_root_dir() / L"geotiff" 
																		  / L"dem_whole_victoria_30x30_vicgrid.tif";
		translate_raster(in_tiff_path, 
						 out_tiled_tiff_path, 
						 RasterFileFormat::geotiff, 
						 std::nullopt/*out_space_grid*/, 
						 std::nullopt/*out_data_type*/, 
						 RasterResampleAlgorithm::mode,
						 std::nullopt/*value_rescale*/,
						 cells_per_tile_side * cells_per_tile_side);
		auto tiled_tiff = Tiff{Tiff::open_read, out_tiled_tiff_path};
		REQUIRE(tiled_tiff.is_tiled());
		auto tile_format_info = tiled_tiff.get_tile_format_info();
		REQUIRE(tile_format_info.full_tile_width() == cells_per_tile_side);
		REQUIRE(tile_format_info.full_tile_height() == cells_per_tile_side);
		REQUIRE(tile_format_info.nof_tile_rows() == tiled_tiff.height() / cells_per_tile_side +
			                                        ((tiled_tiff.height() % cells_per_tile_side == 0) ? 0 : 1));
		REQUIRE(tile_format_info.nof_tile_columns() == tiled_tiff.width() / cells_per_tile_side +
			                                            ((tiled_tiff.width() % cells_per_tile_side == 0) ? 0 : 1));
	}

	{	
		const auto nof_rows = 100;
		const auto nof_cols = 200;
		const auto in_tiff_path = out_dir_token.path() / L"scale_me.tif";
		{
			auto in_tiff = Tiff{Tiff::open_write, in_tiff_path, GeoGridDataType::int16, nof_cols, nof_rows};
			auto in_data = std::vector<int16_t>(nof_rows * nof_cols);
			for (auto i : IndexRange{nof_rows}) {
				std::fill(begin(in_data) + nof_cols * i, 
				          begin(in_data) + nof_cols * (i + 1),
						  static_cast<int16_t>(i - 50)); 
			}
			in_tiff.write_scanlined(Tiff::DataGrid<short>{200, 100, move(in_data)});
		}
		const auto out_tiff_path = out_dir_token.path() / L"me_scaled.tif";
		translate_raster(in_tiff_path, 
						 out_tiff_path, 
						 RasterFileFormat::geotiff, 
						 std::nullopt/*out_space_grid*/, 
						 GeoGridDataType::float32, 
						 std::nullopt/*out_data_type*/,
						 RasterValueRescale{ClosedInterval{6, 7}, ClosedInterval{0.12f, 0.28f}}); // x -> x * 0.16 - 84
		{
			auto out_tiff = Tiff{Tiff::open_read, out_tiff_path};
			auto out_data = out_tiff.read_scanlined<float>();
			for (auto i : IndexRange{nof_rows}) {
				std::for_each(out_data.begin() + nof_cols * i, 
							  out_data.begin() + nof_cols * (i + 1),
							  [i](auto data_val) { 
                                  auto in_val = i - 50;
								  REQUIRE(data_val == Approx{in_val * 0.16f - 0.84f}); 
							  });				
			}
		}
	}
}

TEST_CASE("Test for twist::gis::warp_raster()", "[twist::gis]")
{
	auto gdal_init = GdalInitialiser{};

	auto& data_mgr = TwistTestDataManager::get();
	auto out_dir_token = data_mgr.prepare_empty_temp_test_output_root_dir();

    {
	    const auto tiled_tiff_path = out_dir_token.path() / L"dem_victoria_tiled.tif";
        auto warped_space_grid =  std::optional<SquareGeoSpaceGrid>{}; 
    	{
		    const auto in_tiff_path = data_mgr.twist_gis_test_data_root_dir() / L"geotiff" 
																		      / L"dem_whole_victoria_30x30_vicgrid.tif";
		    const auto cells_per_tile_side = 32;		 
            
            translate_raster(in_tiff_path, 
						     tiled_tiff_path, 
						     RasterFileFormat::geotiff, 
						     std::nullopt/*out_space_grid*/, 
						     std::nullopt/*out_data_type*/, 
						     RasterResampleAlgorithm::mode,
						     std::nullopt/*value_rescale*/,
						     cells_per_tile_side * cells_per_tile_side);

		    auto tiled_tiff = Tiff{Tiff::open_read, tiled_tiff_path};
		    REQUIRE(tiled_tiff.is_tiled());	
		    auto grid_info = read_geo_data_grid_info(tiled_tiff);
		    REQUIRE(grid_info);
			REQUIRE(grid_info->nodata_value() == "-9999");
            const auto space_grid = grid_info->space_grid();
            warped_space_grid = get_subgrid(to_square_grid(space_grid), 
                                            {0, space_grid.nof_rows() / 4, 0, space_grid.nof_columns() / 4});
        }            

        {
		    const auto cells_per_tile_side = 128;	
            const auto warped_tiff_path = out_dir_token.path() / L"dem_victoria_warped.tif";
            warp_raster(tiled_tiff_path, 
				        warped_tiff_path,
                        twist::gis::RasterFileFormat::geotiff, 
                        "-6666",
                        warped_space_grid,
				        std::nullopt/*out_data_type*/, 
				        std::nullopt/*resample_algo*/,
                        cells_per_tile_side * cells_per_tile_side,
                        GeotiffCompression::deflate);

		    auto warped_tiff = Tiff{Tiff::open_read, warped_tiff_path};
		    auto grid_info = read_geo_data_grid_info(warped_tiff);
		    REQUIRE(grid_info);
		    REQUIRE(compare(grid_info->space_grid(), *warped_space_grid));
	        REQUIRE(grid_info->nodata_value() == "-6666");

		    REQUIRE(warped_tiff.is_tiled());
		    auto tile_format_info = warped_tiff.get_tile_format_info();
		    REQUIRE(tile_format_info.full_tile_width() == cells_per_tile_side);
		    REQUIRE(tile_format_info.full_tile_height() == cells_per_tile_side);
		    REQUIRE(tile_format_info.nof_tile_rows() == warped_tiff.height() / cells_per_tile_side +
			                                            ((warped_tiff.height() % cells_per_tile_side == 0) ? 0 : 1));
		    REQUIRE(tile_format_info.nof_tile_columns() == warped_tiff.width() / cells_per_tile_side +
			                                               ((warped_tiff.width() % cells_per_tile_side == 0) ? 0 : 1));
        }
    }
}

TEST_CASE("Test for twist::ErdasImagineFile and twist::gis::warp_raster()", "[twist::gis]")
{
	auto gdal_init = GdalInitialiser{};

	auto& data_mgr = TwistTestDataManager::get();
	auto out_dir_token = data_mgr.prepare_empty_temp_test_output_root_dir();

	const auto in_path = data_mgr.twist_gis_test_data_root_dir() / L"Ecoregion_EG.img";
	const auto out_path = out_dir_token.path() / L"Ecoregion_EG_warped.img";

	auto out_space_grid = SquareGeoSpaceGrid{2784900.0, 2402080.0, 180, 332, 561};
	warp_raster(in_path, 
				out_path,
				RasterFileFormat::img,
				"255",
				out_space_grid,
				GeoGridDataType::uint8);

	auto out_file = ErdasImagineFile{ErdasImagineFile::open, out_path};
	REQUIRE(out_file.data_type() == GeoGridDataType::uint8);

	auto map_info = out_file.read_map_info();
	REQUIRE(map_info);
	if (map_info) {
		REQUIRE(compare_tol(map_info->space_grid(), out_space_grid, grid_coord_tol));
	}

	auto nodata_val = out_file.nodata_value();
	REQUIRE(nodata_val);
	if (nodata_val) {
		REQUIRE(*nodata_val == 255);
	}
}

auto test_gdal_utils2(const GeoRectangleMetre& geo_rect, MessageFeedbackProv& feedback_prov) -> void
{
	auto gdal_init = GdalInitialiser{};

	auto dir_path = fs::path{LR"(F:\frost2\raw_mach_data\multi_studyarea_vic_sa\fuel_machine\narclim\geotiff\climate)"};
	auto src_path = dir_path / L"bioclim_35_se_1950_2019_p01_vicgrid.tif";
	//auto dest_path = dir_path / L"bioclim_35_se_1950_2019_p01_vicgrid__translated.tif";
	auto dest_path = LR"(F:\frost2\_junk\bioclim_35_se_1950_2019_p01_vicgrid__warped.img)";
	auto dest2_path = LR"(F:\frost2\_junk\bioclim_35_se_1950_2019_p01_vicgrid__warped2.img)";

	auto geo_data_grid_info = read_geo_data_grid_info(src_path);
	assert(contains(perimeter_rect(geo_data_grid_info->space_grid()), geo_rect));

	auto subgrid = get_subgrid(geo_data_grid_info->space_grid(),
	                           *get_info_of_subgrid_containing_rect(geo_data_grid_info->space_grid(),
				                                                    geo_rect));
																	
	translate_raster(src_path, 
	                 dest_path, 
				     RasterFileFormat::img, 
					 subgrid,
					 GeoGridDataType::float32, 
					 RasterResampleAlgorithm::average);

	//warp_raster(dest_path, 
	//            dest2_path,
	//	          RasterFileFormat::img,
	//	          "-3.4e+38");
}

auto test_polygonise_raster() -> void
{
	auto gdal_init = GdalInitialiser{};

	auto in_path = LR"(E:\frost\frost_unit_test_data\tiff\ECBRL_EFG_NUM.tif)";
	auto out_path = LR"(E:\frost\frost_unit_test_data\tiff\POLYGON_ECBRL_EFG_NUM)";
	polygonise_raster(in_path, out_path, {"floofloo", FeatureFieldType::int32});	
}

auto test_something(MessageFeedbackProv& feedback_prov) -> void
{

}

}


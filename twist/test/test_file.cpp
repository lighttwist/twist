///  @file  test_file.cpp
///  Implementation file for "test_file.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "test_file.hpp"

#include <cstdio>
#include <iostream>

#include "twist/chrono_utils.hpp"
#include "twist/feedback_providers.hpp"
#include "twist/File.hpp"
#include "twist/type_info_utils.hpp"

#include "twist/math/random_utils.hpp"

#include "test_globals.hpp"

using namespace twist::math;

namespace twist::test {


TEST_CASE("Test class \"FileStd\"", "[twist]") 
{
	const auto data_dir_path = TwistTestDataManager::get().twist_test_data_root_dir() / L"test_file";
	const auto data_size = 10'000'000;

	const auto out_data = generate_uniform_real_sample<double>(data_size, 0, 100);
	std::vector<double> in_data(data_size);

	{	// Create and write to binary file
		File out_file{ data_dir_path / L"real_sample.bin", FileOpenMode::create_write };
		write_buf(out_file, out_data);
		REQUIRE(out_file.get_file_size() == data_size * sizeof(double));
	}
	{	// Read binary file
		File in_file{ data_dir_path / L"real_sample.bin", FileOpenMode::read };
		read_buf(in_file, in_data);
		REQUIRE(in_file.get_file_size() == data_size * sizeof(double));
	}

	REQUIRE(equal_same_size(out_data, in_data));

	{	// Check file pointer position
		static const auto small_data_size = 500'000;
		std::vector<double> small_data(small_data_size);
		File file{ data_dir_path / L"real_sample.bin", FileOpenMode::read };
		read_buf(file, small_data);

		const auto file_pos = file.get_file_pos();
		REQUIRE(file_pos == small_data_size * sizeof(double));
	}
}

template<class F>
void test_any_file(MessageFeedbackProv& feedback_prov)
{
	const auto data_dir_path = TwistTestDataManager::get().twist_test_data_root_dir() / L"test_file";
	const auto data_size = 10'000'000;

	const auto out_data = generate_uniform_real_sample<double>(data_size, 0, 100);
	std::vector<double> in_data(data_size);

	auto start_timept = std::chrono::steady_clock::now();

	{	// Create and write to binary file
		File out_file{ data_dir_path / L"real_sample.bin", FileOpenMode::create_write };
		write_buf(out_file, out_data);
		assert(out_file.get_file_size() == data_size * sizeof(double));
	}
	{	// Read binary file
		File in_file{ data_dir_path / L"real_sample.bin", FileOpenMode::read };
		read_buf(in_file, in_data);
		assert(in_file.get_file_size() == data_size * sizeof(double));
	}

	const auto duration_ms = millisecs_since(start_timept);
	feedback_prov.set_prog_msg(format_str(L"%s writing and reading took %d ms.", 
			type_wname<F>().c_str(), duration_ms));

	assert(equal_same_size(out_data, in_data));

	{	// Check file pointer position
		static const auto small_data_size = 500'000;
		std::vector<double> small_data(small_data_size);
		File file{ data_dir_path / L"real_sample.bin", FileOpenMode::read };
		read_buf(file, small_data);

		[[maybe_unused]] const auto file_pos = file.get_file_pos();
		assert(file_pos == small_data_size * sizeof(double));
	}
}

void test_file1(MessageFeedbackProv& feedback_prov)
{
#if TWIST_OS_WIN
	test_any_file<FileWin>(feedback_prov);
#endif
	test_any_file<FileStd>(feedback_prov);
}

}


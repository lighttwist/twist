///  @file  test_inheritance.cpp
///  Implementation file for "test_inheritance.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "test_inheritance.hpp"

#include "twist/os_utils.hpp"

#pragma warning(disable: 4250)

namespace twist::test {

class Sleeper {
public:
	virtual ~Sleeper() = 0;

	virtual void sleep() = 0;

private:
	bool is_asleep{ true };
}; 

Sleeper::~Sleeper()
{
}


class Walker {
public:
	virtual ~Walker() = 0;

	virtual void walk() = 0;

private:
	bool is_walking{ true };
}; 

Walker::~Walker()
{
}


class Sleepwalker : public virtual Sleeper, public virtual Walker {
public:
	~Sleepwalker() override {}

private:
	bool is_walking{ true };
};

class Bob : public virtual Sleeper {
public:
	~Bob() override {}

	void sleep() override
	{
		out_debug_str("Bob sleeps");
	}
};


class Bobby : public Bob, public Sleepwalker {
public:
	~Bobby() override {}

	//void sleep() override
	//{
	//	Bob::sleep();
	//}

	void walk() override
	{
		out_debug_str("Bobby walks");
	}
};

//               Sleeper    Walker
//                 / \         / 
//                /   \       /    
//              Bob   Sleepwalker
//               \     /
//                \   /
//                Bobby

void test_mi_1(MessageFeedbackProv&)
{
	Sleepwalker* sw = new Bobby{};
	sw->sleep();
}


}

/// @file test_tiff.cpp
/// Implementation file for "test_tiff.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/test/img/test_tiff.hpp"

#include "twist/img/Tiff.hpp"
#include "twist/img/tiff_utils.hpp"
#include "twist/test/test_globals.hpp"

using namespace twist::img;

namespace twist::test::img {

TEST_CASE("Test for class twist::img::Tiff", "[twist::img]")
{
	auto& data_mgr = TwistTestDataManager::get();
	const auto in_path = data_mgr.twist_gis_test_data_root_dir() / L"geotiff" / L"dem_centralhigh_180x180_vicgrid.tif";
	auto out_dir_token = data_mgr.prepare_empty_temp_test_output_root_dir();

	auto pixel_data1 = Tiff::DataGrid<float>{};
	auto pixel_data2 = Tiff::DataGrid<float>{};
	{
		auto tiff = Tiff{Tiff::open_read, in_path};
		pixel_data1 = tiff.read_scanlined<float>();

		const auto row = 100;
		const auto col = 200;
		REQUIRE(tiff.width() > col);
		REQUIRE(tiff.height() > row);
		REQUIRE(read_pixel_scanlined<float>(tiff, row, col) == pixel_data1(row, col));
	}
	{ 
		auto tiff = Tiff{Tiff::open_write, 
		                 out_dir_token.path() / L"scanline.tiff", 
						 twist::gis::enum_for_geo_grid_data_type<float>,
						 pixel_data1.nof_columns(),
						 pixel_data1.nof_rows()};
		tiff.write_scanlined(pixel_data1);
	}
	{ 
		auto tiff = Tiff{Tiff::open_read, out_dir_token.path() / L"scanline.tiff"};
		pixel_data2 = tiff.read_scanlined<float>();
		REQUIRE(equal_same_size(pixel_data1.view_matrix(), pixel_data2.view_matrix()));
	}
	{
		write_tiled_tiff<float>(out_dir_token.path() / L"tiled.tiff", pixel_data1, 16 * 16);
		pixel_data2 = std::get<0>(read_tiled_tiff<float>(out_dir_token.path() / L"tiled.tiff"));
		REQUIRE(equal_same_size(pixel_data1.view_matrix(), pixel_data2.view_matrix()));
	}
}

// --- Unstructured test code ---

void test_tiff([[maybe_unused]] MessageFeedbackProv& feedback_prov)
{
}

}

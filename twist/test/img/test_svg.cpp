/// @file test_svg.cpp
/// Implementation file for "test_svg.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/test/img/test_svg.hpp"

#include "twist/img/svg/simple_svg.hpp"
#include "twist/test/test_globals.hpp"

#include "twist/feedback_providers.hpp"

using namespace twist::img::svg;

namespace twist::test::img {

//TEST_CASE("Test for class twist::img::Tiff", "[twist::img]")
//{
//	auto& data_mgr = TwistTestDataManager::get();
//	const auto in_path = data_mgr.twist_gis_test_data_root_dir() / L"geotiff" / L"dem_centralhigh_180x180_vicgrid.tif";
//	auto out_dir_token = data_mgr.prepare_empty_temp_test_output_root_dir();
//
//	auto pixel_data1 = Tiff::DataGrid<float>{};
//	auto pixel_data2 = Tiff::DataGrid<float>{};
//	{
//		auto tiff = Tiff{Tiff::open_read, in_path};
//		pixel_data1 = tiff.read_scanlined<float>();
//
//		const auto row = 100;
//		const auto col = 200;
//		REQUIRE(tiff.width() > col);
//		REQUIRE(tiff.height() > row);
//		REQUIRE(read_pixel_scanlined<float>(tiff, row, col) == pixel_data1(row, col));
//	}
//	{ 
//		auto tiff = Tiff{Tiff::open_write, 
//		                 out_dir_token.path() / L"scanline.tiff", 
//						 twist::gis::enum_for_geo_grid_data_type<float>,
//						 pixel_data1.nof_columns(),
//						 pixel_data1.nof_rows()};
//		tiff.write_scanlined(pixel_data1);
//	}
//	{ 
//		auto tiff = Tiff{Tiff::open_read, out_dir_token.path() / L"scanline.tiff"};
//		pixel_data2 = tiff.read_scanlined<float>();
//		REQUIRE(equal_same_size(pixel_data1.view_matrix(), pixel_data2.view_matrix()));
//	}
//	{
//		write_tiled_tiff<float>(out_dir_token.path() / L"tiled.tiff", pixel_data1, 16 * 16);
//		pixel_data2 = std::get<0>(read_tiled_tiff<float>(out_dir_token.path() / L"tiled.tiff"));
//		REQUIRE(equal_same_size(pixel_data1.view_matrix(), pixel_data2.view_matrix()));
//	}
//}

// --- Unstructured test code ---

// https://github.com/adishavit/simple-svg

ShapeColl createSVGElements()
{
    ShapeColl elements;

    // Create a LineChart with Long notation
    LineChart chart(Dimensions(12.5, 12.5));

    // Create three Polylines with different colors and strokes
    Polyline polyline_a(Stroke(1.25, Color::Blue));
    Polyline polyline_b(Stroke(1.25, Color::Aqua));
    Polyline polyline_c(Stroke(1.25, Color::Fuchsia));

    // Add points to each polyline
    polyline_a << Point(0, 0) << Point(25, 75) << Point(50, 100)
               << Point(75, 112.5) << Point(100, 110);
    polyline_b << Point(0, 25) << Point(25, 55) << Point(50, 75)
               << Point(75, 80) << Point(100, 75);
    polyline_c << Point(0, 30) << Point(25, 37.5) << Point(50, 35)
               << Point(75, 25) << Point(100, 5);

    // Add the polylines to the chart
    chart << polyline_a << polyline_b << polyline_c;

    // Add the chart to the elements collection
    elements << chart;

    // Create and add another LineChart with Condensed notation
    elements << (LineChart(Dimensions(162.5, 12.5))
                 << (Polyline(Stroke(1.25, Color::Blue))
                     << Point(0, 0) << Point(25, 20) << Point(50, 32.5))
                 << (Polyline(Stroke(1.25, Color::Orange))
                     << Point(0, 25) << Point(25, 40) << Point(50, 50))
                 << (Polyline(Stroke(1.25, Color::Cyan))
                     << Point(0, 12.5) << Point(25, 32.5) << Point(50, 40)));

    // Create and add a Circle with specific properties
    elements << Circle(Point(200, 200), 50, Fill(Color(100, 200, 120)),
                       Stroke(2.5, Color(200, 250, 150)));

    // Create and add a Text element with specific properties
    auto text = Text(Point(12.5, 192.5), "Simple SVG", Fill(Color::Silver),
                     Font(25, "Verdana"));
    elements << text;

    // Create and add a Rectangle to represent the Text bounding box
    auto bb = text.getBoundingBox();
    elements << Rectangle(bb.origin, bb.size.width, bb.size.height,
                          Fill(Color::Transparent), Stroke(1, Color::Red));

    // Create and add a Polygon with specific properties
    elements << (Polygon(Fill(Color(200, 160, 220)),
                         Stroke(1.25, Color(150, 160, 200)))
                 << Point(50, 175) << Point(62.5, 180) << Point(82.5, 175)
                 << Point(87.5, 150) << Point(62.5, 137.5) << Point(45, 157.5));

    // Create and add a Rectangle with specific properties
    elements << Rectangle(Point(175, 137.5), 50, 37.5, Fill(Color::Yellow),
                          Stroke(1, Color::Black));

    // Create an ellipse with specific properties
    elements << Elipse(Point(200, 120), 50, 25, Fill(Color::Red),
                       Stroke(1.5, Color::Black));

    // Return the collection of SVG elements
    return elements;
}

// Demo page shows sample usage of the Simple SVG library.

void demoDocLayout(const Layout &layout, MessageFeedbackProv& feedback_prov)
{
    // Generate filename based on the layout origin type
    std::string filename;
    switch (layout.origin)
    {
        case Layout::TopLeft:
            filename = "G:\\svg_topleft.svg";
            break;
        case Layout::TopRight:
            filename = "G:\\svg_topright.svg";
            break;
        case Layout::BottomLeft:
            filename = "G:\\svg_bottomleft.svg";
            break;
        case Layout::BottomRight:
            filename = "G:\\svg_bottomright.svg";
            break;
    }

    // Create SVG document with specified layout
    Document doc(filename, layout);

    // Draw border rectangle using document dimensions
    Polygon border(Stroke(1, Color::Red));
    border << Point(0, 0) << Point(layout.dimensions.width, 0)
           << Point(layout.dimensions.width, layout.dimensions.height)
           << Point(0, layout.dimensions.height);
    doc << border;

    // Mark origin and the farthest point in the layout with circles
    doc << Circle(Point(0, 0), 20, Fill(Color::Red), Stroke(1, Color::Black));
    doc << Circle(
        Point(layout.dimensions.width - 10, layout.dimensions.height - 10), 20,
        Fill(Color::Red), Stroke(1, Color::Black));

    // Create and add all demo shapes
    ShapeColl elements = createSVGElements();
    doc << elements;

    // Save document and report status
    if (doc.save())
    {
        feedback_prov.set_prog_msg(std::format("File saved successfully: {}", filename));
    }
    else
    {
        feedback_prov.set_prog_msg(std::format("Failed to save the file: {}", filename));
    }
}

auto test_svg1([[maybe_unused]] MessageFeedbackProv& feedback_prov) -> void
{
    try
    {
        // Set canvas dimensions for all layouts
        Dimensions dimensions(500, 500);

        // Define array of all possible coordinate system origins
        const Layout::Origin layouts[] = {Layout::TopLeft, Layout::TopRight,
                                          Layout::BottomLeft,
                                          Layout::BottomRight};

        // Generate SVG file for each coordinate system origin
        for (Layout::Origin origin : layouts)
        {
            Layout layout(dimensions, origin);
            demoDocLayout(layout, feedback_prov);
        }
    }
    catch (const std::exception &e)
    {
        feedback_prov.set_prog_msg(std::format("An exception occurred: {}", e.what()));
    }
    catch (...)
    {
        feedback_prov.set_prog_msg("An unknown exception occurred.");
    }
}

auto test_svg2([[maybe_unused]] MessageFeedbackProv& feedback_prov) -> void
{
    auto layout = Layout{Dimensions{600, 800}};

    auto elements = ShapeColl{};
    //elements << Elipse{Point{200, 120}, 50, 25, Fill{}, Stroke{1, Color::Black}};
    elements << Text{Point{180, 115}, "Zoomba Woomb", Fill{Color::Green}, Font{12, "Verdana"}};

    auto path = L"G:\\bbn.svg"; 
    auto doc = Document{string_to_ansi(path), layout};
    doc << elements;

    
    if (!doc.save()) {
        TWIST_THRO2(L"Failed to save SVG file \"{}\".", path);
    }
}

}

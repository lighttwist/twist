/// @file test_libtiff.cpp
/// Implementation file for "test_libtiff.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/test/img/test_libtiff.hpp"

#pragma warning (disable: 4100)
#pragma warning (disable: 4996)

#include "external/libtiff/include/tiff.h"
#include "external/libtiff/include/tiffconf.h"
#include "external/libtiff/include/tiffio.h"
#include "external/libtiff/include/tiffio.hxx"

#include <stdio.h>
#include <string.h>

#ifdef HAVE_UNISTD_H
# include <unistd.h>
#endif

#include "twist/test/img/external/tif_dir.h"

namespace twist::test::img {

auto test_libtiff1([[maybe_unused]] MessageFeedbackProv& feedback_prov) -> int
{
    // Example of handling custom directories like EXIF.
    // Copyright (c) 2012, Frank Warmerdam <*****@*****.**>

    static const char filename[] = R"(D:\_junk\custom_dir.tif)";

    static const auto SPP = 3; /* Samples per pixel */
    const uint16	width = 1;
    const uint16	length = 1;
    const uint16	bps = 8;
    const uint16	photometric = PHOTOMETRIC_RGB;
    const uint16	rows_per_strip = 1;
    const uint16	planarconfig = PLANARCONFIG_CONTIG;

    static TIFFField customFields[] = {
            {TIFFTAG_IMAGEWIDTH, -1, -1, TIFF_ASCII, 0, TIFF_SETGET_ASCII, TIFF_SETGET_UNDEFINED, 
             FIELD_CUSTOM, 1, 0, (char*)"Custom1", NULL},
	        {TIFFTAG_DOTRANGE, -1, -1, TIFF_ASCII, 0, TIFF_SETGET_ASCII, TIFF_SETGET_UNDEFINED, 
             FIELD_CUSTOM, 1, 0, (char*)"Custom2", NULL}};

    static TIFFFieldArray customFieldArray = { tfiatOther, 0, 2, customFields };

	TIFF		*tif;
	unsigned char	buf[SPP] = { 0, 127, 255 };
	uint64          dir_offset = 0, dir_offset2 = 0;
	uint64          read_dir_offset = 0, read_dir_offset2 = 0;
	uint64          *dir_offset2_ptr = NULL;
	char           *ascii_value;
	uint16          count16 = 0;


	/* We write the main directory as a simple image. */
	tif = TIFFOpen(filename, "w+");
	if (!tif) {
		fprintf (stderr, "Can't create test TIFF file %s.\n", filename);
		return 1;
	}

	if (!TIFFSetField(tif, TIFFTAG_IMAGEWIDTH, width)) {
		fprintf (stderr, "Can't set ImageWidth tag.\n");
		goto failure;
	}
	if (!TIFFSetField(tif, TIFFTAG_IMAGELENGTH, length)) {
		fprintf (stderr, "Can't set ImageLength tag.\n");
		goto failure;
	}
	if (!TIFFSetField(tif, TIFFTAG_BITSPERSAMPLE, bps)) {
		fprintf (stderr, "Can't set BitsPerSample tag.\n");
		goto failure;
	}
	if (!TIFFSetField(tif, TIFFTAG_SAMPLESPERPIXEL, SPP)) {
		fprintf (stderr, "Can't set SamplesPerPixel tag.\n");
		goto failure;
	}
	if (!TIFFSetField(tif, TIFFTAG_ROWSPERSTRIP, rows_per_strip)) {
		fprintf (stderr, "Can't set SamplesPerPixel tag.\n");
		goto failure;
	}
	if (!TIFFSetField(tif, TIFFTAG_PLANARCONFIG, planarconfig)) {
		fprintf (stderr, "Can't set PlanarConfiguration tag.\n");
		goto failure;
	}
	if (!TIFFSetField(tif, TIFFTAG_PHOTOMETRIC, photometric)) {
		fprintf (stderr, "Can't set PhotometricInterpretation tag.\n");
		goto failure;
	}

	/* Write dummy pixel data. */
	if (TIFFWriteScanline(tif, buf, 0, 0) == -1) {
		fprintf (stderr, "Can't write image data.\n");
		goto failure;
	}

	if (!TIFFWriteDirectory( tif )) {
		fprintf (stderr, "TIFFWriteDirectory() failed.\n");
		goto failure;
	}

	/*
	 * Now create an EXIF directory.
	 */
	if (TIFFCreateEXIFDirectory(tif) != 0) {
		fprintf (stderr, "TIFFCreateEXIFDirectory() failed.\n" );
		goto failure;
	}

	if (!TIFFSetField( tif, EXIFTAG_SPECTRALSENSITIVITY, "EXIF Spectral Sensitivity")) {
		fprintf (stderr, "Can't write SPECTRALSENSITIVITY\n" );
		goto failure;
	}

    if (!TIFFWriteCustomDirectory( tif, &dir_offset )) {
		fprintf (stderr, "TIFFWriteCustomDirectory() with EXIF failed.\n");
		goto failure;
	}

	/*
	 * Now create a custom directory with tags that conflict with mainline
	 * TIFF tags.
	 */

	TIFFFreeDirectory( tif );
	if (TIFFCreateCustomDirectory(tif, &customFieldArray) != 0) {
		fprintf (stderr, "TIFFCreateCustomDirectory() failed.\n" );
		goto failure;
	}

	if (!TIFFSetField( tif, TIFFTAG_IMAGEWIDTH, "*Custom1")) { /* not really IMAGEWIDTH */
		fprintf (stderr, "Can't write pseudo-IMAGEWIDTH.\n" );
		goto failure;
	}

	if (!TIFFSetField( tif, TIFFTAG_DOTRANGE, "*Custom2")) { /* not really DOTWIDTH */
		fprintf (stderr, "Can't write pseudo-DOTWIDTH.\n" );
		goto failure;
	}

	if (!TIFFWriteCustomDirectory( tif, &dir_offset2 )) {
		fprintf (stderr, "TIFFWriteCustomDirectory() with EXIF failed.\n");
		goto failure;
	}

	/*
	 * Go back to the first directory, and add the EXIFIFD pointer.
	 */
	TIFFSetDirectory(tif, 0);
	TIFFSetField(tif, TIFFTAG_EXIFIFD, dir_offset );
	TIFFSetField(tif, TIFFTAG_SUBIFD, 1, &dir_offset2 );

	TIFFClose(tif);

	/* Ok, now test whether we can read written values in the EXIF directory. */
	tif = TIFFOpen(filename, "r");

	TIFFGetField(tif, TIFFTAG_EXIFIFD, &read_dir_offset );
	if( read_dir_offset != dir_offset ) {
		fprintf (stderr, "Did not get expected EXIFIFD.\n" );
		goto failure;
	}

	TIFFGetField(tif, TIFFTAG_SUBIFD, &count16, &dir_offset2_ptr );
	read_dir_offset2 = dir_offset2_ptr[0];
	if( read_dir_offset2 != dir_offset2 || count16 != 1) {
		fprintf (stderr, "Did not get expected SUBIFD.\n" );
		goto failure;
	}

	if( !TIFFReadEXIFDirectory(tif, read_dir_offset) ) {
		fprintf (stderr, "TIFFReadEXIFDirectory() failed.\n" );
		goto failure;
	}

	if (!TIFFGetField( tif, EXIFTAG_SPECTRALSENSITIVITY, &ascii_value) ) {
		fprintf (stderr, "reading SPECTRALSENSITIVITY failed.\n" );
		goto failure;
	}

	if( strcmp(ascii_value,"EXIF Spectral Sensitivity") != 0) {
		fprintf (stderr, "got wrong SPECTRALSENSITIVITY value.\n" );
		goto failure;
	}

	/* Try reading the Custom directory */

	if( !TIFFReadCustomDirectory(tif, read_dir_offset2, &customFieldArray) ) {
		fprintf (stderr, "TIFFReadCustomDirectory() failed.\n" );
		goto failure;
	}

	if (!TIFFGetField( tif, TIFFTAG_IMAGEWIDTH, &ascii_value) ) {
		fprintf (stderr, "reading pseudo-IMAGEWIDTH failed.\n" );
		goto failure;
	}

	if( strcmp(ascii_value,"*Custom1") != 0) {
		fprintf (stderr, "got wrong pseudo-IMAGEWIDTH value.\n" );
		goto failure;
	}

	if (!TIFFGetField( tif, TIFFTAG_DOTRANGE, &ascii_value) ) {
		fprintf (stderr, "reading pseudo-DOTRANGE failed.\n" );
		goto failure;
	}

	if( strcmp(ascii_value,"*Custom2") != 0) {
		fprintf (stderr, "got wrong pseudo-DOTRANGE value.\n" );
		goto failure;
	}

	TIFFClose(tif);

	/* All tests passed; delete file and exit with success status. */
	// unlink(filename);
	return 0;

failure:
	/*
	 * Something goes wrong; close file and return unsuccessful status.
	 * Do not remove the file for further manual investigation.
	 */
	TIFFClose(tif);
	return 1;
}

auto test_libtiff2([[maybe_unused]] MessageFeedbackProv& feedback_prov) -> int
{
   /* Write a test picture to a TIFF file, with thumbnail and EXIF, using libtiff.
	*
	* See: https://gitlab.com/libtiff/libtiff/-/issues/43
	*
	* Run "thumb -nocheckpoint" to write data in the order:
	*   primary image, IFD0, thumbnail image, IFD1.
	*   and most software (e.g. Mac Finder, exiftool) does not find IFD1, it seems.
	* With no args, this program uses TIFFCheckpointDirectory to write in the order I'd prefer:
	*   IFD0, IFD1, thumbnail image, primary image.
	*   But running it this way causes IFD1 to overwrite IFD0
	*   and gives an assert on the first call to TIFFSetDirectory.
	*
	* Paul Heckbert, 2012/11/15
	*/

	auto write_image = [](TIFF *tif, int nx, int ny) {
		uint8_t *buf = static_cast<uint8_t*>(_TIFFmalloc(nx*3));
		int x, y;

		assert(buf);
		/* picture has horizontal ramp in red, vertical ramp in green */
		for (y=0; y<ny; y++) {
			uint8_t *p = buf;
			for (x=0; x<nx; x++) {
				*p++ = static_cast<uint8_t>(x*255/(nx-1));  /* r */
				*p++ = static_cast<uint8_t>(y*255/(ny-1));  /* g */
				*p++ = 0;                                   /* b */
			}
			assert(TIFFWriteScanline(tif, buf, y, 0) != -1);
		}
		_TIFFfree(buf);
	};

	auto set_tags = [](TIFF *tif, int nx, int ny, int compression) {
		assert(TIFFSetField(tif, TIFFTAG_IMAGEWIDTH, nx));
		assert(TIFFSetField(tif, TIFFTAG_IMAGELENGTH, ny));
		assert(TIFFSetField(tif, TIFFTAG_BITSPERSAMPLE, 8));
		assert(TIFFSetField(tif, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_RGB));
		assert(TIFFSetField(tif, TIFFTAG_ROWSPERSTRIP, ny));  /* one strip */
		printf("set_tags %d %d %d\n", nx, ny, compression);
		assert(TIFFSetField(tif, TIFFTAG_COMPRESSION, compression));
		if (compression == COMPRESSION_LZW)
		assert(TIFFSetField(tif, TIFFTAG_PREDICTOR, PREDICTOR_HORIZONTAL));
		assert(TIFFSetField(tif, TIFFTAG_SAMPLEFORMAT, SAMPLEFORMAT_UINT));
		assert(TIFFSetField(tif, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG));
		assert(TIFFSetField(tif, TIFFTAG_SAMPLESPERPIXEL, 3));
	};

	int px = 640, py = 480;  /* primary image size */
	int tx = 160, ty = 120;  /* thumbnail image size */
	int d0, d1, d2, d3, d4, d5;
	int h0, h1, h2, h3, h4, h5;
	const char *filename0 = "thumb_0.tif";
	const char *filename1 = "thumb_1.tif";
	unlink(filename0);
	unlink(filename1);

	TIFF *tif = TIFFOpen(filename0, "w");
	assert(tif);

	set_tags(tif, px, py, COMPRESSION_LZW);
	/* write IFD0 (preliminary version) and create an empty IFD1 */
	/* 
		* That is not true. TIFFCheckpointDirectory() does not create a new or empty IFD,
		* it only writes or updates the IFD in the file.
		* To create a new and fresh IFD TIFFCreateDirectory() - clears only internal storage -
		* or TIFFWriteDirectory() - writes current IFD to file and clears internal storage -
		* is needed. 
		*/
	h0 = TIFFNumberOfDirectories(tif);      //debugging  h0=0 
	d0 = TIFFCurrentDirectory(tif);         //debugging  d0=65535  - not yet specified -
	/* h0=0; d0=35535: currently there is no IFD in the file (only in internal storage) */
	assert(TIFFCheckpointDirectory(tif));   //writes IFD to file
	/* Now, there is the first IFD0 in file. */
	h1 = TIFFNumberOfDirectories(tif);      //debugging h1=1 because IFD0 is written to file
	d1 = TIFFCurrentDirectory(tif);         //debugging d1=0 
	/*
		* The IFD1 has to be created first, i.e. the internal storage of the old IFD0 has to be cleared. 
		* This is performed with TIFFCreateDirectory() 
		*/
	assert(!TIFFCreateDirectory(tif));
	h2 = TIFFNumberOfDirectories(tif);      //debugging h2=1, because IFD1 is only internal so far.
	d2 = TIFFCurrentDirectory(tif);         //debugging d2=0
	set_tags(tif, tx, ty, COMPRESSION_NONE);
	/* Write IFD1 (preliminary version) to file.
		* Then, there are two IFDs.
		*/
	assert(TIFFCheckpointDirectory(tif));
	h3 = TIFFNumberOfDirectories(tif);      //debugging h3=2
	d3 = TIFFCurrentDirectory(tif);         //debugging d3=1 because IFD1 is written to file.
	/* 
		* TIFFSetDirectory() reads the given IFD from file into internal storage. 
		* This is not necessary here, because IFD1 is still current.
		*/
	assert(TIFFSetDirectory(tif, 1));
	/* write pixels of thumbnail */
	write_image(tif, tx, ty);
	/* 
		* TIFFWriteDirectory() writes IFD1 to file and clears internal storage.
		* Then, there are two IFDs, because TiffWriteDirectory() does setup a new IFD, 
		* where TIFFCheckpointDirectory() does not 
		*/
	assert(TIFFWriteDirectory(tif));
	h4 = TIFFNumberOfDirectories(tif);      //debugging h4=2 
	d4 = TIFFCurrentDirectory(tif);         //debugging d4=2
	/* set current dir to IFD0 */
	assert(TIFFSetDirectory(tif, 0));
	h5 = TIFFNumberOfDirectories(tif);      //debugging h5=2
	d5 = TIFFCurrentDirectory(tif);         //debugging d5=0
	/* write pixels of primary image */
	write_image(tif, px, py);               //writes primary image to file

	/* 
		* TIFFClose() causes TIFFWriteDirectory() to be called. 
		* It updates IFD0 section in the file. 
		* However, if IFD0 size would have been changed, 
		* a new IFD0 would be written at the end of the file.
		* An image viewer discovers now two images in the file.
		*/
	TIFFClose(tif);


	tif = TIFFOpen(filename1, "w");
	assert(tif);

	d0 = TIFFCurrentDirectory(tif);         //debugging  d0=65535  - not yet specified -
	/* set_tags() does not write IFD to file, but keeps it internal. */
	set_tags(tif, px, py, COMPRESSION_LZW);
	/* Writes pixels of primary image to file, because too much to keep internal. */
	write_image(tif, px, py);
	/* 
	* Now IFD0 is written to file by TIFFWriteDirectory() after the image pixels.
	*/
	assert(TIFFWriteDirectory(tif));
	h1 = TIFFNumberOfDirectories(tif);      //debugging  h1=1
	d1 = TIFFCurrentDirectory(tif);         //debugging  d1=0
	/*
		* Because no new and cleared IFD is created,
		* set_tags() does overwrite internally stored IFD0 data, 
		* and a dirty-flag is set, which marks the old IDF0 in file as invalid.
		*/
	set_tags(tif, tx, ty, COMPRESSION_NONE);
	h2 = TIFFNumberOfDirectories(tif);      //debugging  h2=1
	d2 = TIFFCurrentDirectory(tif);         //debugging  d2=0
	/* Write pixels of thumbnail (appended) to file, 
		* after the meanwhile invalid IFD0, written before.
		*/
	write_image(tif, tx, ty);
	h3 = TIFFNumberOfDirectories(tif);      //debugging  h3=1
	d3 = TIFFCurrentDirectory(tif);         //debugging  d3=0

	/* TIFFClose() causes TIFFWriteDirectory to be called. 
	* The updated IFD0 of the thumbnail is now written at the end of file,
	* and the previously written IFD0 in the file (together with the previously
	* written image pixels) is made invalid. 
	* Thus, an image viewer discovers only one image.
	*/
	TIFFClose(tif);                           // updated IFD0 is written to file at the end.
	return 0;
}

auto test_libtiff3(const fs::path& tif_path, twist::MessageFeedbackProv& feedback_prov) -> int
{
	TIFF *tiff;

	/* Define the number of pages/images (main-IFDs) you are going to write */
	int number_of_images = 3;

/* Define the number of sub - IFDs you are going to write */
#define NUMBER_OF_SUBIFDs 2

	int number_of_sub_IFDs = NUMBER_OF_SUBIFDs;
	toff_t sub_IFDs_offsets[NUMBER_OF_SUBIFDs] = {
		0UL}; /* array for SubIFD tag */
	int blnWriteSubIFD = 0;
	
	if (!(tiff = TIFFOpen(tif_path.string().c_str(), "w")))
		return 1;
	
	for (int i = 0; i < number_of_images; i++)
	{
		char pixel[1] = {static_cast<char>(128)};
	
		TIFFSetField(tiff, TIFFTAG_IMAGEWIDTH, 1);
		TIFFSetField(tiff, TIFFTAG_IMAGELENGTH, 1);
		TIFFSetField(tiff, TIFFTAG_SAMPLESPERPIXEL, 1);
		TIFFSetField(tiff, TIFFTAG_BITSPERSAMPLE, 8);
		TIFFSetField(tiff, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_MINISBLACK);
	
		/* For the last but one multi-page image, add a SubIFD e.g. for a
		* thumbnail */
		if (number_of_images - 2 == i)
			blnWriteSubIFD = 1;
	
		if (blnWriteSubIFD)
		{
		   /* Now here is the trick: the next n directories written
			* will be sub-IFDs of the main-IFD (where n is number_of_sub_IFDs
			* specified when you set the TIFFTAG_SUBIFD field.
			* The SubIFD offset array sub_IFDs_offsets is filled automatically
			* with the proper offset values by the following number_of_sub_IFDs
			* TIFFWriteDirectory() calls and updated in the related main-IFD
			* with the last call.
			*/
			if (!TIFFSetField(tiff, TIFFTAG_SUBIFD, number_of_sub_IFDs,
							  sub_IFDs_offsets))
				return 1;
		}
	
		/* Write dummy pixel to image */
		if (TIFFWriteScanline(tiff, pixel, 0, 0) < 0)
			return 1;
		/* Write image / directory to file */
		if (!TIFFWriteDirectory(tiff))
			return 1;
	
		if (blnWriteSubIFD)
		{
		   /* A SubIFD tag has been written for that main-IFD and this
			* triggers that pervious TIFFWriteDirectory() to switch to the
			* SubIFD-chain for the next number_of_sub_IFDs writings.
			* Thus, only the thumbnail images need to be
			* set up and written to file using TIFFWriteDirectory().
			* The last of this TIFFWriteDirectory() calls will setup
			* the next fresh main-IFD.
			*/
			for (int j = 0; j < number_of_sub_IFDs; j++)
			{
				TIFFSetField(tiff, TIFFTAG_IMAGEWIDTH, 1);
				TIFFSetField(tiff, TIFFTAG_IMAGELENGTH, 1);
				TIFFSetField(tiff, TIFFTAG_SAMPLESPERPIXEL, 1);
				TIFFSetField(tiff, TIFFTAG_BITSPERSAMPLE, 8);
				TIFFSetField(tiff, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_MINISBLACK);
				/* SUBFILETYPE tag is not mandatory for SubIFD writing, but a
				* good idea to indicate thumbnails */
				if (!TIFFSetField(tiff, TIFFTAG_SUBFILETYPE,
								  FILETYPE_REDUCEDIMAGE))
					return 1;
	
				/* Write dummy pixel to thumbnail image */
				pixel[0] = 64;
				if (TIFFWriteScanline(tiff, pixel, 0, 0) < 0)
					return 1;
				/* Writes now in the SubIFD chain */
				if (!TIFFWriteDirectory(tiff))
					return 1;
	
				blnWriteSubIFD = 0;
			}
		}
	}
	TIFFClose(tiff);
	return 0;
}

}

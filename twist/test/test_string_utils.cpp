///  @file  "test_string_utils.cpp"
///  Implementation file for "test_string_utils.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "test_string_utils.hpp"

#include "twist/string_utils.hpp"

#include "test_globals.hpp"

namespace twist::test {

TEST_CASE("Test for trim functions in \"string_utils.hpp/cpp/ipp\"", "[twist]") 
{
	static const auto str1 = L"Bob'\"";  // To get around weird VS2017 compiler bug

	const std::wstring text{ L"\"'Bob'\"" };
	REQUIRE( trim_lead_char(trim_lead_char(text, L'\"'), L'\'') == str1 );
	REQUIRE( trim_trail_char(trim_trail_char(text, L'\"'), L'\'') == L"\"'Bob" );
	REQUIRE( trim_char(trim_char(text, L'\"'), L'\'') == L"Bob" );
}


void test_trim([[maybe_unused]] MessageFeedbackProv& feedback_prov)
{

}

}

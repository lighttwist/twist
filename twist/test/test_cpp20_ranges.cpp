///  @file  "test_cpp20_ranges.cpp"
///  Implementation file for "test_cpp20_ranges.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "test_cpp20_ranges.hpp"

#include "twist/feedback_providers.hpp"

#include <algorithm>
#include <iostream>
#include <ranges>

namespace twist::test {

void test_cpp20_ranges1([[maybe_unused]] MessageFeedbackProv& feedback_prov)
{
	auto v1 = std::vector{3, 6, 1, 8, 12, 5};

	auto s = std::wstring{};
	rg::for_each(v1, [&](auto x) { s += std::to_wstring(x); });

	feedback_prov.set_prog_msg(s);

	auto v2 = std::vector{42};
	rg::copy(v1, back_inserter(v2));

	auto ss = std::wstringstream{};
	ss << L"v2 = ";
	for (auto x : v2) {
		ss << x << L" ";
	}
	feedback_prov.set_prog_msg(ss.str());

	ss.str(L"");
	ss << L"r1 = ";
	auto r1 = vw::iota(0, 4);
	for (auto x : r1) {
		ss << x << L" ";
	}
	feedback_prov.set_prog_msg(ss.str());

	auto r2 = vw::iota(0, 4)
			| vw::transform([](int i) { return std::wstring(i, char(L'a' + i)); })
			| vw::transform([](const std::wstring& s) { return L"bla_" + s; });
//			| vw::join(L'-');
	
	auto v3 = std::vector<std::wstring>{};
	rg::copy(r2, back_inserter(v3));
	ss.str(L"");
	ss << L"v3 = ";
	for (auto x : v3) {
		ss << x << L" ";
	}
	feedback_prov.set_prog_msg(ss.str());


	// view::cache1
/*
	auto rng = vw::iota(0,4)
			| vw::transform([](int i) { return std::string(i, char('a'+i)); })
			| vw::cache1
			| vw::join('-');

	std::vector<char> dest;
	for (auto el : rng) {
		dest.push_back(el);
	}

//	check_equal(rng, {'-','b','-','c','c','-','d','d','d'});
	rg::copy(rng, back_inserter(dest));

	auto dest3 = rg::to<std::vector>(rng);
*/
}

}


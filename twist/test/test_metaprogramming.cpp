///  @file  test_metaprogramming.cpp
///  Implementation file for "test_metaprogramming.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "test_metaprogramming.hpp"

#include <array>
#include <iterator>

#include "twist/array_utils.hpp"
#include "twist/MetaBiMapConstvalType.hpp"
#include "twist/metaprogramming.hpp"
#include "twist/meta_misc_traits.hpp"
#include "twist/gis/gis_globals.hpp"

#pragma warning(disable: 4100)

using namespace twist::gis;

namespace twist::test {

template<class T>
class HasWstringCtor1 {

	using Yes = char(&)[1];
	using No  = char(&)[2]; 
	
	template<class U, class = decltype(U{ std::wstring{} })> 
	static Yes tesst(void*);

	template<class> static No tesst(...);

public:
	static constexpr bool value = sizeof(tesst<T>(nullptr)) == sizeof(Yes);
};

template<class T>
class HasWstringCtor2 {
	template<class U, class = decltype(U{ std::wstring{} })> 
	static std::true_type tesst(void*);

	template<class> static std::false_type tesst(...);

public:
	using type = decltype(tesst<T>(nullptr));

	static constexpr bool value = type::value;
};

struct A {
	A(std::wstring)
	{
	}
};


struct B {
	B()
	{
	}
};


void test_type_traits1()
{
	static_assert(HasWstringCtor2<A>::value);
//	static_assert(HasWstringCtor2<B>::value);
}


template<class T> void do_templated_stuff()
{

}


void test_meta1()
{
	do_templated_stuff<double>();
}

void test_meta3()
{
}

/********************  test_is_valid_lambda_decl1  **********************/

template<class D>
class Bass {
public:
	Bass()
	{
		constexpr auto has_what_i_want1 = is_valid_lambda_decl([](auto d) -> decltype(
			//    &decltype(type_from_val(d))::bass_wants_me_public
				  &decltype(type_from_val(d))::bass_wants_me_private
		) {});

		constexpr auto has_what_i_want2 = is_valid_lambda_decl([](auto d) -> decltype(
				  std::wstring{ type_from_val(d).bass_wants_me_private() }
		) {});

		static_assert(has_what_i_want1(type_to_val<D>));
		static_assert(has_what_i_want2(type_to_val<D>));
	}
};

class Deriv {
public:
	void bass_wants_me_public() {}
private:
	std::wstring bass_wants_me_private() { return L"gog"; }

	template<class> friend class Bass;
};


void test_is_valid_lambda_decl1()
{
	Bass<Deriv> bb;	
	Deriv dd;

	using DerivMem  = decltype(&Deriv::bass_wants_me_public);
	using DerivMem2 = decltype(&decltype(dd)::bass_wants_me_public);

//	using DerivMem3 = decltype(&decltype(dd)::bass_wants_me_private);
}

/***********************************  test_meta2()  **************************************/

using GeoGridDataValueTypeMetaMap = MetaBiMapConstvalType<
		MetaMapConstvalList<
				GeoGridDataType, GeoGridDataType::uint16, GeoGridDataType::int32, 
		        GeoGridDataType::float32, GeoGridDataType::float64>, 
		MetaMapTypeList<
				uint16_t, int32_t, float, double>>;  

template<GeoGridDataType val> 	
using GeoGridDataValueTypeFromEnum = MetaMapFindTypeForConstval<GeoGridDataValueTypeMetaMap, val>;

template<class Type>
constexpr GeoGridDataType geo_grid_data_type_enum_for_type = 
		meta_map_find_constval<GeoGridDataValueTypeMetaMap, Type>();

void test_meta2()
{
	using Fl2 = GeoGridDataValueTypeFromEnum<GeoGridDataType::float32>;
	static_assert(std::is_same_v<Fl2, float>);

	constexpr auto enumval = geo_grid_data_type_enum_for_type<double>;
	static_assert(enumval == GeoGridDataType::float64);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  This should work but does not

template<class T, T... values>
struct ValSequence {

	using Type = T;

	template<class U> 
	using AsIntegerSequence = std::integer_sequence<U, static_cast<U>(values)...>;  // doesn't work
};

enum class Animal { pig = 101, cow = 6, donkey = 24 };

using Enums = ValSequence<Animal, Animal::pig, Animal::cow, Animal::donkey>; 

// should work, does not
// using SizeTs = Enums::AsIntegerSequence<std::size_t>;
// static_assert(std::is_same_v<SizeTs, std::integer_sequence<std::size_t, 101, 6, 24>>);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

using UnorderedInts = std::integer_sequence<int, 2, 6, 9, 17, 15>;
using OrderedInts   = std::integer_sequence<int, 2, 6, 9, 17, 25>;


//static_assert(is_increasing(UnorderedInts{}));
static_assert(is_strictly_increasing(OrderedInts{}));
static_assert(binary_search(OrderedInts{}, 17) == 3);

using Map1 = MetaBiMapConstvalType<
		MetaMapConstvalList<
				GeoGridDataType, GeoGridDataType::int8, GeoGridDataType::uint8, 
				GeoGridDataType::int16, GeoGridDataType::uint16, GeoGridDataType::int32,
				GeoGridDataType::uint32, GeoGridDataType::float32, GeoGridDataType::float64>, 
		MetaMapTypeList<
				int8_t, uint8_t, int16_t, uint16_t, int32_t, uint32_t, float, double>>;  

static_assert(std::is_same_v<Map1::FindType<GeoGridDataType::uint32>, uint32_t>);
constexpr auto cval = Map1::find_constval<float>();
static_assert(cval == GeoGridDataType::float32);
static_assert(Map1::contains_constval<GeoGridDataType::uint16>());
static_assert(!Map1::contains_constval<GeoGridDataType::comp_uint8_uint16>());
static_assert(Map1::contains_type<uint16_t>());
static_assert(!Map1::contains_type<bool>());

using Seq1 = MetaConstvalSequence<GeoGridDataType, GeoGridDataType::int8, 
				GeoGridDataType::uint8, GeoGridDataType::int16, GeoGridDataType::uint16, 
				GeoGridDataType::int32, GeoGridDataType::int32, GeoGridDataType::uint32>;


static_assert(is_increasing(Seq1{}));
static_assert(binary_search(Seq1{}, GeoGridDataType::uint16) == 3);

using Map2 = MetaBiMapConstvalType<
		std::integer_sequence<
				int, 1, 2, 3, 4, 5, 8, 13, 19>, 
		MetaMapTypeList<
				int8_t, uint8_t, int16_t, uint16_t, int32_t, uint32_t, float, double>>;

// static_assert(std::is_same_v<Map2::FindType<5>, int32_t>);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////




template<class C>
struct GetSize {
	static_assert(always_false<C>, "C needs to be a specialisation of MetaConstvalSequence");
};

template<class T, T... constvals>
struct GetSize<MetaConstvalSequence<T, constvals...>> {

	using C = MetaConstvalSequence<T, constvals...>;

	static constexpr size_t value = C::size(); 
};

//constexpr auto aa = GetSize<std::integer_sequence<int, 2, 6, 8, 0>>::value;
constexpr auto bb = GetSize<MetaConstvalSequence<int, 2, 6, 8, 0, -2>>::value;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

template<class T, int v>
class Bob {
public:
	Bob(std::wstring text);

	std::wstring text() const;

private:
	std::wstring  text_;
};


template<class T, int v>
Bob<T, v>::Bob(std::wstring text)
	: text_{ move(text) }
{
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

void test_out_iter_trait()
{
	std::vector<double> vd;
	std::vector<float> vf;

	double ad[] = { 0.3, 0.4 };
	float af[] = { 0.6f, 0.7f };
	float af_const[] = { 0.6f, 0.7f };

	using T1 = decltype( std::back_inserter(vd) );
	using T2 = double* ;

	using U1 = decltype(++(std::declval<T1>()));

	using G = decltype(detail::as_lvalue_ref<T2>());

	[[maybe_unused]] constexpr auto xx1 = HasPreincrement<T1>::value;
	[[maybe_unused]] constexpr auto xx2 = HasPreincrement<T2>::value;
	static_assert(xx1);
//	static_assert(xx2);

//	using U2A = decltype(++(std::declval<T2>()));
//	using U2B = decltype(++get_ref<T1>());

	[[maybe_unused]] constexpr auto yy1 = detail::IsOutIteratorImpl<T1, double>::value;
	[[maybe_unused]] constexpr auto yy2 = detail::IsOutIteratorImpl<T2, float>::value;
	static_assert(yy2);


//	double* ad_it = ad;

	copy(vf, std::back_inserter(vd));
//	copy(af, ad);

	//double* ad_const = ad;
	//copyx(af, ad_const);


}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

namespace detail {

template<class T, class U, class = std::void_t<>>
struct AreEqComparableImpl
    : std::false_type {};

template<class T, class U>
struct AreEqComparableImpl<T, U, std::void_t<
		decltype(bool{ std::declval<T>() == std::declval<U>() }),
		decltype(bool{ std::declval<U>() == std::declval<T>() })>>
	: std::true_type {};

}

template<class T, class U>
using EnableIfEqComparable = std::enable_if_t<detail::AreEqComparableImpl<T, U>::value, int>;

template<class Rng, 
         class Val,
		 class = EnableIfEqComparable<RangeElementCref<Rng>, const Val&>>
auto findx(const Rng& range, const Val& value)
{
	return std::find(begin(range), end(range), value);	
}


class Ty1 {
public:
bool operator==(const Ty1&) const
{
	return true;
}
};


void fnk(bool)
{
}

void test_eq_comp()
{
	std::vector<std::string> vs;
	findx(vs, std::string{});

	std::vector<Ty1> vb;
	findx(vb, Ty1{});

	int i = 9;
	fnk(i);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

void test_have_less_than()
{
	using T1 = int;
	using T2 = float;
	using T3 = std::wstringstream;

	[[maybe_unused]] int a{};
	[[maybe_unused]] int b{};
	std::wstringstream x{}, y{};

	constexpr bool b1 = twist::detail::HaveLessThanImpl<T2, T2>::value;
	static_assert(b1);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

//template<class T, class = std::enable_if_t<!std::is_pointer_v<T>, T>> 
//T internal_to_user_dataxx(unsigned long data)
//{
//	return static_cast<T>(data);
//}
//
//
///// Overload for user data types which are pointer types.
///// Compile error: "Already declared"
//template<class T, class = std::enable_if_t<std::is_pointer_v<T>, T>>
//T internal_to_user_dataxx(unsigned long data)
//{
//	return reinterpret_cast<T>(data);
//}



}

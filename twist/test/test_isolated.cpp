///  @file  test_isolated.cpp
///  Test code isolated from the rest of the codebase 

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include <algorithm>
#include <iterator>
#include <list>
#include <numeric>
#include <stdexcept>
#include <vector>

namespace twist::test {

template<typename Iter>
class RangeX {
public: 
	using self_type = RangeX<Iter>;
	using const_iterator = Iter;
	using iterator = Iter;
	using value_type = typename std::iterator_traits<Iter>::value_type;
	using difference_type = typename std::iterator_traits<Iter>::difference_type;

	/// Constructor.
	///
	/// @param[in] first  Iterator addressing the first element in the range
	/// @param[in] last  Iterator addressing one past the final element in the range
	///
	RangeX(Iter first, Iter last) 
		: first_{ first }
		, last_{ last }
	{
		using IterCategory = typename std::iterator_traits<Iter>::iterator_category;
		if constexpr (std::is_same_v<IterCategory, std::random_access_iterator_tag>) {
			if (first > last) {
				throw std::runtime_error{ "The first iterator must come before the last." };
			}
		}
	}

	/// Constructor; creates a range spanning all elements of a container, from beginning to end.
	///
	/// @tparam  Cont  The container type
	/// @param[in] cont  The container
	///
	template<class Cont> 
	explicit RangeX(const Cont& cont) 
		: first_{ begin(cont) }
		, last_{ end(cont) }
	{
	}

	/// Get the iterator addressing the first element in the range.
	///
	/// @return  The iterator
	///
	[[nodiscard]] Iter begin() const 
	{
		return first_;
	}

	/// Get the iterator addressing one past the final element in the range.
	/// If the range is empty, this function retruns a iterator equal to that returned by end().
	///
	/// @return  The iterator
	///
	[[nodiscard]] Iter end() const 
	{
		return last_;
	}

	/// Find out whether the range is empty (contains no elements).
	///
	/// @return  true if empty
	///
	[[nodiscard]] bool empty() const 
	{
		return first_ == last_;
	}

	/// Find out the size of the range (the number of elements it contains).
	///
	/// @return  The range size
	///
	std::size_t size() const 
	{
		return std::distance(first_, last_);
	}

private:
	Iter  first_;
	Iter  last_;
};


/// Range-based version of std::iota()
template<class Cont, class T>
void iotax(Cont& cont, T val)
{
	std::iota(begin(cont), end(cont), val);
}


void test_isolated1()
{
	std::list<int> l{ 4, 8, 3, 5 };
	std::vector<int> v{ 4, 8, 3, 5 };
	int a[]{ 4, 8, 3, 5 };

	RangeX rng1{ begin(v) , end(v) };
	RangeX rng2{ std::cbegin(v), std::cend(v) };
	RangeX rng3{ std::cend(v), std::cbegin(v) };
	RangeX rng4{ a , a + 4 };
	RangeX rng5{ a + 4, a };

	iotax(rng1, 0);

}

}

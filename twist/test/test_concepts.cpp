///  @file  "test_concepts.cpp"
///  Implementation file for "test_concepts.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "test_concepts.hpp"

#include <concepts>

#pragma warning(disable: 4101)
#pragma warning(disable: 4189)

//  https://akrzemi1.wordpress.com/2016/09/21/concepts-lite-vs-enable_if/
//  https://en.cppreference.com/w/cpp/language/constraints

namespace twist::test {

struct NonMovable {
	NonMovable(NonMovable&&) = delete;
};
 
template<class T>
concept IsNumeric = std::is_integral_v<T> || std::is_floating_point_v<T>;

template<class T>
concept IsMultiplicative = requires(T const a, T const b) {
    { a * b } -> std::convertible_to<T>;
};

template<typename T>
class Wrapper {
public:
	Wrapper() = default;

	Wrapper(const Wrapper&) requires IsNumeric<T> = default;
};


template<class T> requires std::is_integral_v<T>
class Numero1 {

};


template<class T> requires std::is_floating_point_v<T>
class Numero2 {

};

template<IsNumeric T>
T product(T t1, T t2)
{
	return t1 * t2;
}


void test_concepts1()
{
	using namespace std::string_literals;
	
	static_assert(std::movable<std::string>);
	static_assert(!std::movable<NonMovable>);

	auto p1 = product(2.34, 1.45);
	auto p2 = product<double>(2.34, 1.45f);
//	auto p4 = product("bob"s, "rob"s);

	Wrapper<int> w1;
	Wrapper<int> w2{ w1 };
	Wrapper<std::string> w3;
//	Wrapper<std::string> w4{ w3 };

	std::vector<int> l = { 3, -1, 10 };
//	std::set<int> l = { 3, -1, 10 };
	rg::sort(l); 
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

struct NoBasse {
};
struct Basse {
};
struct Derivved : Basse {
};

template<class D, class B>
concept DerivedConcept = std::is_base_of<B, D>::value;
 
template<DerivedConcept<Basse> T>
void funko(T)  // T is constrained by Derived<T, Basse>
{

}

void test_concepts2()
{
	Derivved d;
	funko(d);
}

}

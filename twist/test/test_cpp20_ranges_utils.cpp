//  Inline implementation file for "test_index_range.h"
//
//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,  
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/test/test_cpp20_ranges_utils.hpp"

#include "twist/cpp20_ranges_utils.hpp"
#include "twist/test/test_globals.hpp"

namespace twist::test {

auto test_slice() -> void
{

}

TEST_CASE("Test for twist::slice()", "[twist]") 
{
	auto r1 = {-4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6};

	auto s1r1 = {-3, -2, -1, 0, 1};

	REQUIRE(equal_same_size(r1 | slice(1, 5), s1r1));
	REQUIRE(equal_same_size(r1 | slice(twist::math::IntegerInterval{1, 5}), s1r1));

	auto s2r1 = {4, 5, 6};

	REQUIRE(equal_same_size(r1 | slice(8, 3), s2r1));
	REQUIRE(equal_same_size(r1 | slice(twist::math::IntegerInterval{8, 10}), s2r1));

	REQUIRE(equal_same_size(r1 | slice(8, 20), s2r1));
	REQUIRE(equal_same_size(r1 | slice(twist::math::IntegerInterval{8, 27}), s2r1));

	//auto r2 = {0, 1, 2, 3, 4, 5, 6, 7, 8};
	//assert(equal_same_size(IndexRange{9}, r2));

	//auto r3 = {2u, 3u, 4u, 5u, 6u, 7u};
	//assert(equal_same_size(IndexRange{size_t{2}, size_t{8}}, r3));

	//auto empty_range = IndexRange{6, 6};
	//assert(std::begin(empty_range) == std::end(empty_range));
}

} 

/// @file test_globals.hpp
/// Globals for the "twist::test" namespace

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE

#ifndef TWIST_TEST_GLOBALS_HPP
#define TWIST_TEST_GLOBALS_HPP

#include "twist/globals.hpp"
#include "twist/test/LibraryTestDataManagerBase.hpp"

#include <catch2/catch_approx.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_floating_point.hpp>

namespace twist::test {

class TwistTestDataManager : public LibraryTestDataManagerBase<TwistTestDataManager> {
public:
	/*! Path to the root directory of the "twist" sublibrary (ie the code which is in the root twist source directory)
	    data.
	*/
	[[nodiscard]] auto twist_test_data_root_dir() const -> fs::path;

	//! Path to the root directory of the "twist::db" sublibrary data.
	[[nodiscard]] auto twist_db_test_data_root_dir() const -> fs::path;

	//! Path to the root directory of the "twist::img" sublibrary data.
	[[nodiscard]] auto twist_img_test_data_root_dir() const -> fs::path;

	//! Path to the root directory of the "twist::gis" sublibrary data.
	[[nodiscard]] auto twist_gis_test_data_root_dir() const -> fs::path;

private:
	friend class Singleton<TwistTestDataManager>;

	/*! Constructor.
	    \param[in] lib_data_root_dir_path  Path to the root directory of the "twist" library test data; if the 
		                                   directory does not exist, an exception is thrown
	*/
	explicit TwistTestDataManager(const fs::path& lib_data_root_dir_path);
};

// --- Macros ---

#define TWIST_TEST_FEEDBACK_FUNC_SUCCESS \
			feedback_prov.set_prog_msg(twist::ansi_to_string(std::format("Test function {}() finished successfully.", \
			                                                             TWIST_FUNCTION_NAME)));
}

#endif

///  @file  "test_twist_ranges.cpp"
///  Implementation file for "test_twist_ranges.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "twist/twist_maker.hpp"

#include "test_twist_ranges.hpp"

#include <list>
#include <queue>

#include "twist/feedback_providers.hpp"
#include "twist/ranges.hpp"
#include "twist/type_info_utils.hpp"
#include "twist/db/SqliteSelector.hpp"
#include "twist/math/UnivarDistrSample.hpp"

#include "test_globals.hpp"

namespace twist::test {

TEST_CASE("Test for functions in \"ranges.hpp/ipp\"", "[twist]") 
{
	std::vector<int> r1{ 12, 18, 9, 28, 0, 3 };
	std::set<int>    r2{ 12, 18, 9, 28, 0, 3 };
	std::list<int>   r3{ 12, 18, 9, 28, 0, 3 };
	std::vector<int> r4{ 12, 18, 9, 28, 4, 3 };

	REQUIRE( compare_ranges(r1, r2) == RangeCompResult::permutation );
	REQUIRE( compare_ranges(r1, r3) == RangeCompResult::identical );
	REQUIRE( compare_ranges(r1, r4) == RangeCompResult::different );

	auto it = find_nth_if(r1, 2, [](auto x) { return x < 10; });
	REQUIRE( it - begin(r1) == 5 );

	it = find_nth_if(r1, 2, [](auto x) { return x > 10; });
	REQUIRE( it - begin(r1) == 3 );

	it = find_nth_if(r1, 0, [](auto x) { return x > 20; });
	REQUIRE( it - begin(r1) == 3 );

	it = find_nth_if(r1, 1, [](auto x) { return x > 20; });
	REQUIRE( it == end(r1) );

	it = find_nth_if(r1, 1, [](auto x) { return x > 20; });
	REQUIRE( it == end(r1) );
}


template<typename Iter>
class RangeX {
public: 
	using self_type = RangeX<Iter>;
	using const_iterator = Iter;
	using iterator = Iter;
	using value_type = typename std::iterator_traits<Iter>::value_type;
	using difference_type = typename std::iterator_traits<Iter>::difference_type;

	/// Constructor.
	///
	/// @param[in] first  Iterator addressing the first element in the range
	/// @param[in] last  Iterator addressing one past the final element in the range
	///
	RangeX(Iter first, Iter last) 
		: first_{ first }
		, last_{ last }
	{
		using IterCategory = typename std::iterator_traits<Iter>::iterator_category;
		if constexpr (std::is_same_v<IterCategory, std::random_access_iterator_tag>) {
			if (first > last) {
				TWIST_THROW(L"The first iterator must come before the last.");
			}
		}
	}

	/// Constructor; creates a range spanning all elements of a container, from beginning to end.
	///
	/// @tparam  Cont  The container type
	/// @param[in] cont  The container
	///
	template<class Cont> 
	explicit RangeX(const Cont& cont) 
		: first_{ begin(cont) }
		, last_{ end(cont) }
	{
	}

	/// Get the iterator addressing the first element in the range.
	///
	/// @return  The iterator
	///
	[[nodiscard]] Iter begin() const 
	{
		return first_;
	}

	/// Get the iterator addressing one past the final element in the range.
	/// If the range is empty, this function retruns a iterator equal to that returned by end().
	///
	/// @return  The iterator
	///
	[[nodiscard]] Iter end() const 
	{
		return last_;
	}

	/// Find out whether the range is empty (contains no elements).
	///
	/// @return  true if empty
	///
	[[nodiscard]] bool empty() const 
	{
		return first_ == last_;
	}

	/// Find out the size of the range (the number of elements it contains).
	///
	/// @return  The range size
	///
	std::size_t size() const 
	{
		return std::distance(first_, last_);
	}

private:
	Iter  first_;
	Iter  last_;
};


/// Range-based version of std::iota()
template<class Cont, class T>
void iotax(Cont& cont, T val)
{
	using namespace std;
	std::iota(begin(cont), end(cont), val);
}


void test_range_class()
{
	std::list<int> l{ 4, 8, 3, 5 };
	std::vector<int> v{ 4, 8, 3, 5 };
	int a[]{ 4, 8, 3, 5 };

	RangeX rng1{ begin(v) , end(v) };
	RangeX rng2{ cbegin(v) , cend(v) };
	RangeX rng3{ cend(v) , cbegin(v) };
	RangeX rng4{ a , a + 4 };
	RangeX rng5{ a + 4, a };

	iotax(rng1, 0);

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

class RangeParam {
public:
	template<class Rng> 
	RangeParam(const Rng& range)
	{
	}

private:
	
};

template<class Rng>
void func_taking_range_param_impl(const Rng& range, MessageFeedbackProv& feedback_prov)
{
	std::wstring message;
	for (const auto& s : range) {
		message += s;
		message += L"  ";
	}
	feedback_prov.set_prog_msg(message);
}


template<class Rng>
void func_taking_range_param(const Rng& range, MessageFeedbackProv& feedback_prov)
{
	func_taking_range_param_impl(range, feedback_prov);
}


void func_taking_range_param(std::initializer_list<std::wstring> range, MessageFeedbackProv& feedback_prov)
{
	func_taking_range_param_impl(range, feedback_prov);
}


class StringRange {
public:
	using Citer = std::vector<std::wstring>::const_iterator;

	void add(std::wstring elem) 
	{
		elems_.push_back(move(elem));
	}

	Citer begin() const
	{
		return elems_.begin();
	}

	Citer end() const
	{
		return elems_.end();
	}

private:
	std::vector<std::wstring>  elems_{};
};


void test_range_param(MessageFeedbackProv& feedback_prov)
{
	int x;
	const float* y;
	std::wstringstream z;

	const auto xtyn = type_wname<decltype(x)>();
	const auto ytyn = type_wname<decltype(y)>();
	const auto ztyn = type_wname<decltype(z)>();


	StringRange rng1;
	rng1.add(L"oake");
	rng1.add(L"omide");

	auto rng2 = { L"loulou", L"balou" };

	func_taking_range_param(rng1, feedback_prov);
	
	func_taking_range_param(rng2, feedback_prov);

	func_taking_range_param({ L"koke", L"poke" }, feedback_prov);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

void test_range_element_metafunc()
{
	auto rng = { std::wstring{ L"bob" }, std::wstring{ L"rob" } };
	using Rng = decltype(rng);

	using Vec = std::vector<std::wstring>;
	Vec vec{ std::wstring{ L"knob" }, std::wstring{ L"fob" } };

	using Ty1 = RangeElement<Rng>;
	using Ty2 = RangeElement<Vec>;
	static_assert(std::is_same_v<Ty1, std::wstring>);
	static_assert(std::is_same_v<Ty2, std::wstring>);
}

/// Range-based version of std::fill()
template<class Rng, 
         class Val,
		 class = EnableIfRangeTakes<Rng, Val>>
void fillx(Rng& range, const Val& value)
{
	std::fill(begin(range), end(range), value);
}


/// Range-based version of std::iota()
template<class Rng, 
         class Val,
		 class = EnableIfRangeTakes<Rng, Val>>
void iotay(Rng& range, Val value)
{
	std::iota(begin(range), end(range), value);
}


void test_algo_traits()
{
	std::vector<double> vd;
	std::vector<float> vf;
	fillx(vd, 1.F);
//	fillx(vf, 0.9);

//	iotay(vf, 0.0);
	
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
class DimensionInfo {};

class NetcdfFile {
public:
	using DimInfoRange = MapCvalueRange<std::string, DimensionInfo>; 

	DimInfoRange dims_info() const 
	{
		return crange_second(dims_info_);
	}

private:
	std::map<std::string, DimensionInfo>  dims_info_{};  

};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

template<class, class = std::void_t<>>
struct IsContigContainerImplX 
	: std::false_type {};

template<class Cont>
struct IsContigContainerImplX<Cont, std::void_t<
			std::enable_if_t<twist::detail::IsAnyRangeImpl<Cont>::value>, 
			std::enable_if_t<std::is_integral_v<decltype( rg::size(std::declval<Cont>()) )>>,
			decltype( std::data(std::declval<Cont>()) )>>
	: std::true_type {};

template<class Cont>
using IsContigContainerX = IsContigContainerImplX<Cont>; 

template<class Cont>
using EnableIfContigContainerX = std::enable_if_t<IsContigContainerX<Cont>::value, int>;


template<class BlobElem,
         class Cont, 
         class = EnableIfContigContainerX<Cont>> 	
void get_array_blobx(std::string_view, Cont&)
{
}

template<class Cont,
         class = EnableIfContigContainer<Cont>> 
void set_array_blobx(std::string_view, gsl::strict_not_null<const Cont*>)
{
}

using UnivarSample = twist::math::UnivarDistrSample<float>;

void read_blob_to_sample(const std::string& col_name)
{
	 const UnivarSample sample{1000};
	 std::vector<double> v;

	get_array_blobx<float>(col_name, sample);
	set_array_blobx(col_name, gsl::strict_not_null{ &sample });
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

void filesys()
{
	fs::path p{ L"C:\\bob\\lk" };

	std::vector<fs::path> p_elems;
		
	twist::copy(p, back_inserter(p_elems));
}

}

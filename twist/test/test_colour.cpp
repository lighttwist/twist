/// @file test_colour.cpp
/// Implementation file for "test_colour.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE

#include "twist/twist_maker.hpp"

#include "twist/test/test_globals.hpp"

#include "twist/Colour.hpp"

namespace twist::test {

auto test_colour_ref(Colour::ColourRef ref, std::uint8_t expected_r, std::uint8_t expected_g, std::uint8_t expected_b)
{
    REQUIRE(Colour::colour_ref_from_rgb(expected_r, expected_g, expected_b) == ref);

    auto colour = Colour{ref};
    auto [r, g, b] = colour.rgb();
    REQUIRE(r == expected_r);
    REQUIRE(g == expected_g);
    REQUIRE(b == expected_b);
}

TEST_CASE("Test twist::Colour class", "[twist]") 
{
    static const auto black_colour_ref = Colour::colour_ref_from_rgb(0, 0, 0);
    static const auto white_colour_ref = Colour::colour_ref_from_rgb(255, 255, 255);
    static const auto red_colour_ref = Colour::colour_ref_from_rgb(255, 0, 0);
    static const auto green_colour_ref = Colour::colour_ref_from_rgb(0, 128, 0);
    static const auto blue_colour_ref = Colour::colour_ref_from_rgb(0, 0, 255);
    static const auto light_grey_colour_ref = Colour::colour_ref_from_rgb(192, 192, 192);

    test_colour_ref(black_colour_ref, 0, 0, 0);
    test_colour_ref(white_colour_ref, 255, 255, 255);
    test_colour_ref(red_colour_ref, 255, 0, 0);
    test_colour_ref(green_colour_ref, 0, 128, 0);
    test_colour_ref(blue_colour_ref, 0, 0, 255);
    test_colour_ref(light_grey_colour_ref, 192, 192, 192);
    test_colour_ref(Colour::colour_ref_from_rgb(25, 105, 205), 25, 105, 205);
    test_colour_ref(Colour::colour_ref_from_rgb(143, 249, 78), 143, 249, 78);
}

}


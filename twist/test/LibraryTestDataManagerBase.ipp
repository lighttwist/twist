/// @file LibraryTestDataManagerBase.cpp
/// Inline implementation file for "LibraryTestDataManagerBase.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/file_io.hpp"

namespace twist::test {

// --- LibraryTestDataManagerBase<>::DirectoryDeletionToken class ---

template<class Derived>
LibraryTestDataManagerBase<Derived>::DirectoryDeletionToken::DirectoryDeletionToken(fs::path path)
	: path_{std::move(path)}
{
	TWIST_CHECK_INVARIANT
}

template<class Derived>
LibraryTestDataManagerBase<Derived>::DirectoryDeletionToken::~DirectoryDeletionToken()
{
	TWIST_CHECK_INVARIANT_NO_SCOPE
	if (!cancelled_) {
		auto err_code = std::error_code{};
		remove_all(path_, err_code);
		assert(!err_code);
	}
}

template<class Derived>
auto LibraryTestDataManagerBase<Derived>::DirectoryDeletionToken::path() const -> fs::path
{
	TWIST_CHECK_INVARIANT
	return path_;
}

template<class Derived>
auto LibraryTestDataManagerBase<Derived>::DirectoryDeletionToken::cancel_deletion() -> void
{
	TWIST_CHECK_INVARIANT
	assert(!cancelled_);
	cancelled_ = true;
}

#ifdef _DEBUG
template<class Derived>
auto LibraryTestDataManagerBase<Derived>::DirectoryDeletionToken::check_invariant() const noexcept -> void
{
	assert(!path_.empty());
}
#endif

// --- LibraryTestDataManagerBase class template ---

template<class Derived>
const wchar_t* LibraryTestDataManagerBase<Derived>::temp_test_output_dir_name = L"__temp_test_output";

template<class Derived>
LibraryTestDataManagerBase<Derived>::LibraryTestDataManagerBase(fs::path lib_data_root_dir_path,
                                                                fs::path shared_data_root_dir_path)
	: lib_data_root_dir_path_{lib_data_root_dir_path.lexically_normal()}
	, shared_data_root_dir_path_{shared_data_root_dir_path.lexically_normal()}
{
	if (!exists(lib_data_root_dir_path_)) {
		TWIST_THRO2(L"Library test data directory not found at \"{}\".", lib_data_root_dir_path_.c_str());
	}
	if (!exists(shared_data_root_dir_path_)) {
		TWIST_THRO2(L"Shared data directory not found at \"{}\".", shared_data_root_dir_path_.c_str());
	}
	TWIST_CHECK_INVARIANT
}

template<class Derived>
LibraryTestDataManagerBase<Derived>::~LibraryTestDataManagerBase() = default;

template<class Derived>
auto LibraryTestDataManagerBase<Derived>::library_data_root_dir_path() const -> fs::path
{
	TWIST_CHECK_INVARIANT
	return lib_data_root_dir_path_;
}

template<class Derived>
auto LibraryTestDataManagerBase<Derived>::shared_data_root_dir_path() const -> fs::path
{
	TWIST_CHECK_INVARIANT
	return shared_data_root_dir_path_;
}

template<class Derived>
auto LibraryTestDataManagerBase<Derived>::temp_test_output_root_dir_path() const -> fs::path
{
	TWIST_CHECK_INVARIANT
	return lib_data_root_dir_path_ / temp_test_output_dir_name;
}

template<class Derived>
auto LibraryTestDataManagerBase<Derived>::prepare_empty_temp_test_output_root_dir() -> DirectoryDeletionToken
{
	TWIST_CHECK_INVARIANT
	auto dir_path = temp_test_output_root_dir_path();
	if (exists(dir_path)) {
		delete_dir_contents(dir_path);
	}
	else {
		create_directories(dir_path);
	}
	return DirectoryDeletionToken{std::move(dir_path)};
}

template<class Derived>
auto LibraryTestDataManagerBase<Derived>::delete_temp_test_output_root_dir() -> bool
{
	TWIST_CHECK_INVARIANT
	const auto dir_path = temp_test_output_root_dir_path();
	auto err_code = std::error_code{};
	remove_all(dir_path, err_code);
	return err_code;
}

#ifdef _DEBUG
template<class Derived>
auto LibraryTestDataManagerBase<Derived>::check_invariant() const noexcept -> void
{
	assert(!lib_data_root_dir_path_.empty());
	assert(!shared_data_root_dir_path_.empty());
}
#endif

}

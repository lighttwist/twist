//  Implementation file for "test_date_time.hpp"
//
//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.
//

#include "twist/twist_maker.hpp"

#include "twist/test/test_date_time.hpp"

#include "twist/DateInterval.hpp"
#include "twist/DateTime.hpp"
#include "twist/chrono_utils.hpp"

#include "twist/test/test_globals.hpp"

#pragma warning(disable: 4100)
#pragma warning(disable: 4189) // local variable is initialized but not referenced

using namespace std::chrono;

namespace twist::test {
 
TEST_CASE("Test for class twist::Date", "[twist]") 
{
	Date d1{20, 2, 2019};
	REQUIRE(get_dow(d1) == 3 );
	REQUIRE(get_dow_name(d1) == L"Wednesday");

	Date d2{24, 6, 2119};
	REQUIRE(get_dow(d2) == 6 );
	REQUIRE(get_dow_name(d2) == L"Saturday");

	Date d3{10, 10, 1920};
	REQUIRE(get_dow(d3) == 0 );
	REQUIRE(get_dow_name(d3) == L"Sunday");

	{
		Date dat{ 20, 2, 2019 };

		REQUIRE( add_months(dat,  10) == Date{ 20, 12, 2019 } );

		REQUIRE( add_months(dat,  11) == Date{ 20,  1, 2020 } );
		REQUIRE( add_months(dat,  13) == Date{ 20,  3, 2020 } );
		REQUIRE( add_months(dat,  25) == Date{ 20,  3, 2021 } );

		REQUIRE( add_months(dat, -10) == Date{ 20,  4, 2019 } );
		REQUIRE( add_months(dat, -14) == Date{ 20, 12, 2018 } );
		REQUIRE( add_months(dat, -18) == Date{ 20,  8, 2018 } );
		REQUIRE( add_months(dat, -25) == Date{ 20,  1, 2017 } );
	}

	REQUIRE(get_diff_in_days({15, 3, 1991}, {15, 3, 1991}) ==    0);
	REQUIRE(get_diff_in_days({15, 3, 1994}, {15, 3, 1993}) == -365);
	REQUIRE(get_diff_in_days({15, 3, 1991}, { 5, 3, 1991}) ==  -10);
}

TEST_CASE("Test for class twist::DateInterval", "[twist]") 
{
	REQUIRE(length_days(DateInterval{{15, 3, 1991}, {15, 3, 1991}}) ==   1);
	REQUIRE(length_days(DateInterval{{15, 3, 1991}, {16, 3, 1991}}) ==   2);
	REQUIRE(length_days(DateInterval{{15, 3, 1991}, {14, 3, 1992}}) == 366); // leap year
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  Unstructured test code

constexpr auto secs_from_sys_clock_to_bday = 209520000;
using BdayClock = CustomSysClock<secs_from_sys_clock_to_bday>;


template<class Clock, typename Clock::duration epoch_shift>
struct CustomClockX {
    
	using rep = typename Clock::rep;
    
	using period = typename Clock::period;
    
	using duration = typename Clock::duration;

    using time_point = typename std::chrono::time_point<CustomClockX>;

	/// Whether this clock is a "steady clock" (which is the same as asking whether the original clock is).
    static constexpr bool is_steady = Clock::is_steady;

	/// This clock's epoch, expressed as a time point of the original clock.
    static constexpr typename Clock::time_point epoch = 
			typename Clock::time_point{ epoch_shift };

	/// A time point representing the current time.
    static time_point now() 
	{
        return time_point{ Clock::now() - epoch };
    }
};

/// Clock class which is based on the system_vlock class, but which uses another epoch, obtained by altering 
/// the system clock's epoch (which is usually the Unix Time, always from C++20).
///
/// @tparam  secs_since_sys_epoch  The difference, in seconds, between the system_clock epoch and the 
///				epoch of this clock
///
//template<auto epoch_shift> 
//using CustomSysClockX = CustomClockX<std::chrono::system_clock, epoch_shift>;
//
//constexpr std::chrono::duration<long long> EpochShift = std::chrono::seconds{ 209520000 };
//using BdayClockX = CustomSysClockX<EpochShift>;
//

template<class Clock>
DateTime utc_timepoint_to_datetime_xxx(const typename Clock::time_point& timept)
{
	using namespace std::chrono;

    // Compute time duration since 1Jan1970 00:00:00 (the "system clock" epoch)
    auto time_since_epoch = timept.time_since_epoch();

    // Compute the number of days since 1Jan1970 00:00:00
    const auto days_since_epoch = floor<DurationDays>(time_since_epoch);

    // time_since_epoch is now time duration since midnight of day days_since_epoch
    time_since_epoch -= days_since_epoch;

    // Break days_since_epoch down into year/month/day
    const auto date = days_since_unix_epoch_to_date(days_since_epoch.count());

    // Compute the time
    const auto hour = static_cast<int>(duration_cast<hours>(time_since_epoch).count());
    time_since_epoch -= hours{ hour };
    const auto minute = static_cast<int>(duration_cast<minutes>(time_since_epoch).count());    
	time_since_epoch -= minutes{ minute };
    const auto second = static_cast<int>(duration_cast<seconds>(time_since_epoch).count());
  
    return { date, hour, minute, second };
}


template<class Clock>
DateTime get_epoch()
{
    return utc_timepoint_to_datetime_xxx<Clock>(typename Clock::time_point{});
}



DateTime bdayclock_timepoint_to_datetime_xx(const BdayClock::time_point& timept)
{
	using namespace std::chrono;

    // Compute time duration since 1Jan1970 00:00:00 (the "system clock" epoch)
    auto time_since_epoch = timept.time_since_epoch();

    // Compute the number of days since 1Jan1970 00:00:00
    const auto days_since_epoch = floor<DurationDays>(time_since_epoch); 

	// - DurationDays{ 2425 }

    // time_since_epoch is now time duration since midnight of day days_since_epoch
    time_since_epoch -= days_since_epoch;

    // Break days_since_epoch down into year/month/day
    const auto date = days_since_unix_epoch_to_date(days_since_epoch.count() + 2425);

    // Compute the time
    const auto hour = static_cast<int>(duration_cast<hours>(time_since_epoch).count());
    time_since_epoch -= hours{ hour };
    const auto minute = static_cast<int>(duration_cast<minutes>(time_since_epoch).count());    
	time_since_epoch -= minutes{ minute };
    const auto second = static_cast<int>(duration_cast<seconds>(time_since_epoch).count());
  
    return { date, hour, minute, second };
}


void test_date_time1()
{
	DateTime new_epoch{ 22, 8, 1976 };
	[[maybe_unused]] auto days_since_unix_epoch = date_to_days_since_unix_epoch(new_epoch);

	auto sys_clock_epoch = get_epoch<std::chrono::system_clock>();
	auto bday_clock_epoch = get_epoch<BdayClock>();
//	auto xx = get_epoch<BdayClock>();

	//auto diff = datetime_to_utc_timepoint(new_epoch) - std::chrono::system_clock::time_point{};

	//auto diff_secs = std::chrono::duration_cast<std::chrono::seconds>(diff);

		
//	auto bday_clock_epoch = utc_timepoint_to_datetime(BdayClock::epoch);

//	BdayClock c;

	auto xx = bdayclock_timepoint_to_datetime_xx(BdayClock::time_point{} + std::chrono::hours{ 24 });

	// 22Aug1976 - based clock
//	using Dec1949Clock = CustomSysClock<>;


	//Date oooolddate{2, 1, 1};

	//DateTime oldie{2, 5, 1869, 13, 4, 7};
	//DateTime newie{30, 8, 2069, 12, 5, 8};

	//Date& oldie_dt = oldie;
	//Date newie_dt = newie;

	//oldie_dt = newie_dt;

	//system_clock::time_point unix_time_epoch;

	//// seconds from 1-1-1970 00:00:00 to 1-12-1949 00:00:00  
	//constexpr auto unix_to_narclim_time_diff = seconds{-633830400};  
	////constexpr auto unix_to_narclim_time_diff = hours{ -176064 };

	//auto dt1 = utc_timepoint_to_datetime(unix_time_epoch);

	//system_clock::time_point narclim_epoch_tp{ unix_to_narclim_time_diff };
	//auto dt2 = utc_timepoint_to_datetime(narclim_epoch_tp);

	//auto narclim_tp1 = narclim_epoch_tp + hours{351'396};
	//auto dt3 = utc_timepoint_to_datetime(narclim_tp1);

	//DateTime narclim_start_datetime{1, 12, 1949};
	//auto tpp = datetime_to_utc_timepoint(narclim_start_datetime);
	//tpp += hours{351'396};
	//auto narclim_cur_datetime = utc_timepoint_to_datetime(tpp);

	//DateTime dt4{ 22, 8, 1976, 13, 45, 39 };
	//auto tp1 = datetime_to_utc_timepoint(dt4);

	//tp1 += DurationDays{ 100 };

	//auto dt5 = utc_timepoint_to_datetime(tp1);

	//auto dt6 = narclim_start_datetime;
	//dt6 = add_hours(dt6, 351'396);
	
	//auto my_epoch = unix_time_epoch + seconds(dist_to_unix_time_epoch);

	//DateTime dt2;
	//to_utc_date_time(my_epoch, dt2);

	//// 1949-12-01 based clock
	//using Dec1949Clock = CustomSysClock<days_to_sys_clock_epoch(-7336)>;

	//Dec1949Clock::time_point dec49_clock_epoch;
	//auto dec49_clock_tp1 = dec49_clock_epoch + hours(351'396);

	//DateTime dt3;
	//to_utc_date_time(dec49_clock_tp1, dt3);

}


void test_date_time2()
{
	DateTime dt1{ 13, 3, 2020,  9, 17, 12 };
	DateTime dt2{ 13, 3, 2020,  9, 16, 13 };
	DateTime dt3{ 13, 3, 2020,  9, 16, 12 };
	DateTime dt4{ 13, 3, 2020,  8, 16, 10 };
	DateTime dt5{ 12, 3, 2020, 23, 54, 10 };
	DateTime dt6{ 13, 3, 2020,  1, 16, 10 };

	auto mins = diff_minutes(dt2, dt1);
	mins = diff_minutes(dt3, dt1);
	mins = diff_minutes(dt3, dt4);
	mins = diff_minutes(dt3, dt4);
	mins = diff_minutes(dt5, dt6);
}
 
auto test_datetime3(MessageExceptionFeedbackProv& feedback_prov) -> void
{
	const std::chrono::system_clock::time_point narclim_epoch_timept = 
			datetime_to_utc_timepoint(DateTime{1, 1, 1950});

	auto get_days_since_narclim_epoch = [narclim_epoch_timept](const DateTime& datetime) {
		auto timept = datetime_to_utc_timepoint(datetime);
		//auto secs = std::chrono::duration_cast<std::chrono::seconds>(timept - narclim_epoch_timept);
		//return secs / (24.0 * 60.0 * 60.0);
		auto hours = std::chrono::duration_cast<std::chrono::hours>(timept - narclim_epoch_timept);
		return hours / 24.0;
	};

	auto t1 = get_days_since_narclim_epoch(DateTime{16, 11, 2008, 6, 0, 0});  // 21503.88

	auto d = (t1.count() - 21503.88);
}

}

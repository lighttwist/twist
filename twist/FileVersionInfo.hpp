/// @file FileVersionInfo.hpp
/// FileVersionInfo class definition

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_FILE_VERSION_INFO_HPP
#define TWIST_FILE_VERSION_INFO_HPP

namespace twist {

//! File version info. 
class FileVersionInfo {
public:
	FileVersionInfo(long major_ver_no, long minor_ver_no, long release_no, long build_no);

    [[nodiscard]] long major_ver_no() const;
    
	[[nodiscard]] long minor_ver_no() const;
    
	[[nodiscard]] long release_no() const;

	[[nodiscard]] long build_no() const;

	[[nodiscard]] bool operator==(const FileVersionInfo& rhs) const;

	[[nodiscard]] bool operator!=(const FileVersionInfo& rhs) const;

private:
    long major_ver_no_;
    long minor_ver_no_;
    long release_no_;
	long build_no_;

	TWIST_CHECK_INVARIANT_DECL
};

/*! Get a textual representation of a file info object.
    \param[in] info  The file info
    \param[in] zap_end_zeros  If true, any build numbers that are zero (and are followed by zeroes) are not output
    \return  The text
 */
[[nodiscard]] auto to_str(const FileVersionInfo& info, bool zap_end_zeros = false) -> std::wstring;  

/*! Get a FileVersionInfo object from its textual representation \ p str.
    If \p str does is not a recognised file version representation, an exception is thrown.
*/
[[nodiscard]] auto file_version_info_from_str(const std::wstring& str) -> FileVersionInfo;

} 

#endif 

/// @file std_vector_utils.ipp
/// Inline implementation file for "std_vector_utils.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace twist {

template<class Elem, class Alloc>
[[nodiscard]] auto reserve_vector(Ssize size) -> std::vector<Elem, Alloc>
{
	auto ret = std::vector<Elem, Alloc>{};
	ret.reserve(size);
	return ret;
}

template<class T, class Alloc>
[[nodiscard]] auto reserve_not_nulls(Ssize size) -> std::vector<gsl::not_null<T>, Alloc>
{
	auto ret = std::vector<gsl::not_null<T>, Alloc>{};
	ret.reserve(size);
	return ret;
}

template<class... Types>
[[nodiscard]] auto create_vector(Types&&... elems)
{
	using TypeList = std::tuple<Types...>;
	static_assert(std::tuple_size_v<TypeList> > 0, "At least one element is needed");
	using Type = std::decay_t<std::tuple_element_t<0, TypeList>>;

	auto v = reserve_vector<Type>(sizeof...(elems));
	(v.push_back(std::move(elems)), ...);
	return v;
}

template<class Elem>
auto append(std::vector<Elem> src, std::vector<Elem>& dest) -> typename std::vector<Elem>::iterator
{
    if (dest.empty()) {
        dest = move(src);
        return begin(dest);
    } 
    return dest.insert(end(dest),
                       make_move_iterator(begin(src)),
                       make_move_iterator(end(src)));
}

template<class Elem, class Iter, class Alloc>
[[nodiscard]] auto position_of_iter(const std::vector<Elem, Alloc>& vector, const Iter& iter) -> Ssize
{
	return iter - begin(vector);
}

}

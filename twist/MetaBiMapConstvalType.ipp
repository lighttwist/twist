///  @file  MetaBiMapConstvalType.ipp
///  Inline implementation file for "MetaBiMapConstvalType.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#include "integer_sequence_utils.hpp"

#define TWIST_CLASSTEMPL  template<class Cval, Cval... constvals, class... Types>
#define TWIST_CLASSNAME   MetaBiMapConstvalType<MetaMapConstvalList<Cval, constvals...>,  \
                                                MetaMapTypeList<Types...>>

namespace twist {

TWIST_CLASSTEMPL
constexpr std::size_t TWIST_CLASSNAME::size()
{
	return ConstvalList::size();
}


TWIST_CLASSTEMPL
template<Cval constval>
constexpr int TWIST_CLASSNAME::get_constval_pos()
{
	return binary_search(ConstvalList{}, constval); 
}


TWIST_CLASSTEMPL
template<Cval constval>
constexpr int TWIST_CLASSNAME::get_valid_constval_pos()
{
	constexpr auto pos = get_constval_pos<constval>(); 
	static_assert(pos != -1, "\"constval\" not found in the map's list of constant values.");
	return pos;
}


TWIST_CLASSTEMPL
template<Cval constval>
constexpr bool TWIST_CLASSNAME::contains_constval()
{
	return get_constval_pos<constval>() != -1;
}


TWIST_CLASSTEMPL
template<class Type>
constexpr Cval TWIST_CLASSNAME::find_constval()
{
	constexpr auto pos = tuple_element_index<TypeList, Type>();
	static_assert(pos != -1, "\"Type\" not found in the map's list of types.");
	return get_element<pos>(ConstvalList{});
}


TWIST_CLASSTEMPL
template<class Type>
constexpr bool TWIST_CLASSNAME::contains_type()
{
	return tuple_element_index<TypeList, Type>() != -1;
}


template<class Map, class Type>
constexpr typename Map::Constval meta_map_find_constval()
{
	return Map::template find_constval<Type>();  
}

} 

#undef  TWIST_CLASSNAME
#undef  TWIST_CLASSTEMPL

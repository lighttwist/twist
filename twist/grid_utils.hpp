/// @file grid_utils.hpp
/// Utilities for working with grids in their simplest conceptual form

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_GRID__UTILS_HPP
#define TWIST_GRID__UTILS_HPP

namespace twist {

/*! Zero-based grid cell index, starting with the top-left cell and moving right and down the grid (an explicitly typed 
    integer).
 */
enum class GridCellIndex : Ssize { 
	none = -1  ///< Invalid value
};
 
/*! Stores information about a subgrid of a given grid (which is referred to here as the supergrid).
    A grid is considered to be its own subgrid.
 */
class SubgridInfo {
public:
	/*! Constructor.
	    \param[in] begin_row  The supergrid index of the first row in the subgrid
	    \param[in] end_row  The supergrid index of the last row in the subgrid
	    \param[in] begin_col  The supergrid index of the first column in the subgrid
	    \param[in] end_col  The supergrid index of the last column in the subgrid
	 */
	SubgridInfo(Ssize begin_row, Ssize end_row, Ssize begin_col, Ssize end_col);

	//! The supergrid index of the first row in the subgrid.
	[[nodiscard]] auto begin_row() const -> Ssize;

	//! The supergrid index of the last row in the subgrid.
	[[nodiscard]] auto end_row() const -> Ssize;

	//! The supergrid index of the first column in the subgrid.
	[[nodiscard]] auto begin_column() const -> Ssize;

	//! The supergrid index of the last column in the subgrid.
	[[nodiscard]] auto end_column() const -> Ssize;

private:
	Ssize begin_row_;
	Ssize end_row_;
	Ssize begin_col_;
	Ssize end_col_;

	TWIST_CHECK_INVARIANT_DECL
};

// --- Free functions ---

//! The number of rows in a subgrid described by \p subgrid_info.
[[nodiscard]] auto nof_rows(const SubgridInfo& subgrid_info) -> Ssize;

//! The number of columns in a subgrid described by \p subgrid_info.
[[nodiscard]] auto nof_columns(const SubgridInfo& subgrid_info) -> Ssize;

//! The number of cells in a subgrid described by \p subgrid_info.
[[nodiscard]] auto nof_cells(const SubgridInfo& subgrid_info) -> Ssize;

/*! Find out whether a specific supergrid cell falls within a submatrix.
    \param[in] subgrid_info  The subgrid info  
    \param[in] row  The supergrid row index
    \param[in] col  The supergrid column index
    \return  true if the cell is in the submatrix
 */
[[nodiscard]] auto contains_cell(const SubgridInfo& subgrid_info, Ssize row, Ssize col) -> bool;

/*! Get the dimensions (the number of columns and the number of rows) of a subgrid.
    \param[in] subgrid_info  The subgrid info  
    \return  0. The number of rows
 			 1. The number of columns
 */
[[nodiscard]] auto dims(const SubgridInfo& subgrid_info) -> std::tuple<Ssize, Ssize>;

//! Get the indexes of the rows in the subgrid described by \p subgrid_info as an iterable range.
[[nodiscard]] auto row_range(const SubgridInfo& subgrid_info) -> IndexRange<Ssize>;

//! Get the indexes of the columns in the subgrid described by \p subgrid_info as an iterable range.
[[nodiscard]] auto column_range(const SubgridInfo& subgrid_info) -> IndexRange<Ssize>;

/*! Calculate a grid cell index from the cell row and column indexes.
    \param[in] row  The cell row index; an exception is thrown if it is too large
    \param[in] col  The cell column index; an exception is thrown if it is too large
    \param[in] row_count  The number of rows in the grid
    \param[in] col_count  The number of columns in the grid
    \return  The cell index
 */
[[nodiscard]] auto grid_cell_index_from_row_col(Ssize row, Ssize col, Ssize row_count, Ssize col_count) 
                    -> GridCellIndex;

/*! Calculate a grid cell's row and column indexes from the cell index.
    \param[in] cell_index  The cell index; an exception is thrown if it is too large
    \param[in] row_count  The number of rows in the grid
    \param[in] col_count  The number of columns in the grid
    \return  0. The cell row index
             1. The cell column index
 */
[[nodiscard]] auto grid_row_col_from_cell_index(GridCellIndex cell_index, Ssize row_count, Ssize col_count) 
                    -> std::tuple<Ssize, Ssize>;

} 

#endif 

/// @file ComWrapper.ipp
/// Inline implementation file for "ComWrapper.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace twist::com {

template<typename RawInterface>
ComWrapper<RawInterface>::ComWrapper(const CLSID& clsid)
{
	TWIST_COM_CHECK_HR(interf_.CreateInstance(clsid));
	TWIST_CHECK_INVARIANT
}


template<typename RawInterface>
ComWrapper<RawInterface>::ComWrapper(gsl::not_null<RawInterface*> interf)
	: interf_{ interf.get() }
{
	TWIST_CHECK_INVARIANT
}


template<typename RawInterface>
const typename ComWrapper<RawInterface>::Interface& ComWrapper<RawInterface>::operator->() const 
{
	TWIST_CHECK_INVARIANT
	return interf_;
}	


template<typename RawInterface>
const typename ComWrapper<RawInterface>::Interface& ComWrapper<RawInterface>::interf() const 
{
	TWIST_CHECK_INVARIANT
	return interf_;
}	


#ifdef _DEBUG
template<typename RawInterface>
void ComWrapper<RawInterface>::check_invariant() const noexcept
{
	assert(interf_);
}
#endif

}

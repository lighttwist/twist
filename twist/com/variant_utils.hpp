/// @file variant_utils.hpp
/// Global utilities for working in the Microsoft COM world.

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_COM_VARIANT__UTILS_HPP
#define TWIST_COM_VARIANT__UTILS_HPP

#include "twist/com/com_globals.hpp"

namespace twist {
class Date;
class DateTime;
}

namespace twist::com {

/*! Convert a variant value to a std::wstring object. An exception is thrown if the variant does not contain a BSTR 
    value.
    \param[in] var  The variant
    \return  The  std::wstring  object
 */
std::wstring variant_to_string(const VARIANT& var);

/*! Convert a string to a variant (which will contain a copy of the string). 
    \param[in] str  The string
    \return  The variant
 */
VARIANT string_to_variant(const std::wstring& str);

/*! Convert an ANSI string to a variant (which will contain a copy of the string). 
    \param[in] str  The string
    \return  The variant
 */
VARIANT ansi_to_variant(const std::string& str);

/*! Convert a VARIANT object to a filesystem path value.
    \param[in] var  The VARIANT object; an exception is thrown if it does not contain a string value
    \return  The filesystem path value
 */
fs::path variant_to_path(const VARIANT& var);

/*! Convert a variant value to a bool value. An exception is thrown if the variant does not contain a VARIANT_BOOL 
    value.
    \param[in] var  The variant
    \return  The bool value
 */
bool variant_to_bool(const VARIANT& var);

/*! Convert a variant value to a LONG object. An exception is thrown if the variant does not contain a LONG value.
    \param[in] var  The variant
    \return  The LONG object
 */
LONG variant_to_long(const VARIANT& var);

/*! Create a VARIANT wrapping a specific LONG value.
    \param[in] val  The value to be wrapped
    \return  The VARIANT 
 */
VARIANT long_to_variant(LONG val);

/*! Convert a variant value to a  ULONG  object. An exception is thrown if the variant does not contain a ULONG value.
    \param[in] var  The variant
    \return  The  ULONG  object
 */
ULONG variant_to_ulong(const VARIANT& var);

/*! Convert a variant value to a  FLOAT  object. An exception is thrown if the variant does not contain a FLOAT value.
    \param[in] var  The variant
    \return  The  FLOAT  object
 */
FLOAT variant_to_float(const VARIANT& var);

/*! Create a VARIANT wrapping a specific FLOAT value.
    \param[in] val  The value to be wrapped
    \return  The VARIANT
 */
VARIANT float_to_variant(FLOAT val);

/*! Convert a variant value to a  DOUBLE  object. An exception is thrown if the variant does not contain a DOUBLE 
    value.
    \param[in] var  The variant
    \return  The  DOUBLE  object
 */
DOUBLE variant_to_double(const VARIANT& var);

/*! Create a VARIANT wrapping a specific DOUBLE value.
    \param[in] val  The value to be wrapped
    \return  The VARIANT
 */
VARIANT double_to_variant(DOUBLE val);

/*! Create a VARIANT wrapping a specific IDispatch interface.
    \param[in] val  The value to be wrapped
    \return  The VARIANT
 */
VARIANT idisp_to_variant(IDispatch* val);

/*! Create a VARIANT wrapping a specific SAFEARRAY value.
    \param[in] val  The value to be wrapped
    \param[in] elem_vartype  The VARTYPE value indicating the SAFEARRAY element type
    \return  The VARIANT
 */
VARIANT safearray_to_variant(SAFEARRAY& val, VARTYPE elem_vartype);

/*! Wrap a COM DATE (the type used for storing a datetime value in a VARIANT) value into a VARIANT object.
    \param[in] vardate  COM DATE value 
    \return  The VARIANT object
 */
VARIANT vardate_to_variant(DATE vardate);

/*! Convert a date value to a VARIANT object.
    \param[in] date  The date value
    \return  The VARIANT object 
 */
VARIANT date_to_variant(const Date& date);

/*! Convert a datetime value to a VARIANT object.
    \param[in] datetime  The datetime value
    \return  The VARIANT object
 */
[[nodiscard]] auto datetime_to_variant(const DateTime& datetime) -> VARIANT;

/*! Convert a VARIANT object to a datetime value.
    \param[in] var  The VARIANT object; an exception is thrown if it does not contain a DATE value
    \return  The datetime value
 */
[[nodiscard]] auto variant_to_datetime(const VARIANT& var) -> DateTime;

}

#endif 

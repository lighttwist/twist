/// @file atl_utils.ipp
/// Inline implementation file for "atl_utils.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/type_info_utils.hpp"

namespace twist::com {

template<class Interf, class CoClass>
auto create_com_atl() -> std::tuple<ATL::CComPtr<Interf>, CoClass*>
{
	auto icom = ATL::CComPtr<Interf>{};
	if (CoClass::template CreateInstance<Interf>(&icom) != S_OK) {
		TWIST_THRO2(L"Failed to create {} instance.", type_wname<CoClass>());
	}
	
	auto* com = dynamic_cast<CoClass*>(icom.p);
	if (!com) {
		TWIST_THRO2(L"{} instance corrupt.", type_wname<CoClass>());
	}

	return {std::move(icom), com};
}

template<class DestInterf, class SrcInterf> 
auto query_com_atl(ATL::CComPtr<SrcInterf> src) -> ATL::CComPtr<DestInterf>
{
	auto dest = ATL::CComPtr<DestInterf>{};
	const auto hr = src.QueryInterface<DestInterf>(&dest);
	if (FAILED(hr) || !dest) {
		TWIST_THRO2(L"Error querying COM interface {} from {}.", type_wname<DestInterf>(), type_wname<SrcInterf>());
	}
	return dest;
}
 
} 

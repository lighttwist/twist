/// @file com_utils.ipp
/// Inline implementation file for "com_utils.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/type_info_utils.hpp"

namespace twist::com {

template<class Interf, class CoClass>
[[nodiscard]] auto create_com_smartptr() -> TWIST_COM_SMARTPTR(Interf)
{
	auto interf = TWIST_COM_SMARTPTR(Interf){};
	interf.CreateInstance(__uuidof(CoClass));
	if (!interf) {
		TWIST_THRO2(L"Failed to create {} instance.", twist::type_wname<CoClass>());
	}
	return interf;
}

template<class DestIntf, class SrcIntf> 
[[nodiscard]] auto query_com_smartptr(TWIST_COM_SMARTPTR(SrcIntf) src) -> TWIST_COM_SMARTPTR(DestIntf)
{
	auto dest = TWIST_COM_SMARTPTR(DestIntf){};
	const auto hr = src.QueryInterface<DestIntf>(__uuidof(DestIntf), &dest);
	if (FAILED(hr) || !dest) {
		TWIST_THRO2(L"Error querying COM interface {} from {}.", type_wname<DestIntf>(), type_wname<SrcIntf>());
	}
	return dest;
}


template<class ComClass> 
HRESULT generate_com_error(const std::exception& ex, gsl::not_null<ComClass*> com_obj, const GUID& iid)
{
	std::wstringstream err_msg;
	err_msg << twist::ansi_to_string(ex.what());

	if (auto re = dynamic_cast<const twist::RuntimeError*>(&ex)) {
		if (!re->function_name().empty()) {
			err_msg << L"\n\nError occurred in  " << re->function_name() << L"()";
		}
	}
	return com_obj->Error(err_msg.str().c_str(), iid, E_FAIL);
}

} 

/// @file atl_utils.hpp
/// InterfaceCollectionBase base class template

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_COM_INTERFACE_COLLECTION_BASE_HPP
#define TWIST_COM_INTERFACE_COLLECTION_BASE_HPP

#include "twist/com/atl_utils.hpp"

namespace twist::com {

/*! Base class for COM classes which represent a collection of COM interfaces of the same type (single threaded, 
    implements ISupportErrorInfo).
	\tparam CollCoClass  Collection (derived) COM class type
	\tparam CollInterf  Collection COM class interface type
	\tparam ElemInterf  Interface type stored in the collection
	\tparam clsid  Address of the CLSID for CollCoClass
	\tparam iid  Address of the IID for CollInterf
	\tparam libid  Address of the library ID of the library containing CollCoClass 
 */
template<class CollCoClass, class CollInterf, class ElemInterf, const CLSID* clsid, const IID* iid, const GUID* libid>
class InterfaceCollectionBase : public ATL::CComObjectRootEx<ATL::CComSingleThreadModel>,
								public ATL::CComCoClass<CollCoClass, clsid>,
								public ISupportErrorInfo,
								public ATL::IDispatchImpl<CollInterf, iid, libid, 1, 0> {
public:
	/*! Get the element at a specific position within the collection.
	    \param[in] pos  The (zero-based) position
	    \param[out,retval] arc  The element interface
	 */
	STDMETHOD(at)(LONG pos, ElemInterf** elem) 
	{
		try {
			if (pos >= twist::ssize32(collection_)) {
				TWIST_THROW(L"Element position \"%d\" out of bounds.", pos);
			}
			collection_[pos].CopyTo(elem);
			return S_OK;
		}
		catch (const std::exception& ex) {  
			return TWIST_GENERATE_COM_ERROR(ex);  
		}  
	}

	/*! Find out whether the collection contains a specific arc.
	    \param[in] elem  The element
	    \param[out,retval] contains  true if it does
	 */
	STDMETHOD(contains)(ElemInterf* elem, VARIANT_BOOL* contains) 
	{
		try {
			auto elem_ptr = ATL::CComPtr<ElemInterf>{elem};
			*contains = twist::com::to_varbool(twist::has(collection_, elem_ptr));
			return S_OK;
		}
		catch (const std::exception& ex) {  
			return TWIST_GENERATE_COM_ERROR(ex);  
		}  
	}

	/*! Insert an element at the end of the collection.
	    \param[in] elem  The new element interface
	 */
	STDMETHOD(push_back)(ElemInterf* elem) 
	{
		try {
			collection_.push_back(elem);
			return S_OK;
		}
		catch (const std::exception& ex) {  
			return TWIST_GENERATE_COM_ERROR(ex);  
		}  
	}

	/*! Get the number of elements in the collection.
	    \param[out,retval] size  The number of elements
	 */
	STDMETHOD(size)(LONG* size) 
	{
		*size = static_cast<LONG>(collection_.size());
		return S_OK;
	}

	// --- Other ---

	//! Create an instance of the collection COM class, containing the elements in \p elems.
	[[nodiscard]] static auto create(std::vector<ATL::CComPtr<ElemInterf>> elems) 
	                           -> ATL::CComPtr<CollInterf>
	{
		auto [icom, com] = create_com_atl<CollInterf, CollCoClass>();
		com->collection_ = move(elems);
		return icom;
	}

	//! Get direct access to the underlying list of interfaces.
	[[nodiscard]] auto collection() const -> const std::vector<ATL::CComPtr<ElemInterf>>&
	{
		TWIST_CHECK_INVARIANT
		return collection_;
	}

	BEGIN_COM_MAP(CollCoClass)                                
		COM_INTERFACE_ENTRY(CollInterf)                  
		COM_INTERFACE_ENTRY(IDispatch)                  
		COM_INTERFACE_ENTRY(ISupportErrorInfo)          
	END_COM_MAP()                                       

private:
#ifdef _DEBUG
	void check_invariant() const noexcept
	{
	}
#endif 

	std::vector<ATL::CComPtr<ElemInterf>> collection_;

	TWIST_CHECK_INVARIANT_FRIEND_DECL
};

}

#endif 

/// @file com_class_macros.hpp
/// Macros to help with COM class definition and method implementation

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_COM_COM__CLASS__MACROS_HPP
#define TWIST_COM_COM__CLASS__MACROS_HPP

/// The set of standard declarations present in the definition of a COM class (ATL Simple Object) 
///   Class  The COM class name (eg  COMThing)
///   Interface  The COM interface name (eg  ICOMThing)
///   RegId  The associated registry resource ID (eg  IDR_COMTHING) 
///
#define  TWIST_COM_CLASS_DECLARATIONS(Class, Interface, RegId)      \
				Class();                                            \
				virtual ~Class();                                   \
				DECLARE_REGISTRY_RESOURCEID(RegId)                  \
				BEGIN_COM_MAP(Class)                                \
					COM_INTERFACE_ENTRY(Interface)                  \
					COM_INTERFACE_ENTRY(IDispatch)                  \
					COM_INTERFACE_ENTRY(ISupportErrorInfo)          \
				END_COM_MAP()                                       \
				DECLARE_PROTECT_FINAL_CONSTRUCT()                   \
				HRESULT FinalConstruct();                           \
				void FinalRelease();                                \
				STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);

/// The set of standard declarations present in the definition of a COM class (ATL Simple Object), except the 
/// COM interface map section. Use this macro when declaring COM classes which inherit other interfaces 
/// defined in your applocation.
///   Class  The COM class name (eg  COMThing)
///   Interface  The COM interface name (eg  ICOMThing)
///   RegId  The associated registry resource ID (eg  IDR_COMTHING) 
///
#define  TWIST_COM_CLASS_DECLARATIONS_NO_MAP(Class, Interface, RegId)  \
				Class();                                               \
				virtual ~Class();                                      \
				DECLARE_REGISTRY_RESOURCEID(RegId)                     \
				DECLARE_PROTECT_FINAL_CONSTRUCT()                      \
				HRESULT FinalConstruct();                              \
				void FinalRelease();                                   \
				STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);

/// The set of standard COM method implementations present in the implementation of a COM class (ATL 
/// Simple Object) 
#define  TWIST_COM_CLASS_IMPLEMENTATIONS(Class, InterfaceID)                   \
				STDMETHODIMP Class::InterfaceSupportsErrorInfo(REFIID riid) {  \
					static const IID* arr[] = {&InterfaceID};                  \
					for (int i = 0; i < sizeof(arr) / sizeof(arr[0]); ++i) {   \
						if (InlineIsEqualGUID(*arr[i], riid)) {                \
							return S_OK;                                       \
						}                                                      \
					}                                                          \
					return S_FALSE;                                            \
				}                                                              \
				HRESULT Class::FinalConstruct() {                              \
					return S_OK;                                               \
				}                                                              \
				void Class::FinalRelease() {                                   \
				}             

#endif

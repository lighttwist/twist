/// @file atl_utils.hpp
/// Global utilities for working with Microsoft's ATL (Active Template Library) library

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_COM_ATL__UTILS_HPP
#define TWIST_COM_ATL__UTILS_HPP

#include <atlbase.h>

#include <tuple>

namespace twist::com {

/*! Create and default-initialise a single COM object, and query it for a specific interface.
    \tparam Interf  The interface to be used to communicate with the object
    \tparam CoClass  The coclass type; must implement Interf
    \return  0. Interface to the new COM object
             1. Pointer to the new COM object (no ownership involved)
 */
template<class Interf, class CoClass>
[[nodiscard]] auto create_com_atl() -> std::tuple<ATL::CComPtr<Interf>, CoClass*>;

/*! Query a COM interface, wrapped in an ATL COM smart pointer (the destination interface) from another (the source 
    interface); effectivelly an ATL COM smart pointer cast. If the cast fails (the destination interface cannot be 
    found by querying the source interface), an exception is thrown.
    \tparam DestInterf  The destination COM interface type
    \tparam SrcInterf  The source COM interface type
    \param[in] src  The source interface 
    \return  The requested interface, wrapped in an ATL COM smart pointer
 */
template<class DestInterf, class SrcInterf> 
[[nodiscard]] auto query_com_atl(ATL::CComPtr<SrcInterf> src) -> ATL::CComPtr<DestInterf>;

}

#include "twist/com/atl_utils.ipp"

#endif 

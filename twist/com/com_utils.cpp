/// @file com_utils.cpp
/// Implementation file for "com_utils.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/com/com_utils.hpp"

namespace twist::com {

auto com_initialise(bool multi_threaded) -> void
{
	auto hr = HRESULT{S_OK};
	if (!multi_threaded) {
		hr = ::CoInitialize(nullptr/*reserved*/);
	}
	else {
		hr = ::CoInitializeEx(nullptr/*reserved*/, COINIT_MULTITHREADED/*coInit*/);	
	}
	if (FAILED(hr)) {
		TWIST_THROW(L"CoInitialize() failed with HRESULT %d.", hr);
	}
}

void com_uninitalise()
{
	::CoUninitialize();
}

auto to_varbool(bool b) -> VARIANT_BOOL
{
	return b ? VARIANT_TRUE : VARIANT_FALSE;
}

auto varbool_to_bool(VARIANT_BOOL vb) -> bool
{
	return vb == VARIANT_TRUE;
}

std::wstring bstr_to_string(BSTR bstr)
{
	return {bstr, ::SysStringLen(bstr)};
}

std::wstring bstr_to_string(const _bstr_t& bstr)
{
	return static_cast<const wchar_t*>(bstr);
}


fs::path bstr_to_path(BSTR bstr)
{
	return std::wstring{ bstr, ::SysStringLen(bstr) };	
}


std::wstring_view view(BSTR bstr)
{
	return { bstr, ::SysStringLen(bstr) };
}


std::wstring_view view(const _bstr_t& bstr)
{
	return { static_cast<const wchar_t*>(bstr), bstr.length() };
}


std::string bstr_to_ansi(BSTR bstr)
{
	const auto length = ::SysStringLen(bstr);
	std::string ret(length, '\0');
	auto* ret_data = ret.data();
	for (auto i = 0u; i < length; ++i) {
		*ret_data++ = static_cast<char>(*bstr++);
	}
	return ret;
}


BSTR string_to_bstr(std::wstring_view str)
{
	return ::SysAllocStringLen(str.data(), size32(str));
}


BSTR path_to_bstr(const fs::path & path)
{
	const auto path_cstr = path.c_str();
	return ::SysAllocStringLen(path_cstr, static_cast<unsigned int>(wcslen(path_cstr)));
}


BSTR ansi_to_bstr(std::string_view ansi_str)
{
	const auto str = to_string(ansi_str);
	return string_to_bstr(str);
}


DATE date_to_vardate(const Date& date)
{
	return datetime_to_vardate(DateTime{ date });
}


DATE datetime_to_vardate(const DateTime& datetime)
{
	SYSTEMTIME systime{};
	systime.wDay = static_cast<WORD>(datetime.day());
	systime.wMonth = static_cast<WORD>(datetime.month());
	systime.wYear = static_cast<WORD>(datetime.year());
	systime.wHour = static_cast<WORD>(datetime.hour());
	systime.wMinute = static_cast<WORD>(datetime.minute());
	systime.wSecond = static_cast<WORD>(datetime.second());

	DATE vardate;
	if (!SystemTimeToVariantTime(&systime, &vardate)) {
		TWIST_THROW(L"Could not convert datetime value to a COM DATE value.");
	}

	return vardate;
}


DateTime vardate_to_datetime(DATE vardate)
{
	SYSTEMTIME systime{};
	if (!VariantTimeToSystemTime(vardate, &systime)) {
		TWIST_THROW(L"Could not convert COM DATE value to a datetime value.");	
	}

	return { systime.wDay, systime.wMonth, systime.wYear, systime.wHour, systime.wMinute, systime.wSecond };
}

auto get_err_descr(IErrorInfo* err_info) -> std::wstring
{
	if (!err_info) {
		return L"";
	}

	auto err = std::wstring{};
	auto err_bstr = (BSTR)nullptr; 
	if (SUCCEEDED(err_info->GetDescription(&err_bstr))) { 
		err = bstr_to_string(err_bstr); 
	}
	::SysFreeString(err_bstr);
	return err;
}

void check_hr_ierror(HRESULT hr, const wchar_t* func_name) 
{
	if (FAILED(hr)) {
		IErrorInfoPtr err_info; 
		if (SUCCEEDED(::GetErrorInfo(0, &err_info)) && err_info != nullptr) {
			const std::wstring err_descr = get_err_descr(err_info);
			if (!err_descr.empty() && err_descr != L"Unspecified error.") {
				throw RuntimeError(err_descr, func_name);
			}
		}
		else {
			_com_error com_err(hr);
			if (wcslen(com_err.ErrorMessage()) > 0) {
				throw RuntimeError(com_err.ErrorMessage(), func_name);
			}
		}
		throw RuntimeError(L"Unknown exception occurred in COM method  %s .", func_name);
	} 
}

}


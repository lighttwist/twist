/// @file safearray_utils.hpp
/// Utilities for working with COM SAFEARRAYs

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_COM_SAFEARRAY__UTILS_HPP
#define TWIST_COM_SAFEARRAY__UTILS_HPP

#include "twist/com/com_globals.hpp"
#include "twist/com/com_utils.hpp"
#include "twist/com/variant_utils.hpp"
#include "twist/math/ClosedInterval.hpp"
#include "twist/math/FlatMatrix.hpp"

namespace twist::com {

/// Class storing the bounds information for one of a SAFEARRAY's dimensions 
using SafearrayDimBounds = twist::math::ClosedInterval<LONG>;

/// Get the size of a SAFEARRAY, in one of its dimensions.
///
/// @param[in] safearray  The SAFEARRAY
/// @param[in] dimension  The dimension we are interested in (one-based)
/// @return  The safearray size
///
size_t get_safearray_size(const VARIANT& safearray, size_t dimension);

//! Get the type ID of the elements in \p saferarray.
[[nodiscard]] auto get_safearray_elem_type(gsl::not_null<SAFEARRAY*> safearray) -> VARTYPE;

//! Get the number of dimensions of \p safearray.
[[nodiscard]] auto count_safearray_dimensions(gsl::not_null<SAFEARRAY*> safearray) -> size_t;

//! Get the number of dimensions of the SAFEARRAY held in \p safearray.
[[nodiscard]] auto count_safearray_dimensions(const VARIANT& safearray) -> size_t;

//! Get the bounds for each of the dimensions of \p safearray.
[[nodiscard]] auto get_safearray_dimensions_bounds(gsl::not_null<SAFEARRAY*> safearray) 
                    -> std::vector<SafearrayDimBounds>;

//! Get the bounds for each of the dimensions of the SAFEARRAY held in \p safearray.
[[nodiscard]] auto get_safearray_dimensions_bounds(const VARIANT& safearray) -> std::vector<SafearrayDimBounds>;

/// Check that a safearray contains elements of a specific type, and has a specific number of dimensions.
/// An exception is thrown if either constraint is not met.
///
/// @param[in] safearray  The SAFEARRAY. An exception is thrown if the variant does not contain a safearray.
/// @param[in] elem_type  The expected safearray element type.
/// @param[in] dimensions  The expected number of dimensions.
///
void check_safearray(const VARIANT& safearray, VARTYPE elem_type, size_t dimensions = 1);

/// Check that a safearray contains elements which have one of a specific set of types, and has a specific 
/// number of dimensions. An exception is thrown if either constraint is not met.
///
/// @param[in] safearray  The SAFEARRAY. An exception is thrown if the variant does not contain a safearray.
/// @param[in] elem_types  The set of acceptable safearray element type.
/// @param[in] dimensions  The expected number of dimensions.
///
void check_safearray(const VARIANT& safearray, const std::set<VARTYPE>& elem_types, size_t dimensions = 1);

/// Create a one-dimensional SAFEARRAY instance
///
/// @tparam[in]  elem_type  The VARIANT type ID of the SAFEARRAY elements (eg VT_I4 for element type LONG)
/// @param[in] lo_bound  The lower bound of the dimension
/// @param[in] num_elems  The number of elements in the dimension
/// @return  The SAFEARRAY
///
SAFEARRAY* make_1d_safearray(VARTYPE elem_type, LONG lo_bound, ULONG num_elems);

/// Create a two-dimensional SAFEARRAY instance
///
/// @tparam  elem_type  The VARIANT type ID of the SAFEARRAY elements (eg VT_I4 for element type LONG)
/// @param[in] lo_bound1  The lower bound of the first dimension
/// @param[in] num_elems1  The number of elements in the first dimension
/// @param[in] lo_bound2  The lower bound of the second dimension
/// @param[in] num_elems2  The number of elements in the second dimension
/// @return  The SAFEARRAY
///
SAFEARRAY* make_2d_safearray(VARTYPE sarr_elem_type, Ssize lo_bound1, Ssize num_elems1, Ssize lo_bound2, 
		Ssize num_elems2);

/*! Copy the elements of a SAFEARRAY to an STL-like container; the SAFEARRAY must be "1D-like", ie must have either one 
    dimension (a vector), or two, with one of the dimensions sized one (a matrix with only one row or only one column).
    \tparam SarrElem  The type of the safearray elements (a COM type such as LONG)
    \tparam ContElem  The type of the container elements; it must be possible to cast SarrElem values (using 
                       static_cast) to this type of values
    \tparam ContOutIt  Output iterator type, for inserting elements into the container
    \param[in] safearray  The SAFEARRAY
    \param[in] cont_out_it  Output iterator addressing the first element to write in the container
 */
template<typename SarrElem, class ContElem, class ContOutIt>
auto copy_from_1dlike_safearray(gsl::not_null<SAFEARRAY*> safearray, ContOutIt cont_out_it) -> void;

/*! Copy the elements of a SAFEARRAY to an STL-like container; the SAFEARRAY must be "1D-like", ie must have either one 
    dimension (a vector), or two, with one of the dimensions sized one (a matrix with only one row or only one column).
    \tparam  SarrElem  The type of the safearray elements (a COM type such as LONG)
    \tparam  ContElem  The type of the container elements; it must be possible to cast SarrElem values (using 
                       static_cast) to this type of values
    \tparam  ContOutIt  Output iterator type, for inserting elements into the container
    \param[in]  safearray  The SAFEARRAY
    \param[in]  cont_out_it  Output iterator addressing the first element to write in the container
 */
template<typename SarrElem, class ContElem, class ContOutIt>
auto copy_from_1dlike_safearray(const VARIANT& safearray, ContOutIt cont_out_it) -> void;

/*! Copy the numeric elements of a SAFEARRAY to an STL-like container; the SAFEARRAY must have either one dimension (a 
    vector), or two, with one of the dimensions sized one (a matrix with only one row or only one column). 
    The function will detect automatically the numeric type of the safearray elements and will silently cast the values 
    to the container's type; it is up to the caller to ensure that this makes sense. An exception will be thrown if the 
    safearray does not store numeric values.  
    \tparam ContElem  The type of the container elements; it must be possible to cast the safearray element values 
                      (using static_cast) to this type of values
    \tparam ContOutIt  Output iterator type, for inserting elements into the container
    \param[in] safearray  The SAFEARRAY
    \param[in] cont_out_it  Output iterator addressing the first element to write in the container
 */
template<class ContElem, class ContOutIt>
auto copy_from_1dlike_numeric_safearray(gsl::not_null<SAFEARRAY*> safearray, ContOutIt cont_out_it) -> void;

/// Copy the elements of a SAFEARRAY to a matrix-like container.
///
/// @tparam SarrElem  The type of the safearray elements (a COM type such as LONG)
/// @tparam ContElem  The type of the container elements; it must be possible to cast SarrElem values (using 
///					  static_cast) to this type of values
/// @param[in] safearray  The SAFEARRAY
///
template<typename SarrElem, class ContElem> 
[[nodiscard]] auto copy_from_2d_safearray(const VARIANT& safearray) -> twist::math::FlatMatrix<ContElem>;

/// Copy the elements of a 2D SAFEARRAY to a matrix-like container.
///
/// @tparam  ContElem  The type of the container elements; it must be possible to cast the safearray 
///					element values (using static_cast) to this type of values
/// @param[in] safearray  The SAFEARRAY
///
template<class ContElem>
twist::math::FlatMatrix<ContElem> copy_from_2d_numeric_safearray(const VARIANT& safearray);

/// Create a one-dimensional SAFEARRAY and and fill it with elements read from an STL-like container. The 
/// SAFEARRAY will have the same size as the input container.
///
/// @tparam  SarrElem  The type of the SAFEARRAY elements (a COM type such as LONG)
/// @tparam  sarr_elem_type  The VARIANT type ID of the SAFEARRAY elements (eg VT_I4 for element 
///					type LONG)
/// @tparam  Cont  The input container type
/// @param[in] cont  The input container 
/// @return  The one-dimensional SAFEARRAY, wrapped in a VARIANT
///
template<typename SarrElem, VARTYPE sarr_elem_type, class Cont>
VARIANT copy_to_1d_safearray(const Cont& cont);

/*! Create a one-dimensional SAFEARRAY and and fill it with elements read from an STL-like container, to which a 
    transformation is first applied. The SAFEARRAY will have the same size as the input container.
    \tparam  SarrElem  The type of the SAFEARRAY elements (a COM type such as LONG) 
    \tparam  sarr_elem_type  The VARIANT type ID of the SAFEARRAY elements (eg VT_I4 for element type LONG)
    \tparam  Cont  The input container type
    \tparam  TFunc  The transformation type; a functor type is expected which takes a container element and returns a 
                     SAFEARRAY element
    \param[in]  cont  The input container 
    \param[in]  transform_elem  The element transformation functor
    \return  The one-dimensional SAFEARRAY
 */
template<typename SarrElem, VARTYPE sarr_elem_type, class Cont, typename TFunc>
[[nodiscard]] auto transform_to_1d_rawsafearray(const Cont& cont, TFunc transform_elem) -> SAFEARRAY*;

/*! Create a one-dimensional SAFEARRAY and and fill it with elements read from an STL-like container, to which a 
    transformation is first applied. The SAFEARRAY will have the same size as the input container.
    \tparam  SarrElem  The type of the SAFEARRAY elements (a COM type such as LONG) 
    \tparam  sarr_elem_type  The VARIANT type ID of the SAFEARRAY elements (eg VT_I4 for element type LONG)
    \tparam  Cont  The input container type
    \tparam  TFunc  The transformation type; a functor type is expected which takes a container element and returns a 
                     SAFEARRAY element
    \param[in]  cont  The input container 
    \param[in]  transform_elem  The element transformation functor
    \return  The one-dimensional SAFEARRAY, wrapped in a VARIANT
 */
template<typename SarrElem, VARTYPE sarr_elem_type, class Cont, typename TFunc>
[[nodiscard]] auto transform_to_1d_safearray(const Cont& cont, TFunc transform_elem) -> VARIANT;

/// Create a two-dimensional SAFEARRAY and and fill it with elements read from an STL-like container. 
/// The SAFEARRAY's first dimension will have size one, while the second dimension will have the same size as 
/// the input container.
///
/// @tparam  SarrElem  The type of the SAFEARRAY elements (a COM type such as LONG) 
/// @tparam  sarr_elem_type  The VARIANT type ID of the SAFEARRAY elements (eg VT_I4 for element 
///					type LONG)
/// @tparam  Cont  The input container type
/// @param[in] cont  The input container 
/// @return  The two-dimensional SAFEARRAY, wrapped in a VARIANT
///
template<typename SarrElem, VARTYPE sarr_elem_type, class Cont> 
VARIANT copy_to_1dlike_2d_safearray(const Cont& cont);

/// Create a two-dimensional SAFEARRAY and and fill it with elements read from an STL-like container, to which 
/// a transformation is first applied. The SAFEARRAY's first dimension will have size one, while the second 
/// dimension will have the same size as the input container.
///
/// @tparam  SarrElem  The type of the SAFEARRAY elements (a COM type such as LONG) 
/// @tparam  sarr_elem_type  The VARIANT type ID of the SAFEARRAY elements (eg VT_I4 for element type LONG)
/// @tparam  Cont  The input container type
/// @tparam  TFunc  The transformation type; a functor type is expected which takes a container element and 
///					returns a SAFEARRAY element
/// @param[in] cont  The input container 
/// @param[in] transform_elem  The element transformation functor
/// @return  The two-dimensional SAFEARRAY, wrapped in a VARIANT
///
template<typename SarrElem, VARTYPE sarr_elem_type, class Cont, typename TFunc> 
VARIANT transform_1dlike_to_2d_safearray(const Cont& cont, TFunc transform_elem);

/// Fill an existing two-dimensional SAFEARRAY with elements read from matrix container. 
///
/// @tparam  SarrElem  The type of the SAFEARRAY elements (a COM type such as LONG) 
/// @tparam  ContElem  The input matrix container element type
/// @param[in] cont  The input matrix container 
/// @param[in] safearray  The two-dimensional SAFEARRAY, wrapped in a VARIANT; an exception is thrown if its 
///					dimensions do not match the matrix dimensions
///
template<typename SarrElem, class ContElem> 
void copy_to_2d_safearray(const twist::math::FlatMatrix<ContElem>& cont, const VARIANT& safearray);

} 

#include "twist/com/safearray_utils.ipp"

#endif 


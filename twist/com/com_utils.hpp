/// @file com_utils.hpp
/// Global utilities for working in the Microsoft COM world

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_COM_COM__UTILS_HPP
#define TWIST_COM_COM__UTILS_HPP

#include "twist/DateTime.hpp"

#include "twist/com/com_globals.hpp"

namespace twist::com {

/*! Initialises the COM library on the current thread.
    \param[in] multi_threaded  If true, identifies the concurrency model as multi-threaded; otherwise as single-thread 
                               apartment (STA).
 */
auto com_initialise(bool multi_threaded = false) -> void;

/// Closes the COM library on the current thread, unloads all DLLs loaded by the thread, frees any other 
/// resources that the thread maintains, and forces all RPC connections on the thread to close.
///
void com_uninitalise(); //+TODO: Typo

//! Cast a bool to a VARIANT_BOOL value.
[[nodiscard]] auto to_varbool(bool b) -> VARIANT_BOOL;

//! Cast a VARIANT_BOOL to a bool value.
[[nodiscard]] auto varbool_to_bool(VARIANT_BOOL vb) -> bool;

/// Convert a COM BSTR to a  std::wstring  object.
///
/// @param[in] bstr  The input
/// @return  The output
///
std::wstring bstr_to_string(BSTR bstr);

/// Convert a COM BSTR wrapper to a  std::wstring  object.
///
/// @param[in] bstr  The input BSTR wrapper
/// @return  The output string
///
std::wstring bstr_to_string(const _bstr_t& bstr);

/// Convert a BSTR to a fs::path object.
///
/// @param[in] bstr  The input BSTR path
/// @return  The output path
///
fs::path bstr_to_path(BSTR bstr);

/// Create a string view of a BSTR.
///
/// @param[in] bstr  The input BSTR
/// @return  The output view
///
std::wstring_view view(BSTR bstr);

/// Create a string view of a BSTR.
///
/// @param[in] bstr  The input BSTR
/// @return  The output view
///
std::wstring_view view(const _bstr_t& bstr);

/// Convert a COM BSTR to a std::string object.
///
/// @param[in] bstr  The input.
/// @return  The output.
///
std::string bstr_to_ansi(BSTR bstr);

/// Convert a  std::wstring_view  object to a BSTR.
///
/// @param[in] str  The input string
/// @return  The output string; it must be freed later with SysFreeString()
///
BSTR string_to_bstr(std::wstring_view str);

/// Convert a  fs::path  object to a BSTR.
///
/// @param[in] path  The input path
/// @return  The output string; it must be freed later with SysFreeString()
///
BSTR path_to_bstr(const fs::path& path);

/// Convert a  std::string_view  object to a BSTR.
///
/// @param[in] ansi_str  The input string
/// @return  The output string; it must be freed later with SysFreeString()
///
BSTR ansi_to_bstr(std::string_view ansi_str);

/// Convert a date value to a COM DATE value (the type used for storing a datetime value in a VARIANT).
///
/// @param[in] date  The date value
/// @return  The VARIANT DATE value
///
DATE date_to_vardate(const Date& date);

/// Convert a datetime value to a COM DATE value (the type used for storing a datetime value in a VARIANT).
///
/// @param[in] datetime  The datetime value
/// @return  The VARIANT DATE value
///
DATE datetime_to_vardate(const DateTime& datetime);

/// Convert a COM DATE (the type used for storing a datetime value in a VARIANT) value  to a datetime value.
///
/// @param[in] vardate  COM DATE value 
/// @return  The datetime value
///
DateTime vardate_to_datetime(DATE vardate);

/*! Retrieve the error description from an IErrorInfo interface.
    \param[in] err_info  The error interface
    \return  The description. An empty string is returned if the interface pointer passed in is a nullptr.
 */
[[nodiscard]] auto get_err_descr(IErrorInfo* err_info) -> std::wstring;

/// Check whether an HRESULT value returned by calling a COM method indicates success. If so, nothing is done. 
/// If the result is failure, a  twist::RuntimeError  instance is thrown. If generic IErrorInfo error 
///   information can be retrieved from the current thread, it will be included in the exception message.
/// Do not use this function directly, it is a helper function for the  TWIST_COM_CHECK_HR  macro.
///
/// @param[in] hr  The HRESULT value.
/// @param[in] funcName  Name of the function within which the COM method was called. This is only used (as 
///					part of the exception message) if IErrorInfo error info cannot be retrieved.
///
void check_hr_ierror(HRESULT hr, const wchar_t* func_name);

/// Generate a COM error/exception with the information contained in an internal exception object.
///
/// @tparam  ComClass  The type of the COM object which is to generate the COM error
/// @param[in] ex  The internal exception instance.
/// @param[in] com_obj  The COM object which is to generate the COM error.
/// @param[in] iid  The ID of the COM interface which is to generate the COM error (usually the main 
///					interface of ComClass)
///
template<class ComClass> 
HRESULT generate_com_error(const std::exception& ex, gsl::not_null<ComClass*> com_obj, const GUID& iid);

}

/*! Throw a RuntimeError object if the HRESULT returned by a COM method indicates failure. 
    Includes an error message and the calling function name.
 */
#define TWIST_COM_CHECK_HR(hr)  twist::com::check_hr_ierror(hr, __FUNCTIONW__);

//! Definition for the (non-ATL) COM smart pointer type built into the Visual Studio compiler
#ifdef _COM_SMARTPTR_LEVEL2
	#define TWIST_COM_SMARTPTR(Interface)  _COM_SMARTPTR<_COM_SMARTPTR_LEVEL2<Interface, &__uuidof(Interface)>> 
#else
	#define TWIST_COM_SMARTPTR(Interface)  _COM_SMARTPTR<Interface, &__uuidof(Interface)> 
#endif 

namespace twist::com {

/*! Create and default-initialise a single COM object, and query it for a specific interface.
    \tparam Interf  The interface to be used to communicate with the object
    \tparam CoClass  The coclass type; must implement Interf
    \return  0. Interface to the new COM object, wrapped in a non-ATL COM smart pointer
             1. Pointer to the new COM object (no ownership involved)
 */
template<class Interf, class CoClass>
[[nodiscard]] auto create_com_smartptr() -> TWIST_COM_SMARTPTR(Interf);

/*! Query a COM interface, wrapped in a non-ATL COM smart pointer (the destination interface) from another (the source 
    interface); effectivelly a non-ATL COM smart pointer cast. If the cast fails (the destination interface cannot be 
    found by querying the source interface), an exception is thrown.
    \tparam DestIntf  The destination COM interface type
    \tparam SrcIntf  The source COM interface type
    \param[in] src  The source interface 
 */
template<class DestIntf, class SrcIntf> 
[[nodiscard]] auto query_com_smartptr(TWIST_COM_SMARTPTR(SrcIntf) src) -> TWIST_COM_SMARTPTR(DestIntf); 

}

//! Catch an exception of type _com_error and rethrow it as a standard "twist" library exception
#define TWIST_RETHROW_COM_ERROR \
			catch (_com_error& ex) { \
				TWIST_VTHRO2(twist::com::get_err_descr(ex.ErrorInfo())); \
			} 			

//! Catch a _com_error exception object and rethrow it as a  RuntimeError  exception object.
#define TWIST_COM_RETHROW \
			catch (_com_error& ex) { \
				TWIST_THRO2(L"A COM exception occurred: \n    {}", twist::com::get_err_descr(ex.ErrorInfo())); \
			}  

/*! Given an exception object, generate a COM error for a COM object implementing ISupportErrorInfo by setting the 
    exception error message on the object and returning the E_FAIL HRESULT.
    Uses IDispatchImpl::_tih, a member of one of the standard base classes of a COM class, to obtain the IID.
    Devnote: Incomplete, probably inexact explanation  30Oct'18
 */
#define TWIST_GENERATE_COM_ERROR(ex) \
			twist::com::generate_com_error(ex, gsl::not_null{this}, *this->_tih.m_pguid);

#include "twist/com/com_utils.ipp"

#endif 

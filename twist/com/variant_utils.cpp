/// @file variant_utils.cpp
/// Implementation file for "variant_utils.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/com/variant_utils.hpp"

#include "twist/com/com_utils.hpp"

#include "twist/Date.hpp"
#include "twist/DateTime.hpp"

namespace twist::com {

std::wstring variant_to_string(const VARIANT& var)
{
	if (V_VT(&var) != VT_BSTR) {
		TWIST_THROW(L"The variant does not contain a BSTR value.");
	}
	return bstr_to_string(static_cast<_bstr_t>(var));
}


VARIANT string_to_variant(const std::wstring& str)
{
	VARIANT var;
	::VariantInit(&var);	
	V_VT(&var) = VT_BSTR;
	V_BSTR(&var) = string_to_bstr(str);
	return var;
}


VARIANT ansi_to_variant(const std::string& str)
{
	VARIANT var;
	::VariantInit(&var);	
	V_VT(&var) = VT_BSTR;
	V_BSTR(&var) = ansi_to_bstr(str);
	return var;
}


fs::path variant_to_path(const VARIANT & var)
{
	if (V_VT(&var) != VT_BSTR) {
		TWIST_THROW(L"The variant does not contain a BSTR value.");
	}
	return bstr_to_path(static_cast<_bstr_t>(var));
}


bool variant_to_bool(const VARIANT& var)
{
	if (V_VT(&var) != VT_BOOL) {
		TWIST_THROW(L"The variant does not contain a VARIANT_BOOL value.");
	}
	return varbool_to_bool(V_BOOL(&var));
}


LONG variant_to_long(const VARIANT& var)
{
	if (V_VT(&var) != VT_I4) {
		TWIST_THROW(L"The variant does not contain a LONG value.");
	}
	return V_I4(&var);
}


VARIANT long_to_variant(LONG val)
{
	VARIANT var;
	::VariantInit(&var);	
	V_VT(&var) = VT_I4;
	V_I4(&var) = val;
	return var;
}


ULONG variant_to_ulong(const VARIANT& var)
{
	if (V_VT(&var) != VT_UI4) {
		TWIST_THROW(L"The variant does not contain a ULONG value.");
	}
	return V_UI4(&var);

}


FLOAT variant_to_float(const VARIANT& var)
{
	if (V_VT(&var) != VT_R4) {
		TWIST_THROW(L"The variant does not contain a FLOAT value.");
	}
	return var.fltVal;
}


VARIANT float_to_variant(FLOAT val)
{
	VARIANT var;
	::VariantInit(&var);	
	V_VT(&var) = VT_R4;
	V_R4(&var) = val;
	return var;
}


DOUBLE variant_to_double(const VARIANT& var)
{
	if (V_VT(&var) != VT_R8) {
		TWIST_THROW(L"The variant does not contain a DOUBLE value.");
	}
	return var.dblVal;
}


VARIANT double_to_variant(DOUBLE val)
{
	VARIANT var;
	::VariantInit(&var);	
	V_VT(&var) = VT_R8;
	V_R8(&var) = val;
	return var;
}


VARIANT idisp_to_variant(IDispatch* val)
{
	VARIANT var;
	::VariantInit(&var);	
	V_VT(&var) = VT_DISPATCH;
	V_DISPATCH(&var) = val;
	return var;
}


VARIANT safearray_to_variant(SAFEARRAY& val, VARTYPE elem_vartype)
{
	VARIANT var;
	::VariantInit(&var);				
	V_ARRAY(&var) = &val;
	V_VT(&var) = static_cast<VARTYPE>(VT_ARRAY | elem_vartype); 
	return var;	
}


VARIANT vardate_to_variant(DATE vardate)
{
	VARIANT var{};
	::VariantInit(&var);
	V_VT(&var) = VT_DATE;
	V_DATE(&var) = vardate;
	return var;
}

VARIANT date_to_variant(const Date& date)
{
	return vardate_to_variant(date_to_vardate(date));
}

auto datetime_to_variant(const DateTime& datetime) -> VARIANT
{
	return vardate_to_variant(datetime_to_vardate(datetime));
}

auto variant_to_datetime(const VARIANT& var) -> DateTime
{
	if (V_VT(&var) != VT_DATE) {
		TWIST_THROW(L"The variant object does not contain a DATE value.");
	}
	return vardate_to_datetime(V_DATE(&var));
}

}


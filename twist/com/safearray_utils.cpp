/// @file safearray_utils.cpp
/// Implementation file for "safearray_utils.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "safearray_utils.hpp"

#include "com_utils.hpp"

namespace twist::com {

namespace detail {  // Local code

void mangled_cell_index_to_row_col(int mangled_cell_idx, int row_count, int col_count, 
		int& row, int& col)
{
	int full_cols = mangled_cell_idx / row_count;
	if (full_cols >= col_count) {
		TWIST_THROW(L"Invalid spatial grid cell index %d.", mangled_cell_idx);
	} 
	row = mangled_cell_idx - full_cols * row_count;
	col = full_cols;
}

}

size_t get_safearray_size(const VARIANT& safearray, size_t dimension)
{
	if ((V_VT(&safearray) & VT_ARRAY) == 0) {
		TWIST_THROW(L"The variant does not represent a safearray.");
	}

	const size_t dims = ::SafeArrayGetDim(V_ARRAY(&safearray));
	if (dims < dimension) {
		TWIST_THROW(L"Invalid dimension %d; the safearray is %d-dimensional.", dimension, dims);
	}	

	LONG lo = 0;
	LONG hi = 0;
	TWIST_COM_CHECK_HR(::SafeArrayGetLBound(V_ARRAY(&safearray), 1, &lo));
	TWIST_COM_CHECK_HR(::SafeArrayGetUBound(V_ARRAY(&safearray), 1, &hi));

	return hi - lo;
}

auto get_safearray_elem_type(gsl::not_null<SAFEARRAY*> safearray) -> VARTYPE
{
	auto var_ype = VARTYPE{0};
	TWIST_COM_CHECK_HR(::SafeArrayGetVartype(safearray, &var_ype)); 
	return var_ype;
}

auto count_safearray_dimensions(gsl::not_null<SAFEARRAY*> safearray) -> size_t
{
	return ::SafeArrayGetDim(safearray);
}

size_t count_safearray_dimensions(const VARIANT& safearray)
{
	if ((V_VT(&safearray) & VT_ARRAY) == 0) {
		TWIST_THROW(L"The variant does not represent a safearray.", V_VT(&safearray));
	}
	return count_safearray_dimensions(V_ARRAY(&safearray));
}

auto get_safearray_dimensions_bounds(gsl::not_null<SAFEARRAY*> safearray) -> std::vector<SafearrayDimBounds>
{
	const auto dims = count_safearray_dimensions(safearray);
	auto dims_bounds = reserve_vector<SafearrayDimBounds>(dims);
	for (auto i = 1u; i <= dims; ++i) {
		LONG lo = 0;
		LONG hi = 0;
		TWIST_COM_CHECK_HR(::SafeArrayGetLBound(safearray, i, &lo));
		TWIST_COM_CHECK_HR(::SafeArrayGetUBound(safearray, i, &hi));
		dims_bounds.emplace_back(lo, hi);
	}
	return dims_bounds;
}

auto get_safearray_dimensions_bounds(const VARIANT& safearray) -> std::vector<SafearrayDimBounds>
{
	if ((V_VT(&safearray) & VT_ARRAY) == 0) {
		TWIST_THROW(L"The variant does not represent a safearray.", V_VT(&safearray));
	}
	return get_safearray_dimensions_bounds(V_ARRAY(&safearray));
}

void check_safearray(const VARIANT& safearray, VARTYPE elem_type, size_t dimensions)
{
	check_safearray(safearray, make_set<VARTYPE>(elem_type), dimensions);
}


void check_safearray(const VARIANT& safearray, const std::set<VARTYPE>& elem_types, size_t dimensions)
{
	if ((V_VT(&safearray) & VT_ARRAY) == 0) {
		TWIST_THROW(L"The variant does not represent a safearray.");
	}

	VARTYPE var_ype = 0;
	TWIST_COM_CHECK_HR(::SafeArrayGetVartype(V_ARRAY(&safearray), &var_ype)); 
	if (elem_types.count(var_ype) == 0) {
		TWIST_THROW(L"The safearray does not have the correct type of elements.");
	}

	const size_t dims = ::SafeArrayGetDim(V_ARRAY(&safearray));
	if (dims != dimensions) {
		TWIST_THROW(L"The safearray is %d-dimensional, while being expected to be %d-dimensional.", dims, dimensions);
	}	
}


SAFEARRAY* make_1d_safearray(VARTYPE elem_type, LONG lo_bound, ULONG num_elems)
{
    SAFEARRAYBOUND bound;
    bound.lLbound   = lo_bound;
    bound.cElements = num_elems;

    SAFEARRAY* safearray = ::SafeArrayCreate(elem_type, 1, &bound);
	if (safearray == nullptr) {
		TWIST_THROW(L"Error creating one-dimensional SAFEARRAY instance.");
	}

	return safearray;
}


SAFEARRAY* make_2d_safearray(VARTYPE safearr_elem_type, Ssize lo_bound1, Ssize num_elems1, 
		Ssize lo_bound2, Ssize num_elems2)
{
	SAFEARRAYBOUND bounds[2];
	bounds[0].lLbound = static_cast<LONG>(lo_bound1);
	bounds[0].cElements = static_cast<ULONG>(num_elems1);  // rows
	bounds[1].lLbound = static_cast<LONG>(lo_bound2);
	bounds[1].cElements = static_cast<ULONG>(num_elems2);  // columns

	SAFEARRAY* safearray = ::SafeArrayCreate(safearr_elem_type, 2, bounds);
	if (safearray == nullptr) {
		TWIST_THROW(L"Error creating two-dimensional SAFEARRAY instance.");
	}

	return safearray;
}

} 

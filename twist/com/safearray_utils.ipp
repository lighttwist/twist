/// @file safearray_utils.ipp
/// Inline implementation file for "safearray_utils.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace twist::com {

namespace detail { 

void mangled_cell_index_to_row_col(int mangled_cell_idx, int row_count, int col_count, int& row, int& col);

}

template<typename SarrElem,  class ContElem, class ContOutIt>
auto copy_from_1dlike_safearray(gsl::not_null<SAFEARRAY*> safearray, ContOutIt cont_out_it) -> void
{
	// Check the safearray dimensions
	const auto dims_bounds = get_safearray_dimensions_bounds(safearray);
	const size_t num_dims = dims_bounds.size();
	if (num_dims != 1 && num_dims != 2) {
		TWIST_THROW(L"The array has %d dimensions; it is expected to be either one- or two-dimensional.", num_dims);
	}
	if (num_dims == 2 && length(dims_bounds[0]) != 0 && length(dims_bounds[1]) != 0) {
		TWIST_THROW(L"The array is two-dimensional, and neither of the dimensions is of size one.");
	}

	// Get the bounds of the dimension of interest
	LONG lo = dims_bounds[0].lo();
	LONG hi = dims_bounds[0].hi();	
	if (num_dims == 2 && length(dims_bounds[0]) == 0) {
		lo = dims_bounds[1].lo();
		hi = dims_bounds[1].hi();	
	}

	// Copy the elements into the target container; it's okay to go and grab them straight from memory, as 
	// there is really only one contiguous array of numbers
	SarrElem HUGEP* raw_data = nullptr;
	TWIST_COM_CHECK_HR(::SafeArrayAccessData(safearray, reinterpret_cast<void HUGEP**>(&raw_data)));
	for (LONG i = 0; i <= hi - lo; ++i) {
		*cont_out_it++ = static_cast<ContElem>(raw_data[i]);
	}
	TWIST_COM_CHECK_HR(::SafeArrayUnaccessData(safearray));
}

template<typename SarrElem,  class ContElem, class ContOutIt>
auto copy_from_1dlike_safearray(const VARIANT& safearray, ContOutIt cont_out_it) -> void
{
	if ((V_VT(&safearray) & VT_ARRAY) == 0) {
		TWIST_THROW(L"The variant does not represent a safearray.", V_VT(&safearray));
	}
	return copy_from_1dlike_safearray<SarrElem, ContElem, ContOutIt>(V_ARRAY(&safearray), cont_out_it);
}

template<class ContElem, class ContOutIt>
auto copy_from_1dlike_numeric_safearray(gsl::not_null<SAFEARRAY*> safearray, ContOutIt cont_out_it) -> void
{
	const auto var_ype = get_safearray_elem_type(safearray);
	switch (var_ype) {		
	case VT_I1 : 
		copy_from_1dlike_safearray<std::int8_t, ContElem>(safearray, cont_out_it);
		break;	
	case VT_UI1 : 
		copy_from_1dlike_safearray<std::uint8_t, ContElem>(safearray, cont_out_it);
		break;	
	case VT_I2 : 
		copy_from_1dlike_safearray<std::int16_t, ContElem>(safearray, cont_out_it);
		break;	
	case VT_UI2 : 
		copy_from_1dlike_safearray<std::uint16_t, ContElem>(safearray, cont_out_it);
		break;	
	case VT_I4 : 
		copy_from_1dlike_safearray<std::int32_t, ContElem>(safearray, cont_out_it);
		break;	
	case VT_UI4 : 
		copy_from_1dlike_safearray<std::uint32_t, ContElem>(safearray, cont_out_it);
		break;	
	case VT_R4 : 
		copy_from_1dlike_safearray<float, ContElem>(safearray, cont_out_it);
		break;	
	case VT_R8 : 
		copy_from_1dlike_safearray<double, ContElem>(safearray, cont_out_it);
		break;	
	default : 
		TWIST_THROW(L"The safearray element type (value %d) is not numeric.", var_ype);
	}
}

template<typename SarrElem, class ContElem> 
[[nodiscard]] auto copy_from_2d_safearray(const VARIANT& safearray) -> twist::math::FlatMatrix<ContElem>
{
	// Check the safearray dimensions
	const auto dim_bounds = get_safearray_dimensions_bounds(safearray);
	const size_t num_dims = dim_bounds.size();
	if (num_dims != 2) {
		TWIST_THROW(L"The array has %d dimensions; it is expected to be two-dimensional.", num_dims);
	}
	const auto dim1_length = length(dim_bounds[0]) + 1;
	const auto dim2_length = length(dim_bounds[1]) + 1;
	if ((dim1_length == 0 && dim1_length != 0) || (dim1_length != 0 && dim2_length == 0)) {
		TWIST_THROW(L"Invalid array dimensions: one dimension length is zero and the other non-zero.");
	}
	if (dim1_length == 0 && dim2_length == 0) {
		return twist::math::FlatMatrix<ContElem>{};
	}	

	// Get the dimension bounds 
	const auto dim1_lo = dim_bounds[0].lo();
	const auto dim1_hi = dim_bounds[0].hi();
	const auto dim2_lo = dim_bounds[1].lo();
	const auto dim2_hi = dim_bounds[1].hi();

	twist::math::FlatMatrix<ContElem> matrix{dim1_length, dim2_length};

	// Copy the elements into the target container; it's okay to go and grab them straight from memory, as 
	// there is really only one contiguous array of values
	SarrElem HUGEP* raw_data = nullptr;
	TWIST_COM_CHECK_HR(::SafeArrayAccessData(V_ARRAY(&safearray), reinterpret_cast<void HUGEP**>(&raw_data)));

	int mangled_cell_idx = 0;
	for (auto i = 0; i <= dim1_hi - dim1_lo; ++i) {
		for (auto j = 0; j <= dim2_hi - dim2_lo; ++j) {

			int row = 0;
			int col = 0;
			detail::mangled_cell_index_to_row_col(
					mangled_cell_idx++, dim1_length, dim2_length, row, col);

			matrix(row, col) = static_cast<ContElem>(raw_data[i * dim2_length + j]);
		}
	}
	TWIST_COM_CHECK_HR(::SafeArrayUnaccessData(V_ARRAY(&safearray)));

	return matrix;
}

template<class ContElem>
twist::math::FlatMatrix<ContElem> copy_from_2d_numeric_safearray(const VARIANT& safearray)
{
	//+cjj merge this implementation with the implementation for copy_from_1dlike_numeric_safearray()
	const VARTYPE var_ype = get_safearray_elem_type(V_ARRAY(&safearray));
	switch (var_ype) {		
	case VT_I1: 
		return copy_from_2d_safearray<std::int8_t, ContElem>(safearray);
	case VT_UI1: 
		return copy_from_2d_safearray<std::uint8_t, ContElem>(safearray);
	case VT_I2: 
		return copy_from_2d_safearray<std::int16_t, ContElem>(safearray);
	case VT_UI2: 
		return copy_from_2d_safearray<std::uint16_t, ContElem>(safearray);
	case VT_I4: 
		return copy_from_2d_safearray<std::int32_t, ContElem>(safearray);
	case VT_UI4: 
		return copy_from_2d_safearray<std::uint32_t, ContElem>(safearray);
	case VT_R4: 
		return copy_from_2d_safearray<float, ContElem>(safearray);
	case VT_R8: 
		return copy_from_2d_safearray<double, ContElem>(safearray);
	default: 
		TWIST_THROW(L"The safearray element type (value %d) is not numeric.", var_ype);
	}
}

template<typename SarrElem, VARTYPE sarr_elem_type, class Cont>
VARIANT copy_to_1d_safearray(const Cont& cont)
{
	// Create the SAFEARRAY structure
    SAFEARRAY* safearray = make_1d_safearray(sarr_elem_type, 0, static_cast<ULONG>(cont.size()));

	// Populate the SAFEARRAY
	SarrElem HUGEP* elem_buffer = nullptr;
	TWIST_COM_CHECK_HR(::SafeArrayAccessData(safearray, reinterpret_cast<void HUGEP**>(&elem_buffer)));
	copy(cont, elem_buffer);
	TWIST_COM_CHECK_HR(::SafeArrayUnaccessData(safearray));

	// Return the SAFEARRAY into a VARIANT
	return safearray_to_variant(*safearray, sarr_elem_type);
}

template<typename SarrElem, VARTYPE sarr_elem_type, class Cont, typename TFunc>
[[nodiscard]] auto transform_to_1d_rawsafearray(const Cont& cont, TFunc transform_elem) -> SAFEARRAY*
{
	// Create the SAFEARRAY structure
    auto* safearray = make_1d_safearray(sarr_elem_type, 0, static_cast<ULONG>(cont.size()));

	// Populate the SAFEARRAY
	SarrElem HUGEP* elem_buffer = nullptr;
	TWIST_COM_CHECK_HR(::SafeArrayAccessData(safearray, reinterpret_cast<void HUGEP**>(&elem_buffer)));
	transform(cont, elem_buffer, transform_elem);
	TWIST_COM_CHECK_HR(::SafeArrayUnaccessData(safearray));

	return safearray;
}

template<typename SarrElem, VARTYPE sarr_elem_type, class Cont, typename TFunc>
[[nodiscard]] auto transform_to_1d_safearray(const Cont& cont, TFunc transform_elem) -> VARIANT
{
	// Create the SAFEARRAY structure
    SAFEARRAY* safearray = make_1d_safearray(sarr_elem_type, 0, static_cast<ULONG>(cont.size()));

	// Populate the SAFEARRAY
	SarrElem HUGEP* elem_buffer = nullptr;
	TWIST_COM_CHECK_HR(::SafeArrayAccessData(safearray, reinterpret_cast<void HUGEP**>(&elem_buffer)));
	transform(cont, elem_buffer, transform_elem);
	TWIST_COM_CHECK_HR(::SafeArrayUnaccessData(safearray));

	// Return the SAFEARRAY into a VARIANT
	return safearray_to_variant(*safearray, sarr_elem_type);
}

template<typename SarrElem, VARTYPE sarr_elem_type, class Cont> 
VARIANT copy_to_1dlike_2d_safearray(const Cont& cont)
{
	// Create the SAFEARRAY structure
	SAFEARRAY* safearray = make_2d_safearray(sarr_elem_type, 0, 1, 0, static_cast<ULONG>(cont.size()));

	// Populate the SAFEARRAY
	SarrElem HUGEP* elem_buffer = nullptr;
	TWIST_COM_CHECK_HR(::SafeArrayAccessData(safearray, reinterpret_cast<void HUGEP**>(&elem_buffer)));
	copy(cont, elem_buffer);
	TWIST_COM_CHECK_HR(::SafeArrayUnaccessData(safearray));

	// Return the SAFEARRAY into a VARIANT
	return safearray_to_variant(*safearray, sarr_elem_type);	
}

template<typename SarrElem, VARTYPE sarr_elem_type, class Cont, typename FuncT> 
VARIANT transform_to_1dlike_2d_safearray(const Cont& cont, FuncT transform_elem)
{
	// Create the SAFEARRAY structure
	SAFEARRAY* safearray = make_2d_safearray(sarr_elem_type, 0, 1, 0, static_cast<ULONG>(cont.size()));

	// Populate the SAFEARRAY
	SarrElem HUGEP* elem_buffer = nullptr;
	TWIST_COM_CHECK_HR(::SafeArrayAccessData(safearray, reinterpret_cast<void HUGEP**>(&elem_buffer)));
	transform(cont, elem_buffer, transform_elem);
	TWIST_COM_CHECK_HR(::SafeArrayUnaccessData(safearray));

	// Return the SAFEARRAY into a VARIANT
	return safearray_to_variant(*safearray, sarr_elem_type);	
}

template<typename SarrElem, class ContElem> 
void copy_to_2d_safearray(const twist::math::FlatMatrix<ContElem>& cont, const VARIANT& safearray)
{
	const auto dim_bounds = get_safearray_dimensions_bounds(safearray);
	if (dim_bounds.size() != 2) {
		TWIST_THROW(L"The SAFEARRAY passed is not 2-dimensional.");		
	}
	const auto dim1_length = length(dim_bounds[0]) + 1;
	const auto dim2_length = length(dim_bounds[1]) + 1;
	if (dim1_length != cont.nof_rows() || dim2_length != cont.nof_columns()) {
		TWIST_THROW(L"The SAFEARRAY has invalid dimensions (%dx%d).", dim1_length, dim2_length);
	}
	
	const auto dim1_lo = dim_bounds[0].lo();
	const auto dim1_hi = dim_bounds[0].hi();
	const auto dim2_lo = dim_bounds[1].lo();
	const auto dim2_hi = dim_bounds[1].hi();

	// Copy the elements into the target safearray; it's okay write them straight to memory, as there is 
	// really only one contiguous array of values
	SarrElem HUGEP* raw_data = nullptr;
	TWIST_COM_CHECK_HR(::SafeArrayAccessData(V_ARRAY(&safearray), reinterpret_cast<void HUGEP**>(&raw_data)));

	int mangled_cell_idx = 0;
	for (auto i = 0; i <= dim1_hi - dim1_lo; ++i) {
		for (auto j = 0; j <= dim2_hi - dim2_lo; ++j) {

			int row = 0;
			int col = 0;
			detail::mangled_cell_index_to_row_col(
					mangled_cell_idx++, dim1_length, dim2_length, row, col);

			raw_data[i * dim2_length + j] = cont(row, col);
		}
	}

	TWIST_COM_CHECK_HR(::SafeArrayUnaccessData(V_ARRAY(&safearray)));
}

}

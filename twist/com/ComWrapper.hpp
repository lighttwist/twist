/// @file ComWrapper.hpp
/// ComWrapper class template definition

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_COM_COM_WRAPPER_HPP
#define TWIST_COM_COM_WRAPPER_HPP

#include "com_utils.hpp"

namespace twist::com {

/// A light wrapper for a raw COM interface which exposes it as a _com_ptr_t class template object 
/// instantiated for that COM interface type. The usage is to derive from this class and only supply one or 
/// two constructors in the derived class, corresponding to the constructors in this class which are needed
/// for that specific wrapper.
///
/// @tparam  RawInterface  The raw COM interface name
///
template<typename RawInterface>
class ComWrapper {
public:
	/// The exposed COM interface type, an instantiation of _com_ptr_t 
	using Interface = TWIST_COM_SMARTPTR(RawInterface);

	/// Arrow operator, chains through to the underlying _com_ptr_t arrow operator.
	const Interface& operator->() const;

	/// The underlying _com_ptr_t object.
	const Interface& interf() const;

protected:
	/// Constructor. Use this constructor to create a new instance of a COM class which supports the
	/// interface RawInterface and wrap its interface.
	///
	/// @param[in] clsid  The CLSID of COM class; an exception is thrown if the COM class does not support
	///					the interface RawInterface.
	///
	ComWrapper(const CLSID& clsid);

	/// Constructor. Use this constructor to wrap an existing raw interface.
	///
	/// @param[in] interf  The raw interface pointer  
	///
	ComWrapper(gsl::not_null<RawInterface*> interf);

private:
	TWIST_CHECK_INVARIANT_DECL

	Interface  interf_{};
};

}

#include "ComWrapper.ipp"

#endif 

///  @file  ExplicitUuidType.hpp
///  ExplicitUuidType class template

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_EXPLICIT_UUID_TYPE_HPP
#define TWIST_EXPLICIT_UUID_TYPE_HPP

#include "os_utils.hpp"

namespace twist {

///
/// Class template that makes a UUID (universally unique identifier) type into an explicit (separate) type. 
///
///  @tparam  t_lib_id  The unique ID of the owner library
///	 @tparam  t_type_id  ID of the type; must be unique within the owner library
///
template<unsigned long t_lib_id, unsigned long t_type_id>
class ExplicitUuidType {
public:
	// The library ID
	static const unsigned long  k_lib_id  = t_lib_id;

	/// Constructor. Generates a new, valid UUID.
	///
	ExplicitUuidType();

	/// Constructor. Creates an object based on the textual representation of a UUID.
	///
	/// @param[in] uuid_str  The textual UUID
	///
	explicit ExplicitUuidType(const std::wstring& uuid_str);

	/// Constructor. Creates an object based on another, non-template UUID object.
	///
	/// @param[in] uuid  The UUID object
	///
	explicit ExplicitUuidType(const Uuid& uuid);

	/// Constructor. Creates an object based on another, non-template UUID object.
	///
	/// @param[in] uuid  The UUID object
	///
	template<unsigned long t_other_lib_id, unsigned long t_other_type_id> 
	explicit ExplicitUuidType(const ExplicitUuidType<t_other_lib_id, t_other_type_id>& uuid);
	
	/// Copy constructor.
	///
	ExplicitUuidType(const ExplicitUuidType& src);

	/// Move constructor.
	///
	ExplicitUuidType(ExplicitUuidType&& src);

	/// Assignment operator.
	///
	ExplicitUuidType& operator=(const ExplicitUuidType& rhs);

	/// Move-assignment operator.
	///
	ExplicitUuidType& operator=(ExplicitUuidType&& rhs);

	/// "Equal to" operator
	///
	bool operator==(const ExplicitUuidType& rhs) const;
	
	/// "Not equal to" operator.
	///
	bool operator!=(const ExplicitUuidType& rhs) const;
	
	/// "Smaller than" operator.
	///
	bool operator<(const ExplicitUuidType& rhs) const;

	/// Get the underlying UUID value.
	///
	/// @return  The UUID value
	///
	Uuid value() const;

private:	
	template<unsigned long t_lib_id, unsigned long t_type_id> friend std::wstring to_str(const ExplicitUuidType&, bool);

	Uuid  uuid_;
};

//
//  Free functions
//

/// Get the textual representation of a UUID.
///
/// @param[in] uuid  The UUID.
/// @param[in] brackets  Whether the guid string should be enclosed in curly brackets.
/// @return  The textual UUID.
///
template<unsigned long t_lib_id, unsigned long t_type_id>
std::wstring to_str(const ExplicitUuidType<t_lib_id, t_type_id>& uuid, bool brackets = false);

} 

#include "ExplicitUuidType.ipp"

#endif 

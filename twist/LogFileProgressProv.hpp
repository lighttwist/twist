///  @file  LogFileProgressProv.hpp
///  LogFileProgressProv class, inherits ProgressProv

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_LOG_FILE_PROGRESS_PROV_HPP
#define TWIST_LOG_FILE_PROGRESS_PROV_HPP

#include <fstream>

#include "ProgressProv.hpp"

namespace twist {

///
///  Progress providers that facilitate the indication of progress by outputting progress messages to a log file. 
///  It implements the part of the  ProgressProv  interface related to progress messages/history lines so that the 
///  messages/history lines are streamed into the log file. 
///
class LogFileProgressProv : public ProgressProv {
public:
	/*! Constructor. 
	    \param[in] log_path  The log file path; if a file with this filename exists, it will be deleted.
	 */
	LogFileProgressProv(const fs::path& log_path);
	
	virtual ~LogFileProgressProv();

	virtual void init_message_mode() override;  //  ProgressProv  override
	
	virtual void init_hist_mode(long expected_num_lines) override;  //  ProgressProv  override

	virtual void set_prog_msg(std::wstring_view msg, bool force_update) override;  //  ProgressProv  override

	virtual void add_new_hist_line(const std::wstring& hist_line) override;  //  ProgressProv  override

	virtual void set_present_hist_line(const std::wstring& hist_line) override;  //  ProgressProv  override

    virtual void hide() override;  //  ProgressProv  override

	virtual void get_prog_range(Ssize& min_pos, Ssize& max_pos) const override;  //  ProgProvBase  override

	virtual void set_prog_range(Ssize min_pos, Ssize max_pos) override;  //  ProgProvBase  override

	virtual Ssize get_prog_pos() const override;  //  ProgProvBase  override

	virtual void set_prog_pos(Ssize pos, bool force_update) override;  //  ProgProvBase  override

	virtual void inc_prog_pos(bool force_update) override;  //  ProgProvBase  override

private:		
	/// Find out whether this progress provider uses a log file.
	///
	/// @return  true only if a log file is to be used.
	///
	auto log_to_file() const -> bool;
	
	/// Open the log file.
	///
	/// @return  true only on success.
	///  
	bool open_log_file();
	
	/// Close the log file.
	/// If the log file is not currently open, this method does nothing.
	///
	void close_log_file();
	
	/// Write a line of text to the log file.
	/// If the log file is not currently open, this method does nothing.
	///
	void write_line_to_log_file(std::wstring_view line);

	// The log file name. Empty if the progress provider does not use a log file
	const fs::path log_path_;

	// Whether the log file is currently open for writing.
	bool log_file_open_;

	// Output stream used to write to the log file, if this is required.
	std::wofstream log_file_stream_;

	TWIST_CHECK_INVARIANT_DECL
};

} 

#endif 

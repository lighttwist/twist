///  @file  twist_stl.hpp 
///  Extensions to the STL: introduction of ranges; new iterators; new algorithms; range versions of the old
///  algorithms; utilities for working with containers, iterators, smart pointers, etc

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

#ifndef TWIST_STL_HPP
#define TWIST_STL_HPP

#include <set>
#include <unordered_set>
#include <vector>

#include "iterators.hpp"
#include "metaprogramming.hpp"
#include "ranges.hpp"

namespace twist {

/// Get the number of elements in a container as a 32-bit unsigned integer value, regardless of the platform.
///
/// @tparam  Cont  The container type
/// @param[in] cont  The container 
/// @return  The container size
///
template<class Cont>
uint32_t size32(const Cont& cont)
{
#if TWIST_OS_64BIT
  #ifndef _DEBUG
	return static_cast<uint32_t>(cont.size());
  #else
	const auto size = static_cast<uint32_t>(cont.size()); 
	if (size != cont.size()) {
		TWIST_THROW(L"Container size %u cannot be expressed as an unsigned 32-bit integer", cont.size());	
	}
	return size;
  #endif
#elif TWIST_OS_32BIT
	return cont.size();
#else
	static_assert(false, "Platform is neither 32-bit nor 64-bit.");
#endif
}

/// Get the number of elements in a container as a 32-bit signed integer value, regardless of the platform.
///
/// @tparam  Cont  The container type
/// @param[in] cont  The container 
/// @return  The container size
///
template<class Cont>
int32_t ssize32(const Cont& cont)
{
	const auto usize = size32(cont);
	const auto ssize = static_cast<int32_t>(usize);
	if (ssize < 0) {
		TWIST_THROW(L"Container size %u cannot be expressed as a signed 32-bit integer", cont.size());
	}
	return ssize;
}

/// Range-based version of std::transform() which writes the results of the transformation back to the input
/// range.
template<class Rng, 
         class UnaryOp,
		 class = EnableIfAnyRange<Rng>>
void transform_in_place(Rng& range, UnaryOp op)
{
	using std::begin;
	using std::end;
	std::transform(std::cbegin(range), std::cend(range), begin(range), op);
}

// --- std::map and std::multimap utilities ---

/*! Look for a key value in the map, and insert it if it does not already exist, paired with the default value.
    \note  This is very similar to what operator[] does, except the key value passed in does not get converted to the 
	       key type (if it has a different type) if the key already exists.
    \tparam Key  Key type 
    \tparam Value  Value type 
    \tparam Compare  Comparison callable type for sorting the keys 
    \tparam Allocator  Allocator type for map elements 
	\tparam K  The type of the key value passed in for the search; the search can be conducted without constructing an 
	           instance of Key (if different from K) if Compare::is_transparent is valid and denotes a type
	\param[in] map  The map
	\param[in] key  The key value to look for
	\return  Reference to the value corresponding to the key
 */
template<class Key,
         class Value,
         class Compare = std::less<Key>,
         class Allocator = std::allocator<std::pair<const Key, Value>>,
		 class K>
requires std::equality_comparable_with<Key, K> && std::default_initializable<Value>
auto find_or_insert(std::map<Key, Value, Compare, Allocator>& map, K key) -> Value& 
{
	auto it = map.find(key);
	return it != end(map) ? it->second
			              : map.emplace(key, Value{}).first->second;	
}

/// Given a map instance, this function creates a range object representing the map keys, accessible through 
/// constant iterators.
///
/// @tparam  Map  The map type
/// @param[in] map  The map
/// @return  The range
///
template<class Map>
Range<CiteratorFirst<typename Map::const_iterator>>
get_keys(const Map& map) //+OBSOLETE: Use rg::keys()
{
	return crange_first(map);
}

/// Overload making sure that the original function is not called with a rvalue reference, as that would result in 
/// undefined behaviour.
///
template<class Map>
Range<IteratorFirst<typename Map::const_iterator>>
get_keys(Map&& map) //+OBSOLETE: Use rg::keys()
{
	static_assert(always_false<Map>, "This function cannot be called with an rvalue reference.");
}

/// Given a map instance, this function creates a range object representing the map values, accessible through 
/// constant iterators.
///
/// @tparam  TMap  The map type
/// @param[in] map  The map
/// @return  The range
///
template<class TMap>
Range<CiteratorSecond<typename TMap::const_iterator>>
get_cvalues(const TMap& map)
{
	return crange_second(map);
}

/// Overload making sure that the original function is not called with a rvalue reference, as that would result in 
/// undefined behaviour.
///
template<class Map>
Range<CiteratorSecond<typename Map::const_iterator>>
get_cvalues(Map&& map)
{
	static_assert(always_false<Map>, "This function cannot be called with an rvalue reference.");
}

/// Given a map instance, this function creates a range object representing the map values, accessible through 
/// non-constant iterators.
///
/// @tparam  TMap  The map type
/// @param[in] map  The map
/// @return  The range
///
template<class TMap>
Range<IteratorSecond<typename TMap::iterator>>
get_values(TMap& map)
{
	return range_second(map);
}

/// Overload making sure that the original function is not called with a rvalue reference, as that would result in 
/// undefined behaviour.
///
template<class Map>
Range<IteratorSecond<typename Map::iterator>>
get_values(Map&& map)
{
	static_assert(always_false<Map>, "This function cannot be called with an rvalue reference.");
}

/// Given a map instance whose values are smart pointer instances (eg std::unique_ptr, std::shared_ptr) this function 
/// will extract the naked pointers from their wrappers and output them as constant pointers.
///
/// @tparam  Map  The map type
/// @tparam  OutIter  Output iterator type, addressing constant pointers
/// @param[in] map  The map
/// @param[in] out_it  The output iterator
/// 
template<class Map, typename OutIter>
void get_cvalue_ptrs(const Map& map, OutIter out_it) 
{
	using std::begin;
	using std::end;

	std::transform(begin(map), end(map), out_it, [](const auto& el) {
		return el.second.get();
	});
}

/// Given a map instance whose values are smart pointer instances (eg std::unique_ptr, std::shared_ptr) this 
/// function will extract the naked pointers from their wrappers and output them as const pointers in a vector 
/// instance.
///
/// @tparam  Map  The map type
/// @param[in] map  The map
/// @return  The vector
/// 
template<class Map>
std::vector<const typename Map::mapped_type::element_type*> get_cvalue_ptrs(const Map& map) 
{
	std::vector<const typename Map::mapped_type::element_type*> v;
	get_cvalue_ptrs(map, back_inserter(v));
	return v;
}

/// Given a map instance whose values are smart pointer instances (eg std::unique_ptr, std::shared_ptr) this 
/// function will extract the naked pointers from their wrappers and output them as pointers.
///
/// @tparam  Map  The map type
/// @tparam  OutIter  Output iterator type, addressing pointers
/// @param[in] map  The map
/// @param[in] out_it  The output iterator
/// 
template<class Map, typename OutIter>
void get_value_ptrs(Map& map, OutIter out_it) 
{
	using std::begin;
	using std::end;

	std::transform(begin(map), end(map), out_it, [](auto& el) {
		return el.second.get();
	});
}

/// Given a map instance whose values are smart pointer instances (eg std::unique_ptr, std::shared_ptr) this 
/// function will extract the naked pointers from their wrappers and output them as pointers in a vector 
/// instance.
///
/// @tparam  Map  The map type
/// @param[in] map  The map
/// @return  The vector
/// 
template<class Map>
std::vector<typename Map::mapped_type::element_type*> get_value_ptrs(Map& map) 
{
	std::vector<typename Map::mapped_type::element_type*> v;
	get_value_ptrs(map, back_inserter(v));
	return v;
}

/// Get the range of values in a multimap which match a specific key
///
/// @tparam  TMultimap  The multimap type
/// @param[in] multimap  The multimap
/// @param[in] key  The key
/// @return  A range based on constant iterators addressing the matching values.
///
template<class TMultimap>
Range<CiteratorSecond<typename TMultimap::const_iterator>> 
get_cvalues_for_key(const TMultimap& multimap, const typename TMultimap::key_type& key)
{
	auto lo_it = multimap.lower_bound(key);
	auto hi_it = multimap.upper_bound(key);
	return Range<CiteratorSecond<typename TMultimap::const_iterator>>(lo_it, hi_it);
}

/// Get the range of values in a multimap which match a specific key
///
/// @tparam  TMultimap  The multimap type
/// @param[in] multimap  The multimap
/// @param[in] key  The key
/// @return  A range based on iterators addressing the matching values.
///
template<class TMultimap>
Range<IteratorSecond<typename TMultimap::iterator>> 
get_values_for_key(TMultimap& multimap, const typename TMultimap::key_type& key)
{
	auto lo_it = multimap.lower_bound(key);
	auto hi_it = multimap.upper_bound(key);
	return Range<IteratorSecond<typename TMultimap::iterator>>(lo_it, hi_it);
}

/// Find an exact match for a key-value pair in a multimap.
///
/// @tparam  TMultimap  The multimap type
/// @param[in] multimap  The multimap
/// @param[in] key  The key
/// @param[in] value  The value
/// @return  An iterator addressing the matching element, or the end of the multimap if there is no match.
/// 
template<class TMultimap>
typename TMultimap::const_iterator find_pair(const TMultimap& multimap, 
		const typename TMultimap::key_type& key, const typename TMultimap::mapped_type& value)
{
	auto ret_it = end(multimap);

	const auto hi_it = multimap.upper_bound(key);
	const auto it = std::find_if(multimap.lower_bound(key), hi_it, [&value](const auto& el) { 
		return el.second == value; 
	});
	if (it != hi_it) {
		ret_it = it;
	}
	return ret_it;
}

/// Find an exact match for a key-value pair in a multimap.
///
/// @tparam  TMultimap  The multimap type
/// @param[in] multimap  The multimap
/// @param[in] key  The key
/// @param[in] value  The value
/// @return  An iterator addressing the matching element, or the end of the multimap if there is no match
/// 
template<class TMultimap>
typename TMultimap::iterator find_pair( TMultimap& multimap, const typename TMultimap::key_type& key, 
		const typename TMultimap::mapped_type& value)
{
	auto ret_it = end(multimap);

	const auto hi_it = multimap.upper_bound(key);
	const auto it = std::find_if(multimap.lower_bound(key), hi_it, [&value](const auto& el) { 
		return el.second == value; 
	});
	if (it != hi_it) {
		ret_it = it;
	}
	return ret_it;
}

/// Find an exact match for a key-value pair in a multimap.
///
/// @tparam  TMultimap  The multimap type
/// @param[in] multimap  The multimap
/// @param[in] key  The key
/// @param[in] value  The value
/// @return  true if a matching element is found
/// 
template<class TMultimap>
bool has_pair(const  TMultimap& multimap, const typename TMultimap::key_type& key, 
		const typename TMultimap::mapped_type& value)
{
	return find_pair(multimap, key, value) != end(multimap);
}

//   std::unique_ptr  helper functions  

/// Create a  std::unique_ptr  instance wrapping a pointer to an object's copy.
///
/// @tparam  T  The pointee type. Must be copy-constructable.
/// @param[in] p  Naked pointer to the object to be copied. Can be null. 
/// @return  The unique pointer instance.
/// 
template<typename T>
std::unique_ptr<T> make_uptr_to_copy(T* p)
{
	return std::unique_ptr<T>( p != nullptr ? new T(*p) : nullptr );
}

/// Allocate a copy of the object which an  unique_ptr  instance is pointing at, and wrap it in another  unique_ptr  
/// instance.
///
/// @tparam  T  The pointee type. Must be copy-constructable.
/// @tparam  TDeleter  Unique pointer deleter policy.
/// @param[in] src  The source unique pointer. If it is empty, then the destination pointer will be empty too.
///					It must wrap a normal pointer (as opposed to a pointer to an array).
/// @result  The destination unique pointer.
/// 
template<typename T, typename Deleter>
std::unique_ptr<T, Deleter> make_uptr_to_copy(const std::unique_ptr<T, Deleter>& src)
{
	return std::unique_ptr<T, Deleter>(src ? new T(*src) : nullptr);
}

/// Convert a  std::unique_ptr  templated on a base class, and wrapping a pointer to the base class, to a 
/// std::unique_ptr templated on a class derived from the base, and wrapping a pointer to the derived class.
/// You might think of it as a dynamic_cast for std::unique_ptr.
///
/// @tparam  Derived  The derived class type; there is a compile-time check to make sure this class really is 
///				derived from, or is the same as, the base class
/// @tparam  Base  The base class type
/// @param[in] ptr  Unique pointer to the base class
/// @return  Unique pointer to the derived class
///
template<class Derived, class Base>
std::unique_ptr<Derived> downcast_uptr(std::unique_ptr<Base> ptr)
{
	static_assert(std::is_base_of<Base, Derived>::value, "Invalid downcast.");

	Derived* derived = dynamic_cast<Derived*>(ptr.get());
	if (derived == nullptr && ptr.get() != nullptr) {
		throw std::runtime_error("Invalid downcast.");
	}
	ptr.release();	
	return std::unique_ptr<Derived>(derived);
}

/// Given a unique_ptr instance, move the contained pointer into a shared_ptr instance.
/// Syntactic sugar allowing the use of auto for declaring the shared_ptr instance.
///
/// @param[in] uptr  The unique_ptr instance; it will be reset
/// @return  The shared_ptr instance
///
template<typename T, typename Deleter>
std::shared_ptr<T> move_to_shared(std::unique_ptr<T, Deleter>& uptr)
{
	return std::shared_ptr<T>{ std::move(uptr) };  
}

//  gsl::non_null  helper functions

template<typename T> gsl::not_null<T> make_not_null(T&& x)
{
	return gsl::not_null<T>{ std::forward<T>(x) };
}


/***  std::set  helper functions  ***/

/// Create an  std::set  object containing one element.
///
/// @tparam  T   The set value type
/// @param[in] val  The element value
/// @return  The set
///
template<typename T>
std::set<T> make_set(const T& val)
{
	std::set<T> set;
	set.insert(val);
	return set;
}


/// Create an  std::set  object containing one element.
///
/// @tparam  T   The set value type
/// @param[in] val  The element value
/// @return  The set
///
template<typename T>
std::set<T> make_set(T&& val)
{
	std::set<T> set;
	set.insert(std::forward<T>(val));
	return set;
}

/***  Miscellanea  ***/

/*! Find out whether a range contains any elements with a specific value.
    \tparam Rng  Range type
    \tparam Elem  Element value type; must be equality comparable with the element type
    \param[in] range  The range
    \param[in] val  The value 
    \return  true if it does
 */
template<class Rng, class Elem>
[[nodiscard]] constexpr auto has(const Rng& range, const Elem& val) -> bool
{
	using std::begin;
	using std::end;

	const auto end_it = end(range);
	return std::find(begin(range), end_it, val) != end_it;
}

//! Specialisation of has() for std::set<>.
template<class Kty, class Pr, class Alloc, class Elem>
[[nodiscard]] constexpr auto has(const std::set<Kty, Pr, Alloc>& range, const Elem& val) -> bool
{
	return range.find(val) != range.end();
}

/*! Find out whether a range contains any elements which satisfy a specific condition.
    \tparam Rng  Range type
    \tparam Pred  Unary predicate type, which when applied to an element should return true if the condition is met 
    \param[in] range  The range
    \param[in] pred  The predicate
    \return true if it does
 */
template<class Rng, class Pred>
[[nodiscard]] constexpr auto has_if(const Rng& range, Pred pred) -> bool
{
	using std::begin;
	using std::end;

	const auto end_it = end(range);
	return std::find_if(begin(range), end_it, pred) != end_it;
}

/// Delete specific elements (which match a specific value) from a container, using the "remove-erase idiom".
/// @note  +OBSOLETE: Use std::erase() instead
/// @tparam  Cont   The container type
/// @param[in] cont  The container
/// @param[in] val  The value
/// @return  The number of elements deleted from the container
/// 
template<class Cont>  
Ssize remove_erase(Cont& cont, typename Cont::const_reference val)
{
	auto new_end = std::remove(std::begin(cont), std::end(cont), val);
	const auto num_elems = std::distance(new_end, std::end(cont));
	cont.erase(new_end, std::end(cont));
	return num_elems;
}

/// Delete specific elements (which match a specific predicate) from a container, using the "remove-erase idiom".
/// @note  +OBSOLETE: Use std::erase_if() instead
/// @tparam  Cont   The container type
/// @tparam  Pred   The predicate type - must be a unary predicate
/// @param[in] cont  The container
/// @param[in] pred  The predicate
/// @return  The number of elements deleted from the container
/// 
template<class Cont, 
         class Pred,
		 class = EnableIfPred<Pred, typename Cont::const_reference>>  
Ssize remove_erase_if(Cont& cont, Pred pred)
{
	auto new_end = std::remove_if(std::begin(cont), std::end(cont), pred);
	const auto num_elems = std::distance(new_end, std::end(cont));
	cont.erase(new_end, std::end(cont));
	return num_elems;
}

/*! Delete the element at a specific position in a container, and return its value.
    \tparam Cont   The container type
    \param[in] cont  The container 
	\param[in] pos  The position of the element to be deleted; if invalid, the behaviour is undefined
	\return  The value of the deleted element
 */
template<rg::input_range Cont>
auto extract_at(Cont& cont, Ssize pos) -> rg::range_value_t<Cont>
{
	using std::begin;
	assert(pos >= 0);

	auto it = begin(cont);
	std::advance(it, pos);
	const auto ret = *it;
	cont.erase(it);

	return ret;
}

/*! Find all elements which appear multiple times in a container.
    \tparam Cont   The container type
    \param[in] cont  The container 
    \return  A vector containing copies of the elements which appear multiple times, in the order they appear
 */
template<class Cont> 
[[nodiscard]] auto find_duplicates(const Cont& cont) -> std::vector<typename Cont::value_type>
{
	std::vector<typename Cont::value_type> duplicates; 
	std::unordered_set<typename Cont::value_type> uniques;
	for (const auto& el : cont) {
		if (!uniques.insert(el).second && !twist::has(duplicates, el)) {
			duplicates.push_back(el);
		}
	}
	return duplicates;
}

/*! Find the first element which appears multiple times in a container.
    \tparam Cont   The container type
    \param[in] cont  The container 
    \return  A copy of the first element which appears multiple times, or an empty pointer
 */
template<class Cont> 
[[nodiscard]] auto find_first_duplicate(const Cont& cont) -> std::unique_ptr<typename Cont::value_type>
{
	std::unordered_set<typename Cont::value_type> uniques;
	for (const auto& el : cont) {
		if (!uniques.insert(el).second) {
			return std::make_unique<typename Cont::value_type>(el);
		}
	}
	return nullptr;
}

/// Get the number of elements in a container as an int value (what that means is dependent on the platform). 
/// An exception is thrown if the size cannot be represented as an int.
///
/// @tparam  Cont   The container type
/// @param[in] cont  The container 
/// @return  The container size
///
template<class Cont>
int isize(const Cont& cont)
{
	const typename Cont::size_type size = cont.size();
	const typename Cont::size_type max_int = std::numeric_limits<int>::max();
	if (size > max_int) {
		TWIST_THROW(L"Container size %d cannot be represented as an int.", size);
	}
	return static_cast<int>(size);
} 

/// Apply the std::copy algorithm to the elements of a container and store the results in a std::vector.
///
/// @tparam  Cont   The container type 
/// @param[in] cont  The container
/// @return  The vector of results; it has the same type of elements as the input container
/// 
template <class Cont>
std::vector<typename Cont::value_type> copy_to_vector(const Cont& cont)
{
	std::vector<typename Cont::value_type> ret_vector;

	using std::begin;
	using std::end;
	std::copy(begin(cont), end(cont), std::back_inserter(ret_vector));

	return ret_vector;
}

/// Apply the std::copy_if algorithm to the elements of a container and store the results in a std::vector.
///
/// @tparam  Cont   The container type 
/// @tparam  TPred   The unary predicate type 
/// @param[in] cont  The container
/// @param[in] pred  The predicate
/// @return  The vector of results; it has the same type of elements as the input container
/// 
template <class Cont, class TPred>
std::vector<typename Cont::value_type> copy_if_to_vector(const Cont& cont, TPred pred)
{
	std::vector<typename Cont::value_type> ret_vector;

	using std::begin;
	using std::end;
	std::copy_if(begin(cont), end(cont), std::back_inserter(ret_vector), pred);

	return ret_vector;
}

/// Apply an operation sequentially to the elements of a range and store the results in a std::vector.
///
/// @tparam  Rng   The range type 
/// @tparam  Fn  The operation type 
/// @param[in] range  The range
/// @param[in] func  The operation
/// @return  The vector of results; the type of its elements is the return type of the operation
///  
template<class Rng, 
         class Fn,
		 class = EnableIfFunc<Fn, RangeElementCref<Rng>>> 
auto transform_to_vector(const Rng& range, Fn func) 
{
	using std::begin;
	using std::end;

	using FnRet = std::invoke_result_t<Fn, RangeElement<Rng>>;

	std::vector<FnRet> ret;
	ret.reserve(rg::size(range));
	std::transform(begin(range), end(range), std::back_inserter(ret), func);

	return ret;
}

/// Apply an operation sequentially to the elements of a range and store the results in a std::vector.
///
/// @tparam  Rng   The range type 
/// @tparam  Fn  The operation type 
/// @param[in] range  The range
/// @param[in] func  The operation
/// @return  The vector of results; the type of its elements is the return type of the operation
///  
template<class Rng, 
         class Fn,
		 class = EnableIfFunc<Fn, RangeElementRef<Rng>>> 
auto transform_to_vector(Rng& range, Fn func) 
{
	using std::begin;
	using std::end;

	using FnRet = decltype(func(*begin(range)));
	std::vector<FnRet> ret_vector;

	std::transform(begin(range), end(range), std::back_inserter(ret_vector), func);

	return ret_vector;
}

/// Apply an operation sequentially to the elements of a range and store the results in a std::set.
///
/// @tparam  Rng   The range type 
/// @tparam  Fn  The operation type 
/// @param[in] range  The range
/// @param[in] func  The operation
/// @return  The set of results; the type of its elements is the return type of the operation
/// 
///+TODO: what happens if insertion fails?  
template<class Rng,
         class Fn, 
		 class = EnableIfFunc<Fn, RangeElementRef<Rng>>> 
auto transform_to_set(const Rng& range, Fn func) 
{
	using std::begin;
	using std::end;

	using FnRet = decltype(func(*begin(range)));

	std::set<FnRet> ret_set;
	std::transform(begin(range), end(range), std::inserter(ret_set, end(ret_set)), func);

	return ret_set;
}

/// Apply an operation which returns a pair sequentially to the elements of a range, and store the results in 
/// a std::map. If a map insertion fals, an exception is thrown.
///
/// @tparam  Rng   The range type 
/// @tparam  Fn  The operation type 
/// @param[in] range  The range
/// @param[in] func  The operation
/// @return  The map of results; the type of its keys is the first type in the pair type returned by of the 
///				operation, and the type of its values is the second type in the pair
/// 
template<class Rng,
         class Fn, 
		 class = EnableIfFunc<Fn, RangeElementCref<Rng>>> 
auto transform_to_map(const Rng& range, Fn func) 
{
	using std::begin;

	using FnRet = std::invoke_result_t<Fn, decltype(*begin(range))>;
	using Key = typename FnRet::first_type;
	using Value = typename FnRet::second_type;

	std::map<Key, Value> map;
	for (const auto& elem : range) {
		auto&& fn_ret = std::invoke(func, elem);
		if (auto [_, ok] = map.emplace(std::move(fn_ret.first), std::move(fn_ret.second)); !ok) { 
			TWIST_THROW(L"Map insertion failed.");
		}
	}

	return map;
}

/// Given a container whose values are smart pointer instances (eg std::unique_ptr, std::shared_ptr) this function will 
/// extract the naked pointers from their wrappers and output them as constant pointers.
///
/// @tparam  Cont   The container type
/// @tparam  TOutIter  Output iterator type, addressing constant pointers
/// @param[in] cont  The container
/// @param[in] out_it  The output iterator
/// 
template<class Cont, typename TOutIter>
void get_cptrs(const Cont& cont, TOutIter out_it) 
{
	using std::begin;
	using std::end;
	std::transform(begin(cont), end(cont), out_it, [](auto& el) {
		return el.get();
	});
}

/// Given a container whose values are smart pointer instances (eg std::unique_ptr, std::shared_ptr) this 
/// function will extract the naked pointers from their wrappers and output them as pointers.
///
/// @tparam  Cont   The container type
/// @tparam  TOutIter  Output iterator type, addressing pointers
/// @param[in] cont  The container
/// @param[in] out_it  The output iterator
/// 
template<class Cont, typename TOutIter>
void get_ptrs(Cont& cont, TOutIter out_it) 
{
	using std::begin;
	using std::end;
	std::transform(begin(cont), end(cont), out_it, [](auto& el) {
		return el.get();
	});
}

/***  Other useful algorithms  ***/

/// Find the first and the last element within a range that match a specific predicate.
///
/// @tparam  TIter  Iterator type. Should be bidirectional.
/// @tparam  TPred   The predicate type - must be a unary predicate.
/// @param[in] first  Iterator addressing the first element in the range.
/// @param[in] last  Iterator addressing one past the final element in the range.
/// @param[in] pred  The predicate. 
/// @return  Pair of iterators. If any matching elements are found within the range, the first iterator will 
///					address the first match, while the second will address one past the last match. Otherwise, 
///					they will both address one past the end of the range.
/// 
template<typename TIter, typename TPred>  
std::pair<TIter, TIter> find_first_last_if(TIter first, TIter last, TPred pred)
{
	typedef std::reverse_iterator<TIter> Riter;
	std::pair<TIter, TIter> ret(last, last);
	
	if (first != last) {
		// Find first occurrence of a matching element.
		ret.first = std::find_if(first, last, pred);
		if (ret.first != last) {		
			// Find last occurrence of a matching element.
			Riter rfirst(last);
			Riter rlast(first);
			Riter rfirst_match = std::find_if(rfirst, rlast, pred);
			ret.second = rfirst_match.base();
		}
	}
	return ret;
}

/// Trim all consecutive elements with a specific value from the beginning of a range (e.g. a string).
///
/// @tparam  TIter  Iterator type for the range. Must be a forward iterator.
/// @tparam  TVal  Range element value type
/// @param[in] str  Iterator addressing the beginning of the range.
/// @param[in] str_end  Iterator addressing one past the end of the range.
/// @param[in] val  The value of the elements to be trimmed.
/// @return  Pair of iterators pointing to the beginning, and one beyond the end of, the trimmed range.
///
template<typename TIter, typename TVal>
std::pair<TIter, TIter> trim_left(TIter str, TIter str_end, const TVal& val)
{
	while (str != str_end && *str == val) {
		++str;
	}	
	return std::pair<TIter, TIter>(str, str_end);
}

/// Trim all consecutive elements with a specific value from the end of a range (e.g. a string).
///
/// @tparam  TIter  Iterator type for the range. Must be bidirectional.
/// @tparam  TVal  Range element value type
/// @param[in] str  Iterator addressing the beginning of the range.
/// @param[in] str_end  Iterator addressing one past the end of the range.
/// @param[in] val  The value of the elements to be trimmed.
/// @return  Pair of iterators pointing to the beginning, and one beyond the end of, the trimmed range.
///
template<typename TIter, typename TVal>
std::pair<TIter, TIter> trim_right(TIter str, TIter str_end, const TVal& val)
{
	while (str_end != str) {
		--str_end;
		if (*str_end != val) {
			++str_end;
			break;
		}
	}
	return std::pair<TIter, TIter>(str, str_end);
}

/// Trim all consecutive elements with a specific value from both the beginning and the end of a range (eg 
/// a string).
///
/// @tparam  TIter  Iterator type for the range. Must be bidirectional.
/// @tparam  TVal  Range element value type
/// @param[in] str  Iterator addressing the beginning of the range.
/// @param[in] str_end  Iterator addressing one past the end of the range.
/// @param[in] val  The value of the elements to be trimmed.
/// @return  Pair of iterators pointing to the beginning, and one beyond the end of, the trimmed range.
///
template<typename TIter, typename TVal>
std::pair<TIter, TIter> trim(TIter str, TIter str_end, const TVal& val)
{
	while (str != str_end && *str == val) {
		++str;
	}	
	if (str_end != str) {
		--str_end;
	}
	while (str_end != str && *str_end == val) {
		--str_end;
	}
	return std::pair<TIter, TIter>(str, str_end + 1);
}

/// @cond
namespace detail { 

/// Find the occurrence of any substring that is *not* the same as a specific substring, starting from a 
/// specific position.
///
/// @tparam  TIter1  Iterator type for the string. Must be bidirectional.
/// @tparam  TIter2  Iterator type for the delimiter string. Must be bidirectional.
/// @param[in] str  Iterator addressing the beginning of the string.
/// @param[in] str_end  Iterator addressing one past the end of the string.
/// @param[in] substr  Iterator addressing the beginning of the substring.
/// @param[in] substr_end  Iterator addressing one past the end of the substring.
/// @return  The position where the next occurrence of a substring other than the one passed in is found. 
///		If nothing is found,  str_end  is returned. 
///
template<typename TIter1, typename TIter2>
TIter1 get_next_non_substr_pos(TIter1 str, TIter1 str_end, TIter2 substr, TIter2 substr_end) 
{
    TIter1 ret = str_end;
    
	const size_t substr_len = substr_end - substr;

    TIter1 str_pos = str;
    TIter1 new_str_pos = str_pos;
    while (str_pos != str_end) {
        new_str_pos = std::search(str_pos, str_end, substr, substr_end);
        if (new_str_pos != str_pos) {
            ret = str_pos;
            break;
        }
        else {
            str_pos += substr_len;
        }
    }
    
    return ret;
}

} 
/// @endcond

/// Get the number of fields (substrings), within a string, as delimited by a certain delimiter string (eg a comma). 
/// Zero-length substrings (between adjacent delimiters) are not counted. +TODO: This is pretty odd. Dan 25Oct'23
/// Note: A special situation is when there is no occurrence of the delimiter within the string. This is a 
///   valid situation if and only if the string is not empty. In this case, there is exactly one field and 
///   that is the whole string.
///
/// @tparam  TIter1  Iterator type for the string. Must be bidirectional.
/// @tparam  TIter2  Iterator type for the delimiter string. Must be bidirectional.
/// @param[in] str  Iterator addressing the beginning of the string.
/// @param[in] str_end  Iterator addressing one past the end of the string.
/// @param[in] delimiter  Iterator addressing the beginning of the delimiter string.
/// @param[in] delimiter_end  Iterator addressing one past the end of the delimiter string.
/// @return  The field count. 
///
template<typename TIter1, typename TIter2>
Ssize count_delimited_fields(TIter1 str, TIter1 str_end, TIter2 delimiter, TIter2 delimiter_end)
{
    Ssize ret = 0;

	if (delimiter == delimiter_end) {
		TWIST_THROW(L"Delimiter range cannot be empty.");
    }
    
    TIter1 str_pos = str; 
    while (str_pos != str_end) {
        str_pos = detail::get_next_non_substr_pos(str_pos, str_end, delimiter, delimiter_end);
        if (str_pos != str_end) {
            ++ret;
            str_pos = std::search(str_pos + 1, str_end, delimiter, delimiter_end);
        }
    } 

    return ret;
}

/// Get a specific field (substring), within a string, as delimited by a certain delimiter string (eg a 
///   comma).
/// Note: A special situation is when there is no occurrence of the delimiter within the string. This is a 
///   valid situation if and only if the index of the field required is zero. In this case, the field is the 
///   whole string.
///
/// @tparam  TIter1  Iterator type for the string. Must be bidirectional.
/// @tparam  TIter2  Iterator type for the delimiter string. Must be bidirectional.
/// @param[in] str  Iterator addressing the beginning of the string.
/// @param[in] str_end  Iterator addressing one past the end of the string.
/// @param[in] delimiter  Iterator addressing the beginning of the delimiter string.
/// @param[in] delimiter_end  Iterator addressing one past the end of the delimiter string.
/// @param[in] field_idx  The (zero-based) index of the field, among all the delimited fields. Bear in mind 
///					that zero-length fields (between adjacent delimiters) are not counted. 
/// @return  Pair of iterators pointing to the beginning, and one beyond the end of, the field, if a field 
///					matching the criteria passed in is found. Otherwise, both iterators point to the end of 
///					the string.
///
template<typename TIter1, typename TIter2>
std::pair<TIter1, TIter1> get_delimited_field(TIter1 str, TIter1 str_end, TIter2 delimiter, 
		TIter2 delimiter_end, Ssize field_idx)
{
	if (delimiter == delimiter_end) {
		TWIST_THROW(L"Delimiter string cannot be blank.");
    }

    std::pair<TIter1, TIter1> field(str_end, str_end);
    
    TIter1 str_pos = str;
    TIter1 new_str_pos = str;
    Ssize field_loop = 0;
    while ((str_pos != str_end) && (new_str_pos != str_end)) {
        new_str_pos = detail::get_next_non_substr_pos(str_pos, str_end, delimiter, delimiter_end);
        if (new_str_pos != str_end) {
            str_pos = std::search(new_str_pos, str_end, delimiter, delimiter_end);
            if (field_loop == field_idx) {
                field.first = new_str_pos;
                field.second = str_pos;
                break;
            }
            field_loop++;            
        }
    } 
    
    return field;
}

} 

#endif 


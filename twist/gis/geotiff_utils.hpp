/// @file geotiff_utils.hpp
/// Utilities for working with geotiff files 

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_GIS_GEOTIFF__UTILS_HPP
#define TWIST_GIS_GEOTIFF__UTILS_HPP

#include "twist/gis/GeoDataGridInfo.hpp"
#include "twist/gis/gis_geometry.hpp"
#include "twist/gis/gis_globals.hpp"
#include "twist/math/FlatMatrix.hpp"
#include "twist/math/FlatMatrixStack.hpp"

#include <variant> 

namespace twist::img {
class Tiff;
}

namespace twist::gis {

/*! Keys for the projection values. 
    See http://geotiff.maptools.org/spec/geotiff6.html for value descriptions.
 */
enum class GtiffProjKey {  
	// geotiff Configuration Keys (section 6.2.1)
	gt_model_type                = 1024,  // Model Type Codes      
	gt_raster_type               = 1025,  // Raster Type Codes      
	gt_citation                  = 1026,  // documentation  

	// Geographic CS Parameter Keys (section 6.2.2)
	geographic_type              = 2048,  // geographic CS Type Codes    
	geog_citation                = 2049,  // documentation            
	geog_geodetic_datum          = 2050,  // Geodetic Datum Codes    
	geog_prime_meridian          = 2051,  // Prime Meridian Codes    
	geog_linear_units            = 2052,  // LinearUnits Codes    
	geog_linear_unit_size        = 2053,  // meters                   
	geog_angular_units           = 2054,  // Angular Units Codes 
	geog_angular_unit_size       = 2055,  // radians                  
	geog_ellipsoid               = 2056,  // Ellipsoid Codes    
	geog_semi_major_axis         = 2057,  // GeogLinearUnits          
	geog_semi_minor_axis         = 2058,  // GeogLinearUnits          
	geog_inv_flattening          = 2059,  // ratio                    
	geog_azimuth_units           = 2060,  // Angular Units Codes    
	geog_prime_meridian_long     = 2061,  // GeogAngularUnit   
	geog_to_wgs84                = 2062,  // 3/7 parameter datum shift to WGS84 (proposed addition 2011)

	// Projected CS Parameter Keys (section 6.2.3)
	projected_cs_type            = 3072,  // Projected CS Type Codes  
	pcs_citation                 = 3073,  // documentation          
	projection                   = 3074,  // Projection Codes  
	proj_coord_trans             = 3075,  // coordinate Transformation Codes  
	proj_linear_units            = 3076,  // Linear Units Codes  
	proj_linear_unit_size        = 3077,  // meters                 
	proj_std_parallel1           = 3078,  // GeogAngularUnit
	proj_std_parallel2           = 3079,  // GeogAngularUnit
	proj_nat_origin_long         = 3080,  // GeogAngularUnit
	proj_nat_origin_lat          = 3081,  // GeogAngularUnit
	proj_false_easting           = 3082,  // ProjLinearUnits
	proj_false_northing          = 3083,  // ProjLinearUnits
	proj_false_origin_long       = 3084,  // GeogAngularUnit
	proj_false_origin_lat        = 3085,  // GeogAngularUnit
	proj_false_origin_easting    = 3086,  // ProjLinearUnits
	proj_false_origin_northing   = 3087,  // ProjLinearUnits
	proj_center_long             = 3088,  // GeogAngularUnit
	proj_center_lat              = 3089,  // GeogAngularUnit
	proj_center_easting          = 3090,  // ProjLinearUnits
	proj_center_northing         = 3091,  // ProjLinearUnits
	proj_scale_at_nat_origin     = 3092,  // ratio  
	proj_scale_at_center         = 3093,  // ratio  
	proj_azimuth_angle           = 3094,  // GeogAzimuthUnit
	proj_straight_vert_pole_long = 3095,  // GeogAngularUnit
	proj_rectified_grid_angle    = 3096,  // GeogAngularUnit 

	// Vertical CS keys (section 6.2.4) 
	vertical_cs_type             = 4096,  // ascii 	
	vertical_citation            = 4097,  // uint16
	vertical_datum               = 4098,  // uint16
	vertical_units               = 4099   // uint16
};

//! Known 16-bit integer values for the map contained in a geotiff's projection information.
enum GtiffProjUint16Value : uint16_t {
	ct_transverse_mercator = 1,
	ct_lambert_conf_conic_2sp = 8,
	datum_geocentric_datum_of_australia_1994 = 6283,
	linear_meter = 9001,
	user_defined = 32767
};

//! These codes shall be used for any key that requires specification of an angular unit of measurement
enum GtiffAngularUnitCodes : uint16_t {
   angular_radian =	9101,
   angular_degree =	9102,
   angular_arc_minute =	9103,
   angular_arc_second =	9104,
   angular_grad = 9105,
   angular_gon = 9106,
   angular_dms = 9107,
   angular_dms_hemisphere =	9108
};

//! These codes are provided to specify the projected coordinate system
enum GtiffProjectedCsTypeValues : uint16_t {
	vicgrid = 3111,
	nsw_lambert = 3308,
	sa_lambert = 3107,
	mga_zone_55 = 28355
};

//! Stores the header data of a geotiff's projection information.
struct GtiffProjHeader {
	/*! The current version of the GeoKeyDirectoryTag implementation, and will only change if that tag's key 
        structure is changed; similar to the TIFFVersion (42)
     */
	const uint16_t key_directory_version;
	//! Indicates what revision of key-sets is used
	const uint16_t key_revision;
	/*! Indicates what set of key-codes is used; the complete revision number is denoted 
	    <key_revision> .\ <minor_revision>
     */
	const uint16_t minor_revision;	
};

/*! Class storing information about the projection used by a geotiff's geospatial coordinate system.
    It consists of a small header and a map whose values can have several types.
 */
class GeotiffProjection {
public:
	//! Map value variant type
	using Value = std::variant<uint16_t, double, std::vector<double>, std::string>;  

	//! Map type
	using KeyValueMap = std::map<GtiffProjKey, Value>;

	//! A range of key-value pairs from the map
	using KeyValueRange = Range<KeyValueMap::const_iterator>;

	/*! Constructor.
	    @param[in] header  The projection header data
     */
	GeotiffProjection(GtiffProjHeader header);

	//! The projection header data.
	GtiffProjHeader header() const;

	/*! Get a 16-bit unsigned integer value from the map.
	    \param[in] key  The map key; if the map does contain this key but the associated value is not a 16-bit unsigned 
		                integer, an exception is thrown
	    \return  The value, or nullopt if the map does not contain the key
     */
	std::optional<uint16_t> get_uint16_value(GtiffProjKey key) const;

	/*! Get a 64-bit floating-point number value from the map.	   
	    \param[in] key  The map key; if the map does contain this key but the associated value is not a 64-bit 
		                floating-point number, an exception is thrown
	    \return  The value, or nullopt if the map does not contain the key
     */
	std::optional<double> get_float64_value(GtiffProjKey key) const;

	/*! Get a 64-bit floating-point number array value from the map.	   
	    \param[in] key  The map key; if the map does contain this key but the associated value is not a 64-bit 
		                floating-point number array, an exception is thrown
	    \return  The value, or nullopt if the map does not contain the key
     */
	std::optional<std::vector<double>> get_float64_array_value(GtiffProjKey key) const;

	/*! Get an ASCII (string) value from the map.
	   
	    \param[in] key  The map key; if the map does contain this key but the associated value is not a string, an 
		                exception is thrown
	    \return  The value, or nullopt if the map does not contain the key
     */
	std::optional<std::string> get_ascii_value(GtiffProjKey key) const;

	/*! Add a key and the associated 16-bit unsigned integer value to the map.	   
	    \param[in] key  The map key; if the map already contain this key, an exception is thrown
	    \param[in] value  The value  
     */
	void add_uint16_value(GtiffProjKey key, uint16_t value);

	/*! Add a key and the associated 64-bit floating-point number value to the map.	   
	    \param[in] key  The map key; if the map already contain this key, an exception is thrown
	    \param[in] value  The value  
     */
	void add_float64_value(GtiffProjKey key, double value);

	/*! Add a key and the associated 64-bit floating-point number array value to the map.	   
	    \param[in] key  The map key; if the map already contain this key, an exception is thrown
	    \param[in] value  The value  
     */
	void add_float64_array_value(GtiffProjKey key, const std::vector<double>& value);

	/*! Add a key and the associated ASCII (string) value to the map.
	    \param[in] key  The map key; if the map already contain this key, an exception is thrown
	    \param[in] value  The value  
     */
	void add_ascii_value(GtiffProjKey key, const std::string& value);

	/*! Get a range containing all the values in the map, paired with their keys.
	    \return  The range of key-value pairs 
     */
	KeyValueRange values() const;

	TWIST_NO_COPY_DEF_MOVE(GeotiffProjection)

private:
	template<class Val> 
	std::optional<Val> get_value(GtiffProjKey key, const std::wstring& val_type) const;

	template<class Val> 
	void add_value(GtiffProjKey key, const Val& value);

	std::unique_ptr<GtiffProjHeader> header_;
	std::map<GtiffProjKey, Value> map_;

	TWIST_CHECK_INVARIANT_DECL
};

// --- Free functions ---

/*! Read information about the geographic data grid underlying a geotiff file (if it contains it). 
    This function will only work if the data grid cells are square.
    This information includes the coordinates of the spatial grid, but not the projection used. Use 
    read_projection_info() to read that information.   
    \param[in] tiff  Connection to the geotiff file
    \param[out] flip_y  Whether the TIFF data is flipped on the Y axis (first row in the pixel data is the bottom-most 
                        row, geographically)
    \return  The geographic data grid info; or nullopt if the file does not specify the geographic coordinates, or the 
	         coordinates are unsupported (e.g. the cells are not square)
 */   
[[nodiscard]] auto read_geo_data_grid_info(const twist::img::Tiff& tiff, bool& flip_y) 
                    -> std::optional<GeoDataGridInfo>;

/*! Read information about the geographic data grid underlying a geotiff file (if it contains it). 
    This function will only work if the data grid cells are square.
    This information includes the coordinates of the spatial grid, but not the projection used. Use 
    read_projection_info() to read that information.   
    \param[in] tiff  Connection to the geotiff file
    \return  The geographic data grid info; or nullopt if the file does not specify the geographic coordinates, or the 
	         coordinates are unsupported (e.g. the cells are not square)
 */   
[[nodiscard]] auto read_geo_data_grid_info(const twist::img::Tiff& tiff) -> std::optional<GeoDataGridInfo>;

/*! Read information about the geographic data grid underlying a geotiff file (if it contains it). 
    This function will only work if the data grid cells are square.
    This information includes the coordinates of the spatial grid, but not the projection used. Use 
    read_projection_info() to read that information.   
    \param[in] path  Path to the geotiff file
    \return  The geographic data grid info; or nullopt if the file does not specify the geographic coordinates, or the 
	         coordinates are unsupported (e.g. the cells are not square)
 */   
[[nodiscard]] auto read_geo_data_grid_info(fs::path path) -> std::optional<GeoDataGridInfo>;

/*! Write information about the geographic data grid underlying a geotiff file to the file. If the geotiff already 
    contains information about geographic data grid, this new information may be ignored. This information includes the 
    coordinates of the spatial grid, but not the projection used. Use write_projection_info() to write that information.
    \param[in] tiff  Connection to the geotiff file 
    \param[in] info  The geographic data grid info
 */
auto write_geo_data_grid_info(twist::img::Tiff& tiff, const GeoDataGridInfo& info) -> void;

/*! Given an existing geotiff file, write information about the geographic data grid underlying it to the file. If the 
    geotiff already contains information about geographic data grid, it is lost. This information includes the 
    coordinates of the spatial grid, but not the projection used. Use write_projection_info() to write that information.
    \param[in] path  The geotiff file path
    \param[in] info  The geographic data grid info; an exception is thrown if the grid dimensions or the 
   					pixel data type do not match the existing file details
 */
void replace_geo_data_grid_info(const fs::path& path, const GeoDataGridInfo& info);

/*! Read the geographic projection information (if any) stored in a geotiff file.
    \param[in] tiff  Connection to the TIFF file
    \return  The projection info, or nullopt if the TIFF contains no projection info
 */
[[nodiscard]] auto read_projection_info(const twist::img::Tiff& tiff) -> std::optional<GeotiffProjection>;

/*! Write geographic projection informatio to a geotiff file. 
    If the geotiff already contains projection information, this new information may be ignored.
    \param[in] tiff  Connection to the TIFF file
    \param[in] info  The projection info
 */
auto write_projection_info(twist::img::Tiff& tiff, const GeotiffProjection& info) -> void;

/*! Create and return an object storing information about the projection used by the known geospatial coordinate system 
    with ID \p refsys_id.
 */
[[nodiscard]] auto geotiff_projection_from_known_geo_refsys(KnownGeoRefsysId refsys_id) -> GeotiffProjection;

//! Search for a "known reference system" matching the projection \p proj. If no match is found, nullopt is returned.
[[nodiscard]] auto find_matching_known_geo_refsys(const GeotiffProjection& info) -> std::optional<KnownGeoRefsysId>;

/*! Find out whether the projection used by a geotiff's geospatial coordinate system matches one of the "known 
    geographic coordinate reference systems" defined in this library.   
    \param[in] proj  The geotiff projection
    \param[in] known_refsys_id  The "known reference system" ID
    \return  true if the projection matches the "known reference system" ID
 */   
[[nodiscard]] auto match(const GeotiffProjection& proj, KnownGeoRefsysId known_refsys_id) -> bool;

/*! Write the data stored in a matrix to a new geotiff, following the "scanline" format
    \tparam PixelVal  The type of a pixel value
    \param[in] input_data  The input data matrix
	\param[in] path  Geotiff path; if a file with this path already exists, an exception is thrown  
	\param[in] space_grid  Spatial grid underlying the geotiff data; must match the data matrix dimensions  
	\param[in] nodata_val  Geotiff "no-data" value; pass in nullopt for none  
	\param[in] proj_info  Geotiff projection info; pass in nullopt for none  
    \param[in] bigtiff  Whether the output geotiff should be marked as BigTIFF (bigger than 4GB)
 */
template<class PixelVal>
auto write_geotiff(const twist::math::FlatMatrix<PixelVal>& input_data, 
				   fs::path path,
				   const GeoSpaceGrid& space_grid,
				   std::optional<PixelVal> nodata_val = std::nullopt,
				   std::optional<GeotiffProjection> proj_info = std::nullopt,
				   bool bigtiff = false) -> void;

/*! Write the data stored in a matrix stack to a new multi-band geotiff, one band for each matrix.
    The first matrix in the stack is written to the first band (aka plane) in the geotiff, etc.
    \tparam PixelVal  The type of a pixel value
    \param[in] input_data  The input data matrix stack
	\param[in] path  Geotiff path; if a file with this path already exists, an exception is thrown  
	\param[in] space_grid  Spatial grid underlying the geotiff data; must match the data matrix dimensions  
	\param[in] nodata_val  Geotiff "no-data" value; pass in nullopt for none  
	\param[in] proj_info  Geotiff projection info; pass in nullopt for none  
    \param[in] bigtiff  Whether the output geotiff should be marked as BigTIFF (bigger than 4GB)
 */
template<class PixelVal>
auto write_multi_band_geotiff(const twist::math::FlatMatrixStack<PixelVal>& input_data, 
							  fs::path path,
							  const GeoSpaceGrid& space_grid,
							  std::optional<PixelVal> nodata_val = {},
							  std::optional<GeotiffProjection> proj_info = {},
							  bool bigtiff = false) -> void;

/*! Given the geotiff \p tiff, check a few of its properties. 
    The check will fail in any of the following cases:
	  - The TIFF is not really a geotiff;
	  - The TIFF does not contain projection info;
	  - The TIFF projection does not match the \p expected_known_refsys_id;
	  - \p expected_data_type is not nullopt, and the TIFF does not use the expected data type;
	  - \p expected_nodata_value is not nullopt, and the TIFF does not use the expected "no-data" value placeholder;
      - \p expected_tiled is not nullopt, and the TIFF is tiled when it is not expected to be, or is not tiled when it
	    it is expected to be. 
	\return 0. true if the check was successful
	        1. If the check was unsuccessful, an error message
			2. Information about the TIFF's underlying geographic grid, if the TIFF is a geotiff
 */
[[nodiscard]] auto check_geotiff(const twist::img::Tiff& tiff, 
                                 KnownGeoRefsysId expected_known_refsys_id, 
                                 std::optional<GeoGridDataType> expected_data_type = std::nullopt,
								 std::optional<std::string> expected_nodata_value = std::nullopt,
								 std::optional<bool> expected_tiled = std::nullopt)
                    -> std::tuple<bool, std::wstring, std::optional<GeoDataGridInfo>>;

/*! Given the geotiff \p tiff, check a few of its properties. If the check fails, an exception is thrown.
    The check will fail in any of the following cases:
	  - The TIFF is not really a geotiff;
	  - The TIFF does not contain projection info;
	  - The TIFF projection does not match the \p expected_known_refsys_id;
	  - \p expected_data_type is not nullopt, and the TIFF does not use the expected data type;
	  - \p expected_nodata_value is not nullopt, and the TIFF does not use the expected "no-data" value placeholder;
      - \p expected_tiled is not nullopt, and the TIFF is tiled when it is not expected to be, or is not tiled when it
	    it is expected to be.
	\return  Information about the TIFF's underlying geographic grid
 */
auto check_geotiff_and_throw(const twist::img::Tiff& tiff,  
                             KnownGeoRefsysId expected_known_refsys_id, 
							 std::optional<GeoGridDataType> expected_data_type = std::nullopt, 
							 std::optional<std::string> expected_nodata_value = std::nullopt,
							 std::optional<bool> expected_tiled = std::nullopt) -> GeoDataGridInfo;

/*! Read a subgrid from a geotiff file's pixel grid and save it, along with all the geotiff-specific information, to 
    another geotiff file. The subgrid is calculated to be the smallest subgrid which contains a given geographic 
	rectangle.
    \tparam PixelVal  The type of a pixel value
    \param[in] src_path  Source geotiff path
    \param[in] dest_path  Destination geotiff path
    \param[in] crop_rect  The geographic rectangle which determins the subgrid to save
 */
template<class PixelVal>
auto crop_geotiff(fs::path src_path, fs::path dest_path, const GeoRectangle& crop_rect) -> void;


/*! Create a geotiff containing the same value in each cell (the "fill" value).
    The geotiff data will follow the "scanline" format.
	\tparam PixelVal  The type of a pixel value
    \param[in] path  Path to the geotiff
    \param[in] space_grid  The space grid underlying the geotiff
    \param[in] fill_val  The fill value
    \param[in] fill_val_as_nodata  Whether the fill value should be set as "no-data" value placeholder in the geotiff;
                                   if false, the geotiff will not have a "no-data" value
    \param[in] refsys_id  Georeference system ID for the geotiff projection; nullptr for none
    \param[in] bigtiff  Whether the geotiff should be marked as BigTIFF (bigger than 4GB)
 */
template<class PixelVal>
auto create_blank_geotiff(fs::path path,
                          const GeoSpaceGrid& space_grid,
                          PixelVal fill_val,
                          bool fill_val_as_nodata = false,
                          std::optional<KnownGeoRefsysId> refsys_id = std::nullopt,
                          bool bigtiff = false) -> void;

}

#include "twist/gis/geotiff_utils.ipp"

#endif

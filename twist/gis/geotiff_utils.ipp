/// @file geotiff_utils.ipp
/// Inline implementation file for "geotiff_utils.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/img/Tiff.hpp"

namespace twist::gis {

// --- Local functions ---

namespace detail {

[[nodiscard]] inline auto pad_and_tile_geotiff_helper(const fs::path& in_path, Ssize nof_cells_per_tile) 
                           -> std::tuple<GeoSpaceGrid, double, Ssize>
{
    using namespace twist::img;
    using namespace twist::math;

	if (!is_multiple(nof_cells_per_tile, 256) || !is_perfect_square(nof_cells_per_tile)) {
		TWIST_THRO2(L"The number of cells per tile {} must be a multiple of 256 and a perfect square.",
			        nof_cells_per_tile);
	} 
    const auto nof_cells_per_tile_side = perfect_square_root(nof_cells_per_tile);

    auto in_data_grid_info = std::optional<GeoDataGridInfo>{};
    {
        auto in_tiff = Tiff{Tiff::open_read, in_path};
        if (in_tiff.width() < nof_cells_per_tile_side || in_tiff.height() < nof_cells_per_tile_side) {
            TWIST_THRO2(L"The image is too small to be tiled with tiles of {} cells per side.", nof_cells_per_tile_side);
        }
        in_data_grid_info = read_geo_data_grid_info(in_tiff);
        if (!in_data_grid_info) {
            TWIST_THRO2(L"TIFF file \"{}\" does not include geographic data.", in_tiff.path().filename().c_str());
        }
    }
    const auto& in_space_grid = in_data_grid_info->space_grid();
	const auto space_grid_cell_size = in_space_grid.cell_width();
	if (in_space_grid.cell_height()!= space_grid_cell_size) {
		TWIST_THRO2(L"This function does not (yet) support geotiffs whose underlying geographic grid "
                    L"has unsquare cells.");
	}

    return {in_space_grid, space_grid_cell_size, nof_cells_per_tile_side};
}

}

template<class PixelVal>
auto write_geotiff(const twist::math::FlatMatrix<PixelVal>& input_data, 
				   fs::path path,
				   const GeoSpaceGrid& space_grid,
				   std::optional<PixelVal> nodata_val,
				   std::optional<GeotiffProjection> proj_info,
				   bool bigtiff) -> void
{
	if (exists(path)) {
		TWIST_THRO2(L"TIFF file \"{}\" already exists.", path.filename().c_str());
	}
	if (input_data.nof_rows() != space_grid.nof_rows() || input_data.nof_columns() != space_grid.nof_columns()) {
		TWIST_THRO2(L"Input data matrix dimension ({}x{}) and spatial grid dimensions ({}x{}) do not match.",
		            input_data.nof_rows(), input_data.nof_columns(), space_grid.nof_rows(), space_grid.nof_columns());
	}

	const auto data_grid_info = GeoDataGridInfo{space_grid,
												nodata_val ? std::to_string(*nodata_val) : "",
												enum_for_geo_grid_data_type<PixelVal>};
	auto tiff = twist::img::Tiff{twist::img::Tiff::open_write, 
								 std::move(path), 
								 data_grid_info.data_value_type(), 
								 space_grid.nof_columns(), 
								 space_grid.nof_rows(),
								 1/*nof_planes*/,
								 bigtiff};
	tiff.write_scanlined(input_data);
	write_geo_data_grid_info(tiff, data_grid_info);
	if (proj_info) {
		write_projection_info(tiff, *proj_info);
	}	
}

template<class PixelVal>
auto write_multi_band_geotiff(const twist::math::FlatMatrixStack<PixelVal>& input_data, 
							  fs::path path,
							  const GeoSpaceGrid& space_grid,
							  std::optional<PixelVal> nodata_val,
							  std::optional<GeotiffProjection> proj_info,
							  bool bigtiff) -> void
{
	if (exists(path)) {
		TWIST_THRO2(L"TIFF file \"{}\" already exists.", path.filename().c_str());
	}
	if (input_data.nof_rows() != space_grid.nof_rows() || input_data.nof_columns() != space_grid.nof_columns()) {
		TWIST_THRO2(L"Input data matrix dimension ({}x{}) and spatial grid dimensions ({}x{}) do not match.",
		            input_data.nof_rows(), input_data.nof_columns(), space_grid.nof_rows(), space_grid.nof_columns());
	}

	const auto data_grid_info = GeoDataGridInfo{space_grid,
												nodata_val ? std::to_string(*nodata_val) : "",
												enum_for_geo_grid_data_type<PixelVal>};

	const auto nof_planes = static_cast<uint16_t>(input_data.nof_matrices());
	assert(nof_planes == input_data.nof_matrices());

	auto tiff = twist::img::Tiff{twist::img::Tiff::open_write, 
								 std::move(path), 
								 data_grid_info.data_value_type(), 
								 space_grid.nof_columns(), 
								 space_grid.nof_rows(),
								 nof_planes,
								 bigtiff};

	for (auto plane : IndexRange{nof_planes}) {
		tiff.write_scanlined(input_data.view_matrix(plane).data(), std::nullopt/*subgrid_info*/, plane);
	}

	write_geo_data_grid_info(tiff, data_grid_info);
	if (proj_info) {
		write_projection_info(tiff, *proj_info);
	}
}

template<class PixelVal>
auto crop_geotiff(fs::path src_path, fs::path dest_path, const GeoRectangle& crop_rect) -> void
{
	// Read the geogrphic grid info and subgrid pixel data from the source TIFF 
	auto src_tiff = twist::img::Tiff{twist::img::Tiff::open_read, std::move(src_path)};
	auto src_grid_info = read_geo_data_grid_info(src_tiff);
	if (!src_grid_info) {
		TWIST_THRO2(L"TIFF file \"{}\" is not a GeoTIFF.", src_tiff.path().filename().c_str());
	}
	auto subgrid_info = get_info_of_subgrid_containing_rect(src_grid_info->space_grid(), crop_rect);
	if (!subgrid_info) {
		TWIST_THRO2(L"The crop rectangle is not covered by the geographic grid underlying GeoTIFF file \"{}\" ."
		            L"(or it is smaller than a grid cell).",
		            src_tiff.path().filename().c_str());
	}
	auto data_grid = src_tiff.read_scanlined<PixelVal>(subgrid_info);
	auto proj_info = read_projection_info(src_tiff);

	// Write the pixel data and geographic grid info to the destination TIFF
	auto dest_tiff = twist::img::Tiff{twist::img::Tiff::open_write, 
									  std::move(dest_path), 
									  src_tiff.sample_data_type(), 
									  nof_columns(*subgrid_info), 
									  nof_rows(*subgrid_info)};
	dest_tiff.write_scanlined<PixelVal>(data_grid);
	write_geo_data_grid_info(dest_tiff, GeoDataGridInfo{get_subgrid(src_grid_info->space_grid(), *subgrid_info),
														src_grid_info->nodata_value(),
														src_grid_info->data_value_type()});
	// If the source TIFF contains projection info, copy it to the destination TIFF
	if (proj_info) {
		write_projection_info(dest_tiff, *proj_info);
	}
}

template<class PixelVal>
auto create_blank_geotiff(fs::path path,
                          const GeoSpaceGrid& space_grid,
                          PixelVal fill_val,
                          bool fill_val_as_nodata, 
                          std::optional<KnownGeoRefsysId> refsys_id,
                          bool bigtiff) -> void
{
    const auto data = twist::math::FlatMatrix<PixelVal>{space_grid.nof_rows(), space_grid.nof_columns(), fill_val};
    write_geotiff<PixelVal>(data, 
				            std::move(path),
				            space_grid,
				            fill_val_as_nodata ? fill_val 
                                               : std::optional<PixelVal>{},
				            refsys_id ? geotiff_projection_from_known_geo_refsys(*refsys_id) 
                                      : std::optional<GeotiffProjection>{},
				            bigtiff);
}

}

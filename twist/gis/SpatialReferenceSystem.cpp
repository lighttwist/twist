/// @file SpatialReferenceSystem.cpp
/// Implementation file for "SpatialReferenceSystem.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/gis/SpatialReferenceSystem.hpp"

#include "gdal/include/ogr_spatialref.h"

// https://gis.stackexchange.com/questions/664/whats-the-difference-between-a-projection-and-a-datum

namespace twist::gis {

// --- SpatialReferenceSystem::Impl struct ---

class SpatialReferenceSystem::Impl {
public:
	Impl() 
	{ 
		// Ensure longitude, latitude order https://gdal.org/tutorials/osr_api_tut.html
		refsys_.SetAxisMappingStrategy(OAMS_TRADITIONAL_GIS_ORDER); 
	}

	[[nodiscard]] auto get() -> OGRSpatialReference& { return refsys_; }

private:
	OGRSpatialReference refsys_;
};

// --- SpatialReferenceSystem class ---

SpatialReferenceSystem::SpatialReferenceSystem()
	: impl_{std::make_shared<SpatialReferenceSystem::Impl>()}
{
	TWIST_CHECK_INVARIANT
}

auto SpatialReferenceSystem::get_attr_value(std::wstring_view node_name, int attr_idx) const -> std::wstring
{
	TWIST_CHECK_INVARIANT
	return to_string(impl_->get().GetAttrValue(string_to_ansi(node_name).c_str(), attr_idx));
}

auto SpatialReferenceSystem::get_float_attr_value(std::wstring_view node_name, int attr_idx) const -> double
{
	TWIST_CHECK_INVARIANT
	return to_double_checked(get_attr_value(node_name, attr_idx));
}

auto SpatialReferenceSystem::get_proj_parm(std::wstring_view name) const -> std::optional<double>
{
	auto err = OGRErr{OGRERR_NONE};
	double value = impl_->get().GetProjParm(string_to_ansi(name).c_str(), 0, &err); 
	if (err == OGRERR_NONE) {
		return value;
	}
	return {};
}

auto SpatialReferenceSystem::export_to_pretty_wtk() const -> std::wstring
{
	auto internal_buffer = (char*)nullptr;
	if (impl_->get().exportToPrettyWkt(&internal_buffer) == OGRERR_NONE) {
		return ansi_to_string(internal_buffer);
	}
	return {};
}

auto SpatialReferenceSystem::get_angular_units() const -> std::wstring
{
	auto internal_buffer = (char*)nullptr;
	if (impl_->get().GetAngularUnits(&internal_buffer) == OGRERR_NONE) {
		return ansi_to_string(internal_buffer);
	}
	return {};
}

auto SpatialReferenceSystem::get() const -> OGRSpatialReference&
{
	TWIST_CHECK_INVARIANT
	return impl_->get();
}

#ifdef _DEBUG
void SpatialReferenceSystem::check_invariant() const noexcept
{
	assert(impl_);
}
#endif

}

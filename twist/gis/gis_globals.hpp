/// @file gis_globals.hpp
/// Globals for the "twist::gis" namespace 

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_GIS__GLOBALS_HPP
#define TWIST_GIS__GLOBALS_HPP

#include "twist/MetaBiMapConstvalType.hpp"
#include "twist/type_info_utils.hpp"
#include "twist/math/ClosedInterval.hpp"
#include "twist/math/geometry.hpp"
#include "twist/math/space_grid.hpp"

namespace twist::gis {

//! Known geographic coordinate reference systems.
enum class KnownGeoRefsysId : std::uint32_t {
	sa_lambert94 = 3107, ///< EPSG:3107, GDA94 / SA Lambert (South Australia, Australia; projected)
	vicgrid94 = 3111, ///< EPSG:3111, GDA94 / Vicgrid94 (Victoria, Australia; projected)
	nsw_lambert94 = 3308, ///< EPSG:3308, GDA94 / NSW Lambert (New South Wales, Australia; projected)
	mga_zone_55_94 = 28355, ///< EPSG:28355, GDA94 / MGA zone 55 (Australia - onshore and offshore between 144E and 
	                        ///<                                  150E; projected)
    wgs84 = 4326 ///< EPSG:4326 / WGS84 - World Geodetic System 1984 (geographic)
};

//! Geo-grid cell data value types
enum class GeoGridDataType {
	int8                         =    1, ///< 8bit signed integer
	uint8                        =    2, ///< 8bit unsigned integer
	int16                        =    3, ///< 16bit integer
	uint16                       =    4, ///< 16bit unsigned integer
	int32                        =    5, ///< 32bit integer
	uint32                       =    6, ///< Unsigned 32bit integer
	float32                      =    7, ///< Single Precision floating point
	float64                      =    8, ///< Double Precision floating point
	comp_uint8_uint16            =  100, ///< Composite value (multiple-valued cell): 
										 ///<	8-bit unsigned integer, 16-bit unsigned integer
	comp_uint8_uint8_uint8       = 1000, ///< Composite value (multiple-valued cell): 8-bit unsigned integer, 
										 ///< 8-bit unsigned integer, 8-bit unsigned integer
	comp_float32_float32_float32 = 1001  ///< Composite value (multiple-valued cell): 
										 ///< Single Precision floating point, Single Precision floating point, 
										 ///< Single Precision floating point
};

//! Types of geometry for features appearing in geographic vector files.
enum class VectorGeometryType {
	point = 1, ///< 2D point 
    line_string = 2, ///< Line string (a multi-vertex line)
	polygon = 3 ///< Polygon
};

namespace detail {

using GeoGridDataTypeMetaMap = MetaBiMapConstvalType<
		MetaMapConstvalList<
				GeoGridDataType, GeoGridDataType::int8, GeoGridDataType::uint8, GeoGridDataType::int16, 
				GeoGridDataType::uint16, GeoGridDataType::int32, GeoGridDataType::uint32, 
				GeoGridDataType::float32, GeoGridDataType::float64>, 
		MetaMapTypeList<
				int8_t, uint8_t, int16_t, uint16_t, int32_t, uint32_t, float, double>>;  
}

/// Metafunction which resolves to the language type corresponding to a compile-time constant geo-grid  
/// data type enum value.
///
/// @tparam  val  The constant value; a compile error occurs if it is not one of the GeoGridDataType 
///				which have a corresponding language type
///
template<GeoGridDataType val> 	
using TypeOfGeoGridData = MetaMapFindTypeForConstval<detail::GeoGridDataTypeMetaMap, val>;

/// Get (at compile time) the geo-grid data type enum value corresponding to a language type.
///
/// @tparam  T  The C++ type; a compile error ocurrs if it is not a valid geo-grid data value type
/// @return  The data type enum value
///
template<class Type>
constexpr GeoGridDataType enum_for_geo_grid_data_type = 
		meta_map_find_constval<detail::GeoGridDataTypeMetaMap, Type>();

/// Get the name (textual representation) of a specific GeoGridDataType enum value.
///
/// @param[in] value_type  The enum value; an exception is thrown if it does not match one of the defined 
///					enum constants 
/// @return  The value name
///				
std::wstring to_string(GeoGridDataType value_type);

/// Get the size, in bits, of a geo data grid value of a specific type.
///
/// @param[in] value_type  The data value type
/// @return  The value size (bits)
///
unsigned char bits_per_data_value(GeoGridDataType value_type);

/// Check whether a language type matches a geo-grid cell data value type enum value.
/// If they do not match, an exception is thrown.
///
/// @tparam  T  The C++ type
/// @param[in] value_type  The data value type enum value
///
template<typename T> 
void check_data_value_type(GeoGridDataType value_type);  

/*! Get a "long" name for a "known" geographic coordinate reference system.
    \param[in] refsys_id  The known reference system ID
    \param[in] incl_epsg  Whether to prepend the EPSG number to the name
    \return  The reference system long name
 */
[[nodiscard]] constexpr auto as_long_name(KnownGeoRefsysId refsys_id, bool incl_epsg = true) -> std::wstring;

/*! Get the KnownGeoRefsysId enum value from the "long" coordinate referece system name \p long_name.
    If \p long_name does is not a recognised "long" representation, an exception is thrown.
 */
[[nodiscard]] constexpr auto known_geo_refsys_id_from_long_name(std::wstring_view long_name) -> KnownGeoRefsysId;

//! Whether a \p refsys_d is a valid KnownGeoRefsysId value.
[[nodiscard]] constexpr auto is_valid(KnownGeoRefsysId refsys_id) -> bool;

//! Get all values defined for the KnownGeoRefsysId enum.
[[nodiscard]] constexpr auto all_known_geo_refsys_ids() -> std::vector<KnownGeoRefsysId>;

/*! Gived the "known reference system" ID \p known_refsys_id, get a string, which follows the "well-known text 
    representation of coordinate reference systems" format, corresponding to that reference system.
 */
[[nodiscard]] auto to_wkt_string(KnownGeoRefsysId known_refsys_id) -> const char*;

/*! Gived the "known reference system" ID \p known_refsys_id, get a string, which follows the "PROJ.4" format, 
    corresponding to that reference system.
 */
[[nodiscard]] auto to_proj4_string(KnownGeoRefsysId known_refsys_id) -> const char*;

}

#include "twist/gis/gis_globals.ipp"

#endif

/// @file GeoCoordProjector.cpp
/// Implementation file for "GeoCoordProjector.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/gis/GeoCoordProjector.hpp"

namespace twist::gis {

auto get_bounding_lonlat_quad(const GeoCoordProjector& projector, const GeoRectangleMetre& metre_rect) 
      -> GeoLonLatQuadrangle
{
	const auto bottom_left_deg = projector.metre_to_degree(to_geo_point_metre(metre_rect.bottom_left()));
	const auto bottom_right_deg = projector.metre_to_degree(to_geo_point_metre(metre_rect.bottom_right()));
	const auto top_left_deg = projector.metre_to_degree(to_geo_point_metre(metre_rect.top_left()));
	const auto top_right_deg = projector.metre_to_degree(to_geo_point_metre(metre_rect.top_right()));

	const auto left_deg = std::min(bottom_left_deg.x(), top_left_deg.x());
	const auto right_deg = std::max(bottom_right_deg.x(), top_right_deg.x());
	const auto bottom_deg = std::min(bottom_left_deg.y(), bottom_right_deg.y());
	const auto top_deg = std::max(top_left_deg.y(), top_right_deg.y());

	return {left_deg, bottom_deg, right_deg, top_deg};
}

}

/// @file GeoCoordProjectorGdal.cpp
/// Implementation file for "GeoCoordProjectorGdal.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/gis/GeoCoordProjectorGdal.hpp"

#include "twist/gis/twist_gdal_utils.hpp"

namespace twist::gis {

GeoCoordProjectorGdal::GeoCoordProjectorGdal(const SpatialReferenceSystem& degree_refsys, 
		                                     const SpatialReferenceSystem& metre_refsys)
	: GeoCoordProjector{}
	, degree_to_metre_trans_{degree_refsys, metre_refsys}
	, metre_to_degree_trans_{metre_refsys, degree_refsys}
{
	TWIST_CHECK_INVARIANT
}

GeoCoordProjectorGdal::~GeoCoordProjectorGdal()
{
}

auto GeoCoordProjectorGdal::degree_to_metre(GeoPointDecDegree pt) const -> GeoPointMetre
{
	TWIST_CHECK_INVARIANT
	const auto transformed_pt = degree_to_metre_trans_.transform(pt);
	return GeoPointMetre{transformed_pt.x(), transformed_pt.y()};
}

auto GeoCoordProjectorGdal::metre_to_degree(GeoPointMetre pt) const -> GeoPointDecDegree
{
	TWIST_CHECK_INVARIANT
	const auto transformed_pt = metre_to_degree_trans_.transform(pt);
	return GeoPointDecDegree{transformed_pt.x(), transformed_pt.y()};
}

#ifdef _DEBUG
void GeoCoordProjectorGdal::check_invariant() const noexcept
{
}
#endif

}

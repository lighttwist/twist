/// @file GeoCoordProjector.hpp
/// GeoCoordProjector abstract class 

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_GIS_COORD_PROJECTOR_HPP
#define TWIST_GIS_COORD_PROJECTOR_HPP

#include "twist/gis/gis_geometry.hpp"

namespace twist::gis {

/*! Abstract base for classes which performs coordinate transformations between a given, spheroidal (geographic),
	decimal degree-based coordinate system (longitude and latitude) - typically "WGS84" - and a flat (projected),
	metre-based coordinate system (X and Y coordinates).
	See also https://gis.stackexchange.com/questions/664/whats-the-difference-between-a-projection-and-a-datum
*/
class GeoCoordProjector {
public:
	virtual ~GeoCoordProjector() = default;

	/*! Project a geographic point from decimal degree coordinates to metre coordinates.	
	    \param[in] pt  The point (in decimal degree coordinates)
	    \return  The projected point (in metre coordinates)
	 */
	[[nodiscard]] virtual auto degree_to_metre(GeoPointDecDegree pt) const -> GeoPointMetre = 0;

	 /*! Project a geographic point from metre coordinates to decimal degree coordinates.	
	     \param[in] pt  The point (in metre coordinates)
	     \return  The projected point (in decimal degree coordinates)
	  */
	[[nodiscard]] virtual auto metre_to_degree(GeoPointMetre pt) const -> GeoPointDecDegree  = 0;
	
protected:
	GeoCoordProjector() = default;
};

/*! Given a rectangle in a projected (metre-based) coordinate system, and a coordinate projector, get the quadrangle in 
    geographic (longitude/latitude-based) coordinates which bounds the rectangle.
	\param[in] projector  The coordinate projector
	\param[in] metre_rect  The rectangle in projected coordinates
	\return  The bounding quadrangle in geographic coordinates
 */
[[nodiscard]] auto get_bounding_lonlat_quad(const GeoCoordProjector& projector, const GeoRectangleMetre& metre_rect) 
                    -> GeoLonLatQuadrangle;

}

#endif

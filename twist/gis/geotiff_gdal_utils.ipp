/// @file geotiff_gdal_utils.ipp
/// Inline implementation file for "geotiff_gdal_utils.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/scope.hpp"

namespace twist::gis {

namespace detail {

auto write_geotiff_gdal_impl(std::function<void*(Ssize)> get_input_data_row, 
					         fs::path path,
					         const GeoSpaceGrid& space_grid,
					         //std::optional<PixelVal> nodata_val, 
					         std::optional<SpatialReferenceSystem> proj_info,
					         bool bigtiff) -> void;
}

template<class PixelVal>
auto pad_and_tile_geotiff(const fs::path& in_path, 
                          const fs::path& out_path, 
                          Ssize nof_cells_per_tile,
                          PixelVal /*out_nodata_val*/, //+TODO: Implement. DAA 24Oct'24
                          bool bigtiff) -> GeoSpaceGrid
{
    const auto [in_space_grid, 
                space_grid_cell_size, 
                nof_cells_per_tile_side] = detail::pad_and_tile_geotiff_helper(in_path, nof_cells_per_tile);

    const auto nof_rows_beyond_full_tiles = in_space_grid.nof_rows() % nof_cells_per_tile_side;
    const auto nof_cols_beyond_full_tiles = in_space_grid.nof_columns() % nof_cells_per_tile_side;
    const auto nof_rows_to_add = (nof_rows_beyond_full_tiles != 0) 
                                            ? nof_cells_per_tile_side - nof_rows_beyond_full_tiles 
                                            : 0;
    const auto nof_cols_to_add = (nof_cols_beyond_full_tiles != 0) 
                                             ? nof_cells_per_tile_side - nof_cols_beyond_full_tiles 
                                             : 0;
    auto out_space_grid = GeoSpaceGrid{in_space_grid.left(), 
                                       in_space_grid.bottom(),
                                       space_grid_cell_size,
                                       space_grid_cell_size,
                                       in_space_grid.nof_rows() + nof_rows_to_add,
                                       in_space_grid.nof_columns() + nof_cols_to_add};                                     
    translate_raster(in_path, 
                     out_path, 
                     RasterFileFormat::geotiff, 
                     out_space_grid, 
                     std::nullopt/*out_data_type*/, 
                     RasterResampleAlgorithm::mode, 
				     std::nullopt/*value_rescale*/,
                     nof_cells_per_tile,
                     std::nullopt/*geotiff_compression*/, 
                     bigtiff);  
                                                
    return out_space_grid; 
}


template<class PixelVal>
auto rowpad_and_tile_geotiff(const fs::path& in_path,
                             const fs::path& out_path,
                             Ssize nof_cells_per_tile,
                             PixelVal /*out_nodata_val*/, //+TODO: Implement. DAA 24Oct'24
                             bool bigtiff) -> GeoSpaceGrid
{
    const auto [in_space_grid, 
                space_grid_cell_size, 
                nof_cells_per_tile_side] = detail::pad_and_tile_geotiff_helper(in_path, nof_cells_per_tile);

    const auto nof_rows_beyond_full_tiles = in_space_grid.nof_rows() % nof_cells_per_tile_side;
    const auto nof_rows_to_add = (nof_rows_beyond_full_tiles != 0) 
                                            ? nof_cells_per_tile_side - nof_rows_beyond_full_tiles 
                                            : 0;
    auto out_space_grid = GeoSpaceGrid{in_space_grid.left(), 
                                       in_space_grid.bottom(),
                                       space_grid_cell_size,
                                       space_grid_cell_size,
                                       in_space_grid.nof_rows() + nof_rows_to_add,
                                       in_space_grid.nof_columns()};                                     
    translate_raster(in_path, 
                     out_path, 
                     RasterFileFormat::geotiff, 
                     out_space_grid, 
                     std::nullopt/*out_data_type*/, 
                     RasterResampleAlgorithm::mode, 
				     std::nullopt/*value_rescale*/,
                     nof_cells_per_tile,
                     std::nullopt/*geotiff_compression*/,
                     bigtiff);  
                                                
    return out_space_grid; 
}

template<class PixelVal>
auto write_geotiff_gdal(const twist::math::FlatMatrix<PixelVal>& input_data, 
					    fs::path path,
					    const GeoSpaceGrid& space_grid,
					    std::optional<PixelVal> nodata_val,
					    std::optional<SpatialReferenceSystem> proj_info,
					    bool bigtiff) -> void
{
    write_geotiff_gdal_impl([](Ssize row) { return (void*)input_data.view_row_cells(row).data(); },
                            std::move(path),
                            space_grid,
                            //nodata_val,
                            proj_info,
                            bigtiff);
}

}

/// @file SpatialReferenceSystem.hpp
/// SpatialReferenceSystem class definition

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_GIS_SPATIAL_REFERENCE_SYSTEM_HPP
#define TWIST_GIS_SPATIAL_REFERENCE_SYSTEM_HPP

class OGRSpatialReference;

namespace twist::gis {

/*! This class represents an OpenGIS Spatial Reference System, and wraps a GDAL OGRSpatialReference object, 
    retrievable from this class as a reference to an incomplete type.
	If the system is spheroidal, this class enforces the order longitude, latitude, for coordinate transformations 
	which involve it e.g. OGRCoordinateTransformation::Transform(). 
 */
class SpatialReferenceSystem {
public:
	explicit SpatialReferenceSystem();

	/*! Fetch the value of a specific attribute of a named node.
	    This method finds the named node, and then extracts the value of the indicated child. Thus a call to 
		get_attr_value("UNIT", 1) would return the second child of the UNIT node, which is normally the length of the 
		linear unit in meters.
	    \param[in] node_name  The tree node to look for (case insensitive)
	    \param[in] attr_idx  The index of the child of the node to fetch (zero based)
	    \return  The requested value, or an empty string if it fails for any reason
	 */
	[[nodiscard]] auto get_attr_value(std::wstring_view node_name, int attr_idx = 0) const->std::wstring;
	
	/*! Fetch the value of a specific attribute of a named node, where the attribute value type is floating-point. 
	    If the value is not convertible to a double value, an exception is thrown. This method finds the named node, 
		and then extracts the value of the indicated child. Thus a call to get_attr_value("UNIT", 1) would return the 
		second child of the UNIT node, which is normally the length of the linear unit in meters.
	    \param[in] node_name  The tree node to look for (case insensitive)
	    \param[in] attr_idx  The index of the child of the node to fetch (zero based)
	    \return  The requested value
	 */
	[[nodiscard]] auto get_float_attr_value(std::wstring_view node_name, int attr_idx = 0) const -> double;

	/*! Fetch a projection parameter value.
	    \param[in] node_name  The name of the parameter to fetch, from the set of SRS_PP codes in "ogr_srs_api.hpp".
	    \return  The value of the parameter; or nullopt if the parameter value could not be retrieved
	 */
	[[nodiscard]] auto get_proj_parm(std::wstring_view name) const -> std::optional<double>;

	//! Convert this SRS into a nicely formatted WKT (Well Known Text) string.
	[[nodiscard]] auto export_to_pretty_wtk() const -> std::wstring;

	/*! Fetch angular geographic coordinate system units.
	    If no units are available, a value of "degree" and SRS_UA_DEGREE_CONV will be assumed. This method only checks 
	    directly under the GEOGCS node for units.
	 */
	[[nodiscard]] auto get_angular_units() const -> std::wstring;

	//! Get a reference to the underlying GDAL object.
	[[nodiscard]] auto get() const -> OGRSpatialReference&;

private:
	class Impl;
	std::shared_ptr<Impl> impl_;

	TWIST_CHECK_INVARIANT_DECL
};

}

#endif

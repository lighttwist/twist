/// @file Shapefile.hpp
/// Shapefile class

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_GIS_SHAPEFILE_HPP
#define TWIST_GIS_SHAPEFILE_HPP

#include "twist/gis/gis_globals.hpp"
#include "twist/gis/SpatialReferenceSystem.hpp"
#include "twist/gis/twist_gdal_utils.hpp"

#include <variant>

namespace twist::gis {

/*! What to do when encountering a multi-polygon feature while reading a vector file layer with the "polygon" geometry 
    type.
 */
enum class MultiPolygonOption {
	split = 1, ///< Split the multi-poligon into single polygons; any data fields read with the feature are copied for 
               ///< each single polygon
	skip = 2, ///< Do not read the multi-polygon feature at all
	error = 3, ///< Throw an error
};

/*! Class which represents an open connection to an ESRI ShapeFile, a geospatial vector data format. 
    An ESRI shapefile consists of (at least) three files, stored in the same directory: the main shape file (.shp), 
	the shapefile index file (.shx) and a DBF file (.dbf).
 */
class Shapefile {
public:
	enum class OpenMode {};
	enum class CreateMode {};

	static const OpenMode open{};
	static const CreateMode create{};

	using AttributeValue = std::variant<int, std::string>;

	/*! Constructor. This constructor creates a new shapefile and opens a connection to it.
		\param[in] tag  Tag value used for tag dispatching
        \param[in] shapefile_dir_path  Path to the new shapefile directory
		\param[in] layer_geom_type  The geometry type of the default layer
		\param[in] layer_refsys  The spatial reference system to use for the default layer; nullopt for none
	 */
	explicit Shapefile(CreateMode /*tag*/, 
	                   const fs::path& shapefile_dir_path, 
					   VectorGeometryType layer_geom_type,
					   std::optional<SpatialReferenceSystem> layer_refsys = std::nullopt);  

	/*! Constructor. This constructor opens a connection to an existing shapefile and selects the first layer as the 
	    active layer.
		\param[in] tag  Tag value used for tag dispatching
		\param[in] path  Path to the main (.shp) file in the shapefile
		\param[in] read_only  Whether the shapefile shuould be open in read-only mode
	 */
	explicit Shapefile(OpenMode /*tag*/, fs::path path, bool read_only);  

	~Shapefile();

	//! Whether the shapefile's active layer geometry type is "point".
	[[nodiscard]] auto has_point_geometry() const -> bool;        

	//! Whether the shapefile's active layer geometry type is "line string".
	[[nodiscard]] auto has_line_string_geometry() const -> bool;        

	//! Whether the shapefile's active layer geometry type is "polygon".
	[[nodiscard]] auto has_polygon_geometry() const -> bool; 

	//! Add the "point" feature \p point to the active layer and get its feature ID. 
	auto add_feature(GeoPoint point) -> Ssize;

	//! Add the "string line" feature \p line_string to the active layer and get its feature ID. 
	auto add_feature(const GeoLineString& line_string) -> Ssize;

	//! Add the "polygon" feature \p polygon to the active layer and get its feature ID. 
	auto add_feature(const GeoPolygon& polygon) -> Ssize;

	/*! Add a new column/field to the attribute table. The values in the new column for any existing features will 
	    be NULL.
	    \param[in] field_info  Information about the new column/field
	 */
	auto add_attribute_column(FeatureFieldInfo field_info) -> void;

	/*! Get the number of features in the active layer.
	    \param[in] force  Whether the count should be computed even if it is expensive
	    \return  Feature count, or nullopt if the count is not known 
     */
	[[nodiscard]] auto nof_features(bool force = true) const -> std::optional<Ssize>;

	/*! Read the feature with ID \p feature_id, from the active layer, as a polygon. 
	    The layer is expected to be a polygon layer. 
		If \p feature_id is not found, an exception is thrown.
	 */
	[[nodiscard]] auto read_feature_as_polygon(Ssize feature_id) const -> GeoPolygon;

	/*! Read all the features from the active layer as polygons. 
	    The layer is expected to be a polygon layer.
        \param[in] multipolygon_option  What to do when encountering a multi-polygon feature
	 */
	[[nodiscard]] auto read_all_features_as_polygons(MultiPolygonOption multipolygon_option) const 
                        -> std::vector<GeoPolygon>;

	/*! Read all the features from the active layer as polygons, toghether with the values of one attribute field.
	    The layer is expected to be a polygon layer. 
		\tparam ColVal  The type of values in the attribute column
		\param[in] col_name  The name of the attribute column
        \param[in] multipolygon_option  What to do when encountering a multi-polygon feature
		\return  The polygons, together with the corresponding field values
		\note +TODO: Modify to support an arbitrary number of attribute columns
	 */
	template<class ColVal>
	[[nodiscard]] auto read_all_features_as_polygons_with_attribute(const std::string& col_name,
                                                                    MultiPolygonOption multipolygon_option) const 
					    -> std::vector<std::tuple<GeoPolygon, ColVal>>;

	/*! Read all the features from the active layer as polygons, toghether with 3 attribute field values.
	    The layer is expected to be a polygon layer. 
		\tparam Col1Val  The type of values in the first attribute column
		\tparam Col2Val  The type of values in the second attribute column
		\tparam Col3Val  The type of values in the third attribute column
		\param[in] col1_name  The name of the first attribute column
		\param[in] col2_name  The name of the second attribute column
		\param[in] col3_name  The name of the third attribute column
        \param[in] multipolygon_option  What to do when encountering a multi-polygon feature
		\return  The polygons, together with the corresponding field values
		\note +TODO: Modify to support an arbitrary number of attribute columns
	 */
	template<class Col1Val, class Col2Val, class Col3Val>
	[[nodiscard]] auto read_all_features_as_polygons_with_attributes(const std::string& col1_name,
																	 const std::string& col2_name,
																	 const std::string& col3_name,
                                                                     MultiPolygonOption multipolygon_option) const 
					    -> std::vector<std::tuple<GeoPolygon, Col1Val, Col2Val, Col3Val>>;

	/*! Set the value in the \p col_name column of the attribute table, for the feature with ID \p feature_id, to 
	    \p value. The feature is looked for in the active layer.
	 */
	auto set_feature_attribute_value(Ssize feature_id, const std::string& col_name, const AttributeValue& value) 
	      -> void;

	/* Set the projection used by the shapefile to the "known reference system" with ID \p refsys_id. 
	   If a projection is already specified for the shapefile, it is overwritten.
	 */
	auto set_known_projection(KnownGeoRefsysId refsys_id) -> void;

	//! Save to file all pending changes to the active layer.
	auto save() -> void;

	TWIST_NO_COPY_NO_MOVE(Shapefile)

private:
	auto reset_reading() const -> void;

	[[nodiscard]] auto get_next_feature() const -> void*;

	[[nodiscard]] static auto get_polygons(gsl::not_null<void*> feature, MultiPolygonOption multipolygon_option) 
                               -> std::vector<GeoPolygon>;

	template<class Value>
	auto set_feature_attribute_value_impl(Ssize fid, const std::string& col_name, Value value) -> void;

	static auto get_field(gsl::not_null<void*> feature, const std::string& col_name, int& val) -> void;

	static auto get_field(gsl::not_null<void*> feature, const std::string& col_name, int64_t& val) -> void; 

	static auto get_field(gsl::not_null<void*> feature, const std::string& col_name, double& val) -> void;

	static auto get_field(gsl::not_null<void*> feature, const std::string& col_name, std::string& val) -> void; 

	class Impl;

	fs::path path_;
	std::unique_ptr<Impl> impl_;

	TWIST_CHECK_INVARIANT_DECL
};

// --- Free functions ---

//! Whether \p opt is a valid MultiPolygonOption enum value.
[[nodiscard]] constexpr auto is_valid(MultiPolygonOption opt) -> bool;

}

#include "twist/gis/Shapefile.ipp"

#endif

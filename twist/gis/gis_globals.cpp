/// @file gis_globals.cpp
/// Implementation file for "gis_globals.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/gis/gis_globals.hpp"

#include "twist/gis/SpatialReferenceSystem.hpp"
#include "twist/math/numeric_utils.hpp"

using namespace twist::math;

namespace twist::gis {

std::wstring to_string(GeoGridDataType value_type)
{
	switch (value_type) {
	case GeoGridDataType::int8              : return L"int8";
	case GeoGridDataType::uint8             : return L"uint8";
	case GeoGridDataType::int16             : return L"int16";
	case GeoGridDataType::uint16            : return L"uint16";
	case GeoGridDataType::int32             : return L"int32";
	case GeoGridDataType::uint32            : return L"uint32";
	case GeoGridDataType::float32           : return L"float32";
	case GeoGridDataType::float64           : return L"float64";
	default : return format_str(L"Invalid GeoGridDataType value %d.", value_type);
	}	
}


unsigned char bits_per_data_value(GeoGridDataType value_type)
{
	switch (value_type) {
	case GeoGridDataType::int8              : return  8;
	case GeoGridDataType::uint8             : return  8;
	case GeoGridDataType::int16             : return 16;
	case GeoGridDataType::uint16            : return 16;
	case GeoGridDataType::int32             : return 32;
	case GeoGridDataType::uint32            : return 32;
	case GeoGridDataType::float32           : return 32;
	case GeoGridDataType::float64           : return 64;
	default : TWIST_THROW(L"Invalid GeoGridDataType value %d.", value_type);
	}
}

auto to_wkt_string(KnownGeoRefsysId known_refsys_id) -> const char*
{
	static auto sa_lambert94_wktstr = 
			R"(PROJCS["GDA_1994_SA_lambert",GEOGCS["GCS_GDA_1994",DATUM["D_GDA_1994",)"
			R"(SPHEROID["GRS_1980",6378137.0,298.257222101]],PRIMEM["Greenwich",0.0],)"
			R"(UNIT["Degree",0.0174532925199433]],PROJECTION["Lambert_Conformal_Conic"],)"
			R"(PARAMETER["False_Easting",1000000.0],PARAMETER["False_Northing",2000000.0],)"
			R"(PARAMETER["Central_Meridian",135.0],PARAMETER["Standard_Parallel_1",-28.0],)"
			R"(PARAMETER["Standard_Parallel_2",-36.0],PARAMETER["Latitude_Of_Origin",-32.0],)"
			R"(UNIT["Meter",1.0]])";

	static auto vicgrid94_wktstr = 
			R"(PROJCS["GDA_1994_VICGRID94",GEOGCS["GCS_GDA_1994",DATUM["D_GDA_1994",)"
			R"(SPHEROID["GRS_1980",6378137.0,298.257222101]],PRIMEM["Greenwich",0.0],)"
			R"(UNIT["Degree",0.0174532925199433]],PROJECTION["Lambert_Conformal_Conic"],)"
			R"(PARAMETER["False_Easting",2500000.0],PARAMETER["False_Northing",2500000.0],)"
			R"(PARAMETER["Central_Meridian",145.0],PARAMETER["Standard_Parallel_1",-36.0],)"
			R"(PARAMETER["Standard_Parallel_2",-38.0],PARAMETER["Latitude_Of_Origin",-37.0],)"
			R"(UNIT["Meter",1.0]])";

	static auto nsw_lambert94_wktstr = 
			R"(PROJCS["GDA_1994_NSW_lambert",GEOGCS["GCS_GDA_1994",DATUM["D_GDA_1994",)"
			R"(SPHEROID["GRS_1980",6378137.0,298.257222101]],PRIMEM["Greenwich",0.0],)"
			R"(UNIT["Degree",0.0174532925199433]],PROJECTION["Lambert_Conformal_Conic"],)"
			R"(PARAMETER["False_Easting",9300000.0],PARAMETER["False_Northing",4500000.0],)"
			R"(PARAMETER["Central_Meridian",147.0],PARAMETER["Standard_Parallel_1",-30.75],)"
			R"(PARAMETER["Standard_Parallel_2",-35.75],PARAMETER["Latitude_Of_Origin",-33.25],)"
			R"(UNIT["Meter",1.0]])";

	static auto mga_zone_55_94_wktstr =
			R"(PROJCS["GDA_1994_MGA_zone_55",GEOGCS["GCS_GDA_1994",DATUM["D_GDA_1994",)"
			R"(SPHEROID["GRS_1980",6378137.0,298.257222101]],PRIMEM["Greenwich",0.0],)"
			R"(UNIT["Degree",0.0174532925199433]],PROJECTION["Transverse_Mercator"],)"
			R"(PARAMETER["False_Easting",500000.0],PARAMETER["False_Northing",10000000.0],)"
			R"(PARAMETER["Central_Meridian",147.0],)"
			R"(PARAMETER["Latitude_Of_Origin",0.0],)"
			R"(PARAMETER["Scale_Factor ",0.9996],)"
			R"(UNIT["Meter",1.0]])";

	static auto wgs84_wktstr = R"(GEOGCS["WGS 84",)"
									R"(DATUM["WGS_1984",)"
										R"(SPHEROID["WGS 84",6378137,298.257223563,)"
											R"(AUTHORITY["EPSG","7030"]],)"
										R"(AUTHORITY["EPSG","6326"]],)"
									R"(PRIMEM["Greenwich",0,)"
										R"(AUTHORITY["EPSG","8901"]],)"
									R"(UNIT["degree",0.0174532925199433,)"
										R"(AUTHORITY["EPSG","9122"]],)"
									R"(AUTHORITY["EPSG","4326"]])";

	switch (known_refsys_id) {
	case KnownGeoRefsysId::sa_lambert94: 
		return sa_lambert94_wktstr;
	case KnownGeoRefsysId::vicgrid94: 
		return vicgrid94_wktstr;
	case KnownGeoRefsysId::nsw_lambert94: 
		return nsw_lambert94_wktstr;
	case KnownGeoRefsysId::mga_zone_55_94: 
		return mga_zone_55_94_wktstr;
	case KnownGeoRefsysId::wgs84: 
		return wgs84_wktstr;
	default: 
		TWIST_THRO2(L"Invalid \"known\" geographic reference system ID \"{}\".", static_cast<int>(known_refsys_id));
	}
}

auto to_proj4_string(KnownGeoRefsysId known_refsys_id) -> const char*
{
	static auto sa_lambert94_proj4 = "+proj=lcc +lat_0=-32 +lon_0=135 +lat_1=-28 +lat_2=-36 "
	                                 "+x_0=1000000 +y_0=2000000 "
	                                 "+ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs +type=crs";

	static auto vicgrid94_proj4 = "+proj=lcc +lat_0=-37 +lon_0=145 +lat_1=-36 +lat_2=-38 "
	                              "+x_0=2500000 +y_0=2500000 "
	                              "+ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs +type=crs";

	static auto nsw_lambert94_proj4 = "+proj=lcc +lat_0=-33.25 +lon_0=147 +lat_1=-30.75 +lat_2=-35.75 "
	                                  "+x_0=9300000 +y_0=4500000 "
									  "+ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs +type=crs";

	static auto mga_zone_55_94_proj4 = "+proj=utm +zone=55 +south "
	                                   "+ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs +type=crs";

	static auto wgs84_proj4 = "+proj=longlat +datum=WGS84 +no_defs +type=crs";

	switch (known_refsys_id) {
	case KnownGeoRefsysId::sa_lambert94: 
		return sa_lambert94_proj4;
	case KnownGeoRefsysId::vicgrid94: 
		return vicgrid94_proj4;
	case KnownGeoRefsysId::nsw_lambert94: 
		return nsw_lambert94_proj4;
	case KnownGeoRefsysId::mga_zone_55_94: 
		return mga_zone_55_94_proj4;
	case KnownGeoRefsysId::wgs84: 
		return wgs84_proj4;
	default: 
		TWIST_THRO2(L"Invalid \"known\" geographic reference system ID \"{}\".", static_cast<int>(known_refsys_id));
	}
}

}

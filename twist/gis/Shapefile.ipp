/// @file Shapefile.cpp
/// Inline implementation file for "Shapefile.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/std_vector_utils.hpp"
#include "twist/string_utils.hpp"

namespace twist::gis {

template<class ColVal>
[[nodiscard]] auto Shapefile::read_all_features_as_polygons_with_attribute(
                           const std::string& col_name, 
                           MultiPolygonOption multipolygon_option) const -> std::vector<std::tuple<GeoPolygon, ColVal>>
{
	TWIST_CHECK_INVARIANT		
	using namespace twist;
	
	if (!has_polygon_geometry()) {
		TWIST_THROW(L"The active layer is not a \"polygon\" layer.");
	}

	auto ret = reserve_vector<std::tuple<GeoPolygon, ColVal>>(*nof_features());

    reset_reading();
	auto feature = (void*)nullptr;
    while ((feature = get_next_feature()) != nullptr) {
		auto col_val = ColVal{};
		get_field(feature, col_name, col_val);
        for (auto poly : get_polygons(feature, multipolygon_option)) {
		    ret.emplace_back(std::move(poly), col_val);
        }
    }

	return ret;
}

template<class Col1Val, class Col2Val, class Col3Val>
[[nodiscard]] auto Shapefile::read_all_features_as_polygons_with_attributes(
                           const std::string& col1_name,
						   const std::string& col2_name,
						   const std::string& col3_name,
                           MultiPolygonOption multipolygon_option) const 
			     -> std::vector<std::tuple<GeoPolygon, Col1Val, Col2Val, Col3Val>>
{
	TWIST_CHECK_INVARIANT		
	using namespace twist;
	
	if (!has_polygon_geometry()) {
		TWIST_THROW(L"The active layer is not a \"polygon\" layer.");
	}

	auto ret = reserve_vector<std::tuple<GeoPolygon, Col1Val, Col2Val, Col3Val>>(*nof_features());

    reset_reading();
	auto feature = (void*)nullptr;
    while ((feature = get_next_feature()) != nullptr) {
		auto col1_val = Col1Val{};
		get_field(feature, col1_name, col1_val);
		auto col2_val = Col2Val{};
		get_field(feature, col2_name, col2_val);
		auto col3_val = Col3Val{};
		get_field(feature, col3_name, col3_val);
        for (auto poly : get_polygons(feature, multipolygon_option)) {
            ret.emplace_back(std::move(poly), col1_val, col2_val, col3_val);
        }
    }

	return ret;
}

// --- Free functions ---

[[nodiscard]] constexpr auto is_valid(MultiPolygonOption opt) -> bool
{
    return MultiPolygonOption::split <= opt and opt <= MultiPolygonOption::error;
}

}

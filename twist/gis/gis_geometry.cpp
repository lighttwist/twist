/// @file gis_geometry.cpp
/// Implementation file for "gis_geometry.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/gis/gis_geometry.hpp"

#include "twist/math/ClosedInterval.hpp"

using twist::math::ClosedInterval;

namespace twist::gis {

// --- GeoLineString class ---

GeoLineString::GeoLineString(std::vector<GeoPoint> points)
	: points_{move(points)}
{
	if (ssize(points_) < 2) {
		TWIST_THROW(L"Cannot create a \"line string\" with less than two points.");
	}
	TWIST_CHECK_INVARIANT
}

auto GeoLineString::nof_points() const -> Ssize
{
	TWIST_CHECK_INVARIANT
	return ssize(points_);
}

auto GeoLineString::point(Ssize idx) const -> GeoPoint
{
	TWIST_CHECK_INVARIANT
	return points_.at(idx);
}

auto GeoLineString::points() const -> const std::vector<GeoPoint>&
{
	TWIST_CHECK_INVARIANT
	return points_;
}

#ifdef _DEBUG
auto GeoLineString::check_invariant() const noexcept -> void
{
	assert(ssize(points_) >= 2);
}
#endif

// --- GeoLineString class ---

GeoLinearRing::GeoLinearRing(std::vector<GeoPoint> points)
	: GeoLineString{move(points)}
{
	const auto nof_pts = nof_points();
	if (nof_pts < 3) {
		TWIST_THROW(L"Cannot create a \"linear ring\" with less than three points.");
	}
	if (point(0) != point(nof_pts - 1)) {
		TWIST_THROW(L"The last point must be the same as the first in a \"linear ring\".");
	}
}

// --- GeoLineString class ---

GeoPolygon::GeoPolygon(GeoLinearRing outer_ring, std::vector<GeoLinearRing> inner_rings)
	: outer_ring_{std::move(outer_ring)}
	, inner_rings_{move(inner_rings)}
{
	//+TODO: Check that inner rings fall within outer ring
}

auto GeoPolygon::outer_ring() const -> const GeoLinearRing&
{
	return outer_ring_;
}

auto GeoPolygon::inner_rings() const -> const std::vector<GeoLinearRing>&
{
	return inner_rings_;
}

auto GeoPolygon::is_holey() const -> bool
{
	return !inner_rings_.empty();
}

// --- GeoPointMetre class ---

GeoPointMetre::GeoPointMetre(double x, double y)
	: GeoPoint(x, y)
{
}

// --- GeoPointDecDegree class ---

GeoPointDecDegree::GeoPointDecDegree(double longitude, double latitude)
	: GeoPoint(longitude, latitude)
{
}

double GeoPointDecDegree::longitude() const
{
	return x();
}

double GeoPointDecDegree::latitude() const
{
	return y();
}

// --- GeoLonLatQuadrangle class ---

GeoLonLatQuadrangle::GeoLonLatQuadrangle(double lon1, double lat1, double lon2, double lat2)
	: GeoRectangle{lon1, lat1, lon2, lat2}
{
}

double GeoLonLatQuadrangle::left_longitude() const
{
	return left();
}

double GeoLonLatQuadrangle::right_longitude() const
{
	return right();
}

double GeoLonLatQuadrangle::bottom_latitude() const
{
	return bottom();
}

double GeoLonLatQuadrangle::top_latitude() const
{
	return top();
}

ClosedInterval<double> GeoLonLatQuadrangle::longitude_range() const
{
	return ClosedInterval{left(), right()};
}

ClosedInterval<double> GeoLonLatQuadrangle::latitude_range() const
{
	return ClosedInterval{bottom(), top()};
}

// --- GeoSize class ---

GeoSize::GeoSize()
	: width_{0}
	, height_{0}
{
	if (auto [valid, err] = validate_data(); !valid) TWIST_THROW(err.c_str());
}

GeoSize::GeoSize(double width, double height)
	: width_{width}
	, height_{height}
{
	if (auto [valid, err] = validate_data(); !valid) TWIST_THROW(err.c_str());
}

double GeoSize::width() const
{
	TWIST_CHECK_INVARIANT
	return width_;
}
	
double GeoSize::height() const
{
	TWIST_CHECK_INVARIANT
	return height_;
}

bool GeoSize::is_zero() const
{
	TWIST_CHECK_INVARIANT
	return width_ == 0;
}

std::tuple<bool, std::wstring> GeoSize::validate_data() const  // experimental, only works for read-only classes
{
	if (width_ < 0) return {false, L"Width must be positive"};
	if (height_ < 0) return {false, L"Height must be positive"};
	if ((width_ == 0) != (height_ == 0)) 
			return {false, L"Either both width and height can be zero, or neither."};
	return {true, {}};
}

#ifdef _DEBUG
void GeoSize::check_invariant() const noexcept
{
	assert(std::get<0>(validate_data()));
}
#endif

// --- Free functions ---

auto top_left(const GeoLonLatQuadrangle& quad) -> GeoPointDecDegree
{
	return GeoPointDecDegree{quad.left_longitude(), quad.top_latitude()};
}

auto top_right(const GeoLonLatQuadrangle& quad) -> GeoPointDecDegree
{
	return GeoPointDecDegree{quad.right_longitude(), quad.top_latitude()};
}

auto bottom_left(const GeoLonLatQuadrangle& quad) -> GeoPointDecDegree
{
	return GeoPointDecDegree{quad.left_longitude(), quad.bottom_latitude()};
}

auto bottom_right(const GeoLonLatQuadrangle& quad) -> GeoPointDecDegree
{
	return GeoPointDecDegree{quad.right_longitude(), quad.bottom_latitude()};
}

auto inflate(const GeoLonLatQuadrangle& quad, double dlon, double dlat) -> GeoLonLatQuadrangle
{
	return GeoLonLatQuadrangle{quad.left_longitude() - dlon, 
	                           quad.bottom_latitude() - dlat, 
			                   quad.right_longitude() + dlon, 
							   quad.top_latitude() + dlat};
}

auto calc_bounding_rect(const GeoLineString& line_string) -> GeoRectangle
{
	using Limits = std::numeric_limits<double>;
	auto left = Limits::max();
	auto bottom = Limits::max();
	auto top = Limits::lowest();
	auto right = Limits::lowest();
	for (auto pt : line_string.points()) {
		if (pt.x() < left) {
			left = pt.x();
		}
		else if (pt.x() > right) {
			right = pt.x();
		}
		if (pt.y() < bottom) {
			bottom = pt.y();
		}
		else if (pt.y() > top) {
			top = pt.y();
		}
	}
	return GeoRectangle{left, bottom, right, top};
}

auto calc_bounding_rect(const GeoPolygon& poly) -> GeoRectangle
{
	return calc_bounding_rect(poly.outer_ring());
}

}

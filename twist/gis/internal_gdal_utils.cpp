/// @file internal_gdal_utils.cpp
/// Implementation file for "internal_gdal_utils.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

//  See http://www.gdal.org/files.html

#include "twist/twist_maker.hpp"

#include "twist/gis/internal_gdal_utils.hpp"

#include "twist/cast.hpp"
#include "twist/std_vector_utils.hpp"
#include "twist/gis/gis_globals.hpp"

#include "gdal/include/gdal_priv.h"
#include "gdal/include/ogr_geometry.h"

using gsl::not_null;

namespace twist::gis {

// --- Local functions ---

auto get_gdal_driver_by_name(const std::string& name) -> not_null<GDALDriver*>
{
	auto driver_mgr = not_null{GetGDALDriverManager()};
	return not_null{driver_mgr->GetDriverByName(name.c_str())};
}

// --- Global functions ---

auto get_gdal_geotiff_driver() -> not_null<GDALDriver*>
{
	return get_gdal_driver_by_name("GTiff");
}

auto get_gdal_shapefile_driver() -> not_null<GDALDriver*>
{
	return get_gdal_driver_by_name("ESRI Shapefile");
}

auto gdal_open(const fs::path& path, bool read_only) -> not_null<GDALDataset*>
{
	auto flags = static_cast<unsigned int>(read_only ? GDAL_OF_READONLY : GDAL_OF_UPDATE);
	flags |= GDAL_OF_VERBOSE_ERROR;

	auto dset = GDALOpenEx(path.string().c_str(), 
	                       flags, 
						   nullptr/*allowed_drivers*/, 
						   nullptr/*open_options*/, 
						   nullptr/*sibling_files*/);
	if (!dset) {
		TWIST_THRO2(L"Error opening file \"{}\": {}", path.filename().c_str(), get_last_cpl_error_msg());
	}
	return static_nonull_cast<GDALDataset*>(dset);
}

auto gdal_close(not_null<GDALDataset*> dset) -> void
{
	GDALClose(dset.get());
}

auto gdal_close(GDALDatasetH dset_hnd) -> void
{
	if (dset_hnd) {
		GDALClose(dset_hnd);
	}
}

auto to_gdal(GeoPoint point) -> std::unique_ptr<OGRPoint>
{
	return std::make_unique<OGRPoint>(point.x(), point.y());
}

auto to_gdal(const GeoLineString& line_string) -> std::unique_ptr<OGRLineString>
{
	auto gdal_geometry = std::make_unique<OGRLineString>();
	const auto nof_points = static_cast<int>(line_string.nof_points());
	gdal_geometry->setNumPoints(nof_points);
	for (auto i : IndexRange{nof_points}) {
		const auto point = line_string.point(i);
		gdal_geometry->setPoint(i, point.x(), point.y());
	}
	return gdal_geometry;
}

auto to_gdal(const GeoLinearRing& linear_ring) -> std::unique_ptr<OGRLinearRing>
{
	auto gdal_geometry = std::make_unique<OGRLinearRing>();
	const auto nof_points = static_cast<int>(linear_ring.nof_points());
	gdal_geometry->setNumPoints(nof_points);
	for (auto i : IndexRange{nof_points}) {
		const auto point = linear_ring.point(i);
		gdal_geometry->setPoint(i, point.x(), point.y());
	}
	return gdal_geometry;
}

auto from_gdal(not_null<const OGRLinearRing*> linear_ring) -> GeoLinearRing
{
	auto points = reserve_vector<GeoPoint>(linear_ring->getNumPoints());

	auto iter = not_null{linear_ring->getPointIterator()};
	auto gdal_point = OGRPoint{};
	while (iter->getNextPoint(&gdal_point)) {
		points.emplace_back(gdal_point.getX(), gdal_point.getY());		
	}
	OGRPointIterator::destroy(iter);

	return GeoLinearRing{move(points)};
}

auto to_gdal(const GeoPolygon& polygon) -> std::unique_ptr<OGRPolygon>
{
	auto gdal_geometry = std::make_unique<OGRPolygon>();
	gdal_geometry->addRingDirectly(to_gdal(polygon.outer_ring()).release());
	for (const auto& inner_ring : polygon.inner_rings()) {
		gdal_geometry->addRingDirectly(to_gdal(inner_ring).release());	
	}
	return gdal_geometry;
}

auto ogr_err_name(int err_no) -> std::wstring 
{
	switch (err_no) {
	case OGRERR_NONE: return L"OGRERR_NONE";  // Success 
	case OGRERR_NOT_ENOUGH_DATA: return L"OGRERR_NOT_ENOUGH_DATA";  // Not enough data to deserialize 
	case OGRERR_NOT_ENOUGH_MEMORY: return L"OGRERR_NOT_ENOUGH_MEMORY";  // Not enough memory 
	case OGRERR_UNSUPPORTED_GEOMETRY_TYPE: return L"OGRERR_UNSUPPORTED_GEOMETRY_TYPE";  // Unsupported geometry type 
	case OGRERR_UNSUPPORTED_OPERATION: return L"OGRERR_UNSUPPORTED_OPERATION";  // Unsupported operation 
	case OGRERR_CORRUPT_DATA: return L"OGRERR_CORRUPT_DATA";  // Corrupt data 
	case OGRERR_FAILURE: return L"OGRERR_FAILURE";  // Failure 
	case OGRERR_UNSUPPORTED_SRS: return L"OGRERR_UNSUPPORTED_SRS";  // Unsupported SRS 
	case OGRERR_INVALID_HANDLE: return L"OGRERR_INVALID_HANDLE";  // Invalid handle 
	case OGRERR_NON_EXISTING_FEATURE: return L"OGRERR_NON_EXISTING_FEATURE";  // Non existing feature. Added in GDAL 2.0 
	default: return L"Unrecognised error number";
	}
}

}

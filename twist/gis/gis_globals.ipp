#include "gis_globals.hpp"
///  @file  gis_globals.ipp
///  Inline implementation file for "gis_globals.hpp"

//   Copyright (c) 2007-2025 Dan Ababei
//	 All rights reserved
//
//	 Redistribution and use in source and binary forms, with or without modification, are permitted provided 
//   that the following conditions are met:
//      * Redistributions of source code must retain the above copyright notice, this list of conditions and 
//        the following disclaimer.
//	    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
//        and the following disclaimer in the documentation and/or other materials provided with the 
//        distribution.
//      * The name Dan Ababei may not be used to endorse or promote products derived from this software 
//        without specific prior written permission.
//
//	 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED 
//   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
//   PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, 
//   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
//   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
//   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR 
//   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
//   DAMAGE.

namespace twist::gis {

constexpr auto as_long_name(KnownGeoRefsysId refsys_id, bool incl_epsg) -> std::wstring
{
	using enum KnownGeoRefsysId;
	switch (refsys_id) {
	case sa_lambert94: 
		return incl_epsg ? L"EPSG:3107, GDA94 / SA Lambert" : L"GDA94 / SA Lambert";
	case vicgrid94: 
		return incl_epsg ? L"EPSG:3111, GDA94 / Vicgrid94" : L"GDA94 / Vicgrid94";
	case nsw_lambert94: 
		return incl_epsg ? L"EPSG:3308, GDA94 / NSW Lambert" : L"GDA94 / NSW Lambert";
	case mga_zone_55_94:
		return incl_epsg ? L"EPSG:28355, GDA94 / MGA zone 55" : L"GDA94 / MGA zone 55";
	case wgs84:
		return incl_epsg ? L"EPSG:4326, WGS84 / World Geodetic System 1984" : L"WGS84 / World Geodetic System 1984";
	default: 
		TWIST_THROW(L"Invalid geo reference system ID %d.", refsys_id);
	}
}

[[nodiscard]] constexpr auto known_geo_refsys_id_from_long_name(std::wstring_view long_name) -> KnownGeoRefsysId
{
	using enum KnownGeoRefsysId;
	if (long_name == L"EPSG:3107, GDA94 / SA Lambert" || long_name == L"GDA94 / SA Lambert") {
		return sa_lambert94;
	}
	if (long_name == L"EPSG:3111, GDA94 / Vicgrid94" || long_name == L"GDA94 / Vicgrid94") {
		return vicgrid94;
	}
	if (long_name == L"EPSG:3308, GDA94 / NSW Lambert" || long_name == L"GDA94 / NSW Lambert") {
		return nsw_lambert94;
	}
	if (long_name == L"EPSG:28355, GDA94 / MGA zone 55" || long_name == L"GDA94 / MGA zone 55") {
		return mga_zone_55_94;
	}
	if (long_name == L"EPSG:4326, WGS84 / World Geodetic System 1984" || 
			long_name == L"WGS84 / World Geodetic System 1984") {
		return wgs84;
	}
	TWIST_THRO2(L"Invalid \"known geographic reference system ID\" long string \"{}\".", long_name);
}

constexpr auto is_valid(KnownGeoRefsysId refsys_id) -> bool
{
	return has(all_known_geo_refsys_ids(), refsys_id);
}

constexpr auto all_known_geo_refsys_ids() -> std::vector<KnownGeoRefsysId>
{
	using enum KnownGeoRefsysId;
	return std::vector{sa_lambert94, vicgrid94, nsw_lambert94, mga_zone_55_94, wgs84};
}

template<class T>
void check_data_value_type(GeoGridDataType value_type)
{
	const auto& templ_type_id = typeid(T);
	const auto templ_type_name = get_name(templ_type_id);

	switch (value_type) {
	case GeoGridDataType::int8: 
		if (templ_type_id != typeid(int8_t)) {
			TWIST_THROW(L"Incompatible types \"int8\" and \"%s\".", templ_type_name.c_str());
		}
		break;
	case GeoGridDataType::uint8: 
		if (templ_type_id != typeid(uint8_t)) {
			TWIST_THROW(L"Incompatible types \"uint8\" and \"%s\".", templ_type_name.c_str());
		}
		break;
	case GeoGridDataType::int16: 
		if (templ_type_id != typeid(int16_t)) {
			TWIST_THROW(L"Incompatible types \"int16\" and \"%s\".", templ_type_name.c_str());
		}
		break;
	case GeoGridDataType::uint16: 
		if (templ_type_id != typeid(uint16_t)) {
			TWIST_THROW(L"Incompatible types \"uint16\" and \"%s\".", templ_type_name.c_str());
		}
		break;
	case GeoGridDataType::int32: 
		if (templ_type_id != typeid(int32_t)) {
			TWIST_THROW(L"Incompatible types \"int32\" and \"%s\".", templ_type_name.c_str());
		}
		break;
	case GeoGridDataType::uint32: 
		if (templ_type_id != typeid(uint32_t)) {
			TWIST_THROW(L"Incompatible types \"uint32\" and \"%s\".", templ_type_name.c_str());
		}
		break;
	case GeoGridDataType::float32: 
		if (templ_type_id != typeid(float)) {
			TWIST_THROW(L"Incompatible types \"float32\" and \"%s\".", templ_type_name.c_str());
		}
		break;
	case GeoGridDataType::float64: 
		if (templ_type_id != typeid(double)) {
			TWIST_THROW(L"Incompatible types \"float64\" and \"%s\".", templ_type_name.c_str());
		}
		break;
	default: 
		TWIST_THROW(L"Invalid data value type ID %d.", value_type);
	}
}

}

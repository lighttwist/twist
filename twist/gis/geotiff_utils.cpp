/// @file geotiff_utils.cpp
/// Implementation file for "geotiff_utils.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/gis/geotiff_utils.hpp"

#include "twist/std_variant_utils.hpp"
#include "twist/gis/gis_geometry.hpp"
#include "twist/img/Tiff.hpp"

#include "external/libtiff/include/tiffconf.h"
#include "external/libtiff/include/tiffio.h"

using namespace twist::img;
using namespace twist::math;

// Tag values for the fields where TIFFs store GeoTIFF-specific information
static const auto model_pixel_scale_tifftag = 33550u;
static const auto model_transformation_tag  = 34264u; 
static const auto model_tie_point_tifftag   = 33922u;
static const auto geo_key_directory_tifftag = 34735u;  // Alias: ProjectionInfoTag, CoordSystemInfoTag
static const auto geo_double_params_tifftag = 34736u; 
static const auto geo_ascii_params_tifftag  = 34737u;
static const auto nodata_tifftag            = 42113u;

/* More info at https://docs.ogc.org/is/19-008r4/19-008r4.html
                http://geotiff.maptools.org/spec/geotiff2.6.html#2.6

	ModelTiepointTag:
		  Tag = 33922 (8482.H) 
		  Type = DOUBLE (IEEE Double precision)
		  N = 6*K,  K = number of tiepoints
		  Alias: GeoreferenceTag

	ModelPixelScaleTag:
		  Tag = 33550
		  Type = DOUBLE (IEEE Double precision)
		  N = 3

	ModelTransformationTag
		  Tag  =  34264  (85D8.H) 
		  Type =  DOUBLE    
		  N    =  16
*/

namespace twist::gis {

// --- GeotiffProjection ---

GeotiffProjection::GeotiffProjection(GtiffProjHeader header)
	: header_{ new GtiffProjHeader(header) }
{
	TWIST_CHECK_INVARIANT
}


GtiffProjHeader GeotiffProjection::header() const
{
	TWIST_CHECK_INVARIANT
	return *header_;
}


std::optional<uint16_t> GeotiffProjection::get_uint16_value(GtiffProjKey key) const
{
	TWIST_CHECK_INVARIANT
	return get_value<uint16_t>(key, L"uint16");
}


std::optional<double> GeotiffProjection::get_float64_value(GtiffProjKey key) const
{
	TWIST_CHECK_INVARIANT
	return get_value<double>(key, L"float64");
}


std::optional<std::vector<double>> GeotiffProjection::get_float64_array_value(GtiffProjKey key) const
{
	TWIST_CHECK_INVARIANT
	return get_value<std::vector<double>>(key, L"float64 array");
}


std::optional<std::string> GeotiffProjection::get_ascii_value(GtiffProjKey key) const
{
	TWIST_CHECK_INVARIANT
	return get_value<std::string>(key, L"ascii");
}


void GeotiffProjection::add_uint16_value(GtiffProjKey key, uint16_t value)
{
	TWIST_CHECK_INVARIANT
	add_value(key, value);
}


void GeotiffProjection::add_float64_value(GtiffProjKey key, double value)
{
	TWIST_CHECK_INVARIANT
	add_value(key, value);
}


void GeotiffProjection::add_float64_array_value(GtiffProjKey key, const std::vector<double>& value)
{
	TWIST_CHECK_INVARIANT
	add_value(key, value);
}


void GeotiffProjection::add_ascii_value(GtiffProjKey key, const std::string& value)
{
	TWIST_CHECK_INVARIANT
	add_value(key, value);
}


GeotiffProjection::KeyValueRange GeotiffProjection::values() const
{
	TWIST_CHECK_INVARIANT
	return Range{begin(map_), end(map_)};  //+TODO: very strangely, begin(map_) crashes in debug mode if map_ is empty!
}


template<class Val>
std::optional<Val> GeotiffProjection::get_value(GtiffProjKey key, const std::wstring& val_type) const
{
	TWIST_CHECK_INVARIANT
	auto it = map_.find(key);
	if (it == end(map_)) return {};
	if (!std::holds_alternative<Val>(it->second)) {
		TWIST_THROW(L"The value for key %d is not of the '%s' type.", key, val_type.c_str());
	}
	return std::get<Val>(it->second);
}


template<class Val>
void GeotiffProjection::add_value(GtiffProjKey key, const Val& value)
{
	TWIST_CHECK_INVARIANT
	const auto result = map_.emplace(key, value);
	if (!result.second) {
		TWIST_THROW(L"Key %d already exists in the map.", key);
	}
}


#ifdef _DEBUG
void GeotiffProjection::check_invariant() const noexcept
{
	assert(header_);
}
#endif

// --- Local types ---

struct ProjectionKeyEntry { 
	const uint16_t key_id;
	const uint16_t tiff_tag_location;
	const uint16_t count;
	const uint16_t value_offset;
};

// --- Local functions ---

bool is_valid(GtiffProjKey key)
{
	return (key >= GtiffProjKey::gt_model_type && key <= GtiffProjKey::gt_citation) ||
	       (key >= GtiffProjKey::geographic_type && key <= GtiffProjKey::geog_to_wgs84) || 
		   (key >= GtiffProjKey::projected_cs_type && key 
				<= GtiffProjKey::proj_straight_vert_pole_long) ||
		   (key >= GtiffProjKey::vertical_cs_type && key <= GtiffProjKey::vertical_units);
}

// --- Free functions ---

auto read_geo_data_grid_info(const Tiff& tiff, bool& flip_y) -> std::optional<GeoDataGridInfo>
{
	// Read the geographic space grid underlying the raster
    auto pixel_size_x = 0.0;
    auto pixel_size_y = 0.0;
    flip_y = false;
    auto bottom = 0.0;
    auto left = 0.0;

    if (tiff.has_field(model_pixel_scale_tifftag)) {
	    // See http://build-failed.blogspot.com.au/2014/12/processing-geotiff-files-in-net-without.html
	    //     https://docs.thefoundry.co.uk/products/nuke/developers/63/ndkdevguide/examples/tiffReader.cpp
	    const auto model_pixel_scale = tiff.get_double_field_values(model_pixel_scale_tifftag);  
	    pixel_size_x = model_pixel_scale[0];
	    pixel_size_y = model_pixel_scale[1];
        flip_y = model_pixel_scale[1] < 0.0; 
   
	    const auto model_tiepoint = tiff.get_double_field_values(model_tie_point_tifftag);  
	    auto origin_x = model_tiepoint[3];
	    auto origin_y = model_tiepoint[4];

	    left = origin_x;
	    bottom = origin_y - tiff.height() * pixel_size_y;
   }
   else if (tiff.has_field(model_transformation_tag)) {
        // See http://geotiff.maptools.org/spec/geotiff2.6.html
        const auto transform = tiff.get_double_field_values(model_transformation_tag);
        if (ssize(transform) != 16) {
            TWIST_THRO2(L"TIFF ModelTransformationTag must contain 16 components.");
        }
        if (transform[12] != 0 || transform[13] != 0 || transform[14] != 0 || transform[15] != 1.0) {
            TWIST_THRO2(L"TIFF ModelTransformationTag matrix transform not supported.");
        }
        if (transform[1] != 0 || transform[2] != 0 || transform[4] != 0 || transform[6] != 0) {
            TWIST_THRO2(L"TIFF ModelTransformationTag matrix transform not supported.");
        }

        pixel_size_x = std::fabs(transform[0]);
        pixel_size_y = std::fabs(transform[5]);
        flip_y = transform[5] > 0.0;

        auto origin_x = transform[3];
        auto origin_y = transform[7];

	    left = origin_x;
	    bottom = origin_y;
    }
    else {
		return std::nullopt;  // The TIFF is not a GeoTIFF		  
    }

	// Use a (wildly arbitrary) tolerance while determining the (square) cell's size, to deal with situations such as 
    // pixel_size_x = 180.00000000000125, pixel_size_y = 180.00000000000676, and to check whether it should be integer
	static const double pixel_tol = 1e-11;

    double cell_size = pixel_size_x;
    if (pixel_size_x != pixel_size_y) {
	    if (!equal_tol(pixel_size_x, pixel_size_y, pixel_tol)) {
		    TWIST_THRO2(L"Grid cell width {} does not match cell height {} in TIFF \"{}\".", 
				        pixel_size_x, pixel_size_y, tiff.path().filename().c_str());
	    }
	    cell_size = std::trunc(pixel_size_x / pixel_tol) * pixel_tol;
    }
    if (equal_tol(cell_size, std::round(cell_size), pixel_tol)) {
        cell_size = std::round(cell_size);    
    }

	const auto geo_space_grid = SquareGeoSpaceGrid{left, bottom, cell_size, 
                                                   static_cast<int>(tiff.height()), static_cast<int>(tiff.width())};

	// Read "no-data" placeholder value, if any
	std::string nodata_value;
	if (tiff.has_field(nodata_tifftag)) {
		nodata_value = tiff.get_string_field_value(nodata_tifftag);
	}

	return std::make_optional<GeoDataGridInfo>(geo_space_grid, move(nodata_value), tiff.sample_data_type());
}

auto read_geo_data_grid_info(const Tiff& tiff) -> std::optional<GeoDataGridInfo>
{
    auto flip_y = false;
    return read_geo_data_grid_info(tiff, flip_y);
}

auto read_geo_data_grid_info(fs::path path) -> std::optional<GeoDataGridInfo>
{
	return read_geo_data_grid_info(Tiff{Tiff::open_read, std::move(path)});
}

auto write_geo_data_grid_info(Tiff& tiff, const GeoDataGridInfo& info) -> void
{
	// model_pixel_scale_tifftag
	if (!tiff.has_field(model_pixel_scale_tifftag)) {
		tiff.define_field(model_pixel_scale_tifftag, GeoGridDataType::float64); 		
	}
	const std::vector<double> model_pixel_scale{info.space_grid().cell_width(), info.space_grid().cell_height()};
	tiff.set_double_field_values(model_pixel_scale_tifftag, model_pixel_scale);  

	// model_tie_point_tifftag
	if (!tiff.has_field(model_tie_point_tifftag)) {
		tiff.define_field(model_tie_point_tifftag, GeoGridDataType::float64); 		
	}
	const auto origin = perimeter_rect(info.space_grid()).top_left();
	const std::vector<double> model_tiepoint{0, 0, 0, origin.x(), origin.y(), 0};
	tiff.set_double_field_values(model_tie_point_tifftag, model_tiepoint);  

	if (!info.nodata_value().empty()) {
		if (!tiff.has_field(nodata_tifftag)) {
			tiff.define_ascii_field(nodata_tifftag);
		}
		tiff.set_string_field_value(nodata_tifftag, info.nodata_value());
	}
}


void replace_geo_data_grid_info(const fs::path& path, const GeoDataGridInfo& info)
{
	// There doesn't seem to be a simple way to overwrite the geographic grid info if it exists in a GeoTIFF
	// so we simply read the contents of the existing file and save it in a new one; on success, we replace
	// the old one with the new

	auto sample_data_type = std::optional<GeoGridDataType>{};
	Tiff::DataGrid<uint8_t> data{};
	std::optional<GeotiffProjection> proj_info;
	{	
		Tiff in_tif{Tiff::open_read, path};
		if (info.space_grid().nof_columns() != in_tif.width() || 
				info.space_grid().nof_rows() != in_tif.height()) {
			TWIST_THROW(L"Error writing geographical data grid information to \"%s\": "
					L"The TIFF dimensions do not match the geographical data grid info.",
					in_tif.path().filename().c_str());
		}
		sample_data_type = in_tif.sample_data_type();
		data = in_tif.read_scanlined<uint8_t>();
		proj_info = read_projection_info(in_tif);
	}
	assert(sample_data_type);
	const auto temp_out_path = get_unused_path(path);
	{
		auto out_tif = Tiff{Tiff::open_write, 
		                    temp_out_path, 
							*sample_data_type, 
				            info.space_grid().nof_columns(), 
							info.space_grid().nof_rows()};
		out_tif.write_scanlined<uint8_t>(data);
		write_geo_data_grid_info(out_tif, info);
		if (proj_info) {
			write_projection_info(out_tif, *proj_info);
		}
	}

	// All good, replace the old file with the new 
	remove(path);
	rename(temp_out_path, path);
}

auto read_projection_info(const Tiff& tiff) -> std::optional<GeotiffProjection>
{
	// http://geotiff.maptools.org/spec/geotiff2.4.html

	if (!tiff.has_field(geo_key_directory_tifftag)) {
		return std::nullopt;
	}

	const auto geo_key_dir_values = tiff.get_uint16_field_values(geo_key_directory_tifftag);
	if (geo_key_dir_values.empty()) {
		return std::nullopt;
	}
	if (geo_key_dir_values.size() % 4) {
		TWIST_THRO2(L"Projection info in geotiff \"{}\" is corrupt.", tiff.path().filename().c_str());
	}
	 
	auto proj_info = std::make_optional<GeotiffProjection>(GtiffProjHeader{geo_key_dir_values[0], 
	                                                                       geo_key_dir_values[1], 
																		   geo_key_dir_values[2]});

	auto key_count = size_t{geo_key_dir_values[3]};
	auto key_entries = reserve_vector<ProjectionKeyEntry>(key_count);

	const auto tag_entry_count = geo_key_dir_values.size() / 4;
	assert(key_count <= tag_entry_count - 1);
	if (key_count > tag_entry_count - 1) {
		key_count = tag_entry_count - 1;
	}

	for (auto i : IndexRange{key_count}) {
		const auto first_value_idx = (i + 1) * 4;
		key_entries.emplace_back(geo_key_dir_values[first_value_idx], 
		                         geo_key_dir_values[first_value_idx + 1], 
				                 geo_key_dir_values[first_value_idx + 2], 
								 geo_key_dir_values[first_value_idx + 3]);
	}

	auto double_params = std::vector<double>{};
	if (tiff.has_field(geo_double_params_tifftag)) {
		double_params = tiff.get_double_field_values(geo_double_params_tifftag);
	}

	auto ascii_params = std::string{};
	if (tiff.has_field(geo_ascii_params_tifftag)) {
		ascii_params = tiff.get_string_field_value(geo_ascii_params_tifftag);
	}

	for (auto i : IndexRange{key_count}) {
		const auto& key_entry = key_entries[i];

		const auto key = static_cast<GtiffProjKey>(key_entry.key_id);
		
		const bool is_valid_key = is_valid(key);
		assert(is_valid_key);
		if (is_valid_key) {
			
			if (key_entry.tiff_tag_location == 0) {
				// Value type is 'uint16', and the value is stored in the entry itself, as the offset
				proj_info->add_uint16_value(key, key_entry.value_offset);
			}
			else if (key_entry.tiff_tag_location == geo_double_params_tifftag) {
				assert(key_entry.count > 0);
				assert(key_entry.value_offset < double_params.size());

				if (key_entry.value_offset < double_params.size()) {

					if (key_entry.count == 1) {
						// Value type is 'float64', and the value is stored in the 
						// 'geo_double_params_tifftag' tag
						proj_info->add_float64_value(key, double_params[key_entry.value_offset]);
					}
					else {
						// Value type is 'float64 array', and the array elements are stored in the 
						// 'geo_double_params_tifftag' tag
						const auto array_first = begin(double_params) + key_entry.value_offset;
						proj_info->add_float64_array_value(key, {array_first, array_first + key_entry.count});					
					}
				}
			}
			else if (key_entry.tiff_tag_location == geo_ascii_params_tifftag) {
				// Value type is text, and the value is stored in the geo_ascii_params_tifftag tag
				assert(key_entry.value_offset < ascii_params.size());
				if (key_entry.value_offset < ascii_params.size()) {					
					
					auto count = size_t{key_entry.count};
					assert(key_entry.value_offset + count <= ascii_params.size()); // maybe includes the final null character
					if (key_entry.value_offset + count >= ascii_params.size()) {
						count = std::string::npos;
					}

					const auto value = trim_trail_char(ascii_params.substr(key_entry.value_offset, count), '|');
					proj_info->add_ascii_value(key, value);
				}
			}
			else {
				// Unrecognised tag location
				assert(false);
			}
		}
	}

 	return proj_info;
}

auto write_projection_info(Tiff& tiff, const GeotiffProjection& info) -> void
{
	auto key_entries = std::vector<ProjectionKeyEntry>{};
	auto double_params = std::vector<double>{};
	auto ascii_params = std::string{};

	for (const auto& kv : info.values()) {
		const auto key = kv.first;
		const auto is_valid_key = is_valid(key);
		assert(is_valid_key);
		if (is_valid_key) {

			const auto& value = kv.second;
			if (holds_uint16(value)) {
				// Value type is 'uint16', and the value is stored in the entry itself, as the offset
				key_entries.emplace_back(static_cast<uint16_t>(key), 
				                         uint16_t{0}, 
										 uint16_t{1}, 
										 get_uint16(value));				
			}
			else if (holds_double(value)) {
				// Value type is 'float64', and the value is stored in the 'geo_double_params_tifftag' tag
				const auto offset = static_cast<uint16_t>(double_params.size());
				double_params.push_back(get_double(value));
				key_entries.emplace_back(static_cast<uint16_t>(key), 
				                         static_cast<uint16_t>(geo_double_params_tifftag), 
										 uint16_t{1}, 
										 offset);				
			}
			else if (holds_vector<double>(value)) {
				// Value type is 'float64 array', and the array elements are stored in the 
				// 'geo_double_params_tifftag' tag
				const auto offset = static_cast<uint16_t>(double_params.size());
				const auto values = get_vector<double>(value); 
				double_params.insert(end(double_params), begin(values), end(values));
				key_entries.emplace_back(static_cast<uint16_t>(key), 
				                         static_cast<uint16_t>(geo_double_params_tifftag), 
						                 static_cast<uint16_t>(values.size()), 
										 offset);				
			}
			else if (holds_string(value)) {
				// Value type is 'ascii', and the value is stored in the 'geo_ascii_params_tifftag' tag
				const auto offset = static_cast<uint16_t>(ascii_params.size());
				const auto count = static_cast<uint16_t>(get_string(value).size() + 1);
				ascii_params += get_string(value) + "|";
				key_entries.emplace_back(static_cast<uint16_t>(key), 
				                         static_cast<uint16_t>(geo_ascii_params_tifftag), 
										 count, 
										 offset);				
			}
		}
	}

	// Put together the array of numbers for the "geo key directory"/"projection info" tag, which consists of
	//   * The three header values
	//   * The number of entries
	//   * For each entry, the four value making up that entry

	auto geo_key_dir_values = std::vector<uint16_t>{info.header().key_directory_version,
													info.header().key_revision,
													info.header().minor_revision};

	geo_key_dir_values.push_back(static_cast<uint16_t>(key_entries.size()));

	for (const auto& e : key_entries) {
		geo_key_dir_values.push_back(e.key_id);
		geo_key_dir_values.push_back(e.tiff_tag_location);
		geo_key_dir_values.push_back(e.count);
		geo_key_dir_values.push_back(e.value_offset);
	}

	// Write to the "GeoKeyDirectory" tag
	if (!tiff.has_field(geo_key_directory_tifftag)) {
		tiff.define_field(geo_key_directory_tifftag, GeoGridDataType::uint16);
	}
	tiff.set_uint16_field_values(geo_key_directory_tifftag, geo_key_dir_values);

	if (!double_params.empty()) {
		// Write to the "GeoDoubleParams" tag
		if (!tiff.has_field(geo_double_params_tifftag)) {
			tiff.define_field(geo_double_params_tifftag, GeoGridDataType::float64);
		}
		tiff.set_double_field_values(geo_double_params_tifftag, double_params);
	}

	if (!ascii_params.empty()) {
		ascii_params += "|";
		if (!tiff.has_field(geo_ascii_params_tifftag)) {
			tiff.define_ascii_field(geo_ascii_params_tifftag);  
		}
		tiff.set_string_field_value(geo_ascii_params_tifftag, ascii_params);
	}
}

auto geotiff_projection_from_known_geo_refsys(KnownGeoRefsysId refsys_id) -> GeotiffProjection
{
	auto proj = GeotiffProjection{GtiffProjHeader{1/*key_directory_version*/,
											      1/*key_revision*/,
												  0/*minor_revision*/}};
	switch (refsys_id) {
		using enum KnownGeoRefsysId;
	case vicgrid94: 
		proj.add_uint16_value(GtiffProjKey::gt_model_type, 1);
		proj.add_uint16_value(GtiffProjKey::gt_raster_type, 1);
		proj.add_ascii_value(GtiffProjKey::gt_citation, "GDA94 / Vicgrid");
		proj.add_ascii_value(GtiffProjKey::geog_citation, "GDA94");
		proj.add_uint16_value(GtiffProjKey::geog_angular_units, GtiffAngularUnitCodes::angular_degree);
		proj.add_uint16_value(GtiffProjKey::projected_cs_type, GtiffProjectedCsTypeValues::vicgrid);
		proj.add_uint16_value(GtiffProjKey::proj_linear_units, GtiffProjUint16Value::linear_meter);
		return proj;
	case nsw_lambert94: 
		proj.add_uint16_value(GtiffProjKey::gt_model_type, 1);
		proj.add_uint16_value(GtiffProjKey::gt_raster_type, 1);
		proj.add_ascii_value(GtiffProjKey::gt_citation, "GDA94 / NSW Lambert");
		proj.add_ascii_value(GtiffProjKey::geog_citation, "GDA94");
		proj.add_uint16_value(GtiffProjKey::geog_angular_units, GtiffAngularUnitCodes::angular_degree);
		proj.add_uint16_value(GtiffProjKey::projected_cs_type, GtiffProjectedCsTypeValues::nsw_lambert);
		proj.add_uint16_value(GtiffProjKey::proj_linear_units, GtiffProjUint16Value::linear_meter);
		return proj;
	case sa_lambert94: 
		proj.add_uint16_value(GtiffProjKey::gt_model_type, 1);
		proj.add_uint16_value(GtiffProjKey::gt_raster_type, 1);
		proj.add_ascii_value(GtiffProjKey::gt_citation, "GDA94 / SA Lambert");
		proj.add_ascii_value(GtiffProjKey::geog_citation, "GDA94");
		proj.add_uint16_value(GtiffProjKey::geog_angular_units, GtiffAngularUnitCodes::angular_degree);
		proj.add_uint16_value(GtiffProjKey::projected_cs_type, GtiffProjectedCsTypeValues::sa_lambert);
		proj.add_uint16_value(GtiffProjKey::proj_linear_units, GtiffProjUint16Value::linear_meter);
		return proj;
	case mga_zone_55_94: 
		proj.add_uint16_value(GtiffProjKey::gt_model_type, 1);
		proj.add_uint16_value(GtiffProjKey::gt_raster_type, 1);
		proj.add_ascii_value(GtiffProjKey::gt_citation, "GDA94 / MGA zone 55");
		proj.add_ascii_value(GtiffProjKey::geog_citation, "GDA94");
		proj.add_uint16_value(GtiffProjKey::geog_angular_units, GtiffAngularUnitCodes::angular_degree);
		proj.add_uint16_value(GtiffProjKey::projected_cs_type, GtiffProjectedCsTypeValues::mga_zone_55);
		proj.add_uint16_value(GtiffProjKey::proj_linear_units, GtiffProjUint16Value::linear_meter);
		return proj;
	case wgs84:
		proj.add_uint16_value(GtiffProjKey::gt_model_type, 2);
		proj.add_uint16_value(GtiffProjKey::gt_raster_type, 1);
		proj.add_uint16_value(GtiffProjKey::geographic_type, 4326/*GCS_WGS_84*/);
		proj.add_ascii_value(GtiffProjKey::geog_citation, "WGS 84");
		proj.add_uint16_value(GtiffProjKey::geog_angular_units, GtiffAngularUnitCodes::angular_degree);
		proj.add_float64_value(GtiffProjKey::geog_semi_major_axis, 6378137.0);
		proj.add_float64_value(GtiffProjKey::geog_inv_flattening, 298.257223563);
		return proj;
	default:
		TWIST_THROW(L"Unrecognised reference system ID {}.", static_cast<int>(refsys_id)); 
	}
}

auto find_matching_known_geo_refsys(const GeotiffProjection& proj) -> std::optional<KnownGeoRefsysId>
{
	const auto refsys_ids = all_known_geo_refsys_ids();
	if (auto it = rg::find_if(refsys_ids, [&proj](auto id) { return match(proj, id); }); it != end(refsys_ids)) {
		return *it;
	}
	return std::nullopt;
}

auto match(const GeotiffProjection& proj, KnownGeoRefsysId known_refsys_id) -> bool
{
	static const auto float_tol = 1E-6;

	bool ret = true;
	std::optional<unsigned int> proj_cs_type;

	auto uint16_value_is = [&proj](auto key, auto expected_val) {
		const auto val = proj.get_uint16_value(key);
		return val && val == expected_val;
	};
	auto uint16_value_is_missing_user_defined_or_equals = [&proj](auto key, auto expected_val) {
		const auto val = proj.get_uint16_value(key);
		return !val || (val == GtiffProjUint16Value::user_defined) || (val == expected_val);
	};
	auto float64_value_is = [&proj](auto key, auto expected_val) {
		const auto val = proj.get_float64_value(key);
		return val && equal_tol(*val, expected_val, float_tol);
	};

	switch (known_refsys_id) {
	case KnownGeoRefsysId::sa_lambert94:
		if (!uint16_value_is(GtiffProjKey::proj_linear_units, GtiffProjUint16Value::linear_meter)) {
			return false;
		}
		proj_cs_type = proj.get_uint16_value(GtiffProjKey::projected_cs_type);
		if (proj_cs_type && (proj_cs_type != GtiffProjUint16Value::user_defined) && (proj_cs_type != 0u)) {
			ret = proj_cs_type == 3107u; // EPSG:3107
		}
		else {
			ret = uint16_value_is_missing_user_defined_or_equals(
				          GtiffProjKey::geog_geodetic_datum,
					      GtiffProjUint16Value::datum_geocentric_datum_of_australia_1994);
			ret = uint16_value_is(GtiffProjKey::proj_coord_trans,
					              GtiffProjUint16Value::ct_lambert_conf_conic_2sp) && ret;
			ret = float64_value_is(GtiffProjKey::geog_semi_major_axis, 6378137.00) && ret;
			ret = float64_value_is(GtiffProjKey::geog_inv_flattening, 298.257222101) && ret,
			ret = float64_value_is(GtiffProjKey::proj_std_parallel1, -28.0) && ret;
			ret = float64_value_is(GtiffProjKey::proj_std_parallel2, -36.0) && ret;
			ret = float64_value_is(GtiffProjKey::proj_false_origin_long, 135.0) && ret;
			ret = float64_value_is(GtiffProjKey::proj_false_origin_lat, -32.0) && ret;
			ret = float64_value_is(GtiffProjKey::proj_false_origin_easting, 1000000.0) && ret;
			ret = float64_value_is(GtiffProjKey::proj_false_origin_northing, 2000000.0) && ret;
		}
		break;

	case KnownGeoRefsysId::vicgrid94: 
		if (!uint16_value_is_missing_user_defined_or_equals(GtiffProjKey::proj_linear_units, 
															GtiffProjUint16Value::linear_meter)) {
			return false;
		}
		proj_cs_type = proj.get_uint16_value(GtiffProjKey::projected_cs_type);
		if (proj_cs_type && (proj_cs_type != GtiffProjUint16Value::user_defined) && (proj_cs_type != 0u)) {
			ret = proj_cs_type == 3111u; // EPSG:3111
		} 
		else {
			ret = uint16_value_is_missing_user_defined_or_equals(
				          GtiffProjKey::geog_geodetic_datum,
					      GtiffProjUint16Value::datum_geocentric_datum_of_australia_1994);
			ret = uint16_value_is(GtiffProjKey::proj_coord_trans,
					              GtiffProjUint16Value::ct_lambert_conf_conic_2sp) && ret;
			ret = float64_value_is(GtiffProjKey::geog_semi_major_axis, 6378137.00) && ret;
			ret = float64_value_is(GtiffProjKey::geog_inv_flattening, 298.257222101) && ret,
			ret = float64_value_is(GtiffProjKey::proj_std_parallel1, -36.0) && ret;
			ret = float64_value_is(GtiffProjKey::proj_std_parallel2, -38.0) && ret;
			ret = float64_value_is(GtiffProjKey::proj_false_origin_long, 145.0) && ret;
			ret = float64_value_is(GtiffProjKey::proj_false_origin_lat, -37.0) && ret;
			ret = float64_value_is(GtiffProjKey::proj_false_origin_easting, 2500000.0) && ret;
			ret = float64_value_is(GtiffProjKey::proj_false_origin_northing, 2500000.0) && ret;
		}
		break;

	case KnownGeoRefsysId::nsw_lambert94: 
		if (!uint16_value_is(GtiffProjKey::proj_linear_units, GtiffProjUint16Value::linear_meter)) {
			return false;
		}		
		proj_cs_type = proj.get_uint16_value(GtiffProjKey::projected_cs_type);
		if (proj_cs_type && (proj_cs_type != GtiffProjUint16Value::user_defined) && (proj_cs_type != 0u)) {
			ret = proj_cs_type == 3308u; // EPSG:3308
			// Confusingly, there does seem to be a "Somalia onshore" proj using this EPSG too, 
			// not sure if clashes are possible 
		} 
		else {
			ret = uint16_value_is_missing_user_defined_or_equals(
				          GtiffProjKey::geog_geodetic_datum,
					      GtiffProjUint16Value::datum_geocentric_datum_of_australia_1994);
			ret = uint16_value_is(GtiffProjKey::proj_coord_trans,
					              GtiffProjUint16Value::ct_lambert_conf_conic_2sp) && ret;
			ret = float64_value_is(GtiffProjKey::geog_semi_major_axis, 6378137.00) && ret;
			ret = float64_value_is(GtiffProjKey::geog_inv_flattening, 298.257222101) && ret,
			ret = float64_value_is(GtiffProjKey::proj_std_parallel1, -30.75) && ret;
			ret = float64_value_is(GtiffProjKey::proj_std_parallel2, -35.75) && ret;
			ret = float64_value_is(GtiffProjKey::proj_false_origin_long, 147.0) && ret;
			ret = float64_value_is(GtiffProjKey::proj_false_origin_lat, -33.25) && ret;
			ret = float64_value_is(GtiffProjKey::proj_false_origin_easting, 9300000.0) && ret;
			ret = float64_value_is(GtiffProjKey::proj_false_origin_northing, 4500000.0) && ret;			
		}
		break;

	case KnownGeoRefsysId::mga_zone_55_94:
		if (!uint16_value_is(GtiffProjKey::proj_linear_units, GtiffProjUint16Value::linear_meter)) {
			return false;
		}
		proj_cs_type = proj.get_uint16_value(GtiffProjKey::projected_cs_type);
		if (proj_cs_type && (proj_cs_type != GtiffProjUint16Value::user_defined) && (proj_cs_type != 0u)) {
			ret = proj_cs_type == 28355u; // EPSG:28355
		}
		else {
			ret = uint16_value_is_missing_user_defined_or_equals(
				          GtiffProjKey::geog_geodetic_datum,
					      GtiffProjUint16Value::datum_geocentric_datum_of_australia_1994);
			ret = uint16_value_is(GtiffProjKey::proj_coord_trans,
				                  GtiffProjUint16Value::ct_transverse_mercator) && ret;
			ret = float64_value_is(GtiffProjKey::geog_semi_major_axis, 6378137.00) && ret;
			ret = float64_value_is(GtiffProjKey::geog_inv_flattening, 298.257222101) && ret,
			ret = float64_value_is(GtiffProjKey::proj_scale_at_nat_origin, 0.9996) && ret;
			ret = float64_value_is(GtiffProjKey::proj_false_origin_long, 147.0) && ret;
			ret = float64_value_is(GtiffProjKey::proj_false_origin_lat, 0.0) && ret;
			ret = float64_value_is(GtiffProjKey::proj_false_origin_easting, 500000.0) && ret;
			ret = float64_value_is(GtiffProjKey::proj_false_origin_northing, 10000000.0) && ret;
		}
		break;

	default: 
		TWIST_THROW(L"Invalid geo reference system ID %d.", known_refsys_id);
	}
	return ret;
}

[[nodiscard]] auto check_geotiff(const Tiff& tiff, 
							     KnownGeoRefsysId expected_known_refsys_id, 
							     std::optional<GeoGridDataType> expected_data_type, 
								 std::optional<std::string> expected_nodata_value,
							     std::optional<bool> expected_tiled) 
      -> std::tuple<bool, std::wstring, std::optional<GeoDataGridInfo>>
{
	// Read info about the underlaying geographic grid
	auto geo_grid_info = read_geo_data_grid_info(tiff);
	if (!geo_grid_info) {
		return {false, 
		        std::format(L"TIFF file \"{}\" contains no GeoTIFF coordinates.", tiff.path().filename().c_str()), 
				std::nullopt};
	}

	// Check that the TIFF has the right projection
	const auto proj_info = read_projection_info(tiff);
	if (!proj_info) {
		return {false, 
		        std::format(L"TIFF file \"{}\" does not contain projection information. "
				            L"It is expected to use projection \"{}\".", 
				            tiff.path().filename().c_str(), 
					        as_long_name(expected_known_refsys_id)),
				geo_grid_info};
	}
	if (!match(*proj_info, expected_known_refsys_id)) {
		return {false, 
		        std::format(L"TIFF file \"{}\" uses the wrong projection. " 
							L"It is expected to use projection \"{}\".", 
							tiff.path().filename().c_str(), 
							as_long_name(expected_known_refsys_id)),
				geo_grid_info};
	}

	if (expected_data_type) {
		// Check that the TIFF has the correct pixel type
		if (tiff.sample_data_type() != expected_data_type) {
			return {false, 
					std::format(L"TIFF file \"{}\" uses the wrong data type \"{}\"; expected type \"{}\". ", 
								tiff.path().filename().c_str(),
								to_string(tiff.sample_data_type()), 
								to_string(*expected_data_type)),
					geo_grid_info};
		}
	}

	if (expected_nodata_value) {
		// Check that the TIFF does uses the expected "no-data" value placeholder
		assert(!is_whitespace(*expected_nodata_value));
		if (geo_grid_info->nodata_value() != *expected_nodata_value) {
			return {false, 
					std::format(L"TIFF file \"{}\" uses the wrong \"no-data\" value placeholder \"{}\"; "
					            L"expected value \"{}\". ", 
								tiff.path().filename().c_str(),
								ansi_to_string(*expected_nodata_value), 
								ansi_to_string(geo_grid_info->nodata_value())),
					geo_grid_info};
		}
	}

	if (expected_tiled) {
		// Check that the TIFF is/isn't tiled
		if (tiff.is_tiled() && !(*expected_tiled)) {
			return {false, 
					std::format(L"TIFF file \"{}\" is tiled; it is expected not to be.", 
					            tiff.path().filename().c_str()),
				    geo_grid_info};
		}
		if (!tiff.is_tiled() && *expected_tiled) {
			return {false, 
					std::format(L"TIFF file \"{}\" is not tiled; it is expected to be.", 
					            tiff.path().filename().c_str()),
					geo_grid_info};
		}		
	}

	return {true, L"", *geo_grid_info};
}

auto check_geotiff_and_throw(const Tiff& tiff,  
                             KnownGeoRefsysId expected_known_refsys_id, 
							 std::optional<GeoGridDataType> expected_data_type, 
							 std::optional<std::string> expected_nodata_value,
							 std::optional<bool> expected_tiled) -> GeoDataGridInfo
{
	auto [ok, err, geo_grid_info] = check_geotiff(tiff, 
	                                              expected_known_refsys_id, 
												  expected_data_type, 
												  move(expected_nodata_value),
												  expected_tiled);
	if (!ok) {
		TWIST_VTHRO2(err);
	}
	assert(geo_grid_info);
	return *geo_grid_info;
}

}

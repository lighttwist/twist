/// @file geotiff_gdal_utils.hpp
/// Utilities for working with GeoTIFF files, using the GDAL library as backend

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_GIS_GEOTIFF__GDAL__UTILS_HPP
#define TWIST_GIS_GEOTIFF__GDAL__UTILS_HPP

#include "twist/gis/geotiff_defs.hpp"
#include "twist/gis/gis_geometry.hpp"
#include "twist/gis/SpatialReferenceSystem.hpp"
#include "twist/gis/twist_gdal_utils.hpp"
#include "twist/math/FlatMatrix.hpp"

namespace twist::gis {

/*! Modify the underlying spatial grid of a geotiff (with square cells) by dividing each cell into a number of 
    subcells. 
    \param[in] in_path  Path to the input geotiff
    \param[in] out_path  Path to the output geotiff; if it already exists, an exception is thrown
    \param[in] nof_subcells_per_cell  The number of subcells in a cell; must be a perfect square
    \param[in] resample_algo  The algorithm to be used for resampling
    \param[in] out_data_type  The data type of the output raster values; nullopt means unchanged
    \return  The space grid underlying the output geotiff
 */
auto subdivide_geotiff_cells(const fs::path& in_path,
                             const fs::path& out_path,
                             Ssize nof_subcells_per_cell,
                             RasterResampleAlgorithm resample_algo = 
                                    RasterResampleAlgorithm::mode,
                             std::optional<GeoGridDataType> out_data_type = std::nullopt) -> GeoSpaceGrid;

/*! Given a geotiff and a tile size, this function creates a new geotiff containing the same data, following the 
    "tiled" format.
    \param[in] in_path  Path to the input geotiff
    \param[in] out_path  Path to the output geotiff
    \param[in] nof_cells_per_tile  The number of cells in a tile; the value must be a perfect square and a multiple 
                                   of 256
*/
auto tile_geotiff(const fs::path& in_path, const fs::path& out_path, Ssize nof_cells_per_tile) -> void;

/*! Given a geotiff and a tile size, this function creates a new geotiff containing the same data, following the 
    "tiled" format. Minimum padding is added (if necessary) on the top and to the right of the original space and data 
    grid so that the image width and height are multiples of the tile size (ie the number of cells along the side of a 
    tile), which means the new image is composed only of whole tiles.
	\tparam PixelVal  The type of a pixel value 
    \param[in] in_path  Path to the input geotiff
    \param[in] out_path  Path to the output geotiff
    \param[in] nof_cells_per_tile  The number of cells in a tile; the value must be a perfect square and a multiple 
                                   of 256
    \param[in] out_nodata_val  The "no-data" value placeholder in the output geotiff; the padding will be filled with 
                               this value; any "no-data" values in the input geotiff will be replaced with this value, 
                               even if no padding is added //+TODO: Not yet implemented. DAN 24Oct'24
    \param[in] bigtiff  Whether the output geotiff should be marked as BigTIFF (bigger than 4GB)
    \return  The space grid underlying the output geotiff
*/
template<class PixelVal>
auto pad_and_tile_geotiff(const fs::path& in_path, 
                          const fs::path& out_path, 
                          Ssize nof_cells_per_tile,
                          PixelVal out_nodata_val,
                          bool bigtiff) -> GeoSpaceGrid;

/*! Given a geotiff and a tile size, this function creates a new geotiff containing the same data, following the 
    "tiled" format. Minimum padding is added (if necessary) on the top of the original space and data grid so that the 
    image height is a multiple of the tile size (ie the number of cells along the side of a tile), which means the new 
    image is composed of whole tiles on the vertical.
	\tparam PixelVal  The type of a pixel value 
    \param[in] in_path  Path to the input geotiff
    \param[in] out_path  Path to the output geotiff
    \param[in] nof_cells_per_tile  The number of cells in a tile; the value must be a perfect square and a multiple 
                                   of 256
    \param[in] out_nodata_val  The "no-data" value placeholder in the output geotiff; the padding will be filled with 
                               this value; any "no-data" values in the input geotiff will be replaced with this value, 
                               even if no padding is added //+TODO: Not yet implemented. DAN 24Oct'24
    \param[in] bigtiff  Whether the output geotiff should be marked as BigTIFF (bigger than 4GB)
    \return  The space grid underlying the output geotiff
*/
template<class PixelVal>
auto rowpad_and_tile_geotiff(const fs::path& in_path, 
                             const fs::path& out_path, 
                             Ssize nof_cells_per_tile,
                             PixelVal out_nodata_val,
                             bool bigtiff) -> GeoSpaceGrid;

/*! Write the data stored in a matrix to a new geotiff.
    \note  The implementation is inclomplete.
	\tparam PixelVal  The type of a pixel value
    \param[in] input_data  The input data matrix
	\param[in] path  Geotiff path; if a file with this path already exists, an exception is thrown  
	\param[in] space_grid  Spatial grid underlying the geotiff data; must match the data matrix dimensions  
	\param[in] nodata_val  Geotiff "no-data" value; pass in nullopt for none  
	\param[in] proj_info  Geotiff projection info; pass in nullopt for none  
    \param[in] bigtiff  Whether the output geotiff should be marked as BigTIFF (bigger than 4GB)
 */
template<class PixelVal>
auto write_geotiff_gdal(const twist::math::FlatMatrix<PixelVal>& input_data, 
					    fs::path path,
					    const GeoSpaceGrid& space_grid,
					    std::optional<PixelVal> nodata_val = {},
					    std::optional<SpatialReferenceSystem> proj_info = {},
					    bool bigtiff = false) -> void; //+TODO: Finish implementation. DAN 25Feb2025

//+TODO: Experimental work in progress (this function does not work for bigtiffs). DAN 13Oct2023
auto read_geotiff_block(const fs::path& in_path, Ssize block_x_idx, Ssize block_y_idx) -> std::vector<unsigned char>;

}

#include "twist/gis/geotiff_gdal_utils.ipp"

#endif

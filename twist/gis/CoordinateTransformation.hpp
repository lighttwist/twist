/// @file CoordinateTransformation.hpp
/// CoordinateTransformation class definition

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_GIS_COORDINATE_TRANSFORMATION_HPP
#define TWIST_GIS_COORDINATE_TRANSFORMATION_HPP

#include "twist/gis/gis_geometry.hpp"

namespace twist::gis {
class SpatialReferenceSystem;
}
class OGRCoordinateTransformation;

namespace twist::gis {

/*! This class deals with transforming geographic coordinates from one (source) coordinate system to another 
    (destination) coordinate system and wraps a GDAL OGRCoordinateTransformation object, retrievable from this class as 
	a reference to an incomplete type.
 */
class CoordinateTransformation {
public:
	/*! Constructor.
		\param[in] src_refsys  The source reference system
		\param[in] dest_refsys  The destination reference system
	 */
	explicit CoordinateTransformation(const SpatialReferenceSystem& src_refsys, 
	                                  const SpatialReferenceSystem& dest_refsys);

	CoordinateTransformation(const CoordinateTransformation&) = delete;  

	CoordinateTransformation(CoordinateTransformation&&);  

	~CoordinateTransformation();

	CoordinateTransformation& operator=(const CoordinateTransformation&) = delete;  

	CoordinateTransformation& operator=(CoordinateTransformation&&);

	//! Transform the coordinates of the geographic point \p pt.
	[[nodiscard]] auto transform(const GeoPoint& pt) const -> GeoPoint;

	//! Get a reference to the underlying GDAL object.
	[[nodiscard]] auto get() const -> OGRCoordinateTransformation&;

private:
	class Impl;
	std::unique_ptr<Impl> impl_;

	TWIST_CHECK_INVARIANT_DECL
};

}

#endif

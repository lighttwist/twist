/// @file geotiff_gdal_utils.cpp
/// Implementation file for "geotiff_gdal_utils.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/gis/geotiff_gdal_utils.hpp"

#include "twist/scope.hpp"
#include "twist/gis/internal_gdal_utils.hpp"
#include "twist/math/numeric_utils.hpp"

#include "gdal/include/gdal_priv.h"

using namespace twist::math;

namespace twist::gis {

namespace detail {

auto write_geotiff_gdal_impl(std::function<void*(Ssize)> get_input_data_row, 
					         fs::path path,
					         const GeoSpaceGrid& space_grid,
					         //std::optional<PixelVal> nodata_val, 
					         std::optional<SpatialReferenceSystem> proj_info,
					         bool bigtiff) -> void
{
	auto set_geo_transform = [](gsl::not_null<GDALDataset*> dset,  
	                            double left,
								double bottom,
								double cell_width, 
								double cell_height) {
		auto data = std::vector{left, cell_width, 0., bottom, 0., cell_height};
		dset->SetGeoTransform(data.data());									
	};
	
	auto driver = get_gdal_geotiff_driver();
	auto dset = driver->Create(path.string().c_str(), 
	                           static_cast<int>(space_grid.nof_columns()), 
							   static_cast<int>(space_grid.nof_rows()), 
							   1/*bands*/, 
	                           GDT_Float32, 
							   nullptr/*options*/);
	set_geo_transform(dset, space_grid.left(), space_grid.bottom(), space_grid.cell_width(), space_grid.cell_height());
	if (proj_info) {
		dset->SetSpatialRef(&proj_info->get());
	}

	auto row_buf = reinterpret_cast<float*>(CPLMalloc(sizeof(float) * space_grid.nof_columns()));
    scope (exit) { 
		CPLFree(row_buf); 
	};

	for (auto i : IndexRange{space_grid.nof_rows()}) {
		dset->GetRasterBand(1)->RasterIO(GF_Write, 
										0/*x_off*/, 
										static_cast<int>(i)/*y_off*/, 
										static_cast<int>(space_grid.nof_columns())/*x_size*/, 
										1/*y_size*/, 
										get_input_data_row(i), 
										static_cast<int>(space_grid.nof_columns())/*buf_x_size*/, 
										1/*buf_y_size*/, 
										GDT_Float32, 
										0/*pixel_space*/, 
										0/*line_space*/,
										nullptr/*extra_arg*/);
	}

	gdal_close(dset);

	//+TODO: Finish implementation. 
	bigtiff;
}

}

auto subdivide_geotiff_cells(const fs::path& in_path, 
                             const fs::path& out_path, 
                             Ssize nof_subcells_per_cell,
                             RasterResampleAlgorithm resample_algo,
					         std::optional<GeoGridDataType> out_data_type) -> GeoSpaceGrid
{
    if (nof_subcells_per_cell <= 0 || !is_perfect_square(nof_subcells_per_cell)) {
        TWIST_THRO2(L"The number of subcells per cell {} must be a non-zero perfect square.", 
                    nof_subcells_per_cell);
    }
    auto nof_subcells_per_cell_side = perfect_square_root(nof_subcells_per_cell);

    auto in_grid_info = read_geo_data_grid_info(in_path);
    if (!in_grid_info) {
        TWIST_THRO2(L"Input file \"{}\" is not a geotiff.", in_path.c_str());
    }
    const auto& in_space_grid = in_grid_info->space_grid();
	const auto space_grid_cell_size = in_space_grid.cell_width();
	if (in_space_grid.cell_height()!= space_grid_cell_size) {
		TWIST_THRO2(L"This function does not (yet) support geotiffs whose underlying geographic grid "
                    L"has unsquare cells.");
	}

    auto out_space_grid = GeoSpaceGrid{in_space_grid.left(),
                                       in_space_grid.bottom(),
                                       space_grid_cell_size / nof_subcells_per_cell_side,
                                       space_grid_cell_size / nof_subcells_per_cell_side,
                                       in_space_grid.nof_rows() * nof_subcells_per_cell_side,
                                       in_space_grid.nof_columns() * nof_subcells_per_cell_side};
    
    translate_raster(in_path, out_path, RasterFileFormat::geotiff, out_space_grid, out_data_type, resample_algo); 

    return out_space_grid;
}

auto tile_geotiff(const fs::path& in_path, const fs::path& out_path, Ssize nof_cells_per_tile) -> void
{
    translate_raster(in_path, 
                     out_path, 
                     RasterFileFormat::geotiff, 
                     std::nullopt/*out_space_grid*/,
                     std::nullopt/*out_data_type*/, 
                     RasterResampleAlgorithm::mode,
                     std::nullopt/*value_rescale*/,
                     Ssize{nof_cells_per_tile});
}

auto read_geotiff_block(const fs::path& in_path, Ssize /*block_x_idx*/, Ssize /*block_y_idx*/) 
      -> std::vector<unsigned char>
{
    //+TODO: Finish implementation. If we really need this one?

    auto dset = gdal_open(in_path, true/*read_only*/);

    auto nof_bands = dset->GetRasterCount();
    if (nof_bands != 1) {
        TWIST_THRO2(L"Geotiff \"{}\" has multiple bands.", in_path.filename().c_str());
    }
    auto poBand = dset->GetRasterBand(1);

//    memset( panHistogram, 0, sizeof(GUIntBig) * 256 );
 
    CPLAssert( poBand->GetRasterDataType() == GDT_Byte );
 
    int nXBlockSize, nYBlockSize;
 
    poBand->GetBlockSize( &nXBlockSize, &nYBlockSize );
    int nXBlocks = (poBand->GetXSize() + nXBlockSize - 1) / nXBlockSize;
    int nYBlocks = (poBand->GetYSize() + nYBlockSize - 1) / nYBlockSize;
 
    GByte *pabyData = (GByte *) CPLMalloc(nXBlockSize * nYBlockSize);
    scope (exit) { CPLFree(pabyData); };
 
    for( int iYBlock = 0; iYBlock < nYBlocks; iYBlock++ )
    {
        for( int iXBlock = 0; iXBlock < nXBlocks; iXBlock++ )
        {
            int        nXValid, nYValid;
 
            poBand->ReadBlock( iXBlock, iYBlock, pabyData );
 
            // Compute the portion of the block that is valid
            // for partial edge blocks.
            poBand->GetActualBlockSize(iXBlock, iYBlock, &nXValid, &nYValid);
  
            // Collect the histogram counts.
            for( int iY = 0; iY < nYValid; iY++ )
            {
                for( int iX = 0; iX < nXValid; iX++ )
                {
                    // panHistogram[pabyData[iX + iY * nXBlockSize]] += 1;
                }
            }
        }
    }    
    return {};
}

}

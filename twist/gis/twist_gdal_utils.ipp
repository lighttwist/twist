/// @file twist_gdal_utils.ipp
/// Inline implementation file for "twist_gdal_utils.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace twist::gis {

// --- RasterValueRescale class ---

template<class SrcPixelVal, class DestPixelVal>
requires twist::is_number<SrcPixelVal> && twist::is_number<DestPixelVal>
RasterValueRescale::RasterValueRescale(twist::math::ClosedInterval<SrcPixelVal> in_range, 
	                             twist::math::ClosedInterval<DestPixelVal> out_range)
	: in_lo_{std::to_string(in_range.lo())}
	, in_hi_{std::to_string(in_range.hi())}
	, out_lo_{std::to_string(out_range.lo())}
	, out_hi_{std::to_string(out_range.hi())}
{
	TWIST_CHECK_INVARIANT
}

template<class SrcPixelVal>
requires twist::is_number<SrcPixelVal>
auto RasterValueRescale::to_nan() -> RasterValueRescale
{
	auto ret = RasterValueRescale{};
	ret.in_lo_ = std::to_string(std::numeric_limits<SrcPixelVal>::lowest()); 
    ret.in_hi_ = std::to_string(std::numeric_limits<SrcPixelVal>::max());
	ret.out_lo_ = "nan";
	ret.out_hi_ = "nan";
	return ret;
}

// --- Free functions ---

template<class NodataVal>
[[nodiscard]] auto get_single_band_nodata_value(const RasterDatasetInfo& raster_info) -> std::optional<NodataVal>
{
    if (size(raster_info.bands_info) != 1) {
		TWIST_THRO2(L"The raster is not single-band.");
    }
    auto nodata_val = std::optional<uint8_t>{};
    if (const auto nodata_val_dbl = raster_info.bands_info.begin()->second.no_data_value) {
        nodata_val = static_cast<uint8_t>(*nodata_val_dbl);
    }
    return nodata_val;
}

}

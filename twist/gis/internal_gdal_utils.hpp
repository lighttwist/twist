/// @file internal_gdal_utils.hpp
/// Utilities, internal to this library, for working with GDAL (Geospatial Data Abstraction Library) 

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_GIS_INTERNAL__GDAL__UTILS_HPP
#define TWIST_GIS_INTERNAL__GDAL__UTILS_HPP

#include "twist/gis/gis_globals.hpp"
#include "twist/gis/twist_gdal_utils.hpp"

#include "gdal/include/gdal.h"

#define CHECK_OGRERR_EX(err_no, err_descr)  if (err_no != OGRERR_NONE)  \
				TWIST_THROW(L"%s: error code %d (%s).", err_descr, err_no, ogr_err_name(err_no).c_str());

#define CHECK_OGRERR(err_no)  if (err_no != OGRERR_NONE)  \
				TWIST_THROW(L"OGR error occurres: error code %d (%s).", err_no, ogr_err_name(err_no).c_str());

class GDALDataset;
class GDALDriver;
class OGRLinearRing;
class OGRLineString;
class OGRPoint;
class OGRPolygon;

namespace twist::gis {

//+TODO: Add comments

[[nodiscard]] auto get_gdal_geotiff_driver() -> gsl::not_null<GDALDriver*>;

[[nodiscard]] auto get_gdal_shapefile_driver() -> gsl::not_null<GDALDriver*>;

[[nodiscard]] auto gdal_open(const fs::path& path, bool read_only) -> gsl::not_null<GDALDataset*>;

/*! Close GDAL dataset \p dset.
    For non-shared datasets the dataset object is destoroyed, recovering all dataset related resources. 
	For shared datasets the dataset is dereferenced, and closed only if the referenced count has dropped below 1.
*/
auto gdal_close(gsl::not_null<GDALDataset*> dset) -> void;

/*! Close GDAL dataset with handle \p dset_hnd.
    For non-shared datasets the dataset object is destoroyed, recovering all dataset related resources. 
	For shared datasets the dataset is dereferenced, and closed only if the referenced count has dropped below 1.
	\note  If \p dset_hnd is nullptr, nothing happens.
*/
auto gdal_close(GDALDatasetH dset_hnd) -> void;

[[nodiscard]] constexpr auto to_gdal(FeatureFieldType type) -> OGRFieldType;

[[nodiscard]] constexpr auto to_gdal(VectorGeometryType geom_type) -> OGRwkbGeometryType;

[[nodiscard]] auto to_gdal(GeoPoint point) -> std::unique_ptr<OGRPoint>;

[[nodiscard]] auto to_gdal(const GeoLineString& line_string) -> std::unique_ptr<OGRLineString>;

[[nodiscard]] auto to_gdal(const GeoLinearRing& linear_ring) -> std::unique_ptr<OGRLinearRing>;

[[nodiscard]] auto from_gdal(gsl::not_null<const OGRLinearRing*> linear_ring) -> GeoLinearRing;

[[nodiscard]] auto to_gdal(const GeoPolygon& polygon) -> std::unique_ptr<OGRPolygon>;

[[nodiscard]] auto ogr_err_name(int err_no) -> std::wstring;

}

#include "twist/gis/internal_gdal_utils.ipp"

#endif  

/// @file gis_geometry.hpp
/// Geometry classes for the "gdal" library 

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_GIS__GEOMETRY_HPP
#define TWIST_GIS__GEOMETRY_HPP

#include "twist/gis/gis_globals.hpp"
#include "twist/math/geometry.hpp"
#include "twist/math/space_grid.hpp"

namespace twist::gis {

//! Tag type for spheroidal, decimal degree coordinates (longitude/latitude)
class DecDegreeCoordTag {};  

//! Tag type for flat, metre geographic coordinates (x/y)
class MetreCoordTag {};  

//! Geographic coordinate types.
enum class GisCoordType {
	none = 0,
	dec_degree = 1, ///< Spheroidal, decimal degree coordinates (longitude/latitude)
	metre = 2 ///< Projected/flat, metre coordinates (x/y)
};

//! A geographic point (ie a point on a map) in any coordinates.
using GeoPoint = twist::math::Point<double>;

//! A geographic vector (ie a vector on a map) in any coordinates.
using GeoVector = twist::math::Vector<double>;

//! A geographic point (ie a point on a map) in projected metre coordinates.  
class GeoPointMetre : public GeoPoint {
public:
	using CoordKind = MetreCoordTag;

	GeoPointMetre(double x, double y);
};

/*! A geographic point (ie a point on a map) in decimal degrees (longitude and latitude, in a geographic coordinate 
    system).  
 */
class GeoPointDecDegree : public GeoPoint {
public:
	using CoordKind = DecDegreeCoordTag;
	using GeoPoint::Coord;
	using GeoPoint::GeoPoint;

	GeoPointDecDegree(double longitude, double latitude);

	double longitude() const;

	double latitude() const;
};

//! A two-dimensional geographic size, specifying a width and height, both positive.
class GeoSize {
public:
	GeoSize();

	GeoSize(double width, double height);

	double width() const; 
	
	double height() const;

	bool is_zero() const;

private:
	std::tuple<bool, std::wstring> validate_data() const;

	double width_;
	double height_;

	TWIST_CHECK_INVARIANT_DECL
};

/// A geographic rectangle (ie a rectangle on a map) in any coordinates
using GeoRectangle = twist::math::Rectangle<double>;

//! A geographic rectangle (ie a rectangle on a map) in metre (flat) coordinates.  
class GeoRectangleMetre : public GeoRectangle {
public:
	using GeoRectangle::GeoRectangle;

	using CoordKind = MetreCoordTag;
};

//! A longitude-latitude quadrangle, that is a quadrangle on a spheroid, bounded by two meridians and two parallels. 
class GeoLonLatQuadrangle : public GeoRectangle {
public:
	using CoordKind = DecDegreeCoordTag;

	/*! Constructor.
	    \param[in] lon1  The longitude (X ccordinate) of one of the bounding meridians
	    \param[in] lat1  The latitude (Y coordinate) of one of the bounding parallels
	    \param[in] lon2  The longitude (X coordinate) of the other bounding meridian
	    \param[in] lat2  The latitude (Y coordinate) of the other bounding parallel
	 */
	GeoLonLatQuadrangle(double lon1, double lat1, double lon2, double lat2);

	//! The longitude (X coordinate) of the bounding meridian on the left.
	double left_longitude() const;

	//! The longitude (X coordinate) of the bounding meridian on the right.
	double right_longitude() const;

	//! The latitude (Y coordinate) of the bounding parallel at the bottom.
	double bottom_latitude() const;

	//! The latitude (Y coordinate) of the bounding parallel at the top.
	double top_latitude() const;

	//! The rangle of longitudes (X coordinates) specified by the bounding meridians.
	twist::math::ClosedInterval<double> longitude_range() const;

	//! The rangle of latitudes (Y coordinates) specified by the bounding parallels.
	twist::math::ClosedInterval<double> latitude_range() const;
};

//! A geographic grid (ie a grid on a map) with rectangular (including square) cells, in any coordinates.
using GeoSpaceGrid = twist::math::SpaceGrid<double>;

//! A geographic grid (ie a grid on a map) with square cells, in any coordinates.
using SquareGeoSpaceGrid = twist::math::SquareSpaceGrid<double>;

//! A "line string" (a line connecting a series of points on a map) in any coordinates. 
class GeoLineString {
public:
	/*! Constructor.
		\param[in] points  The points connected by the line string; if there are less than two points in the list, 
		                   an exception is thrown
	 */
	explicit GeoLineString(std::vector<GeoPoint> points);

	//! The number of points connected by the line string.
	[[nodiscard]] auto nof_points() const -> Ssize;

	//! Get the point with index \p idx in the list of points connected by the line string.
	[[nodiscard]] auto point(Ssize idx) const -> GeoPoint;

	//! The points connected by the line string.
	[[nodiscard]] auto points() const -> const std::vector<GeoPoint>&;

private:
	std::vector<GeoPoint> points_;

	TWIST_CHECK_INVARIANT_DECL
};

/*! A "linear ring" (a line connecting a series of points on a map, with the last point being the same as the first) in
    any coordinates. 
 */
class GeoLinearRing : public GeoLineString {
public:
	/*! Constructor.
		\param[in] points  The points connected by the linear ring; if the last point does not match the first, or if 
		                   there are less than three points in the list, an exception is thrown
	 */
	explicit GeoLinearRing(std::vector<GeoPoint> points);
};

//! A "polygon" in any map coordinates. A polygon consists of one outer ring and zero or more inner rings (the holes).
class GeoPolygon {
public:
	/*! Constructor.
		\param[in] outer_ring  The outer ring
		\param[in] inner_rings  List of inner rings (if any); the class does not check whether they are valid (ie 
		                        whether they fall within the outer ring and do not intersect each-other)
	 */
	explicit GeoPolygon(GeoLinearRing outer_ring, std::vector<GeoLinearRing> inner_rings = {});

	//! The outer ring.
	[[nodiscard]] auto outer_ring() const -> const GeoLinearRing&;

	//! List of inner rings, if any.
	[[nodiscard]] auto inner_rings() const -> const std::vector<GeoLinearRing>&;

    //! Whether the polygon is "holey", ie has any inner rings.
    [[nodiscard]] auto is_holey() const -> bool;

private:
	GeoLinearRing outer_ring_;
	std::vector<GeoLinearRing> inner_rings_;
};

// --- Free functions ---

/// Create a GeoPointMetre object from a geometric Point<> object.
///
/// @tparam  The coordinate type for the input point
/// @param[in] pt  The input, geometric point  
/// @return  The output, geographic point
///
template<class T> 
GeoPointMetre to_geo_point_metre(twist::math::Point<T> pt);

/// Create a GeoPointDecDegree object from a geometric Point<> object.
///
/// @tparam  The coordinate type for the input point
/// @param[in] pt  The input, geometric point  
/// @return  The output, geographic point
///
template<class T> 
GeoPointDecDegree to_geo_point_dec_degree(twist::math::Point<T> pt);

/// Create a GeoRectangleMetre object from a geometric Rectangle<> object.
///
/// @tparam  The coordinate type for the input rectangle
/// @param[in] rect  The input, geometric rectangle  
/// @return  The output, geographic rectangle in metre (flat) coordinates
///
template<class T>
GeoRectangleMetre to_geo_rect_metre(const twist::math::Rectangle<T>& rect);

//! The top-left point of a quadrangle in spheroidal coordinates.
[[nodiscard]] auto top_left(const GeoLonLatQuadrangle& quad) -> GeoPointDecDegree;

//! The top-right point of a quadrangle in spheroidal coordinates.
[[nodiscard]] auto top_right(const GeoLonLatQuadrangle& quad) -> GeoPointDecDegree;

//! The bottom-left point of a quadrangle in spheroidal coordinates.
[[nodiscard]] auto bottom_left(const GeoLonLatQuadrangle& quad) -> GeoPointDecDegree;

//! The bottom-right point of a quadrangle in spheroidal coordinates.
[[nodiscard]] auto bottom_right(const GeoLonLatQuadrangle& quad) -> GeoPointDecDegree;

/*! Expand (or shrink) a longitude/latitude quadrangle by a margin in each direction.
    \param[in] quad  The original quadrangle
    \param[in] dlon  The distance by which the quadrangle should be expanded horizontally (longitude decimal degrees); 
	                 pass in a nagative number to deflate (shrink) the quadrangle
    \param[in] dlat  The distance by which the quadrangle should be expanded certically (latitude decimal degrees); 
	                 pass in a nagative number to deflate (shrink) the quadrangle
    \return  The expanded (or shrunk) quadrangle
 */
[[nodiscard]] auto inflate(const GeoLonLatQuadrangle& quad, double dlon, double dlat) -> GeoLonLatQuadrangle;

//! Calculate the bounding rectangle of the line string \p line_string.
[[nodiscard]] auto calc_bounding_rect(const GeoLineString& line_string) -> GeoRectangle;

//! Calculate the bounding rectangle of the polygon \p poly.
[[nodiscard]] auto calc_bounding_rect(const GeoPolygon& poly) -> GeoRectangle;

}

#include "twist/gis/gis_geometry.ipp"

#endif

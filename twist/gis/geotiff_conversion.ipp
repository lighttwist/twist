/// @file geotiff_conversion.ipp
/// Inline implementation file for "geotiff_conversion.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/db/BinmatFile.hpp"
#include "twist/gis/geotiff_utils.hpp"
#include "twist/gis/GeoDataGridInfo.hpp"
#include "twist/img/Tiff.hpp"
#include "twist/img/tiff_utils.hpp"

namespace twist::gis {

// --- GeotiffConverter class ---

template<class TiffVal>
GeotiffConverter<TiffVal>::GeotiffConverter(const fs::path& geotiff_path)
	: tiff_{twist::img::Tiff::open_read, geotiff_path}
{
	geotiff_info_ = read_geo_data_grid_info(tiff_);
	if (!geotiff_info_) {
		TWIST_THRO2(L"\"{}\" is not a geotiff.", geotiff_path.filename().c_str());
	}
	TWIST_CHECK_INVARIANT
}

template<class TiffVal>
auto GeotiffConverter<TiffVal>::geotiff_space_grid() const -> GeoSpaceGrid
{
	TWIST_CHECK_INVARIANT
	return geotiff_info_->space_grid();
}

template<class TiffVal>
template<class BinmatVal, 
         InvocableR<BinmatVal, TiffVal> Transform> 
auto GeotiffConverter<TiffVal>::convert_to_binmat(const fs::path& binmat_path,
                                                  std::optional<GeoRectangle> zone_to_convert,
                                                  std::string nodata_val_override,
                                                  Transform transform,
                                                  double zone_snap_tol) const -> GeoDataGridInfo
{
	TWIST_CHECK_INVARIANT
	assert(zone_snap_tol >= 0);

	const auto geo_space_grid = geotiff_info_->space_grid();

	auto subgrid_to_convert_info = std::optional<twist::SubgridInfo>{};
	if (zone_to_convert) {
		subgrid_to_convert_info = get_info_of_subgrid_containing_rect(geo_space_grid, 
		                                                              *zone_to_convert, 
		                                                              zone_snap_tol);
		if (!subgrid_to_convert_info) {
			TWIST_THRO2(L"The zone to convert is not contained inside the extent of geotiff '{}'.", 
					    tiff_.path().filename().c_str());
		}
	}

	// Figure out the "no-data" value and its override (if any); if an override is specified but there is no
	// "no-data" value specified for the geotiff, then ignore the override
	auto nodata_val = std::optional<BinmatVal>{};
	auto nodata_val_overrd = std::optional<BinmatVal>{};
	auto new_nodata_val_str = std::string{};
	if (!geotiff_info_->nodata_value().empty()) {
		nodata_val = to_number<BinmatVal>(geotiff_info_->nodata_value());
		if (!nodata_val_override.empty()) {
			nodata_val_overrd = to_number<BinmatVal>(nodata_val_override);
			new_nodata_val_str = nodata_val_override;
		}
		else {
			nodata_val_overrd = nodata_val; // Simply set the override value to the "no-data" value
			new_nodata_val_str = geotiff_info_->nodata_value();
		}
	}
	
	const auto row_cnt = subgrid_to_convert_info ? nof_rows(*subgrid_to_convert_info) 
	                                             : geo_space_grid.nof_rows();	
	const auto col_cnt = subgrid_to_convert_info ? nof_columns(*subgrid_to_convert_info) 
	                                             : geo_space_grid.nof_columns();	

	auto binmat_file = twist::db::BinmatFile<BinmatVal>::create_for_single_matrix(binmat_path, 
	                                                                              row_cnt, 
																				  col_cnt, 
																				  new_nodata_val_str);
	auto do_select_and_apply_conversion = [&](auto transf) {
		return apply_conversion(*binmat_file, subgrid_to_convert_info, new_nodata_val_str, std::make_optional(transf));
	};

	if (nodata_val) {
		assert(nodata_val_overrd);
		return do_select_and_apply_conversion([=](auto tiffval) {
			if (static_cast<BinmatVal>(tiffval) == *nodata_val) {
				// We hit the "no-data" value; do not apply the transformation, simply return the "no-data" 
				// value (or its override, whose value here is the original "no-data" value if an override was 
				// not specified)
				return *nodata_val_overrd;
			}
			return std::invoke(transform, tiffval);
		});
	}

	return do_select_and_apply_conversion([=](auto tiffval) {
		return std::invoke(transform, tiffval);
	});		
}

template<class TiffVal>
template<class BinmatVal, 
         InvocableR<BinmatVal, const SquareGeoSpaceGrid&, const twist::math::FlatMatrix<TiffVal>&,
                               const SquareGeoSpaceGrid&, Ssize, Ssize> Transform>
auto GeotiffConverter<TiffVal>::resample_and_convert_to_binmat(const SquareGeoSpaceGrid& binmat_space_grid,
                                                               std::string binmat_nodata_val,
                                                               fs::path binmat_path,
                                                               Transform transform) -> GeoDataGridInfo
{
    const auto geotiff_space_grid = to_square_grid(geotiff_info_->space_grid());

    // Read the geotiff data in memory, from the cells which intersect the binmat perimeter rectangle 
    const auto binmat_rect = perimeter_rect(binmat_space_grid);
	if (!contains(perimeter_rect(geotiff_space_grid), binmat_rect)) {
		TWIST_THRO2(L"The extent of the geotiff \"{}\" does not cover the binmat space grid.", 
				    tiff_.path().filename().c_str());
	}
    const auto tiff_subgrid_info = get_info_of_subgrid_containing_rect(geotiff_space_grid, binmat_rect);
    assert(tiff_subgrid_info);
    const auto tiff_space_subgrid = get_subgrid(geotiff_space_grid, *tiff_subgrid_info);
    const auto tiff_subgrid_data = tiff_.read_scanlined<TiffVal>(tiff_subgrid_info);

    // Create a data grid for the binmat
    auto binmat_data = twist::math::FlatMatrix<BinmatVal>{binmat_space_grid.nof_rows(), 
                                                          binmat_space_grid.nof_columns()};


    // Transform values from the geotiff data to the binmat data
    for (auto i : IndexRange{binmat_space_grid.nof_rows()}) {
        for (auto j : IndexRange{binmat_space_grid.nof_columns()}) {
            binmat_data(i, j) = std::invoke(transform, tiff_space_subgrid, tiff_subgrid_data, binmat_space_grid, i, j);
        }
    }

    // Write data to binmat
	auto binmat_file = twist::db::BinmatFile<BinmatVal>::create_for_single_matrix(binmat_path, 
	                                                                              binmat_space_grid.nof_rows(), 
																				  binmat_space_grid.nof_columns(), 
																				  binmat_nodata_val);    
    binmat_file->write_whole_data_to_whole_file(binmat_data);

    return GeoDataGridInfo{binmat_space_grid, binmat_nodata_val, enum_for_geo_grid_data_type<BinmatVal>};
}

template<class TiffVal>
template<class BinmatVal, 
         InvocableR<BinmatVal, TiffVal> Transform> 
auto GeotiffConverter<TiffVal>::apply_conversion(twist::db::BinmatFile<BinmatVal>& binmat_file,
                                                 std::optional<twist::SubgridInfo> subgrid_to_convert_info,
                                                 std::string nodata_val,
                                                 std::optional<Transform> transform) const -> GeoDataGridInfo
{
	TWIST_CHECK_INVARIANT
	auto space_grid = geotiff_info_->space_grid();

	if (subgrid_to_convert_info) {
		if (binmat_file.nof_rows() != nof_rows(*subgrid_to_convert_info) || 
				binmat_file.nof_columns() != nof_columns(*subgrid_to_convert_info)) {
			TWIST_THRO2(L"The dimensions of the matrix to write to do not match the subgrid dimensions.");
		}
		space_grid = get_subgrid(space_grid, *subgrid_to_convert_info);	
	}

    auto data = twist::img::Tiff::DataGrid<TiffVal>{};
    if (subgrid_to_convert_info) {
        //+TODO: Currently there are no faclities in twistlib to read only a subgrid of a tiled TIFF
	    data = tiff_.read_scanlined<TiffVal>(subgrid_to_convert_info);
    }
    else {
        data = twist::img::read_pixel_data<TiffVal>(tiff_);
    }
	binmat_file.write_whole_data_to_whole_file(std::move(data), transform);

	return GeoDataGridInfo{space_grid, move(nodata_val), enum_for_geo_grid_data_type<BinmatVal>}; 
}

#ifdef _DEBUG
template<class TiffVal>
void GeotiffConverter<TiffVal>::check_invariant() const noexcept
{
	assert(geotiff_info_);
}
#endif

// --- Free functions ---

template<class BinmatVal, 
         class TiffVal,
		 InvocableR<TiffVal, BinmatVal> Transform>
auto binmat_to_geotiff(const twist::db::BinmatFile<BinmatVal>& binmat, 
                       fs::path geotiff_path,
					   const SquareGeoSpaceGrid& geotiff_space_grid,
					   std::optional<TiffVal> geotiff_nodata_val,
					   std::optional<GeotiffProjection> geotiff_proj_info,
					   Transform transform,
					   bool bigtiff) -> void
{
	auto create_geotiff = [geotiff_path = std::move(geotiff_path),
						   &geotiff_space_grid,
						   &geotiff_nodata_val,
						   geotiff_proj_info = move(geotiff_proj_info),
						   bigtiff](const auto& geotiff_data) mutable {
		write_geotiff(geotiff_data, 
		              std::move(geotiff_path), 
					  geotiff_space_grid, 
					  geotiff_nodata_val, 
					  move(geotiff_proj_info), 
					  bigtiff);
	};

	auto binmat_data = binmat.read_whole_to_single_matrix();

	if constexpr (std::is_same_v<Transform, std::identity>) {
		static_assert(std::is_same_v<BinmatVal, TiffVal>);
		create_geotiff(binmat_data); 
	}
	else if constexpr (std::is_same_v<BinmatVal, TiffVal>) {
		// Transform the data in place
		rg::transform(binmat_data, binmat_data.begin(), transform);
		create_geotiff(binmat_data); 
	} 
	else {
		// Transform the data into a new container
		auto geotiff_data = reserve_vector<TiffVal>(binmat_data.size());
		rg::transform(binmat_data, back_inserter(geotiff_data), transform);
		create_geotiff(geotiff_data); 	
	}
}

}

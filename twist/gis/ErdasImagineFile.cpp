/// @file ErdasImagineFile.cpp
/// Implementation file for "ErdasImagineFile.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

//  https://www.loc.gov/preservation/digital/formats/fdd/fdd000420.shtml
//  ftp://ftp.ecn.purdue.edu/jshan/86/help/html/appendices/erdas_imagine__img_files.htm
//  https://worldwind31.arc.nasa.gov/svn/trunk/GDAL/GDAL-1.7.2/frmts/hfa/hfaopen.cpp

#include "twist/twist_maker.hpp"

#include "twist/gis/ErdasImagineFile.hpp"

#include "twist/gis/twist_gdal_utils.hpp"

#include "gdal/include/frmts/hfa/hfa.h"

#include "twist/string_utils.hpp"
#include "twist/math/numeric_utils.hpp"

using namespace twist::gis;
using namespace twist::math;

#define  TWIST_CHECK_CPL_RESULT(result)  \
			if (result != CPLErr::CE_None)  \
				TWIST_THROW(L"GDAL error: \"%s\"", get_last_cpl_error_msg().c_str());

#define TWIST_THROW_ERDAS(msg_format, ...)  \
			TWIST_THROW(L"Error in ERDAS Imagine file \"%s\": %s",  \
					path_.filename().c_str(), format_str(msg_format, __VA_ARGS__).c_str())

namespace twist::gis {

static const Ssize  k_def_tile_width = 64;  // pixels
static const Ssize  k_def_tile_height = 64;  // pixels

// --- Local functions ---

GeoGridDataType to_geo_grid_data_type(EPTType data_type)
{
	switch (data_type) {
	case EPT_u1   : TWIST_THROW(L"Unsupported data type \"EPT_u1\".");
	case EPT_u2   : TWIST_THROW(L"Unsupported data type \"EPT_u2\".");
	case EPT_u4   : TWIST_THROW(L"Unsupported data type \"EPT_u4\".");
	case EPT_s8   : return GeoGridDataType::int8;
	case EPT_u8   : return GeoGridDataType::uint8;
	case EPT_s16  : return GeoGridDataType::int16;
	case EPT_u16  : return GeoGridDataType::uint16;
	case EPT_s32  : return GeoGridDataType::int32;
	case EPT_u32  : return GeoGridDataType::uint32;
	case EPT_f32  : return GeoGridDataType::float32;
	case EPT_f64  : return GeoGridDataType::float64;		
	case EPT_c64  : TWIST_THROW(L"Unsupported data type \"EPT_c64\".");
	case EPT_c128 : TWIST_THROW(L"Unsupported data type \"EPT_c128\".");
	default : TWIST_THROW(L"Unrecognized data type ID \"%d\".", data_type);
	}
}


EPTType to_img_data_type(GeoGridDataType data_type)
{
	switch (data_type) {
	case GeoGridDataType::int8    : return EPT_s8;
	case GeoGridDataType::uint8   : return EPT_u8;
	case GeoGridDataType::int16   : return EPT_s16;
	case GeoGridDataType::uint16  : return EPT_u16;
	case GeoGridDataType::int32   : return EPT_s32;
	case GeoGridDataType::uint32  : return EPT_u32;
	case GeoGridDataType::float32 : return EPT_f32;
	case GeoGridDataType::float64 : return EPT_f64;		
	default : TWIST_THROW(L"Unrecognized data type ID \"%d\".", data_type);
	}
}


ErdasImagineFile::MapInfo::Unit ansi_to_unit(const std::string& str)
{
	if (str.compare("meters") == 0) {
		return ErdasImagineFile::MapInfo::Unit::metre;
	}
	TWIST_THROW(L"Unsupported unit string \"%s\".", ansi_to_string(str).c_str());
}


std::string unit_to_ansi(ErdasImagineFile::MapInfo::Unit unit)
{
	if (unit == ErdasImagineFile::MapInfo::Unit::metre) {
		return "meters";
	}
	TWIST_THROW(L"Unsupported unit ID %d.", unit);
}

// --- ErdasImagineFile::Handle class ---

class ErdasImagineFile::Handle {
public:
	Handle(HFAHandle hnd) : hnd_(hnd) {}

	~Handle() { if (hnd_ != nullptr) HFAClose(hnd_); }

	HFAHandle get() const { return hnd_; }

	bool empty() const { return hnd_ == nullptr; }

private:
	const HFAHandle hnd_;
};

// --- ErdasImagineFile class ---

ErdasImagineFile::ErdasImagineFile(CreateMode, fs::path path, Ssize width, Ssize height, GeoGridDataType data_type)
	: path_{std::move(path)} 
	, width_{width}
	, height_{height}
	, nof_bands_{1}
	, data_type_{data_type}
	, data_type_bits_{bits_per_data_value(data_type)}
	, tile_width_{k_def_tile_width}  
	, tile_height_{k_def_tile_height}  
{
	handle_ = std::make_unique<Handle>(
			HFACreate(path_.string().c_str(), static_cast<int>(width), static_cast<int>(height), 
					  1/*bands*/, to_img_data_type(data_type), nullptr/*options*/));
	if (handle_->empty()) {
		TWIST_THROW(L"Error creating \"ERDAS Imagine\" file at \"%s\": %s", 
				    path_.filename().c_str(), get_last_cpl_error_msg().c_str());
	}
	
	conclude_init();
	TWIST_CHECK_INVARIANT
}

ErdasImagineFile::ErdasImagineFile(OpenMode, fs::path path)
	: path_{std::move(path)} 
{
	handle_ = std::make_unique<Handle>(HFAOpen(path_.string().c_str(), "r"));
	if (handle_->empty()) {
		TWIST_THROW(L"\"ERDAS Imagine\" file open error: %s", get_last_cpl_error_msg().c_str());
	}
	int width{0};
	int height{0};
	TWIST_CHECK_CPL_RESULT(HFAGetRasterInfo(handle_->get(), &width, &height, &nof_bands_));

	width_ = width;
	height_ = height;

	auto data_type = EPTType::EPT_MIN;
	int compression_type = 0;
	TWIST_CHECK_CPL_RESULT(HFAGetBandInfo(handle_->get(), 1, &data_type, &width, &height, &compression_type));

	tile_width_ = width;
	tile_height_ = height;
	data_type_ = to_geo_grid_data_type(data_type);
	data_type_bits_ = HFAGetDataTypeBits(data_type);

    if ((tile_width_ % 16) != 0 || (tile_height_ % 16) != 0) {
        TWIST_THROW_ERDAS(L"Tile size %dx%d is not multiple of 16x16.", tile_width_, tile_height_);
	}

	conclude_init();
	TWIST_CHECK_INVARIANT
}

ErdasImagineFile::~ErdasImagineFile()
{
	TWIST_CHECK_INVARIANT
}

auto ErdasImagineFile::path() -> fs::path
{
	TWIST_CHECK_INVARIANT
	return path_;
}

auto ErdasImagineFile::width() const -> Ssize
{
	TWIST_CHECK_INVARIANT
	return width_;
}

auto ErdasImagineFile::height() const -> Ssize
{
	TWIST_CHECK_INVARIANT
	return height_;
}

auto ErdasImagineFile::nof_bands() const -> int
{
	TWIST_CHECK_INVARIANT
	return nof_bands_;
}

auto ErdasImagineFile::data_type() const -> GeoGridDataType
{
	TWIST_CHECK_INVARIANT
	return data_type_;
}

auto ErdasImagineFile::nodata_value(int band) const -> std::optional<double>
{
	TWIST_CHECK_INVARIANT
	if (band > nof_bands_) {
		TWIST_THROW_ERDAS(L"Invalid band index %d.", band);
	}
	auto nodata_val = 0.0;
	if (HFAGetBandNoData(handle_->get(), band, &nodata_val) != FALSE) {
		return nodata_val;
	}
	return std::nullopt;
}

auto ErdasImagineFile::read_map_info() const -> std::optional<MapInfo>
{
	TWIST_CHECK_INVARIANT
	const auto* hfa_map_info = HFAGetMapInfo(handle_->get());
	if (hfa_map_info == nullptr) {
		return {};
	}	
	if (hfa_map_info->units == nullptr || hfa_map_info->proName == nullptr) {
		TWIST_THROW_ERDAS(L"Incomplete \"map info\" record found.");
	}
	const double cell_size = hfa_map_info->pixelSize.width;
	if (cell_size != hfa_map_info->pixelSize.height) {
		TWIST_THROW_ERDAS(L"Unsupported cell size %f x %f.", cell_size, hfa_map_info->pixelSize.height);
	}
	const double grid_left = hfa_map_info->upperLeftCenter.x - cell_size / 2;
	const double grid_bottom = hfa_map_info->lowerRightCenter.y - cell_size / 2;
	assert(grid_left + cell_size * width_ == hfa_map_info->lowerRightCenter.x + cell_size / 2);
	assert(grid_bottom + cell_size * height_ == hfa_map_info->upperLeftCenter.y + cell_size / 2);

	const SquareGeoSpaceGrid space_grid{ grid_left, grid_bottom, cell_size, height_, width_ };

	auto map_info = std::make_optional<MapInfo>(
			space_grid, ansi_to_unit(hfa_map_info->units), ansi_to_string(hfa_map_info->proName));

	const auto* pe_string = HFAGetPEString(handle_->get());
	if (pe_string != nullptr) {
		map_info->set_refsys_proj_string(ansi_to_string(pe_string));
	}

	const auto* hfa_proj_params = HFAGetProParameters(handle_->get());
	if (hfa_proj_params != nullptr) {

		MapInfo::ProjectionParams proj_params;
		proj_params.proj_type = static_cast<MapInfo::ProjectionType>(hfa_proj_params->proType);
		proj_params.proj_number = hfa_proj_params->proNumber;
		proj_params.proj_exe_name = ansi_to_string(hfa_proj_params->proExeName);  
		proj_params.proj_name = ansi_to_string(hfa_proj_params->proName);
		proj_params.proj_zone = hfa_proj_params->proZone;
		twist::copy(hfa_proj_params->proParams, proj_params.proj_params.data());  
		proj_params.proj_spheroid = {
				ansi_to_string(hfa_proj_params->proSpheroid.sphereName),
				hfa_proj_params->proSpheroid.a,
				hfa_proj_params->proSpheroid.b,
				hfa_proj_params->proSpheroid.eSquared,
				hfa_proj_params->proSpheroid.radius };

		const auto* hfa_datum = HFAGetDatum(handle_->get());
		if (hfa_datum != nullptr) {

			MapInfo::Datum datum;
			datum.datum_name = ansi_to_string(hfa_datum->datumname);
			datum.type = static_cast<MapInfo::DatumType>(hfa_datum->type); 
			if (datum.type == MapInfo::DatumType::parametric) {			
				twist::copy(hfa_datum->params, datum.params.data());
			}
			datum.grid_name = ansi_to_string(hfa_datum->gridname);
			proj_params.datum = datum;
		}

		map_info->set_projection_params(proj_params);
	}
	
	return map_info;
}

void ErdasImagineFile::write_map_info(const MapInfo& map_info)
{
	TWIST_CHECK_INVARIANT
	auto proj_name = string_to_ansi(map_info.projection_name());
	const auto top_left_cell_centre = get_cell_centre_from_row_col(
			map_info.space_grid(), 0, 0); 
	const auto bottom_right_cell_centre = get_cell_centre_from_row_col(
			map_info.space_grid(), map_info.space_grid().nof_rows() - 1, map_info.space_grid().nof_columns() - 1); 
	auto unit_str = unit_to_ansi(map_info.unit());

	const Eprj_MapInfo hfa_map_info{
			proj_name.data(),  // projection name
			{top_left_cell_centre.x(), top_left_cell_centre.y()},  // map coordinates of center of upper left pixel
			{bottom_right_cell_centre.x(), bottom_right_cell_centre.y()},  // map coordinates of center of lower right pixel
			{map_info.space_grid().cell_size(), map_info.space_grid().cell_size()},  // pixel size in map units
			unit_str.data()};  // units of the map

	TWIST_CHECK_CPL_RESULT( HFASetMapInfo(handle_->get(), &hfa_map_info) );

	if (map_info.refsys_proj_string()) {
		const auto hfa_pe_string = string_to_ansi(*map_info.refsys_proj_string());
		TWIST_CHECK_CPL_RESULT( HFASetPEString(handle_->get(), hfa_pe_string.data()) );	
	}

	const auto proj_params = map_info.projection_params();
	if (proj_params) {

		Eprj_ProParameters hfa_proj_params;
		hfa_proj_params.proType = static_cast<Eprj_ProType>(proj_params->proj_type);
        hfa_proj_params.proNumber = proj_params->proj_number;
		auto hfa_proj_exe_name = string_to_ansi(proj_params->proj_exe_name);
        hfa_proj_params.proExeName = hfa_proj_exe_name.data();
		auto hfa_proj_name = string_to_ansi(proj_params->proj_name);
        hfa_proj_params.proName = hfa_proj_name.data();                         
		hfa_proj_params.proZone = proj_params->proj_zone;
        twist::copy(proj_params->proj_params, hfa_proj_params.proParams);           

		auto hfa_sphere_name = string_to_ansi(proj_params->proj_spheroid.sphere_name);
        hfa_proj_params.proSpheroid = {
				hfa_sphere_name.data(),
				proj_params->proj_spheroid.a,
				proj_params->proj_spheroid.b,
				proj_params->proj_spheroid.e_squared,
				proj_params->proj_spheroid.radius };

		TWIST_CHECK_CPL_RESULT( HFASetProParameters(handle_->get(), &hfa_proj_params) );

		if (proj_params->datum) {

			Eprj_Datum hfa_datum;
			auto hfa_datum_name = string_to_ansi(proj_params->datum->datum_name);
			hfa_datum.datumname = hfa_datum_name.data();
			hfa_datum.type = static_cast<Eprj_DatumType>(proj_params->datum->type);  
			if (proj_params->datum->type == MapInfo::DatumType::parametric) {
				twist::copy(proj_params->datum->params, &hfa_datum.params[0]);
			}
			auto hfa_grid_name = string_to_ansi(proj_params->datum->grid_name);
			hfa_datum.gridname = hfa_grid_name.data();

			TWIST_CHECK_CPL_RESULT( HFASetDatum(handle_->get(), &hfa_datum) );		
		}
	}
}

auto ErdasImagineFile::read_uint8(std::optional<uint8_t>& nodata_value) const -> std::unique_ptr<DataGrid<uint8_t>>
{
	TWIST_CHECK_INVARIANT
	return read(nodata_value);
}

std::unique_ptr<ErdasImagineFile::DataGrid<uint16_t>> 
ErdasImagineFile::read_uint16(std::optional<uint16_t>& nodata_value) const
{
	TWIST_CHECK_INVARIANT
	return read(nodata_value);
}

std::unique_ptr<ErdasImagineFile::DataGrid<int16_t>> 
ErdasImagineFile::read_int16(std::optional<int16_t>& nodata_value) const
{
	TWIST_CHECK_INVARIANT
	return read(nodata_value);
}

std::unique_ptr<ErdasImagineFile::DataGrid<int32_t>> 
ErdasImagineFile::read_int32(std::optional<int32_t>& nodata_value) const
{
	TWIST_CHECK_INVARIANT
	return read(nodata_value);
}

void ErdasImagineFile::write_uint8(const DataGrid<uint8_t>& data, std::optional<uint8_t> nodata_value)
{
	TWIST_CHECK_INVARIANT
	write(data, nodata_value);
}

void ErdasImagineFile::write_int16(const DataGrid<int16_t>& data, 
		std::optional<int16_t> nodata_value)
{
	TWIST_CHECK_INVARIANT
	write(data, nodata_value);
}

void ErdasImagineFile::write_uint16(const DataGrid<uint16_t>& data, std::optional<uint16_t> nodata_value)
{
	TWIST_CHECK_INVARIANT
	write(data, nodata_value);
}

void ErdasImagineFile::write_int32(const DataGrid<int32_t>& data, 
		std::optional<int32_t> nodata_value)
{
	TWIST_CHECK_INVARIANT
	write(data, nodata_value);
}

void ErdasImagineFile::conclude_init()
{
	// Called in ctor: no invariant check
	bytes_per_tile_ = static_cast<int>(tile_height_ * tile_width_ * data_type_bits_ / 8);
}


template<typename T> 
std::unique_ptr<ErdasImagineFile::DataGrid<T>> ErdasImagineFile::read(std::optional<T>& nodata_value) const
{
	TWIST_CHECK_INVARIANT
	int tiles_per_row = 0;
	int tiles_per_col = 0;
	const auto tile_buffer = prepare_tile_buffer<T>(tiles_per_row, tiles_per_col);

	auto data_matrix = std::make_unique<DataGrid<T>>(height_, width_);

	for (int tile_x = 0; tile_x < tiles_per_row; ++tile_x) {
		for (int tile_y = 0; tile_y < tiles_per_col; ++tile_y) {

			TWIST_CHECK_CPL_RESULT(HFAGetRasterBlockEx(
					handle_->get(), 1/*band*/, tile_x, tile_y, tile_buffer.get(), bytes_per_tile_));
			
			for (int tile_row = 0; tile_row < tile_height_; ++tile_row) {
						
				auto img_row = tile_y * tile_height_ + tile_row;
				if (img_row < height_) {			
					
					const auto value_count = (tile_x + 1) * tile_width_ < width_ 
							? tile_width_ 
							: width_ - tile_x * tile_width_;

					const auto* tile_start = tile_buffer.get() + tile_row * tile_width_;
							
					auto matrix_row_values = data_matrix->view_row_cells(
							img_row, tile_x * tile_width_, value_count);   
					std::copy(tile_start, tile_start + value_count, matrix_row_values.begin()); 
				}
			}
		}
	}

	auto nodata_val = 0.0;
	if (HFAGetBandNoData(handle_->get(), 1/*band*/, &nodata_val) != FALSE) {
		if (nodata_val != static_cast<T>(nodata_val)) {
			TWIST_THROW_ERDAS(L"\"No-data\" value %f does not match type \"%s\".", 
					nodata_val, type_wname<T>().c_str());
		}
		nodata_value = static_cast<T>(nodata_val);
	}
	else {
		nodata_value.reset();
	}

	return data_matrix;
}


template<typename T> 
void ErdasImagineFile::write(const DataGrid<T>& data_matrix, std::optional<T> nodata_value)
{
	TWIST_CHECK_INVARIANT
	int tiles_per_row = 0;
	int tiles_per_col = 0;
	const auto tile_buffer = prepare_tile_buffer<T>(tiles_per_row, tiles_per_col);

	for (int tile_x = 0; tile_x < tiles_per_row; ++tile_x) {
		for (int tile_y = 0; tile_y < tiles_per_col; ++tile_y) {
			
			for (int tile_row = 0; tile_row < tile_height_; ++tile_row) {
						
				auto img_row = tile_y * tile_height_ + tile_row;
				if (img_row < height_) {			
					
					const auto value_count = (tile_x + 1) * tile_width_ < width_ 
							? tile_width_ 
							: width_ - tile_x * tile_width_;

					auto* tile_start = tile_buffer.get() + tile_row * tile_width_;
							
					const auto matrix_row_values = data_matrix.view_row_cells(
							img_row, tile_x * tile_width_, value_count);
					twist::copy(matrix_row_values, tile_start);   
				}
			}
			TWIST_CHECK_CPL_RESULT( 
					HFASetRasterBlock(handle_->get(), 1/*band*/, tile_x, tile_y, tile_buffer.get()) );
		}
	}
	if (nodata_value) {
		TWIST_CHECK_CPL_RESULT( HFASetBandNoData(handle_->get(), 1/*band*/, *nodata_value) );
	}
}


template<typename T>
ErdasImagineFile::BlockBuffer<T> ErdasImagineFile::prepare_tile_buffer(
		int& tiles_per_row, int& tiles_per_col) const
{
	TWIST_CHECK_INVARIANT
	check_data_value_type<T>(data_type_);

	if (nof_bands_ != 1) {
		TWIST_THROW_ERDAS(L"Image has %d bands; only one band is currently supported.", nof_bands_);
	}

	tiles_per_row = ceil_to_int((double)width_ / tile_width_);
    tiles_per_col = ceil_to_int((double)height_ / tile_height_);

	BlockBuffer<T> tile_buffer{static_cast<T*>(VSIMalloc(bytes_per_tile_)), &VSIFree};
	assert(tile_buffer);

	return tile_buffer;
}


#ifdef _DEBUG
void ErdasImagineFile::check_invariant() const noexcept
{
	assert(handle_ && !handle_->empty());
	assert(width_ > 0);
	assert(height_ > 0);
	assert(nof_bands_ > 0);
	assert(data_type_ >= GeoGridDataType::int8 && data_type_ <= GeoGridDataType::float64);
	assert(tile_width_ > 0 && (tile_width_ % 16) == 0);
	assert(tile_height_ > 0 && (tile_height_ % 16) == 0);
}
#endif

// --- ErdasImagineFile::MapInfo class ---

ErdasImagineFile::MapInfo::MapInfo(const SquareGeoSpaceGrid& space_grid, Unit unit, 
		const std::wstring& proj_name)
	: space_grid_(space_grid)
	, unit_(unit)
	, proj_name_(proj_name)
{
	TWIST_CHECK_INVARIANT
}

auto ErdasImagineFile::MapInfo::space_grid() const -> SquareGeoSpaceGrid
{
	TWIST_CHECK_INVARIANT
	return space_grid_;
}


ErdasImagineFile::MapInfo::Unit ErdasImagineFile::MapInfo::unit() const
{
	TWIST_CHECK_INVARIANT
	return unit_;
}


std::wstring ErdasImagineFile::MapInfo::projection_name() const
{
	TWIST_CHECK_INVARIANT
	return proj_name_;
}


std::optional<std::wstring> ErdasImagineFile::MapInfo::refsys_proj_string() const
{
	TWIST_CHECK_INVARIANT
	return refsys_proj_string_;
}


void ErdasImagineFile::MapInfo::set_refsys_proj_string(const std::wstring& str)
{
	TWIST_CHECK_INVARIANT
	if (refsys_proj_string_) {
		TWIST_THROW(L"The reference system projection engine string has already been set.");
	}
	refsys_proj_string_ = str;
}


std::optional<ErdasImagineFile::MapInfo::ProjectionParams> ErdasImagineFile::MapInfo::projection_params() const
{
	TWIST_CHECK_INVARIANT
	return proj_params_;
}


void ErdasImagineFile::MapInfo::set_projection_params(const ErdasImagineFile::MapInfo::ProjectionParams& params)
{
	TWIST_CHECK_INVARIANT
	if (proj_params_) {
		TWIST_THROW(L"The projection parameters have already been set.");
	}
	proj_params_ = params;
}


#ifdef _DEBUG
void ErdasImagineFile::MapInfo::check_invariant() const noexcept
{
	assert(unit_ == Unit::metre);
	assert(!proj_name_.empty());
}
#endif


}

/// @file GeoCoordProjectorGdal.hpp
/// GeoCoordProjectorGdal class, inherits GeoCoordProjector

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_GIS_COORD_PROJECTOR_GDAL_HPP
#define TWIST_GIS_COORD_PROJECTOR_GDAL_HPP

#include "twist/gis/GeoCoordProjector.hpp"
#include "twist/gis/CoordinateTransformation.hpp"

namespace twist::gis {
class SpatialReferenceSystem;
}

namespace twist::gis {

//! A GDAL-based implementation of GeoCoordProjector.
class GeoCoordProjectorGdal : public GeoCoordProjector {
public:
	/*! Constructor.
	    \param[in] degree_refsys  The spheroidal/geographic/degree-based spatial reference system
	    \param[in] metre_refsys  The flat/projected/metre-based spatial reference system
	*/
	GeoCoordProjectorGdal(const SpatialReferenceSystem& degree_refsys, 
			              const SpatialReferenceSystem& metre_refsys);

	~GeoCoordProjectorGdal() final;

	[[nodiscard]] auto degree_to_metre(GeoPointDecDegree pt) const -> GeoPointMetre final; // GeoCoordProjector override

	[[nodiscard]] auto metre_to_degree(GeoPointMetre pt) const -> GeoPointDecDegree final; // GeoCoordProjector override

	TWIST_NO_COPY_DEF_MOVE(GeoCoordProjectorGdal)

private:
	CoordinateTransformation degree_to_metre_trans_;
	CoordinateTransformation metre_to_degree_trans_;

	TWIST_CHECK_INVARIANT_DECL
};

}

#endif

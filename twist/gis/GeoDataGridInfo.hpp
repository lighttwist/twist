/// @file GeoDataGridInfo.hpp
/// GeoDataGridInfo class definition

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_GIS_GEO_DATA_GRID_INFO_HPP
#define TWIST_GIS_GEO_DATA_GRID_INFO_HPP

#include "twist/gis/gis_globals.hpp"
#include "twist/gis/gis_geometry.hpp"
#include "twist/math/space_grid.hpp"

namespace twist::gis {

//! Stores information about a geographic data grid, ie a data grid paired with geographic coordinates.
class GeoDataGridInfo {
public:	
	/*! Constructor.
	    \param[in] space_grid  The spatial grid underlying the data grid (in geographic coordinates)
	    \param[in] nodata_value  Textual representation of the "no-data" placeholder value; pass in an empty string if 
	                             the grid does not use a "no-data" value
	    \param[in] data_value_type  The type of a grid cell value
	 */
	GeoDataGridInfo(const GeoSpaceGrid& space_grid,
	                std::string nodata_value, 
	                GeoGridDataType data_value_type);

	//! Reference to the spatial grid underlying the data grid (in geographic coordinates).
	[[nodiscard]] auto space_grid() const -> GeoSpaceGrid;

	//! Textual representation of the "no-data" placeholder value; empty if the grid does not use a "no-data" value.
	[[nodiscard]] auto nodata_value() const -> std::string;

	//! The type of a grid cell value.
	[[nodiscard]] auto data_value_type() const -> GeoGridDataType;

private:
	GeoSpaceGrid space_grid_;
	std::string nodata_value_;
	GeoGridDataType data_value_type_;	

	TWIST_CHECK_INVARIANT_DECL
};

}

#endif

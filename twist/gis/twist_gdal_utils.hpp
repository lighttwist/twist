/// @file twist_gdal_utils.hpp
/// Utilities for working with GIS (Geographic information system) data via GDAL (Geospatial Data Abstraction Library) 

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_GIS_GDAL__UTILS_HPP
#define TWIST_GIS_GDAL__UTILS_HPP

#include "twist/gis/geotiff_defs.hpp"
#include "twist/gis/geotiff_utils.hpp"
#include "twist/gis/gis_geometry.hpp"
#include "twist/gis/gis_globals.hpp"

#include <mutex>
#include <thread>

namespace twist::gis {
class GeoCoordProjector;
class SpatialReferenceSystem;
}

namespace twist::gis {

/// Common GIS raster file formats, that is, files containing georeferenced gridded data.
enum class RasterFileFormat {
	geotiff = 1,  ///< GeoTIFF
	png = 2,  ///< PNG (Portable Network Graphics)
	ascii = 3,  ///< Esri (ARC/INFO) ASCII
	img = 4  ///< ERDAS Imagine
};

/// Very simple geographic information about a raster dataset, often part of the information stored in a 
///   raster file reconisable by GDAL. 
/// See GDALDataset in the GDAL documentation for details.
struct RasterGeoInfo {
	GeoPoint origin;
	GeoVector pixel_size;
};

/// Information about a raster dataset, eg as stored in a raster file reconisable by GDAL.
/// See GDALDataset in the GDAL documentation for details.
struct RasterDatasetInfo {
	struct RasterBandInfo {
		int width;
		int height;
		int block_width; 
		int block_height;
		GeoGridDataType data_value_type;
		double min_value;
		double max_value;
		std::optional<double> no_data_value;
		int overview_count;
		// ColorInterpretation colour_interpretation;
		std::wstring colour_interpretation_name;
		int colour_table_entry_count;
	};

	std::wstring driver_name;
	std::wstring driver_long_name;
	int width;
	int height;
	int band_count;
	std::optional<std::wstring> projection;
	std::optional<RasterGeoInfo> geo_info; 
	std::map<int, RasterBandInfo> bands_info;
};

/*! Resamling algorithm to be used for recalculating cell values in a raster during a raster operation, eg conversion 
    of raster data between different formats.
 */
enum class RasterResampleAlgorithm {
	none = 0, ///< Invalid value
	nearest = 1, ///< Nearest neighbour resampling (fastest algorithm, worst interpolation quality)
	bilinear = 2, ///< Bilinear resampling
	cubic = 3, ///< Cubic resampling
	cubic_spline = 4, ///< Cubic spline resampling
	lanczos = 5, ///< Lanczos windowed sinc resampling
	average = 6, ///< Average resampling, computes the average of all non-"nodata" contributing pixels
	mode = 7 ///< Mode resampling, selects the value which appears most often of all the sampled points
};

/// Feature field data types
enum class FeatureFieldType {
	int32 = 1,  ///< Simple 32bit integer                    
    int32_list = 2,  ///< List of 32bit integers     
    int64 = 3,  ///< Single 64bit integer                    
    int64_list = 4,  ///< List of 64bit integers                  
	float64 = 5,  ///< Double Precision floating point       
    float64_list = 6,  ///< List of doubles                        
    string = 7,  ///< String of ASCII chars                
    string_list = 8,  ///< Array of strings                       
    binary = 9,  ///< Raw Binary data                       
    date = 10,  ///< Date                                    
    time = 11,  ///< Time                                  
    date_time = 12  ///< Date and Time                         
};

/*! Information about a feature field, ie an attribute of a feature layer/class; for example a field in a vector 
    layer's attribute table. 
 */
class FeatureFieldInfo {
public:
	/*! Constructor.
	    \param[in] name  The field name; be aware that the current shapefile specification imposes a maximum field name 
	                     size of 10 charaters 
	    \param[in] type  The field data type
	 */
	FeatureFieldInfo(std::string name, FeatureFieldType type);

	/// The field name.
	std::string name() const;

	/// The field data type.
	FeatureFieldType type() const;

private:
	std::string  name_;
	FeatureFieldType  type_;

	TWIST_CHECK_INVARIANT_DECL
};

//! Class whose instances call gdal_initialise() in the constructor and gdal_uninitialise() in the destructor. 
class [[nodiscard]] GdalInitialiser {
public:
	explicit GdalInitialiser();

	~GdalInitialiser();

	TWIST_NO_COPY_NO_MOVE(GdalInitialiser)
};

/*! Stores the data associated with a parameter which specifies that the input pixels values should be the scaled 
    and/or shifted in the output (by applying a linear transformation). The transformation is specified by providing an 
	example range of input pixel values and the range of output values that it should map to (the value in the example 
	range needn't actually exist in the input data). The transformation is then applied to all input pixel values.
 */
class RasterValueRescale {
public:
	/*! Constructor. 
	    \tparam SrcPixelVal  The pixel value type of the input raster 
	    \tparam DestPixelVal  The pixel value type of the output raster 
	    \param[in] in_range  The input raster pixel value range for calculating the scaling
	    \param[in] out_range  The output raster pixel value range for calculating the scaling
	*/
	template<class SrcPixelVal, class DestPixelVal>
	requires twist::is_number<SrcPixelVal> && twist::is_number<DestPixelVal>
	explicit RasterValueRescale(twist::math::ClosedInterval<SrcPixelVal> in_range, 
	                            twist::math::ClosedInterval<DestPixelVal> out_range);

	//! Create an instance specifying a transformation of all input values to "nan". +TODO: Not tested! 1Oct'23
	template<class SrcPixelVal>
	requires twist::is_number<SrcPixelVal>
	[[nodiscard]] static auto to_nan() -> RasterValueRescale; 

	//! Lower bound of the example input raster pixel value range (stored as a string).
	[[nodiscard]] auto in_lo() const -> std::string;

	//! Upper bound of the example input raster pixel value range (stored as a string).
	[[nodiscard]] auto in_hi() const -> std::string;

	//! Lower bound of the output raster pixel value range (stored as a string).
	[[nodiscard]] auto out_lo() const -> std::string;

	//! Upper bound of the output raster pixel value range (stored as a string).
	[[nodiscard]] auto out_hi() const -> std::string;

private:
	RasterValueRescale() = default;

    std::string in_lo_;
	std::string in_hi_;
	std::string out_lo_;
	std::string out_hi_;

	TWIST_CHECK_INVARIANT_DECL
};

using GdalWarningListener = std::function<void(std::wstring)>;

/*! Initialise the GDAL library, as used by the "twist::gis" library.
    Call this function before using the "twist::gis" GDAL facilities from your program.
 */
auto gdal_initialise() -> void;

/*! Un-initialise the GDAL library, as used by the "twist::gis" library.
    Call this function after you are done using the "twist::gis" GDAL facilities from your program.
 */
auto gdal_uninitialise() -> void;

/*! Add a function which will be called when GDAL code emits a warning, with the warning message as the parameter.
    If multiple listeners are added using this function, they will be called in the order they were added. 
 */
auto gdal_push_warning_listener(GdalWarningListener listener) -> void;

/*! Create a OpenGIS Spatial Reference System object based on a the coordinate system imported from an ESRI .prj file.
    \param[in] path  The ESRI .prj file path; currently only GEOGRAPHIC, UTM, STATEPLANE, GREATBRITIAN_GRID, ALBERS, 
	                 EQUIDISTANT_CONIC, TRANSVERSE (mercator), POLAR, MERCATOR and POLYCONIC projections are supported
    \return  The spatial reference system object
 */
[[nodiscard]] auto refsys_from_prj_file(const fs::path& path) -> SpatialReferenceSystem;

/*! Create a OpenGIS Spatial Reference System object based on a the coordinate system imported from the string 
    \p wkt_str, which follows the "well-known text representation of coordinate reference systems" format.
 */
[[nodiscard]] auto refsys_from_wkt_string(const char* wkt_str) -> SpatialReferenceSystem;

/*! Create an OpenGIS Spatial Reference System object based on a "known reference system" ID.
    \param[in] known_refsys_id  The "known reference system" ID
    \return  The spatial reference system object
 */
[[nodiscard]] auto refsys_from_known(KnownGeoRefsysId known_refsys_id) -> SpatialReferenceSystem;

/*! Search for a "known reference system" matching the reference system \p refsys. 
    If no match is found, nullopt is returned.
 */
[[nodiscard]] auto find_matching_known_geo_refsys(const SpatialReferenceSystem& refsys) 
                    -> std::optional<KnownGeoRefsysId>;

/*! Check whether a specific geospatial reference system matches a "known reference system" ID.
    Note that in the current implementation can be refined.
    \param[in] refsys  The reference system
    \param[in] known_refsys_id  The "known reference system" ID
    \return  true if they match
 */
[[nodiscard]] auto match(const SpatialReferenceSystem& refsys, KnownGeoRefsysId known_refsys_id) -> bool;

/*! Create a OpenGIS Spatial Reference System object based on a geographic coordinate system with a well known name.
    \param[in] name  The well known geographic coordinate system name; the following values are currently supported:
   						"WGS84": same as "EPSG:4326" but has no dependence on EPSG data files
   						"WGS72": same as "EPSG:4322" but has no dependence on EPSG data files
   						"NAD27": same as "EPSG:4267" but has no dependence on EPSG data files
   						"NAD83": same as "EPSG:4269" but has no dependence on EPSG data files
   						"EPSG:n": same as doing an ImportFromEPSG(n).
    \return  The spatial reference system object
 */   
[[nodiscard]] auto refsys_from_well_known_name(const std::wstring& name) -> SpatialReferenceSystem;

//! Create a OpenGIS Spatial Reference System object based on the EPSG number \p epsg.
[[nodiscard]] auto refsys_from_epsg(int epsg) -> SpatialReferenceSystem;

/*! Create a OpenGIS Spatial Reference System object based on the standard geographic coordinate system "WGS84" (so 
    using decimal degrees as coordinates).
 */
[[nodiscard]] auto refsys_from_wgs84() -> SpatialReferenceSystem;

/*! Create a GDAL-based projector which performs coordinate transformations between a "projected" coordinate system 
    (based on X and Y coordinates, and measured in metres) based on a "known reference system" ID, and the "geographic" 
	coordinate system "WGS84" (based on longitude and latitude, and measured in degrees).
    \param[in] known_refsys_id  The "known reference system" ID
 */
[[nodiscard]] auto make_wgs84_coord_projector(KnownGeoRefsysId known_refsys_id) -> std::unique_ptr<GeoCoordProjector>;

/*! Create a GDAL-based projector which performs coordinate transformations between a "projected" coordinate system 
    (based on X and Y coordinates, and measured in metres), which is read from a ESRI .prj file, and the "geographic" 
	coordinate system "WGS84" (based on longitude and latitude, and measured in degrees).
    \param[in] prj_path  The ESRI .prj file path
 */
[[nodiscard]] auto make_wgs84_coord_projector(const fs::path& prj_path) -> std::unique_ptr<GeoCoordProjector>;

/*! Transform a geographic point's coordinates from one geographic coordinate system to another.
    \param[in] pt  The geographic point
    \param[in] src_ref  The source spatial reference system 
    \param[in] dest_ref  The destination spatial reference system 
    \return  The point in transformed coordinates
 */
[[nodiscard]] auto transform(const GeoPoint& pt, 
						     const SpatialReferenceSystem& src_ref, 
		                     const SpatialReferenceSystem& dest_ref) -> GeoPoint;

/// Given a raster file reconisable by GDAL (eg GeoTIFF, Esri (ARC/INFO) ASCII, ERDAS Imagine, etc), read 
/// information about the raster dataset from that file.
///
/// @param[in] raster_file_path  The raster file path; an exception is thrown if it is not a valid raster 
///					file recognised by the GDAL library
/// @return  Information about the raster dataset
///
RasterDatasetInfo read_raster_info(const fs::path& raster_file_path);

/*! Given an instance of raster dataset information, extract the geographic extent (perimeter rectangle) of the data 
    grid.
    \param[in] raster_info  The raster dataset information; it must contain geographic information and its origin must 
                            be the top-left  
    \return  The geographic extent of the raster
 */
[[nodiscard]] auto get_geo_extent(const RasterDatasetInfo& raster_info) -> GeoRectangle;

/*! Given an instance of raster dataset information, extract the spatial grid underlying the data grid.
    \param[in] raster_info  The raster dataset information; it must contain geographic information, its cells must be 
                            square and the origin must be the top-left 
    \return  The spatial grid underlying the raster
 */
[[nodiscard]] auto get_geo_space_grid(const RasterDatasetInfo& raster_info) -> SquareGeoSpaceGrid;  

/*! Given an instance of single-band raster dataset information, extract the "no-data" placeholder value (if any) for 
    its band.
    \tparam  The expected type of the "no-data" placeholder value
    \param[in] raster_info  The raster dataset information; if it does not describe a single-band raster, an exception
                            is thrown
    \return  The "no-data" placeholder value; or nullopt if none is specified
 */
template<class NodataVal>
[[nodiscard]] auto get_single_band_nodata_value(const RasterDatasetInfo& raster_info) -> std::optional<NodataVal>;

/*! Burn vector geometries (points, lines, and polygons) read from one or more vector layers into the raster band(s) of 
    a raster image.
    \param[in] in_path  Path of the input file containing the vector layer(s); must follow an OGR supported vector 
	                    format
    \param[in] out_path  Path of the output raster file; must not already exist 
    \param[in] in_layer_names  The names of the vector layer(s) to be burned into the output raster 
    \param[in] in_attr_name  The name of the attribute field of the vector layer features to be used for a burn-in 
	                         value; the value will be burned into all output bands
    \param[in] out_grid  Grid which defines the extent of the input layer(s) to be burned into to the raster and the 
	                     cell size of the output raster
    \param[in] out_data_type  The data type of the output raster band(s)
    \param[in] nodata_value  Optional "no-data" value placeholder  
    \param[in] out_format  The format of the output raster file; if nullopt, if not specified, the format is guessed 
	                       from the extension +TODO: Is that true? 1Oct'23
 */
auto rasterise_vector(const fs::path& in_path, 
                      const fs::path& out_path, 
		              const std::vector<std::string>& in_layer_names, 
					  std::string in_attr_name, 
		              const SquareGeoSpaceGrid& out_grid, 
		              GeoGridDataType out_data_type, 
					  std::optional<std::string> nodata_value = std::nullopt,
		              std::optional<RasterFileFormat> out_format = std::nullopt) -> void;

/*! Polygonise a raster, ie create a vector layer in which the raster cells which are connected (in the 4-direction/
    rook's case connectivity sense) and have the same value are added to the same polygon. 
    The function creates a shapefile storing the vector layer with an attributes table containing a field which stores 
	the source raster value corresponding to each polygon.
    Note that currently the source pixel values are read into a signed 32bit integer buffer, so floating-point or 
	complex bands will be implicitly truncated before processing. 
    \param[in] in_path  The path of the input raster file
    \param[in] out_path  The path of the output shapefile directory; if the directory exists and it is not empty, an 
	                     exception is thrown 
    \param[in] out_field_info  Information about the shapefile attributes table field  
 */   
auto polygonise_raster(const fs::path& in_path, const fs::path& out_path, FeatureFieldInfo out_field_info) 
       -> void;  

/*! Same as the overload which takes one field info parameter, except it creates a second field, described by 
    \p out_field2_info, which will have NULL values.
 */ 
auto polygonise_raster(const fs::path& in_path, const fs::path& out_path, 
		               FeatureFieldInfo out_field1_info, FeatureFieldInfo out_field2_info) -> void;  

/*! Read data from a raster file, convert it to a differen format and/or perform some operations such as subsetting, 
    resampling and rescaling pixels, and save the converted data to a new raster file.
    This is the equivalent of the "gdal_translate" utility, but only implements a subset of its functionality.	
    \param[in] in_path  The path of the input raster file
    \param[in] out_path  The path of the output raster file; if the file already exists, an exception is thrown 
    \param[in] out_space_grid  Spatial grid underlying the output raster; nullopt means the grid stays unchanged
    \param[in] out_format  The output raster file format
    \param[in] out_data_type  The data type of the output raster values; nullopt means the data type stays unchanged
    \param[in] resample_algo  The algorithm to be used for resampling; "nearest neighbour" is used if nullopt is 
	                          passed in. Pay extra attention when using algo type "nearest"; the space grid underlying 
							  the output image may not match \p out_space_grid. Consider using warp_raster() instead.
	\param[in] geotiff_nof_cells_per_tile  Geotiff-specific parameter: To create a tiled geotiff, pass in the number of
	                                       cells in a tile - the value must be a perfect square and a multiple of 256; 
										   nullopt means the output geotiff is stripped
	\param[in] value_rescale  Specifies that the input pixels values should be rescaled/shifted (using a linear 
	                          transformation); nullopt means no rescaling
	\param[in] geotiff_compression  Geotiff-specific parameter: Data compression; nullopt means the output geotiff is 
	                                not compressed
	\param[in] geotiff_bigtiff  Geotiff-specific parameter: Whether the output geotiff should be marked as BigTIFF 
	                            (bigger than 4GB)
 */    
auto translate_raster(const fs::path& in_path, 
                      const fs::path& out_path, 
					  RasterFileFormat out_format, 
		              std::optional<GeoSpaceGrid> out_space_grid, 
					  std::optional<GeoGridDataType> out_data_type = std::nullopt, 
					  std::optional<RasterResampleAlgorithm> resample_algo = std::nullopt,
					  std::optional<RasterValueRescale> value_rescale = std::nullopt,
					  std::optional<Ssize> geotiff_nof_cells_per_tile = std::nullopt,
					  std::optional<GeotiffCompression> geotiff_compression = std::nullopt,
					  bool geotiff_bigtiff = false) -> void; 

/*! Image reprojection and warping function.
    This is the equivalent of the "gdalwarp" utility, but only implements a subset of its functionality.	
    \param[in] in_path  The path of the input raster file
    \param[in] out_path  The path of the output raster file; if the file already exists, an exception is thrown 
    \param[in] out_format  The output raster file format; the format will be the same as the input raster's if nullopt 
	                       is passed in; if this argument is not nullopt, make sure to also give the output path an 
						   extension matching the desired format, otherwise an exception will be thrown
	\param[in] out_nodata_val  The "no-data" value placeholder in the destination image; if the source image specifies
	                           a "no-data" value, then any pixel values matching the old value will be changed to the 
							   new value; pass in "None" to have no "no-data" specification in the destination image;
							   nullopt means the "no-data" specification stays unchanged
							   +TODO: Test this parameter exhaustively. 1Oct'23
    \param[in] out_space_grid  Spatial grid underlying the output raster; nullopt means the grid stays unchanged
    \param[in] out_data_type  The data type of the output raster values; nullopt means the data type stays unchanged
    \param[in] resample_algo  The algorithm to be used for resampling; nullopt means "nearest neighbour"
	\param[in] geotiff_nof_cells_per_tile  Geotiff-specific parameter: To create a tiled geotiff, pass in the number of
	                                       cells in a tile - the value must be a perfect square and a multiple of 256; 
										   nullopt means the output geotiff is stripped
	\param[in] geotiff_compression  Geotiff-specific parameter: Data compression; nullopt means the output geotiff is 
	                                not compressed
 */
auto warp_raster(const fs::path& in_path, 
				 const fs::path& out_path,
				 std::optional<RasterFileFormat> out_format,  
				 std::optional<std::string> out_nodata_val = std::nullopt,
				 std::optional<SquareGeoSpaceGrid> out_space_grid = std::nullopt, 
				 std::optional<GeoGridDataType> out_data_type = std::nullopt, 
				 std::optional<RasterResampleAlgorithm> resample_algo = std::nullopt,
			     std::optional<Ssize> geotiff_nof_cells_per_tile = std::nullopt,
                 std::optional<GeotiffCompression> geotiff_compression = std::nullopt) -> void;

/*! Find out whether a specific "feature field type" value is one of the defined enum constants.
    \param[in] type  The value
    \return  true if the value is defined
 */
[[nodiscard]] auto is_defined(FeatureFieldType type) -> bool;

/*! Fetch the last error message posted with CPLError(), that hasn't been cleared by CPLErrorReset(). 
    An empty string if returned if there is no posted error message. 
 */
[[nodiscard]] auto get_last_cpl_error_msg() -> std::wstring;

}

#include "twist/gis/twist_gdal_utils.ipp"

#endif  

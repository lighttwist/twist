/// @file twist_gdal_utils.cpp
/// Implementation file for "twist_gdal_utils.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

//  See http://www.gdal.org/files.html

#include "twist/twist_maker.hpp"

#include "twist/gis/twist_gdal_utils.hpp"

#include "twist/cast.hpp"
#include "twist/Chronometer.hpp"
#include "twist/scope.hpp"
#include "twist/gis/GeoCoordProjectorGdal.hpp"
#include "twist/gis/gis_globals.hpp"
#include "twist/gis/internal_gdal_utils.hpp"
#include "twist/gis/SpatialReferenceSystem.hpp"
#include "twist/math/numeric_utils.hpp"

// Disable warning C4251: 'std::*' needs to have dll-interface to be used by clients of class 'std::*'
// The warning is triggered by including "cpl_string.hpp"
#pragma warning(disable: 4251)  

#include "gdal/include/gdal_alg.h"
#include "gdal/include/gdal_utils.h"
#include "gdal/include/gdal_priv.h"
#include "gdal/include/ogr_spatialref.h"
#include "gdal/include/ogrsf_frmts.h"
#include "gdal/include/cpl_string.h"
#include "gdal/include/cpl_conv.h" // for CPLMalloc()

#include <iomanip>

using namespace twist::math;

using gsl::not_null;
using twist::format_str;
using twist::string_to_ansi;

namespace twist::gis {

static const auto double_arg_prec = 8; 

// --- Local types ---

class CplErrorHandler {
public:
	static auto __stdcall cpl_error_handler(CPLErr err_class, CPLErrorNum err_no, const char* msg) -> void
	{
		const auto lock = std::lock_guard<std::mutex>{mutex__};
		switch (err_class) {
			case CE_None: 
				return; // Success 
			case CE_Debug: 
			case CE_Failure: 
			case CE_Fatal: 
				TWIST_THRO2(L"{} error occurred with error number {}: \"{}\".",
					        cpl_err_class_name(err_class), err_no, ansi_to_string(msg));
			case CE_Warning: {
				const auto warning = std::format(L"GDAL warning occurred with error number {}: \"{}\".",
					                             err_no, ansi_to_string(msg));
				for (const auto& listener : warning_listeners__) {
					std::invoke(listener, warning);
				}
				break;
			}
			default:
				TWIST_THRO2(L"Unrecognised \"CPLErr\" error class {}.", cpl_err_class_name(err_class));
		}
	}

	[[nodiscard]] static auto push_warning_listener(GdalWarningListener listener) -> auto
	{
		const auto lock = std::lock_guard<std::mutex>{mutex__};
		warning_listeners__.push_back(move(listener));
	}

private:
	[[nodiscard]] static auto cpl_err_class_name(int err_no) -> std::wstring
	{
		switch (err_no) {
		case CE_None: return L"CE_None"; // Success 
		case CE_Debug: return L"CE_Debug";
		case CE_Warning: return L"CE_Warning";
		case CE_Failure: return L"CE_Failure";
		case CE_Fatal: return L"CE_Fatal";
		default: return L"Unrecognised error number";
		}
	}

	static inline std::vector<GdalWarningListener> warning_listeners__ = {};
	static inline std::mutex mutex__ = {};
};

class SplitArgs {
public:
	SplitArgs();

	auto add(std::string_view switch_name) -> void;

	auto add(std::string_view switch_name, const char* value) -> void;

	void add(std::string_view switch_name, std::optional<std::string> value);

	void add(std::string_view switch_name, const std::vector<std::string>& values);

	void add(std::string_view switch_name, std::optional<RasterFileFormat> raster_format);

	void add(std::string_view switch_name, GeoGridDataType data_type);

	void add(std::string_view switch_name, std::optional<GeoGridDataType> data_type);

	void add(std::string_view switch_name, GeoSize size);

	void add(std::string_view switch_name, std::optional<GeoSize> size);

	void add_lu(std::string_view switch_name, const GeoRectangle& rect);

	void add_lu(std::string_view switch_name, std::optional<GeoRectangle> rect);

	void add_ul(std::string_view switch_name, const GeoRectangle& rect);

	void add_ul(std::string_view switch_name, std::optional<GeoRectangle> rect);

	void add(std::string_view switch_name, std::optional<RasterResampleAlgorithm> algo);

	auto add(std::string_view switch_name, std::optional<RasterValueRescale> value_rescale) -> void;

	char** get();

	TWIST_NO_COPY_DEF_MOVE(SplitArgs)
private:
	void push_back(double value);

	std::vector<std::string>  args_{};
	std::stringstream  str_{};
	std::vector<char*>  raw_args_{};
};

// --- Local functions ---

GeoGridDataType geo_grid_data_value_type(GDALDataType data_type)
{
	switch (data_type) {
	case GDT_Unknown: TWIST_THROW(L"Unsupported GDAL data type 'GDT_Unknown'.");
    case GDT_Byte: return GeoGridDataType::uint8;
    case GDT_UInt16: return GeoGridDataType::uint16;
    case GDT_Int16: return GeoGridDataType::int16;
    case GDT_UInt32: return GeoGridDataType::uint32;
    case GDT_Int32: return GeoGridDataType::int32;
    case GDT_Float32: return GeoGridDataType::float32;
    case GDT_Float64: return GeoGridDataType::float64;
    case GDT_CInt16: TWIST_THROW(L"Unsupported GDAL complex data type 'GDT_CInt16'.");
    case GDT_CInt32: TWIST_THROW(L"Unsupported GDAL complex data type 'GDT_CInt32'.");
    case GDT_CFloat32: TWIST_THROW(L"Unsupported GDAL complex data type 'GDT_CFloat32'.");
    case GDT_CFloat64: TWIST_THROW(L"Unsupported GDAL complex data type 'GDT_CFloat64'.");
	default: TWIST_THROW(L"Unrecognised GDAL data type value '%d'.", data_type);
	}
}


GDALDataType gdal_data_type(GeoGridDataType data_type)
{
	switch (data_type) {
    case GeoGridDataType::uint8: return GDT_Byte;
    case GeoGridDataType::uint16: return GDT_UInt16;
    case GeoGridDataType::int16: return GDT_Int16;
    case GeoGridDataType::uint32: return GDT_UInt32;
    case GeoGridDataType::int32: return GDT_Int32;
    case GeoGridDataType::float32: return GDT_Float32;
    case GeoGridDataType::float64: return GDT_Float64;
	case GeoGridDataType::int8: TWIST_THROW(L"Unsupported data type 'GeoGridDataType::int8'.");
	default: TWIST_THROW(L"Unrecognised data type value '%d'.", data_type);
	}
}

std::string output_data_type_argument(GeoGridDataType data_type)
{
	switch (data_type) {
	case GeoGridDataType::uint8  : return "Byte";
	case GeoGridDataType::int16  : return "Int16";
	case GeoGridDataType::uint16 : return "UInt16";
	case GeoGridDataType::int32  : return "Int32";
	case GeoGridDataType::uint32 : return "UInt32";
	case GeoGridDataType::float32: return "Float32";
	case GeoGridDataType::float64: return "Float64";
	case GeoGridDataType::int8: 
		TWIST_THROW(L"Data value type 'int8' invalid is an output raster data type.");
	default: 
		TWIST_THROW(L"Unrecognised raster data type value %d.", data_type);
	}
}

std::string output_driver_argument(RasterFileFormat format)
{
	switch (format) {
	case RasterFileFormat::geotiff : return "GTiff";
	case RasterFileFormat::png : return "PNG";
	case RasterFileFormat::ascii: return "AAIGrid";
	case RasterFileFormat::img: return "HFA";
	default: TWIST_THROW(L"Unrecognised raster file format value %d.", format);
	}
}

std::string rect_argument_ul(const GeoRectangle& rect)
{
	// ulx uly lrx lry
	std::stringstream s;
	s << std::fixed << std::setprecision(double_arg_prec) 
			        << rect.left() << " " << rect.top() << " " << rect.right() << " " << rect.bottom(); 
	return s.str();
}

std::string rect_argument_lu(const GeoRectangle& rect)
{
	// lrx lry ulx uly 
	std::stringstream s;
	s << std::fixed << std::setprecision(double_arg_prec) 
			        << rect.left() << " " << rect.bottom() << " " << rect.right() << " " << rect.top(); 
	return s.str();
}

std::string size_argument(const GeoSize& size)
{
	// width height
	std::stringstream s;
	s << std::fixed << std::setprecision(double_arg_prec) 
			        << size.width() << " " << size.height(); 
	return s.str();
}

std::string resample_algorithm_argument(RasterResampleAlgorithm alg)
{
	switch (alg) {
	case RasterResampleAlgorithm::none: TWIST_THROW(L"Invalid resampling algorithm 'none'.");
	case RasterResampleAlgorithm::nearest: return "near";
	case RasterResampleAlgorithm::bilinear: return "bilinear";
	case RasterResampleAlgorithm::cubic: return "cubic";
	case RasterResampleAlgorithm::cubic_spline: return "cubicspline";
	case RasterResampleAlgorithm::lanczos: return "lanczos";
	case RasterResampleAlgorithm::average: return "average";
	case RasterResampleAlgorithm::mode: return "mode";
	default: TWIST_THROW(L"Unrecongnised resampling algorithm value %d.", alg);
	}
}

// function which checks extentions against out formats
[[nodiscard]] auto check_file_extension_match_raster_format(const fs::path& file_path, 
                                                            std::optional<RasterFileFormat> out_format) -> void
{
	if (!out_format) {
		return;
	}

	auto ext = file_path.extension().wstring();
	to_lower(ext);

	auto good_extensions = std::vector<std::wstring>{};
	switch (*out_format) {
	case RasterFileFormat::geotiff: 
		good_extensions = {L".tiff", L".tif"};
		break;
	case RasterFileFormat::png: 
		good_extensions = {L".png"};
		break;
	case RasterFileFormat::ascii: 
		good_extensions = {L".asc"};
		break;
	case RasterFileFormat::img: 
		good_extensions = {L".img"};
		break;
	}

	if (!twist::has(good_extensions, ext)) {
		TWIST_THRO2(L"Incompatible output raster format \"{}\" and file extension \"{}\".", 
		            ansi_to_string(output_driver_argument(*out_format)), ext);
	}
}


// --- Local type implementations ---

SplitArgs::SplitArgs()
{
	str_ << std::fixed << std::setprecision(double_arg_prec);
}

auto SplitArgs::add(std::string_view switch_name) -> void
{
	args_.emplace_back(switch_name);
}

auto SplitArgs::add(std::string_view switch_name, const char* value) -> void
{
	args_.emplace_back(switch_name);
	args_.emplace_back(value);
}

void SplitArgs::add(std::string_view switch_name, std::optional<std::string> value)
{
	if (value) {
		args_.emplace_back(switch_name);
		args_.emplace_back(move(*value));
	}
}

void SplitArgs::add(std::string_view switch_name, const std::vector<std::string>& values)
{
	args_.emplace_back(switch_name);
	for (const auto& v : values) {
		args_.push_back(v);
	}
}

void SplitArgs::add(std::string_view switch_name, std::optional<RasterFileFormat> value)
{
	if (value) {
		args_.emplace_back(switch_name);
		args_.push_back(output_driver_argument(*value));
	}
}

void SplitArgs::add(std::string_view switch_name, GeoGridDataType data_type)
{
	args_.emplace_back(switch_name);
	args_.push_back(output_data_type_argument(data_type));
}

void SplitArgs::add(std::string_view switch_name, std::optional<GeoGridDataType> data_type)
{
	if (data_type) {
		add(switch_name, *data_type);
	}
}

void SplitArgs::add(std::string_view switch_name, GeoSize size)
{
	args_.emplace_back(switch_name);
	push_back(size.width());
	push_back(size.height());
}

void SplitArgs::add(std::string_view switch_name, std::optional<GeoSize> size)
{
	if (size) add(switch_name, *size); 
}

void SplitArgs::add_lu(std::string_view switch_name, const GeoRectangle& rect)
{
	args_.emplace_back(switch_name);
	push_back(rect.left());
	push_back(rect.bottom());
	push_back(rect.right());
	push_back(rect.top());
}

void SplitArgs::add_lu(std::string_view switch_name, std::optional<GeoRectangle> rect)
{
	if (rect) {
		add_lu(switch_name, *rect);
	}
}

void SplitArgs::add_ul(std::string_view switch_name, const GeoRectangle& rect)
{
	args_.emplace_back(switch_name);
	push_back(rect.left());
	push_back(rect.top());
	push_back(rect.right());
	push_back(rect.bottom());
}

void SplitArgs::add_ul(std::string_view switch_name, std::optional<GeoRectangle> rect)
{
	if (rect) {
		add_ul(switch_name, *rect);
	}
}

void SplitArgs::add(std::string_view switch_name, std::optional<RasterResampleAlgorithm> algo)
{
	if (algo) {
		args_.emplace_back(switch_name);
		args_.push_back(resample_algorithm_argument(*algo));
	}
}

auto SplitArgs::add(std::string_view switch_name, std::optional<RasterValueRescale> value_rescale) -> void
{
	if (value_rescale) {
		args_.emplace_back(switch_name);
		args_.push_back(value_rescale->in_lo());
		args_.push_back(value_rescale->in_hi());
		args_.push_back(value_rescale->out_lo());
		args_.push_back(value_rescale->out_hi());
	}
}

char** SplitArgs::get()
{
	assert(!args_.empty());

	raw_args_.clear();

	raw_args_.resize(args_.size() + 1);
	std::transform(begin(args_), end(args_), begin(raw_args_), [](auto& s){ return s.data(); });
	raw_args_.back() = nullptr;
	
	return raw_args_.data();
}

void SplitArgs::push_back(double val) 
{
	str_.str("");
	str_ << val;
	args_.push_back(str_.str());
}

// --- FeatureFieldInfo class ---

FeatureFieldInfo::FeatureFieldInfo(std::string name, FeatureFieldType type)
	: name_{move(name)}
	, type_{type}
{
	TWIST_CHECK_INVARIANT
}


std::string FeatureFieldInfo::name() const
{
	TWIST_CHECK_INVARIANT
	return name_;
}


FeatureFieldType FeatureFieldInfo::type() const
{
	TWIST_CHECK_INVARIANT
	return type_;
}


#ifdef _DEBUG
void FeatureFieldInfo::check_invariant() const noexcept
{
	assert(!is_whitespace(name_));
	assert(is_defined(type_));
	
}
#endif

// --- GdalInitialiser class ---

GdalInitialiser::GdalInitialiser()
{
	gdal_initialise();
}

GdalInitialiser::~GdalInitialiser()
{
	gdal_uninitialise();
}

// --- Global functions ---

auto gdal_initialise() -> void
{
	// CPLPushErrorHandler can be used if registering different handlers
    CPLSetErrorHandler(&CplErrorHandler::cpl_error_handler);
	GDALAllRegister(); 
}

auto gdal_uninitialise() -> void
{
	// CPLPopErrorHandler() can be used if un-registring different handlers
	GDALDestroyDriverManager();
    CPLSetErrorHandler(nullptr);
}

auto gdal_push_warning_listener(GdalWarningListener listener) -> void
{
	CplErrorHandler::push_warning_listener(move(listener));
}

auto refsys_from_prj_file(const fs::path& path) -> SpatialReferenceSystem
{
	if (!exists(path)) {
		TWIST_THRO2(L"ESRI prj file not found at \"{}\"", path.c_str());
	}

	auto** file_buffer = CSLLoad(path.string().c_str());
	scope(exit) { CSLDestroy(file_buffer); }; 

	auto spatial_ref = SpatialReferenceSystem{};
	CHECK_OGRERR_EX(spatial_ref.get().importFromESRI(file_buffer),
			        std::format(L"Error reading ESRI prj file \"{}\"", path.c_str()).c_str()); 

	return spatial_ref;
}

auto refsys_from_wkt_string(const char* prj_str) -> SpatialReferenceSystem
{
	auto lines = CPLStringList{};
	lines.AddString(prj_str);

	auto spatial_ref = SpatialReferenceSystem{};
	CHECK_OGRERR_EX(spatial_ref.get().importFromESRI(lines),
			        format_str(L"Error reading ESRI prj string \"%s\"", prj_str).c_str()); 

	return spatial_ref;
}

auto refsys_from_known(KnownGeoRefsysId known_refsys_id) -> SpatialReferenceSystem
{
	return refsys_from_wkt_string(to_wkt_string(known_refsys_id));
}

auto find_matching_known_geo_refsys(const SpatialReferenceSystem& refsys) -> std::optional<KnownGeoRefsysId>
{
	const auto refsys_ids = all_known_geo_refsys_ids();
	if (auto it = rg::find_if(refsys_ids, [&refsys](auto id) { return match(refsys, id); }); 
	         it != end(refsys_ids)) {
		return *it;
	}
	return std::nullopt;
}

auto match(const SpatialReferenceSystem& refsys, KnownGeoRefsysId known_refsys_id) -> bool
{
	static const double tolerance = 10E-6;

	bool ret = true;

	auto match_attr = [&refsys](auto name, int attr_idx, auto value) {
		const auto value_read = refsys.get_attr_value(name, attr_idx);
		return wequal_no_case(value_read, value);
	};
	auto match_float_attr = [&refsys](auto name, int attr_idx, auto value) {
		return equal_tol(refsys.get_float_attr_value(name, attr_idx), value, tolerance);
	};
	auto match_param = [&refsys](auto name, auto value) {
		const auto refsys_value = refsys.get_proj_parm(name);
		if (!refsys_value) {
			TWIST_THROW(L"Could not read reference system parameter \"%s\".", name);
		}
		return equal_tol(*refsys_value, value, tolerance);
	};

	const auto pretty_wtk = refsys.export_to_pretty_wtk();
	
	switch (known_refsys_id) {
	case KnownGeoRefsysId::sa_lambert94: 
		//ret = match_attr(L"GEOGCS", 0, L"GCS_GDA_1994") && ret; 
		ret = (match_attr(L"GEOGCS|DATUM", 0, L"Geocentric_Datum_of_Australia_1994") ||
			   match_attr(L"GEOGCS|DATUM", 0, L"GDA_1994")) && ret;
		ret = match_attr(L"GEOGCS|DATUM|SPHEROID", 0, L"GRS_1980") && ret;
		ret = match_attr(L"PROJECTION", 0, L"Lambert_Conformal_Conic_2SP") && ret;
		ret = match_attr(L"UNIT", 0, L"metre") && ret;
		ret = match_float_attr(L"UNIT", 1, 1.0) && ret;
		ret = match_param(L"False_Easting", 1000000.0) && ret;
		ret = match_param(L"False_Northing", 2000000.0) && ret;
		ret = match_param(L"Central_Meridian", 135.0) && ret;
		ret = match_param(L"Standard_Parallel_1", -28.0) && ret;
		ret = match_param(L"Standard_Parallel_2", -36.0) && ret;
		ret = match_param(L"Latitude_Of_Origin", -32.0) && ret;
		break;

	case KnownGeoRefsysId::vicgrid94: 
		//+TODO: Remove the commented out values when completely comfortable with the new GDAL.
		//       Those values worked before the GDAL upgrade from 2.2.2 to 3.8.0. DAA 9Oct'23
	 // ret = match_attr(L"GEOGCS", 0, L"GCS_GDA_1994") && ret; 
		ret = match_attr(L"GEOGCS", 0, L"GDA94") && ret;
		ret = (match_attr(L"GEOGCS|DATUM", 0, L"Geocentric_Datum_of_Australia_1994") ||
			   match_attr(L"GEOGCS|DATUM", 0, L"GDA_1994")) && ret;
	 // ret = match_attr(L"GEOGCS|DATUM|SPHEROID", 0, L"GRS_1980") && ret; 
		ret = match_attr(L"GEOGCS|DATUM|SPHEROID", 0, L"GRS 1980") && ret;
		ret = match_attr(L"PROJECTION", 0, L"Lambert_Conformal_Conic_2SP") && ret;
	 // ret = match_attr(L"UNIT", 0, L"Meter") && ret;
		ret = match_attr(L"UNIT", 0, L"metre") && ret;
		ret = match_float_attr(L"UNIT", 1, 1.0) && ret;
		ret = match_param(L"False_Easting", 2500000.0) && ret;
		ret = match_param(L"False_Northing", 2500000.0) && ret;
		ret = match_param(L"Central_Meridian", 145.0) && ret;
		ret = match_param(L"Standard_Parallel_1", -36.0) && ret;
		ret = match_param(L"Standard_Parallel_2", -38.0) && ret;
		ret = match_param(L"Latitude_Of_Origin", -37.0) && ret;
		break;

	case KnownGeoRefsysId::nsw_lambert94: 
		ret = match_attr(L"GEOGCS", 0, L"GCS_GDA_1994") && ret;
		ret = (match_attr(L"GEOGCS|DATUM", 0, L"Geocentric_Datum_of_Australia_1994") ||
			   match_attr(L"GEOGCS|DATUM", 0, L"GDA_1994")) && ret;		
		ret = match_attr(L"GEOGCS|DATUM|SPHEROID", 0, L"GRS_1980") && ret;
		ret = match_attr(L"PROJECTION", 0, L"Lambert_Conformal_Conic_2SP") && ret;
		ret = match_attr(L"UNIT", 0, L"Meter") && ret;
		ret = match_float_attr(L"UNIT", 1, 1.0) && ret;
		ret = match_param(L"False_Easting", 9300000.0) && ret;
		ret = match_param(L"False_Northing", 4500000.0) && ret;
		ret = match_param(L"Central_Meridian", 147.0) && ret;
		ret = match_param(L"Standard_Parallel_1", -30.75) && ret;
		ret = match_param(L"Standard_Parallel_2", -35.75) && ret;
		ret = match_param(L"Latitude_Of_Origin", -33.25) && ret;
		break;

	case KnownGeoRefsysId::mga_zone_55_94:
		ret = match_attr(L"GEOGCS", 0, L"GCS_GDA_1994") && ret;
		ret = (match_attr(L"GEOGCS|DATUM", 0, L"Geocentric_Datum_of_Australia_1994") ||
			   match_attr(L"GEOGCS|DATUM", 0, L"GDA_1994")) && ret;
		ret = match_attr(L"GEOGCS|DATUM|SPHEROID", 0, L"GRS_1980") && ret;
		ret = match_attr(L"PROJECTION", 0, L"Transverse_Mercator") && ret;
		ret = match_attr(L"UNIT", 0, L"Meter") && ret;
		ret = match_float_attr(L"UNIT", 1, 1.0) && ret;
		ret = match_param(L"False_Easting", 500000.0) && ret;
		ret = match_param(L"False_Northing", 10000000.0) && ret;
		ret = match_param(L"Central_Meridian", 147.0) && ret;
		ret = match_param(L"Latitude_Of_Origin", 0.0) && ret;
		break;

	case KnownGeoRefsysId::wgs84: //+TODO: Implement
		TWIST_THRO2(L"Matching not yet implemented for \"known\" geographic reference system \"{}\".", 
		            as_long_name(known_refsys_id, false/*incl_epsg*/));

	default: 
		TWIST_THRO2(L"Invalid \"known\" geographic reference system ID {}.", static_cast<int>(known_refsys_id));
	}
	return ret;
}

auto refsys_from_well_known_name(const std::wstring& name)-> SpatialReferenceSystem
{
	auto spatial_ref = SpatialReferenceSystem{};
	CHECK_OGRERR_EX(spatial_ref.get().SetWellKnownGeogCS(string_to_ansi(name).c_str()),
			        std::format(L"Error setting geographic coordinate system with name \"{}\"", name).c_str());
	return spatial_ref;
}

auto refsys_from_epsg(int epsg) -> SpatialReferenceSystem
{
	auto spatial_ref = SpatialReferenceSystem{};
	CHECK_OGRERR_EX(spatial_ref.get().importFromEPSG(epsg),
			        std::format(L"Error setting geographic coordinate system with EPSG {}", epsg).c_str());
	return spatial_ref;
}

auto refsys_from_wgs84() -> SpatialReferenceSystem
{
	return refsys_from_well_known_name(L"WGS84");
}

auto make_wgs84_coord_projector(KnownGeoRefsysId known_refsys_id) -> std::unique_ptr<GeoCoordProjector>
{
	return std::make_unique<GeoCoordProjectorGdal>(refsys_from_wgs84(), refsys_from_known(known_refsys_id));
}

auto make_wgs84_coord_projector(const fs::path& prj_path) -> std::unique_ptr<GeoCoordProjector>
{
	return std::make_unique<GeoCoordProjectorGdal>(refsys_from_wgs84(), refsys_from_prj_file(prj_path));
}

auto transform(const GeoPoint& pt, const SpatialReferenceSystem& src_ref, 
		       const SpatialReferenceSystem& dest_ref) -> GeoPoint
{
	auto coord_trans = std::unique_ptr<OGRCoordinateTransformation>{
			                   OGRCreateCoordinateTransformation(&src_ref.get(), &dest_ref.get())};
	if (!coord_trans) {
		TWIST_THRO2(L"Failed to create coordinate transformation: {}", get_last_cpl_error_msg());
	}
	auto x = pt.x();
	auto y = pt.y();
	if (coord_trans->Transform(1, &x, &y) != 1) {
		TWIST_THRO2(L"Failed to apply coordinate transformation.");		
	}
	return GeoPoint{x, y};
}


RasterDatasetInfo read_raster_info(const fs::path& raster_file_path)
{
    auto dset = gdal_open(raster_file_path, true/*read_only*/);
	scope(exit) { gdal_close(dset); };

	auto raster_dset_info = RasterDatasetInfo{};
	raster_dset_info.driver_name = ansi_to_string(dset->GetDriver()->GetDescription());
	raster_dset_info.driver_long_name = ansi_to_string(dset->GetDriver()->GetMetadataItem(GDAL_DMD_LONGNAME));
	raster_dset_info.width = dset->GetRasterXSize();
	raster_dset_info.height = dset->GetRasterYSize();
	raster_dset_info.band_count = dset->GetRasterCount();
	if (dset->GetProjectionRef() != nullptr) {
		raster_dset_info.projection = ansi_to_string(dset->GetProjectionRef());
	}
	double geo_transforms[6];
	if (dset->GetGeoTransform(geo_transforms) == CE_None) {
	   raster_dset_info.geo_info = {{geo_transforms[0], geo_transforms[3]}, {geo_transforms[1], geo_transforms[5]}};
	}	

	for (auto b : IndexRange{1, raster_dset_info.band_count + 1}) {
		
		auto& band_info = raster_dset_info.bands_info[b];

		auto band = gsl::make_not_null(dset->GetRasterBand(b));

		band_info.width = band->GetXSize();
		band_info.height = band->GetYSize();

		band->GetBlockSize(&band_info.block_width, &band_info.block_height);
		band_info.data_value_type = geo_grid_data_value_type(band->GetRasterDataType());
		//band_info.colour_interpretation = f(band->GetColorInterpretation());
		band_info.colour_interpretation_name = ansi_to_string(
				GDALGetColorInterpretationName(band->GetColorInterpretation()));

		double min_max_values[] = {0, 0};
		auto got_min = int{FALSE};
		auto got_max = int{FALSE};
		min_max_values[0] = band->GetMinimum(&got_min);
		min_max_values[1] = band->GetMaximum(&got_max);
		if (!got_min || !got_max) {
			[[maybe_unused]] const auto result = band->ComputeRasterMinMax(FALSE/*bApproxOK*/, min_max_values);
			assert(result == CE_None);	
		}
		band_info.min_value = min_max_values[0];
		band_info.max_value = min_max_values[1];

		band_info.overview_count = band->GetOverviewCount();

		if (auto* colour_table = band->GetColorTable(); colour_table) {
			band_info.colour_table_entry_count = colour_table->GetColorEntryCount();
		}
		else {
			band_info.colour_table_entry_count = 0;
		}

		auto got_no_data_value = 0;
		if (auto no_data_value = band->GetNoDataValue(&got_no_data_value); got_no_data_value == TRUE) {
			band_info.no_data_value = no_data_value;
		}
	 }	

	return raster_dset_info;
}

auto get_geo_extent(const RasterDatasetInfo& raster_info) -> GeoRectangle
{
	if (!raster_info.geo_info) {
		TWIST_THROW(L"The raster information contains no geographic information.");
	}
	
	const auto& geo_info = *raster_info.geo_info;
	if (geo_info.pixel_size.x() < 0 || geo_info.pixel_size.y() > 0) {
		TWIST_THROW(L"Raster origin is not the top-left.");
	}

	return {geo_info.origin.x(), 
			geo_info.origin.y() - raster_info.height * abs(geo_info.pixel_size.y()), 
			geo_info.origin.x() + raster_info.width * geo_info.pixel_size.x(), 
			geo_info.origin.y()};
}

auto get_geo_space_grid(const RasterDatasetInfo& raster_info) -> SquareGeoSpaceGrid
{
	if (!raster_info.geo_info) {
		TWIST_THROW(L"The raster information contains no geographic information.");
	}
	
	const auto& geo_info = *raster_info.geo_info;
	if (abs(geo_info.pixel_size.x()) != abs(geo_info.pixel_size.y())) {
		TWIST_THROW(L"The raster pixels are not square.");
	}
	if (geo_info.pixel_size.x() < 0 || geo_info.pixel_size.y() > 0) {
		TWIST_THROW(L"Raster origin is not the top-left.");
	}

	const double pixel_size = geo_info.pixel_size.x();

	return {geo_info.origin.x(), 
			geo_info.origin.y() - raster_info.height * pixel_size, 
			pixel_size, 
			raster_info.height, 
			raster_info.width};
}

auto rasterise_vector(const fs::path& in_path, 
                      const fs::path& out_path, 
					  const std::vector<std::string>& in_layer_names, 
					  std::string in_attr_name, 
					  const SquareGeoSpaceGrid& out_grid, 
					  GeoGridDataType out_data_type, 
					  std::optional<std::string> nodata_value,
					  std::optional<RasterFileFormat> out_format) -> void
{
	assert(!in_layer_names.empty());
	assert(!in_attr_name.empty());

	// https://svn.osgeo.org/gdal/trunk/gdal/apps/gdal_rasterize_bin.cpp

	auto in_dset = GDALOpenEx(
			in_path.string().c_str(), GDAL_OF_VECTOR | GDAL_OF_READONLY | GDAL_OF_VERBOSE_ERROR,
			nullptr/*allowed_drivers*/, nullptr/*open_options*/, nullptr/*sibling_files*/);

	SplitArgs args;
	args.add("-l", in_layer_names);
	args.add("-a", move(in_attr_name));
	args.add_lu("-te", perimeter_rect(out_grid));
	args.add("-of", out_format);
	args.add("-tr", GeoSize{out_grid.cell_size(), out_grid.cell_size()});
	args.add("-ot", out_data_type);
	args.add("-a_nodata", nodata_value);

    auto options = GDALRasterizeOptionsNew(args.get(), nullptr/*options_for_binary*/);
    int usage_error = 0;

	auto dest_dset = GDALRasterize(
			out_path.string().c_str(), nullptr/*h_dest_ds*/, in_dset, options, &usage_error);

    GDALRasterizeOptionsFree(options);

    gdal_close(dest_dset);
    gdal_close(in_dset);

	if (usage_error != 0) {
		TWIST_THROW(L"GDALRasterize() reported usage error.");
	}
}

auto polygonise_raster(const fs::path& in_path, const fs::path& out_path, FeatureFieldInfo out_field_info) 
      -> void
{
	if (exists(out_path) && !is_empty(out_path)) {
		TWIST_THROW(L"The output directory \"%s\" exists and is not empty.", out_path.stem().c_str());
	}

	const auto in_dset = gdal_open(in_path, true/*read_only*/);
	scope(exit) { gdal_close(in_dset); };

	auto driver = get_gdal_shapefile_driver();

	const auto out_dset = not_null{driver->Create(out_path.string().c_str(), 
	                                              in_dset->GetRasterXSize(), 
												  in_dset->GetRasterYSize(), 
			                                      in_dset->GetRasterCount(), 
												  GDT_Unknown, 
												  nullptr/*options*/)};
	scope(exit) { gdal_close(out_dset); };
	
	auto out_layer = not_null{out_dset->CreateLayer(
			out_path.stem().string().c_str(), nullptr/*spatial_ref*/, wkbMultiPolygon, nullptr/*options*/)};

	auto field_defn = OGRFieldDefn{out_field_info.name().c_str(), to_gdal(out_field_info.type())};
	CHECK_OGRERR( out_layer->CreateField(&field_defn) );

	const auto in_raster_band = not_null{in_dset->GetRasterBand(1)};

	GDALPolygonize(in_raster_band, nullptr/*mask_band*/, out_layer, 0/*i_pix_val_field*/, nullptr/*options*/, 
			nullptr/*progress*/, nullptr/*progress_arg*/);  
	
	CHECK_OGRERR( out_layer->SyncToDisk() );	
}

auto polygonise_raster(const fs::path& in_path, const fs::path& out_path, 
		               FeatureFieldInfo out_field1_info, FeatureFieldInfo out_field2_info) -> void
{
	if (exists(out_path) && !is_empty(out_path)) {
		TWIST_THROW(L"The output directory \"%s\" exists and is not empty.", out_path.stem().c_str());
	}

	const auto in_dset = gdal_open(in_path, true/*read_only*/);
	scope(exit) { gdal_close(in_dset); };

	auto driver = get_gdal_shapefile_driver();

	const auto out_dset = not_null{driver->Create(
			out_path.string().c_str(), in_dset->GetRasterXSize(), in_dset->GetRasterYSize(), 
			in_dset->GetRasterCount(), GDT_Unknown, nullptr/*options*/)};
	scope(exit) { gdal_close(out_dset); };
	
	auto out_layer = not_null{out_dset->CreateLayer(
			out_path.stem().string().c_str(), nullptr/*spatial_ref*/, wkbMultiPolygon, nullptr/*options*/)};

	auto field1_defn = OGRFieldDefn{out_field1_info.name().c_str(), to_gdal(out_field1_info.type())};
	CHECK_OGRERR(out_layer->CreateField(&field1_defn));

	auto field2_defn = OGRFieldDefn{out_field2_info.name().c_str(), to_gdal(out_field2_info.type())};
	CHECK_OGRERR(out_layer->CreateField(&field2_defn));

	const auto in_raster_band = not_null{in_dset->GetRasterBand(1)};

	GDALPolygonize(in_raster_band, nullptr/*mask_band*/, out_layer, 0/*i_pix_val_field*/, nullptr/*options*/, 
			       nullptr/*progress*/, nullptr/*progress_arg*/);  
	
	CHECK_OGRERR(out_layer->SyncToDisk());	
}

auto translate_raster(const fs::path& in_path, 
                      const fs::path& out_path, 
		              RasterFileFormat out_format, 
		              std::optional<GeoSpaceGrid> out_space_grid,
					  std::optional<GeoGridDataType> out_data_type, 
		              std::optional<RasterResampleAlgorithm> resample_algo,
					  std::optional<RasterValueRescale> value_rescale,
					  std::optional<Ssize> geotiff_nof_cells_per_tile,
					  std::optional<GeotiffCompression> geotiff_compression,
					  bool geotiff_bigtiff) -> void
{
	// http://www.gdal.org/gdal_tutorial.html
	// http://www.gdal.org/gdal__utils_8h.html#a1cf5b30de14ccaf847ae7a77bb546b28
	// https://lists.osgeo.org/pipermail/gdal-dev/2015-November/043080.html

	if (geotiff_nof_cells_per_tile) {
		if (!is_multiple(*geotiff_nof_cells_per_tile, 256) || !is_perfect_square(*geotiff_nof_cells_per_tile)) {
			TWIST_THRO2(L"The number of cells per tile {} must be a multiple of 256 and a perfect square.",
			            *geotiff_nof_cells_per_tile);
		} 
	}

	if (exists(out_path)) {
		TWIST_THRO2(L"Output file \"{}\" already exists.", out_path.filename().c_str());
	}
	create_directories(out_path.parent_path());

	auto src_dset = (GDALDataset*)nullptr;
	auto dest_dset = (GDALDataset*)nullptr;
    auto options = (GDALTranslateOptions*)nullptr;
	scope(exit) {
		if (dest_dset) {
			gdal_close(dest_dset);
		}
		if (src_dset) {
			gdal_close(src_dset);
		}		
		if (options) {
			GDALTranslateOptionsFree(options);
		}					
	};

	auto args = SplitArgs{};
	args.add("-of", out_format);
	args.add("-ot", out_data_type);
	if (out_space_grid) {
		args.add_ul("-projwin", perimeter_rect(*out_space_grid));
		args.add("-tr", GeoSize{out_space_grid->cell_width(), out_space_grid->cell_height()});
	}
	args.add("-scale", value_rescale);
	args.add("-r", resample_algo);
	if (geotiff_nof_cells_per_tile) {
		const auto nof_cells_per_tile_side = perfect_square_root(*geotiff_nof_cells_per_tile);
		args.add("-co", "TILED=YES");
		args.add("-co", std::format("BLOCKXSIZE={}", nof_cells_per_tile_side));
		args.add("-co", std::format("BLOCKYSIZE={}", nof_cells_per_tile_side));
	}
	if (geotiff_compression == GeotiffCompression::deflate) {
		args.add("-co", "COMPRESS=DEFLATE"); 
		args.add("-co", "PREDICTOR=2"); 
			//+TODO: Only supported for 8, 16, 32 bit samples (support for 64 bit was added in libtiff > 4.3.0).
			//       Without this option, "gdal_translate" outputs messages of the form "bit length overflow code 0 
			//       bits 6->7". 11Sep'23
		args.add("-co", "ZLEVEL=9"); 
	}
	if (geotiff_bigtiff) {
		args.add("-co", "BIGTIFF=YES"); 
	}

	options = GDALTranslateOptionsNew(args.get(), nullptr/*options_for_binary*/);

	src_dset = gdal_open(in_path, true/*read_only*/);
	dest_dset = static_cast<GDALDataset*>(GDALTranslate(out_path.string().c_str(), 
	                                                    src_dset, 
														options, 
														nullptr/*usage_error*/));
	if (!dest_dset) {
		TWIST_THRO2(L"GDALTranslate failed for file \"{}\". ", in_path.filename().c_str());
	}
}

auto warp_raster(const fs::path& in_path, 
				 const fs::path& out_path,
				 std::optional<RasterFileFormat> out_format, 
				 std::optional<std::string> out_nodata_val,
				 std::optional<SquareGeoSpaceGrid> out_space_grid, 
				 std::optional<GeoGridDataType> out_data_type, 
				 std::optional<RasterResampleAlgorithm> resample_algo,
                 std::optional<Ssize> geotiff_nof_cells_per_tile,
                 std::optional<GeotiffCompression> geotiff_compression) -> void
{
	check_file_extension_match_raster_format(out_path, out_format);
	auto args = SplitArgs{};
	args.add("-of", out_format);
	args.add("-dstnodata", out_nodata_val);
	if (out_space_grid) {
		args.add_lu("-te", perimeter_rect(*out_space_grid));
		args.add("-tr", GeoSize{out_space_grid->cell_size(), out_space_grid->cell_size()});
	}
	args.add("-ot", out_data_type);
	args.add("-r", resample_algo);
	if (geotiff_nof_cells_per_tile) {
		const auto nof_cells_per_tile_side = perfect_square_root(*geotiff_nof_cells_per_tile);
		args.add("-co", "TILED=YES");
		args.add("-co", std::format("BLOCKXSIZE={}", nof_cells_per_tile_side));
		args.add("-co", std::format("BLOCKYSIZE={}", nof_cells_per_tile_side));
	}
	if (geotiff_compression == GeotiffCompression::deflate) {
		args.add("-co", "COMPRESS=DEFLATE"); 
		args.add("-co", "PREDICTOR=2"); 
			//+TODO: Only supported for 8, 16, 32 bit samples (support for 64 bit was added in libtiff > 4.3.0).
			//       Without this option, "gdal_translate" outputs messages of the form "bit length overflow code 0 
			//       bits 6->7". 11Sep'23
		args.add("-co", "ZLEVEL=9"); 
	}

	auto src_dset = (GDALDataset*)nullptr;
	auto dest_dset = (GDALDataset*)nullptr;
    auto options = (GDALWarpAppOptions*)nullptr;
	scope(exit) {
		if (dest_dset) {
			gdal_close(dest_dset);
		}
		if (src_dset) {
			gdal_close(src_dset);	
		}
		if (options) {
			GDALWarpAppOptionsFree(options);
		}
	};

	options = GDALWarpAppOptionsNew(args.get(), nullptr/*options_for_binary*/);

    src_dset = gdal_open(in_path, true/*read_only*/);

	auto src_dset_hnd = static_cast<GDALDatasetH>(src_dset);
	dest_dset = static_nonull_cast<GDALDataset*>(GDALWarp(out_path.string().c_str(), 
												 nullptr/*h_dst_ds*/, 
												 1/*src_count*/,
												 &src_dset_hnd, 
												 options, 
												 nullptr/*usage_error*/));	
}

auto is_defined(FeatureFieldType type) -> bool
{
	return type >= FeatureFieldType::int32 && type <= FeatureFieldType::date_time;
}

auto get_last_cpl_error_msg() -> std::wstring
{
	return ansi_to_string(CPLGetLastErrorMsg());
}

// --- RasterValueRescale class ---

auto RasterValueRescale::in_lo() const -> std::string
{
	TWIST_CHECK_INVARIANT
	return in_lo_;
}

auto RasterValueRescale::in_hi() const -> std::string
{
	TWIST_CHECK_INVARIANT
	return in_hi_;
}

auto RasterValueRescale::out_lo() const -> std::string
{
	TWIST_CHECK_INVARIANT
	return out_lo_;
}

auto RasterValueRescale::out_hi() const -> std::string
{
	TWIST_CHECK_INVARIANT
	return out_hi_;
}

#ifdef _DEBUG
auto RasterValueRescale::check_invariant() const noexcept -> void
{
	assert(!in_lo_.empty());
	assert(!in_hi_.empty());
	assert(!out_lo_.empty());
	assert(!out_hi_.empty());
}
#endif

}

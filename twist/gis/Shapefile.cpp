/// @file Shapefile.cpp
/// Implementation file for "Shapefile.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/gis/Shapefile.hpp"

#include "twist/cast.hpp"
#include "twist/file_io.hpp"
#include "twist/Overload.hpp"
#include "twist/scope.hpp"
#include "twist/std_vector_utils.hpp"
#include "twist/gis/internal_gdal_utils.hpp"

#include "gdal/include/ogrsf_frmts.h"

#include <cinttypes>

using gsl::not_null;

namespace twist::gis {

// --- Shapefile::Impl class ---

class Shapefile::Impl {
public:
	explicit Impl(not_null<GDALDataset*> dataset)
		: dataset_{dataset}
		, layer_{dataset->GetLayer(0)}
	{
	}

	~Impl()
	{
		gdal_close(dataset_);
	}

	[[nodiscard]] auto dataset() -> GDALDataset&
	{
		return *dataset_;
	}

	[[nodiscard]] auto layer() -> OGRLayer&
	{
		return *layer_;
	}

	[[nodiscard]] auto add_feature(std::unique_ptr<OGRGeometry> geometry) -> Ssize
	{
		auto feature = not_null{OGRFeature::CreateFeature(layer_->GetLayerDefn())};
		CHECK_OGRERR(feature->SetGeometryDirectly(geometry.release()));
		CHECK_OGRERR(layer_->CreateFeature(feature)); //+TODO: Does this take ownership or not?
		return feature->GetFID();
	}

	[[nodiscard]] static auto get_polygons(not_null<OGRFeature*> feature, MultiPolygonOption multipolygon_option) 
                               -> std::vector<GeoPolygon>
	{
		auto geometry = not_null{feature->GetGeometryRef()};
        const auto geometry_type = geometry->getGeometryType();
        if (geometry_type == OGRwkbGeometryType::wkbMultiPolygon) {
            if (multipolygon_option == MultiPolygonOption::error) {
                TWIST_THRO2 (L"Feature with ID {} is a multi-polygon, which is not allowed.", feature->GetFID());
            }
            if (multipolygon_option == MultiPolygonOption::skip) {
                return {};
            }
        }
        if (geometry_type != OGRwkbGeometryType::wkbPolygon && geometry_type != OGRwkbGeometryType::wkbMultiPolygon) {
            TWIST_VTHRO2(multipolygon_option == MultiPolygonOption::error
                                 ? L"Feature with ID {} is not a polygon."
                                 : L"Feature with ID {} is neither a polygon nor a multi-polygon.",
                         feature->GetFID());            
        }

        auto results = std::vector<GeoPolygon>{};
        auto add_poly_to_results = [&results](not_null<OGRPolygon*> gdal_poly) {
		    auto outer_ring = from_gdal(gdal_poly->getExteriorRing());
		    auto inner_rings = std::vector<GeoLinearRing>{};
		    const auto nof_inner_rings = gdal_poly->getNumInteriorRings();
		    if (nof_inner_rings > 0) {
			    inner_rings.reserve(nof_inner_rings);
			    for (auto i : IndexRange{nof_inner_rings}) {
				    inner_rings.push_back(from_gdal(gdal_poly->getInteriorRing(i)));
			    }
		    }
		    results.emplace_back(std::move(outer_ring), move(inner_rings));
        };

		if (geometry_type == OGRwkbGeometryType::wkbPolygon) {
		    auto gdal_poly = dyn_nonull_cast<OGRPolygon*>(geometry);
            add_poly_to_results(gdal_poly);
        }
        else if (geometry_type == OGRwkbGeometryType::wkbMultiPolygon) {
		    auto gdal_multipoly = dyn_nonull_cast<OGRMultiPolygon*>(geometry);
            results.reserve(gdal_multipoly->getNumGeometries());
            for (auto child_gdal_poly : *gdal_multipoly) {
                add_poly_to_results(child_gdal_poly);
            }
        }

		return results;
	}

private:
	not_null<GDALDataset*> dataset_;
	not_null<OGRLayer*> layer_;
};

// --- Shapefile class ---

Shapefile::Shapefile(CreateMode /*tag*/, 
                     const fs::path& shapefile_dir_path, 
					 VectorGeometryType layer_geom_type,
					 std::optional<SpatialReferenceSystem> layer_refsys)
{
	const auto def_layer_name = shapefile_dir_path.filename().wstring();
	path_ = shapefile_dir_path / (def_layer_name + L".shp");

	if (exists(path_)) {
		TWIST_THRO2(L"File \"{}\" already exists.", path_.c_str());
	}
	create_directories(shapefile_dir_path.parent_path());

	// https://gdal.org/tutorials/vector_api_tut.html
	
	auto driver = get_gdal_shapefile_driver();
	const auto dset = not_null{driver->Create(shapefile_dir_path.string().c_str(), 
	                                          0/*x_size*/, 
											  0/*y_size*/, 
			                                  0/*bands*/, 
											  GDT_Unknown, 
											  nullptr/*options*/)};
    dset->CreateLayer(string_to_ansi(def_layer_name).c_str(), 
	                  layer_refsys ? std::addressof(layer_refsys->get()) : nullptr, 
					  to_gdal(layer_geom_type), 
					  nullptr/*options*/);

	impl_ = std::make_unique<Impl>(dset);
	TWIST_CHECK_INVARIANT
}

Shapefile::Shapefile(OpenMode /*tag*/, fs::path path, bool read_only)
{
	const auto access_flags = read_only ? GDAL_OF_READONLY | GDAL_OF_VERBOSE_ERROR 
                                        : GDAL_OF_UPDATE | GDAL_OF_VERBOSE_ERROR;

    auto* dset = static_cast<GDALDataset*>(GDALOpenEx(path.string().c_str(),
                                                      GDAL_OF_VECTOR | access_flags,
                                                      nullptr/*allowed_drivers*/,
                                                      nullptr/*open_options*/,
                                                      nullptr/*sibling_files*/));
    if (!dset) {
        TWIST_THRO2(L"Error opening shapefile \"{}\": {}", path.filename().c_str(), get_last_cpl_error_msg());
    }

	path_ = std::move(path);
	impl_ = std::make_unique<Impl>(dset);
	TWIST_CHECK_INVARIANT
}

Shapefile::~Shapefile()
{
	TWIST_CHECK_INVARIANT
}

auto Shapefile::has_point_geometry() const -> bool
{
	TWIST_CHECK_INVARIANT
	return impl_->layer().GetGeomType() == wkbPoint;
}

auto Shapefile::has_line_string_geometry() const -> bool
{
	TWIST_CHECK_INVARIANT
	return impl_->layer().GetGeomType() == wkbLineString;
}

auto Shapefile::has_polygon_geometry() const -> bool
{
	TWIST_CHECK_INVARIANT
	return impl_->layer().GetGeomType() == wkbPolygon;
}

auto Shapefile::add_feature(GeoPoint point) -> Ssize
{
	TWIST_CHECK_INVARIANT
	return impl_->add_feature(to_gdal(point));
}

auto Shapefile::add_feature(const GeoLineString& line_string) -> Ssize
{
	TWIST_CHECK_INVARIANT
	return impl_->add_feature(to_gdal(line_string));
}

auto Shapefile::add_feature(const GeoPolygon& polygon) -> Ssize
{
	TWIST_CHECK_INVARIANT
	return impl_->add_feature(to_gdal(polygon));
}

auto Shapefile::add_attribute_column(FeatureFieldInfo field_info) -> void
{
	TWIST_CHECK_INVARIANT
	auto field_defn = OGRFieldDefn{field_info.name().c_str(), to_gdal(field_info.type())};
	CHECK_OGRERR(impl_->layer().CreateField(&field_defn));
}

auto Shapefile::nof_features(bool force) const -> std::optional<Ssize>
{
	TWIST_CHECK_INVARIANT
	if (const auto count = impl_->layer().GetFeatureCount(force ? 1 : 0); count != -1) {
		return count;
	}
	return std::nullopt;
}

auto Shapefile::read_feature_as_polygon(Ssize feature_id) const -> GeoPolygon
{
	TWIST_CHECK_INVARIANT	
	if (!has_polygon_geometry()) {
		TWIST_THROW(L"The active layer is not a \"polygon\" layer.");
	}

	auto feature = impl_->layer().GetFeature(feature_id);
	if (!feature) {
		TWIST_THROW(L"Feature with ID " PRId64 " not found in the active layer.");
	}
	auto polygons = Impl::get_polygons(feature, MultiPolygonOption::error);
    assert(ssize(polygons) == 1);
    return polygons.front();
}

auto Shapefile::read_all_features_as_polygons(MultiPolygonOption multipolygon_option) const -> std::vector<GeoPolygon>
{
	TWIST_CHECK_INVARIANT	
	if (!has_polygon_geometry()) {
		TWIST_THROW(L"The active layer is not a \"polygon\" layer.");
	}

	auto ret = reserve_vector<GeoPolygon>(impl_->layer().GetFeatureCount());

    reset_reading();
	auto feature = (OGRFeature*)nullptr;
    while ((feature = impl_->layer().GetNextFeature()) != nullptr) {
		ret.append_range(Impl::get_polygons(feature, multipolygon_option));
    }

	return ret;
}

auto Shapefile::set_feature_attribute_value(Ssize feature_id, const std::string& col_name, const AttributeValue& value) 
                 -> void
{
	TWIST_CHECK_INVARIANT
	std::visit(
		Overload{[&](int arg) { set_feature_attribute_value_impl(feature_id, col_name, arg); },
				 [&](const std::string& arg) { set_feature_attribute_value_impl(feature_id, col_name, arg.c_str()); }}, 
		value);
}

auto Shapefile::set_known_projection(KnownGeoRefsysId refsys_id) -> void
{
	TWIST_CHECK_INVARIANT
	const auto prj_file_path = path_.parent_path() / (path_.stem().wstring() + L".prj");
	auto stream = open_text_file_for_writing<char>(prj_file_path, true/*override*/);
	stream << to_wkt_string(refsys_id);
}

auto Shapefile::save() -> void
{
	TWIST_CHECK_INVARIANT
	CHECK_OGRERR(impl_->layer().SyncToDisk());
}

auto Shapefile::reset_reading() const -> void
{
	TWIST_CHECK_INVARIANT
	impl_->layer().ResetReading();
}

auto Shapefile::get_next_feature() const -> void*
{
	TWIST_CHECK_INVARIANT
	return impl_->layer().GetNextFeature();
}

auto Shapefile::get_polygons(not_null<void*> feature, MultiPolygonOption multipolygon_option) 
      -> std::vector<GeoPolygon>
{
	return Impl::get_polygons(static_cast<OGRFeature*>(feature.get()), multipolygon_option);
}

template<class Value>
auto Shapefile::set_feature_attribute_value_impl(Ssize feature_id, const std::string& col_name, Value value) -> void
{
	TWIST_CHECK_INVARIANT
	auto feature = not_null{impl_->layer().GetFeature(feature_id)};
	feature->SetField(col_name.c_str(), value);
	impl_->layer().SetFeature(feature);
}

auto Shapefile::get_field(not_null<void*> feature, const std::string& col_name, int& val) -> void
{ 
	val = static_cast<OGRFeature*>(feature.get())->GetFieldAsInteger(col_name.c_str());
}

auto Shapefile::get_field(not_null<void*> feature, const std::string& col_name, int64_t& val) -> void
{ 
	val = static_cast<OGRFeature*>(feature.get())->GetFieldAsInteger64(col_name.c_str());
}

auto Shapefile::get_field(not_null<void*> feature, const std::string& col_name, double& val) -> void
{ 
	val = static_cast<OGRFeature*>(feature.get())->GetFieldAsDouble(col_name.c_str());
}

auto Shapefile::get_field(not_null<void*> feature, const std::string& col_name, std::string& val) -> void
{ 
	val = static_cast<OGRFeature*>(feature.get())->GetFieldAsString(col_name.c_str());
}

#ifdef _DEBUG
void Shapefile::check_invariant() const noexcept
{
	assert(impl_);
}
#endif

}

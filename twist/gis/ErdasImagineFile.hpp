/// @file ErdasImagineFile.hpp
/// ErdasImagineFile class

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_GIS_ERDAS_IMAGINE_FILE_HPP
#define TWIST_GIS_ERDAS_IMAGINE_FILE_HPP

#include "twist/gis/gis_geometry.hpp"
#include "twist/math/FlatMatrix.hpp"

namespace twist::gis {

/*! Class which represents an open connection to a binary raster file following the "ERDAT Imagine" file format (we 
    will refer to it as an IMG file as its typical extension is ".img").
 */
class ErdasImagineFile {
public:
	//! The map coordinate system information which can be embedded in an IMG file.
	class MapInfo {
	public:
		//! The units used by the coordinate system
		enum class Unit { 
			metre ///< Metre
		};

		//! The type of the projection information
		enum class ProjectionType {
	        internal, ///< Projection is built into the eprj package as function calls 
            external ///< Projection is accessible as an EXTERNal executable 
		};

		enum class DatumType {
			parametric, ///< The datum info is 7 doubles
			grid, ///< The datum info is a name
			regression,
			none
		};

		struct ProjectionSpheroid {
			std::wstring  sphere_name; ///< name of the ellipsoid 
			double  a; ///< semi-major axis of ellipsoid
			double  b; ///< semi-minor axis of ellipsoid 
			double  e_squared; ///< eccentricity-squared
			double  radius; ///< radius of the sphere
		};

		struct Datum {
			std::wstring datum_name; ///< name of the datum
			DatumType type; ///< The datum type
			std::array<double, 7> params; ///< The parameters for type k_parametric 
			std::wstring grid_name; ///< name of the grid file
		};

		struct ProjectionParams {			
			ProjectionType proj_type;  ///< projection type
			int proj_number;  ///< projection number for internal projections
			std::wstring proj_exe_name;  ///< projection executable name for EXTERNal projections
			std::wstring proj_name;  ///< projection name
			int proj_zone;  ///< projection zone (UTM, SP only)
			std::array<double, 15> proj_params;  ///< projection parameters array in the GCTP form
			ProjectionSpheroid proj_spheroid;  ///< projection spheroid
			std::optional<Datum> datum;
		};

		MapInfo(const SquareGeoSpaceGrid& space_grid, Unit unit, const std::wstring& projection_name);

		[[nodiscard]] auto space_grid() const -> SquareGeoSpaceGrid;

		Unit unit() const;

		std::wstring projection_name() const;

		std::optional<std::wstring> refsys_proj_string() const;

		void set_refsys_proj_string(const std::wstring& str);

		std::optional<ProjectionParams> projection_params() const;

		void set_projection_params(const ProjectionParams& datum);

	private:
		const SquareGeoSpaceGrid space_grid_;
		const Unit unit_;
		const std::wstring proj_name_;
		std::optional<std::wstring> refsys_proj_string_;
		std::optional<ProjectionParams> proj_params_;
		TWIST_CHECK_INVARIANT_DECL
	};

	enum class OpenMode {};
	enum class CreateMode {};

	static const OpenMode open{};
	static const CreateMode create{};

	template<class T> using DataGrid = twist::math::FlatMatrix<T>;

    /*! Constructor. This constructor creates a new IMG file and opens a connection to it.        
        \param[in] tag  Tag value used for tag dispatching
        \param[in] path  Path to the new IMG file
        \param[in] width  The width of the new IMG file in pixels/cells; in other words the number of columns in the 
		                  underying grid
        \param[in] height  The height of the new IMG file in pixels/cells; in other words the number of rows in the 
						   underying grid
        \param[in] data_type  The data type of a pixel/cell value
	 */   
	ErdasImagineFile(CreateMode /*tag*/, fs::path path, Ssize width, Ssize height, GeoGridDataType data_type);  

    /*! Constructor. This constructor opens a connection to an existing IMG file.
        \param[in] tag  Tag value used for tag dispatching
        \param[in] path  Path to the IMG file
	 */
	ErdasImagineFile(OpenMode /*tag*/, fs::path path);  

	~ErdasImagineFile();

	//! The IMG file path.
	[[nodiscard]] auto path() -> fs::path;

	//! The width of the IMG file in pixels/cells.
	[[nodiscard]] auto width() const -> Ssize;

	//! The height of the IMG file in pixels/cells.
	[[nodiscard]] auto height() const -> Ssize;
	
	//! The number of bands in the image.
	[[nodiscard]] auto nof_bands() const -> int;
	
    //! The data type of a pixel/cell value.
	[[nodiscard]] auto data_type() const -> GeoGridDataType;

	/*! Get the "no-data" value placeholder in a specific band.
	    \param[in] band  The band (one-based) index; if invalid, an exception is thrown 
		\return  The "no-data" value; or nullopt if there is none in the band
	 */
	[[nodiscard]] auto nodata_value(int band = 1) const -> std::optional<double>;

	/*! Read the geo-referencing information included in the IMG file (if any), which describes the geographic 
	    coordinates of the underlying spatial grid.
	    \return  The map (geo-referencing) information, or nullopt if the IMG file does not include such information
	 */
	[[nodiscard]] auto read_map_info() const -> std::optional<MapInfo>;

	/*! Write geo-referencing information, which describes the geographic coordinates of the underlying 
	    spatial grid, to the IMG file. Existing map information is overwritten.
	    \param[in] info  The map (geo-referencing) information
	 */
	auto write_map_info(const MapInfo& info) -> void;

	/*! Read all the cell values from the IMG file, when the file stores 8-bit unsigned integer data values.
	    \param[out]  nodata_value  The "no-data" placeholder value, if the file uses one; nullopt otherwise
	    \return  Data grid populated with the cell values read from the file; the grid dimensions will match the 
		         dimensions of the grid underlying the IMG file
	 */
	[[nodiscard]] auto read_uint8(std::optional<uint8_t>& nodata_value) const -> std::unique_ptr<DataGrid<uint8_t>>;

	/// Read all the cell values from the IMG file, when the file stores 16-bit signed integer data values.
	///
	/// @param[in] nodata_value  The "no-data" placeholder value, if the file uses one; nullopt otherwise
	/// @return  Data grid populated with the cell values read from the file; the grid dimensions will 
	///					match the dimensions of the grid underlying the IMG file
	///
	std::unique_ptr<DataGrid<int16_t>> read_int16(std::optional<int16_t>& nodata_value) const;

	/// Read all the cell values from the IMG file, when the file stores 16-bit unsigned integer data values.
	///
	/// @param[in] nodata_value  The "no-data" placeholder value, if the file uses one; nullopt otherwise
	/// @return  Data grid populated with the cell values read from the file; the grid dimensions will 
	///					match the dimensions of the grid underlying the IMG file
	///
	std::unique_ptr<DataGrid<uint16_t>> read_uint16(std::optional<uint16_t>& nodata_value) const;

	/// Read all the cell values from the IMG file, when the file stores 32-bit signed integer data values.
	///
	/// @param[in] nodata_value  The "no-data" placeholder value, if the file uses one; nullopt otherwise
	/// @return  Data grid populated with the cell values read from the file; the grid dimensions will 
	///					match the dimensions of the grid underlying the IMG file
	///
	std::unique_ptr<DataGrid<int32_t>> read_int32(std::optional<int32_t>& nodata_value) const;

	/// Write all the cell values in the IMG file. The data value type of the file must be 8-bit unsigned integer.
	///
	/// @param[in] data  Data grid containing the cell values to write; the grid dimensions must match the
	///					dimensions of the grid underlying the IMG file
	/// @param[in] nodata_value  Optional "no-data" placeholder value
	///
	void write_uint8(const DataGrid<uint8_t>& data, std::optional<uint8_t> nodata_value);

	/// Write all the cell values in the IMG file. The data value type of the file must be 16-bit integer.
	///
	/// @param[in] data  Data grid containing the cell values to write; the grid dimensions must match the
	///					dimensions of the grid underlying the IMG file
	/// @param[in] nodata_value  Optional "no-data" placeholder value
	///
	void write_int16(const DataGrid<int16_t>& data, std::optional<int16_t> nodata_value);

	/// Write all the cell values in the IMG file. The data value type of the file must be 16-bit unsigned integer.
	///
	/// @param[in] data  Data grid containing the cell values to write; the grid dimensions must match the dimensions 
	///                  of the grid underlying the IMG file
	/// @param[in] nodata_value  Optional "no-data" placeholder value
	///
	void write_uint16(const DataGrid<uint16_t>& data, std::optional<uint16_t> nodata_value);

	/// Write all the cell values in the IMG file. The data value type of the file must be 32-bit signed integer.
	///
	/// @param[in] data  Data grid containing the cell values to write; the grid dimensions must match the
	///					dimensions of the grid underlying the IMG file
	/// @param[in] nodata_value  Optional "no-data" placeholder value
	///
	void write_int32(const DataGrid<int32_t>& data, std::optional<int32_t> nodata_value);

	TWIST_NO_COPY_NO_MOVE(ErdasImagineFile)

private:
	template<typename T> using BlockBuffer = std::unique_ptr<T, void(*)(void*)>;

	void conclude_init();

	template<typename T> std::unique_ptr<DataGrid<T>> read(std::optional<T>& nodata_value) const;

	template<typename T> void write(const DataGrid<T>& data, std::optional<T> nodata_value);

	template<typename T> BlockBuffer<T> prepare_tile_buffer(int& tiles_per_row, int& tiles_per_col) const;

	const fs::path path_;

	class Handle;
	std::shared_ptr<Handle> handle_;	

	Ssize width_{0};
	Ssize height_{0};
	int nof_bands_{0};
	GeoGridDataType data_type_{0};
	int data_type_bits_{0};

	Ssize tile_width_{0};
	Ssize tile_height_{0};
	int bytes_per_tile_{0};

	TWIST_CHECK_INVARIANT_DECL
};

// --- Free functions ---

/// Read all the cell values from an IMG ("ERDAT Imagine") file.
///
/// @tparam  DataVal  The (numeric) type of a cell data value 
/// @param[in] file  The IMG file  
/// @param[in] nodata_value  The "no-data" placeholder value, if the file uses one; nullopt otherwise
/// @return  Data grid populated with the cell values read from the file; the grid dimensions will 
///					match the dimensions of the grid underlying the IMG file
///
template<class DataVal,
         class = EnableIfNumber<DataVal>>
std::unique_ptr<ErdasImagineFile::DataGrid<DataVal>> read(const ErdasImagineFile& file, 
		std::optional<DataVal>& nodata_value);

}

#endif

#include "twist/gis/ErdasImagineFile.ipp"

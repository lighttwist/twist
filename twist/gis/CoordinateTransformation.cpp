/// @file CoordinateTransformation.cpp
/// Implementation file for "CoordinateTransformation.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/gis/CoordinateTransformation.hpp"

#include "twist/gis/SpatialReferenceSystem.hpp"
#include "twist/gis/twist_gdal_utils.hpp"

#include "gdal/include/ogr_spatialref.h"

namespace twist::gis {

// --- CoordinateTransformation::Impl struct ---

class CoordinateTransformation::Impl {
public:
	Impl(const SpatialReferenceSystem& src_refsys, const SpatialReferenceSystem& dest_refsys) 
	{ 
		trans_.reset(OGRCreateCoordinateTransformation(&src_refsys.get(), &dest_refsys.get()));	
		if (!trans_) {
			TWIST_THRO2(L"Failed to create coordinate transformation: {}", get_last_cpl_error_msg());
		}
	}

	[[nodiscard]] auto get() const -> OGRCoordinateTransformation& { return *trans_; }

private:
	std::unique_ptr<OGRCoordinateTransformation> trans_;
};

// --- CoordinateTransformation class ---

CoordinateTransformation::CoordinateTransformation(const SpatialReferenceSystem& src_refsys, 
												   const SpatialReferenceSystem& dest_refsys)
	: impl_{std::make_unique<CoordinateTransformation::Impl>(src_refsys, dest_refsys)}
{
	TWIST_CHECK_INVARIANT
}

CoordinateTransformation::CoordinateTransformation(CoordinateTransformation&&) = default;


CoordinateTransformation::~CoordinateTransformation()
{
}

auto CoordinateTransformation::operator=(CoordinateTransformation&&) -> CoordinateTransformation& = default;

auto CoordinateTransformation::transform(const GeoPoint& pt) const -> GeoPoint
{
	TWIST_CHECK_INVARIANT
	auto x = pt.x();
	auto y = pt.y();
	if (impl_->get().Transform(1, &x, &y) != 1) {
		TWIST_THRO2(L"Failed to apply coordinate transformation.");		
	}
	return GeoPoint{x, y};
}

auto CoordinateTransformation::get() const -> OGRCoordinateTransformation&
{
	TWIST_CHECK_INVARIANT
	return impl_->get();
}

#ifdef _DEBUG
void CoordinateTransformation::check_invariant() const noexcept
{
	assert(impl_);
}
#endif

}

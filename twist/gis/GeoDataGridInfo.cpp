/// @file GeoDataGridInfo.cpp
/// Implementation file for "GeoDataGridInfo.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "twist/twist_maker.hpp"

#include "twist/gis/GeoDataGridInfo.hpp"

using namespace twist::math;

namespace twist::gis {

// --- GeoDataGridInfo class ---

GeoDataGridInfo::GeoDataGridInfo(const GeoSpaceGrid& space_grid, std::string nodata_value, 
                                 GeoGridDataType data_value_type)
	: space_grid_{space_grid}
	, nodata_value_{std::move(nodata_value)}
	, data_value_type_{data_value_type}
{
	TWIST_CHECK_INVARIANT
}

auto GeoDataGridInfo::space_grid() const -> GeoSpaceGrid
{
	TWIST_CHECK_INVARIANT
	return space_grid_;
}

auto GeoDataGridInfo::nodata_value() const -> std::string
{
	TWIST_CHECK_INVARIANT
	return nodata_value_;
}

auto GeoDataGridInfo::data_value_type() const -> GeoGridDataType
{
	TWIST_CHECK_INVARIANT
	return data_value_type_;
}

#ifdef _DEBUG
auto GeoDataGridInfo::check_invariant() const noexcept -> void
{
	assert((GeoGridDataType::int8 <= data_value_type_ && 
            data_value_type_ <= GeoGridDataType::float64) || 
           (GeoGridDataType::comp_uint8_uint16 <= data_value_type_ && 
            data_value_type_ <= GeoGridDataType::comp_float32_float32_float32));
	if (!nodata_value_.empty()) {
		assert(!is_whitespace(nodata_value_));
	}
}
#endif

}

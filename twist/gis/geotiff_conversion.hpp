/// @file geotiff_conversion.hpp
/// Utilities for converting between geotiffs to other raster file formats

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef TWIST_GIS_GEOTIFF__CONVERSION_HPP
#define TWIST_GIS_GEOTIFF__CONVERSION_HPP

#include "twist/concepts.hpp"
#include "twist/db/BinmatFile.hpp"
#include "twist/gis/gis_geometry.hpp"
#include "twist/img/Tiff.hpp"

namespace twist::gis {
class GeoDataGridInfo;
}

namespace twist::gis {

/*! Class for converting a geotiff file into other gridded data formats (eg to a binmat file).
    \tparam TiffVal  The geotiff data value type
 */ 
template<class TiffVal>
class GeotiffConverter {
public:
	/*! Constructor.
	    \param[in] geotiff_path  The geotiff path
	 */
	GeotiffConverter(const fs::path& geotiff_path);

	//! The spatial grid underlying the geotiff.
	[[nodiscard]] auto geotiff_space_grid() const -> GeoSpaceGrid;

	/*! Read data values from the geotiff file and write them, after applying a transformation to each value, to a 
	    newly created binmat file whose underlying spatial grid is aligned with the geotiff's. This function can 
        either convert all the data in the geotiff grid, to a binmat having the same dimensions as the geotiff; or only 
        a subgrid of the geotiff data, defined by a rectangular geographic zone which is intersected with the geotiff 
        grid. 
	    \tparam BinmatVal  The value type of the output binmat 
	    \tparam Transformation  The type of the transformation; a callable type
	    \param[in] binmat_path  The path of the output binmat file; if the file already exists, an exception is thrown  
	    \param[in] zone_to_convert  A geographic rectangle whose intersection with the geotiff grid defines the subgrid 
		                            to convert (the smallest subgrid which contains the rectangle will be converted); 
	                                pass in nullopt to convert the whole geotiff grid
	    \param[in] nodata_val_override  An override of the "no-data" value in the geotiff (if any); the "no-data" 
		                                values in the geotiff will be replaced with the numeric value represented by 
										this string in the output binmat; pass in an empty string for no override
	    \param[in] transform  Transformation to be applied to each geotiff cell data value before it is written to the 
                              binmat
		\param[in] zone_snap_tol  If the zone rectangle is provided and any side of it is within this tolerance from a 
		                          row or column boundary in the space grid underlying the binmat, then the subgrid 
								  which will be converted will snap to that boundary
        \return  Information about the information about a geographic data grid corresponding to the binmat
	 */
	template<class BinmatVal, 
	         InvocableR<BinmatVal, TiffVal> Transform = std::identity>
	auto convert_to_binmat(const fs::path& binmat_path, 
						   std::optional<GeoRectangle> zone_to_convert, 
						   std::string nodata_val_override,
						   Transform transform = {},
						   double zone_snap_tol = twist::math::def_grid_snap_tol) const -> GeoDataGridInfo;

	/*! Read data values from the geotiff file and write them, after applying a resampling transformation, to a newly 
        created binmat file whose underlying spatial grid is not aligned with the geotiff's. 
	    \tparam BinmatVal  The value type of the output binmat 
        \tparam Transform  Callable type for the data resampling transformation 
        \param[in] binmat_space_grid  The space grid underlying the output binmat
        \param[in] binmat_nodata_val  Textual representation of the "no-data" placehodler value for the binmat; empty 
                                      string for none
        \param[in] binmat_path  Path to the output binmat
        \param[in] transform  Data resampling transformation; it will be called once for each binmat cell: it returns 
                              the value to write in the binmat cell, and its parameters are: 
                               * the space grid underlying the data grid which was read from the geotiff (the function 
                                 makes sure this covers the binmat's space grid perimeter)
                               * the data grid read from the geotiff
                               * the space grid underlying the binmat
                               * the binmat cell row index
                               * the binmat cell column index
        \return  Information about the information about a geographic data grid corresponding to the binmat
     */
    template<class BinmatVal, 
             InvocableR<BinmatVal, const SquareGeoSpaceGrid& /*tiff_space_grid*/,
                                   const twist::math::FlatMatrix<TiffVal>& /*tiff_grid_data*/,
                                   const SquareGeoSpaceGrid& /*binmat_space_grid*/,
                                   Ssize /*binmat_row_idx*/,
                                   Ssize /*binmat_col_idx*/> Transform>
    auto resample_and_convert_to_binmat(const SquareGeoSpaceGrid& binmat_space_grid,
                                        std::string binmat_nodata_val,
                                        fs::path binmat_path,
                                        Transform transform) -> GeoDataGridInfo;

	TWIST_NO_COPY_NO_MOVE(GeotiffConverter)

private:
	template<class BinmatVal, 
			 InvocableR<BinmatVal, TiffVal> Transform> 
	[[nodiscard]] auto apply_conversion(twist::db::BinmatFile<BinmatVal>& binmat_file, 
										std::optional<twist::SubgridInfo> subgrid_to_convert_info, 
										std::string nodata_val,
										std::optional<Transform> transform) const -> GeoDataGridInfo;

	twist::img::Tiff tiff_;
	std::optional<GeoDataGridInfo> geotiff_info_;

	TWIST_CHECK_INVARIANT_DECL
};

// --- Free functions ---

/*! Convert a binmat file to a geotiff. The binmat is expected to store the cell values of a single matrix. 
    A transformation can be applied to the binmat data before writing it to the geotiff.
    \tparam BinmatVal  The type of the data value in a binmat cell 
    \tparam TiffVal  The type of the data value in a geotiff cell 
	\tparam Transform  The type of the (optional) callable used to transform a binmat cell value into a geotiff cell 
                       value
	\param[in] binmat  Connection to the binmat file
	\param[in] geotiff_path  Geotiff path; if a file with this path already exists, an exception is thrown  
	\param[in] geotiff_space_grid  Spatial grid underlying the geotiff data; must match the binmat data matrix 
	                               dimensions  
	\param[in] geotiff_nodata_val  Geotiff "no-data" value; pass in nullopt for none  
	\param[in] geotiff_proj_info  Geotiff projection info; pass in nullopt for none  
	\param[in] transform  Callable to transform a binmat cell value into a geotiff cell value; pass in std::identity 
                          for none
    \param[in] bigtiff  Whether the output geotiff should be marked as BigTIFF (bigger than 4GB)
 */ 
template<class BinmatVal, 
         class TiffVal = BinmatVal,
		 InvocableR<TiffVal, BinmatVal> Transform = std::identity>
auto binmat_to_geotiff(const twist::db::BinmatFile<BinmatVal>& binmat, 
                       fs::path geotiff_path,
					   const SquareGeoSpaceGrid& geotiff_space_grid,
					   std::optional<TiffVal> geotiff_nodata_val = {},
					   std::optional<GeotiffProjection> geotiff_proj_info = {},
					   Transform transform = {},
					   bool bigtiff = false) -> void;

}

#include "twist/gis/geotiff_conversion.ipp"

#endif

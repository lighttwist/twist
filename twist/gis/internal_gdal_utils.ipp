/// @file internal_gdal_utils.cpp
/// Inline implementation file for "internal_gdal_utils.hpp"

//  Copyright (c) 2007-2025, Dan Ababei
//	All rights reserved
//
//	Redistribution and use in source and binary forms, with or without modification, are permitted provided that the 
//  following conditions are met:
//    * Redistributions of source code must retain the above copyright notice, this list of conditions and the 
//      following disclaimer.
//	  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the 
//      following disclaimer in the documentation and/or other materials provided with the distribution.
//    * The names Dan Ababei and Lighttwist may not be used to endorse or promote products derived from this software 
//      without specific prior written permission.
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
//  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
//  DISCLAIMED. IN NO EVENT SHALL DAN ABABEI BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
//  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
//  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
//  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace twist::gis {

[[nodiscard]] constexpr auto to_gdal(FeatureFieldType type) -> OGRFieldType
{
	switch (type) {
	case FeatureFieldType::int32: return OFTInteger;                    
    case FeatureFieldType::int32_list: return OFTIntegerList;        
    case FeatureFieldType::int64: return OFTInteger64;                        
    case FeatureFieldType::int64_list: return OFTInteger64List;                          
	case FeatureFieldType::float64: return OFTReal;               
    case FeatureFieldType::float64_list: return OFTRealList;                             
    case FeatureFieldType::string: return OFTString;                        
    case FeatureFieldType::string_list: return OFTStringList;                              
    case FeatureFieldType::binary: return OFTBinary;                             
    case FeatureFieldType::date: return OFTDate;                                           
    case FeatureFieldType::time: return OFTTime;                                          
    case FeatureFieldType::date_time: return OFTDateTime;                                 
	default: TWIST_THROW(L"Invalid 'feature field type' value %d.", type);
	}
}

[[nodiscard]] constexpr auto to_gdal(VectorGeometryType geom_type) -> OGRwkbGeometryType
{
	switch (geom_type) {
	case VectorGeometryType::point:
		return wkbPoint;
	case VectorGeometryType::line_string:
		return wkbLineString;
	case VectorGeometryType::polygon:
		return wkbPolygon;
	default:
		TWIST_THRO2(L"Unrecognised VectorGeometryType value {}.", static_cast<int>(geom_type));
	}
}

}
